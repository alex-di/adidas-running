<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    private $_config = null;

    /**
     * инициализация сессии
     */
    protected function _initSession()
    {
        return new Zend_Session_Namespace("zend");
    }

    protected function _initTranslate()
    {
        //DR_View_Helper_Translate::enabledLanguages();
    }

    /**
     * Инициализация классов
     *
     */

    protected function _initAutoload()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            $_SERVER['HTTP_REFERER'] = '';
        }

        $moduleLoader = new Zend_Application_Module_Autoloader(array('namespace' => '', 'basePath' => APPLICATION_PATH));
        return $moduleLoader;
    }
    
    /**
     * Инициализация кеша
     *
     */
    public function _initCache() {

        Zend_Registry::set('cache', new DR_Models_Cache($_SERVER['DOCUMENT_ROOT'] . '/cache/'));
    }
    
    
    /**
     *
     * Инициализация Вида
     */
    protected function _initView()
    {
        # - ИнициализациЯ объекта Zend_View
        $view = new Zend_View();

        # - Задание базового URL
        $view->baseUrl = '/';
        # - Задание пути для view части
        $view->setBasePath(ROOT_PATH.'/application/views/');

        # Хелперы
        $view->addHelperPath('DR/View/Helper/', 'DR_View_Helper');


        # - Настройка расширения view скриптов с помощью Action помошников
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');

        $viewRenderer->setView($view)->setViewSuffix('tpl');

        return $view;

    }

    /**
     *
     * инициализация настроек
     */
    protected function _initConfig()
    {
        require APPLICATION_PATH . '/settings/config.php';
        $this->_config = new Zend_Config($config);
    }

    /**
     *
     * ИнициализациЯ Базы
     *	-	Назначение: База данных
     *	-	Дата: 31.12.2011 20:25
     * @version 1.0
     * @final
     */
    public function _initDbAdapter()
    {

        $mysql = Zend_Db::factory($this->_config->mysql);


        # - Задание адаптера по умолчанию для наследников класса Zend_Db_Table_Abstract
        Zend_Db_Table_Abstract::setDefaultAdapter($mysql);

        # - Занесение объекта соединениЯ c базой в реестр
        Zend_Registry::set('mysql', $mysql);
        
        #Кеш метаданных
        Zend_Registry::set('db_meta_cache', $this->_config->debug->metacache);
        Zend_Registry::set('db_cache', $this->_config->debug->cache);
        Zend_Registry::set('debug', $this->_config->debug->on);
        #die('INIT DB');
    }


    //инициализация ЧПУ
    public function _initRewrite()
    {
        # - Создание объекта front контроллера
        $router = new Zend_Controller_Router_Rewrite();
        $front = Zend_Controller_Front::getInstance();    

        $router->addRoute("article_view", new Zend_Controller_Router_Route_Regex('article/(.+)', array(
            'controller' => 'index',
            'action' => 'index',
            'module' => 'articles'), array(1 => "stitle"), "article/%s"
        ));
        $router->addRoute("route_view", new Zend_Controller_Router_Route_Regex('route/(.+)', array(
            'controller' => 'index',
            'action' => 'index',
            'module' => 'articles'), array(1 => "stitle"), "route/%s"
        ));
        $router->addRoute("event_view", new Zend_Controller_Router_Route_Regex('event/(.+)', array(
            'controller' => 'index',
            'action' => 'index',
            'module' => 'articles'), array(1 => "stitle"), "event/%s"
        ));
        # Профиль
        $router->addRoute("profile", new Zend_Controller_Router_Route_Regex('profile11/(.+)', array(
            'controller' => 'profile',
            'action' => 'index',
            'module' => 'users'), array(1 => "id"), "profile11/%s"
        ));
        $router->addRoute("profile_view", new Zend_Controller_Router_Route_Regex('profile/(.+)', array(
            'controller' => 'profile',
            'action' => 'view',
            'module' => 'users'), array(1 => "id"), "profile/%s"
        ));
        $router->addRoute("profile_clothes", new Zend_Controller_Router_Route_Regex('profile/(.+)/clothes', array(
            'controller' => 'clothes',
            'action' => 'index',
            'module' => 'users'), array(1 => "id"), "profile/%s/clothes"
        ));
        $router->addRoute("profile_favorites", new Zend_Controller_Router_Route_Regex('profile/(.+)/favorites', array(
            'controller' => 'favorites',
            'action' => 'index',
            'module' => 'users'), array(1 => "id"), "profile/%s/favorites"
        ));
        $router->addRoute("profile_articles_modern_view", new Zend_Controller_Router_Route_Regex('profile/(.+)/articles/modern', array(
            'controller' => 'articles',
            'action' => 'view',
            'module' => 'users',
            'type' => 'modern'), array(1 => "id"), "profile/%s/articles/modern"
        ));
        $router->addRoute("profile_articles_draft_view", new Zend_Controller_Router_Route_Regex('profile/(.+)/articles/draft', array(
            'controller' => 'articles',
            'action' => 'view',
            'module' => 'users',
            'type' => 'draft'), array(1 => "id"), "profile/%s/articles/draft"
        ));
        $router->addRoute("profile_articles_add", new Zend_Controller_Router_Route_Regex('profile/(.+)/articles/add', array(
            'controller' => 'articles',
            'action' => 'edit',
            'module' => 'users'), array(1 => "id"), "profile/%s/articles/add"
        ));
        $router->addRoute("profile_articles_edit", new Zend_Controller_Router_Route_Regex('profile/(.+)/articles/edit/(.+)', array(
            'controller' => 'articles',
            'action' => 'edit',
            'module' => 'users'), array(1 => "id", 2=>'article_id'), "profile/%s/articles/edit/%s"
        ));

        $router->addRoute("profile_routes_add", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/add', array(
            'controller' => 'routes',
            'action' => 'edit',
            'module' => 'users'), array(1 => "id"), "profile/%s/routes/add"
        ));
        $router->addRoute("profile_routes_edit", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/edit/(.+)', array(
            'controller' => 'routes',
            'action' => 'edit',
            'module' => 'users'), array(1 => "id", 2=>'route_id'), "profile/%s/routes/edit/%s"
        ));
        $router->addRoute("profile_routes_run", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/run', array(
            'controller' => 'routes',
            'action' => 'view',
            'module' => 'users',
            'type'=>'run'), array(1 => "id"), "profile/%s/routes/run"
        ));
        $router->addRoute("profile_routes_like", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/like', array(
            'controller' => 'routes',
            'action' => 'view',
            'module' => 'users',
            'type'=>'like'), array(1 => "id"), "profile/%s/routes/like"
        ));
        $router->addRoute("profile_routes_modern", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/modern', array(
            'controller' => 'routes',
            'action' => 'view',
            'module' => 'users',
            'type'=>'modern'), array(1 => "id"), "profile/%s/routes/modern"
        ));
        $router->addRoute("profile_routes_draft", new Zend_Controller_Router_Route_Regex('profile/(.+)/routes/draft', array(
            'controller' => 'routes',
            'action' => 'view',
            'module' => 'users',
            'type'=>'draft'), array(1 => "id"), "profile/%s/routes/draft"
        ));
        # статические страници
        $router->addRoute("pages", new Zend_Controller_Router_Route_Regex('pages/(.+)', array(
            'controller' => 'view',
            'action' => 'index',
            'module' => 'pages'), array(1 => "link"), "pages/%s"
        ));
        
        //контакты
        $router->addRoute("contacts", new Zend_Controller_Router_Route('contacts', array(
            'controller' => 'view',
            'action' => 'index',
            'module' => 'pages', 
            'link'=>'contacts')));
        
        //Ошибка 404
        $router->addRoute("404", new Zend_Controller_Router_Route('error404', array(
            'controller' => 'error',
            'action' => 'error404',
            'module' => 'default', 
        )));
        
        //События
        $router->addRoute("events", new Zend_Controller_Router_Route('events', array(
            'controller' => 'events',
            'action' => 'index',
            'module' => 'materials', 
        )));
        
        //События - будущие
        $router->addRoute("eventsfuture", new Zend_Controller_Router_Route('/events/future', array(
            'controller' => 'events',
            'action' => 'index',
            'module' => 'materials',
            'period' => 'future',
        )));
        
        //События - прошедшие
        $router->addRoute("eventspast", new Zend_Controller_Router_Route('/events/past', array(
            'controller' => 'events',
            'action' => 'index',
            'module' => 'materials',
            'period' => 'past'
        )));
        


        $router->addRoute("social", new Zend_Controller_Router_Route('social', array(
            'controller' => 'social',
            'action' => 'index',
            'module' => 'materials', 
        )));
        
        $router->addRoute("clothes", new Zend_Controller_Router_Route('clothes', array(
            'controller' => 'clothes',
            'action' => 'index',
            'module' => 'materials', 
        )));
        $router->addRoute("micoach", new Zend_Controller_Router_Route('micoach', array(
            'controller' => 'micoach',
            'action' => 'index',
            'module' => 'materials', 
        )));
        $router->addRoute("trainings", new Zend_Controller_Router_Route('trainings', array(
            'controller' => 'trainings',
            'action' => 'index',
            'module' => 'materials', 
        )));
        $router->addRoute("motivation", new Zend_Controller_Router_Route('inspirate', array(
            'controller' => 'motivation',
            'action' => 'index',
            'module' => 'materials', 
        )));
        $router->addRoute("runbase", new Zend_Controller_Router_Route('runbase', array(
            'controller' => 'runbase',
            'action' => 'index',
            'module' => 'materials', 
        )));
        $router->addRoute("runbaseevents", new Zend_Controller_Router_Route('runbase/events', array(
            'controller' => 'runbase',
            'action' => 'events',
            'module' => 'materials', 
        )));
        $router->addRoute("runbasecard", new Zend_Controller_Router_Route('card', array(
            'controller' => 'runbase',
            'action' => 'card',
            'module' => 'materials', 
        )));
        
        $router->addRoute("runbasecontacts", new Zend_Controller_Router_Route('runbase/contacts', array(
            'controller' => 'runbase',
            'action' => 'contacts',
            'module' => 'materials', 
        )));

        $router->addRoute("iaaf", new Zend_Controller_Router_Route('iaaf', array(
            'controller' => 'iaaf',
            'action' => 'index',
            'module' => 'materials',
        )));

        $router->addRoute("springblade", new Zend_Controller_Router_Route('springblade', array(
            'controller' => 'springblade',
            'action' => 'index',
            'module' => 'materials',
        )));

        $router->addRoute("boost", new Zend_Controller_Router_Route('boost', array(
            'controller' => 'boost',
            'action' => 'index',
            'module' => 'materials',
        )));

        $router->addRoute("energy", new Zend_Controller_Router_Route('energy', array(
            'controller' => 'energy',
            'action' => 'index',
            'module' => 'materials',
        )));

        $router->addRoute("future", new Zend_Controller_Router_Route('future', array(
            'controller' => 'future',
            'action' => 'index',
            'module' => 'materials'
        )));

        $router->addRoute("calendar", new Zend_Controller_Router_Route('calendar', array(
            'controller' => 'calendar',
            'action' => 'index',
            'module' => 'materials',
        )));

        $router->addRoute("search_query", new Zend_Controller_Router_Route_Regex('search/q/(.+)', array(
            'controller' => 'index',
            'action' => 'index',
            'module' => 'search', 
        ), array(1 => "query"), "search/q/%s"));
        
        $router->addRoute("search_tag", new Zend_Controller_Router_Route_Regex('search/tag/(.+)', array(
            'controller' => 'index',
            'action' => 'index',
            'module' => 'search', 
        ), array(1 => "tag"), "search/tag/%s"));
        
        $router->addRoute("items_review", new Zend_Controller_Router_Route_Regex('items/review/(.+)', array(
            'controller' => 'review',
            'action' => 'index',
            'module' => 'items', 
        ), array(1 => "stitle"), "items/review/%s"));
        
        $router->addRoute("dialogs", new Zend_Controller_Router_Route_Regex('messages/dialog/(.+)', array(
            'controller' => 'dialog',
            'action' => 'index',
            'module' => 'messages', 
        ), array(1 => "users_id"), "messages/dialog/%s"));

        $front->setRouter($router);

        return $front;
    }

}
