
    <footer>
    	     <div class="limit"> 
               <nav class="left_menu">
                    <ul>
                         <li class="first"><a target="_blank" href="http://www.adidas.ru/shop">shop</a></li>
                         <li class=""><a target="_blank" href="http://www.adidas.ru/men/men,ru_RU,sc.html">Мужчины</a></li>
                         <li><a target="_blank" href="http://www.adidas.ru/women/women,ru_RU,sc.html">женщины</a></li>
                         <li><a target="_blank" href="http://www.adidas.ru/kids/kids,ru_RU,sc.html">Дети</a></li>
                    </ul>
               </nav> 
               <nav class="right_menu">
                    <ul>
                         <li><a target="_blank" href="http://www.adidas.ru/football/Football,ru_RU,sc.html">футбол</a></li>
                         <li class=""><a target="_blank" href="http://www.adidas.ru/running/Running,ru_RU,sc.html">Бег</a></li>
                         <li><a target="_blank" href="http://www.adidas.ru/basketball/Basketball,ru_RU,sc.html">баскетбол</a></li>
                         <li><a target="_blank" href="http://www.adidas.ru/originals/Originals,ru_RU,sc.html">originals</a></li>                                                  
                         <li><a target="_blank" href="http://mygirls.adidas.ru">#mygirls</a></li>
                         <li><a target="_blank" href="http://discover.adidas.ru/goallin/">Go all in</a></li>
                    </ul>
               </nav>  
               <div style="clear: both;"></div> 
               <div class="copyright">
                    <div>© ООО "Адидас". Все права защищены.</div>
                    <a target="_blank" href="http://www.adidas.ru/%D0%A3%D1%81%D0%BB%D0%BE%D0%B2%D0%B8%D1%8F-%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D0%B8/help-topics-terms_and_conditions,ru_RU,pg.html">Юридическая информация</a>
                    <a target="_blank" href="http://www.adidas.ru/%D0%9E%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-%D0%BF%D0%B5%D1%80%D1%81%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85/help-topics-privacypolicy,ru_RU,pg.html">Политика конфиденциальности</a>
               </div>
          </div>
    </footer>
    
 <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21617272 = new Ya.Metrika({id:21617272,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21617272" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --> 

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5WWTPG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5WWTPG');</script>
<!-- End Google Tag Manager -->  
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42119313-1', 'adidas-running.ru');
  ga('send', 'pageview');
</script>

<script>
	 window.onload = $('.desc_text_block > .inner-bg').height(200);
</script>
<?if(Zend_Auth::getInstance()->hasIdentity()):?>
    <div class="modal" id="add_modal" style="display: none">
        <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_add')?>" class="add_article"><span></span>добавить статью</a>
        <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_add')?>" class="add_route"><span></span>добавить маршрут</a>
    </div>
<?endif;?>


<?
if(isset($_SESSION['showHints'])){
  unset($_SESSION['showHints']); 
  ?>
  <link href="/design/hints/css/hints.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="/design/hints/js/hints.js"></script>
  <div class="hints-over">
    <div class="hints-logo"></div>
    <div class="hints-arrows">
        <div class="hints-arrow hints-arrow-left disabled"></div>
        <div class="hints-arrow hints-arrow-right"></div>
    </div>
    <div class="hints-slides">
      <div class="hints-slide active hints-slide-1"></div>
      <div class="hints-slide hints-slide-2"></div>
      <div class="hints-slide hints-slide-3"></div>
      <div class="hints-slide hints-slide-4"></div>
      <div class="hints-slide hints-slide-5"></div>
      <div class="hints-slide hints-slide-6"></div>
      <div class="hints-slide hints-slide-7"></div>
      <div class="hints-slide hints-slide-8"></div>
      <div class="hints-slide hints-slide-9"></div>
      <div class="hints-slide hints-slide-10"></div>
      <div class="hints-slide hints-slide-11"></div>
      <div class="hints-slide hints-slide-12"></div>
    </div>
    <div class="hints-pages">
      <div class="hints-page active hints-page-1" data-page="1"></div>
      <div class="hints-page hints-page-2" data-page="2"></div>
      <div class="hints-page hints-page-3" data-page="3"></div>
      <div class="hints-page hints-page-4" data-page="4"></div>
      <div class="hints-page hints-page-5" data-page="5"></div>
      <div class="hints-page hints-page-6" data-page="6"></div>
      <div class="hints-page hints-page-7" data-page="7"></div>
      <div class="hints-page hints-page-8" data-page="8"></div>
      <div class="hints-page hints-page-9" data-page="9"></div>
      <div class="hints-page hints-page-10" data-page="10"></div>
      <div class="hints-page hints-page-11" data-page="11"></div>  
      <div class="hints-page hints-page-12" data-page="12"></div>  
    </div>
    <div class="hints-close"></div>
  </div>
  <?
}
?>
</body>
</html>