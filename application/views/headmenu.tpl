<header>
   <div class="banner">
        
     <?= $this->banner('glavnaya-banner-v-shapke', '<img src="/design/images/banner.png" alt="img" />') ?>
        
   </div>
   <div class="top_part">
        <div class="limit">
             <div class="span logo"><a href="/" class="logo"></a> </div>
             <div class="span menu">
                  <nav>
                       <ul>
                            <li class="yellow"><a href="<?echo $this->url(array(), 'social')?>">общение</a></li>
                            <li class="green"><a href="<?echo $this->url(array(), 'trainings')?>">тренировки</a></li>
                            <li class="black"><a href="<?echo $this->url(array(), 'motivation')?>">мотивация</a></li>
                       </ul>
                  </nav>
             </div>
             <div class="span search">
                  <form class="search" onsubmit="search(this);return false">
                       <input type="button" value=" " />
                       <input type="text" placeholder="ПОИСК" maxlength="20"/>
                  </form>
                  <script type="text/javascript">
                        function search(form) {
                            var value = $(form).find('input[type="text"]').val();
                            if('' != value && value.length > 2)
                                window.location = '<?echo $this->url(array('query'=>''), 'search_query')?>'+window.encodeURIComponent(value);
                        }
                  </script>
                  <ul class="social_ico">
                       <li class="fb"><a target="_blank" href="http://www.facebook.com/adidasRunning"></a></li>                                   
                       <li class="tw"><a target="_blank" href="https://twitter.com/adidasRU"></a></li>
                       <li class="vk"><a target="_blank" href="http://vk.com/adidasrunning"></a></li>
                  </ul>
                  <div style="clear: both;"></div>
             </div>
        </div>
   </div>
   <!-- ++++++++++++++++++++++++++++++++++++++++++ -->
   
   <div class="bottom_part limit">
        <nav>
             <ul>
                  <li><a href="<?echo $this->url(array(), 'clothes')?>">Экипировка</a></li>
                  <li><a href="/routes">Маршруты</a></li>
                  <li><a href="runbase/events">Runclub</a></li>
                  <li><a href="<?echo $this->url(array(), 'micoach')?>">micoach</a></li>
                  <li><a href="<?echo $this->url(array(), 'events')?>">События</a></li>
                  <li><a href="/users/famous">Люди</a></li>
             </ul>
        </nav>
	    <?php if(Zend_Auth::getInstance()->hasIdentity()): ?>
            <div class="login_logout">
                <?$eventsCounter = Model_Users::getCountEvents(Zend_Auth::getInstance()->getIdentity()->id)?>
                <div onclick='window.location="/auth/logout"' class="logout"><span></span></div>
                <?if($eventsCounter->count_modern_article):?>
                    <div class="n_box green" style="cursor: pointer;" onclick="window.location='<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_modern_view')?>'"><span></span><?echo $eventsCounter->count_modern_article?></div>
                <?endif;?>
                <div class="n_box black" style="cursor: pointer;display: none;" onclick="window.location='/messages'"><span></span>0</div>
                <div class="login">
                    <?$isHasAvatar = !empty(Zend_Auth::getInstance()->getIdentity()->avatar);?>
                    <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_view')?>" <?if($isHasAvatar):?>class="ava"<?endif;?>>
                        <? if($isHasAvatar): ?>
                            <img alt="img" src="<?= Model_Files::getPreview(Zend_Auth::getInstance()->getIdentity()->avatar, 100) ?>"/>
                        <?endif;?>
                    </a>
                    <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_view')?>" class="name">
                        <?= Zend_Auth::getInstance()->getIdentity()->name ?>
                    </a>
                </div>
                <div class="add_box add_link" onclick="showDialogFastAddArticles()"><span></span></div>
                <div style="clear: both;"></div>
            </div> 
	    <? else: 
            $request = Zend_Registry::get('request'); 
        ?>
			<? if($request['controller'] != 'login'): ?>
    			<div class="login_logout drop_menu_button">
    			    <div class="login ">Войти
    
    			    </div>
    			</div>
    			    <form class="login_form drop_menu_content" onsubmit="return jphp('/auth/login', this)">
    				    <label>войти с помощью</label>
    				    <a href="javascript:void(0)" onclick="vkLogin()" class="vk">вконтакте<span></span></a>
    				    <a href="javascript:void(0)" onclick="faceboockLogin()" class="fb">facebook<span></span></a>
    				    <a href="javascript:void(0)" onclick="twitterLogin()" class="tw">twitter<span></span></a>
                        <a href="javascript:void(0)" onclick="foursquareLogin()" class="fs">foursquare<span></span></a>
    				    <div class="control">
    					<input type="text" placeholder="E-mail" name="login"/>
    				    </div>
    				    <div class="control">
    					<input type="password" placeholder="Пароль" name="password"/>
    				    </div>
    				    <div class="link_area">
    					<a href="/auth/register">Регистрация</a>
    					<a href="/auth/remember">Забыли пароль?</a>
    				    </div>
    				    <input type="submit" value="вход"/>
    				    <div style="clear: both;"></div>
    				</form>
			<? endif;  ?>
	    <? endif ?>
  </div> 

</header>