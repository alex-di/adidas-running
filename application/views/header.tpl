<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<?php echo Zend_Layout::getMvcInstance()->getView()->metadata('adidas Running')?>

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<link href="/design/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/design/css/isotope.css" rel="stylesheet" type="text/css" />
<link href="/design/css/banner-rotator.css" rel="stylesheet" type="text/css"/> 
<link href="/design/css/style.css" rel="stylesheet" type="text/css" />
<link href="/design/css/fixes2.css" rel="stylesheet" type="text/css" />
<link href="/design/css/runbase.css?<?= rand(0, 10000) ?>" rel="stylesheet" type="text/css" />
<link href="/design/css/mediaqueries.css" rel="stylesheet" type="text/css" />
<link href="/design/css/modals.css" rel="stylesheet" type="text/css" />   
<link href="/design/css/fixes.css?<?= rand(0, 10000) ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 9]><link href="/design/css/ie_style.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 8]><link href="/design/css/ie8_style.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 7]><link href="/design/css/ie7_style.css" rel="stylesheet" type="text/css" /><![endif]-->


<script type="text/javascript" src="/design/js/jquery-1-9-0.js"></script>
<script type="text/javascript" src="/design/js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="/design/modules/migrate/src/migrate.js"></script>
<script type="text/javascript" src="/design/modules/migrate/src/core.js"></script>
<script type="text/javascript" src="/design/js/isotope.js"></script>    
<script type="text/javascript" src="/design/js/banner-rotator.js"></script>    
<script type="text/javascript" src="/design/js/banner-rotator-3.js"></script>
<script type="text/javascript" src="/design/js/simplegallery.js"></script>
<script type="text/javascript" src="/design/js/styled.js"></script>
<script type="text/javascript" src="/design/js/main.js"></script>
<script type="text/javascript" src="/design/js/placeholder.js"></script>
<script type="text/javascript" src="/design/js/dialogmessage.js"></script>


<script type="text/javascript" src="/design/js/slider.js"></script>
<script type="text/javascript" src="/design/modules/jqueryphp/js/jquery.php.js"></script>
<script type="text/javascript" src="/design/js/sollux.js"></script>

<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?49"></script>
<script type="text/javascript" src="/design/js/social/auth.js"></script>
<script type="text/javascript" src="/design/modules/rcarousel/lib/jquery.ui.rcarousel.min.js"></script>
<?echo $this->headLink()?>
<?echo $this->headScript() ?>
<script type="text/javascript">
  $(function(){
	  VK.init({apiId: 3661023});
  });
</script>
    <?php echo $this->headScript() ?>
<?if(Zend_Auth::getInstance()->hasIdentity()): 
    $id = Zend_Auth::getInstance()->getIdentity()->id;
    $unReadCount = Model_Messages::getCountUnread($id);
    $user = array('name' => Zend_Auth::getInstance()->getIdentity()->name, 
                'sname' => Zend_Auth::getInstance()->getIdentity()->sname, 
                'avatar' => Zend_Auth::getInstance()->getIdentity()->avatar);
?>
    <script type="text/javascript" src="/design/modules/socket.io-client/dist/socket.io.min.js"></script>
    <script type="text/javascript" src="/design/js/messages/scripts.js"></script>
    <script type="text/javascript">
        initMessagesObject(<?echo $id?>, <?echo $unReadCount?>, <?echo json_encode($user)?>);
    </script>
<?endif;?>

<script type="text/javascript" src="/design/js/modals.js"></script>

</head>    
<body>
<?
    $url = $_SERVER['REQUEST_URI'];
    $url = substr($url, 1);

    $cat = "adida265";
    switch($url){
        case "":
            $cat = "adida046";
            break;

        case (preg_match('/^runbase.*/', $url) ? true : false) :
            $cat = "adida708";
            break;

        case "card":
            $cat = "adida708";
            break; 

        case "clothes":
            $cat = "adida146";
            break;

        case (preg_match('/^event.*/', $url) ? true : false) :
            $cat = "adida838";
            break;

        case (preg_match('/^route.*/', $url) ? true : false) :
            $cat = "adida474";
            break;

        case (preg_match('/^micoach.*/', $url) ? true : false) :
            $cat = "adida559";
            break;

        case (preg_match('/^social.*/', $url) ? true : false) :
            $cat = "adida619";
            break;

        case (preg_match('/^trainings.*/', $url) ? true : false) :
            $cat = "adida074";
            break; 

        case (preg_match('/^inspirate.*/', $url) ? true : false) :
            $cat = "adida356";
            break;

        case (preg_match('/^users.*/', $url) ? true : false) :
            $cat = "adida346";
            break;

        case (preg_match('/^profile.*/', $url) ? true : false) :
            $cat = "adida346";
            break;

        case (preg_match('/^article*/', $url) ? true : false) :
            list($a, $alias) = explode('/', $url);
            $article = api::getMaterials()->getMaterials($alias);
            $article = Model_Materials::getFirst($article);
            $category = $article['category_id'];

            if ($category == 130){
                $cat = "adida146";  
            } 

            if ($category == 88){
                $cat = "adida265";  
            } 

            if ($category == 96){
                $cat = "adida356";  
            } 

            if ($category == 97){
                $cat = "adida474";  
            } 

            if ($category == 98){
                $cat = "adida346";  
            } 

            break;
    }
?>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Adidas-Running Home page
URL of the webpage where the tag is expected to be placed: http://adidas-running.ru/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 01/30/2014
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://4360243.fls.doubleclick.net/activityi;src=4360243;type=home;cat=<?= $cat ?>;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4360243.fls.doubleclick.net/activityi;src=4360243;type=home;cat=<?= $cat ?>;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->