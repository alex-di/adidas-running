<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Apple iOS and Android stuff (do not remove) -->
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="/design/admin/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/fonts/ptsans/stylesheet.css" media="screen" />

<link rel="stylesheet" type="text/css" href="/design/admin/css/core/form.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/core/login.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/core/button.css" media="screen" />

<link rel="stylesheet" type="text/css" href="/design/admin/css/mws.theme.css" media="screen" />

<!-- JavaScript Plugins -->
<script type="text/javascript" src="/design/admin/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/design/modules/jqueryphp/js/jquery.php.js"></script>
<!-- jQuery-UI Dependent Scripts -->
<script type="text/javascript" src="/design/admin/js/jquery-ui-effecs.min.js"></script>

<!-- Plugin Scripts -->
<script type="text/javascript" src="/design/admin/plugins/placeholder/jquery.placeholder-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/validate/jquery.validate-min.js"></script>
<script type="text/javascript" src="/design/admin/js/admin.scripts.js"></script>
<link rel="stylesheet" type="text/css" href="/design/admin/css/themer.css" media="screen" />

<title>Admin panel - Login</title>

</head>

<body>
    <? echo $this->layout()->content ?>
</body>
</html>
