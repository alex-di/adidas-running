<?= $this->partial('header.tpl') ?> 
        <section class="layout">
              <?= $this->partial('headmenu.tpl') ?> 
              <section class="content">    	
                    <div class="limit">
                        <?= $this->layout()->content ?> 
                    </div>      
                <!-- footer-push -->
                <div class="footer-push"><div class="limit"></div></div>
            </section>
        </section>
<?= $this->partial('footer.tpl')?> 