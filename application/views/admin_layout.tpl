<?php echo $this->partial('admin_header.tpl') ?>
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            <?
                if(isset($this->breadCrumbs) && !empty($this->breadCrumbs)) {
                    echo $this->partial('breadcrumbs.tpl', array('data'=>$this->breadCrumbs->getBreadcrumbs()));
                    $this->breadCrumbs = null;
                }
                    
                echo $this->layout()->content 
            ?>
            </div>
            <!-- Inner Container End -->
<? echo $this->partial('admin_footer.tpl') ?>