<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Apple iOS and Android stuff (do not remove) -->
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="/design/admin/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/fonts/ptsans/stylesheet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/fluid.css" media="screen" />


<link rel="stylesheet" type="text/css" href="/design/admin/css/mws.style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/icons/16x16.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/icons/24x24.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/icons/32x32.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/demo.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/plugins/timepicker/timepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/plugins/prettyphoto/css/prettyPhoto.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/plugins/colorpicker/colorpicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/jui/jquery.ui.css" media="screen" />
<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="/design/admin/css/mws.theme.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/bootstrap.css" media="screen" />
<!-- JavaScript Plugins -->

<script type="text/javascript" src="/design/admin/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/design/modules/jqueryphp/js/jquery.php.js"></script>
<script type="text/javascript" src="/design/admin/js/jquery.mousewheel-min.js"></script>
<script type="text/javascript" src="/design/admin/js/admin.index.js"></script>
<script type="text/javascript" src="/design/admin/js/admin.edit.js"></script>
<!-- jQuery-UI Dependent Scripts -->
<script type="text/javascript" src="/design/admin/js/jquery-ui.js"></script>
<script type="text/javascript" src="/design/admin/plugins/spinner/ui.spinner.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/timepicker/timepicker-min.js"></script>
<script type="text/javascript" src="/design/admin/js/jquery.ui.touch-punch.min.js"></script>

<!-- Plugin Scripts -->
<script type="text/javascript" src="/design/admin/plugins/imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/duallistbox/jquery.dualListBox-1.3.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/jgrowl/jquery.jgrowl-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/fileinput/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="/design/admin/plugins/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/datatables/jquery.dataTables-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/prettyphoto/js/jquery.prettyPhoto-min.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="/design/admin/plugins/flot/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/design/admin/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/flot/jquery.flot.stack.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/colorpicker/colorpicker-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/tipsy/jquery.tipsy-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/sourcerer/Sourcerer-1.2-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/placeholder/jquery.placeholder-min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="/design/admin/plugins/elrte/js/elrte.min.js"></script>
<script type="text/javascript" src="/design/admin/plugins/elfinder/js/elfinder.min.js"></script>

<link rel="stylesheet" type="text/css" href="/design/admin/plugins/elrte/css/elrte.full.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/plugins/elfinder/css/elfinder.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/themer.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/css/fixes.css" media="screen" />
<!-- Core Script -->
<script type="text/javascript" src="/design/admin/js/mws.js"></script>
<script type="text/javascript" src="/design/admin/js/admin.scripts.js"></script>

<title>Admin panel</title>

</head>

<body>

	<!-- Header -->
	<div id="mws-header" class="clearfix">
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
        
        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
        	<div id="mws-logo-wrap">
                <a href="http://sollux.com.ua" target="_blank">
            	   <img src="/design/admin/images/logo.png" alt="mws admin" />
                </a>
			</div>
        </div>
        
        <!-- User Tools (notifications, logout, profile, change password) -->
        <div id="mws-user-tools" class="clearfix">
            <!-- User Information and functions section -->
            <div id="mws-user-info" class="mws-inset" style="box-shadow:none;background-image:none;border-radius:none">    
                
                    <div id="mws-user-functions">
                        <div id="mws-username">
                            <? if(Zend_Auth::getInstance()->hasIdentity()): ?>
                                Привет, <? echo Zend_Auth::getInstance()->getIdentity()->name, " ", Zend_Auth::getInstance()->getIdentity()->sname ?>
                            <? endif; ?>
                        </div>
                        <ul>
                            <? if(Zend_Auth::getInstance()->hasIdentity()): ?>
                                <li><a href="/admin/auth/logout">Выход</a></li>
                            <? else: ?>
                                <li><a href="/admin/auth">Вход</a></li>
                            <? endif; ?>
                        </ul>
                    </div>
            </div>
        </div>
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        
        <!-- Sidebar Wrapper -->
        <div id="mws-sidebar">

            
            <!-- Main Navigation -->
            <div id="mws-navigation">
                <? echo $this->action('sidebar', 'index', 'admin');?>
            </div>            
        </div>
 
                       
