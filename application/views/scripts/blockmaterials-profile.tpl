<?php if(count($this->data)):?>
   <? foreach($this->data as $event): ?>
        <?
            if(!isset($event['banner_preview']))
                $event['banner_preview'] = '';
            if(isset($event['photos']) && !empty($event['photos']))
                $event['banner_preview'] = $event['photos'];
            switch($event['type']) {
                case Model_Events::TYPE_FAVORITEUSERS:
                        echo $this->partial('partials/profile/favorites.tpl', array('data' => array(
                                                                                    'event_is_read' => $event['event_is_read'],
                                                                                    'event_id' => $event['event_id'],
                                                                                    'is_event'=>1,
                                                                                    'author_event_sex'=>$event['author_event_sex'], 
                                                                                    'avatar' => $event['author_event_avatar'],
                                                                                    'id' => $event['author_user_id'],
                                                                                    'name' => $event['author_event_name'],
                                                                                    'sname' => $event['author_event_sname'],)));
                    break;
                case Model_Events::TYPE_CLOTHESADD:
                        echo $this->partial('partials/profile/clothes.tpl', array('data' => array(
                                                                                    'event_is_read' => $event['event_is_read'],
                                                                                    'event_id' => $event['event_id'],
                                                                                    'id'=>$event['items_id'], 
                                                                                    'image' => $event['items_image'],
                                                                                    'adidas_link' =>$event['adidas_link'],
                                                                                    'stitle' => $event['items_stitle'],
                                                                                    'is_has_review' => $event['is_has_review'],
                                                                                    'name' => $event['items_name'],
                                                                                    'is_event' => 1,
                                                                                    'status_clothes' => $event['items_status_clothes'],
                                                                                    'author_event_sex'=>$event['author_event_sex'],
                                                                                    'author_event_name'=>$event['author_event_name'],
                                                                                    'author_event_sname'=>$event['author_event_sname'],
                                                                                    'author_event_avatar'=>$event['author_event_avatar'],
                                                                                    'author_user_id'=>$event['author_user_id'],)));
                    break;
                case Model_Events::TYPE_MODERN_MATERIALS:
                case Model_Events::TYPE_COMMENT_MATERIALS:
                case Model_Events::TYPE_LIKE_MATERIALS:
                case Model_Events::TYPE_RUN_MATERIALS:
                    if(Model_Materials::CATEGORY_EVENTS == $event['materials_category']) {
                      
                        echo $this->partial('partials/event.tpl', array('event' => array(
                                                            'event_is_read' => $event['event_is_read'],
                                                            'event_id' => $event['event_id'],
                                                            'id'=>$event['materials_id'], 
                                                            'users_id' => $event['materials_users_id'],
                                                            'stitle' => $event['materials_stitle'],
                                                            'is_event' => 1,
                                                            'counter' => $event['counter'],
                                                            'author_event_sex'=>$event['author_event_sex'],
                                                            'author_event_name'=>$event['author_event_name'],
                                                            'author_event_sname'=>$event['author_event_sname'],
                                                            'author_event_avatar'=>$event['author_event_avatar'],
                                                            'author_user_id'=>$event['author_user_id'],
                                                            'event_type' => $event['type'],
                                                            'is_modern' => $event['materials_is_modern'],
                                                            'date_start_event' => $event['date_start_event'],
                                                            'name' => $event['materials_name'],)));
                    } elseif(Model_Materials::CATEGORY_ROUTERS == $event['materials_category']) {

                        echo $this->partial('partials/route.tpl', array('route' => array(
                                    'event_is_read' => $event['event_is_read'],
                                    'event_id' => $event['event_id'],
                                    'id'=>$event['materials_id'], 
                                    'users_id' => $event['materials_users_id'],
                                    'stitle' => $event['materials_stitle'],
                                    'is_event' => 1,
                                    'counter' => $event['counter'],
                                    'author_event_sex'=>$event['author_event_sex'],
                                    'author_event_name'=>$event['author_event_name'],
                                    'author_event_sname'=>$event['author_event_sname'],
                                    'author_event_avatar'=>$event['author_event_avatar'],
                                    'author_user_id'=>$event['author_user_id'],
                                    'is_profile_material' => 1,
                                    'banner_preview' => $event['banner_preview'],
                                    'event_type' => $event['type'],
                                    'is_modern' => $event['materials_is_modern'],
                                    'mileage' => $event['mileage'],
                                    'name_point_start' => $event['name_point_start'],
                                    'name_point_finish' => $event['name_point_finish'],
                                    'name' => $event['materials_name'],
                                    'routes_type' => $event['routes_type'],
                                    'comments'=>$event['materials_comments'],
                                    'rate'=>$event['materials_rate'])));
                    } else {
                        echo $this->partial('partials/article.tpl', array('article' => array(
                                                                                    'event_is_read' => $event['event_is_read'],
                                                                                    'event_id' => $event['event_id'],
                                                                                    'id'=>$event['materials_id'], 
                                                                                    'users_id' => $event['materials_users_id'],
                                                                                    'stitle' => $event['materials_stitle'],
                                                                                    'category_id' => $event['materials_category'],
                                                                                    'is_event' => 1,
                                                                                    'counter' => $event['counter'],
                                                                                    'author_event_sex'=>$event['author_event_sex'],
                                                                                    'author_event_name'=>$event['author_event_name'],
                                                                                    'author_event_sname'=>$event['author_event_sname'],
                                                                                    'author_event_avatar'=>$event['author_event_avatar'],
                                                                                    'author_user_id'=>$event['author_user_id'],
                                                                                    'event_type' => $event['type'],
                                                                                    'is_modern' => $event['materials_is_modern'],
                                                                                    'is_profile_material' => 1,
                                                                                    'banner_preview' => $event['banner_preview'],
                                                                                    'name' => $event['materials_name'],
                                                                                    'short_text'=>$event['short_text'],
                                                                                    'date'=>$event['materials_date'],
                                                                                    'comments'=>$event['materials_comments'],
                                                                                    'rate'=>$event['materials_rate'],)));
                    }
                    
            }
        ?>
   
   <? endforeach;?>
    <div style="clear: both;"></div> 
<?php endif;?>

<script type="text/javascript">
    $(function(){
	   defaultPaginator.setCountPage(<?echo $this->pageCount?>); 
    });
</script>

<?if(isset($this->noFindData) && (!isset($this->pageCount) || $this->pageCount == 0)):?>
    <?echo $this->noFindData;?>
    <script type="text/javascript">
        $(function(){
            $('#container').isotope('destroy');
        })
    </script>
<?endif;?>