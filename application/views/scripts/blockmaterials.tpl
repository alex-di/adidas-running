<?php if(count($this->data)):?>
   <? foreach($this->data as $material): ?>
        <?
            if(isset($this->needTemplate) && !is_null($this->needTemplate)) {
                echo $this->partial($this->needTemplate, array('data' => $material));
            } else {
                switch($material['category_id']) {
                    case Model_Materials::CATEGORY_COACH_ARTICLES :
                    case Model_Materials::CATEGORY_INSPIRING_ARTICLES :
                    case Model_Materials::CATEGORY_ROUTE_ARTICLES :
                    case Model_Materials::CATEGORY_USER_ARTICLES :
                    case Model_Materials::CATEGORY_SHOP_REVIEW:
                    case Model_Materials::CATEGORY_NOT_SELECTED:
                            echo $this->partial('partials/article.tpl', array('article' => $material));
                        break;
                    case Model_Materials::CATEGORY_ROUTERS :
                            echo $this->partial('partials/route.tpl', array('route' => $material));
                        break;
                    case Model_Materials::CATEGORY_EVENTS :
                            echo $this->partial('partials/event.tpl', array('event' => $material));
                        break;
                    case Model_Materials::CATEGORY_QUETES :
                            echo $this->partial('partials/quote.tpl', array('quote' => $material));
                        break;
                    case Model_Materials::CATEGORY_TWITTER_POST :
                    case Model_Materials::CATEGORY_INSTAGRAM_POST :
                            echo $this->partial('partials/social.tpl', array('post' => $material));
                        break;
                }
            }

        ?>
   
   <? endforeach;?>
    <div style="clear: both;"></div> 
<?php endif;?>
<?if(isset($this->pageCount)):?>
    <? $jsVariable = empty($this->jsPaginator) ? "defaultPaginator" : $this->jsPaginator?>
    <script type="text/javascript">
        $(function(){
            if(typeof <?echo $jsVariable?> != 'undefined' && <?echo $jsVariable?> instanceof Paginator)
        	   <?echo $jsVariable?>.setCountPage(<?echo $this->pageCount?>); 
        });
    </script>
<?endif;?>
<?if(isset($this->noFindData) && !is_null($this->noFindData) && (!isset($this->pageCount) || $this->pageCount == 0)):?>
    <?echo $this->noFindData;?>
    <script type="text/javascript">
        $(function(){
            $('#container').isotope('destroy');
        })
    </script>
<?endif;?>