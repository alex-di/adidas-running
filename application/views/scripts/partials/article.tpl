<?$href = Zend_Registry::isRegistered('drafts_article') ? $this->url(array('id'=>$this->article['users_id'], 'article_id'=>$this->article['id']), 'profile_articles_edit') : $this->url(array('stitle'=>$this->article['stitle']), 'article_view');
    if(isset($this->article['is_unique']) && $this->article['is_unique'])
        $href = $this->article['article_url'];
?>
<div class="element article
    <? switch($this->article['category_id']) {
        case Model_Materials::CATEGORY_COACH_ARTICLES:
        case Model_Materials::CATEGORY_SHOP_REVIEW:
            echo "art_master"; break;
        case Model_Materials::CATEGORY_INSPIRING_ARTICLES:
            echo "art_inspirate"; break;
        case Model_Materials::CATEGORY_ROUTE_ARTICLES:
            echo "art_route"; break;
        case Model_Materials::CATEGORY_USER_ARTICLES:
        case Model_Materials::CATEGORY_NOT_SELECTED:
            echo "art_user"; break;
        
    }
    if(isset($this->article['is_event']) && !empty($this->article['is_event']) && Model_Events::TYPE_MODERN_MATERIALS == $this->article['event_type']) {
        echo " check ";
    }
    $isEventAva = false;
    if(isset($this->article['is_event']) && !empty($this->article['is_event']))
        $isEventAva = 1 == $this->article['counter'] && (Model_Events::TYPE_LIKE_MATERIALS == $this->article['event_type'] || Model_Events::TYPE_COMMENT_MATERIALS == $this->article['event_type']);
    ?>
" <?if(!$isEventAva):?>onclick="window.location='<?php echo $href?>'"<?endif;?>>
    <?if(isset($this->article['is_event']) && !empty($this->article['is_event'])):?>
        <div class="profile_info <?if($isEventAva):?>ava<?endif;?>" <?if($isEventAva):?> onclick="window.location = '<?echo $this->url(array('id'=>$this->article['author_user_id']), 'profile_view')?>'"<?endif;?>>
            <?if(Model_Events::TYPE_LIKE_MATERIALS == $this->article['event_type']):?>
                <span class="ico hand"></span>
                <?if(1 == $this->article['counter']):?>
                    <?if(empty($this->article['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->article['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->article['author_event_name'].' '.$this->article['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->article['author_event_sex'], array('оценил', 'оценила'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->article['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->article['counter'], array("человек оценил", "человек оценили", "человек оценили", "человека оценили", "человек оценили"))?> статью</div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_COMMENT_MATERIALS == $this->article['event_type']):?>
                <span class="ico comment"></span>
                <?if(1 == $this->article['counter']):?>
                    <?if(empty($this->article['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->article['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->article['author_event_name'].' '.$this->article['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->article['author_event_sex'], array('прокомментировал', 'прокомментировала'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->article['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->article['counter'], array("новый коментарий", "новых коментариев", "новых коментариев", "новых коментариев", "новых коментариев"))?></div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_MODERN_MATERIALS == $this->article['event_type']):?>
                <span class="ico check"></span>
                <div class="text">Одобрено модератором</div>
            <?elseif(Model_Materials::TYPE_CANCELED == $this->article['is_modern']):?>
                <span class="ico cross"></span>
                <div class="text">Отклонено модератором</div>
            <?elseif(Model_Materials::TYPE_NOT_MODERN == $this->article['is_modern']):?>
                <span class="ico black_check"></span>
                <div class="text">Отправлено на модерацию</div>
            <?endif?>
        </div>
        <?if(Model_Users::isCurrentUserProfile() && !$this->article['event_is_read']):?>
            <script type="text/javascript">EventsList.add(<?echo $this->article['event_id']?>)</script>
        <?endif;?> 
    <?endif;?>
    <a href="<?php echo $href?>">
   	    <?php
            if(isset($this->article['is_unique']) && $this->article['is_unique']) {
                $image = Model_Files::getPreview($this->article['title_image'], 260);
            } elseif(!isset($this->article['is_profile_material'])) {
                $keys = array_keys($this->article['photos']);
                $image = Model_Files::getPreview($this->article['photos'][$keys[0]]['path'], 260);
            } else {
                $image = $this->article['banner_preview'];
            }

        ?>
        <?if(empty($image)):?>
             <div class="article_empty_image"></div>
        <?else:?>
             <img onload="onLoadImage(this)" src="<?= $image ?>" alt="img" class="main"/>
        <?endif;?>
       
    </a>
    <div class="lines"></div>
    <div class="animation_text">
         <a href="<?php echo $href?>" class="title"><?= $this->article['name']?></a>
         <? //$lenght = round(mb_strlen($this->article['name'], 'UTF-8') * 3 / 2); ?>
         
         <div class="short_text">
            <?if(!Zend_Registry::isRegistered('drafts_article')):?>
                <?= isset($this->article['short_text']) ? $this->article['short_text'] : '';//DR_Api_Tools_Tools::getShortString($this->article['short_text'], 60)?>
            <?endif;?>
         </div>
    </div>
    <div class="sub_info">
        <div class="text"><?= DR_Api_Tools_Tools::formatDate($this->article['date']) ?></div>
        <div class="comments"><span></span><?php echo $this->article['comments']?></div>
        <div class="likes"><span></span><?php echo $this->article['rate']?></div>      
    </div>
</div>