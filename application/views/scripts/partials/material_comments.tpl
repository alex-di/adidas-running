    <div class="comment_point">
        <div class="img_box" onclick="window.location='<?echo $this->url(array('id'=>$this->data['users_id']), 'profile_view')?>'" style="cursor: pointer;">
        	<?php if(empty($this->data['avatar'])):?>
        		<img src="/design/images/ava_default.png" style="width:130px;" alt="img" />
        	<?php else:?>
        		<img src="<?php echo Model_Files::getPreview($this->data['avatar'], 260);?>" alt="img" />
        	<?php endif;?>
        </div>
        <div class="name" onclick="window.location='<?echo $this->url(array('id'=>$this->data['users_id']), 'profile_view')?>'" style="cursor: pointer;"><?php echo $this->data['username']?></div>
        <div class="date"><?php echo DR_Api_Tools_Tools::getCommentDate($this->data['date'])?></div>
        <div style="clear: both;"></div>
        <div class="comment_text">
        	<?php echo $this->data['text']?>
        </div>
    </div>