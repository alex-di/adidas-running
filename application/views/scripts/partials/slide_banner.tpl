<?
    $matches = array('/\[yellow\](.*)\[\/yellow\]/', '/\[black\](.*)\[\/black\]/');
    $replaces = array('<span class="text_box yellow">$1</span>', '<span class="text_box black">$1</span>');
?>
<li class="<?= !empty($this->banner['banner_color']) ? $this->banner['banner_color'] : 'black'; ?>">
    
    <a href="<?= Model_Files::getPreview($this->banner['banner_image'], 560) ?>"></a>
    <?if(!empty($this->banner['banner_link'])):?>
        <a href="<?= $this->banner['banner_link'] ?>"></a>
    <? endif ?>
    
    <div  class="promo_lines"></div> 
   
    <div  class="bg_box" style="">
        <div><span class="text_box"><?= preg_replace($matches, $replaces, $this->banner['banner_name'])?></span></div>
    </div>
    <div class="lines_block"></div>                                                             
</li>