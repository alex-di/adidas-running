<div class="element people" style="background-image: url(<?= '/design/images/bg/bg'.rand(1,5).'.png';/*$this->user->bg*/ ?>);">
    <? $href = empty($this->user['article_stitle']) ? 'javascript:void(0)" style="cursor:default' : $this->url(array('stitle'=>$this->user['article_stitle']), 'article_view')?>
    <a class="photo" <??> href="<?= $href ?>">
        <img src="<?= Model_Files::getPreview($this->user['avatar'], 260); ?>" alt="img" class="main" />
    </a>
    <div class="lines"></div>
    <a href="<?= $href ?>"  class="people_name"><?= $this->user['name'], ' ', $this->user['sname']?></a>
    <a href="<?= $href ?>" class="title"><?= $this->user['article_name'] ?></a>
    <div class="sub_info">
        <div class="text"><?= !empty($this->user['article_date']) ? DR_Api_Tools_Tools::formatDate($this->user['article_date']) : '---' ?></div>
        <div class="record_count">Всего записей: <?= $this->user['article_count'] ?></div>   
    </div>
</div>