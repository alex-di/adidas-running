<?$href = Zend_Registry::isRegistered('drafts_route') ? $this->url(array('id'=>$this->route['users_id'], 'route_id'=>$this->route['id']), 'profile_routes_edit') : $this->url(array('stitle'=>$this->route['stitle']), 'route_view');
$isEventAva = false;
if(isset($this->route['is_event']) && !empty($this->route['is_event']))
    $isEventAva = 1 == $this->route['counter'] && (Model_Events::TYPE_LIKE_MATERIALS == $this->route['event_type'] || Model_Events::TYPE_COMMENT_MATERIALS == $this->route['event_type']);

$profileUsersId = Zend_Registry::isRegistered('profile_users_id') ? Zend_Registry::get('profile_users_id') : 0;
?>
<div id="<?echo $this->route['id']?>" class="element route" <?if(!$isEventAva):?>onclick="window.location='<?php echo $href?>'"<?endif;?>>
    <?if(isset($this->route['is_event']) && !empty($this->route['is_event'])):?>
        <div style="z-index: 5;top: -12px;" class="profile_info <?if($isEventAva):?>ava<?endif;?>" <?if($isEventAva):?> onclick="window.location = '<?echo $this->url(array('id'=>$this->route['author_user_id']), 'profile_view')?>'"<?endif;?>>
            <?if(Model_Events::TYPE_RUN_MATERIALS == $this->route['event_type']):?>
                <span class="ico check"></span>
                <div class="text">
                    <?if($profileUsersId == $this->route['author_user_id']):?>
                        <?echo DR_Api_Tools_Tools::getTextForSex($this->route['author_event_sex'], array('Пробежал', 'Пробежала'))?>
                    <?else:?>
                        <?echo $this->route['author_event_name'].' '.$this->route['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->route['author_event_sex'], array('пробежал', 'пробежала'))?>
                    <?endif;?>
                </div>
            <?elseif(Model_Events::TYPE_LIKE_MATERIALS == $this->route['event_type']):?>
                <span class="ico hand"></span>
                <?if(1 == $this->route['counter']):?>
                    <?if(empty($this->route['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->route['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->route['author_event_name'].' '.$this->route['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->route['author_event_sex'], array('оценил', 'оценила'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->route['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->route['counter'], array("человек оценил", "человек оценили", "человек оценили", "человека оценили", "человек оценили"))?> статью</div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_COMMENT_MATERIALS == $this->route['event_type']):?>
                <span class="ico comment"></span>
                <?if(1 == $this->route['counter']):?>
                    <?if(empty($this->route['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->route['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->route['author_event_name'].' '.$this->route['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->route['author_event_sex'], array('прокомментировал', 'прокомментировала'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->route['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->route['counter'], array("новый коментарий", "новых коментариев", "новых коментариев", "новых коментариев", "новых коментариев"))?></div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_MODERN_MATERIALS == $this->route['event_type']):?>
                <span class="ico check"></span>
                <div class="text">Одобрено модератором</div>
            <?elseif(Model_Materials::TYPE_CANCELED == $this->route['is_modern']):?>
                <span class="ico cross"></span>
                <div class="text">Отклонено модератором</div>
            <?elseif(Model_Materials::TYPE_NOT_MODERN == $this->route['is_modern']):?>
                <span class="ico black_check"></span>
                <div class="text">Отправлено на модерацию</div>
            <?endif?>
        </div>
        <?if(Model_Users::isCurrentUserProfile() && !$this->route['event_is_read']):?>
            <script type="text/javascript">EventsList.add(<?echo $this->route['event_id']?>)</script>
        <?endif;?>  
    <?endif;?>
     <div class="route_map">
     	<?php
            
            if(!isset($this->route['is_profile_material'])) {
                $path = $this->route['photos'];
                if(is_array($this->route['photos'])) {
                    $keys = array_keys($this->route['photos']);
                    $path = $this->route['photos'][$keys[0]]['path'];
                }

                $image = Model_Files::getPreview($path, 260);
            } else {
                
                $image = $this->route['banner_preview'];
            }

        ?>
     	<img alt="img" onload="onLoadImage(this)" src="<?php echo $image?>"/>
     </div>
     <div  class="route_length"><?= number_format($this->route['mileage'], 2, '.', '') ?></div>
     <div class="lines"></div>
     <a href="<?php echo $href?>" class="title"><?= DR_Api_Tools_Tools::getShortString($this->route['name'], 20)?></a>
     <div class="from_to">
        <?if(!empty($this->route['name_point_start'])):?>
            <?php echo DR_Api_Tools_Tools::getShortString($this->route['name_point_start'], 24)?> ><br /><?php echo DR_Api_Tools_Tools::getShortString($this->route['name_point_finish'], 25)?>
        <?else:?>
            <br /><br />
        <?endif;?>
     </div>
     <div class="town">
     	<?php
            if(is_array($this->route['routes_type'])) {
          		$keys = array_keys($this->route['routes_type']);
	            echo $this->route['routes_type'][$keys[0]];
            } else {
                echo $this->route['routes_type'];
            }

     	?>
     </div>
     <div class="sub_info">
        <!-- <div class="text">297 пробежали</div> -->
   		<div class="comments"><span></span><?php echo $this->route['comments']?></div>
    	<div class="likes"><span></span><?php echo $this->route['rate']?></div>      
     </div>
</div>
