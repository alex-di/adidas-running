<?$href = Zend_Registry::isRegistered('drafts_article') ? $this->url(array('id'=>$this->event['users_id'], 'article_id'=>$this->event['id']), 'profile_articles_edit') : $this->url(array('stitle'=>$this->event['stitle']), 'event_view');
$isEventAva = false;
$isNextEvent = Zend_Registry::isRegistered(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT) && isset($this->event['id']) && $this->event['id'] == Zend_Registry::get(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT);
if(isset($this->event['is_event']) && !empty($this->event['is_event']))
    $isEventAva = 1 == $this->event['counter'] && (Model_Events::TYPE_LIKE_MATERIALS == $this->event['event_type'] || Model_Events::TYPE_COMMENT_MATERIALS == $this->event['event_type']);
?>
<div class="element event <?if($isNextEvent):?>current<?endif;?>" <?if(!$isEventAva):?>onclick="window.location='<?php echo $href?>'"<?endif;?>>
    <?if(isset($this->event['is_event']) && !empty($this->event['is_event'])):?>
        <div class="profile_info <?if($isEventAva):?>ava<?endif;?>" <?if($isEventAva):?> onclick="window.location = '<?echo $this->url(array('id'=>$this->event['author_user_id']), 'profile_view')?>'"<?endif;?>>
            <?if(Model_Events::TYPE_LIKE_MATERIALS == $this->event['event_type']):?>
                <span class="ico hand"></span>
                <?if(1 == $this->event['counter']):?>
                    <?if(empty($this->event['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->event['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->event['author_event_name'].' '.$this->event['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->event['author_event_sex'], array('оценил', 'оценила'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->event['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->event['counter'], array("человек оценил", "человек оценили", "человек оценили", "человека оценили", "человек оценили"))?> статью</div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_COMMENT_MATERIALS == $this->event['event_type']):?>
                <span class="ico comment"></span>
                <?if(1 == $this->event['counter']):?>
                    <?if(empty($this->event['author_event_avatar'])):?>
                        <img alt="img" src="/design/images/avatar_default.png"/>
                    <?else:?>
                        <img alt="img" src="<?echo Model_Files::getPreview($this->event['author_event_avatar'], 100)?>"/>
                    <?endif;?>
                    <div class="text"><?echo $this->event['author_event_name'].' '.$this->event['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->event['author_event_sex'], array('прокомментировал', 'прокомментировала'))?> статью</div>
                <?else:?>
                     <div class="text"><?echo $this->event['counter'].' '.DR_Api_Tools_Tools::getWordsByNumber($this->event['counter'], array("новый коментарий", "новых коментариев", "новых коментариев", "новых коментариев", "новых коментариев"))?></div>
                <?endif;?>
            <?elseif(Model_Events::TYPE_MODERN_MATERIALS == $this->event['event_type']):?>
                <span class="ico check"></span>
                <div class="text">Одобрено модератором</div>
            <?elseif(Model_Materials::TYPE_CANCELED == $this->event['is_modern']):?>
                <span class="ico cross"></span>
                <div class="text">Отклонено модератором</div>
            <?elseif(Model_Materials::TYPE_NOT_MODERN == $this->event['is_modern']):?>
                <span class="ico black_check"></span>
                <div class="text">Отправлено на модерацию</div>
            <?endif?>
        </div>
        <?if(Model_Users::isCurrentUserProfile() && !$this->event['event_is_read']):?>
            <script type="text/javascript">EventsList.add(<?echo $this->event['event_id']?>)</script>
        <?endif;?> 
    <?endif;?>
    <div class="event_container">  
         <div class="day"><?= intval(date('d', strtotime($this->event['date_start_event']))); ?></div>
         <div class="lines"></div>
         <div class="month"><?= DR_Api_Tools_Tools::rusMonth($this->event['date_start_event']); ?></div>
         <a href="<?php echo $href?>" class="title"><?= $this->event['name'] ?></a>
    </div>
</div> 