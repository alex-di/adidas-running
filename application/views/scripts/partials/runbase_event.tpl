
<div class="runbase-event">
    <div class="date-time">
        <div class="number"><?= date('d', strtotime($this->event['date_start_event'])); ?></div>
        <div class="month"><?= DR_Api_Tools_Tools::rusMonth($this->event['date_start_event']); ?></div>
        <div class="dow"><?= DR_Api_Tools_Tools::rusDayOfWeek($this->event['date_start_event']); ?></div>
        <div class="time"><?= date('H:i', strtotime($this->event['date_start_event']))?></div>
    </div>

    <div class="desc">
        <div class="title">
            <? if(isset($this->event['event_link']) and !empty($this->event['event_link'])): ?>
                <a style="color:#8dc63f" target="_blank" href="/event/<?= $this->event['event_link'] ?>"><?= $this->event['name'] ?></a>
            <? elseif(isset($this->event['anchor_article_link']) && !empty($this->event['anchor_article_link'])): ?>
                <a style="color:#8dc63f" target="_blank" href="<?= $this->event['anchor_article_link'] ?>"><?= $this->event['name'] ?></a>
            <?else:?>
                <?= $this->event['name'] ?>
            <? endif ?>
        </div>

        <div class="name">
            <?= $this->event['author']?>
        </div>

        <a class="reserve" href="javascript:void(0);" onclick="return $.php('<?= $this->action ?>', 
        {runbase_event_id:<?= $this->event['id']?>,
        date_event:'<?= $this->event['date_start_event']?>'});">
            забронировать шкафчик
        </a>
    </div>
</div>