	<?php
        if (Zend_Auth::getInstance()->getIdentity() !== null && isset(Zend_Auth::getInstance()->getIdentity()->id)) {
            $isCurrentUserProfile = $this->user->id == Zend_Auth::getInstance()->getIdentity()->id;
        } else {
            $isCurrentUserProfile = false;
        }
    ?>
    <ul class="tabs">
		<li id="profile" class="active" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_view')?>'">профайл <?if($isCurrentUserProfile && $this->user->count_events_profile):?>(<?echo $this->user->count_events_profile?>)<?endif;?></li>
		<? if($isCurrentUserProfile):?>
            <li id="messages" class="active" onclick="window.location = '/messages'">сообщения</li>
    		<li id="favorites" class="active" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_favorites')?>'">избранные <?if($this->user->count_events_favorites):?>(<?echo $this->user->count_events_favorites?>)<?endif;?></li>
		<? endif; ?>
        <li id="routes" class="active" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_run')?>'">маршруты</li>
        <li id="article" class="active" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_articles_modern_view')?>'"><?if($isCurrentUserProfile):?>мои статьи<?else:?>статьи<?endif;?></li>
        <li id="clothes" class="active" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_clothes')?>'">экипировка</li>
	</ul>
    <script type="text/javascript">
        $(function(){
           setInterval(function(){
                $('.section.vertical .tabs').css('margin-top', $('.person_box.my_profile').height() + 15);
           }, 100);
        });
    </script>