<?if(count($this->data)):?>
    <?foreach($this->data as $dragElement):?>
        <?foreach($dragElement as $type=>$data):?>
            <?foreach($data as $namePostfix=>$values):?>
                <?if('text' == $type):?>
                    <div class="grey_box with_arrow drop_element" data-type="text" data-name-postfix="<?echo $namePostfix?>">
                        <div class="grey_box_title">текст</div>
                        <div class="outer_button">
                            <div class="round_button first" onclick="move(this, 'first', 'drag_box_element')"></div>
                            <div class="round_button prev" onclick="move(this, 'up', 'drag_box_element')"></div>
                            <div class="label">переместить</div>
                            <div class="round_button next" onclick="move(this, 'down', 'drag_box_element')"></div>
                            <div class="round_button last" onclick="move(this, 'last', 'drag_box_element')"></div>
                            <div class="round_button delete" onclick="delteDragElement(this)"></div>
                            <div class="label red" onclick="delteDragElement(this)">удалить текст</div>
                        </div>
                        <div class="text_area">
                            <div id="toolbar_for_editor<?echo $namePostfix?>" class="top_panel">
                                <div class="ico bold editor_button" data-edit="bold">Полужирный</div>
                                <div class="ico italic editor_button" data-edit="italic">Курсив</div>
                                <div class="ico num_list editor_button" data-edit="insertorderedlist"><span></span></div>
                                <div class="ico mark_list editor_button" data-edit="insertunorderedlist"><span></span></div>
                                <div class="ico letter editor_button" data-edit="formatblock h2"><span></span></div>
                            </div>
                            <div id="editor<?echo $namePostfix?>" class="editor_area">
                                <?echo $values['editor'];?>
                            </div>
                        </div>
                        <div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить текст</div>
                    </div>
                    <script type="text/javascript">
                        $(function(){
                           $('#editor<?echo $namePostfix?>').wysiwyg({toolbarSelector: '#toolbar_for_editor<?echo $namePostfix?>', onChangeContent: changeEditorContent}); 
                        });
                    </script>
                <?elseif('photo' == $type):?>
                    <div class="grey_box with_arrow drop_element" data-type="photo" data-name-postfix="<?echo $namePostfix?>">
                        <div class="grey_box_title">фото</div>
                        <div class="outer_button">
                            <div class="round_button first" onclick="move(this, 'first', 'drag_box_element')"></div>
                            <div class="round_button prev" onclick="move(this, 'up', 'drag_box_element')"></div>
                            <div class="label">переместить</div>
                            <div class="round_button next" onclick="move(this, 'down', 'drag_box_element')"></div>
                            <div class="round_button last" onclick="move(this, 'last', 'drag_box_element')"></div>
                            <div class="round_button delete" onclick="delteDragElement(this)"></div>
                            <div class="label red" onclick="delteDragElement(this)">удалить галерею</div>
                        </div>
                        <div class="drag_box drag_box_photo">
                            <?foreach($values['list'] as $listData):?>
                                <?foreach($listData as $postfix=>$listvalue):?>
                                    <div class="drop_element" data-name-postfix="<?echo $postfix?>">
                                        <div class="gallery_img_box">
                                            <img src="<?echo $listvalue['photo']?>" alt="img" />
                                            <div class="button_box">
                                                <div class="round_button delete" onclick="deletePhotoElement(this)"></div>
                                                <div class="round_button prev" onclick="move(this, 'up', 'drag_box_photo')"></div>
                                                <div class="round_button next" onclick="move(this, 'down', 'drag_box_photo')"></div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="photo<?echo $postfix?>" value="<?echo $listvalue['photo']?>"/>
                                        <input type="text" name="description<?echo $postfix?>" class="yellow_input delimiter" placeholder="описание фотографии" value="<?echo $listvalue['description']?>"/>
                                    </div>
                                <?endforeach;?>
                            <?endforeach;?>
                        </div>
                        <div class="load_process disabled">
                            <div class="label">Загрузка фото 67%</div>
                            <div class="container">
                                <div class="progress" style="width: 67%;"></div>
                            </div>
                        </div>
                        <div id="gallery<?echo $namePostfix?>" class="add_button img uploader"><span></span><?if(count($values['list'])):?>добавить еще фото в галерею<?else:?>добавить фото в галерею<?endif;?></div>
                        <div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить галерею</div>
                    </div>
                    <script type="text/javascript">
                        $(function(){
                               var settings = Materials.gallery_upload_settings;
                               settings.browse_button = 'gallery<?echo $namePostfix?>';
                               initUploader(settings); 
                        });
                    </script>
                <?elseif('video' == $type):?>
                    <div class="grey_box with_arrow drop_element" data-type="video" data-name-postfix="<?echo $namePostfix?>">
                        <div class="grey_box_title">видео</div>
                        <div class="outer_button">
                            <div class="round_button first" onclick="move(this, 'first', 'drag_box_element')"></div>
                            <div class="round_button prev" onclick="move(this, 'up', 'drag_box_element')"></div>
                            <div class="label">переместить</div>
                            <div class="round_button next" onclick="move(this, 'down', 'drag_box_element')"></div>
                            <div class="round_button last" onclick="move(this, 'last', 'drag_box_element')"></div>
                            <div class="round_button delete" onclick="delteDragElement(this)"></div>
                            <div class="label red" onclick="delteDragElement(this)">удалить видео</div>
                        </div>
                        <input name="name<?echo $namePostfix?>" type="text" class="yellow_input" placeholder="название видео" value="<?echo $values['name']?>"/>
                        <input onchange="validateVideo(this)" name="link<?echo $namePostfix?>" type="text" class="yellow_input delimiter" placeholder="ссылка на видео" value="<?echo htmlspecialchars($values['link'])?>"/>
                        <textarea onkeypress="changeTextareaContent(this)" name="description<?echo $namePostfix?>" placeholder="описание" class="yellow_textarea"><?echo $values['description']?></textarea>
                        <div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить видео</div>
                    </div>
                    <script type="text/javascript">
                        $(function(){
                           changeTextareaContent($('[name="description<?echo $namePostfix?>"]')); 
                        });
                    </script>
                <?elseif('audio' == $type):?>
                    <div class="grey_box with_arrow drop_element" data-type="audio" data-name-postfix="<?echo $namePostfix?>">
                        <div class="grey_box_title">аудио</div>
                        <div class="outer_button">
                            <div class="round_button first" onclick="move(this, 'first', 'drag_box_element')"></div>
                            <div class="round_button prev" onclick="move(this, 'up', 'drag_box_element')"></div>
                            <div class="label">переместить</div>
                            <div class="round_button next" onclick="move(this, 'down', 'drag_box_element')"></div>
                            <div class="round_button last" onclick="move(this, 'last', 'drag_box_element')"></div>
                            <div class="round_button delete" onclick="delteDragElement(this)"></div>
                            <div class="label red" onclick="delteDragElement(this)">удалить аудио</div>
                        </div>
                        <input type="text" class="yellow_input <?if(!count($values['list'])):?>disabled<?endif;?> playlist_name" placeholder="название плэйлиста" name="playlist<?echo $namePostfix?>" value="<?echo $values['playlist']?>"/>
                        <div class="drag_box">
                            <?foreach($values['list'] as $listData):?>
                                <?foreach($listData as $postfix=>$listvalue):?>
                                    <div class="audio_point drop_element" data-name-postfix="<?echo $postfix?>">
                                        <?if(isset($listvalue['audioname'])):?>
                                            <input name="audioname<?echo $postfix?>" type="text" class="yellow_input" placeholder="название аудио" value="<?echo $listvalue['audioname']?>"/><br/><br/>
                                        <?endif;?>
                                        <?echo $listvalue['audio']?>
                                        <br/><br/><br/>
                                        <div class="delete_button" onclick="deleteAudioElement(this)"><span></span>удалить</div>
                                        <input type="hidden" name="audio<?echo $postfix?>" value="<?echo htmlspecialchars($listvalue['audio'])?>"/>
                                    </div>
                                <?endforeach?>
                            <?endforeach?>
                        </div>
                        <div class="add_link">
                            <input type="text" class="yellow_input" placeholder="ссылка на аудио" />
                            <div class="add_button" onclick="addAudioElement(this)"><span></span>добавить</div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                <?endif;?>
            <?endforeach?>
        <?endforeach?>
    <?endforeach?>
<?endif;?>