<?$isEvent = isset($this->data['is_event']) && $this->data['is_event'];
 
    $isLike = isset($this->data['status_clothes']) && 0 == $this->data['status_clothes'];
    $isHave = isset($this->data['status_clothes']) && 1 == $this->data['status_clothes'];
    $isLike = $isHave ? true : $isLike;
?>
<div id="clothes_<?echo $this->data['id']?>" class="element goods">
    <?if($isEvent):?>
        <div class="profile_info ava" onclick="window.location = '<?echo $this->url(array('id'=>$this->data['author_user_id']), 'profile_view')?>'">
            <span class="ico plus"></span>
            <?if(empty($this->data['author_event_avatar'])):?>
                <img alt="img" src="/design/images/avatar_default.png"/>
            <?else:?>
                <img alt="img" src="<?echo Model_Files::getPreview($this->data['author_event_avatar'], 100)?>"/>
            <?endif;?>
            <div class="text"><?echo $this->data['author_event_name'].' '.$this->data['author_event_sname']?> <?echo DR_Api_Tools_Tools::getTextForSex($this->data['author_event_sex'], array('добавил', 'добавила'))?> </div>
        </div>
        <?if(Model_Users::isCurrentUserProfile() && !$this->data['event_is_read']):?>
            <script type="text/javascript">EventsList.add(<?echo $this->data['event_id']?>)</script>
        <?endif;?>
    <?endif;?>
    <div class="img_box">
      <img onload="onLoadImage(this)" src="<?echo $this->data['image']?>" alt="img" />
      <div class="white_shadow">
            <a class="buy" href="<?echo $this->data['adidas_link']?>" target="_blank">купить</a>
            <?if($this->data['is_has_review']):?>
                <a class="view" href="<?echo $this->url(array('stitle'=>$this->data['stitle']), 'items_review')?>">обзор<span></span></a>
            <?endif;?>
            <?if((Zend_Registry::isRegistered('status_clothes_like') && Zend_Registry::get('status_clothes_like')) || !Model_Users::isCurrentUserProfile() || $isEvent):?>
                <?if(!Model_Users::isCurrentUserProfile() || $isEvent):?>
                    <div class="i_have <?if($isLike):?>disabled<?endif;?>" onclick="addClothes(<?echo $this->data['id']?>, 0, this)">я хочу<span></span></div>
                <?endif;?>
                <div class="i_have ui_have_clothes <?if($isHave):?>disabled<?endif;?>" onclick="addClothes(<?echo $this->data['id']?>, 1, this)">у меня есть<span></span></div>
            <?endif;?>
            <?if(Model_Users::isCurrentUserProfile() && !$isEvent):?>
                <div class="delete" onclick="deleteClothes(<?echo $this->data['id']?>)">удалить<span></span></div>
            <?endif;?>
      </div>
    </div>                                  
    <a class="title"><?echo $this->data['name']?></a>
</div>