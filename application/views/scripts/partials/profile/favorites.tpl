
<div class="element person_box">
    <?if(isset($this->data['is_event']) && $this->data['is_event']):?>
        <div class="profile_info">
            <span class="ico star"></span>
            <div class="text"><?echo DR_Api_Tools_Tools::getTextForSex($this->data['author_event_sex'], array('Добавил', 'Добавила'))?> <?if(Model_Users::isCurrentUserProfile()):?>вас<?endif;?> в избранное</div>
        </div>
        <?if(Model_Users::isCurrentUserProfile() && !$this->data['event_is_read']):?>
            <script type="text/javascript">EventsList.add(<?echo $this->data['event_id']?>)</script>
        <?endif;?>
    <?endif;?>
    <div class="photo_box" onclick="window.location='<?echo $this->url(array('id'=>$this->data['id']), 'profile_view')?>'">
        <?if(empty($this->data['avatar'])):?>
            <img onload="onLoadImage(this)" src="/design/images/avatar_default.png" alt="img" class="photo" />
        <?else:?>
            <img onload="onLoadImage(this)" src="<?echo Model_Files::getPreview($this->data['avatar'], 260)?>" alt="img" class="photo" />
        <?endif;?>
        <img src="/design/images/article_img_bg.png" alt="img" class="bottom_shadow" />
    </div>
    <a class="title" href="<?echo $this->url(array('id'=>$this->data['id']), 'profile_view')?>"><?echo $this->data['name'].' '.$this->data['sname']?></a>
</div> 