<div class="person_box element my_profile">
    <div class="photo_box" style="cursor: default;">
        <? if(!empty($this->user->avatar)): ?>
            <img src="<?echo Model_Files::getPreview($this->user->avatar, 260)?>" alt="img" class="photo" />
        <?else:?>
            <img src="/design/images/avatar_default.png" alt="img" class="photo" />
        <? endif ?>
        
        <img src="/design/images/article_img_bg.png" alt="img" class="bottom_shadow" />
    </div>
    <a style="cursor: default;" class="title"><?= $this->escape($this->user->name), " ", $this->escape($this->user->sname) ?></a>
    <div class="about" style="cursor: default;">
        <?if('0000-00-00 00:00:00' != $this->user->bday && !empty($this->user->bday)) {
            $years = DR_Api_Tools_Tools::yearDifference($this->user->bday, time());
            echo $years .' '.DR_Api_Tools_Tools::getPostfixYears($years).', ';
        }?> <?= $this->escape($this->user->city) ?></div>
    <div class="social">
        <?if(!empty($this->user->vk_id)):?>
            <a target="_blank" href="http://vk.com/id<?echo $this->user->vk_id?>" class="vk"></a>
        <?endif;?>
        <?if(!empty($this->user->fb_id)):?>
            <a target="_blank" href="https://www.facebook.com/<?echo $this->user->fb_id?>" class="fb"></a>
        <?endif;?>
        <?if(!empty($this->user->tw_id)):?>
            <a target="_blank" href="https://twitter.com/account/redirect_by_id?id=<?echo $this->user->tw_id?>" class="tw"></a>
        <?endif;?>
    </div>
    <?php 
        if (Zend_Auth::getInstance()->getIdentity() !== null && isset(Zend_Auth::getInstance()->getIdentity()->id)) {
    ?>
    <div class="option_box">
        <div class="lines top"></div>
        <!-- <div class="lines bottom"></div> -->
        <a href="/users/profile/edit" class="edit">редактировать<span></span></a>
        <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_add')?>" class="add_article">добавить статью<span></span></a>
        <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_add')?>" class="add_route">добавить маршрут<span></span></a>
        <a href="javascript:void(0)" <?//if(in_array(Zend_Auth::getInstance()->getIdentity()->id, array(1,28,15,26, 578))):?>onclick="messageDialog({'users_id': <?echo $this->user->id?>, 'username': '<?= $this->escape($this->user->name), " ", $this->escape($this->user->sname) ?>'})"<?//endif;?> class="message">сообщение<span></span></a>
        <?$isFavorite = $this->user->is_favorite;?>
        <a <?if($isFavorite):?>style="display: none;"<?endif;?> href="javascript:addFavorites(<?echo $this->user->id?>)" class="favorite">в избранные<span></span></a>
        <a <?if(!$isFavorite):?>style="display: none;"<?endif;?> href="javascript:deleteFavorites(<?echo $this->user->id?>)" class="del_favorite">удалить из избранных<span></span></a>
    </div>
    <?php 
        }
    ?>
</div>