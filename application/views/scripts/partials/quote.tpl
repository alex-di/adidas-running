<div class="element <?= mb_strlen($this->quote['quote'], 'utf8') > 70 ? 'long' : ''; ?> quote">
    <div class="lines top"></div>
    <div class="lines bottom"></div>
    <div class="quote_text"><span>«</span><?= $this->quote['quote']; ?>»</div>
    <div class="quote_author"><?= $this->quote['quote_autor']; ?></div>
</div>  