<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<?php echo Zend_Layout::getMvcInstance()->getView()->metadata('adidas Running')?>

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->


<?echo $this->headLink()?>
<script type="text/javascript" src="/design/js/jquery-1-9-0.js"></script>
<script type="text/javascript" src="/design/js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="/design/modules/migrate/src/migrate.js"></script>
<script type="text/javascript" src="/design/modules/migrate/src/core.js"></script>
<script type="text/javascript" src="/design/js/isotope.js"></script>    
<script type="text/javascript" src="/design/js/styled.js"></script>
<script type="text/javascript" src="/design/js/main.js"></script>
<script type="text/javascript" src="/design/js/placeholder.js"></script>
<script type="text/javascript" src="/design/js/dialogmessage.js"></script>

<script type="text/javascript" src="/design/modules/jqueryphp/js/jquery.php.js"></script>
<script type="text/javascript" src="/design/js/sollux.js"></script>

<script type="text/javascript" src="/design/js/social/auth.js"></script>

<?echo $this->headScript() ?>

    <?php echo $this->headScript() ?>
<?if(Zend_Auth::getInstance()->hasIdentity()): 
    $id = Zend_Auth::getInstance()->getIdentity()->id;
    $unReadCount = Model_Messages::getCountUnread($id);
    $user = array('name' => Zend_Auth::getInstance()->getIdentity()->name, 
                'sname' => Zend_Auth::getInstance()->getIdentity()->sname, 
                'avatar' => Zend_Auth::getInstance()->getIdentity()->avatar);
?>
    <script type="text/javascript" src="/design/modules/socket.io-client/dist/socket.io.min.js"></script>
    <script type="text/javascript" src="/design/js/messages/scripts.js"></script>
    <script type="text/javascript">
        initMessagesObject(<?echo $id?>, <?echo $unReadCount?>, <?echo json_encode($user)?>);
    </script>
<?endif;?>

<script type="text/javascript" src="/design/js/modals.js"></script>

</head>    
<body>	