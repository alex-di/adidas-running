<?= $this->partial('header.tpl')?>

	<?= $this->partial('headmenu.tpl')?> 

    <section class="content">
        <?= $this->layout()->content ?>
    </section>

<div class="limit">
	<?= $this->partial('footer.tpl')?> 
</div>