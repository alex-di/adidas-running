<?php
    function utf8_wordwrap($str, $width, $break, $cut = false) {
        if (!$cut) {
            $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.',}\b#U';
        } else {
            $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.'}#';
        }
        if (function_exists('mb_strlen')) {
            $str_len = mb_strlen($str,'UTF-8');
        } else {
            $str_len = preg_match_all('/[\x00-\x7F\xC0-\xFD]/', $str, $var_empty);
        }
        $while_what = ceil($str_len / $width);
        $i = 1;
        $return = '';
        while ($i < $while_what) {
            preg_match($regexp, $str,$matches);
            $string = $matches[0];
            $return .= $string.$break;
            $str = substr($str, strlen($string));
            $i++;
        }
        return $return.$str;
    }
?>

<script type="text/javascript">
    var reLayoutIsotope = {};
    var onLoadImage = function(img) {
        var id = $(img).parents('.isotope').attr('id');
        reLayoutIsotope[id] = 1;
    }
    $(function(){
       setInterval(function(){
            for(var id in reLayoutIsotope) {
                $('#'+id).isotope('reLayout');
            }
            reLayoutIsotope = {};
       }, 100);
    });
</script>

<?php 
	//_d($this->data);
    $isDraftArticle = in_array($this->data['is_modern'], array(Model_Materials::TYPE_DRAFTS, Model_Materials::TYPE_CANCELED));
	$isRouteArticle = Model_Materials::CATEGORY_ROUTERS == $this->data['category_id'];
	$isEventArticle = Model_Materials::CATEGORY_EVENTS == $this->data['category_id'];
    $sidebarContentClass = 'art_user';

    if($isEventArticle)
        $sidebarContentClass = 'event';
    if($this->isBlogArticle)
        $sidebarContentClass = 'blog';

    if(Model_Materials::CATEGORY_COACH_ARTICLES == $this->data['category_id'])
        $sidebarContentClass = 'art_inspirate';
	if($isRouteArticle):
?>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
<script src="/design/js/google/mileage_overlay.js"></script>
<script src="/design/js/google/config.js"></script>
<script type="text/javascript">

    var map;
    var polyline = new google.maps.Polyline();
    var radius_square = <?echo sqrt($this->data['radius_square'])?>;
    var center_lat = <?echo $this->data['center_lat']?>;
    var center_lng = <?echo $this->data['center_lng']?>;
    var ne = new google.maps.LatLng(center_lat+radius_square, center_lng+radius_square);
    var sw = new google.maps.LatLng(center_lat-radius_square, center_lng-radius_square);
    var bounds = new google.maps.LatLngBounds(sw, ne);
    function initialize() {
        $('section.content').prepend('<div id="map-canvas" class="route_canvas"></div><img src="/design/images/article_img_bg.png" alt="img" class="lines route_lines" />');
        var startPosition = new google.maps.LatLng(<?echo $this->data['start_lat']?>, <?echo $this->data['start_lng']?>);
        var centerPosition = new google.maps.LatLng(<?echo $this->data['center_lat']?>, <?echo $this->data['center_lng']?>);
        var mapSelector = 'map-canvas';
        $('#'+mapSelector).css('height', '700px');
        var mapDiv = document.getElementById(mapSelector);
        var mapOptions = {
            scrollwheel:false,
            zoom: 18,
            center: centerPosition,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl:false,
            panControl:false,
            zoomControl: false,
            styles: STYLES_MAP
        };
        map = new google.maps.Map(mapDiv, mapOptions);
        initControl(map);

        var marker = getMarker(startPosition, map);
        var overlay = new MileageOverlay(<?echo number_format($this->data['mileage'], 2, '.', '')?>, marker, map, true);
   	    var overview_path = <?echo $this->data['overview_path']?>;
        var latLngArray = [];
        polyline = new google.maps.Polyline({strokeColor: '#FFEC2C', strokeWeight: 4});
        for(var i in overview_path) {
        	latLngArray.push(new google.maps.LatLng(overview_path[i].jb, overview_path[i].kb));
        }
        polyline.setPath(latLngArray);
        polyline.setMap(map);
        var bounds = getBoundsForPoly(polyline);
        //map.fitBounds(bounds);


        var zoomLevel = getBoundsZoomLevel(bounds, {height: $('#'+mapSelector).height() - 160, width: $('#'+mapSelector).width()});
        map.setZoom(zoomLevel);



        //var northEast = bounds.getNorthEast();
        //var southWest = bounds.getSouthWest();

        //map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(southWest.lat() + 0.1*southWest.lat(), southWest.lng()), new google.maps.LatLng(northEast.lat() + 0.1*northEast.lat(), northEast.lng())));
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
    <div class="route_title">
        <!--<div id="map-canvas"></div>
        <img src="/design/images/article_img_bg.png" alt="img" class="lines" />-->
        <div class="route_length">
            <?echo number_format($this->data['mileage'], 2, '.', '')?> <span>км</span>
            <div class="lines"></div>
        </div>
        <div class="local">
            <div class="flag"></div>
            <?php echo $this->data['name_point_start']?> >
            <br />
            <?php echo $this->data['name_point_finish']?>
            <div class="town">
            	<?php $keys = array_keys($this->data['routes_type']);
            			echo $this->data['routes_type'][$keys[0]];
            	?>
            </div>
        </div>
        <? if (!$isDraftArticle): ?>
            <div class="done <?php echo (isset($this->isRun) && $this->isRun === true) ? 'click' : '' ?>" onclick="runningRoute(<?echo $this->data['id']?>, 1, this)"><span></span>пробежал</div>
            <div class="i_want <?php echo (isset($this->isWantRun) && $this->isWantRun === true) ? 'click' : '' ?>" onclick="runningRoute(<?echo $this->data['id']?>, 0, this)" <?php echo (isset($this->isRun) && $this->isRun === true) ? 'style="display:none"' : '' ?>><span></span>хочу пробежать</div>
        <?endif?>
    </div>
    <h1 class="route_title_box"><?php echo $this->data['name']; ?></h1>

<?php endif;?>
<div class="sidebar_content <? echo $sidebarContentClass?>">
    <?php 
        $isFoto = false;
        if(!$isRouteArticle):
            $isFoto = !empty($this->data['title_image']);
            
        ?>

        <div class="title_img <? if(!$isFoto):?>no_photo<?endif;?>">
            <? if($isFoto):?>
                <img src="<?php echo $this->data['title_image']?>" alt="img" class="main" />
            <?endif?>
            <img src="/design/images/article_img_bg.png" alt="img" class="lines" />
            <?if(Model_Materials::CATEGORY_EVENTS == $this->data['category_id']):?>
                <div class="day"><?= intval(date('d', strtotime($this->data['date_start_event']))); ?></div>
                <div class="month"><?= DR_Api_Tools_Tools::rusMonth($this->data['date_start_event']); ?></div>
                <?if(isset($this->data['is_need_register']) && time() <= strtotime($this->data['date_start_event']) && !$this->isAllreadyEventRegister):?>
                    <a <?if(!Zend_Auth::getInstance()->hasIdentity()):?>href="/auth/register"<?else:?>href="javascript:void(0)" onclick="registerOnEvent(<?echo $this->data['id']?>, this)"<?endif;?> class="registration">зарегистрироваться</a>
                <?endif;?>
            <?endif;?>
        </div>
        <h1><?php 
            //echo utf8_wordwrap($this->data['name'], 30, "\n", true); 
            echo $this->data['name'];
        ?></h1>

    <?php endif;?>
    <div id="fb-root"></div>
    <div class="social_box">
    	<div class="social_button">
        	<?
            $resourceURL = 'http://' . strtr($_SERVER['HTTP_HOST'], array('www.' => '')) . $this->url(array('stitle' => $this->data['stitle']), Model_Materials::getRoutesName($this->data['category_id']));
            if(!$isDraftArticle):?>
                <div class="soc_cont">
                    <div id="vk_like1" style="display:inline-block; float:left;"></div>
                    
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
                    <div id="tw-button1"></div>
                    
                	<div class="g-plusone" data-size="medium" data-callback="plusOne" data-href="<?echo $resourceURL?>"></div>
                    
                    
                </div>
                <? if(Model_Groups::FAMOUS == $this->data['group'] || isset($this->data['is_profile_material'])): ?>
                    <div class="author" style="display: block;">
                        <?if(empty($this->data['author_avatar'])):?>
                            <img src="/design/images/img1.png" alt="img" />
                        <?else:?>
                            <img src="<?echo Model_Files::getPreview($this->data['author_avatar'], 100)?>" alt="img" />
                        <?endif;?>
                        <?php echo $this->data['author']?>
                    </div>
                <? endif ?>
            <?endif;?>
    	</div>
        
            <?if(!$isDraftArticle):?>
                <script type="text/javascript">
                    $(function(){
                        var vkLikeWidgetOptions = {type: "mini", height: 20};
                        <?
                        $this->headMeta()->appendProperty('og:title', $this->data['name'].' - adidas running')
                                         ->appendProperty('og:type', 'article')
                                         ->appendProperty('og:site_name', 'adidas Running')
                                         ->appendProperty('og:url', $resourceURL)
                                         ->appendProperty('fb:app_id', '460390354041042');
                        if ($isFoto):
                            //$image = 'http://' . $this->http_host . Model_Files::getPreview($this->data['title_image'], 560);
                            $image = 'http://' . $this->http_host . $this->data['title_image'];
                            $this->headMeta()->appendProperty('og:image', $image);
                        ?>
                            var pageImage = '<?echo $image?>';
                            vkLikeWidgetOptions['pageImage'] = pageImage;
                        <?endif;?>
                        VK.Widgets.Like('vk_like1', vkLikeWidgetOptions);
                        VK.Widgets.Like('vk_like2', vkLikeWidgetOptions);
                        VK.Observer.subscribe('widgets.like.liked', function(e){
            	           materialVote(<?echo $this->data['id']?>, 'vk', 1);
                       	});
                        VK.Observer.subscribe('widgets.like.unliked', function(e){
                    	   materialVote(<?echo $this->data['id']?>, 'vk', 0);
                       	});
                     });
                    
                    window.twttr = (function (d,s,id) {
            		  var t, js, fjs = d.getElementsByTagName(s)[0];
            		  if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
            		  js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
            		  return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
            		}(document, "script", "twitter-wjs"));
            		var resourceURL = '<?echo $resourceURL?>';
                    twttr.ready(function (twttr) {

                        
            			twttr.widgets.createShareButton(
              				  resourceURL,
              				  document.getElementById('tw-button1')
              			);
            			twttr.widgets.createShareButton(
            				  resourceURL,
            				  document.getElementById('tw-button2')
            			);
            			twttr.events.bind('tweet',  function(intentEvent){
                            materialVote(<?echo $this->data['id']?>, 'tw', 1);
                		});
            		});
            		function plusOne(data){
                		if(data.state == "on") {
                    		materialVote(<?echo $this->data['id']?>, 'gp', 1);
                    	} else {
                    	   materialVote(<?echo $this->data['id']?>, 'gp', 0);
                    	}
                	}
                    window.___gcfg = {lang: 'ru'};
                    (function() {
                        $('.g-plusone').data('href', resourceURL);
            		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            		    po.src = 'https://apis.google.com/js/plusone.js';
            		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                    })();
                    (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=460390354041042";
                        js.async = true;
                        fjs.parentNode.insertBefore(js, fjs);
                      }(document, 'script', 'facebook-jssdk'));
          		    window.fbAsyncInit = function() { 
                        FB.init({
                            appId: '460390354041042', 
                            status: true,
                            cookie: true,
                            xfbml: true,
                            oauth: true
                        });

                        FB.Event.subscribe('edge.create',
                            function(response) {
                                materialVote(<?echo $this->data['id']?>, 'fb', 1);
                            }
                        );
                        FB.Event.subscribe('edge.remove',
                            function(response) {
                                materialVote(<?echo $this->data['id']?>, 'fb', 0);
                            }
                        );
                     };

            	</script>
            <?endif;?>

    </div>
    <div class="material_body" style="min-height: 10px;">
        <?php 
            //echo utf8_wordwrap($this->data['material_body'], 90, "\n", true); 
            echo $this->data['material_body'];
        ?>
    </div>

    <?php if(count($this->data['gallery'])):?>
    	<script type="text/javascript">
            function nextSlideGallery(galleryId){
                $('#simplegallery'+galleryId+' .navpanelfg').find('.navimages').last().click();
            }
            $(function(){
                var isSpecial = $('div.spectial-event').length;
            	<?php foreach($this->data['gallery'] as $index=>$images):?>
                	new simpleGallery({
                		wrapperid: "simplegallery<?php echo $index+1?>", //ID of main gallery container,
                        dimensions:isSpecial ? [840, 600] : [560, 560],
                		imagearray: [
                            <?php foreach($images as $img):
                                $info = pathinfo($img['src']);
                                $img840 = $info['dirname']. "/prev840." . $info['extension'];
                            ?>
                				[!isSpecial ? "<?php echo $img['src']?>" : "<?php echo $img840?>", "javascript:nextSlideGallery(<?php echo $index+1?>)", "", "<?echo $img['name']?>"],
                			<?php endforeach;?>
                		],
                		autoplay: [false, <?php echo rand(2000, 3000)?>, 2],
                		persist: false,
                		fadeduration: 300
                	});
            	<?php endforeach;?>
            });
    	</script>
    <?php endif;?>
     <aside>
        <!--<div class="element event current">
            <div class="event_container">  
                 <div class="day">13</div>
                 <div class="lines"></div>
                 <div class="month">июля</div>
                 <a  href="" class="title">пражский марафон</a>
            </div>
        </div>-->
        <?if(Zend_Auth::getInstance()->hasIdentity() && $isDraftArticle && $this->data['users_id'] == Zend_Auth::getInstance()->getIdentity()->id):?>
            <div style="position:fixed;z-index: 5;" class="grey_sidebar sidebar_article_draft">
                <div class="title"><?if(!$isRouteArticle):?>Статья<?else:?>Маршрут<?endif;?></div>
                <div class="add_point routh" onclick="window.location = '<?echo $isRouteArticle ? $this->url(array('id'=>$this->data['users_id']), 'profile_routes_draft') : $this->url(array('id'=>$this->data['users_id']), 'profile_articles_draft_view')?>'">в черновики<span></span></div>
                <div class="add_point moder" onclick="goToModern()">на модерацию<span></span></div>
                <div class="add_point edit" onclick="window.location = '<?echo $isRouteArticle ? $this->url(array('id'=>$this->data['users_id'], 'route_id'=>$this->data['id']), 'profile_routes_edit') :$this->url(array('id'=>$this->data['users_id'], 'article_id'=>$this->data['id']), 'profile_articles_edit')?>'">редактировать<span></span></div>
            </div>
            <script type="text/javascript">
                $(function(){
                    repositionMenuForm();
                    $(window).scroll(function(){
                        repositionMenuForm();
                    });
                })

                function repositionMenuForm() {
                    var ui = $('.grey_sidebar');
                    var topLimit = $('.sidebar_content').offset().top; 
                    var bottomLimit = $('.material_body').offset().top + $('.material_body').height() - ui.height();
                    
                    var percent = $(window).scrollTop() / ($(document).height() - $(window).height());
                    var TOP = $(window).height() - ui.height();
                    TOP = TOP < 0 ? 0 : TOP / 2;
                    var position = percent * ($(document).height() - $(window).height()) + TOP;
                    if(position > bottomLimit)
                        position = bottomLimit;
                    if(position < topLimit)
                        position = topLimit;

                    ui.offset({top: position});
                }
                
                function goToModern(path){
                    $.post('/users/articles/processtomodern/id/<?echo $this->data['id']?>', {}, function(){
                       window.location = '<?echo $isRouteArticle ? $this->url(array('id'=>$this->data['users_id']), 'profile_routes_draft') : $this->url(array('id'=>$this->data['users_id']), 'profile_articles_draft_view')?>'; 
                    });
                }
            </script> 
        <?endif;?>
        <?php if(count($this->data['items'])):?>
            <?php foreach($this->data['items'] as $item):?>
                <div id="block_item<?echo $item['id']?>" class="element goods goods_block" style="display: none;">
                    <a class="goods_title"><?echo DR_Api_Tools_Tools::getShortString($item['name'], 18)?></a>
                    <div class="img_box">
                        <img src="<?echo $item['image']?>" alt="img" />
                        <?$isReviewItem = isset($this->itemsReview[$item['id']]) && count($this->itemsReview[$item['id']]['review_article']);
                            $isBuyLink = !empty($item['link']);

                        ?>
                        <!--
                        <div class="hover_box" <?if(!$isReviewItem || !$isBuyLink):?>style="height: 57px;"<?endif;?>>
                            <?if($isReviewItem):?>
                                <a href="<?echo $this->url(array('stitle'=>$item['stitle']), 'items_review')?>">обзор</a>
                            <?endif;?>
                            <?if($isBuyLink):?>
                                <a href="<?echo $item['link']?>">купить</a>
                            <?endif;?>
                        </div>
                        -->
                        <div class="white_shadow">
                          <a class="buy" href="<?echo $item['link']?>">купить</a>
                          <?if($isReviewItem):?>
                            <?if(count($this->itemsReview[$item['id']]['review_article']) > 1):?>
                                <a class="view" href="<?echo $this->url(array('stitle'=>$item['stitle']), 'items_review')?>">обзор<span></span></a>
                            <?elseif(!in_array($this->data['stitle'], $this->itemsReview[$item['id']]['review_article'])):?>
                                <a class="view" href="<?echo $this->url(array('stitle'=>$this->data['stitle']), 'article_view')?>">обзор<span></span></a>
                            <?endif;?>
                          <?endif;?>
                          <?if(Zend_Auth::getInstance()->hasIdentity()):
                            $isLike = isset($this->itemsReview[$item['id']]) && 1 == $this->itemsReview[$item['id']]['status'];
                            $isHave = isset($this->itemsReview[$item['id']]) && 2 == $this->itemsReview[$item['id']]['status'];
                            $isLike = $isHave ? true : $isLike;
                          ?>
                              <div class="i_have <?if($isLike):?>disabled<?endif;?>" onclick="addClothes(<?echo $item['id']?>, 0, this)">я хочу<span></span></div>
                              <div class="i_have ui_have_clothes <?if($isHave):?>disabled<?endif;?>" onclick="addClothes(<?echo $item['id']?>, 1, this)">у меня есть<span></span></div>
                          <?endif;?>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function(){
                        setInterval(function(){
                            var textBlock = $('#item_<?echo $item['id']?>');
                            var itemBlock = $('#block_item<?echo $item['id']?>');
                            var materialBody = $('.material_body');
                            var materialOffset = materialBody.offset().top + materialBody.height();


                            var topTextBlock = textBlock.offset().top;
                            if(topTextBlock + itemBlock.height() > materialOffset)
                                topTextBlock -= itemBlock.height();
                            itemBlock.show().offset({'top': topTextBlock - 20});
                        }, 100);

                    });
                </script>
            <?endforeach?>
        <?php endif;?>
    </aside>
    <div style="clear: both;"></div>
</div>

<div class="social_boxes">
    <?if(!$isDraftArticle):?>
        <div class="vk"><span></span>
        	<div class="container">
        		<div id="vk_like2"></div>
        	</div>
        </div>
        <div class="fb"><span></span>
        	<div class="container">
        		<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
        	</div>
        </div>
        <div class="tw"><span></span>
        	<div class="container">
        		<div id="tw-button2"></div>
        	</div>
        </div>
        <div class="gp"><span></span>
        	<div class="container">
        		<div class="g-plusone" data-size="medium" data-callback="plusOne" data-href="<?echo $resourceURL?>"></div>
        	</div>
        </div>
    <?endif;?>
    <p style="clear: both;"></p>
</div>
<div class="tags">
    <?if(count($this->data['tags'])):?>
        <div class="title">тэги ></div>
        <?php foreach($this->data['tags'] as $tag):?>
        	<strong onclick="window.location='<?echo $this->url(array('tag'=>urlencode($tag)), 'search_tag')?>'"><?php echo $tag?></strong>
        <?php endforeach;?>
    <?endif;?>
</div>
<?if($this->isBlogArticle && false):?>
    <script type="text/javascript">
        var topBlogPaginator = new Paginator('/articles/index/blockmaterialsblog');
        topBlogPaginator.setContainerSelector('#container_blog_top')
                        .setParam('users_id', <?php echo $this->data['users_id']?>)
                        .setParam('jsPaginator', 'topBlogPaginator')
                        .setParam('allready_load_id', <? $data = count($this->topBlogMaterials) ? array_keys($this->topBlogMaterials) : array(); echo json_encode($data)?>)
                        .setCallbackEndPaginator(function(currentPage, countPage) {
                            var ui = $('#top_blog_more');
                            if(currentPage >= countPage) {
                                ui.hide();
                            } else {
                                ui.show();
                            }
                        });
    </script>
    <div id="container_blog_top">
        <?echo $this->partial('partials/people.tpl', array('user' => $this->user));?>
        <?echo $this->partial('blockmaterials.tpl', array('data' => $this->topBlogMaterials, 'jsPaginator'=>'topBlogPaginator', 'pageCount'=>$this->user['article_count'] > 3 ? 1 : 0));?>
        <div style="clear:both;"></div>
    </div>
    <script type="text/javascript">
        $(function(){
             $("#container_blog_top .element").not('.people').first().addClass('active');
        });
    </script>
    <div id="top_blog_more" class="more_article blog" style="display: none;" onclick="topBlogPaginator.getMaterials()">все записи блогера</div>
<?endif;?>
<?if($this->isBlogArticle):?>
    <div class="article_list_title">другие записи <!--<span><?echo $this->user['name'] . ' ' . $this->user['sname']?></span>--> ></div>
    <script type="text/javascript">
        var bottomBlogPaginator = new Paginator('/articles/index/blockmaterialsblog');
        bottomBlogPaginator.setContainerSelector('#container_blog_top')
                        .setParam('users_id', <?php echo $this->data['users_id']?>)
                        .setParam('jsPaginator', 'bottomBlogPaginator')
                        .setParam('allready_load_id', <? $data = count($this->bottomBlogMaterials) ? array_keys($this->bottomBlogMaterials) : array(); echo json_encode($data)?>)
                        .setCallbackEndPaginator(function(currentPage, countPage) {
                            var ui = $('#bottom_blog_more');
                            if(currentPage >= countPage) {
                                ui.hide();
                            } else {
                                ui.show();
                            }
                        });
        $(function(){
            initIsotope('#container_blog_top, #container_blog_bottom');
        })
    </script>
    <div id="container_blog_bottom">
        <?echo $this->partial('blockmaterials.tpl', array('data' => $this->bottomBlogMaterials, 'jsPaginator'=>'bottomBlogPaginator', 'pageCount'=>$this->user['article_count'] > 4 ? 1 : 0));?>
        <div style="clear:both;"></div>
    </div>
    <div id="bottom_blog_more" class="more_article blog" style="display: none;" onclick="bottomBlogPaginator.getMaterials()">все записи блогера</div>
<?endif;?>

<div id="block_article_list_title" class="article_list_title">похожие статьи ></div>

<script type="text/javascript">
    var similarPaginator = new Paginator('/articles/index/similar');
    similarPaginator.setParam('resource', <?php echo $this->data['id']?>)
                    .setParam('jsPaginator', 'similarPaginator')
                    .setCallbackEndPaginator(function(currentPage, countPage) {
                        var ui = $('#similar_more');
                        if(currentPage >= countPage) {
                            ui.text('');
                        } else {
                            ui.text('мне нужно больше статей');
                        }
                        if(0 == countPage) {
                            $('#block_article_list_title').text('');
                        }
                    });
</script>
<div id="container" class="article_list">
	<?php echo $this->action('similar', 'index', 'articles', array('resource'=>$this->data['id'], 'jsPaginator'=>'similarPaginator'));?>
    <div class="clear:both;"></div>
</div>
<div id="similar_more" class="more_article" onclick="similarPaginator.getMaterials();"></div>


<div class="comment_area">
	<?php if(Zend_Auth::getInstance()->hasIdentity() && !$isDraftArticle):?>

	                     <form class="comment_point new_comment" onsubmit="return commentSubmit(this)">
                            <div class="img_box">
                            	<?php if(empty(Zend_Auth::getInstance()->getIdentity()->avatar)):?>
                            		<img style="width: 130px;" src="/design/images/ava_default.png" alt="img" />
                            	<?php else:?>
                            		<img src="<?php echo Zend_Auth::getInstance()->getIdentity()->avatar?>" alt="img" />
                            	<?php endif;?>
                            </div>
                            <textarea placeholder="Написать комментарий" name="text"></textarea>
                            <input type="submit" value="опубликовать" />
                            <div style="clear: both;"></div>
                        	<script type="text/javascript">
                        		$(function(){
                        		    $('.comment_point textarea').focusin(function(){
                      		            $(this).next().show();
                                        $(this).css('min-height', '130px');
                        		    });
                                    $('.comment_point textarea').focusout(function(){
                                          if('' == $(this).val()) {
                                             $(this).next().hide();
                                             $(this).css('min-height', '50px');
                                          } else {
                                            $(this).css('min-height', '130px');
                                          }
                        		    });

                                });

								function commentSubmit(obj){
								    $(obj).find('textarea').css('min-height', '50px');
								    $(obj).find('textarea').next().hide();
                                    addMaterialsComment(obj, <?php echo $this->data['id']?>)
                                    return false;
								}
							</script>
                        </form>
    <?php endif;?>
	<?php echo $this->action('index', 'materials', 'comments', array('resource'=>$this->data['id']));?>
</div>