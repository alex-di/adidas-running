<?php

/**
 * Просмотр материала
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Articles_indexController extends DR_Controllers_Site {
	
    public function indexAction() {
		$article = api::getMaterials()->getMaterials($this->_getParam('stitle'));
        //_d($article);
		if (count($article)) {
		      //_d($article);
			$article = Model_Materials::getFirst($article);
            $this->view->headTitle($article['name'].' - adidas running');
            if (count($article['items'])) {                
                $this->view->itemsReview = array();
                api::getItems()->_new(array('t.id'))
                                ->in('t.id', array_keys($article['items']))
                                ->joinLeft(array('ref1'=>'Model_References'), 'ref1.reference_id = t.id and ref1.ref_type = '.Model_References::REF_TYPE_MATERIAL_SHOP, array())
                                ->joinLeft(array('mt'=>'Model_Materials'), 'mt.id = ref1.object_id', array('article_review_stitle'=>'mt.stitle'));
                                //->forSelect(array('id'=>'total_review'));
                if (Zend_Auth::getInstance()->hasIdentity()) {
                    api::getItems()->joinLeft(array('ref2'=>'Model_References'), 'ref2.reference_id = t.id and ref2.ref_type = '.Model_References::REF_TYPE_USER_ITEMS.' and ref2.object_id = '.Zend_Auth::getInstance()->getIdentity()->id, array())
                                ->joinLeft(array('meta'=>'Model_Meta'), 'meta.resource_id = ref2.id and meta.modules_id = '.Model_Meta::ITEMS_REFERENCE_USERS." and meta.key='".Model_Meta::ITEMS_REFERENCE_USERS_KEY."'", array('status_clothes'=>'meta.int_value'));
                }
                $data = api::getItems()->rows();
                if(count($data)) {
                    foreach($data as $row) {
                        $statusClothes = 0;
                        if(isset($row->status_clothes) && !is_null($row->status_clothes)) 
                            $statusClothes = empty($row->status_clothes) ? 1 : 2;
                        $this->view->itemsReview[$row->id]['status'] = $statusClothes;
                        if(!isset($this->view->itemsReview[$row->id]['review_article']))
                            $this->view->itemsReview[$row->id]['review_article'] = array();
                        if(!empty($row->article_review_stitle))
                            $this->view->itemsReview[$row->id]['review_article'][] = $row->article_review_stitle;
                    }
                }
            }
            $this->view->isAllreadyEventRegister = 0;
            if(Model_Materials::CATEGORY_EVENTS == $article['category_id'] && isset($article['is_need_register']) && Zend_Auth::getInstance()->hasIdentity()) {
                $this->view->isAllreadyEventRegister = api::getReferences()->where('t.object_id', Zend_Auth::getInstance()->getIdentity()->id)
                                    ->where('t.reference_id', $article['id'])
                                    ->where('t.ref_type', Model_References::REF_TYPE_USER_EVENTS)
                                    ->count();
                    
            }
            $this->view->isBlogArticle = false;
            if (Model_Groups::FAMOUS == $article['group']) {
                // если статья блогера
                $this->view->isBlogArticle = true;
                $this->view->user = api::getUsers()->getFamousPeople($article['users_id']);
                $this->view->user = $this->view->user[0];
                $this->view->topBlogMaterials = $this->getLastBlogMaterials($article['users_id']);
                $this->view->bottomBlogMaterials = $this->getBottomBlogMaterials($article['id'], $article['users_id']);
            }

            if (Model_Materials::CATEGORY_ROUTERS == $article['category_id'] && Zend_Auth::getInstance()->hasIdentity()) {
                $reference = api::getReferences()->_new(array('t.id', 'm.int_value', 'meta_id'=>'m.id'))
                                   ->where('t.object_id', Zend_Auth::getInstance()->getIdentity()->id)
                                   ->where('t.ref_type', Model_References::REF_TYPE_USER_RUNROUTES)
                                   ->where('t.reference_id', $article['id'])
                                   ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."'")
                                   ->row();
                if ($reference) {
                    if ($reference->int_value == 0) {
                        $this->view->isWantRun = true;    
                    }
                    if ($reference->int_value == 1) {
                        $this->view->isRun = true;    
                    }                    
                }
                //var_dump($reference);
                //die();
            }
            $this->view->data = $article;
		} else {
			$this->_redirect('/');
		}
	}
    /**
     * получить последнии статьи блогера
     */
    protected function getLastBlogMaterials($users_id, $count = 3) {
        $data = api::getMaterials()->_new(array('id'))
                            ->where('t.is_modern', 1)
                            ->where('t.users_id', $users_id)
                            ->in('t.category_id', Model_Users::getFamousArticleCategory())
                            ->order('id DESC')->limit($count)
                            ->forSelect(array('id'=>'id'));
        
        $materials_id = array_keys($data);
        if(count($materials_id))
            return api::getMaterials()->getMaterials($materials_id);
        return array();
    }
    /**
     * получить несколько предедущих и следующих статей блогера.
     */
    protected function getBottomBlogMaterials($current_materials_id, $users_id, $count = 4) {
        $data = array();
        // предедущии статьи
        $data[] = api::getMaterials()->_new(array('id'))
                            ->where('t.is_modern', 1)
                            ->where('t.id', $current_materials_id, '<')
                            ->where('t.users_id', $users_id)
                            ->in('t.category_id', Model_Users::getFamousArticleCategory())
                            ->order('t.id DESC')->limit($count)->forSelect(array('id'=>'id'));
        // следующие статьи
        $data[] = api::getMaterials()->_new(array('id'))
                    ->where('t.is_modern', 1)
                    ->where('t.id', $current_materials_id, '>')
                    ->where('t.users_id', $users_id)
                    ->in('t.category_id', Model_Users::getFamousArticleCategory())
                    ->order('t.id ASC')->limit($count)->forSelect(array('id'=>'id'));
        // орпеделяем половину количества необходимых статьей
        $half = ($count - $count%2) / 2;
        // берем сначала статьи с меньшей половины
        $firstData = count($data[0]) > count($data[1]) ? 1 : 0;
        $lastData = $firstData == 0 ? 1 : 0;
        if(0 == $firstData)
            $data[$firstData] = array_slice($data[$firstData], -$half); 
        else
            $data[$firstData] = array_slice($data[$firstData], 0, $half);
        // остальные статьи добераем с большей половины
        if(0 == $lastData)
            $data[$lastData] = array_slice($data[$lastData], -($count-count($data[$firstData]))); 
        else
            $data[$lastData] = array_slice($data[$lastData], 0, $count - count($data[$firstData]));
        $materials_id = array_merge($data[0], $data[1]);

        if(count($materials_id))
            return api::getMaterials()->getMaterials($materials_id);
        return array();
        
    }
    /**
     * блок материалов блогера
     */
   	public function blockmaterialsblogAction() {
		$page = $this->_getParam('page', 1);
        $users_id = $this->_getParam('users_id', 0);
        $perPage = 10;
		$materials = api::getMaterials();
	    $materials->_new(array('id'))
				->where('t.users_id', $users_id)
                ->in('t.category_id', Model_Users::getFamousArticleCategory())
				->where('is_modern', 1);
        // исключаем с выборки материалы, которые уже были загружены
        if(isset($_POST['allready_load_id']) && is_array($_POST['allready_load_id']) && count($_POST['allready_load_id']))
            $materials->notIn('t.id', $_POST['allready_load_id']);
            
		list($data, $paginator) = $materials->pageRows($page, $perPage);
        $this->view->pageCount = $paginator->count();
		$ids = Model_Materials::getArrayObjectId($data);
		if(count($ids)) {
			$this->view->data = $materials->getMaterials($ids);
		}
		parent::blockmaterialsAction();
	}
    /**
     * похожие материалы
     */
	public function similarAction() {
		$resource_id = $this->_getParam('resource');
		$page = $this->_getParam('page', 1);
		list($this->view->data, $this->view->pageCount) = $this->getSimilarMaterials($resource_id, $page, 4);

        parent::blockmaterialsAction();
	}
    /**
     * получить похожие материалы
     */
	protected function getSimilarMaterials($resource_id, $page, $per_page) {
		$reference = api::getReferences()->_new(array('id', 'reference_id'))
				->where('t.ref_type', Model_References::REF_TYPE_MATERIAL_MATERIALS)
				->where('t.object_id', $resource_id)->forSelect(array('id' => 'reference_id'));
		$ids = array();
        if (count($reference))
			$ids = array_values($reference);
		$data = $ids;
        $ids[] = intval($resource_id);

		$safe_query = "SELECT keywords_id FROM Model_Materialkeys WHERE materials_id = " . intval($resource_id);

		$rows = api::getMaterials()->from('Model_Materialkeys')->fields(array('t.materials_id'))
                    ->joinLeft(array('mt'=>'Model_Materials'), 'mt.id = t.materials_id', array())
                    ->where('mt.id IS NOT NULL')
                    ->where("t.keywords_id IN ($safe_query)")
                    ->notIn('t.materials_id', $ids)
                    ->order('materials_id DESC')
                    ->group('t.materials_id')
                    ->rows();

        foreach($rows as $row)
            $data[] = $row->materials_id;
        list($data, $paginator) = api::getMaterials()->pageArray($data, $page, $per_page);
		$result = array();
		if (count($data)) {
			$ids = array();
			foreach ($data as $id) {
				$ids[] = $id;
			}
			$result = api::getMaterials()->getMaterials($ids);
		}
		return array($result, $paginator->count());

	}
}