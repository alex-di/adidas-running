<?php

/**
 * @author appalach
 * @version 1.0
 * @final
 */
class Auth_logoutController extends DR_Controllers_Site
{
    /**
     * вход
     */
    public function indexAction() {
        $referer = $this->getRequest()->getHeader('referer');
        if (!$referer) {
            $referer = '/';
        }
        
        $M = api::getUsers();
        $M->doLogout();

        $this->_redirect($referer);
    }
    
}