<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Auth_confirmController extends DR_Api_Controllers_Site {
    //главная
    public function indexAction(){
        if($this->_getParam("link")) {
            $M = new Model_Users;
            $data = $M->getSimpleData(array("where"=>array("hash"=>$this->_getParam("link"))));
            if(count($data)) {
                $data = $data->offsetGet(0);
                $M->doSave(array("is_confirm"=>1), $data->id);
                $email = array (
                            'title'   => $this->view->settings['email_title_confirm_reg'],
                            'email'   => $data->email,
                            'email_callback'  => $this->view->settings['email_callback'],
                            'message' => str_replace
                            (
                                array
                                (
                                    '{username}',
                                    '{password}'
                                )
                                , 
                                array
                                (
                                    $data->name." ".$data->sname,
                                    $data->password               
                                ),
                                $this->view->settings['email_body_confirm_reg']
                            )
                    );
                DR_Api_Tools_Tools::send($email);
                $M->doLogin(array("login"=>$data->email, "pass"=>$data->password));
            }
             
            $initials = $data->name;
            $initials .= $data->sname != "" ? " ".$data->sname : "";
            $this->view->message = $initials . ", ".$this->view->translate("ваша регистрация подтверждена, сейчас вы будите перемещены на главную страницу сайта"); 
        } else {
            $this->_redirect("/");
        }
    }
}