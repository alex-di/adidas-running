<?php

/**
 * @author appalach
 * @version 1.0
 * @final
 */
class Auth_loginController extends DR_Controllers_Site {
	/**
	 * вход
	 */
	public function indexAction() {

		if ($this->_request->isPost()) {
			$M = new Model_Users;
			$scripts = array();
            $_POST['password'] = Model_Users::cryptoPassword($_POST['password']);
			$Data = $M->doLogin($_POST);
			if (!$Data) {
				$M->updateEnterTime($_SERVER['REMOTE_ADDR']);
				$location = $_SERVER['HTTP_REFERER'];
                //_d($location);
				$this->_redirectAjaxAction($location, null);
	
			} else {
				if(!strpos($_SERVER['HTTP_REFERER'], '/auth/login'))
					$scripts[] = 'window.location = "/auth/login"';
				isset($Data['login']) ? ($Data['login'] == "empty" ? $Data['login'] = $this->view
								->translate("Введите email")
								: $Data['login'] = $this->view->translate("E-mail неверный")) : $Data['login'] = '';
				isset($Data['password']) ? ($Data['password'] == "empty" ? $Data['password'] = $this->view
										->translate("Введите пароль")
								: $Data['password'] = $this->view->translate("Пароль неверный"))
						: $Data['password'] = '';
	
			}
			$this->Response($Data, $scripts);
		}

		if (Zend_Auth::getInstance()->getIdentity() !== null) 
        {
            $this->_redirect('/');
        }
	}

}
