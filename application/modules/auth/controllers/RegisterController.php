<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Auth_registerController extends DR_Controllers_Site
{

    /**
     *	 регистрация
     */
    public function indexAction()
    {
        if (Zend_Auth::getInstance()->getIdentity() !== null) 
        {
            $this->_redirect('/');
        }
    }
    
    public function memberAction() {
        DR_Api_Tools_Auth::putToStoreData($_POST);
        die;
    }
    public function saveAction()
    {
        if ($this->_request->isPost())
        {
            if ($_POST['bday'] == 'ДД.ММ.ГГГГ')
                $_POST['bday'] = '';
            if ($_POST['phone'] == 'X-XXX-XXXX')
                $_POST['phone'] = '';
            
            if (isset($_POST['email']) && $_POST['email'] == Zend_Auth::getInstance()->getIdentity()->email)
                unset($_POST['email']);

            if (isset($_POST['avatar']) && !empty($_POST['preview_x_start']) && !empty($_POST['preview_y_start'])) {
                $_POST['avatar'] = Model_Files::cropFile($_POST['avatar'], $_SERVER['DOCUMENT_ROOT'], $_POST['preview_width'], $_POST['preview_height'], $_POST['preview_x_start'], $_POST['preview_y_start']);
                $fileinfo = pathinfo($_POST['avatar']);
                $M = new DR_Api_Tools_Photo;

                $way = $_SERVER['DOCUMENT_ROOT'] . $fileinfo['dirname'] . '/' . $fileinfo['filename'];

                if (!file_exists($way))
                    mkdir($way, 0755);

                $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev100." . $fileinfo['extension'], 0, 100);
                $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev225." . $fileinfo['extension'], 0, 225);
                $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev260." . $fileinfo['extension'], 0, 260);
            }

            unset($_POST['preview_x_start']);
            unset($_POST['preview_y_start']);
            unset($_POST['preview_width']);
            unset($_POST['preview_height']);

            $_POST['sex'] = !isset($_POST['sex']) ? "" : $_POST['sex'];

            $allow_fields = array(
                "email",
                "group_id",
                "password",
                "password2",
                "avatar",
                "name",
                "sname",
                "sex",
                "bday",
                "city",
                "fb_id",
                "vk_id",
                "tw_id",
                "fq_id",
                "fb_username",
                "vk_username",
                "tw_username",
                "fq_username",
                "phone",
                "accept_rules");

            $this->getValidator()
                 ->setFieldsAllowedForSave($allow_fields)
                 ->setNotRequiredFields(array("bday",
                                            "avatar",
                                            "phone",
                                            "password",
                                            "password2",
                                            "fb_id",
                                            "vk_id",
                                            "tw_id",
                                            "fq_id",
                                            "fb_username",
                                            "vk_username",
                                            "tw_username",
                                            "fq_username"))
            ->setElement('email')
                ->email('Неверный email')
                ->recordNoExists('Email уже зарегистрирован')
            ->setElement('bday')
                ->date('Неверный формат даты', array('format' => 'd.m.Y'))
            ->setElement('phone')
                ->phone();

            $_POST = DR_Api_Tools_Tools::trimValues($_POST);            
            
            $user_id = null;
            ##### Если человек уже зарегестрирован под владельцем карты ожидающим регистрации
            
            $user_runbase = api::getUsers()->_new()->where('t.group_id', Model_Groups::RUNBASE_UNREGISTER_CARD_MASTER)
                ->where('email', $_POST['email'])
                ->row();
            
            if(count($user_runbase)) {
                $user_id = $user_runbase->id;
            } else {
                $this->getValidator()
                ->setElement('email')
                    ->email('Неверный e-mail')
                    ->recordNoExists('E-mail уже зарегистрирован');
            }
            ################   
            
            list($valid, $Data) = $this->isValid($_POST);

            $scripts = array();
            if (!isset($_POST['accept_rules']))
            {
                $valid = false;
                $scripts[] = "$.showInfo('Вам необходимо ознакомится и согласится с правилами')";
            }
            else {
                unset($Data['accept_rules']);
            }
            
            
            if ($valid)
            {
                if(!empty($_POST['bday'])) {
                    $date = DateTime::createFromFormat('d.m.Y', $_POST['bday']);
                    $_POST['bday'] = $date->format('Y-m-d');
                }
                $_POST['group_id'] = Model_Groups::USERS;
                //_d($_POST);
                
                $password = $_POST['password'];
                $_POST['password'] = Model_Users::cryptoPassword($_POST['password']);
                $users_id = api::getUsers()->doSave($_POST, $user_id);
                $meta_keys = array("fb_username", "vk_username", "tw_username", "fq_username", "phone");
                foreach($meta_keys as $key) {
                    if(isset($_POST[$key])) {
                        api::getMeta()->doSave(array('modules_id'=>Model_Meta::USERS, 'resource_id'=>$users_id, 'key'=>$key, 'value'=>$_POST[$key]));
                    }
                }
                api::getUsers()->doLogin(array("login" => $_POST['email'], "password" => $password));
                DR_Api_Tools_Auth::clear();
                //$scripts[] = '$.showInfo("Успешно сохранено", "/");';
                $scripts[] = 'window.location = "/";';
            } elseif(!count($scripts)) {
                $passwordValidator = new DR_Api_Validate_Validators_Password();
                $messageTemplates = $passwordValidator->getMessageTemplates();
                if(in_array($Data['password'], $messageTemplates)) {
                    $Data['password'] = 'Неверный формат';
                    $scripts[] = "$.showInfo('".implode('. ', $messageTemplates)."')";
                } else {
                    $scripts[] = "$.showInfo('Неверно заполнены поля')";
                }
                
            }
            if(!empty($Data['sex'])) {
                $scripts[] = "$('#block_sex').addClass('error');";
            } else {
                $scripts[] = "$('#block_sex').removeClass('error');";
            }

            $this->Response($Data, $scripts);
        }
    }
}
