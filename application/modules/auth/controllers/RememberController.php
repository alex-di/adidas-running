<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Auth_rememberController extends DR_Controllers_Site
{


    public function indexAction()
    {
        if (Zend_Auth::getInstance()->getIdentity() !== null) 
        {
            $this->_redirect('/');
        }
    }

    
    /**
     * вспомнить пароль
     */
    public function saveAction()
    {
        if ($this->_request->isPost())
        {
            $this->getValidator()->setElement('email')->email('Неверный email')->recordExists('Email не найден ');

            list($valid, $Data) = $this->isValid($_POST);
            $sripts = array();
            if ($valid)
            { 
                $M = api::getUsers();
                $user = $M($_POST['email'], 'email');

                $hash = md5(microtime() . $_POST['email']);
                $user->hash = $hash;
                $user->save();

                $email = array(
                    'title' => $this->view->settings['email_title_remember'],
                    'email' => $user->email,
                    'email_callback' => $this->view->settings['email_callback'],
                    'message' => str_replace(array(
                        '{password}',
                        '{login}',
                        '{link}',
                        ), array(
                        $user->password,
                        $user->email,
                        '<a href="' . str_replace("www.", "http://", $_SERVER['HTTP_HOST']) . '/auth/remember/change/hash/' . $hash . '">' . str_replace("www.", "http://", $_SERVER['HTTP_HOST']) . '/auth/remember/change/hash/' . $hash . "</a>"), $this->view->settings['email_body_remember']));
                //_d($email);
                DR_Api_Tools_Tools::send($email);
                $sripts[] = "$.showInfo('" . $this->view->translate("На указанный Вами e-mail оправлена ссылка для изменения пароля") . "');";

            } else {
                $sripts[] = "$.showInfo('Неверно заполнены поля')";
            }
            $this->Response($Data, $sripts);
        }
    }


    public function changeAction()
    {

        if ($this->_request->isPost())
        {
            $this->getValidator()->setElement('hash')->recordExists(null, array('table' => api::USERS, 'field' => 'hash'));

            list($valid, $Data) = $this->isValid($_POST);
            $sripts = array();
            if ($valid)
            {
                $users = api::getUsers();
                $user = $users($_POST['hash'], 'hash');

                $user->hash = "";
                $user->save();
                
                $users->doSave(array('password' => Model_Users::cryptoPassword($_POST['password'])), $user->id);
                $sripts[] = "$.confirm({message: '" . $this->view->translate("Ваш пароль изменен") . "', okButton: 'Ok'}, redirectOnMain);";
                //$sripts[] = "window.location = '/'";
            }

            $this->Response($Data, $sripts);
        } else
        {
            if ($hash = $this->_getParam('hash'))
            {
                $this->view->hash = $hash;
            } else
            {
                $this->_redirect('/');
            }

        }
    }
}
