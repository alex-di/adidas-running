<?php /**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Auth_socialController extends DR_Controllers_Site
{

    //Вход через ВК
    public function vkAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('social_layout');
        $settings = (array)json_decode($this->view->settings['vk_settings']);
        $app_id = $settings['id'];//3743345;//3661023;
        $secret = $settings['secret'];//'HompObOcfVHAKGD64C3l';//'m3fwTTKRPZys3gG3hA0b';
        require_once('./library/Other/vk/vk.php');
        $vk = new Vk($app_id, $secret);
        $reguest = Zend_Controller_Front::getInstance()->getRequest()->getParams();

        if(isset($reguest['code'])) {
            $token = $vk->getAccessToken($reguest['code'], 'http://'.$this->host.'/auth/social/vk');
            $body = $vk->api('users.get', array('uids'=>$token['user_id'], 'access_token'=>$token['access_token']));
            if(isset($body['response'][0])) {
                    $response = $body['response'][0];
              	    $model_users = api::getUsers();
                    $user = array();
                    if(!empty($response['uid']))
    			         $user = $model_users->_new()->where('vk_id', $response['uid'])->row();
                    $this->view->data = array('id'=>$response['uid'], 'name'=>$response['first_name'], 'sname'=>$response['last_name'], 'is_login'=>false, 'location' => '/auth/register');
                    DR_Api_Tools_Auth::putToStoreSocialData(array('namespace'=>'vk_id', 'uid'=>$response['uid'], 'name'=>$response['first_name'], 'sname'=>$response['last_name']));
                    
                    if(count($user) && !empty($user->email)) {
                        if(!Zend_Auth::getInstance()->hasIdentity() || $user->id != Zend_Auth::getInstance()->getIdentity()->id) {
                            Model_Meta::saveData('vk_access_token', $user->id, Model_Meta::USERS, array('text_value'=>$token['access_token']));
                        	$model_users->doLogin(array("login" => $user->email, "password" => $user->password));
                            $model_users->updateEnterTime($_SERVER['REMOTE_ADDR']);
                        	$this->view->data['location'] = $this->getLastUri();
                            $this->view->data['is_login'] = true;
                            DR_Api_Tools_Auth::clearNamespace('vk_id');
                        }
                    }
                }
        } else {
            $url = $vk->getAuthorizeURL('wall,offline', 'http://'.$this->host.'/auth/social/vk');
            header('Location: ' . $url);
        }
    

    }
    public function fbAction() {
        Zend_Layout::getMvcInstance()->setLayout('social_layout');
        require_once('./library/Other/facebook/facebook.php');//265755300235473 7f90f2326f6daa1d02c33cd0bd9903ac
        $settings = (array)json_decode($this->view->settings['fb_settings']);
        //$config = array('appId' => '460390354041042', 'secret' => '00865ee89f1cc810124682853cc63f6a');
        $config = array('appId' => $settings['id'], 'secret' => $settings['secret']);
        $facebook = new Facebook($config);
        
        
        
        $user_id = $facebook->getUser();
        $login_url = null;
        $user_profile = array();
        if($user_id) {
            try {
                $user_profile = $facebook->api('/me','GET');                
            } catch(FacebookApiException $e) {
                $login_url = $facebook->getLoginUrl(array('scope'=>'read_stream,publish_stream')); 
            }   
        } else {
            // No user, print a link for the user to login
            $login_url = $facebook->getLoginUrl(array('scope'=>'read_stream,publish_stream'));
        }
        if(!is_null($login_url)) {
            header('Location: ' . $login_url);
        } else {
            //_d($user_profile);
       	    $model_users = api::getUsers();
            $user = array();
            if(!empty($user_profile['id']))
			     $user = $model_users->_new()->where('fb_id', $user_profile['id'])->row();
            
            $this->view->data = array('id'=>$user_profile['id'], 'name'=>$user_profile['first_name'], 'sname'=>$user_profile['last_name'], 'is_login'=>false, 'location' => '/auth/register');
            DR_Api_Tools_Auth::putToStoreSocialData(array('namespace'=>'fb_id', 'uid'=>$user_profile['id'], 'name'=>$user_profile['first_name'], 'sname'=>$user_profile['last_name']));
            
            if(count($user) && !empty($user->email)) {
                if(!Zend_Auth::getInstance()->hasIdentity() || $user->id != Zend_Auth::getInstance()->getIdentity()->id) {
                    Model_Meta::saveData('fb_access_token', $user->id, Model_Meta::USERS, array('text_value'=>$facebook->getAccessToken()));
                	$model_users->doLogin(array("login" => $user->email, "password" => $user->password));
                    $model_users->updateEnterTime($_SERVER['REMOTE_ADDR']);
                	$this->view->data['location'] = $this->getLastUri();
                    $this->view->data['is_login'] = true;
                    DR_Api_Tools_Auth::clearNamespace('fb_id');
                }
            }
            $facebook->destroySession();
        }
    }

    public function twAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('social_layout');
        
        $settings = (array)json_decode($this->view->settings['tw_settings']);

        $consumer_key = $settings['id'];//'Wu4InbtwnqvtIucgxcA';
        $consumer_secret = $settings['secret'];//'1DA7yDUl6qmUI6Dh91B1jgQmMcHcxSy7o5Jy3zAbXyA';
        if (!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
            $twitteroauth = new DR_Api_Tools_Twitter($consumer_key, $consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            // Let's request the access token
            $token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
            $oauth_token = $token['oauth_token'];
            $access_token = $token['oauth_token_secret'];

            // Let's get the user's info
            $user_info = $twitteroauth->get('account/verify_credentials');
                        
            $user_id = $user_info->id . "";
        	$model_users = api::getUsers();
            $user = array();
            if(!empty($user_id))
			     $user = $model_users->_new()->where('tw_id', $user_id)->row();
            $this->view->data = array('id'=>$user_id, 'name'=>$user_info->name, 'sname'=>'', 'is_login'=>false, 'location' => '/auth/register');
            DR_Api_Tools_Auth::putToStoreSocialData(array('namespace'=>'tw_id', 'uid'=>$user_id, 'name'=>$user_info->name, 'sname'=>''));
            
            if(count($user) && !empty($user->email)) {
                if(!Zend_Auth::getInstance()->hasIdentity() || $user->id != Zend_Auth::getInstance()->getIdentity()->id) {
                    Model_Meta::saveData('tw_oauth_token', $user->id, Model_Meta::USERS, array('text_value'=>$oauth_token));
                	Model_Meta::saveData('tw_access_token', $user->id, Model_Meta::USERS, array('text_value'=>$access_token));
                    $model_users->doLogin(array("login" => $user->email, "password" => $user->password));
                    $model_users->updateEnterTime($_SERVER['REMOTE_ADDR']);
                	$this->view->data['location'] = $this->getLastUri();
                    $this->view->data['is_login'] = true;
                    DR_Api_Tools_Auth::clearNamespace('tw_id');
                }
            }
            unset($_SESSION['oauth_token']);
            unset($_SESSION['oauth_token_secret']);

        } else {

            $this->setLastUri();
            $twitteroauth = new DR_Api_Tools_Twitter($consumer_key, $consumer_secret);
            // Requesting authentication tokens, the parameter is the URL we will be redirected to
            $request_token = $twitteroauth->getRequestToken('http://'.$this->host.'/auth/social/tw');

            // Saving them into the session
            $_SESSION['oauth_token'] = $request_token['oauth_token'];
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

            // If everything goes well..
            if ($twitteroauth->http_code == 200) {
                // Let's generate the URL and redirect
                $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
                header('Location: ' . $url);
            }
        }
    }
    public function foursquareAction(){
        Zend_Layout::getMvcInstance()->setLayout('social_layout');
        require_once('./library/Other/foursquare/FoursquareAPI.class.php');
        $settings = (array)json_decode($this->view->settings['fq_settings']);
       	$foursquare = new FoursquareAPI($settings['id'], $settings['secret']);
        $resirectUri = 'http://'.$this->host.'/auth/social/foursquare';
        if(!isset($_GET['code'])) {
            header('Location: ' . $foursquare->AuthenticationLink($resirectUri));
        } else {
            $token = $foursquare->GetToken($_GET['code'], $resirectUri);
            $response = $foursquare->GetPublic('users/self', array('oauth_token' => $token));
            $response = json_decode($response);
            $this->view->data = array('id'=>'', 'name'=>'', 'sname'=>'', 'is_login'=>false, 'location' => '/auth/register');
            if(isset($response->response->user->id)) {
                $user_id = $response->response->user->id;
            	$model_users = api::getUsers();
                $user = array();
                if(!empty($user_id))
    			     $user = $model_users->_new()->where('fq_id', $user_id)->row();
                $this->view->data = array('id'=>$user_id, 'name'=>$response->response->user->firstName, 'sname'=>$response->response->user->lastName, 'is_login'=>false, 'location' => '/auth/register');
                DR_Api_Tools_Auth::putToStoreSocialData(array('namespace'=>'fq_id', 'uid' => $user_id, 'name' => $response->response->user->firstName, 'sname'=>$response->response->user->lastName));
                
                if(count($user) && !empty($user->email)) {
                    if(!Zend_Auth::getInstance()->hasIdentity() || $user->id != Zend_Auth::getInstance()->getIdentity()->id) {
                    	Model_Meta::saveData('fq_access_token', $user->id, Model_Meta::USERS, array('text_value'=>$token));
                        $model_users->doLogin(array("login" => $user->email, "password" => $user->password));
                        $model_users->updateEnterTime($_SERVER['REMOTE_ADDR']);                        
                    	$this->view->data['location'] = $this->getLastUri();
                        $this->view->data['is_login'] = true;
                        DR_Api_Tools_Auth::clearNamespace('fq_id');
                    }
                }
            }

        }
        
    }

}
