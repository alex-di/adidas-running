<span class="right_title"><?=$this->translate("Регистрация")?></span>
<form class="registration_form no_auth"  onsubmit="return myAjax('/auth/register/', this)" method="post">
    
    <label for="email"><span id="err_email"></span></label>
    <input name="email" type="text" placeholder="mail" class="text_field"/>
    
    <label for="password"><span id="err_password"></span></label>
    <input name="password" type="password" placeholder="password" class="text_field"/>
    
    <label for="name"><span id="err_name"></span></label>
    <input name="name" type="text" placeholder="name" class="text_field"/>
    
    <label for="sname"><span id="err_sname"></span></label>
    <input name="sname" type="text" placeholder="surname" class="text_field"/>
    <input type="submit" class="submit" value="<?=$this->translate("регистрация")?>" name=""/>
</form>
<div class="shadow"><img alt="img" src="/design/images/right_column_shadow.png"/></div>
<div class="social_login no_auth_sicial">
	 <a class="facebook_reg" href="javascript:void(0)" onclick="loginFB()"><?=$this->translate("Войти через Facebook")?></a>
     <a class="twitter_reg" href="/auth/social/tw"><?=$this->translate("Войти через Twitter")?></a>
     <a class="vkontacte_reg" href="/auth/social/vk"><?=$this->translate("Войти через VK")?></a>            
</div>
<div class="label_block">
	<span class="label">tags</span>
</div>
<?if(count($this->tags)):?>
    <div style="padding:0 5px;" class="trands">
        <?foreach($this->tags as $row):?>
            <a href="<?=$this->url(array("tag"=>$row['name']), "search_tag")?>"><?=$row['name']?><div class="triangle"></div></a> 
        <? endforeach; ?>
    </div>
<?endif;?>