        <form id="enter_dialog_form" class="login_form" onsubmit="return myAjax('/auth/index/login', this)" method="post">
            <input name="login" type="text" placeholder="mail" class="text_field">
            <input name="pass" type="password" placeholder="password" class="text_field"><br /><br />
            <a href="javascript:void(0)" onclick="getRememberpassForm()" class="forget_pass" id="remember_pass_link2"><?=$this->translate("Забыли пароль?")?></a><br />
            <div class="blue_button login_button"  onclick="$('#enter_dialog_form').submit()"><?=$this->translate("войти")?></div>                            
            <input name="" type="checkbox" class="remember_me">
            <span class="remember_me"><?=$this->translate("Чужой комп")?></span>
        </form>
        <div class="social_button">
        	<span class="no_account no_account_window">
            	<?=$this->translate("Ещё нет аккаунта?")?><br />
                <span class="green_registration" style="color:#4d83ba;" onclick="$.closeDialog();$('.registration_tabs li').eq(1).click()"><?=$this->translate("Регистрация")?></span><br/> 
                <?=$this->translate("или")?>
            </span>
            <a  href="javascript:void(0)" onclick="loginFB()" class="facebook_reg"><?=$this->translate("Войти через Facebook")?></a>
            <a href="/auth/social/tw" class="twitter_reg"><?=$this->translate("Войти через Twitter")?></a>
            <a href="/auth/social/vk" class="vkontacte_reg"><?=$this->translate("Войти через VK")?></a>
        </div>