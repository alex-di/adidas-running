<div class="box login_box visible"> <!--login-->
	<section class="registration">        	
        <form id="enter_form" class="registration_form" onsubmit="return myAjax('/auth/index/login', this)" method="post">
            
            <label for="login"><span id="err_login"></span></label>
            <input name="login" type="text" placeholder="mail" class="text_field"/>
            
            <label for="pass"><span id="err_pass"></span></label>
            <input name="pass" type="password" placeholder="password" class="text_field"/>
            
            <a href="javascript:void(0)" onclick="getRememberpassForm()" class="forget_pass" id="remember_pass_link1"><?=$this->translate("Забыли пароль?")?></a><br />                        
            <div class="vhod_button blue_button login_button" onclick="$('#enter_form').submit()"><?=$this->translate("ВОЙТИ")?></div>
            <input name="" type="checkbox" class="remember_me"/>
            <span class="remember_me"><?=$this->translate("Чужой комп")?></span>
            <br />
            <span class="no_account"><?=$this->translate("Ещё нет аккаунта?")?><br />
            	<span class="green_registration" onclick="$('.registration_tabs li').eq(1).click()"><?=$this->translate("Регистрация")?></span> <b><?=$this->translate("в voozl")?></b> - <?=$this->translate("1 минута")."!"?>
            </span>
        </form>
    </section>                
</div>

