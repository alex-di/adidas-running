<h2 class="login_title">вы ввели неверный e-mail или пароль, попробуйте еще раз</h2> 
<form onsubmit="return jphp('/auth/login', this)" class="registration login_page">
    <div class="registration_column">
        <div class="control">
            <label>e-mail</label>
            <input name="login" type="text" placeholder="e-mail" />
            <div class="error"></div>
        </div>
        <div class="control first_pass">
            <label>пароль</label>
            <input name="password" type="password" placeholder="Пароль" />
            <div class="error"></div>
        </div>
        <div class="link_area">
            <a href="/auth/remember">Забыли пароль?</a>
            <a href="/auth/register">Регистрация</a>	    
        </div>
        <input type="submit" value="вход" class="login_submit" />
        <div style="clear: both;"></div>
    </div>  
    <div class="registration_column login_column">
        <div class="control">
            <label>войти с помощью</label>
            <div class="form_social">
                <a href="javascript:void(0)" onclick="vkLogin()" class="vk"><span class="ico"></span><p class="text">вконтакте</p><span class="del"></span></a>
                <a href="javascript:void(0)" onclick="twitterLogin()" class="tw"><span class="ico"></span><p class="text">twitter</p><span class="del"></span></a>
                <a href="javascript:void(0)" onclick="faceboockLogin()" class="fb"><span class="ico"></span><p class="text">facebook</p><span class="del"></span></a>
                <a href="javascript:void(0)" onclick="foursquareLogin()" class="fs"><span class="ico"></span><p class="text">foursquare</p><span class="del"></span></a>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
</form> 