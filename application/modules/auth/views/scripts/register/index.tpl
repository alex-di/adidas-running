<link rel="stylesheet" type="text/css" href="/design/modules/imgareaselect/css/imgareaselect-default.css" media="screen" />
<script type="text/javascript" src="/design/modules/imgareaselect/scripts/jquery.imgareaselect.js"></script>
<div class="modal" id="edit_preview" style="display: none">
    <div class="title">редактирование превью</div>
    <div class="sub_text">Осталось выбрать квадратную область для маленьких фотографий.<br /> Выбранная миниатюра будет использоваться в новостях, личных сообщениях и комментариях.</div>
    <img  src="" alt="img" />
    
    <div class="save" onclick="saveImageArea()"><span></span>сохранить изменения</div>
    <div class="close" onclick="$.modal().close();"><span></span>отменить</div>
</div>

<?php $socialData = DR_Api_Tools_Auth::getSocialData();
//_d($socialData);
    function getSocialData($fild, $socialData, $default = '') {
            return isset($socialData[$fild]) ? $socialData[$fild] : $default;
    }
?>
<h2>Регистрация на сайте</h2>
<script type="text/javascript">
    var twLoginOnSite = function(twData) {
        if(undefined != twData.is_login && twData.is_login)
            window.location = '/';
        else 
            addSocialData('tw', twData.id, twData.name);
    }
    var vkLoginOnSite = function(vkData) {
        if(undefined != vkData.is_login && vkData.is_login)
            window.location = '/';
        else 
            addSocialData('vk', vkData.id, vkData.name+' '+vkData.sname); 
    }
    var fbLoginOnSite = function(fbData) {
        if(undefined != fbData.is_login && fbData.is_login)
            window.location = '/';
        else 
            addSocialData('fb', fbData.id, fbData.name+' '+fbData.sname);
    }
    var fqLoginOnSite = function(fqData) {
        if(undefined != fqData.is_login && fqData.is_login)
            window.location = '/';
        else 
            addSocialData('fq', fqData.id, fqData.name+' '+fqData.sname);
    }
    $(function(){
        $('.fix_text_color input[type="text"]').each(function() {
            if($(this).val() == '') {
                $(this).val($(this).data('placeholder')).addClass('green_text');
            }
        });
        $('.fix_text_color input[type="text"]').on('focusin', function(){
            if($(this).data('placeholder') == $(this).val()) {
                $(this).val('');
            }
            $(this).removeClass('green_text');
        }).on('focusout', function(){
            if('' == $(this).val()) {
                $(this).val($(this).data('placeholder')).addClass('green_text');
            }
        });
    });
</script>

<form onsubmit="return jphp('/auth/register/save', this)" class="registration">
    <div class="registration_column">
        <div class="control">
            <label>e-mail</label>
            <input name="email" type="text" value="<?php echo getSocialData('email', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>пароль</label>
            <input name="password" type="password" value="<?php echo getSocialData('password', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>повторите пароль</label>                                
            <input name="password2" type="password" value="<?php echo getSocialData('password2', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
    </div>
    <div class="registration_column registration_column-person">
        <div class="control">
            <label>имя</label>
            <input name="name" type="text" value="<?php echo getSocialData('name', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>фамилия</label>
            <input name="sname" type="text" value="<?php echo getSocialData('sname', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>город</label>
            <input name="city" type="text" value="<?php echo getSocialData('city', $socialData)?>"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control file">
         <?= $this->upload('auth_uploader', 'auth' ,'flash', array('fileUploaded'=>'callback', 'error' => 'uploadError', 'filesAdded'=>'startUpload'), 'true') ?>      
            <div class="img_box">
                <input id="avatar" type="hidden" name="avatar" value=""/>
                <!-- <input type="hidden" name="preview" value=""> -->
                <input type="hidden" name="preview_x_start" value="">
                <input type="hidden" name="preview_y_start" value="">
                <input type="hidden" name="preview_width" value="">
                <input type="hidden" name="preview_height" value="">
            </div>
            <label id="auth_uploader">
                <?if(empty($avatar)):?>
                загрузить фото 
                <?else:?>
                изменить фото
                <?endif;?>
                <span></span>
            </label>
        </div>
    </div>
    <div class="registration_column registration_column-contacts">
        <div class="control fix_text_color">
            <label>телефон</label>
            <input type="text" data-placeholder="X-XXX-XXXX" value="" name="phone"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div id="block_sex" class="control sex">
            <label>пол</label>
            <div class="error_box sex">
                <label class="sex_label" for="man"><input type="radio" name="sex" id="man" value="man"/>М</label>
                <label class="sex_label second" for="woman"><input type="radio" name="sex" id="woman" value="woman" />Ж</label>
            </div>
            <script type="text/javascript">
                //$("#<?echo getSocialData('sex', $socialData, 'man')?>").attr('checked', 'checked').parent().addClass('current');
            </script>
        </div>
        <div class="control birth_day fix_text_color">
            <label>дата рождения</label>
            <input type="text" data-placeholder="ДД.ММ.ГГГГ" value="" name="bday" />
            <div class="error">Текст ошибки</div>
        </div>
    </div>
    <div class="registration_column">
        <div class="control">
            <label>добавить аккаунты</label>
            <div class="form_social">
                <a id="vk_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'vk')" class="vk <?if(isset($socialData['vk_id'])):?>active<?endif;?>">
                    <span class="ico"></span>
                    <p class="text">
                        <?if(isset($socialData['vk_id'])):?>
                          <?echo $socialData['vk_id']['name'].' '.$socialData['vk_id']['sname']?>
                          <input type="hidden" name="vk_id" value="<?echo $socialData['vk_id']['uid']?>"/>
                          <input type="hidden" name="vk_username" value="<?echo $socialData['vk_id']['name'].' '.$socialData['vk_id']['sname']?>"/>
                        <?else:?>
                            вконтакте
                        <?endif?>
                    </p>
                    <span class="del" onclick="resetAccount(this, 'вконтакте')"></span>
                </a>
                <a id="fb_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'fb')"  class="fb <?if(isset($socialData['fb_id'])):?>active<?endif;?>">
                    <span class="ico"></span>
                    <p class="text">
                        <?if(isset($socialData['fb_id'])):?>
                          <?echo $socialData['fb_id']['name'].' '.$socialData['fb_id']['sname']?>
                          <input type="hidden" name="fb_id" value="<?echo $socialData['fb_id']['uid']?>"/>
                          <input type="hidden" name="fb_username" value="<?echo $socialData['fb_id']['name'].' '.$socialData['fb_id']['sname']?>"/>
                        <?else:?>
                            facebook
                        <?endif?>
                    </p>
                    <span class="del" onclick="resetAccount(this, 'facebook')"></span>
                </a>
                <a id="tw_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'tw')" class="tw <?if(isset($socialData['tw_id'])):?>active<?endif;?>">
                    <span class="ico"></span>
                    <p class="text">
                        <?if(isset($socialData['tw_id'])):?>
                          <?echo $socialData['tw_id']['name'].' '.$socialData['tw_id']['sname']?>
                          <input type="hidden" name="tw_id" value="<?echo $socialData['tw_id']['uid']?>"/>
                          <input type="hidden" name="tw_username" value="<?echo $socialData['tw_id']['name'].' '.$socialData['tw_id']['sname']?>"/>
                        <?else:?>
                            twitter
                        <?endif?>
                    </p>
                    <span class="del" onclick="resetAccount(this, 'twitter')"></span>
                </a>
                <a id="fq_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'fq')" class="fs <?if(isset($socialData['fq_id'])):?>active<?endif;?>">
                    <span class="ico"></span>
                    <p class="text">
                        <?if(isset($socialData['fq_id'])):?>
                          <?echo $socialData['fq_id']['name'].' '.$socialData['fq_id']['sname']?>
                          <input type="hidden" name="fq_id" value="<?echo $socialData['fq_id']['uid']?>"/>
                          <input type="hidden" name="fq_username" value="<?echo $socialData['fq_id']['name'].' '.$socialData['fq_id']['sname']?>"/>
                        <?else:?>
                            foursquare
                        <?endif?>
                    </p>
                    <span class="del" onclick="resetAccount(this, 'foursquare')"></span>
                </a>
            </div>
        </div>                            
    </div>
    <div style="clear: both;"></div>
    <div class="button_area">
        <? $accept_rules = getSocialData('accept_rules', $socialData)?>
        <input type="checkbox" name="accept_rules" value="1" class="styled" <?if(!empty($accept_rules)):?>checked="checked"<?endif;?>/>
        <label class="accept">Я согласен с <a target="_blank" href="/pages/rules">правилами</a></label>
        <input type="reset" onclick="window.location= '<?= $_SERVER['HTTP_REFERER']; ?>'" value="Отмена" />
        <input type="submit" value="Регистрация" />
    </div>
    <div style="clear: both;"></div>
</form>


<script type="text/javascript">
    var maxPreviewWidth = 600;
    var previewBlockWidth = 200;
    var previewBlockHeight = 200;
    // настройки превью 
    var settings = {
        preview: {preview: {width: 260, height: 260}, absolute: {width: 0, height: 0}, is_ready: false}
    };

    //функция обработки загрузки картинки
    function callback(up, file, response) {
        jsa = $.parseJSON(response.response);
        if ('error' != jsa.status) {
            $("#avatar").val(jsa.file);
            if ($("#auth_uploader").hasClass('has_avatar')) {
                $("#auth_uploader").find('img').attr('src', jsa.file);
                $("#auth_uploader").find('img').parent().addClass('nomax');
            } else {
                $("#auth_uploader").prepend('<div class="edit_img nomax"><img src="'+jsa.file+'"/></div>');
                $("#auth_uploader").addClass('has_avatar').find('.text').remove();
                $("#auth_change_uploader").show();
            }

            settings['preview'].absolute.width = jsa.xsize;
            settings['preview'].absolute.height = jsa.ysize;
            settings['preview'].is_ready = true;

            $("#auth_change_uploader").text('изменить фото');

            editArea($("#auth_change_uploader"), 'preview', {minWidth: 260, minHeight: 260, x1: 0, y1: 0, x2: 260, y2: 260, aspectRatio:'1:1'})
        } else {
            $.showInfo(jsa.message);
            $("#auth_change_uploader").text('изменить фото');
            $("#auth_uploader").find('.text').text('загрузить фото');
        }
    };


    function startUpload(){
        $("#auth_change_uploader").text('загрузка');
        $("#auth_uploader").find('.text').text('загрузка');
    }

    function uploadError(up, err) {
        if (-600 == err.code) {
            $.showInfo("Недопустимый размер файла. Максимально допустимый размер файла "+formatSize(up.settings.max_file_size));
        } else {
            $.showInfo(err.message);
        }
    }

    var currentOptionImageArea = {};
    function editArea(button, namespace, option) {
        if(settings[namespace].is_ready) {
            var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
                
            option.minWidth = option.minWidth * scale;
            if(undefined != option.maxWidth)
                option.maxWidth = option.minWidth;
            option.x2 = option.minWidth;
            var previewHeight = maxPreviewWidth * settings[namespace].absolute.height / settings[namespace].absolute.width;
            option.minHeight = option.minHeight * scale;
            if(undefined != option.maxHeight)
                option.maxHeight = option.minHeight;
            option.y2 = option.minHeight;
            
            //var currentImage = $(button).parent().find('.img_area img');
            var src = $('#avatar').val();
            var currentSrc = $('#edit_preview > img').attr('src');
            $('#edit_preview > img').attr('src', src).data('namespace', namespace);
            
            //$('#edit_preview .image_preview_block img').attr('src', src);
            currentOptionImageArea = option;
            if('' != $('input[name="'+namespace+'_x_start"]').val()) {
                currentOptionImageArea.x1 = scale * parseFloat($('input[name="'+namespace+'_x_start"]').val());
                currentOptionImageArea.y1 = scale * parseFloat($('input[name="'+namespace+'_y_start"]').val());
                currentOptionImageArea.x2 = currentOptionImageArea.x1 + scale * parseFloat($('input[name="'+namespace+'_width"]').val());
                currentOptionImageArea.y2 = currentOptionImageArea.y1 + scale * parseFloat($('input[name="'+namespace+'_height"]').val());
            }
            currentOptionImageArea.onSelectChange = changeSelect;
            currentOptionImageArea.persistent = true;
            currentOptionImageArea.onInit = initImageArea;
            currentOptionImageArea.handles = 'corners';
            currentOptionImageArea.movable = true;
            //currentOptionImageArea.parent = '#edit_preview';
            $('#edit_preview').modal().open({
                                        closeOnOverlayClick: false,
                                        closeOnEsc: false,
                                        isReposition: false,
                                        onOpen: function(){
                                            //$('.uploader').hide();
                                            //if(currentSrc == src) 
                                               initImageAreaSelect($('#edit_preview > img'));
                                            
                                        },
                                        onClose: function(){
                                            //$('.uploader').show();
                                            $('#edit_preview > img').imgAreaSelect({remove:true});
                                            $('body').css('overflow', 'auto');
                                        },
                                        onReposition: function(){
                                            //$('#edit_preview > img').imgAreaSelect({ instance: true }).update();
                                        }
                                        });
        }
    }

    function saveImageArea(){
        var img = $('#edit_preview > img');
        var ias = img.imgAreaSelect({ instance: true });
        setFields(img, ias.getSelection());
        $.modal().close();
    }

    function changeSelect(){

    }

    function initImageAreaSelect(img){
        $(img).imgAreaSelect(currentOptionImageArea);
    }
    function initImageArea(img, selection){
        changeSelect(img, selection);
        setFields(img, selection, true);
        
        var top = ($(window).height() - $('#edit_preview').height()) / 4;
        top =  $('#edit_preview').offset().top - (top > 0 ? top : 0);
        window.scrollTo(0, top);
        //$('body').css('overflow', 'hidden');
        $('#edit_preview > img').imgAreaSelect({ instance: true }).update();
    }
    function selectEndImageArea(img, selection){
        setFields(img, selection);
    }
    function showButtonPreviewEdit(namespace) {
        $('input[name="'+namespace+'"]').parent().find('.edit_preview_link').removeClass('disabled');
    }
    function initImagesPreview(namespace) {
        var selectionWidth = parseFloat($('input[name="'+namespace+'_width"]').val());
        var selectionHeight = parseFloat($('input[name="'+namespace+'_height"]').val());
        var x_start = parseFloat($('input[name="'+namespace+'_x_start"]').val());
        var y_start = parseFloat($('input[name="'+namespace+'_y_start"]').val());
        var scaleX = settings[namespace].preview.width / selectionWidth;
        var scaleY = settings[namespace].preview.height / selectionHeight;
        $("input[name="+namespace+"]").parent().find(".img_area > img").css({
            width: Math.round(scaleX * settings[namespace].absolute.width),
            height: Math.round(scaleY * settings[namespace].absolute.height),
            marginLeft: -Math.round(scaleX * x_start),
            marginTop: -Math.round(scaleY * y_start),
            display: 'block'
        });
    }
    
    function setFields(img, selection, firstInit) {
        var namespace = $(img).data('namespace');
        var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
            
        var selectionWidth = selection.width / scale;
        var selectionHeight = selection.height / scale;
        var x_start = selection.x1 / scale;
        var y_start = selection.y1 / scale;

        var scaleX = settings[namespace].preview.width / selectionWidth;
        var scaleY = settings[namespace].preview.height / selectionHeight;
        if (firstInit == true) {
            $("input[name=avatar]").parent().find("img").css({
                maxWidth: '100%'
            })
        } else {
            $("input[name=avatar]").parent().find("img").css({
                marginLeft: -Math.round(scaleX * x_start),
                marginTop: -Math.round(scaleY * y_start),
                maxWidth: 'none',
                width: Math.round(scaleX * settings[namespace].absolute.width),
                height: Math.round(scaleY * settings[namespace].absolute.height)
            });    
        }
        
        $('input[name="'+namespace+'_x_start"]').val(x_start);
        $('input[name="'+namespace+'_y_start"]').val(y_start);
        $('input[name="'+namespace+'_width"]').val(selectionWidth);
        $('input[name="'+namespace+'_height"]').val(selectionHeight);
    }  


    function addSocialAccount(obj, namespace) {
        if(!$(obj).hasClass('active')) {
            if('tw' == namespace) {
                twitterLogin();
            }
            if('vk' == namespace) {
                vkLogin();
            }
            if('fb' == namespace) {
                faceboockLogin();
            }
            if('fq' == namespace) {
                foursquareLogin();
            }
        }
    }
    function addSocialData(namespace, uid, username) {

        $('#'+namespace+'_id').addClass('active')
                       .find('.text')
                       .empty()
                       .html(username+'<input type="hidden" name="'+namespace+'_id" value="'+uid+'"/>'
                                     +'<input type="hidden" name="'+namespace+'_username" value="'+username+'"/>');
    }

    function resetAccount(span, text) {
        var aTag = $(span).parent();
        setTimeout(function(){
            aTag.removeClass('active').find('.text').empty().html(text); 
        }, 100);
               
    }
    
    $(function() {
        $(".num_input").keydown(function(event) {
            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                 // Разрешаем: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                 // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // Ничего не делаем
                     return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }  
            }
        });
    });            
   
</script>