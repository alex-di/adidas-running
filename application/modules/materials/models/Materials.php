<?php

class Model_Materials extends DR_Models_AbstractTable {
	const CATEGORY_COACH_ARTICLES = 88;
	const CATEGORY_INSPIRING_ARTICLES = 96;
	const CATEGORY_ROUTE_ARTICLES = 97;
	const CATEGORY_USER_ARTICLES = 98;
	const CATEGORY_ROUTERS = 89;
	const CATEGORY_EVENTS = 90;
	const CATEGORY_QUETES = 91;
	const CATEGORY_TWITTER_POST = 92; #из соц сетей
	const CATEGORY_INSTAGRAM_POST = 117;
	const CATEGORY_SHOP_REVIEW = 130;
	const CATEGORY_MICOACH_ARTICLES = 137;
	const CATEGORY_RUNBASE = 153;
    const CATEGORY_UNIQUE = 170;
	const CATEGORY_NOT_SELECTED = 171;
    #Сортировка события
	const FUTURE_MATERIALS = 'future'; #будущие
	const PAST_MATERIALS = 'past'; #прошедшие
	#события - мета ключи
	const START_EVENT = 'date_start_event';

	#Цитаты и заметки мета ключи
	const QUOTE_TEXT = 'quote';
	const QUOTE_AUTOR = 'quote_autor';

	const ROUTE_MILEAGE_1_3 = 120;
	const ROUTE_MILEAGE_3_5 = 121;
	const ROUTE_MILEAGE_5_10 = 122;
	const ROUTE_MILEAGE_10_50 = 123;
	const ROUTE_TYPE_NOT_SELECTED = 172;
	#RUNBASE ТЕГ для новостей
	const RUNBASE_TAG = 'runbase';
	const MICOACH_TAG = 'micoach';

    const TYPE_NOT_MODERN = 0;
	const TYPE_MODERN = 1;
    const TYPE_CANCELED = 2;
    const TYPE_DRAFTS = 3;
    
    const REGISTER_NAMESPACE_EVENT_NEXT = 'FIRST_EVENT_NEXT';
    public function routeBetween($route_mileage) {
	   $this->joinLeft(array('meta_mileage' => api::META),
							"meta_mileage.resource_id = t.id and meta_mileage.modules_id = " . Model_Meta::MATERIALS
									. " and meta_mileage.key = 'mileage'", array('mileage'=>'meta_mileage.double_value'));
		$from = 0;
		$to = 0;
		switch ((int) $route_mileage) {
			case self::ROUTE_MILEAGE_1_3;
				$from = 1;
				$to = 3;
				break;
			case self::ROUTE_MILEAGE_3_5;
				$from = 3;
				$to = 5;
				break;
			case self::ROUTE_MILEAGE_5_10;
				$from = 5;
				$to = 10;
				break;
			case self::ROUTE_MILEAGE_10_50;
				$from = 10;
				$to = 50;
				break;
		}
		if ($from != $to) 
            $this->between('meta_mileage.double_value', $from, $to, '>=');
		
		return $this;
	}
    public static function getCategoryForSimilarArticles(){
        return array(self::CATEGORY_COACH_ARTICLES, self::CATEGORY_EVENTS, self::CATEGORY_INSPIRING_ARTICLES, self::CATEGORY_INSTAGRAM_POST, self::CATEGORY_MICOACH_ARTICLES, self::CATEGORY_ROUTERS, self::CATEGORY_ROUTE_ARTICLES, self::CATEGORY_SHOP_REVIEW, self::CATEGORY_TWITTER_POST);
    }
	public function getMaterials($material_id = array()) {
		$this->_new();
		if (is_string($material_id)) {
			$this->where('t.stitle', $material_id);
		} else {
			if (is_integer($material_id)) {
				$this->where('t.id', $material_id);
			} else {
				$this->in('t.id', $material_id)->order('FIELD(t.id, ' . implode(', ', $material_id) . ')');
			}
		}
		$data = $this
				->joinLeft(array('meta' => api::META),
						"meta.resource_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS,
						array('meta.text_value', 'meta.value', 'meta.int_value', 'meta.file_id', 'meta.key',
								'meta.list_value_id', 'meta.double_value', 'meta.date_value'))
				->joinLeft(array('lv' => api::LIST_VALUES), "lv.id = meta.list_value_id",
						array('list_value' => 'lv.value'))
				->joinLeft(array('fl' => api::FILES), "fl.id = meta.file_id",
						array('fl.path', 'fl.ext', 'file_name' => 'fl.name'))
				->joinLeft(array('mtkey' => api::MATERIALKEYS), "mtkey.materials_id = t.id", array('mtkey.keywords_id'))
				->joinLeft(array('kw' => api::KEYWORDS), "kw.id = mtkey.keywords_id",
						array('keyword_name' => 'kw.name'))
				->joinLeft(array('us' => api::USERS), "us.id = t.users_id",
						array('username' => "concat(us.name, ' ', us.sname)", "us.avatar", "us.group_id"))
				->joinLeft(array('rd' => api::ROUTESDATA), "rd.materials_id = t.id",
						array('routesdata_id' => 'rd.id', 'rd.radius_square', 'rd.center_lat', 'rd.center_lng',
								'rd.start_lat', 'rd.start_lng', 'rd.end_lat', 'rd.end_lng'))
				->joinLeft(array('rf' => api::REFERENCES),
						"rf.object_id = t.id and rf.ref_type = " . Model_References::REF_TYPE_MATERIAL_ITEMS, array())
                ->joinLeft(array('rev' => api::REFERENCES),
						"rev.object_id = t.id and rev.ref_type = " . Model_References::REF_TYPE_RUNBASE_EVENT, array())
                ->joinLeft(array('ev' => api::MATERIALS), "ev.id = rev.reference_id",
						array("event_link" => "ev.stitle"))
				->joinLeft(array('it' => api::ITEMS), "it.id = rf.reference_id",
						array('items_id' => 'it.id', 'items_name' => 'it.name', 'items_image' => 'it.image',
								'items_link' => 'it.adidas_link','items_stitle' => 'it.stitle'))->rows();

        
		$result = array();
		if (count($data)) {
			foreach ($data as $row) {
				$result[$row->id]['id'] = $row->id;
				$result[$row->id]['name'] = $row->name;
				$result[$row->id]['users_id'] = $row->users_id;
				$result[$row->id]['category_id'] = $row->category_id;
				$result[$row->id]['stitle'] = $row->stitle;
                $result[$row->id]['title_image'] = $row->title_image;
				$result[$row->id]['rate'] = $row->rate;
				$result[$row->id]['comments'] = $row->comments;
				$result[$row->id]['date'] = $row->date;
				$result[$row->id]['is_modern'] = $row->is_modern;
                $result[$row->id]['is_unique'] = $row->is_unique;
				$result[$row->id]['author'] = $row->username;
				$result[$row->id]['author_avatar'] = $row->avatar;
                $result[$row->id]['group'] = $row->group_id;
				$result[$row->id]['radius_square'] = $row->radius_square;
				$result[$row->id]['center_lat'] = $row->center_lat;
				$result[$row->id]['center_lng'] = $row->center_lng;
				$result[$row->id]['start_lat'] = $row->start_lat;
				$result[$row->id]['start_lng'] = $row->start_lng;
				$result[$row->id]['end_lat'] = $row->end_lat;
				$result[$row->id]['end_lng'] = $row->end_lng;
				$result[$row->id]['routesdata_id'] = $row->routesdata_id;
                $result[$row->id]['event_link'] = $row->event_link;
                
                
				if (!empty($row->date_value))
					$result[$row->id][$row->key] = $row->date_value;
				if (!empty($row->list_value_id))
					$result[$row->id][$row->key][$row->list_value_id] = $row->list_value;
				if (!empty($row->double_value))
					$result[$row->id][$row->key] = $row->double_value;
				if (!empty($row->text_value) && 'gallery' != $row->key)
					$result[$row->id][$row->key] = $row->text_value;
				if (!empty($row->value) && 'gallery' != $row->key)
					$result[$row->id][$row->key] = $row->value;
				if (!isset($result[$row->id]['gallery']))
					$result[$row->id]['gallery'] = array();
				if (!isset($result[$row->id]['photos']))
					$result[$row->id]['photos'] = array();
				if (!isset($result[$row->id]['tags']))
					$result[$row->id]['tags'] = array();
				if (!isset($result[$row->id]['items']))
					$result[$row->id]['items'] = array();
				if (!empty($row->items_id))
					$result[$row->id]['items'][$row->items_id] = array('name' => $row->items_name,
							'image' => $row->items_image, 'link' => $row->items_link, 'stitle'=>$row->items_stitle, 'id' => $row->items_id);
                
				if (!empty($row->keywords_id))
					$result[$row->id]['tags'][$row->keywords_id] = $row->keyword_name;
				if ('photos' == $row->key && !empty($row->file_id)) {
					$result[$row->id]['photos'][$row->file_id] = array('name' => $row->file_name, 'ext' => $row->ext,
							'path' => $row->path);
				} elseif ('gallery' == $row->key) {
					$result[$row->id]['gallery'][$row->int_value][$row->value] = array('src'=>$row->value, 'name'=>$row->text_value);
				}
			}
		}
		return $result;
	}

	public static function getFirst($data) {
		$keys = array_keys($data);
		return $data[$keys[0]];
	}
	public static function getArrayObjectId($data) {
		$result = array();
		if (count($data)) {
			foreach ($data as $row) {
				$result[] = $row['id'];
			}
		}
		return $result;
	}

	#Вставить цитаты    
    public function insertQuotes($ids, $period, $page, $perPage) {
        $totalPage = $page * $perPage;
        
        $needQoutes = ($perPage - $perPage % $period) / $period;
        if(($perPage - $perPage % $period) / $period == 0)
            $needQoutes = 1;
        $position = 0;
        for($i = 1; $i <= ($totalPage - $totalPage % $period) / $period; $i++){
            if($i * $period > $totalPage - $perPage) {
                $position = $perPage - ($totalPage - $i * $period);
                break;
            }
        }
        $result = $ids;
        if(0 != $position && $position < count($ids)) {
            $data = array();
            $data[] = array_slice($ids, 0, $position, true);
            for($i = $position ; $i < count($ids); $i+=$period) {
                $data[] = array_slice($ids, $i, $period, true);
               // _d($i, false);
            }
            $quotes = $this->_new()->where('t.category_id', Model_Materials::CATEGORY_QUETES)->order('RAND()')->limit($needQoutes)
		                ->forSelect(array('id' => 'id'));
            $result = array();
            foreach($data as $chunk) {
                $quete = array_shift($quotes);
                if(!is_null($quete))
                    $chunk[] = $quete;
                $result = array_merge($result, $chunk);
            }
            //_d($result, false);
        }
        return $result;
        //_d('totalPage / period '.(($totalPage - $totalPage % $period) / $period), false);
        //_d('needQoutes '.$needQoutes, false);
        //_d('position '.$position);
    }
    public function insertNextEvents($ids) {
  		if (count($ids)) {
			$event = $this->_new(array('id'))->where('t.category_id', Model_Materials::CATEGORY_EVENTS)
                    ->joinLeft(array('meta' => api::META),
						"meta.resource_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS . " and meta.key='date_start_event'",
						array())
					->where('meta.date_value > NOW()')->order('t.id ASC')->row();
			if (count($event)) {
				$period = round(1, count($ids) - 1);
				$left = array_slice($ids, 0, $period, true);
				$right = array_slice($ids, $period, count($ids), true);
				$ids = array_merge($left, array($event->id), $right);
			}
		}

		return $ids;
    }
    public function getNextEvent(){
		$event = $this->_new(array('id'))->where('t.category_id', Model_Materials::CATEGORY_EVENTS)
                    ->joinLeft(array('meta' => api::META),
						"meta.resource_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS . " and meta.key='date_start_event'", array())
					->where('meta.date_value > NOW()')->order('meta.date_value ASC')
                    ->row();
        if(count($event))
            return $event->id;
        return 0;
    }    
    public function getSimilarMaterials($materials_id) {
        return $this->from(api::REFERENCES)
            ->where('t.object_id', $materials_id)
            ->where('t.ref_type', Model_References::REF_TYPE_MATERIAL_MATERIALS)

            ->joinLeft(array('mt'=>$this->_name), 'mt.id = t.reference_id', array('mt.name', 'mt.id'))
            ->joinLeft(array('lv'=>'Model_Listvalues'), 'lv.id = mt.category_id', array('category_name'=>'lv.value'))->rows();    
    }
    
    public function getRunbaseEvents($materials_id) {
        return $this->from(api::REFERENCES)
            ->where('t.object_id', $materials_id)
            ->where('t.ref_type', Model_References::REF_TYPE_RUNBASE_EVENT)

            ->joinLeft(array('mt'=>$this->_name), 'mt.id = t.reference_id', array('mt.name', 'mt.id'))
            ->joinLeft(array('lv'=>'Model_Listvalues'), 'lv.id = mt.category_id', array('category_name'=>'lv.value'))->row();    
    }
    
    public function getReviewItems($materials_id) {
        return $this->from(api::REFERENCES)
            ->where('t.object_id', $materials_id)
            ->where('t.ref_type', Model_References::REF_TYPE_MATERIAL_SHOP)

            ->joinLeft(array('it'=>'Model_Items'), 'it.id = t.reference_id', array('it.name', 'it.id'))
            ->rows();    
    }
	public function getLastSocialPost() {
		return $this->_new(array('t.date', 't.id'))
				->in('t.category_id',
						array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST))
				->where("meta5.value  is not null")
                ->where("t.is_modern", self::TYPE_MODERN)
				->joinLeft(array('meta1' => api::META),
						"meta1.resource_id = t.id and meta1.modules_id = " . Model_Meta::MATERIALS
								. " and meta1.key = 'post_user_screen_name'",
						array('post_user_screen_name' => 'meta1.value'))
				->joinLeft(array('meta2' => api::META),
						"meta2.resource_id = t.id and meta2.modules_id = " . Model_Meta::MATERIALS
								. " and meta2.key = 'post_user_avatar'", array('post_user_avatar' => 'meta2.value'))
				->joinLeft(array('meta3' => api::META),
						"meta3.resource_id = t.id and meta3.modules_id = " . Model_Meta::MATERIALS
								. " and meta3.key = 'post_profile_link'", array('post_profile_link' => 'meta3.value'))
				->joinLeft(array('meta4' => api::META),
						"meta4.resource_id = t.id and meta4.modules_id = " . Model_Meta::MATERIALS
								. " and meta4.key = 'post_material_link'", array('post_material_link' => 'meta4.value'))
				->joinLeft(array('meta5' => api::META),
						"meta5.resource_id = t.id and meta5.modules_id = " . Model_Meta::MATERIALS
								. " and meta5.key = 'post_photo'", array('post_photo' => 'meta5.value'))
				->order('t.id DESC')->row();

	}

    public static function getRoutesName($category_id) {
        switch ($category_id) {
            case self::CATEGORY_COACH_ARTICLES :
            case self::CATEGORY_INSPIRING_ARTICLES :
            case self::CATEGORY_ROUTE_ARTICLES :
            case self::CATEGORY_USER_ARTICLES :
            case self::CATEGORY_SHOP_REVIEW:
            case self::CATEGORY_NOT_SELECTED:
                return 'article_view';
            case self::CATEGORY_ROUTERS :
                return 'route_view';
            case self::CATEGORY_EVENTS :
                return 'event_view';
        }

        return 'article_view';
    }

    public function getMaterialsFromFamous($user_id = false, $sort = 'id', $page = 1, $perPage = 10) {

        // $article_categories = Model_Materials::CATEGORY_COACH_ARTICLES . ',' .
        //                       Model_Materials::CATEGORY_INSPIRING_ARTICLES . ',' .
        //                       Model_Materials::CATEGORY_ROUTE_ARTICLES . ',' .
        //                       Model_Materials::CATEGORY_USER_ARTICLES;

        //$article_categories = Array(Model_Materials::CATEGORY_INSPIRING_ARTICLES, Model_Materials::CATEGORY_COACH_ARTICLES, Model_Materials::CATEGORY_ROUTE_ARTICLES,  Model_Materials::CATEGORY_USER_ARTICLES);

        $this->_new(array('id'))
             ->where('is_modern', 1)
             //->where('t.category_id IN(?)', $article_categories)
             //->in('t.category_id', $article_categories)
             ->order($sort . ' DESC');

        if ($user_id) {
            $this->where('t.users_id', $user_id);
        } else {
            $this->queryIn('t.users_id', 'SELECT id FROM Model_Users WHERE group_id = '. Model_Groups::FAMOUS);
        }

        $perPage = 100;

        return $this->pageRows($page, $perPage);
    }

	public function saveParserData($data, $tag_id) {
	    $count_save = 0;
		if (count($data)) {
			$keys = array_keys($data);
			//$existsMaterials = $this->_new(array('hash'))->in('t.hash', $keys)->forSelect(array('hash' => 'hash'));
			$model_socialpost = api::getSocialpost();
            $existsMaterials = $model_socialpost->_new(array('hash'))->in('t.hash', $keys)->forSelect(array('hash' => 'hash'));
            $model_meta = api::getMeta();
			foreach ($data as $uid => $row) {
				if (!isset($existsMaterials[$uid])) {
				    $count_save++;
					$newId = $this
							->doSave(
									array('name' => $row['text'], 'category_id' => $row['category_id']));
                    
					$model_socialpost->doSave(array('hash' => $uid, 'materials_id'=>$newId));
					
                    $model_meta
							->doSave(
									array('key' => 'post_user_screen_name', 'resource_id' => $newId,
											'modules_id' => Model_Meta::MATERIALS,
											'value' => $row['user']['screen_name']));
					$model_meta
							->doSave(
									array('key' => 'post_user_avatar', 'resource_id' => $newId,
											'modules_id' => Model_Meta::MATERIALS, 'value' => $row['user']['avatar']));
					$model_meta
							->doSave(
									array('key' => 'post_profile_link', 'resource_id' => $newId,
											'modules_id' => Model_Meta::MATERIALS,
											'value' => $row['user']['profile_link']));
					$model_meta
							->doSave(
									array('key' => 'post_material_link', 'resource_id' => $newId,
											'modules_id' => Model_Meta::MATERIALS, 'value' => $row['material_link']));
  					$model_meta
							->doSave(
									array('key' => 'post_tag_id', 'resource_id' => $newId,
											'modules_id' => Model_Meta::MATERIALS, 'list_value_id' => $tag_id));
					if (isset($row['photo'])) {
						$model_meta
								->doSave(
										array('key' => 'post_photo', 'resource_id' => $newId,
												'modules_id' => Model_Meta::MATERIALS, 'value' => $row['photo']));
					}
				}

			}
		}
        return $count_save;
	}
}