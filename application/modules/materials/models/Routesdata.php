<?php

class Model_Routesdata extends DR_Models_AbstractTable {
    
    const MINIMUM_IMAGE_PADDING_IN_PX = 5;
    const IMAGE_WIDTH_IN_PX = 260;
    const IMAGE_HEIGHT_IN_PX = 260;
    static $backgroundColorRoute = array('r' => 255, 'g' => 255, 'b' => 255);
	static $colorRoute = array('r' => 255, 'g' => 236, 'b' => 44);
    
    public static function getCoordinatesFromLatLng($overviewPath) {
        $QUARTERPI = pi() / 4;
        $maxX = 0;
        $maxY = 0;
        $minX = null;
        $minY = null;
        $temp = array();
        foreach($overviewPath as $latLng) {
            //http://en.wikipedia.org/wiki/Mercator_projection
            $longitude = $latLng->kb * pi() / 180;
            $latitude = $latLng->jb * pi() / 180;
            $x = $longitude;
            $y = log(tan($QUARTERPI + 0.5 * $latitude));
            $temp[] = array('x'=>$x, 'y'=>$y);
            if($maxX < $x) $maxX = $x;
            if($maxY < $y) $maxY = $y;
            if($minX > $x || is_null($minX)) $minX = $x;
            if($minY > $y || is_null($minY)) $minY = $y;
        }

        $result = array();
        $startX = 0.5 * self::MINIMUM_IMAGE_PADDING_IN_PX;
        $width = self::IMAGE_WIDTH_IN_PX - self::MINIMUM_IMAGE_PADDING_IN_PX;
        $startY = 0.5 * self::MINIMUM_IMAGE_PADDING_IN_PX;
        $height = self::IMAGE_HEIGHT_IN_PX - self::MINIMUM_IMAGE_PADDING_IN_PX;      
        foreach($temp as $coord) {
            $x = $startX + $width * ($coord['x'] - $minX) / ($maxX - $minX);
            $y = $startY + $height - $height * ($coord['y'] - $minY) / ($maxY - $minY);
            $result[] = array('x'=> $x, 'y'=> $y);
        }
        return $result;
    }

    public static function drawRoute($path) {

        $image = imagecreate(self::IMAGE_WIDTH_IN_PX, self::IMAGE_HEIGHT_IN_PX);
        
        $black = imagecolorallocate($image, 0, 0, 0);
        imagecolortransparent($image, $black);

        $color = imagecolorallocate ($image, self::$colorRoute['r'], self::$colorRoute['g'], self::$colorRoute['b']);
        for($i = 0; $i < count($path) - 1; $i++) {
            self::imageBoldLine($image, $path[$i]['x'], $path[$i]['y'], $path[$i+1]['x'], $path[$i+1]['y'], $color, 4);
        }

        //$filename = '/data/routes/'.md5(microtime()).'.jpg';
        //imagejpeg($image,  $_SERVER['DOCUMENT_ROOT'].$filename, 85);
        
        $filename = '/data/routes/'.md5(microtime()).'.jpg';
        imagepng($image,  $_SERVER['DOCUMENT_ROOT'] . $filename, 7);
        imagedestroy($image);
        return $filename;
    }

    public static function imageBoldLine($resource, $x1, $y1, $x2, $y2, $Color, $BoldNess=2)
    {
        $center = round($BoldNess/2);
        for($i = 0; $i < $BoldNess; $i++) { 
            $a = $center - $i; 
            if($a < 0){
                $a -= $a;
            }
            for($j = 0; $j < $BoldNess; $j++) {
                $b = $center - $j; 
                if($b < 0){
                    $b -= $b;
                }
                $c = sqrt($a * $a + $b * $b);
                if($c <= $BoldNess) {
                    imageLine($resource, $x1 + $i, $y1 + $j, $x2 + $i, $y2 + $j, $Color);
                }
            }
        }        
    } 
}
