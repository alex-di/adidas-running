<?php
class Model_Keywords extends DR_Models_AbstractTable
{
    public function saveMaterialKeywords($keywords_string, $material_id) {
    	$keywords_string = explode(',', $keywords_string);
    	$keywords = array();
    	foreach($keywords_string as $keyword) {
    		$keywords[] = trim($keyword);
    	}
    	$existKeywords = $this->_new()->in('name', $keywords)->forSelect(array('name'=>'id'));
    	$model_materialkeys = api::getMaterialkeys();
    				$model_materialkeys
					->executeQuery(
							'DELETE FROM Model_Materialkeys WHERE materials_id = ' . $material_id);
    	foreach ($keywords as $keyword) {
    		$keywords_id = 0;
    		if(!isset($existKeywords[$keyword])) {
    			$keywords_id = $this->doSave(array('name'=>$keyword));
    			$existKeywords[$keyword] = $keywords_id;
    		} else {
    			$keywords_id = $existKeywords[$keyword];
    		}
    		$model_materialkeys->doSave(array('materials_id'=>$material_id, 'keywords_id'=>$keywords_id));
    	}
    }
}