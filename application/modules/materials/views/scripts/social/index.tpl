<script type="text/javascript">
    MenuNavigator.setCurrentMenu('<?= $this->url(array(), "social")?>');
	defaultPaginator = new Paginator('/materials/social/blockmaterials');
</script>
<div id="container">
    <div class="corner-stamp social">
        <div class="title">присоединяйся к нам в социальных сетях</div>
        <div style="background: none repeat scroll 0 0 #FFFFFF;">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FadidasRunning&amp;width=260&amp;height=325&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;show_border=true&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:260px; height:325px;" allowTransparency="true"></iframe>
        </div>
         <!--<div id="fb-root"></div>
        <div style="background: none repeat scroll 0 0 #FFFFFF;" class="fb-like-box" data-href="https://www.facebook.com/adidasRunning" data-width="260" data-height="309" data-show-faces="true" data-stream="false" data-show-border="true" data-header="false"></div>-->
        <br/> <br/>
        <div id="vk_groups" ></div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "260", height: "309"}, 50263349);

            /*(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));*/
        </script>
    </div>

    <div class="element promo hashgtag_box">
        <h1>добавляй свои фото и твиты с&nbsp;хештегом</h1>
        <div class="animation">
            <script type="text/javascript">
                $(function () {
                    var tags = [];
                    <?
                    foreach ($this->list_tags as $val) {
                        $words = explode(' ', trim($val->value));
                        $wordsArray = Array(isset($words[0])? $words[0]:"", isset($words[1])? $words[1]:"", $val->ru == 1 ? "rus" : "eng");
                        ?>
                        tags.push({word1:'<span>#</span><?= $wordsArray[0]?>', word2:'<?= $wordsArray[1]?>', lang: '<?= $wordsArray[2]?>'});
                        <?
                    }
                    ?>

                    var currentTag = 1;
                    var lineHeight = 50;

                    $('div.tag.hidden').css({top:'-72px'});
                    $('div.tag.active').css({top:'0'});

                    $('div.tag-line-1 div.tag.active').addClass(tags[0].lang).html(tags[0].word1);
                    $('div.tag-line-2 div.tag.active').addClass(tags[0].lang).html(tags[0].word2);

                    $('div.tag-line-1 div.tag.hidden').addClass(tags[0].lang).html(tags[1].word1);
                    $('div.tag-line-2 div.tag.hidden').addClass(tags[0].lang).html(tags[1].word2);

                    function showCarouselTag() {
                        $('.tag').animate({top:'+=72px'}, 500, function () {
                            $(this).toggleClass('active').toggleClass('hidden');

                            if ($(this).is('.hidden')){
                                $(this)
                                        .css({top:'-72px'})
                                        .removeClass('eng')
                                        .removeClass('rus')
                                        .addClass(tags[currentTag].lang);

                                if($(this).is('div.tag-line-1 div.hidden'))
                                    $(this).html(tags[currentTag].word1);

                                if($(this).is('div.tag-line-2 div.hidden'))
                                    $(this).html(tags[currentTag].word2);
                            }
                        });

                        currentTag++;
                        if (currentTag == tags.length)
                            currentTag = 0;
                    }

                    setInterval(showCarouselTag, 3000);
                });
            </script>

            <div class="carousel-tag-line tag-line-1">
                <div class="tag hidden tag1"></div>
                <div class="tag active tag2"></div>
            </div>

            <div class="carousel-tag-line tag-line-2">
                <div class="tag hidden tag1"></div>
                <div class="tag active tag2"></div>
            </div>
        </div>
        <p>Мы бежим не ради наград и достижений.<br/>
            Мы не ждём, что нас похвалят или вручат медаль.<br/>
            Нами движет то, что способен понять<br/> только такой же как мы.<br/>
            Эмоция, которую невозможно объяснить словами.<br/>
            Эмоция, которую мы передаём в движении.<br/>
            Это бег. Это наша страсть.
        </p>
    </div>
    <?= $this->action('blockmaterials', 'social', 'materials')?>
 </div>