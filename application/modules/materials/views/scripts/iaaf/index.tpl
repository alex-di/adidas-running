﻿<link href="/design/css/iaaf.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" language="javascript" src="/design/modules/lytebox/lytebox1.js"></script>
<link rel="stylesheet" href="/design/modules/lytebox/lytebox1.css" type="text/css" media="screen"/>

<div class="limit">
<div class="iaaf">
<div class="iaaf-title">iaaf</div>
<div class="iaaf-subtitle">
    чемпионат мира <span class="adineue">iaaf</span><br/>
    по легкой атлетике<br/>
    москва <span class="adineue">2013</span>
</div>

<div class="iaaf-social">
</div>

<div class="iaaf-desc">
    <div class="column first">
        <p>IAAF (International Association of Athletics Federations) - Международная ассоциация легкоатлетических
            федераций, основанная еще в 1912 году в Стокгольме, Швеция. Именно под эгидой IAAF проходят все главные
            легкоатлетические соревнования в мире.</p>

        <p>Компания adidas, лидер в области спортивной индустрии, не могла остаться в стороне от деятельности IAAF.
            В 2008 году концерн заключил контракт с организацией сроком на 11 лет, став главным спонсором
            мероприятий IAAF. В целом же adidas поддерживает соревнования по легкой атлетике уже более 85 лет.</p>

        <p>В этом году Чемпионат мира IAAF по легкой атлетике пройдет в Москве, с 10 по 18 августа. Лучшие спортсмены
            мира померяются силами в главных дисциплинах: беге на разные дистанции, прыжках в длину и высоту, метании
            копья и т.д.</p>
    </div>
    <div class="column">
        <p>В Москву приедет 4-кратная чемпионка мира по прыжкам в высоту Бланка Влашич (Хорватия), Олимпийский чемпион,
            рекордсмен мира на дистанции 800 м Дэвид Рудиша (Кения), многократный Олимпийский чемпион и чемпион мира
            Морис Грин (США), чемпионка мира 2011 года в беге на 100 метров с барьерами Салли Пирсон (Австралия),
            Олимпийская чемпионка по прыжкам с шестом Джен Сур (США), Олимпийский чемпион 2012 в эстафете 4×100 м Йохан
            Блейк (Ямайка). Из-за травмы спортсмен не сможет выступить на соревнованиях, но с удовольствием встретится с
            поклонниками.</p>

        <p>Если ты с замиранием сердца следишь за спортивными состязаниями, приходи в Лужники, чтобы поддержать своих
            любимых спортсменов! </p>
    </div>
    <div style="clear:both;"></div>
</div>

<a href="#" class="iaaf-button"></a>

<div class="clearfix"></div>

<div class="iaaf-subtitle">
    чемпионаты мира iaaf<br/>
    по легкой атлетике
</div>

<div class="iaaf-cities"></div>

<div class="iaaf-subtitle">
    мировые звезды бега
</div>

<div class="iaaf-stars">

    <div class="iaaf-star">
        <img src="/design/images/iaaf/moris.jpg" alt="" width="258" height="258"/>

        <div class="iaaf-star-desc">
            <div class="iaaf-star-name">Морис Грин</div>
            <div class="iaaf-star-country">США</div>
            <div class="iaaf-star-date">23 июля 1974</div>

            <p>Действующий рекордсмен мира в дисциплине бег на 60 м в помещении.</p>

            <p>Экс-рекордсмен в беге на 100 м.</p>

            <p>Многократный Олимпийский чемпион и чемпион мира.</p>
        </div>
    </div>

    <div class="iaaf-star">
        <img src="/design/images/iaaf/david.jpg" alt="" width="258" height="258"/>

        <div class="iaaf-star-desc">
            <div class="iaaf-star-name">Дэвид Рудиша</div>
            <div class="iaaf-star-country">Кения</div>
            <div class="iaaf-star-date">17 декабря 1988</div>

            <p>Олимпийский чемпион, рекордсмен мира на дистанции 800 м.</p>
        </div>
    </div>

    <div class="iaaf-star">
        <img src="/design/images/iaaf/blanka.jpg" alt="" width="258" height="258"/>

        <div class="iaaf-star-desc">
            <div class="iaaf-star-name">Бланка Влашич</div>
            <div class="iaaf-star-country">Хорватия</div>
            <div class="iaaf-star-date">8 ноября 1983</div>

            <p>Вице-чемпионка Олимпиады-2008 в Пекине.</p>

            <p>Чемпионка Европы.</p>

            <p>Четырехкратная чемпионка мира.</p>
        </div>
    </div>

    <div class="iaaf-star">
        <img src="/design/images/iaaf/djene.jpg" alt="" width="258" height="258"/>

        <div class="iaaf-star-desc">
            <div class="iaaf-star-name">Джен Сур</div>
            <div class="iaaf-star-country">США</div>
            <div class="iaaf-star-date">5 февраля 1981</div>

            <p>Олимпийская чемпионка по прыжкам с шестом.</p>

            <p>Рекордсменка мира по прыжкам с шестом в помещении.</p>

            <p>Абсолютная рекордсменка США.</p>

            <p>Серебряный призер Олипиады в Пекине.</p>

            <p>Победительница Олимпиады в Лондоне.</p>
        </div>
    </div>

    <div class="iaaf-star">
        <img src="/design/images/iaaf/bleik.jpg" alt="" width="258" height="258"/>

        <div class="iaaf-star-desc">
            <div class="iaaf-star-name">Йохан Блейк</div>
            <div class="iaaf-star-country">Ямайка</div>
            <div class="iaaf-star-date">26 декабря 1989</div>

            <p>Олимпийский чемпион 2012 (эстафета, 4×100 м).</p>

            <p> Двукратный серебряный призер Олимпицских игр (100 и 200 м).</p>

            <p>Чемпион мира 2011 года (100 м).</p>

            <p>Двукратный чемпион мира 2011 года (100 м, эстафета, 4×100 м).</p>

            <p>Чемпион мира среди юниоров 2006 (эстафета, 4×100 м).</p>

            <p>Мировой рекордсмен сборной Ямайки (эстафета, 4×100 м).</p>
        </div>
    </div>
</div>
<div style="clear: both;"></div>
<div class="article_list_title" style="margin-top:275px; margin-bottom:30px; display:block;">фотографии ЧМ-2013</div>
<div class="tags" style="padding-left:20px;">
    <a href="/design/images/iaaf/gal/1.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/1.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/2.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/2.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/3.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/3.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/4.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/4.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/5.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/5.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/6.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/6.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/7.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/7.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/8.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/8.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/9.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/9.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/10.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/10.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/11.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/11.jpg"/>
    </a>

    <a href="/design/images/iaaf/gal/12.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/12.jpg"/> </a>

    <a href="/design/images/iaaf/gal/13.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/13.jpg"/> </a>

    <a href="/design/images/iaaf/gal/14.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/14.jpg"/> </a>

    <a href="/design/images/iaaf/gal/15.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/15.jpg"/> </a>

    <a href="/design/images/iaaf/gal/16.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/16.jpg"/> </a>

    <a href="/design/images/iaaf/gal/17.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/17.jpg"/> </a>

    <a href="/design/images/iaaf/gal/18.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/18.jpg"/> </a>

    <a href="/design/images/iaaf/gal/19.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/19.jpg"/> </a>

    <a href="/design/images/iaaf/gal/20.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/20.jpg"/> </a>

<a href="/design/images/iaaf/gal/21.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/22.jpg"/> </a>

<a href="/design/images/iaaf/gal/22.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/22.jpg"/> </a>

<a href="/design/images/iaaf/gal/23.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/23.jpg"/> </a>

<a href="/design/images/iaaf/gal/24.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/24.jpg"/> </a>

<a href="/design/images/iaaf/gal/25.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/25.jpg"/> </a>

<a href="/design/images/iaaf/gal/26.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/26.jpg"/> </a>

<a href="/design/images/iaaf/gal/27.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/27.jpg"/> </a>

<a href="/design/images/iaaf/gal/28.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/28.jpg"/> </a>

<a href="/design/images/iaaf/gal/29.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/29.jpg"/> </a>

<a href="/design/images/iaaf/gal/30.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/30.jpg"/> </a>

<a href="/design/images/iaaf/gal/31.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/31.jpg"/> </a>

<a href="/design/images/iaaf/gal/32.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/32.jpg"/> </a>

<a href="/design/images/iaaf/gal/33.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/33.jpg"/> </a>

<a href="/design/images/iaaf/gal/34.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/34.jpg"/> </a>

<a href="/design/images/iaaf/gal/35.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/35.jpg"/> </a>

<a href="/design/images/iaaf/gal/36.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/36.jpg"/> </a>

<a href="/design/images/iaaf/gal/37.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/38.jpg"/> </a>

<a href="/design/images/iaaf/gal/38.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/38.jpg"/> </a>

<a href="/design/images/iaaf/gal/39.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/39.jpg"/> </a>

<a href="/design/images/iaaf/gal/40.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/40.jpg"/> </a>

<a href="/design/images/iaaf/gal/41.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/41.jpg"/> </a>

<a href="/design/images/iaaf/gal/42.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/42.jpg"/> </a>

<a href="/design/images/iaaf/gal/43.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/43.jpg"/> </a>

<a href="/design/images/iaaf/gal/44.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/44.jpg"/> </a>

<a href="/design/images/iaaf/gal/45.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/45.jpg"/> </a>

<a href="/design/images/iaaf/gal/46.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/46.jpg"/> </a>

<a href="/design/images/iaaf/gal/47.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/47.jpg"/> </a>

<a href="/design/images/iaaf/gal/48.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/48.jpg"/> </a>

<a href="/design/images/iaaf/gal/49.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/49.jpg"/> </a>

<a href="/design/images/iaaf/gal/50.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo">
        <img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/50.jpg"/> </a>



<a href="/design/images/iaaf/gal/51.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/51.jpg"/> </a>
<a href="/design/images/iaaf/gal/52.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/52.jpg"/> </a>
<a href="/design/images/iaaf/gal/53.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/53.jpg"/> </a>
<a href="/design/images/iaaf/gal/54.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/54.jpg"/> </a>
<a href="/design/images/iaaf/gal/55.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/55.jpg"/> </a>
<a href="/design/images/iaaf/gal/56.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/56.jpg"/> </a>
<a href="/design/images/iaaf/gal/57.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/57.jpg"/> </a>
<a href="/design/images/iaaf/gal/58.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/58.jpg"/> </a>
<a href="/design/images/iaaf/gal/59.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/59.jpg"/> </a>
<a href="/design/images/iaaf/gal/60.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/60.jpg"/> </a><a href="/design/images/iaaf/gal/61.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/61.jpg"/> </a>
<a href="/design/images/iaaf/gal/62.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/62.jpg"/> </a>
<a href="/design/images/iaaf/gal/63.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/63.jpg"/> </a>
<a href="/design/images/iaaf/gal/64.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/64.jpg"/> </a>
<a href="/design/images/iaaf/gal/65.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/65.jpg"/> </a>
<a href="/design/images/iaaf/gal/66.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/66.jpg"/> </a>
<a href="/design/images/iaaf/gal/67.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/67.jpg"/> </a>
<a href="/design/images/iaaf/gal/68.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/68.jpg"/> </a>
<a href="/design/images/iaaf/gal/69.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/69.jpg"/> </a>
<a href="/design/images/iaaf/gal/70.jpg" rel="lytebox[imagewall]" style="display: none;" class="chm_photo"><img onload="$(this).parent().fadeIn(200)" src="/design/images/iaaf/gal/70.jpg"/>
</div>
<script type="text/javascript">
    initLytebox();
</script>
<div style="clear: both;"></div>
<div class="social_boxes">
    <div class="vk"><span></span>

        <div class="container">
            <div id="vk_like2"></div>
        </div>
    </div>
    <div class="fb"><span></span>

        <div class="container">
            <div class="fb-like" data-send="false" data-layout="button_count" data-width="450"
                 data-show-faces="false"></div>
        </div>
    </div>
    <div class="tw"><span></span>

        <div class="container">
            <div id="tw-button2"></div>
        </div>
    </div>
    <div class="gp"><span></span>

        <div class="container">
            <div class="g-plusone" data-size="medium" data-callback="plusOne"></div>
        </div>
    </div>
    <p style="clear: both;"></p>
</div>
<div class="tags">
    <?if(count($this->data['tags'])):?>
    <div class="title">тэги ></div>
    <?php foreach($this->data['tags'] as $tag):?>
    <strong onclick="window.location='<?echo $this->url(array('tag'=>$tag), 'search_tag')?>'"><?php echo $tag?></strong>
    <?php endforeach;?>
    <?endif;?>
</div>
<div id="block_article_list_title" class="article_list_title">похожие статьи ></div>


<script type="text/javascript">
    var similarPaginator = new Paginator('/articles/index/similar');
    similarPaginator.setParam('resource', <?php echo $this->data['id']?>)
    .setParam('jsPaginator', 'similarPaginator')
            .setCallbackEndPaginator(function (currentPage, countPage) {
                var ui = $('#similar_more');
                if (currentPage >= countPage) {
                    ui.text('');
                } else {
                    ui.text('мне нужно больше статей');
                }
                if (0 == countPage) {
                    $('#block_article_list_title').text('');
                }
            });
</script>
<div id="container" class="article_list">
    <?php echo $this->action('similar', 'index', 'articles', array('resource'=>$this->data['id'],
    'jsPaginator'=>'similarPaginator'));?>
    <div class="clear:both;"></div>
</div>
<div id="similar_more" class="more_article" onclick="similarPaginator.getMaterials();"></div>
<div class="comment_area">
    <?php if(Zend_Auth::getInstance()->hasIdentity() && !$isDraftArticle):?>

    <form class="comment_point new_comment" onsubmit="return commentSubmit(this)">
        <div class="img_box">
            <?php if(empty(Zend_Auth::getInstance()->getIdentity()->avatar)):?>
            <img style="width: 130px;" src="/design/images/ava_default.png" alt="img"/>
            <?php else:?>
            <img src="<?php echo Zend_Auth::getInstance()->getIdentity()->avatar?>" alt="img"/>
            <?php endif;?>
        </div>
        <textarea placeholder="Написать комментарий" name="text"></textarea>
        <input type="submit" value="опубликовать"/>

        <div style="clear: both;"></div>
        <script type="text/javascript">
            $(function () {
                $('.comment_point textarea').focusin(function () {
                    $(this).next().show();
                    $(this).css('min-height', '130px');
                });
                $('.comment_point textarea').focusout(function () {
                    if ('' == $(this).val()) {
                        $(this).next().hide();
                        $(this).css('min-height', '50px');
                    } else {
                        $(this).css('min-height', '130px');
                    }
                });

            });

            function commentSubmit(obj) {
                $(obj).find('textarea').css('min-height', '50px');
                $(obj).find('textarea').next().hide();
                addMaterialsComment(obj, <?php echo $this->data['id'] ?>);
                return false;
            }
        </script>
    </form>
    <?php endif;?>
    <?php echo $this->action('index', 'materials', 'comments', array('resource'=>$this->data['id'],
    'layout'=>'layouts/iaaf'));?>
</div>
</div>
</div>
<script type="text/javascript">
    window.twttr = (function (d, s, id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function (f) {
            t._e.push(f)
        } });
    }(document, "script", "twitter-wjs"));
    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (intentEvent) {
            materialVote( <? echo $this->data['id'] ?>,'tw');
        });
        twttr.widgets.createShareButton(
                window.location.href,
                document.getElementById('tw-button2')
        );
    });
    function plusOne(data) {
        if (data.state == "on") {
            materialVote( <? echo $this->data['id'] ?>, 'gp');
        }
    }
    window.___gcfg = {lang: 'ru'};
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=460390354041042";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create',
                function (response) {
                    materialVote( <? echo $this->data['id'] ?>, 'fb');
                }
        );
    };
    $(function () {
        VK.Widgets.Like('vk_like2', {type: "mini", height: 20});
        VK.Observer.subscribe('widgets.like.liked', function (e) {
            materialVote( <?echo $this->data['id'] ?>, 'vk');
        });
    });
</script>
