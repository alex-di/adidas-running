<div class="boost-content">
<div class="boost-front">
    <div class="title"><span>boost —</span><br/> революция в беге</div>
    <p>В 2013 году компания adidas запустила революционную
        инновацию в спорте. Технологию, которая изменит спорт
        навсегда. 27 февраля 2013 стартовали продажи новых
        кроссовок energy Boost. Boost –это принципиально новая
        технология амортизации. Она обеспечивает большее
        количество отдачи энергии, чем какой-либо
        другой материал.</p>

    <a href="#boost-map" class="boost-map-link">где купить</a>

    <div class="boost-likes">
        <div class="boost-likes-vk">
            <div id="vk_like"></div>
        </div>
        <div id="boost-likes-fb">
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like" data-href="http://www.adidas-running.ru/boost" data-send="false"
                 data-layout="button_count" data-width="450" data-show-faces="false"></div>
        </div>
        <div id="boost-likes-twi">
            <a href="https://twitter.com/share" class="twitter-share-button"
               data-text="Заряди город энергией бега" data-via="adidasRU" data-lang="en"
               data-related="adidasRU">Tweet</a>
        </div>
        <div id="boost-likes-google">
            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
            <g:plusone size="medium"></g:plusone>
        </div>
    </div>
</div>

<div class="boost-programs">
    <div class="title">программы тестирования</div>
    <div class="boost-text narrow">
        <p>В ходе разработки новых кроссовок специалистами компании adidas
            было проведено множество тестов. Программа тестирования adidas
            была сфокусирована на трех главных аспектах в характеристиках
            материала BOOST в сравнении с обычным материалом пены
            для подошвы EVA (этиленвинилацетат):</p>
        <ul>
            <li>
                Возврат энергии, т. е. доля энергии, которая сохраняется
                во время сжатия материала и возвращается при восстановления
                его первоначальноейе формы. Определяется при помощи теста,
                имитирующего удар по пятке при беге.
            </li>
            <li>
                Независимость от окружающих температурных условий
                проверяется при сжатии и деформации подошвы в специальных
                камерах, воссоздающих различные температурные условия.
            </li>
            <li>
                Степень прочности определяется после цикла из 10 000 сжатий.
            </li>
        </ul>
        <p> Согласно полученным результатам исследований, материал BOOST
            превосходит EVA по всем перечисленным характеристикам.</p>
    </div>
    <div style="height: 45px"></div>

    <div class="title">энергетические капсулы boost</div>
    <div class="boost-text">

        <p style="line-height: 28px">Ключевую роль в BOOST играют так называемые капсулы -
            округлые компоненты материала подошвы. Разработанные
            в соавторстве с мировой химической компанией BASF -
            твредые гранулы термопластика(TPU) в буквальном смысле
            надуваются и превращаются в маленькие энергетические
            капсулы, образующие оригинальную промежуточную
            подошву. Они настолько эффективно сохраняют и высвобождают энергию, что бегуны чувствуют разницу как
            только надевают кроссовки.Для создания подошвы короссовок BOOSTиспользуются передовые технологии,
            применяемые в автомобильной промышленности, соединяющие гранулы BOOST вместе с помощью паровысокого
            давления.</p>
    </div>
</div>

<div class="boost-pluses">
    <div class="boost-plus" style="left:75px;top:90px;">
        <p><span>Внешняя защита пятки.</span><br/>
            Защищает ногу и поддерживает стабильное положение ноги.</p>
    </div>
    <div class="boost-plus" style="left:275px;top:435px;">
        <p><span>Подошва BOOST</span> возвращает больше энергии при отталкивании, чем любой другой материал для
            амортизации в индустрии бега.</p>
    </div>
    <div class="boost-plus" style="left:170px;top:890px;">
        <p>Идеальная посадка кроссовок на ноге &mdash; верх облегает ногу как внутренний носок.</p>
    </div>
    <div class="boost-plus" style="left:691px;top:1238px;">
        <p><span>TECHFIT </span><br/>
            Дышащий сетчатый верх<br/> с применением TECHFIT по контуру
            обуви обеспечивает идеальную посадку
            по ноге при первой примерке.
        </p>
    </div>
    <div class="boost-plus" style="left:123px;top:1505px;">
        <p><span>ADIWEAR</span><br/>
            Очень прочная, не оставляющая следов подошва.</p>
    </div>
    <div class="boost-plus" style="left:620px;top:1598px;">
        <p>
            <span>Система TORSION</span><br/>
            Облегченная структура поддержки стопы, которая обеспечивает стабильность средней части стопы, а также
            независимое движение между передней и задней частями обуви для лучшей адаптации к поверхности.
        </p>
    </div>
    <div class="boost-plus" style="left:468px;top:1928px;">
        <p>
            <span>MICOACH</span><br/>
            Обувь с технологией BOOST совместима с&nbsp;устройством MICOACH.
        </p>
    </div>
</div>

<div class="boost-video">
    <iframe width="853" height="480" src="//www.youtube.com/embed/2QXayMkbvpk" frameborder="0" allowfullscreen></iframe>
</div>

<div class="boost-adv">
    <div class="title">ключевые преимущества boost</div>
    <div class="subtitle">СОЧЕТАНИЕ МЯГКОГО КОМФОРТА И АКТИВНОГО<br/> ВОЗВРАТА ЭНЕРГИИ В BOOST</div>

    <p>
        Технология BOOST сочетает себе “плюшевый” комфорт и активную энергию совершенно новым революционным
        способом. До сегодняшнего дня всегда приходилось искать компромисс между комфортом и энергией. Удобная
        обувь, как правило, мягкая. Обувь сохраняющая энергию, напротив, как правило более жесткая, для того, чтобы
        сохранить как можно больше энергии. Это всё равно что сравнивать диван с автомобилем Формулы-1. Диван
        удобен, когда вы садитесь и как бы утопаете на нём, но при этом требуется много энергии, чтобы подняться. С
        другой стороны машина Формулы 1 &mdash; очень быстрая, но не такая удобная. Подошва BOOST сочетает в себе эти
        два
        преимущества совершенно новым и уникальным способом.
    </p>

    <div class="subtitle2"> СРОК СЛУЖБЫ</div>
    <p> Несмотря на то что подошва BOOST мягче и имеет гораздо более высокую энергию отдачи, чем материал EVA, BOOST
        сохраняет свои характеристики намного дольше по сравнению с традиционным материалом, который деформируется и
        теряет свои свойства после долгой носки.
    </p>

    <div class="subtitle2"> ИДЕАЛЬНО ПОДХОДИТ ДЛЯ ЛЮБЫХ ПОГОДНЫХ УСЛОВИЙ</div>
    <p> Инновационная легкая структура, состоящая из энергетических капсул поддерживает высокий
        уровень амортизации лучше, чем любая другая система амортизации эва в обувной индустрии.
        В отличие от стандартной подошвы из EVA, которая становится жесткой при холоде и мягкой в жару, BOOST в 3
        раза более термостойкая.
    </p>

    <div class="subtitle2"> ЛЕГКОСТЬ. МИНИМАЛИЗМ</div>
    <p> BOOST позволяет создать более простую конструкцию обуви. Новые революционные характеристики материала BOOST
        позволили команде adidas переосмыслить традиционные способы конструкции кроссовок. Подошва модели energy
        BOOST более тонкая и легкая, но остается при этом комфортной
        и соответствует самым высоким техническим требованиям.
    </p>
</div>

<div class="boost-questions">
    <div class="title">вопросы и ответы</div>
    <ol>
        <li><span>ЧТО ЖЕ ТАКОГО “МАГИЧЕСКОГО” В ПОДОШВЕ BOOST?</span>

            <p>Она одновременно 1) очень мягкая, комфортная и 2) обеспечивает отличный возврат энергии. Высокая
                эластичность, упругость, удобство перехода с пятки на носок во время бега. Характеристики материала
                практически не зависят от окружающих температур BOOST сохраняет свои амортизирующие свойства намного
                дольше,
                не деформируется, в отличии
                от обычных кроссовок, которые теряют амортизирующие свойства в процессе носки. </p>
        </li>

        <li><span>НАСКОЛЬКО УСТОЙЧИВА BOOST ПРИ СВОЕЙ МЯГКОСТИ?</span>

            <p>Действительно, может показаться, что подошва BOOST настолько мягкая, что может сделать кроссовки
                неустойчивыми. Но биомеханические тесты показали отличную устойчивость кроссовок.
                В этом ключевую роль играет геометрическая конструкция обуви а не такие традиционные элементы, как
                pro-moderator или formotion). Даже люди со слегка нарушенной пронацией (избыточной или недостаточной)
                могут
                использовать BOOST в беге на любые дистанции. </p>
        </li>

        <li><span>КАК РАБОТАЕТ ТЕХНОЛОГИЯ BOOST ПРИ БЕГЕ ПО ПЕРЕСЕЧЕННОЙ, ВНЕДОРОЖНОЙ
        ПОВЕРХНОСТИ?</span>

            <p>Согласно обратной связи, которую мы получаем от бегунов, новые кроссовки подходят для бега
                по пересеченной местности. Благодаря своей мягкости и гибкости подошва BOOST очень хорошо адаптируется
                на
                неровных поверхностях. Подошва, однако, не имеет такого же сцепления
                с поверхностью, как в моделях обуви для бега по пересеченной местности. Можно порекомендовать
                специальную
                модель (Supernova Trail, Response Trail, Kanadia). </p>
        </li>
        <li><span>С ВИДУ ПОДОШВА BOOST СМОТРИТСЯ, КАК ДЕШЕВЫЙ ПЕНОПЛАСТ.</span>

            <p>Да, действительно, BOOST выглядит иначе, чем мы привыкли. Особая структура материала, однако, и является
                причиной его “магических” свойств. На разработку этого материала (eTPU, т.е. вспененный термопластичный
                полиуретан) ушли годы исследований adidas совместно с одной
                из ведущих мировых химических компаний. </p>
        </li>
        <li><span>ЕСТЬ ЛИ РИСК ПОВРЕЖДЕНИЯ ПОДОШВЫ BOOST?</span>

            <p>Не больше, чем стандартной модели обуви для бега. В процессе создания в команде разработчиков этому
                вопросу
                уделялось повышенное внимание.Наши многосторонние исследования показали,
                что износостойкость новой подошвы гораздо выше стандартной, применяемой ранее. </p>
        </li>
        <li><span>ЖЕЛТЕЕТ ЛИ ПОДОШВА BOOST СО ВРЕМЕНЕМ?</span>

            <p>Во время использования подошва может испачкаться, как и в любой другой обуви, особенно если носить
                кроссовки
                в бездорожье. Вы можете легко очистить обувь специальными средствами
                по уходу, которые используются для обычных кроссовок. Пенный материал BOOST был тщательно протестирован
                и
                соответствует всем требованиям устойчивости к воздействиям окружающей среды, таких как УФ-света, тепла и
                влаги. </p>
        </li>
        <li><span>ПОДОШВА BOOST ТАКАЯ МЯГКАЯ, ДАЖЕ В ОБЛАСТИ ПЯТКИ, НЕ РИСКОВАННО ЛИ ЭТО ДЛЯ БЕГУНА?</span>

            <p>В BOOST мы объединили самые важные для бегуна преимущества: сочетание мягкого комфорта
                и возврата энергии при беге.В течение многих лет считалось, что использование более твердых
                амортизирующих
                материалов в подошве обуви является лучшим решением, чтобы минимизировать потери энергии (например,
                adiprene).
                Однако, еще в 1993 году начались исследования в поисках материала, который не только амортизирует, но и
                сохраняет и освобождает назад энергию, что позволило бы улучшить эффективность бега. Таким
                революционным
                материалом стал BOOST. </p>
        </li>
        <li><span>ПОЧЕМУ ВЫ РЕШИЛИ ПЕРЕЙСТИ К ТАКОМУ МЯГКОМУ МАТЕРИАЛУ ПОДОШВЫ?</span>

            <p>Мы хорошо понимаем потребности бегуна, и ключевыми являются комфорт и амортизация при беге с минимальными
                потерями энергии. Компания adidas всегда стремилась предложить самые передовые материалы и конструкции
                обуви,
                чтобы достичь этой цели. Много лет это был материал EVA, также применялись технологии adiprene,
                специальные
                конструкции подошвы
                (например, Bounce), но BOOST стала инновационным прорывом. </p>
        </li>
        <li><span>ПЛАНИРУЕТЕ ЛИ ПРЕДСТАВИТЬ BOOST В ДРУГИХ КАТЕГОРИЯХ?</span>

            <p>Adidas уже начал исследовать новые виды спорта для применения этой технологии, наши инженеры проводят
                тесты
                материала, а также продолжают искать новые методы его производства, чтобы перевести BOOST на новый
                улучшенный
                уровень. Оставайтесь с нами в и следите
                за новинками с следующих сезонах! </p>
        </li>
        <li><span>ЧТО ЖЕ ТАКОГО ОСОБЕННОГО В НОВОМ МАТЕРИАЛЕ ВЕРХА?</span>

            <p>Эластичный сетчатый материал адаптируется к любой форме ног и превосходно охлаждает. Прочное покрытие
                TECHFIT
                позволяет снизить вес обуви, превосходно облегает ногу и ощущается словно
                вторая кожа.У вас больше не будет проблем с посадкой на ноге и в области пятки. </p>
        </li>
        <li><span>РАССКАЖИТЕ МНЕ ПОЖАЛУЙСТА О СТРОЕНИИ ПЯТОЧНОЙ ЧАСТИ ОБУВИ?</span>

            <p>Внешняя защита для пятки сохраняет стабильное положение ноги при беге. Онадвижется
                динамично в вертикальном направлении по ноге и обеспечивает большую эффективность
                при беге. Ее конструкция позволяет избежать повреждения ахиллова сухожилия. </p>
        </li>
        <li><span>А ПОЧЕМУ ТАКАЯ ВЫСОКАЯ ЦЕНА?</span>

            <p> BOOST &mdash; это революционный материал, на разработку которого ушли годы исследований
                и производство которого – очень сложный технический процесс. Материала с подобными характеристиками не
                существует на рынке, и в мире очень мало поставщиков, способных его
                производить. Именно поэтому количество пар, которые мы представляем, ограничено. Команда adidas работает
                над
                тем, чтобы снизить производственные издержки и привнести
                материал BOOST и в менее дорогие модели беговой обуви. </p>
        </li>
    </ol>

</div>

<div class="" style="position: relative;margin: 30px 0 0 0; "><a name="boost-map"></a>

    <div class="title" style="display:inline">
        где купить
    </div>

    <div class="combo town" style="width:250px;float:right;margin: 0 40px 0 0;display:none">
        <div class="current">Москва</div>
        <div class="arrow"></div>
        <ul>
            <li>Санкт-Петербург</li>
            <li>Москва</li>
            <li>Март</li>
            <li>Брянск</li>
            <li>Уфа</li>
            <li>Казань</li>
        </ul>
    </div>
</div>

</div>

<link href="/design/css/runbase.css" rel="stylesheet" type="text/css"/>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
<script type="text/javascript"
        src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="/design/js/google/config.js"></script>

<script type="text/javascript">
    var STYLES_MAP_RUNBASE = [
        { "featureType": "water", "stylers": [
            { "color": /*"#dbeefc"*/"#cfcec8" }
        ] },
        { "featureType": "road", "stylers": [
            { "color": /*"#cecdc8"*/"#f1f1f0" }
        ] },
        { "featureType": "transit.station.rail", "elementType": "labels.icon", "stylers": [
            { "color": "#6c695b" },
            { "invert_lightness": true }
        ] },
        { "featureType": "landscape.natural", "stylers": [
            { "color": /*"#f2fce4"*/"#ffffff" }
        ] },
        { "featureType": "landscape.man_made", "stylers": [
            { "color": "#fafaf9" }
        ] },
        { "featureType": "poi", "stylers": [
            { "color": "#f1f1f0" }
        ] },
        { "featureType": "administrative", "elementType": "labels", "stylers": [
            { "color": "#2e2a16" },
            { "visibility": "on" },
            { "saturation": 2 },
            { "weight": 0.1 }
        ] },
        { "featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [
            { "color": "#2e2a16" }
        ] }
    ];

    var map;
    var markers = [];
    var markerCluster = null;
    var isClickMarker = false;
    var overlay = {};

    var cities = [
        {name: 'Москва', lat: 55.752356, long: 37.6236278}
    ];

    var shops = [
        {name: '«Олимп»', lat: 55.7618758, long: 37.5669044, address: 'г. Москва, ул. Красная Пресня, 23 строение 1А', phone: '(495) 725-58-98<br/>(499) 252-44-12 (зал футбол)<br/>(499) 787-76-01', hours: '10:00&ndash;22:00'},
        {name: 'ТРЦ «Вегас»', lat: 55.5869562, long: 37.7250639, address: 'Московская область, Ленинский район, 24км МКАД', phone: '(495) 727-44-57 (касса)'},
        {name: '«МЕГА» Белая дача', lat: 55.654244, long: 37.844253, address: 'Московская область, г. Котельники, 1-ый Покровский пр-д, 5', phone: '(495) 660-16-03 (касса)<br/>(495) 660-16-02 (офис)'},
        {name: '«Золотой Вавилон»', lat: 55.8455692, long: 37.6649465, address: 'г. Москва, ул. Декабристов, 12', phone: '(495) 660-16-03 (касса)<br/>(495) 660-16-02 (офис)', hours: '8:00-0:00'},
        {name: '«МЕГА» Теплый Стан', lat: 55.6037, long: 37.4912, address: 'Московская область, 40км МКАД', phone: '(495) 933-74-50', hours: '10:00-23:00'},
        {name: 'ТЦ «Райкин Плаза»', lat: 55.797018, long: 37.618231, address: 'Москва, ул. Шереметьевская, 8, корп. 1'}
    ];

    function initialize() {
        $('section.layout div.limit').addClass('limit-width-fix');

        var mapSelector = 'boost-map';
        var mapDiv = document.getElementById(mapSelector);
        var mapOptions = {
            zoom: 10,
            scrollwheel: false,
            center: new google.maps.LatLng(cities[0].lat, cities[0].long),//Moscow,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            panControl: false,
            zoomControl: false
            //styles: STYLES_MAP_RUNBASE
        };
        map = new google.maps.Map(mapDiv, mapOptions);

        $.each(shops, function (index, shop) {
            var loc = new google.maps.LatLng(shop.lat, shop.long);

            var marker = new google.maps.Marker({
                position: loc,
                icon: 'http://adidas-running.ru/boost/images/flag.png',
                map: map,
                id: index
            });

            var content = '<div class="infobox-corner js-infobox-' + index + '"></div><div class="infobox"><div class="infobox-top">';

            content += '<div class="infobox-close" onclick="$(\'div.infoBox\').hide();"></div>';

            if (shop.name) {
                content += '<div class="infobox-name">' + shop.name + '</div>';
            }

            if (shop.address) {
                content += '<div class="infobox-address">' + shop.address + '</div>';
            }

            if (shop.phone) {
                content += '<div class="infobox-phone">' + shop.phone + '</div>';
            }

            content += '</div>';

            if (shop.hours) {
                content += '<div class="infobox-hours"><div class="infobox-hours-title">Часы работы:</div>' + shop.hours + '</div>';
            }

            content += '</div>';

            var infobox = new InfoBox({
                content: content,
                disableAutoPan: false,
                pixelOffset: new google.maps.Size(-50, 0),
                zIndex: null,
                closeBoxMargin: "28px 15px 0px 0px",
                closeBoxURL: "http://adidas-running.ru/boost/images/infobox-close.png",
                infoBoxClearance: new google.maps.Size(1, 1),
                isHidden: false
            });

            infobox.open(map, marker);

            google.maps.event.addListener(marker, 'click', function () {
                $('div.infoBox').hide();
                $('div.js-infobox-' + this.id).parent().show();
                map.panTo(loc);
            });
        });

//        $('div.combo li').click(function () {
//            alert($(this).text());
//        });

        initControl(map, false);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="boost-map-wrapper">
    <div class="boost-map-shadow"></div>
    <div class="boost-map-shadow-white"></div>
    <div id="boost-map"></div>
</div>

<div class="boost-social">
    <div class="limit" style="width:1200px !important">
        <div class="social_boxes">
            <div class="vk"><span></span>

                <div class="container">
                    <div id="vk_like2"></div>
                </div>
            </div>
            <div class="fb"><span></span>

                <div class="container">
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450"
                         data-show-faces="false"></div>
                </div>
            </div>
            <div class="tw"><span></span>

                <div class="container">
                    <div id="tw-button2"></div>
                </div>
            </div>
            <div class="gp"><span></span>

                <div class="container">
                    <div class="g-plusone" data-size="medium" data-callback="plusOne"></div>
                </div>
            </div>
            <p style="clear: both;"></p>
        </div>
        <div class="tags">
            <?if(count($this->data['tags'])):?>
            <div class="title">тэги ></div>
            <?php foreach($this->data['tags'] as $tag):?>
            <strong onclick="window.location='<?echo $this->url(array('tag'=>$tag), 'search_tag')?>'"><?php echo $tag?></strong>
            <?php endforeach;?>
            <?endif;?>
        </div>
        <div id="block_article_list_title" class="article_list_title">похожие статьи ></div>


        <script type="text/javascript">
            var similarPaginator = new Paginator('/articles/index/similar');
            similarPaginator.setParam('resource', <?php echo $this->data['id']?>)
            .setParam('jsPaginator', 'similarPaginator')
                    .setCallbackEndPaginator(function (currentPage, countPage) {
                        var ui = $('#similar_more');
                        if (currentPage >= countPage) {
                            ui.text('');
                        } else {
                            ui.text('мне нужно больше статей');
                        }
                        if (0 == countPage) {
                            $('#block_article_list_title').text('');
                        }
                    });
        </script>
        <div id="container" class="article_list">
            <?php echo $this->action('similar', 'index', 'articles', array('resource'=>$this->data['id'],
            'jsPaginator'=>'similarPaginator'));?>
            <div class="clear:both;"></div>
        </div>
        <div id="similar_more" class="more_article" onclick="similarPaginator.getMaterials();"></div>
        <div class="comment_area">
            <?php if(Zend_Auth::getInstance()->hasIdentity() && !$isDraftArticle):?>

            <form class="comment_point new_comment" onsubmit="return commentSubmit(this)">
                <div class="img_box">
                    <?php if(empty(Zend_Auth::getInstance()->getIdentity()->avatar)):?>
                    <img style="width: 130px;" src="/design/images/ava_default.png" alt="img"/>
                    <?php else:?>
                    <img src="<?php echo Zend_Auth::getInstance()->getIdentity()->avatar?>" alt="img"/>
                    <?php endif;?>
                </div>
                <textarea placeholder="Написать комментарий" name="text"></textarea>
                <input type="submit" value="опубликовать"/>

                <div style="clear: both;"></div>
                <script type="text/javascript">
                    $(function () {
                        $('.comment_point textarea').focusin(function () {
                            $(this).next().show();
                            $(this).css('min-height', '130px');
                        });
                        $('.comment_point textarea').focusout(function () {
                            if ('' == $(this).val()) {
                                $(this).next().hide();
                                $(this).css('min-height', '50px');
                            } else {
                                $(this).css('min-height', '130px');
                            }
                        });

                    });

                    function commentSubmit(obj) {
                        $(obj).find('textarea').css('min-height', '50px');
                        $(obj).find('textarea').next().hide();
                        addMaterialsComment(obj, <?php echo $this->data['id'] ?>);
                        return false;
                    }
                </script>
            </form>
            <?php endif;?>
            <?php echo $this->action('index', 'materials', 'comments', array('resource'=>$this->data['id'],
            'layout'=>'layouts/boost'));?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.twttr = (function (d, s, id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function (f) {
            t._e.push(f)
        } });
    }(document, "script", "twitter-wjs"));

    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (intentEvent) {
            materialVote( <? echo $this->data['id'] ?>,'tw');
        });
        twttr.widgets.createShareButton(
                window.location.href,
                document.getElementById('tw-button2')
        );
    });
    function plusOne(data) {
        if(data.state == "on") {
            materialVote(<?echo $this->data['id']?>, 'gp', 1);
        } else {
           materialVote(<?echo $this->data['id']?>, 'gp', 0);
        }
    }
    window.___gcfg = {lang: 'ru'};
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=460390354041042";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 1);
            }
        );
        FB.Event.subscribe('edge.remove',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 0);
            }
        );
    };
    $(function () {
        VK.Widgets.Like('vk_like', {type: "mini", height: 20});
        VK.Widgets.Like('vk_like2', {type: "mini", height: 20});
        VK.Observer.subscribe('widgets.like.liked', function(e){
           materialVote(<?echo $this->data['id']?>, 'vk', 1);
        });
        VK.Observer.subscribe('widgets.like.unliked', function(e){
           materialVote(<?echo $this->data['id']?>, 'vk', 0);
        });
    });
</script>
