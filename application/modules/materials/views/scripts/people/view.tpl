<div id="container">   
    <div class="element people" style="background-image: url(images/bg/bg4.png);">
        <a href="" class="photo"><img src="/design/images/img16.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <a href="" class="people_name">вадим янгиров</a>
        <a href="" class="title">пражский марафон</a>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="record_count">Всего записей: 5</div>   
        </div>
    </div>
         
    <div class="element article art_master">
        <a href=""><img src="/design/images/img1.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">Революция в беге</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                                              
        
    <div class="element article art_master">
        <a href=""><img src="/design/images/img2.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">ПРЕВРАТИ ЗАНЯТИЯ СПОРТОМ В НАСТОЯЩЕЕ УДОВОЛЬСТВИЕ</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div> 
    
    <div class="element article art_master">
        <a href=""><img src="/design/images/img4.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
             <a href="" class="title">Будь максимальный</a>
             <div class="short_text">Преврати занятия спортом в настоящее удовольствие — тренируйся на свежем воздухе!</div>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                        
    <div style="clear:both;"></div>
    </div>
    <div class="more_article blog">все записи</div>
    
    
    <!-- ++++++++++++++++++++++++ ARTICLE AREA ++++++++++++++++++++++++ -->
    <div class="sidebar_content blog">
    <div class="title_img">
        <img src="/design/images/img5.png" alt="img" class="main" />
        <img src="/design/images/article_img_bg.png" alt="img" class="lines" />
        <div class="user_name">вадим янгиров</div>
    </div>
    <h1 class="article_title">как я провел лето  в беге</h1>
    <div class="social_box">
        <img src="/design/images/likes.png" alt="img" />
        <div class="author">
            <img src="/design/images/img1.png" alt="img" />
            Андрей Кораблев
        </div>
    </div>
    <p>Конечно, если вы уже начали заниматься бегом – это уже большое достижение, которое совсем скоро принесет свои плоды. Наверняка друг или подруга, который разделяет ваши увлечения и следует вашим же утренним или вечерним маршрутом, уже рассказал вам о том, как нужно правильно бегать. Что ж, житейский опыт – хорошая штука, но воспользуемся мнением профессионалов. Итак, что же нужно знать для того, чтобы быть уверенным в правильности техники?</p>
    <div class="gallery_name">10 правил бега</div>
    <div id="simplegallery1"></div>
    <p>Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс. Более того, бег будет доведен до такого автоматизма, что тело само начнет действовать правильно.</p>
    <p>Не забывайте и о правильном дыхании: оно таит в себе те самые скрытые резервы, второе дыхание, которое однажды непременно откроется вам. И как бы вы ни желали поделиться ощущениями с другом, бегущим рядом с вами, отложите обмен опытом до окончания дистанции.</p>
    <div class="video_container">video_container</div>
    <p>Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс. Более того, бег будет доведен до такого автоматизма, что тело само начнет действовать правильно. Не забывайте и о правильном дыхании: оно таит в себе те самые скрытые резервы, второе дыхание, которое однажды непременно откроется вам. И как бы вы ни желали поделиться ощущениями с другом, бегущим рядом с вами, отложите обмен опытом до окончания дистанции.</p>
    <aside>
        <div class="element event current">
            <div class="event_container">  
                 <div class="day">13</div>
                 <div class="lines"></div>
                 <div class="month">июля</div>
                 <a  href="" class="title">пражский марафон</a>
            </div>
        </div>
        
        <div class="element goods">
            <a class="goods_title">Обувь для бега cc revolution w</a>
            <div class="img_box">
                <img src="/design/images/img8.png" alt="img" />
                <div class="hover_box">
                    <a href="">обзор</a>
                    <a href="">купить</a>
                </div>
            </div>
        </div>
        
        <div class="element goods">
            <a class="goods_title">Куртка STR RRUN JK</a>
            <div class="img_box">
                <img src="/design/images/img9.png" alt="img" />
                <div class="hover_box">
                    <a href="">обзор</a>
                    <a href="">купить</a>
                </div>
            </div>
        </div>
    </aside>
    <div style="clear: both;"></div>                        
    </div>
    <div class="social_boxes">
    <div class="vk"><span></span></div>
    <div class="fb"><span></span></div>
    <div class="tw"><span></span></div>
    <div class="gp"><span></span></div>
    <p style="clear: both;"></p>
    </div>
    <div class="tags">
    <div class="title">тэги ></div>
    <strong>Стадион</strong>
    <strong>Climacool</strong>
    <strong>miCoach</strong>
    <strong>Весна</strong>
    <strong>Ускорение</strong>
    </div>
    
    <!-- ++++++++++++++++++++++++ OTHER ARTICLE BY THIS USER++++++++++++++++++++++++ -->
    <div class="article_list_title">другие записи <span>вадима янгирова</span> ></div>
    <div id="container">        
    <div class="element article art_master">
        <a href=""><img src="/design/images/img1.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">Революция в беге</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                                              
        
    <div class="element route">
          <div class="route_map l289"></div>
          <div  class="route_length">28.9</div>
          <div class="lines"></div>
          <a href="" class="title">Маршрут</a>
          <div class="from_to">Парк Горького ><br /> Воробьевы горы</div>
          <div class="town">Набережная</div>
          <div class="sub_info">
              <div class="text">297 пробежали</div>
              <div class="comments"><span></span>14</div>
              <div class="likes"><span></span>50</div>      
          </div>
    </div>
    
    <div class="element article art_master">
        <a href=""><img src="/design/images/img4.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
             <a href="" class="title">Будь максимальный</a>
             <div class="short_text">Преврати занятия спортом в настоящее удовольствие — тренируйся на свежем воздухе!</div>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>
    
    <div class="element article art_master">
        <a href=""><img src="/design/images/img1.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">Революция в беге</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                        
    <div style="clear:both;"></div>
    </div>
    <div class="more_article blog">все записи</div>
    
    
    <!-- ++++++++++++++++++++++++ TOPIC ARTICLE ++++++++++++++++++++++++ -->
    <div class="article_list_title">статьи на эту тему ></div>
    <div id="container">        
    <div class="element article art_master">
        <a href=""><img src="/design/images/img1.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">Революция в беге</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                                              
        
    <div class="element route">
          <div class="route_map l289"></div>
          <div  class="route_length">28.9</div>
          <div class="lines"></div>
          <a href="" class="title">Маршрут</a>
          <div class="from_to">Парк Горького ><br /> Воробьевы горы</div>
          <div class="town">Набережная</div>
          <div class="sub_info">
              <div class="text">297 пробежали</div>
              <div class="comments"><span></span>14</div>
              <div class="likes"><span></span>50</div>      
          </div>
    </div>
    
    <div class="element article art_master">
        <a href=""><img src="/design/images/img4.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
             <a href="" class="title">Будь максимальный</a>
             <div class="short_text">Преврати занятия спортом в настоящее удовольствие — тренируйся на свежем воздухе!</div>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>
    
    <div class="element article art_master">
        <a href=""><img src="/design/images/img1.png" alt="img" class="main" /></a>
        <div class="lines"></div>
        <div class="animation_text">
          <a href="" class="title">Революция в беге</a>
        </div>
        <div class="sub_info">
            <div class="text">11 марта 2013</div>
            <div class="comments"><span></span>14</div>
            <div class="likes"><span></span>50</div>      
        </div>
    </div>                        
    <div style="clear:both;"></div>
    </div>
    <div class="more_article">мне нужно больше статей</div>
    
    
    <!-- ++++++++++++++++++++++++ COMMENT AREA ++++++++++++++++++++++++ -->
    <div class="comment_area">
    <form class="comment_point new_comment">
        <div class="img_box"><img src="/design/images/img1.png" alt="img" /></div>
        <textarea placeholder="Написать комментарий"></textarea>
        <input type="submit" value="опубликовать" />
        <div style="clear: both;"></div>
    </form>
    
    <div class="comment_point">
        <div class="img_box"><img src="/design/images/img1.png" alt="img" /></div>
        <div class="name">Алексей Мещанский</div>
        <div class="date">сегодня в 17:34</div>
        <div style="clear: both;"></div>
        <div class="comment_text">Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс.</div>
        
    </div>
    
    <div class="comment_point">
        <div class="img_box"><img src="/design/images/img2.png" alt="img" /></div>
        <div class="name">Алексей Мещанский</div>
        <div class="date">сегодня в 17:34</div>
        <div style="clear: both;"></div>
        <div class="comment_text">Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс. Не забывайте и о правильном дыхании: оно таит в себе те самые скрытые резервы, второе дыхание, которое однажды непременно откроется вам. Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. </div>
        
    </div>
    
    <div class="comment_point">
        <div class="img_box"><img src="/design/images/img3.png" alt="img" /></div>
        <div class="name">Алексей Мещанский</div>
        <div class="date">сегодня в 17:34</div>
        <div style="clear: both;"></div>
        <div class="comment_text">Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс. Понятно, что в самом начале следить за всей техникой сразу вряд ли получится. Однако со временем вы научитесь легко контролировать этот процесс. </div>
       
    </div>
</div>