<link href="/design/future/css/future.css" rel="stylesheet" type="text/css"/>
<div class="future-title">
	<h1><span>adidas running></span><br/>каким его <br/>
	видишь ты</h1>
</div>        
<div class="future-likes">
	<div class="future-likes-item">
		<script>
		    $(function () {
        VK.Widgets.Like('vk_like', {type: "mini", height: 20});
    });
		</script>
	    <div id="vk_like"></div>
	</div>
	<div class="future-likes-item">
	    <div id="fb-root"></div>
	    <script>(function (d, s, id) {
	            var js, fjs = d.getElementsByTagName(s)[0];
	            if (d.getElementById(id)) return;
	            js = d.createElement(s);
	            js.id = id;
	            js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
	            fjs.parentNode.insertBefore(js, fjs);
	        }(document, 'script', 'facebook-jssdk'));</script>
	    <div class="fb-like" data-href="http://www.adidas-running.ru/boost" data-send="false"
	         data-layout="button_count" data-width="450" data-show-faces="false"></div>
	</div>
	<div class="future-likes-item">
		<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</div>
	<div class="future-likes-item">
	    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	    <g:plusone size="medium"></g:plusone>
	</div>
</div>

<div class="future-step future-step-arrow">
	<div class="arr arr1"></div>
	<h1>полгода назад мы</br>
	создали беговой</br>
	портал <span>adidas-running.ru</span></h1>
	<p>
	чтобы рассказывать вам о соревнованиях, правилах тренировок и личном опыте спортсменов. Что-то получалось хорошо, что-то не очень, но мы рады видеть интерес со стороны бегового сообщества.</p>
</div>

<div class="future-step future-step-star">
	<div class="arr arr2"></div>
	<h1>теперь мы решили</br>
	обратиться к вам!</h1>
	<p>активным увлеченным бегунам, чтобы с вашей помощью <b>развиваться дальше</b> в правильном направлении.</p>
</div>

<div class="future-step future-step-messages">
	<div class="arr arr3"></div>
	<!-- <a href="http://www.surveygizmo.com/s3/1446175/Adidas-running" class="button" target="_blank"><span></span></a> -->
	<h1>присоединяйся</br>
	к краудсорсинг-</br>
	проекту</h1>

	<p>«<span>adidas running></span>, каким его видишь ты», делись мнениями, и выдвигай необычные решения поставленных задач. Мы готовы к глобальным переменам.</p>
</div>

<div class="future-step future-step-present">
	<h1>участника, чей вклад </br>
	в проект окажется самым ценным</h1>

	<p>компания adidas вознаградит поездкой в одну из мировых столиц на <b>марафон*</b>. Другие удачные  идеи будут отмечены ценными подарками &mdash; <b>часами Smart Run miCoach</b>.</p>

	<div class="disclaimer">
		*Главным спонсором марафона выступит компания adidas.</br>Место и дата марафона, а также сроки поездки будут определены организаторами проекта позднее. <br/>
		<a href="/files/crowdsourcing_rules.pdf" target="_blank" style="color:#8dc63f;font-size:15px;display:block;margin:10px 0 0 0;text-decoration:underline">Юридические правила и условия проекта</a>
	</div>
</div>