<div class="micoach_title"></div>

<div class="micoach_box">
    <div class="micoach_point">
        <div class="arrow"></div>
        <div class="img_box"><img src="/design/images/img20.png" alt="img" /></div>
        <div class="title">выбери экипировку и зарегистрируйся.</div>
        <div class="text">
            miCoach - это линейка устройств и программ, в которых используются новейшие разработки в области технологий отслеживания спортивных результатов.
            <div style="clear: both;"></div>
	    <a class="button" href="http://micoach.adidas.com/ru/ui/Product/#!/pacer" target="_blank">комплект pacer <span></span></a>
	    <br />
	    <a class="button" href="http://micoach.adidas.com/ru/ui/Product/#!/mobile" target="_blank">мобильное приложение<span></span></a>
        </div>                            
    </div>
    <div class="micoach_point">
        <img src="/design/images/promo_line.png" alt="img" class="promo_line left" />
        <img src="/design/images/promo_line.png" alt="img" class="promo_line right" />
        <div class="arrow"></div>
        <div class="img_box"><img src="/design/images/img22.png" alt="img" /></div>
        <div class="title">следи за своими спортивными результатами.</div>
        <div class="text">miCoach записывает информацию о твоих движениях и реакцию твоего организма во время тренировок или соревнований и помогает тебе составить Тренировочный План, основанный на целях и текущей физической подготовке. Все тренировки также сопровождаются аудио-инструкциями в режиме реального времени.</div>
    </div>
    <div class="micoach_point ie_last">
        <div class="arrow"></div>
        <div class="img_box"><img src="/design/images/img21.png" alt="img" /></div>
        <div class="title">онлайн синхронизация. сравни. сразись.</div>
        <div class="text">miCoach позволяет синхронизировать Ваши результаты, сравнивать и рассказывать о них друзьям онлайн - где бы ты ни был. Поэтому даже во время игры Вы можете рассказать всему миру о своих успехах на поле в прямом эфире.</div>
    </div>
    <div style="clear:both;"></div>
</div>
<?//echo $this->micoachPage->text?>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<div id="container" class="runbase-articles-container" style="margin: 0 0 0 -22px">
    <?= $this->action('blockmaterials', 'micoach', 'materials') ?> 
    <div style="clear: both;"></div> 
</div>
<script type="text/javascript">
    defaultPaginator = new Paginator('/materials/micoach/blockmaterials');
    function updateStrip(order) {
        defaultPaginator.resetPage();
        defaultPaginator.setParam('sort', order)
        defaultPaginator.getMaterials(true);
    }
    MenuNavigator.setCurrentMenu('<?echo $this->url(array(), 'micoach')?>');
</script>