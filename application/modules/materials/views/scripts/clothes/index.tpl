<div class="sort">
    <!--<div class="title">Сортировать</div>-->
    <ul>
         <li class="current" onclick="updateStrip('id')">Новые</li>
         <li onclick="updateStrip('rate')">Популярные</li>
    </ul>
</div>
<script type="text/javascript">
    MenuNavigator.setCurrentMenu('<?echo $this->url(array(), 'clothes')?>');
	defaultPaginator = new Paginator('/materials/clothes/blockmaterials');
	function updateStrip(order) {
		defaultPaginator.resetPage();
		defaultPaginator.setParam('sort', order)
		defaultPaginator.getMaterials(true);
	}

</script>
                     
                    
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
 
 
<div class="panel large_promo">
    <img src="/design/images/banner_left_lines.png" alt="img" class="banner_lines left" />
    <img src="/design/images/banner_right_lines.png" alt="img" class="banner_lines right" />
 	<div class="rotator_container">
        <div id="preview1" class="banner-rotator">
            <ul> 
                <? foreach($this->slider_banners as $banner): ?>
                    <li class="<?= !empty($banner['banner_color']) ? $banner['banner_color'] : 'black'; ?>">
                        <a href="<?= $banner['banner_image']; ?>"></a>     
                        <div class="lines_block"></div>                                                              
                    </li>
                <? endforeach ?>
            </ul>
        </div>	
  </div>    
</div> 


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<div id="container">  
    <?= $this->action('blockmaterials', 'clothes', 'materials')?>
</div>