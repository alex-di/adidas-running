<link href="/design/css/runbase.css" rel="stylesheet" type="text/css"/>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
<script src="/design/js/google/config.js"></script>

<script type="text/javascript">
    MenuNavigator.setCurrentMenu('<?= $this->url(array(), "runbase")?>');
    var STYLES_MAP_RUNBASE = [
        { "featureType": "water", "stylers": [
            { "color": /*"#dbeefc"*/"#cfcec8" }
        ] },
        { "featureType": "road", "stylers": [
            { "color": /*"#cecdc8"*/"#f1f1f0" }
        ] },
        { "featureType": "transit.station.rail", "elementType": "labels.icon", "stylers": [
            { "color": "#6c695b" },
            { "invert_lightness": true }
        ] },
        { "featureType": "landscape.natural", "stylers": [
            { "color": /*"#f2fce4"*/"#ffffff" }
        ] },
        { "featureType": "landscape.man_made", "stylers": [
            { "color": "#fafaf9" }
        ] },
        { "featureType": "poi", "stylers": [
            { "color": "#f1f1f0" }
        ] },
        { "featureType": "administrative", "elementType": "labels", "stylers": [
            { "color": "#2e2a16" },
            { "visibility": "on" },
            { "saturation": 2 },
            { "weight": 0.1 }
        ] },
        { "featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [
            { "color": "#2e2a16" }
        ] }
    ];

    var map;
    var polyline = new google.maps.Polyline();
    var markers = [];
    var markerCluster = null;
    var isClickMarker = false;
    var overlay = {};

    function initialize() {
        $('section.layout div.limit').addClass('limit-width-fix');
        //$('#runbase-map').width(scr).css('left', -((scr - 1160) / 2).toString() + 'px')

        var mapSelector = 'runbase-map';
        var mapDiv = document.getElementById(mapSelector);
        var mapOptions = {
            zoom: 16,
            scrollwheel: false,
            center: new google.maps.LatLng(55.721, 37.557),//Moscow,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            panControl: false,
            zoomControl: false,
            styles: STYLES_MAP_RUNBASE
        };
        map = new google.maps.Map(mapDiv, mapOptions);

        var iconBase = 'http://adidas-running.ru/design/images/runbase/contacts/';

        var marker1 = new google.maps.Marker({
            position: new google.maps.LatLng(55.72252, 37.562004),
            icon: iconBase + 'sport.png',
            map: map
        });

        var marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(55.719977, 37.560727),
            icon: iconBase + 'luj.png',
            map: map
        });

        var marker3 = new google.maps.Marker({
            position: new google.maps.LatLng(55.72143, 37.551),
            icon: iconBase + 'flag2.png',
            map: map
        });

        initControl(map, false);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>
    <div class="limit">
        <div class="runbase" style="min-height:0">
            <div class="sort runbase_sort">
              <ul>
                <li><a href="/runbase/events">runclub></a></li>
                <li><a href="/runbase">runbase&nbsp;></a></li>  
                <!-- <li><a href="/card">Как получить карту</a></li> -->
                <li class="current"><a href="/runbase/contacts">Контакты</a></li>
              </ul>
            </div>
        </div>
    </div>
</div>


<!-- <div class="runbase" style="min-height:0">
    <div class="runbase-menu">
        <a href="/runbase">runbase&nbsp;></a>
        <a href="/runbase/events">Расписание</a>
        <a href="/card">Как получить карту</a>
        <a href="#" class="active">Контакты</a>
    </div>
</div>
 -->
<div class="runbase-contacts">
    <div class="runbase-map-container">
        <div class="map-over"></div>
        <div id="runbase-map" class="runbase-map"></div>
    </div>

    <div class="runbase" style="min-height:0;position:relative">

        <div class="text">
            <div class="title">ул. лужники, д. 24, стр. 10</div>
            <div class="phone">звоните нам в будние дни с 11:00 до 18:00<br/>
                8 (495) 648-54-69</div>
            <p>runbase> находится на территории спортивного комплекса «Мультиспорт».<br/>
                Стадион «Лужники» — центральная часть Олимпийского комплекса «Лужники», расположенного неподалёку от
                Воробьёвых гор в Москве. Ближайшие станции метро — «Спортивная» и «Воробьёвы горы».</p>
        </div>
        <?php
            $isAnonymous = (Zend_Auth::getInstance()->getIdentity() !== null && isset(Zend_Auth::getInstance()->getIdentity()->id)) ? false : true;
            $isError = (isset($this->formError)) ? true : false;
        ?>
        <div class="send <?php echo ($isAnonymous) ? 'anonymous' : ''; echo ($isError) ? ' error' : ''; ?>">
            <h1>обратная связь</h1>
            <form action="/runbase/contacts/" method="post" id="sendContactsForm">
                <?php
                    if ($isError) {
                ?>
                <p class="errorText"><?php echo $this->formError; ?></p>
                <?php
                    }
                    if ($isAnonymous) {
                ?>
                <input type="email" name="email" id="email" onfocus="if($('#email').val()=='ваш e-mail'){$('#email').val('')}" onblur="if($('#email').val()==''){$('#email').val('ваш e-mail')}" value="ваш e-mail">
                <?php 
                    }
                ?>
                <textarea name="message" id="message" onfocus="if($('#message').val()=='ваше сообщение'){$('#message').val('')}" onblur="if($('#message').val()==''){$('#message').val('ваше сообщение')}">ваше сообщение</textarea>
                <a href="javascript:void(0)" id="sendContactsFormButton" class="button">отправить</a>
            </form>
            <script>
                $(document).ready(function(){
                    $('#sendContactsFormButton').on('click', function(){
                        var msg = $('#message').val();

                        if (msg.toLowerCase() == 'ваше сообщение') {
                            return false;
                        } else {
                            $('#sendContactsForm').submit();
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>