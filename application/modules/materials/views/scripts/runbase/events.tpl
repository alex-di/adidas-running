<link href="/design/css/runbase.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    MenuNavigator.setCurrentMenu('runbase/events');
</script>

<div class="limit">
    <div class="runbase">
        
        <!--
        <div class="runbase-menu">
            <a href="/runbase">runbase&nbsp;></a>
            <a href="#" class="active">Расписание</a>
            <a href="/card">Как получить карту</a>
            <a href="/runbase/contacts">Контакты</a>
        </div>
        -->
        
        <div class="sort runbase_sort">
          <ul>
            <li class="current"><a href="/runbase/events">runclub></a></li>
            <li><a href="/runbase">runbase&nbsp;></a></li>  
            <!-- <li><a href="/card">Как получить карту</a></li> -->
            <li><a href="/runbase/contacts">Контакты</a></li>
          </ul>
        </div>

<!--         <div class="notification"> 
            <h1>уважаемые бегуны!</h1>

            <p>Во время дождя занятия с тренерами проводиться не будут.</p>
        </div> -->
        <a href="http://adidas-running.ru/event/begovogo-kluba-adidas-runclub-moscow">
            <img src="http://adidas-running.ru/design/images/560x560_runclub.jpg" style="margin: 0 0 15px"/>
        </a>
    <div class="material_body">
    <p><strong>Что такое </strong><strong>adidas </strong><strong>runclub&gt;?</strong></p>
    <ul>
    <li>это сообщество, объединяющее любителей бега</li>
    <li> это отправная точка беговой тренировки при любой погоде вместе с квалифицированным тренером (бесплатно, три раза в неделю)</li>
    </ul>
    <p><strong>Какие преимущества  дает членство в </strong><strong>adidas </strong><strong>runclub</strong><strong>&gt;? </strong></p>
    <ul>
    <li>бонусная карта, позволяющая получать подарки за количество посещенных тренировок</li>
    <li>заранее подготовленный маршрут и пробежка под руководством профессионального тренера</li>
    </ul>
    <p><strong>В чем отличие между предыдущими тренировками на </strong><strong>runbase</strong><strong>&gt; и тренировками в </strong><strong>runclub</strong><strong>&gt;?</strong></p>
    <ul>
    <li>Ежедневные тренировки по расписанию  на runbase&gt; отменяются.  Для всех обладателей карты runner&gt; сохраняются возможность прохода на базу и бронь шкафчика в любое время.</li>
    <li> Новый формат тренировок с runclub&gt; — бег три раза в неделю (вторник, четверг и суббота) под руководством тренера и по заранее известному маршруту.  Все желающие также смогут заполнить заявку и получить карту runner&gt;. </li>
    <li>Теперь каждая тренировка ран клуба будет разделяться по уровню — одна группа начинающая, вторая— продвинутая. Они будут заниматься параллельно с двумя разным тренерами.</li>
    <li>Каждую субботу в runclub&gt;  вести тренировку будет англоговорящий тренер!</li>
    <li>Посещать тренировки сможет абсолютно каждый, вне зависимости от того есть у него карта runner&gt; или нет. В это время двери всегда будут открыты.</li>
    </ul>
    <p><strong>Как добраться до </strong><strong>adidas </strong><strong>runbase</strong><strong>&gt;?</strong></p>
    <p>Адрес беговой базы можно посмотреть <a href="http://adidas-running.ru/runbase/contacts" target="_blank">здесь</a>.</p>
    <p><strong>Где получить чеще больше информации о  adidas runclub&gt; moscow?</strong></p>
    <p>Больше информации ты найдешь, посетив <a href="http://vk.com/adidas_runclub_moscow" target="_blank">страницу официальной группы клуба ВКонтакте</a>. </p>
</div>

        <div class="runbase-events">
            <? if(count($this->events)){ ?>
            <? foreach($this->events as $event): ?>
            <?= $this->partial('partials/runbase_event.tpl', array('event' => $event, 'action' =>
            '/materials/runbase/reserve'))?>
            <? endforeach ?>
            <? }
            else{
                ?>
                <!-- <h2>Нет ближайших тренировок</h2> -->
                <?
            } ?>
            <div class="clearfix"></div>

        </div>
    </div>