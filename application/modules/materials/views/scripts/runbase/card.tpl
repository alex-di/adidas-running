<link href="/design/css/runbase.css" rel="stylesheet" type="text/css"/>

<div class="runbase-card">
    <script type="text/javascript">
        MenuNavigator.setCurrentMenu('<?= $this->url(array(), "runbase")?>');
    </script>

    <div class="limit">
        <div class="runbase">
            
            <!--
            <div class="runbase-menu">
                <a href="/runbase">runbase&nbsp;></a>
                <a href="/runbase/events">Расписание</a>
                <a href="#" class="active">Как получить карту</a>
                <a href="/runbase/contacts">Контакты</a>
            </div>
            -->
            
            <div class="sort runbase_sort">
              <ul>
                <li><a href="/runbase" class="runbase">runbase&nbsp;></a></li>
                <li><a href="/runbase/events">Расписание</a></li>
                <li class="current"><a href="#">Как получить карту</a></li>
                <li><a href="/runbase/contacts">Контакты</a></li>
              </ul>
            </div>
        </div>
    </div>

    <div class="runbase-card-social">
        <div id="contentEnergy__likes_vk">
            <!-- Put this script tag to the <head> of your page -->
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?97"></script>

            <script type="text/javascript">
                VK.init({apiId: 3766513, onlyWidgets: true});
            </script>

            <!-- Put this div tag to the place, where the Like block will be -->
            <div id="vk_like"></div>
            <script type="text/javascript">
                VK.Widgets.Like("vk_like", {type: "mini"});
            </script>
        </div>
        <div id="contentEnergy__likes_fb">
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like" data-href="http://www.adidas-running.ru/energy" data-send="false"
                 data-layout="button_count" data-width="450" data-show-faces="false"></div>
        </div>
        <div id="contentEnergy__likes_twi">
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="Заряди город энергией бега"
               data-via="adidasRU" data-lang="en" data-related="adidasRU">Tweet</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
        </div>
        <div id="contentEnergy_likes_google">
            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
            <g:plusone size="medium"></g:plusone>
        </div>
    </div>

    <div class="runbase-card-promo">
        <div class="text">
            <p>карта бегуна runbase> <br/>&mdash; это знак твоей принадлежности <br/>к беговому сообществу</p>
        </div>
    </div>
</div>

<div class="card-features">
    <div class="card-feature feature-1">
        <div class="title">ключ от твоих<br/> возможностей</div>
        <p>Наслаждайся уютной раздевалкой, индивидуальными электронными шкафчиками и душевыми, питьевой водой и
            бесплатным Wi-Fi.</p>
    </div>

    <div class="card-feature feature-2">
        <div class="title">индикатор<br/> достижений</div>
        <p>Сравнивай свои показатели после каждой пробежки и делись лучшими из них в социальных сетях.</p>
    </div>

    <div class="card-feature feature-3">
        <div class="title">пропуск<br/> в мир бега</div>
        <p><a href="http://adidas-running.ru/auth/register">Зарегистрируйся</a> на сайте и общайся с продвинутыми
            бегунами. Возможно, именно эти люди
            помогут тебе двигаться в нужном направлении!</p>
    </div>

    <div class="card-feature feature-4">
        <div class="title">эксклюзивные<br/> предложения</div>
        <p>Специально для владельцев карт runbase&gt; мы&nbsp;подготовили ряд интересных спецпредложений на&nbsp;покупку
            беговой экипировки.</p>
    </div>

    <div class="card-feature feature-5">
        <div class="title">билеты на самые громкие<br/> события</div>
        <p>Ежегодно мы&nbsp;проводим и&nbsp;спонсируем десятки беговых мероприятий. С&nbsp;картой runbase&gt; ты&nbsp;всегда
            в&nbsp;списке желанных гостей!</p>
    </div>

    <div class="card-feature feature-6">
        <div class="title">двигайся<br/>вперед</div>
        <p>И&nbsp;помни, что ты&nbsp;можешь получить карту coach&gt; или inspirator&gt;, если будешь активно двигаться
            вперёд! Для людей с&nbsp;такими картами мы&nbsp;подготовили ещё больше неожиданных сюрпризов!</p>
    </div>

    <div class="clear"></div>
</div>

<div class="runbase-get-card">
    <div class="runbase-get-card-left">
        <h2>как получить карту?</h2>

        <script type="text/javascript" src="//vk.com/js/api/openapi.js?101"></script>


        <ul>
            <li>
                Вступи в&nbsp;одно из&nbsp;сообществ adidas Running или <a
                        href="http://adidas-running.ru/auth/register">зарегистрируйся</a> на&nbsp;сайте
                adidas-running.ru
            </li>

            <li>
                <div id="vk_groups"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 0, width: "450", height: "170", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 50263349);
                </script>
            </li>

            <li>
                <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FadidasRunning&amp;width=450&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=361115304016465"
                        scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:258px;"
                        allowTransparency="true"></iframe>
            </li>

            <li> Следи за&nbsp;беговыми событиями. На&nbsp;самых интересных из&nbsp;них будут раздаваться флаеры на&nbsp;получение
                индивидуальных карт runner&gt; </li>

            <li style="margin: 10px 0 32px"> Приди с&nbsp;флаером на&nbsp;adidas runbase&gt;, заполни анкету и&nbsp;получи
                свою карту
            </li>

            <li style="text-transform: uppercase">
                В&nbsp;течение недели твоя карта будет активирована и&nbsp;привязана к&nbsp;учётной записи на&nbsp;сайте
            </li>
        </ul>
        <!-- VK Widget -->


    </div>

    <div class="runbase-get-card-right">
        <h2>как привязать карту к&nbsp;профилю к&nbsp;социальных сетях?</h2>

        <p class="grey">
            Если ты&nbsp;хочешь делиться беговыми статусами со&nbsp;своими друзьями, то&nbsp;не&nbsp;забудь привязать к&nbsp;карте
            свои профили в&nbsp;социальных сетях.
        </p>
        <ul style="margin: 40px 0 0">
            <li style="margin: 10px 0 34px">При регистрации или входе на&nbsp;сайт выбери одну из&nbsp;социальных
                сетей
            </li>
            <li>Пройди в&nbsp;свой личный кабинет, кликни по&nbsp;кнопке «Редактировать» и&nbsp;управляй своими
                социальными сетями
            </li>
        </ul>
    </div>
    <div class="clearfix" style="clear:both"></div>
</div>
