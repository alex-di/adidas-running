<div class="limit">
    <div class="runbase">
        
        <div class="sort runbase_sort">
          <ul>
            <li><a href="/runbase/events">runclub></a></li>
            <li class="current"><a href="#">runbase&nbsp;></a></li>  
            <!-- <li><a href="/card">Как получить карту</a></li> -->
            <li><a href="/runbase/contacts">Контакты</a></li>
          </ul>
        </div>

        <div class="runbase-slider-pages-container">
            <div class="runbase-slider-pages">
                <div class="runbase-slider-page active js-runbase-slider-page1" data-page="1">1</div>                    
                <div class="runbase-slider-page-arrow">></div>                     
                <div class="runbase-slider-page js-runbase-slider-page2" data-page="2">2</div>
                <div class="runbase-slider-page-arrow">></div>           
                <div class="runbase-slider-page js-runbase-slider-page3" data-page="3">3</div> 
                <div class="runbase-slider-page-arrow">></div>           
                <div class="runbase-slider-page js-runbase-slider-page4" data-page="4">4</div> 
                <div class="runbase-slider-page-arrow">></div>           
                <div class="runbase-slider-page js-runbase-slider-page5" data-page="5">5</div>
                <div class="clearfix"></div>
            </div>    
        </div> 

        <script type="text/javascript">
        $(function(){
            var currentSlide = 1;
            var shouldGoNextSlide = true;
            $('div.runbase-slider-page').click(function(){
                shouldGoNextSlide = false;
                goToSlide($(this).data('page'), true);
            });

            function goToSlide(slide, animate){
                var width = $('.runbase-slider-item:first').width();
                var margin = width*(slide-1);
                if(animate){
                     $('div.runbase-slider').animate({'margin-left': '-'+margin+'px'}, 500);
                    currentSlide = slide;
                    $('div.runbase-slider-page').removeClass('active');
                    $('div.js-runbase-slider-page'+slide).addClass('active');               
                } else {
                    $('div.runbase-slider').css({'margin-left': '-'+margin+'px'});
                }

            }

            function goNextSlide(){
                var nextSlide = currentSlide+1;
                if(nextSlide > 5)
                    nextSlide = 1;
                
                if(shouldGoNextSlide)                        
                    goToSlide(nextSlide, true);
                if(shouldGoNextSlide)                    
                    setTimeout(goNextSlide, 5000);
            }        

            $(window).resize(function(){
                goToSlide(currentSlide, false);
            });

            setTimeout(goNextSlide, 10000);
        });
        </script>

        <div class="runbase-slider-container">
            <div class="runbase-slider">
                <div class="runbase-slider-item">
                    <div class="runbase-slide-text">
                        <img src="/design/images/runbase/index/text.png" style="width:100%"/>
                    </div>
                </div>
                <div class="runbase-slider-item">
                     <div class="runbase-promo">
                        <div class="promo-text">
                            <?= $this->settings['runbase_promo_text'] ?>
                        </div>

                        <div class="promo-features">
                            <div class="promo-feature">
                                <?= $this->settings['runbase_promo_features_1'] ?>
                            </div>
                            <div class="promo-feature">
                                <?= $this->settings['runbase_promo_features_2'] ?>
                            </div>
                            <div class="promo-feature">
                                <?= $this->settings['runbase_promo_features_3'] ?>
                            </div>
                            <div class="promo-feature">
                                <?= $this->settings['runbase_promo_features_4'] ?>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>     
                </div>
                <div class="runbase-slider-item">
                    <div class="runbase-slide1"></div>
                </div>
                <div class="runbase-slider-item">
                    <div class="runbase-slide2"></div>
                </div>
                  <div class="runbase-slider-item">
                    <div class="runbase-slide3"></div>
                </div>              
            </div>     
        </div> 
      
        <?if(isset($this->event)):?>
            <div class="runbase-event">
                <div class="date-time">
                    <div class="number"><?= date('d', strtotime($this->event['date_start_event'])); ?></div>
                    <div class="month"><?= DR_Api_Tools_Tools::rusMonth($this->event['date_start_event']); ?></div>
                    <div class="dow"><?= DR_Api_Tools_Tools::rusDayOfWeek($this->event['date_start_event']); ?></div>
                    <div class="time"><?= date('H:i', strtotime($this->event['date_start_event']))?></div>
                </div>
            
                <div class="title">
                    <?= $this->event['name'] ?>
                </div>
            
                <div class="name">
                    <?= $this->event['author']?>
                </div>
            </div>
        <?endif;?>
        <form id="register_event_runbase" onsubmit="return jphp('/materials/runbase/reserve', this);" >
        <input name="runbase_event_id" value="1" type="hidden" />
        
        <div class="runbase-register">
            <div class="header">регистрация на runbase></div>
        
            <div class="date-time">
                <div class="combo length">
                    <div class="current">Дата</div>
                    <div class="arrow"></div>
                    <ul id="day_start_event">
                        <? 
                        $isNextDay = intval(date('H')) >= 19;
                        for($i = 0; $i<14; $i++): 
                            if($isNextDay) {
                                $isNextDay = false;
                                continue;
                            }
                        ?>
                            <li data-id="<?= date('Y-m-d', strtotime("+ $i days"))  ?>"><?= date('d', strtotime("+ $i days")) ?> <?= DR_Api_Tools_Tools::rusMonth(date('Y-m-d H:i:s', strtotime("+ $i days"))); ?></li>
                        <? endfor ?>
                        
                    </ul>
                    <input type="text" name="day_start_event" value=""/>
                </div>

                <div class="combo length">
                    <div class="current">Время</div>
                    <div class="arrow"></div>
                    <ul id="time_start_event">
                        <? for($i = 0; $i<13; $i++): ?>
                            <li data-id="<?= 7+$i  ?>:00"><?= 7+$i  ?>:00-<?= 10+$i  ?>:00</li>
                        <? endfor ?>
                        
                    </ul>
                    <input type="text" name="time_start_event" value=""/>
                </div>
            </div>

            <input class="button" type="submit" value="зарегистрироваться"/>
        </div>
        </form>
        <script type="text/javascript">
            $(function(){
               $('[name="day_start_event"]').change(function(){
                    console.log($(this).val());
                    var temp = new Date().toJSON().split("T"); 
                    var currentDate = temp[0];
                    var ui = $('#time_start_event').parent();
                    
                    ui.find('.current').text('Время');
                    ui.find('[name="time_start_event"]').val('');
                    if(currentDate == $(this).val()) {
                        var time = "<?echo date('H:i', time() + 60 * 60)?>";
                        ui.find('li').each(function() {
                            var temp = $(this).data('id').split(':');
                            var selectTime = (temp[0] < 10 ? "0" : '') + temp[0] + ':00'; 
                            if(selectTime > time) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    } else {
                        ui.find('li').show();
                    }
               }); 
            });
        </script>
        <div class="clearfix"></div>
        
        <div id="container" class="runbase-articles-container" style="margin: 0 0 0 -22px">
            <?= $this->action('blockmaterials', 'runbase', 'materials')?>  
            <div style="clear: both;"></div> 
        </div>
        <script type="text/javascript">
            defaultPaginator = new Paginator('/materials/runbase/blockmaterials');
            function updateStrip(order) {
                defaultPaginator.resetPage();
                defaultPaginator.setParam('sort', order)
                defaultPaginator.getMaterials(true);
            }
            MenuNavigator.setCurrentMenu('<?echo $this->url(array(), 'runbase')?>');
        </script>

    </div>
</div>

<script>
    function reserve(id) {
        $.post('/materials/runbase/reserve', {}, function(data){
            
        });
    }    
</script>