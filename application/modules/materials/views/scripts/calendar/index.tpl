<div class="limit"> 
<div class="calendar"> 

<h1 class="title">
	календарь беговых событий&nbsp;в&nbsp;мире на&nbsp;2013&nbsp;год
</h1>

<div class="calendar-likes">
    <div class="calendar-likes-item">
        <div id="vk_like"></div>
    </div>
    <div class="calendar-likes-item">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-href="http://www.adidas-running.ru/boost" data-send="false"
             data-layout="button_count" data-width="450" data-show-faces="false"></div>
    </div>
    <div class="calendar-likes-item">
        <a href="https://twitter.com/share" class="twitter-share-button"
           data-text="Заряди город энергией бега" data-via="adidasRU" data-lang="en"
           data-related="adidasRU">Tweet</a>
    </div>
    <div class="calendar-likes-item">
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
        <g:plusone size="medium"></g:plusone>
    </div>
</div>


<div class="calendar-month">
	<h1>октябрь</h1>
	<div class="calendar-days">
		<div class="calendar-day">
			<h2>03</h2>
			<h3>Jungle Marathon</h3>
            <img src="design/calendar/images/flags/brasil.png" class="flag" width="34" height="22"/>
			<div class="calendar-place">Пара, Бразилия</div>
			<div class="calendar-distance">Дистанции: <b>марафон</b>, <b>120</b>&nbsp;км, <b>240</b>&nbsp;км</div>
		</div>

        <div class="calendar-day">
            <h2>06</h2>
            <h3>Brussels Marathon</h3>
            <img src="design/calendar/images/flags/belgium.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Брюссель, Бельгия</div>
            <div class="calendar-distance">Дистанции: <b>1</b>&nbsp;км забег для детей, <b>4</b>&nbsp;км забег для мужчин, <b>4</b>&nbsp;км забег для&nbsp;леди, <b>полумарафон, марафон</b></div>
        </div>

        <div class="calendar-day">
            <h2>06</h2>
            <h3>Белоцерковский марафон 2013</h3>
            <img src="design/calendar/images/flags/ukraine.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Белая Церковь, Украина</div>
            <div class="calendar-distance">Дистанции: <b>10</b>&nbsp;км, <b>марафон</b></div>
        </div>

        <div class="calendar-day">
            <h2>13</h2>
            <h3>Budapest Marathon</h3>
            <img src="design/calendar/images/flags/vengria.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Будапешт, Венгрия</div>
            <div class="calendar-distance">Дистанции: <b>3.5</b>&nbsp;км, <b>минимарафон 7,5</b>&nbsp;км, <b>30</b>&nbsp;км, <b>марафон</b></div>
        </div>

        <div class="calendar-day">
            <h2>13</h2>
            <h3>Cologne Marathon</h3>
            <img src="design/calendar/images/flags/germany.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Кельн, Германия</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>      
  
        <div class="calendar-day">
            <h2>13</h2>
            <h3>Чикагский Марафон</h3>
            <img src="design/calendar/images/flags/usa.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Чикаго, США</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>        

        <div class="calendar-day">
            <h2>20</h2>
            <h3>Amsterdam Marathon</h3>
            <img src="design/calendar/images/flags/holland.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Амстердам, Голландия</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>   

        <div class="calendar-day">
            <h2>20</h2>
            <h3>Haile Gebrselassie Marathon</h3>
            <img src="design/calendar/images/flags/ethiopia.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Аваса, Эфиопия</div>
            <div class="calendar-distance">Дистанции: <b>детский забег</b>, <b>1</b>&nbsp;миля, <b>5</b>&nbsp;км, <b>полумарафон</b> и&nbsp;<b>марафон</b></div>
        </div>  

        <div class="calendar-day">
            <h2>20</h2>
            <h3>Morgenpost Dresden Marathon</h3>
            <img src="design/calendar/images/flags/germany.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Дрезден, Германия</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>  

        <div class="calendar-day">
            <h2>27</h2>
            <h3>Frankfurt Marathon</h3>
            <img src="design/calendar/images/flags/germany.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Франкфурт, Германия</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>     

        <div class="calendar-day">
            <h2>29</h2>
            <h3>Dublin Marathon</h3>
            <img src="design/calendar/images/flags/ireland.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Дублин, Ирландия</div>
            <div class="calendar-distance">Дистанции: <b>5</b>&nbsp;миль, <b>10</b>&nbsp;миль, <b>10</b>&nbsp;км и&nbsp;<b>полумарафон</b></div>
        </div>   

		<div style="clear:both"></div>
	</div>
</div>

<div class="calendar-month">
    <h1>ноябрь</h1>
    <div class="calendar-days">
        <div class="calendar-day">
            <h2>03</h2>
            <h3>Нью-Йоркский Марафон</h3>
            <img src="design/calendar/images/flags/usa.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Нью-Йорк, США</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>

        <div class="calendar-day">
            <h2>03</h2>
            <h3>adidas Auckland Marathon</h3>
            <img src="design/calendar/images/flags/new-zeland.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Окленд, Новая Зеландия</div>
            <div class="calendar-distance">Дистанции: <b>детский марафон</b>, <b>5</b>&nbsp;км, <b>полумарафон</b></div>
        </div>

        <div class="calendar-day">
            <h2>10</h2>
            <h3>Marathon des Alpes-Maritimes</h3>
            <img src="design/calendar/images/flags/france.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Ницца, Франция</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div> 

        <div class="calendar-day">
            <h2>11</h2>
            <h3>Athens Marathon</h3>
            <img src="design/calendar/images/flags/greece.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Афины, Греция</div>
            <div class="calendar-distance">Дистанции: <b>5</b>&nbsp;км, <b>10</b>&nbsp;км, <b>марафон</b></div>
        </div>   

        <div class="calendar-day">
            <h2>17</h2>
            <h3>17-й Полумарафон Булонь-Бийанкур</h3>
            <img src="design/calendar/images/flags/france.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Париж, Франция</div>
            <div class="calendar-distance">Дистанции: <b>полумарафон</b></div>
        </div> 

        <div class="calendar-day">
            <h2>17</h2>
            <h3>Istanbul Eurasia Marathon</h3>
            <img src="design/calendar/images/flags/turkey.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Стамбул, Турция</div>
            <div class="calendar-distance">Дистанции: <b>8</b>&nbsp;км, <b>15</b>&nbsp;км, <b>марафон</b></div>
        </div>  

         <div class="calendar-day">
            <h2>20</h2>
            <h3>Antarctic Ice Marathon</h3>
            <img src="design/calendar/images/flags/pole.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Южный Полярный Круг</div>
            <div class="calendar-distance">Дистанции: <b>полумарафон</b>, <b>марафон</b>, <b>100</b>&nbsp;км</div>
        </div>                              

         <div class="calendar-day">
            <h2>21</h2>
            <h3>Cyprus International 4-day Challenge 2013</h3>
            <img src="design/calendar/images/flags/cyprus.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Кипр</div>
            <div class="calendar-distance">Дистанции: <b>5</b>&nbsp;км, <b>10</b>&nbsp;км, <b>полумарафон</b></div>
        </div>    
        <div style="clear:both"></div>
    </div>
</div>


<div class="calendar-month">
    <h1>декабрь</h1>
    <div class="calendar-days">
        <div class="calendar-day">
            <h2>08</h2>
            <h3>Honolulu Marathon</h3>
            <img src="design/calendar/images/flags/hawai.png" class="flag" width="34" height="22"/>
            <div class="calendar-place">Гонолулу, Гавайи</div>
            <div class="calendar-distance">Дистанции: <b>марафон</b></div>
        </div>
        <div style="clear:both"></div>
    </div>
</div>


</div>
</div>

<div class="calendar-social">
    <div class="limit">
        <div class="social_boxes">
            <div class="vk"><span></span>

                <div class="container">
                    <div id="vk_like2"></div>
                </div>
            </div>
            <div class="fb"><span></span>

                <div class="container">
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450"
                         data-show-faces="false"></div>
                </div>
            </div>
            <div class="tw"><span></span>

                <div class="container">
                    <div id="tw-button2"></div>
                </div>
            </div>
            <div class="gp"><span></span>

                <div class="container">
                    <div class="g-plusone" data-size="medium" data-callback="plusOne"></div>
                </div>
            </div>
            <p style="clear: both;"></p>
        </div>
        <div class="tags">
            <?if(count($this->data['tags'])):?>
            <div class="title">тэги ></div>
            <?php foreach($this->data['tags'] as $tag):?>
            <strong onclick="window.location='<?echo $this->url(array('tag'=>$tag), 'search_tag')?>'"><?php echo $tag?></strong>
            <?php endforeach;?>
            <?endif;?>
        </div>
        <div id="block_article_list_title" class="article_list_title">похожие статьи ></div>


        <script type="text/javascript">
            var similarPaginator = new Paginator('/articles/index/similar');
            similarPaginator.setParam('resource', <?php echo $this->data['id']?>)
            .setParam('jsPaginator', 'similarPaginator')
                    .setCallbackEndPaginator(function (currentPage, countPage) {
                        var ui = $('#similar_more');
                        if (currentPage >= countPage) {
                            ui.text('');
                        } else {
                            ui.text('мне нужно больше статей');
                        }
                        if (0 == countPage) {
                            $('#block_article_list_title').text('');
                        }
                    });
        </script>
        <div id="container" class="article_list">
            <?php echo $this->action('similar', 'index', 'articles', array('resource'=>$this->data['id'],
            'jsPaginator'=>'similarPaginator'));?>
            <div class="clear:both;"></div>
        </div>
        <div id="similar_more" class="more_article" onclick="similarPaginator.getMaterials();"></div>
        <div class="comment_area">
            <?php if(Zend_Auth::getInstance()->hasIdentity() && !$isDraftArticle):?>

            <form class="comment_point new_comment" onsubmit="return commentSubmit(this)">
                <div class="img_box">
                    <?php if(empty(Zend_Auth::getInstance()->getIdentity()->avatar)):?>
                    <img style="width: 130px;" src="/design/images/ava_default.png" alt="img"/>
                    <?php else:?>
                    <img src="<?php echo Zend_Auth::getInstance()->getIdentity()->avatar?>" alt="img"/>
                    <?php endif;?>
                </div>
                <textarea placeholder="Написать комментарий" name="text"></textarea>
                <input type="submit" value="опубликовать"/>

                <div style="clear: both;"></div>
                <script type="text/javascript">
                    $(function () {
                        $('.comment_point textarea').focusin(function () {
                            $(this).next().show();
                            $(this).css('min-height', '130px');
                        });
                        $('.comment_point textarea').focusout(function () {
                            if ('' == $(this).val()) {
                                $(this).next().hide();
                                $(this).css('min-height', '50px');
                            } else {
                                $(this).css('min-height', '130px');
                            }
                        });

                    });

                    function commentSubmit(obj) {
                        $(obj).find('textarea').css('min-height', '50px');
                        $(obj).find('textarea').next().hide();
                        addMaterialsComment(obj, <?php echo $this->data['id'] ?>);
                        return false;
                    }
                </script>
            </form>
            <?php endif;?>
            <?php echo $this->action('index', 'materials', 'comments', array('resource'=>$this->data['id'],
            'layout'=>'layouts/springblade'));?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.twttr = (function (d, s, id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function (f) {
            t._e.push(f)
        } });
    }(document, "script", "twitter-wjs"));
    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (intentEvent) {
            materialVote( <? echo $this->data['id'] ?>,'tw');
        });
        twttr.widgets.createShareButton(
                window.location.href,
                document.getElementById('tw-button2')
        );
    });
    function plusOne(data) {
        if(data.state == "on") {
            materialVote(<?echo $this->data['id']?>, 'gp', 1);
        } else {
           materialVote(<?echo $this->data['id']?>, 'gp', 0);
        }
    }
    window.___gcfg = {lang: 'ru'};
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=460390354041042";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 1);
            }
        );
        FB.Event.subscribe('edge.remove',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 0);
            }
        );
    };
    $(function () {
        VK.Widgets.Like('vk_like', {type: "mini", height: 20});
        VK.Widgets.Like('vk_like2', {type: "mini", height: 20});
        VK.Observer.subscribe('widgets.like.liked', function(e){
           materialVote(<?echo $this->data['id']?>, 'vk', 1);
        });
        VK.Observer.subscribe('widgets.like.unliked', function(e){
           materialVote(<?echo $this->data['id']?>, 'vk', 0);
        });
    });
</script>
