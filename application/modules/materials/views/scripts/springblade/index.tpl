﻿<link href="/design/css/springblade/springblade.css?<?= rand(0,10000) ?>" rel="stylesheet" type="text/css"/>

<section class="springblade-top">
    <div class="springblade-promo limit">
        <div class="springblade-menu">
            <a href="#springblade-map">где купить</a>
            <a href="http://www.adidas.ru/muzhchiny-springblade#">для мужчин</a>
            <a href="http://www.adidas.ru/zhenschiny-springblade">для женщин</a>
        </div>
        <div class="explosive">
            взрывная&nbsp;энергия
        </div>

        <div class="springblade-likes">
            <div class="springblade-likes-item">
                <div id="vk_like"></div>
            </div>
            <div class="springblade-likes-item">
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-like" data-href="http://www.adidas-running.ru/boost" data-send="false"
                     data-layout="button_count" data-width="450" data-show-faces="false"></div>
            </div>
            <div class="springblade-likes-item">
                <a href="https://twitter.com/share" class="twitter-share-button"
                   data-text="Заряди город энергией бега" data-via="adidasRU" data-lang="en"
                   data-related="adidasRU">Tweet</a>
            </div>
            <div class="springblade-likes-item">
                <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                <g:plusone size="medium"></g:plusone>
            </div>
        </div>

        <div class="springblade-text">
            <p>В&nbsp;течение шести лет отдел инновационных технологий компании adidas искал форму, способную
                трансформировать в&nbsp;силу отталкивания энергию каждого шага.</p>

            <p>Решением стали кроссовки Springblade, экстремальный дизайн которых позволяет бегуну произвести революцию
                в&nbsp;тренировках.</p>

            <p>Новинка наделена подошвой с&nbsp;шестнадцатью обособленными прозрачными пластинами из&nbsp;высокопрочного
                полимера. Толщина, геометрия и&nbsp;расположение пластин рассчитаны так, чтобы обеспечивать эффективный
                возврат энергии и&nbsp;стабильный бег на&nbsp;любой поверхности и&nbsp;в&nbsp;любых климатических
                условиях. </p>
        </div>

        <div class="springblade-video">
            <iframe width="560" height="315" src="//www.youtube.com/embed/ngUrBm4G1aM" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>

    <div class="springblade-features limit">
        <div class="springblade-feature" style="width:260px;top:520px;left:0">
            <h3>износостойкий<br/>
                материал TechFit</h3>
            создает эффект «второй кожи»
        </div>

        <div class="springblade-feature" style="width:265px;top:285px;left:30px">
            <h3>эластичный сетчатый<br/> материал</h3>
            принимает форму стопы и&nbsp;улучшает циркуляцию воздуха
        </div>

        <div class="springblade-feature" style="width:240px;top:20px;left:660px">
            <h3>защитная внешняя<br/>
                вставка на пятке</h3>
            фиксирует стопу и&nbsp;повышает устойчивость
        </div>

        <div class="springblade-feature" style="width:325px;top:615px;left:685px">
            <h3>16 обособленных энергопластин</h3>
            накапливают энергию шага и&nbsp;трансформируют ее&nbsp;в&nbsp;силу отталкивания
        </div>

        <div class="springblade-feature" style="width:315px;top:170px;left:740px">
            <h3>термостойкость Springblade</h3>
            в 7 раз превышает термостойкость традиционной подошвы adidas из EVA
        </div>
    </div>

    <div class="limit">
        <div class="" style="position: relative;margin: 0 0 30px 0; "><a name="springblade-map"></a>

            <div class="title" style="display:inline">
                где купить
            </div>

            <div class="combo town" style="width:250px;float:right;margin: 0 40px 0 0;display:none">
                <div class="current">Москва</div>
                <div class="arrow"></div>
                <ul>
                    <li>Санкт-Петербург</li>
                    <li>Москва</li>
                    <li>Март</li>
                    <li>Брянск</li>
                    <li>Уфа</li>
                    <li>Казань</li>
                </ul>
            </div>
        </div>
    </div>

    <link href="/design/css/runbase.css" rel="stylesheet" type="text/css"/>
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
    <script type="text/javascript"
            src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
    <script src="/design/js/google/config.js"></script>

    <script type="text/javascript">
        var map;
        var markers = [];
        var markerCluster = null;
        var isClickMarker = false;
        var overlay = {};

        var cities = [
            {name: 'Москва', lat: 55.752356, long: 37.6236278}
        ];

        var shops = [
            {name: '«Олимп»', lat: 55.7618758, long: 37.5669044, address: 'г. Москва, ул. Красная Пресня, 23 строение 1А', phone: '(495) 725-58-98<br/>(499) 252-44-12 (зал футбол)<br/>(499) 787-76-01', hours: '10:00&ndash;22:00'},
            {name: 'ТРЦ «Вегас»', lat: 55.5869562, long: 37.7250639, address: 'Московская область, Ленинский район, 24км МКАД', phone: '(495) 727-44-57 (касса)'},
            {name: '«МЕГА» Белая дача', lat: 55.654244, long: 37.844253, address: 'Московская область, г. Котельники, 1-ый Покровский пр-д, 5', phone: '(495) 660-16-03 (касса)<br/>(495) 660-16-02 (офис)'},
            {name: '«Золотой Вавилон»', lat: 55.8455692, long: 37.6649465, address: 'г. Москва, ул. Декабристов, 12', phone: '(495) 660-16-03 (касса)<br/>(495) 660-16-02 (офис)', hours: '8:00-0:00'},
            {name: '«МЕГА» Теплый Стан', lat: 55.6037, long: 37.4912, address: 'Московская область, 40км МКАД', phone: '(495) 933-74-50', hours: '10:00-23:00'},
            {name: 'ТЦ «Райкин Плаза»', lat: 55.797018, long: 37.618231, address: 'Москва, ул. Шереметьевская, 8, корп. 1'},
            {name: 'ТЦ «Атриум»', lat: 55.749792, long: 37.632495, address: 'Москва, Земляной Вал, 33',phone: '(495) 725-44-92<br/> (495) 725-44-91 (касса)', hours: '10:00-23:00'},
            {name: 'Мира 76', lat: 55.7536701, long: 37.6565106, address: 'Москва, пр. Мира, д. 76, стр. 1',phone: '787-76-83<br/> 787-76-84', hours: '10:00-21:00'},
            {name: 'ТЦ «Капитолий»', lat: 55.749792, long: 37.632495, address: 'Москва, пр. Вернадского, д. 6',phone: '(495) 258-04-12 (касса)<br/> 258-04-73 (офис)', hours: '10:00-22:00'},
            {name: 'ТЦ «Европейский»', lat: 55.745038, long: 37.566736, address: 'Москва, пл. Киевского Вокзала, 2',phone: '(495) 229-61-18<br/> (495) 229-61-17', hours: '10:00-22:00'},
            {name: 'ТЦ «Бум»', lat: 55.846164, long: 37.3580759, address: 'Москва, ул. Перерва д. 43, к. 1',phone: '(495) 229-61-18<br/> (495) 229-61-17', hours: '10:00-22:00'},
            {name: 'ТЦ «Ладья»', lat: 55.846247, long: 37.357977, address: 'Москва, ул. Дубравная, д.34/29',phone: '(495) 792-57-89 (касса)<br/> 792-57-90 (офис)', hours: '10:00-22:00'},
            {name: 'ТЦ «РИО»', lat: 55.91018, long: 37.541049, address: 'Москва, Дмитровское шоссе, 163А',phone: '(495) 981-65-55 (доб.6014)', hours: '10:00-22:00'},
            {name: 'ТЦ «Фестиваль»', lat: 55.678093, long: 37.467106, address: 'Москва, Мичуринский проспект, Олимпийская деревня, д. 3, к. 1',phone: '(495) 666-26-93 (касса)<br/> 666-26-94 (офис)', hours: '10:00-22:00'},
            {name: 'ТЦ «Гагаринский»', lat: 55.706974, long: 37.592161, address: 'Москва, ул. Вавилова, вл. 3',phone: '(495) 234-47-32 (касса)<br/> 234-47-27 (зал)', hours: '10:00-22:00'},
            {name: 'ТЦ «Ереван Плаза»', lat: 55.708932, long: 37.622202, address: 'Москва, ул. Большая Тульская, д. 13',phone: '(495) 660-12-50 (касса)<br/> (495) 660-12-49 (офис)', hours: '10:00-22:00'},
            {name: 'ТЦ «Mall of Russia»', lat: 55.748915, long: 37.539035, address: 'Москва, ул. Пресненская наб., д. 2',phone: '(495)727-98-19 касса<br/> 727-98-18 офис', hours: '10:00-22:00'},
            {name: 'ТЦ «Калейдоскоп»', lat: 55.849897, long: 37.449441, address: 'Москва, Химкинский б-р, вл. 7-23,3-й этаж, помещение С-07',phone: '8(495)604-15-86 (касса)<br/> 8(495)604-15-87 (офис)', hours: '10:00-22:00'},
            {name: 'ТЦ «Европарк»', lat: 55.767062, long: 37.380674, address: 'Москва, Рублевское шоссе, д.62, пом. №70,71,72',phone: '(968) 608-74-53', hours: '10:00-22:00'},
            {name: 'ТЦ «ВИВА Бутово»', lat: 55.567281, long: 37.558043, address: 'Москва, ул. Поляны, д. 8',phone: '(495)788-14-63 (касса)<br/> (495)788-14-62 (офис)', hours: '10:00-22:00'},
            {name: 'ш. Энтузиастов, д.12, корп.2', lat: 55.747454, long: 37.707003, address: 'Москва, ш. Энтузиастов, д.12, корп.2',phone: '(495) 725-60-91', hours: '10:00-22:00'},
            {name: 'ТЦ «МЕГА Химки»', lat: 55.912129, long: 37.39719, address: 'Москвcкая область, Ленинградское шоссе, д. 1',phone: '(495) 221-33-91<br/> (495) 221-33-92', hours: '10:00-23:00'},
            {name: 'ТЦ «Глобал Сити»', lat: 55.622738, long: 37.606066, address: 'Москва, ул. Кировоградская, 14',phone: '(495) 956-62-25 (24)', hours: '10:00-22:00'},
            {name: 'ТЦ «Золотой Вавилон»', lat: 55.863584, long: 37.603019, address: 'Москва, ул. Декабристов, д.12',phone: '(499) 650-83-43', hours: '10:00-22:00'},
            {name: 'ТЦ «Тушино»', lat: 55.82443, long: 37.445305, address: 'Москва, пр. Стратонавтов, д. 11, стр. 1',phone: '(495) 491-08-12-зал<br/> (499) 740-87-31', hours: '10:00-21:00'},
            {name: 'ТЦ «МосМАрт Боровское»', lat: 55.65919, long: 37.429941, address: 'Москва, Ленинский район, 47км МКАД, стр. 20',phone: '(495) 644-25-57 (касса)<br/> 644-25-58 (офис)', hours: '10:00-22:00'},
            {name: 'Ореховый б-р, д.22а', lat: 55.612293, long: 37.732752, address: 'Москва, Ореховый б-р, д.22а',phone: '(495) 666-26-95 (касса)<br/> 666-26-98 (офис)', hours: '10:00-21:00'},
            {name: 'ТЦ «Красный кит»', lat: 55.91653, long: 37.755111, address: 'Москвская область, Шараповский пр-д, д.2', phone: '225-21-42 касса<br/> 225-21-43 офис', hours: '10:00-22:00'},
            {name: 'ТЦ «Иридиум»', lat: 55.982802, long: 37.174852, address: 'Москва, Крюковская пл., д.1', phone:'(499) 940-02-58 (касса)', hours: '10:00-22:00'}
        ];

        function initialize() {
            $('section.layout div.limit').addClass('limit-width-fix');

            var mapSelector = 'springblade-map';
            var mapDiv = document.getElementById(mapSelector);
            var mapOptions = {
                zoom: 10,
                scrollwheel: false,
                center: new google.maps.LatLng(cities[0].lat, cities[0].long),//Moscow,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                streetViewControl: false,
                panControl: false,
                zoomControl: false
            };
            map = new google.maps.Map(mapDiv, mapOptions);

            $.each(shops, function (index, shop) {
                var loc = new google.maps.LatLng(shop.lat, shop.long);

                var marker = new google.maps.Marker({
                    position: loc,
                    icon: 'http://adidas-running.ru/design/images/springblade/flag.png',
                    map: map,
                    id: index
                });

                var content = '<div class="infobox-corner js-infobox-' + index + '"></div><div class="infobox"><div class="infobox-top">';

                content += '<div class="infobox-close" onclick="$(\'div.infoBox\').hide();"></div>';

                if (shop.name) {
                    content += '<div class="infobox-name">' + shop.name + '</div>';
                }

                if (shop.address) {
                    content += '<div class="infobox-address">' + shop.address + '</div>';
                }

                if (shop.phone) {
                    content += '<div class="infobox-phone">' + shop.phone + '</div>';
                }

                content += '</div>';

                if (shop.hours) {
                    content += '<div class="infobox-hours"><div class="infobox-hours-title">Часы работы:</div>' + shop.hours + '</div>';
                }

                content += '</div>';

                var infobox = new InfoBox({
                    content: content,
                    disableAutoPan: false,
                    pixelOffset: new google.maps.Size(-50, 0),
                    zIndex: null,
                    closeBoxMargin: "28px 15px 0px 0px",
                    closeBoxURL: "http://adidas-running.ru/design/images/springblade/infobox-close.png",
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false
                });

                infobox.open(map, marker);

                google.maps.event.addListener(marker, 'click', function () {
                    $('div.infoBox').hide();
                    $('div.js-infobox-' + this.id).parent().show();
                    map.panTo(loc);
                });
            });

//TODO:
//        $('div.combo li').click(function () {
//            alert($(this).text());
//        });

            initControl(map, false);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <div class="springblade-map-wrapper">
        <div class="springblade-map-shadow"></div>
        <div id="springblade-map"></div>
    </div>
</section>


<div class="springblade-social">
    <div class="limit" style="width:1200px !important">
        <div class="social_boxes">
            <div class="vk"><span></span>

                <div class="container">
                    <div id="vk_like2"></div>
                </div>
            </div>
            <div class="fb"><span></span>

                <div class="container">
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450"
                         data-show-faces="false"></div>
                </div>
            </div>
            <div class="tw"><span></span>

                <div class="container">
                    <div id="tw-button2"></div>
                </div>
            </div>
            <div class="gp"><span></span>

                <div class="container">
                    <div class="g-plusone" data-size="medium" data-callback="plusOne"></div>
                </div>
            </div>
            <p style="clear: both;"></p>
        </div>
        <div class="tags">
            <?if(count($this->data['tags'])):?>
            <div class="title">тэги ></div>
            <?php foreach($this->data['tags'] as $tag):?>
            <strong onclick="window.location='<?echo $this->url(array('tag'=>$tag), 'search_tag')?>'"><?php echo $tag?></strong>
            <?php endforeach;?>
            <?endif;?>
        </div>
        <div id="block_article_list_title" class="article_list_title">похожие статьи ></div>


        <script type="text/javascript">
            var similarPaginator = new Paginator('/articles/index/similar');
            similarPaginator.setParam('resource', <?php echo $this->data['id']?>)
            .setParam('jsPaginator', 'similarPaginator')
                    .setCallbackEndPaginator(function (currentPage, countPage) {
                        var ui = $('#similar_more');
                        if (currentPage >= countPage) {
                            ui.text('');
                        } else {
                            ui.text('мне нужно больше статей');
                        }
                        if (0 == countPage) {
                            $('#block_article_list_title').text('');
                        }
                    });
        </script>
        <div id="container" class="article_list">
            <?php echo $this->action('similar', 'index', 'articles', array('resource'=>$this->data['id'],
            'jsPaginator'=>'similarPaginator'));?>
            <div class="clear:both;"></div>
        </div>
        <div id="similar_more" class="more_article" onclick="similarPaginator.getMaterials();"></div>
        <div class="comment_area">
            <?php if(Zend_Auth::getInstance()->hasIdentity() && !$isDraftArticle):?>

            <form class="comment_point new_comment" onsubmit="return commentSubmit(this)">
                <div class="img_box">
                    <?php if(empty(Zend_Auth::getInstance()->getIdentity()->avatar)):?>
                    <img style="width: 130px;" src="/design/images/ava_default.png" alt="img"/>
                    <?php else:?>
                    <img src="<?php echo Zend_Auth::getInstance()->getIdentity()->avatar?>" alt="img"/>
                    <?php endif;?>
                </div>
                <textarea placeholder="Написать комментарий" name="text"></textarea>
                <input type="submit" value="опубликовать"/>

                <div style="clear: both;"></div>
                <script type="text/javascript">
                    $(function () {
                        $('.comment_point textarea').focusin(function () {
                            $(this).next().show();
                            $(this).css('min-height', '130px');
                        });
                        $('.comment_point textarea').focusout(function () {
                            if ('' == $(this).val()) {
                                $(this).next().hide();
                                $(this).css('min-height', '50px');
                            } else {
                                $(this).css('min-height', '130px');
                            }
                        });

                    });

                    function commentSubmit(obj) {
                        $(obj).find('textarea').css('min-height', '50px');
                        $(obj).find('textarea').next().hide();
                        addMaterialsComment(obj, <?php echo $this->data['id'] ?>);
                        return false;
                    }
                </script>
            </form>
            <?php endif;?>
            <?php echo $this->action('index', 'materials', 'comments', array('resource'=>$this->data['id'],
            'layout'=>'layouts/springblade'));?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.twttr = (function (d, s, id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function (f) {
            t._e.push(f)
        } });
    }(document, "script", "twitter-wjs"));
    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (intentEvent) {
            materialVote( <? echo $this->data['id'] ?>,'tw');
        });
        twttr.widgets.createShareButton(
                window.location.href,
                document.getElementById('tw-button2')
        );
    });
    function plusOne(data) {
		if(data.state == "on") {
    		materialVote(<?echo $this->data['id']?>, 'gp', 1);
    	} else {
    	   materialVote(<?echo $this->data['id']?>, 'gp', 0);
    	}
    }
    window.___gcfg = {lang: 'ru'};
    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=460390354041042";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 1);
            }
        );
        FB.Event.subscribe('edge.remove',
            function(response) {
                materialVote(<?echo $this->data['id']?>, 'fb', 0);
            }
        );
    };
    $(function () {
        VK.Widgets.Like('vk_like', {type: "mini", height: 20});
        VK.Widgets.Like('vk_like2', {type: "mini", height: 20});
        VK.Observer.subscribe('widgets.like.liked', function(e){
           materialVote(<?echo $this->data['id']?>, 'vk', 1);
       	});
        VK.Observer.subscribe('widgets.like.unliked', function(e){
    	   materialVote(<?echo $this->data['id']?>, 'vk', 0);
       	});
    });
</script>
