<section id="contentEnergy">
<div id="contentEnergyWrapper">
<div id="contentEnergy__header">
    <h1>Заряди город энергией бега</h1>
    <ul id="contentEnergy__stats">
        <li>4 забега</li>
        <!--                            <li>10000 бегунов</li>-->
        <li>200 ампер часов</li>
        <li>1 освещенный стадион</li>
    </ul>
    <div id="contentEnergy__likes">
        <div id="contentEnergy__likes_vk">
            <!-- Put this script tag to the <head> of your page -->
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?97"></script>

            <script type="text/javascript">
                VK.init({apiId: 3766513, onlyWidgets: true});
            </script>

            <!-- Put this div tag to the place, where the Like block will be -->
            <div id="vk_like"></div>
            <script type="text/javascript">
                VK.Widgets.Like("vk_like", {type: "mini"});
            </script>
        </div>
        <div id="contentEnergy__likes_fb">
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like" data-href="http://www.adidas-running.ru/energy" data-send="false"
                 data-layout="button_count" data-width="450" data-show-faces="false"></div>
        </div>
        <div id="contentEnergy__likes_twi">
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="Заряди город энергией бега"
               data-via="adidasRU" data-lang="en" data-related="adidasRU">Tweet</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
        </div>
        <div id="contentEnergy_likes_google">
            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
            <g:plusone size="medium"></g:plusone>
        </div>
    </div>
</div>
<div id="contentEnergy__summary">
    <p>Энергия бега&nbsp;— материальна. Каждый шаг&nbsp;— это энергетический импульс, каждый километр&nbsp;— это минута
        освещения целой спортивной площадки. Когда энергия множества бегунов объединяется ради единой цели, нет ничего
        невозможного.</p>

    <p>Участвуй в&nbsp;проекте «Заряди город энергией бега», чтобы направить свою энергию на&nbsp;освещение стадиона в&nbsp;г.&nbsp;Протвино,
        где сейчас нет электричества.</p>
    <a class="microsite-button various fancybox.iframe"
       href="https://vk.com/video_ext.php?oid=-50263349&id=166432125&hash=9a3692c8f8a0a38b">
    </a>
</div>

<div class="my_lamp_x"></div>
<div class="my_lamp_1" id="lamp">

    <div class="amper-big">
        <span class="fline">уже накоплено</span><br/>
        <span class="sline">100 000 А</span>
    </div>

    <div class="cep">
        <!--                            <div class="point_6_1"  ></div>
                                    <div class="point_6_2"  ></div>
                                    <div class="point_6_3"  ></div>-->
        <div class="point_6_4"></div>
        <!--                            <div class="point_6_5"  ></div>
                                    <div class="point_6_6"  ></div>
                                    <div class="point_6_7"  ></div>-->

        <!--                            <div class="point_7_1"  ></div>
                                    <div class="point_7_2"  ></div>
                                    <div class="point_7_3"  ></div>-->
        <div class="point_7_4"></div>
        <!--                            <div class="point_7_5"  ></div>
                                    <div class="point_7_6"  ></div>
                                    <div class="point_7_7"  ></div>
                                    <div class="point_7_8"  ></div>
                                    <div class="point_7_9"  ></div>
                                    <div class="point_7_10"  ></div>
                                    <div class="point_7_11"  ></div>-->

        <!--                            <div class="point_8_1"  ></div>
                                    <div class="point_8_2"  ></div>
                                    <div class="point_8_3"  ></div>-->
        <div class="point_8_4"></div>
        <!--                            <div class="point_8_5"  ></div>-->

        <div class='popup_6'>
            <div class="popup_text2">
                <div class="popup_discr2">Между забегами заряжай мобильные генераторы <br/>во время тренировок на adidas
                    runbase>
                </div>
                <div class="popup_login2 popup_6_login_link_wrapper">
                    <a href="http://adidas-running.ru/runbase/"
                       class="popup_6_login_link"><strong>Зарегистрироваться</strong> </a><img src="/design/energy/images/fly.png"
                                                                                               class="popup_6_login_img"/>
                </div>
            </div>
        </div>

        <div class='popup_7'>
            <div class="popup_text">
                <div class="popup_discr">Продолжаем собирать <br> энергию даже во время <br> тренировок</div>
                <div class="popup_login popup_7_login_link_wrapper">
                    <a href="http://adidas-running.ru/runbase/"
                       class="popup_7_login_link"><strong>Зарегистрироваться</strong> </a><img src="/design/energy/images/fly.png"
                                                                                               class="popup_7_login_img"/>
                </div>
            </div>
        </div>

        <div class='popup_8'>
            <div class="popup_text">
                <div class="popup_discr">Приходи на adidas runbase> <br> и внеси свой вклад <br> в энергию бега</div>
                <div class="popup_login popup_8_login_link_wrapper">
                    <a href="http://adidas-running.ru/runbase/"
                       class="popup_8_login_link"><strong>Зарегистрироваться</strong> </a><img src="/design/energy/images/fly.png"
                                                                                               class="popup_8_login_img"/>
                </div>
            </div>
        </div>
    </div>
    <div class="cep1">
        <div class="point_1"></div>
        <div class="point_2"></div>
        <div class="point_3"></div>
        <div class="point_4"></div>
        <div class="point_5"></div>
    </div>
    <div class='popup_1'>
        <div class="popup_text ">
            <div class="popup_date">13.07</div>
            <div class="popup_title">ADIDAS <br>ENERGY RUN</div>
            <div class="popup_discr">Первый энергетический<br> забег, открывающий проект<br> "Заряди город энергией
                бега".
            </div>
            <div class="popup_km"><strong>10 и 3 км<br>5000 человек</strong></div>
            <div class="popup_login popup_1_about_link_wrapper">
                <a href="http://adidas-running.ru/event/adidas-energy-run" class="popup_1_about_link"><strong>О
                        забеге</strong></a> <img class="popup_1_about_img" src="/design/energy/images/fly.png"/><br/>
                <a href="http://vk.com/videos-50263349?z=video-50263349_166432125%2Fclub50263349" class="popup_1_login_link" target="_blank">
                    <strong>Смотреть видео</strong> </a><img src="/design/energy/images/fly.png" class="popup_1_login_img" />
            </div>
        </div>
    </div>
    <div class='popup_2'>
        <div class="popup_text">
            <div class="popup_date">04.08</div>
            <div class="popup_title">ОСЕННИЙ <br>ГРОМ</div>
            <div class="popup_discr">Ежегодный полумарафон<br/>по осенней Москве<br></div>
            <div class="popup_km"><strong>21 и 10 км<br>3000 человек</strong></div>
            <div class="popup_login popup_2_about_link_wrapper">
                <a href="http://adidas-running.ru/event/speshi-na-osenniy-grom" class="popup_2_about_link"><strong>О
                        забеге</strong></a> <img class="popup_2_about_img" src="/design/energy/images/fly.png"/><br/>
                <a href="http://vk.com/videos-50263349?z=video-50263349_166698863%2Fclub50263349" class="popup_2_login_link" target="_blank">
                    <strong>Смотреть видео</strong> </a><img src="/design/energy/images/fly.png" class="popup_2_login_img" />
            </div>
        </div>
    </div>
    <div class='popup_3'>

        <div class="popup_text">
            <div class="popup_date">25.08</div>
            <div class="popup_title">КРАСОЧНЫЙ <br>ЗАБЕГ</div>
            <div class="popup_discr">Самые веселые и яркие<br> 5 километров по Москве!</div>
            <div class="popup_km"><strong>5 км<br>2500 человек</strong></div>
            <div class="popup_login popup_3_about_link_wrapper">
                <a href="http://adidas-running.ru/event/zabeg-kogda-beg-ne-prosto-sport"
                   class="popup_3_about_link"><strong>О забеге</strong></a> <img class="popup_3_about_img"
                                                                                 src="/design/energy/images/fly.png"/><br/>
                <a href="http://vk.com/videos-50263349?z=video-50263349_166753778%2Fclub50263349" class="popup_3_login_link" target="_blank">
                    <strong>Смотреть видео</strong> </a><img src="/design/energy/images/fly.png" class="popup_3_login_img" />
            </div>
        </div>
    </div>
    <div class='popup_4'>
        <div class="popup_text">
            <div class="popup_date">15.09</div>
            <div class="popup_title">МОСКОВСКИЙ <br>МАРАФОН</div>
            <div class="popup_discr">Легендарный марафон,<br/> в котором из года в год <br> принимают участие тысячи<br>бегунов.
            </div>
            <div class="popup_km"><strong>42 км<br>10 км<br>2500 человек</strong></div>
            <div class="popup_login popup_4_about_link_wrapper">
                <a href="http://adidas-running.ru/event/moskovskiy-marafon" class="popup_4_about_link"><strong>О
                        забеге</strong></a> <img class="popup_4_about_img" src="/design/energy/images/fly.png"/>
                <!--a href="#" class="popup_3_login_link"><strong>Зарегистрироваться</strong> </a><img src="/design/energy/images/fly.png" class="popup_3_login_img" /-->
            </div>
            <div class="popup_login2 popup_6_login_link_wrapper">
                <a href="http://moscowmarathon.org/uchastnikam/42.2-km/" target="_blank" class="popup_6_login_link"><strong>Зарегистрироваться</strong>
                </a><img src="/design/energy/images/fly.png" class="popup_6_login_img"/>
            </div>

        </div>
    </div>

</div>
<ol id="contentEnergy__checkpoints">
    <li id="contentEnergy__checkpoints_item1">
        <time datetime="2013-07-13">13.07</time>
        <span class="title">Adidas Energy Run</span>
        <span class="amper-way">3000 А</span>
    </li>
    <li id="contentEnergy__checkpoints_item2">
        <time datetime="2013-07-25">04.08</time>
        <span class="title">Осенний гром</span>
    </li>
    <li id="contentEnergy__checkpoints_item3">
        <time datetime="2013-08-25">25.08</time>
        <span class="title">Красочный забег</span>
    </li>
    <li id="contentEnergy__checkpoints_item4">
        <time datetime="2013-09-15">15.09</time>
        <span class="title">Московский марафон</span>
    </li>
    <li id="contentEnergy__checkpoints_item5">
        <time datetime="2013-09-15">21.09</time>
        <span class="title">ФИНАЛ</span>

        <p>Накопленная энергия осветит городской стадион в подмосковном Протвино. Приходи и стань частью праздника. Начало в 18.30.</p>
    </li>
</ol>

<div id="contentEnergy__aside">
    <!--                        <h2 class="green">94 бегуна зарядили город энергией бега во время adidas energy run</h2>-->
    <h2>как направить свою энергию бега на общую цель</h2>
    <ol>
        <li>
            <span class="icon icon-run"></span>

            <p>Приходи на забеги<br/> adidas или на runbase &gt;</p>
        </li>
        <li>
            <span class="icon icon-clock"></span>

            <p style="margin-top: 3px">Получи мобильный<br/> генератор, преобразующий<br/> энергию бега в электричество
            </p>
        </li>
        <li>
            <span class="icon icon-accumulate"></span>

            <p style="margin-top: 2px">Заряжай своей энергией<br/> специальный накопитель вместе<br/> с тысячами других
                бегунов</p>
        </li>
        <li class="last">
            <span class="icon icon-smile"></span>

            <p style="margin-top: -10px">Улыбнись, когда твоя энергия<br/> превратится в свет, озаряющий<br/> спортивную
                площадку, которая никогда раньше не была освещена</p>
        </li>
        <!--                            <li>-->
        <!--                                <span class="icon icon-arrow"></span>-->
        <!--                                <p></p>-->
        <!--                            </li>-->
        <!--                            <li>-->
        <!--                                <span class="icon icon-clock"></span>-->
        <!--                                <p>Бегай с мобильным энергогенератором, который тебе выдадут на старте</p>-->
        <!--                            </li>-->
        <!--                            <li>-->
        <!--                                <span class="icon icon-accumulate"></span>-->
        <!--                                <p>Заряжай своей энергией специальный накопитель вместе с тысячами других бегунов  </p>-->
        <!--                            </li>-->
        <!--                            <li>-->
        <!--                                <span class="icon icon-run"></span>-->
        <!--                                <p>Приходи на финальное беговое мероприятие и ты увидишь как твоя энергия бега превратится  в свет </p>-->
        <!--                            </li> -->
    </ol>
</div>
</div>
</section>