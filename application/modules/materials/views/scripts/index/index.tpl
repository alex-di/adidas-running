<div class="sort">
    <!--<div class="title">Сортировать</div>-->
    <ul>
         <li class="current" onclick="updateStrip('id')">Новые</li>
         <li onclick="updateStrip('rate')">Популярные</li>
    </ul>
</div>
<script type="text/javascript">
	defaultPaginator = new Paginator('/materials/index/blockmaterials');
	function updateStrip(order) {
		defaultPaginator.resetPage();
		defaultPaginator.setParam('sort', order)
		defaultPaginator.getMaterials(true);
	}

</script>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<div id="container">
	<?php if(count($this->data)):?>  
	    <div class="corner-stamp index">
	        <div class="corner_element social bg2">
	            <a href="<?php echo $this->data->post_material_link?>" target="_blank" class="photo">
                    <img style="max-height: 260px;" src="<?php echo $this->data->post_photo?>" alt="img" onload="onLoadImage()" onerror="BadPost.add(<?echo $this->data->id?>)"/>
                </a>
	            <a href="<?php echo $this->data->post_profile_link?>" class="ava"><img src="<?php echo $this->data->post_user_avatar?>" alt="img" /></a>
	            <div class="name">@<?php echo $this->data->post_user_screen_name?></div>
	            <!--<div class="date"><?php echo DR_Api_Tools_Tools::formatDate($this->data->date)?></div>-->
	            <a  href="<?php echo $this->url(array(), 'social')?>" class="social_title">добавляй свои фото</a>
	            <div class="animation small-animation">

                        <script type="text/javascript">
                            $(function () {
                                var tags = [];
                                <?
                                        foreach ($this->list_tags as $val) {
                                    $words = explode(' ', trim($val->value));
                                    $wordsArray = Array(isset($words[0])? $words[0]:"", isset($words[1])? $words[1]:"", $val->ru == 1 ? "rus" : "eng");
                                    ?>
                                    tags.push({word1:'<span>#</span><?= $wordsArray[0]?>', word2:'<?= $wordsArray[1]?>', lang: '<?= $wordsArray[2]?>'});
                                <?
                                }
                                ?>

                                var currentTag = 1;
                                var lineHeight = 50;

                                $('div.tag.hidden').css({top:'-34px'});
                                $('div.tag.active').css({top:'0'});

                                $('div.tag-line-1 div.tag.active').addClass(tags[0].lang).html(tags[0].word1);
                                $('div.tag-line-2 div.tag.active').addClass(tags[0].lang).html(tags[0].word2);

                                $('div.tag-line-1 div.tag.hidden').addClass(tags[0].lang).html(tags[1].word1);
                                $('div.tag-line-2 div.tag.hidden').addClass(tags[0].lang).html(tags[1].word2);

                                function showCarouselTag() {
                                    $('.tag').animate({top:'+=34px'}, 400, function () {
                                        $(this).toggleClass('active').toggleClass('hidden');

                                        if ($(this).is('.hidden')){
                                            $(this)
                                                    .css({top:'-34px'})
                                                    .removeClass('eng')
                                                    .removeClass('rus')
                                                    .addClass(tags[currentTag].lang);

                                            if($(this).is('div.tag-line-1 div.hidden'))
                                                $(this).html(tags[currentTag].word1);

                                            if($(this).is('div.tag-line-2 div.hidden'))
                                                $(this).html(tags[currentTag].word2);
                                        }
                                    });

                                    currentTag++;
                                    if (currentTag == tags.length)
                                        currentTag = 0;
                                }

                                setInterval(showCarouselTag, 3000);
                                
                                $('.index_slider').mySlider();
                            });
                        </script>
                    <div class="carousel-tag-line tag-line-1">
                        <div class="tag hidden tag1"></div>
                        <div class="tag active tag2"></div>
                    </div>

                    <div class="carousel-tag-line tag-line-2">
                        <div class="tag hidden tag1"></div>
                        <div class="tag active tag2"></div>
                    </div>
	            </div>
	        </div>
	    </div>

    <?php endif;?>
   <?= $this->action('view', 'slider', 'banners', array('key' => Model_Slider::SLIDER_ON_MAIN))?>
   
	<?= $this->action('blockmaterials', 'index', 'materials')?>

   <div style="clear: both;"></div>                              	
</div>