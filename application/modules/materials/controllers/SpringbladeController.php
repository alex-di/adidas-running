<?php

class Materials_SpringbladeController extends DR_Controllers_Site
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layouts/springblade');
        $article = api::getMaterials()->getMaterials(Model_Artunique::SPRINGBLADE_MATERIALS_ID);
        $this->view->data = Model_Materials::getFirst($article);
    }
}
