<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_MotivationController extends DR_Controllers_Site
{

    public function indexAction()
    {
        $this->view->headTitle('Мотивация - adidas running');
    }
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $sort = $this->_getParam('sort', 'id');
        $perPage = 10;
        $materials = api::getMaterials();
        list($data, $paginator) = $materials->_new(array('id'))->in('t.category_id', array(
            Model_Materials::CATEGORY_INSPIRING_ARTICLES,
            Model_Materials::CATEGORY_EVENTS))->order($sort . ' DESC')->where('is_modern', 1)->pageRows($page, $perPage);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
		if(count($ids)) {
		  
			$ids = $materials->insertQuotes($ids, 8, $page, $perPage);
			$this->view->data = $materials->getMaterials($ids);
		}
        Zend_Registry::set(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT, $materials->getNextEvent());
        parent::blockmaterialsAction();
    }
}
