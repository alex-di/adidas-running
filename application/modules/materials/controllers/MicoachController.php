<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_MicoachController extends DR_Controllers_Site {

	public function indexAction() {
	    $this->view->headTitle('micoach - adidas running');
        $this->view->micoachPage = api::getPages()->where('t.id', Model_Pages::MICOACH_PAGE)->row();
	}
	public function blockmaterialsAction() {
        $this->view->headTitle('runbase - adidas running');
        Zend_Layout::getMvcInstance()->setLayout('layout');
        $page = $this->_getParam('page', 1);
        $materials = api::getMaterials();
        $runbase_articles = api::getKeywords()->_new(array('id' => 'mtk.materials_id'))
                                        ->where('mtk.materials_id is not null')
                                        ->where('t.name', Model_Materials::MICOACH_TAG)
                                        ->where('mt.is_modern', 1)
                                        ->joinLeft(array('mtk' => 'Model_Materialkeys'), "mtk.keywords_id = t.id", array())
                                        ->joinLeft(array('mt' => 'Model_Materials'), "mt.id = mtk.materials_id", array());

        list($data, $paginator) = $runbase_articles->pageRows($page, 10);
        //_d($data);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
        if (count($ids))
        {
            $this->view->data = $materials->getMaterials($ids);
        }
        parent::blockmaterialsAction();
	}
}
