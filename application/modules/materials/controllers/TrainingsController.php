<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_TrainingsController extends DR_Controllers_Site
{

    public function indexAction()
    {
        $this->view->headTitle('Тренировки - adidas running');
    }
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $sort = $this->_getParam('sort', 'id');
        $materials = api::getMaterials();
        list($data, $paginator) = $materials->_new(array('id'))->in('t.category_id', array(
            Model_Materials::CATEGORY_COACH_ARTICLES, Model_Materials::CATEGORY_SHOP_REVIEW,
            Model_Materials::CATEGORY_ROUTERS))->order($sort . ' DESC')->where('is_modern', 1)->pageRows($page, 10);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
        if (count($ids))
        {
            if($page == 1) {
                 $ids = $materials->insertNextEvents($ids);
            }
            $this->view->data = $materials->getMaterials($ids);
        }
        Zend_Registry::set(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT, $materials->getNextEvent());
        parent::blockmaterialsAction();
    }
}
