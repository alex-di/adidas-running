<?php

/**
 * Главная
 * 
 * @author appalach
 * @version 1.3
 * @final
 */
class Materials_IndexController extends DR_Controllers_Site {

	public function indexAction() {
		$this->view->data = api::getMaterials()->getLastSocialPost();
		$this->view->list_tags = api::getLists()->_new()
				->where('t.name', 'hash_post_tags')
				->joinLeft(array("um" => api::LIST_VALUES), "t.id = um.list_id", array("um.id", "um.value", "um.ru"))->rows();
		
	}
	
	
	public function testAction() {
		
	}
	
	public function blockmaterialsAction() {
		$page = $this->_getParam('page', 1);
        $perPage = 10;
		$sort = $this->_getParam('sort', 'id');
		$materials = api::getMaterials();
		list($data, $paginator) = $materials->_new(array('id'))
                ->notIn('t.category_id', array(Model_Materials::CATEGORY_TWITTER_POST, 
                                                Model_Materials::CATEGORY_INSTAGRAM_POST, 
                                                Model_Materials::CATEGORY_QUETES, 
                                                Model_Materials::CATEGORY_RUNBASE, 
                                                Model_Materials::CATEGORY_UNIQUE))
                ->order($sort . ' DESC')
				->where('is_modern', 1)->pageRows($page, $perPage);
		$this->view->pageCount = $paginator->count();
		$ids = Model_Materials::getArrayObjectId($data);
		if(count($ids)) {
			$ids = $materials->insertQuotes($ids, 12, $page, $perPage);
			$this->view->data = $materials->getMaterials($ids);
		}
        Zend_Registry::set(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT, $materials->getNextEvent());
		parent::blockmaterialsAction();
	}
}
