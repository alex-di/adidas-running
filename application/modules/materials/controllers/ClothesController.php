<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_ClothesController extends DR_Controllers_Site
{

    public function indexAction()
    { 
        $this->view->headTitle('Одежда - adidas running');
        $sliders = api::getSlider();
        $this->view->slider_banners = $sliders->_new(array('t.id'))->where('t.key', Model_Slider::SLIDER_IN_CLOTHES)
            ->joinLeft(
                array(
                    'b' => api::BANNERS
                ),
                'b.slider_id = t.id',
                array(
                    'banner_image' => 'b.file',
                    'banner_name' => 'b.name',
                    'banner_color' => 'b.color',
                )
            )
            ->rows();
        //_d($this->view->slider_banners);
    }
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $sort = $this->_getParam('sort', 'id');
        $materials = api::getMaterials();
        list($data, $paginator) = $materials->_new(array('id'))->in('t.category_id', array(
            Model_Materials::CATEGORY_SHOP_REVIEW))->order($sort . ' DESC')->where('is_modern', 1)->pageRows($page, 10);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
        if(count($ids))
            $this->view->data = $materials->getMaterials($ids);
        parent::blockmaterialsAction();
    }
}
