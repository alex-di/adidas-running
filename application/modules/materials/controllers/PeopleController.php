<?php

/**
 * Люди
 * 
 * @author appalach
 * @version 1.3
 * @final
 */
class Materials_PeopleController extends DR_Controllers_Site
{


    public function indexAction()
    {
        $this->view->headTitle('Люди - adidas running');

    }

    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $sort = $this->_getParam('sort', 'id');
        $author = $this->_getParam('author_id', false);

        $perPage = 10;

        $materials = api::getMaterials();
        $articles = $materials->getMaterialsFromFamous($author, $sort, $page, $perPage);

        list($data, $paginator) = $articles;

        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);

        if (count($ids)) {
			$ids = $materials->insertQuotes($ids, 14, $page, $perPage);
			$this->view->data = $materials->getMaterials($ids);
		} else {
            $this->view->data = array();
        }

        parent::blockmaterialsAction();
    }
    
    #блог пользователя
    public function viewAction()
    {
        if ($id = $this->_getParam('id')) {
	        $articles = api::getMaterials();
	        $this->view->alldata = $articles->_new()->where('is_modern', 1)->queryIn('t.users_id', $id)->rows();
            $data = $this->view->alldata->offsetGet(0);
	        if (count($data)) {
	            $this->_redirect('/article/' . $data->stitle);
	        } else {
	            $this->_redirect('/famous');
	        }
	    }
    }
}
