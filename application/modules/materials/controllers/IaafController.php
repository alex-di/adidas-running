<?php

class Materials_IaafController extends DR_Controllers_Site
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layouts/iaaf');
        $article = api::getMaterials()->getMaterials(Model_Artunique::IAAF_MATERIALS_ID);
        $this->view->data = Model_Materials::getFirst($article);
    }
    public function testAction(){
        Zend_Layout::getMvcInstance()->setLayout('layouts/iaaf');
        $article = api::getMaterials()->getMaterials(Model_Artunique::IAAF_MATERIALS_ID);
        $this->view->data = Model_Materials::getFirst($article);
    }

}
