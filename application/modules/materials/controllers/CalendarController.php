<?php

class Materials_CalendarController extends DR_Controllers_Site
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layouts/calendar');
        $this->view->headLink()->appendStylesheet('/design/calendar/css/calendar.css')
                               ->appendStylesheet('/design/css/fixes.css')
                               ->appendStylesheet('/design/css/reset.css')
                               ->appendStylesheet('/design/css/mediaqueries.css')
                               ->appendStylesheet('/design/css/style.css');

        $article = api::getMaterials()->getMaterials(Model_Artunique::CALENDAR_MATERIALS_ID);                               
        $this->view->data = Model_Materials::getFirst($article);
    }
}
