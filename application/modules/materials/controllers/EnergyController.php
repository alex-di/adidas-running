<?php

class Materials_EnergyController extends DR_Controllers_Site
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layouts/boost');
        $this->view->headLink()->appendStylesheet('/design/css/reset.css')
                               ->appendStylesheet('/design/css/style.css')
                               ->appendStylesheet('/design/css/mediaqueries.css')
                               ->appendStylesheet('/design/css/fixes.css')
                               ->appendStylesheet('/design/energy/css/adidas-energy.css')
                               ->appendStylesheet('/design/energy/fancybox/jquery.fancybox.css?v=2.1.5')
                               ->appendStylesheet('/design/energy/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5')
                               ->appendStylesheet('/design/energy/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');
        $this->view->headScript()->appendFile('/design/energy/js/jquery.preload-1.0.8-min.js')
                                 ->appendFile('/design/energy/js/script.js')
                                 ->appendFile('/design/energy/fancybox/jquery.fancybox.pack.js?v=2.1.5')
                                 ->appendFile('/design/energy/fancybox/jquery.mousewheel-3.0.6.pack.js')
                                 ->appendFile('/design/energy/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5')
                                 ->appendFile('/design/energy/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6')
                                 ->appendFile('/design/energy/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7');
    }
}
