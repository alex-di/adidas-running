<?php

/**
 * Люди
 * 
 * @author appalach
 * @version 1.3
 * @final
 */
class Materials_EventsController extends DR_Controllers_Site
{


    public function indexAction()
    {
        $this->view->headTitle('События - adidas running');
        $this->view->type = Model_Materials::FUTURE_MATERIALS;
        $countFutureEvent = api::getMaterials()->_new(array('id'))->where('is_modern', 1)
                                ->where('t.category_id', Model_Materials::CATEGORY_EVENTS)
                                ->where('m.date_value', new Zend_Db_Expr('NOW()'), '>=')
                                ->joinLeft(array('m' => api::META), 't.id = m.resource_id and m.key = "' . Model_Materials::START_EVENT . '" and m.modules_id = ' . Model_Meta::MATERIALS)
                                ->count();
        if(!$countFutureEvent)
            $this->view->type = Model_Materials::PAST_MATERIALS;
    }
    public function processregisterAction()
    {
        $events_id = $this->_getParam('id');
        api::getReferences()->saveReference(Zend_Auth::getInstance()->getIdentity()->id, Model_References::REF_TYPE_USER_EVENTS, array($events_id));
        die('OK');
    }

    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $sort = $this->view->sort = $this->_getParam('sort', Model_Materials::FUTURE_MATERIALS);

        $articles = api::getMaterials();

        $articles->_new(array('id'))->where('is_modern', 1)
                ->where('t.category_id', Model_Materials::CATEGORY_EVENTS)
                ->joinLeft(array('m' => api::META), 't.id = m.resource_id and m.key = "' . Model_Materials::START_EVENT . '" and m.modules_id = ' . Model_Meta::MATERIALS, array('event_start' => 'm.date_value'));

        #Будующие события
        if ($sort == Model_Materials::FUTURE_MATERIALS)
        {
            $articles->where('m.date_value', new Zend_Db_Expr('NOW()'), '>=') ->order('event_start ASC');
        }
        #Прошедшие события
        if ($sort == Model_Materials::PAST_MATERIALS)
        {
            $articles->where('m.date_value', new Zend_Db_Expr('NOW()'), '<=') ->order('event_start DESC');;
        }

        list($data, $paginator) = $articles->pageRows($page, 10);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
        if (count($ids)) {
            $this->view->data = $articles->getMaterials($ids);
            if($sort == Model_Materials::FUTURE_MATERIALS && 1 == $page)
                Zend_Registry::set(Model_Materials::REGISTER_NAMESPACE_EVENT_NEXT, $ids[0]);
        }
            
        parent::blockmaterialsAction();
    }
}
