<?php

class Materials_BoostController extends DR_Controllers_Site
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layouts/boost');
        $this->view->headLink()->appendStylesheet('/design/css/boost/boost.css')
                               ->appendStylesheet('/design/css/runbase.css')
                               ->appendStylesheet('/design/css/fixes.css')
                               ->appendStylesheet('/design/css/reset.css')
                               ->appendStylesheet('/design/css/mediaqueries.css')
                               ->appendStylesheet('/design/css/style.css');

        $article = api::getMaterials()->getMaterials(Model_Artunique::BOOST_MATERIALS_ID);                               
        $this->view->data = Model_Materials::getFirst($article);
    }
}
