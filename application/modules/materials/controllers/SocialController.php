<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_SocialController extends DR_Controllers_Site {

	
	public function indexAction() {
		$this->view->headTitle('Общение - adidas running');
		$this->view->list_tags = api::getLists()->_new()
				->where('t.name', 'hash_post_tags')
				->joinLeft(array("um" => api::LIST_VALUES), "t.id = um.list_id", array("um.id", "um.value", "um.ru"))->rows();
	}
	public function blockmaterialsAction() {
		$page = $this->_getParam('page', 1);

		$materials = api::getMaterials();
		list($data, $paginator) = $materials->_new(array('id'))
				->in('t.category_id', array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST))->order('id DESC')
				->where('is_modern', 1)->pageRows($page, 10);
		$this->view->pageCount = $paginator->count();
		$ids = Model_Materials::getArrayObjectId($data);
        if(count($ids))
            $this->view->data = $materials->getMaterials($ids);

		parent::blockmaterialsAction();
	}
    
    public function processcheckpostAction(){
        if(isset($_POST['data']) && is_array($_POST['data']) && count($_POST['data'])) {
            $materials_id = array();
            foreach($_POST['data'] as $id) {
                $materials_id[] = intval($id);
            }
            api::getMaterials()->executeQuery('UPDATE Model_Materials SET is_modern = '.Model_Materials::TYPE_CANCELED.' WHERE id IN(' . implode(',', $materials_id) .')');
        }
        die;
    }
}
