<?php

/**
 *
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Materials_RunbaseController extends DR_Controllers_Site
{

    public function indexnAction()
    {
        $this->view->headTitle('runbase - adidas running');
        Zend_Layout::getMvcInstance()->setLayout('runbase_layout');
    }

    public function blockmaterialsAction()
    {
        $this->view->headTitle('runbase - adidas running');
        Zend_Layout::getMvcInstance()->setLayout('runbase_layout');
        $page = $this->_getParam('page', 1);
        $materials = api::getMaterials();
        $runbase_articles = api::getKeywords()->_new(array('id' => 'mtk.materials_id'))
                                        ->where('mtk.materials_id is not null')
                                        ->where('t.name', Model_Materials::RUNBASE_TAG)
                                        ->where('mt.is_modern', 1)
                                        ->joinLeft(array('mtk' => 'Model_Materialkeys'), "mtk.keywords_id = t.id", array())
                                        ->joinLeft(array('mt' => 'Model_Materials'), "mt.id = mtk.materials_id", array());

        list($data, $paginator) = $runbase_articles->pageRows($page, 10);
        //_d($data);
        $this->view->pageCount = $paginator->count();
        $ids = Model_Materials::getArrayObjectId($data);
        if (count($ids))
        {
            $this->view->data = $materials->getMaterials($ids);
        }
        parent::blockmaterialsAction();
        //_d($this->view->data);

    }

    public function indexAction()
    {
        #ближайшее событи runbase
        $materials = api::getMaterials();        
        $data = $materials->_new()->where('t.category_id', Model_Materials::CATEGORY_RUNBASE)->joinLeft(array('meta' => 'Model_Meta'), "meta.key = '" . Model_Materials::START_EVENT . "' and meta.date_value > NOW()  and meta.resource_id = t.id", array('date_start_event' => 'meta.date_value'))->where('meta.date_value', '', '!=')->order('meta.date_value ASC') //->limit(1)
            //->debug(true)
        ->rows();
        //_d($data);
        $ids = Model_Materials::getArrayObjectId($data);
        if (count($ids))
        {
            $this->view->event = Model_Materials::getFirst($materials->getMaterials($ids));
        }
        //_d($this->view->event);
    }

    public function eventsAction()
    {
        $materials = api::getMaterials();
        $data = $materials->_new()->where('t.category_id', Model_Materials::CATEGORY_RUNBASE)->joinLeft(array('meta' => 'Model_Meta'), "meta.key = '" . Model_Materials::START_EVENT . "' and meta.date_value > NOW() and meta.resource_id = t.id", array('date_start_event' => 'meta.date_value'))->where('meta.date_value', '', '!=')->order('meta.date_value ASC')->rows();

        Zend_Layout::getMvcInstance()->setLayout('runbase_layout');

        $ids = Model_Materials::getArrayObjectId($data);

        if (count($ids))
        {
            $this->view->events = $materials->getMaterials($ids);
        }
    }


    public function reserveAction()
    {

        if (!Zend_Auth::getInstance()->hasIdentity())
        {
            $this->Response(array(), array("$.showInfo('Для резервирования шкафчика необходима авторизация на портале')"));
        }


        $current_user = api::getUsers()->_new()->where('id', Zend_Auth::getInstance()->getIdentity()->id)->row();
        if (empty($current_user->card_id))
        {
            $this->Response(array(), array("$.showInfo('<div class=\"bind-card\"><span class=\"close\">закрыть</span><p class=\"bold\">твоя карта еще не&nbsp;привязана к&nbsp;аккаунту, поэтому ты&nbsp;не&nbsp;можешь забронировать шкафчик заранее. но&nbsp;ты&nbsp;можешь полноценно использовать карту <span>runner&gt;</span> на&nbsp;территории <span>runbase&gt;</span></p><p>Если&nbsp;с&nbsp;момента&nbsp;подачи&nbsp;заявки&nbsp;на&nbsp;регистрацию&nbsp;прошло больше 7&nbsp;рабочих дней, сообщи об&nbsp;этом на&nbsp;горячую линию: <b>8&nbsp;(495)&nbsp;648-54-69</b></p></div>')"));
        }

        $events_id = $this->_getParam('runbase_event_id', 0);
        $date = null;
        if (isset($_POST['date_event']))
        {
            $date = $_POST['date_event'];
        }
        if (isset($_POST['day_start_event']) and !empty($_POST['day_start_event']) and isset($_POST['time_start_event']) and !empty($_POST['time_start_event']))
        {
            $date = date("Y-m-d H:i:s", strtotime($_POST['day_start_event'] . " " . $_POST['time_start_event']));
        }
        if (is_null($date))
        {
            $this->Response(array(), array("$.showInfo('Не выбрана дата и время')"));
        }

        if (!$events_id)
        {
            $this->Response(array(), array("$.showInfo('Ошибка доступа')"));
        }

        $max_time_minus = intval($this->view->settings['runbase_time_reserve']);
        $max_time_plus = $max_time_minus - 1;
        $referen = api::getReferences()->where('t.ref_type', Model_References::REF_TYPE_RUNBASE)->between('t.date', date("Y-m-d H:i:s", strtotime($date . " - $max_time_minus hours")), date("Y-m-d H:i:s", strtotime($date . " + $max_time_plus hours")), '>=', '<=')->joinLeft(array('u' => api::USERS), 't.reference_id = u.id', array('u.sex'))->rows();
        $mans = 0;
        $womans = 0;
        foreach ($referen as $record)
        {
            if ($record->sex == 'man')
            {
                $mans++;
            } else
            {
                $womans++;
            }
        }

        switch (Zend_Auth::getInstance()->getIdentity()->sex)
        {
            case 'man':
                if ($mans >= intval($this->view->settings['runbase_max_man']))
                {
                    $this->Response(array(), array("$.showInfo('Все места забранированы. Выберите другое время')"));
                }
                break;
            case 'woman':
                if ($womans >= intval($this->view->settings['runbase_max_woman']))
                {
                    $this->Response(array(), array("$.showInfo('Все места забранированы. Выберите другое время')"));
                }
                break;
        }


        $res = api::getReferences()->saveReference($events_id, Model_References::REF_TYPE_RUNBASE, array(Zend_Auth::getInstance()->getIdentity()->id), $date);
        if (!$res)
        {
            $this->Response(array(), array("$.showInfo('У вас есть активная бронь на это время или событие')"));
        }

        $this->Response(array(), array("$.showInfo('Вы успешно забронировали шкафчик')"));
    }

    public function cardAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('runbase_layout');
    }

    public function contactsAction()
    {
        if ($this->getRequest()->isPost()) {
            $adidasMail = 'runbase@adidas-running.ru';
            $message = $this->getRequest()->getParam('message', false);

            if (Zend_Auth::getInstance()->getIdentity() !== null && isset(Zend_Auth::getInstance()->getIdentity()->id)) {
                $user = Zend_Auth::getInstance()->getIdentity();
                $emailFrom = $user->email;
                $nameFrom = $user->name . ' ' . $user->sname;
            } else {
                $emailFrom = $this->getRequest()->getParam('email', false);
                $nameFrom = "Гость портала Adidas Running";
            }

            if (!empty($message) && strtolower($message) !== 'ваше сообщение' && $emailFrom !== false) {
                $message .= "\n" . "E-mail для обратной связи: " . $emailFrom;

                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyText($message);
                $mail->addTo($adidasMail);
                $mail->setSubject('Вопрос с сайта Adidas Running');

                $isSent = true;
                try {
                    $mail->send();
                } catch (Exception $e){
                    $isSent = false;
                }
                
                if (!$isSent) {
                    // TODO: Уточнить про то как показывать уведомление
                }
            } else {
                $this->view->assign('formError', 'Пожалуйста, заполните все поля');
            }
        }

        Zend_Layout::getMvcInstance()->setLayout('runbase_layout');
    }

}
