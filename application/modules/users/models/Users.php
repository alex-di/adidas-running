<?php

/**
 * Модель для юзеров
 */
class Model_Users extends DR_Models_AbstractTable
{


    public static $current_module_id = 33;
    
    public function getProfileInfo($user_id) {
        $data = $this->_new(array('t.*'))->where('t.id', $user_id)
                     ->joinLeft(array('meta' => api::META), 'meta.resource_id = t.id and meta.modules_id = '.Model_Meta::USERS, array('meta.value', 'meta.text_value', 'meta.key'))
                     ->rows();
        $result = array();
        if(count($data)) {
            foreach($data as $row) {
                $result['id'] = $row->id;
                $result['name'] = $row->name;
                $result['sname'] = $row->sname;
                $result['group_id'] = $row->group_id;
                $result['bday'] = $row->bday;
                $result['sex'] = $row->sex;
                $result['city'] = $row->city;
                $result['avatar'] = $row->avatar;
                $result['vk_id'] = $row->vk_id;
                $result['fb_id'] = $row->fb_id;
                $result['tw_id'] = $row->tw_id;
                $result['fq_id'] = $row->fq_id;
                $result['card_id'] = $row->card_id;
                if(!empty($row->text_value)) 
                    $result[$row->key] = $row->text_value;
                if(!empty($row->value)) 
                    $result[$row->key] = $row->value;
            }
        }
        return $result;
    }
    /**
     * Идентификатор группы
     */
    public function getRoleId($default_group = 0)
    {
        $group = Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->group_id : $default_group;
        return 'group_' . $group;
    }
    
    public static function isCurrentUserProfile() {
        if (Zend_Registry::isRegistered('request')) {
           $request = Zend_Registry::get('request');
           return (isset($request['module']) && 'messages' == $request['module']) || (isset(Zend_Auth::getInstance()->getIdentity()->id) && isset($request['id']) && $request['id'] == Zend_Auth::getInstance()->getIdentity()->id);
        }   
        return false;    
    }
    /**
     *	Обновить дату последнего входа
     */
    public function updateEnterTime()
    {
        $row = $this->fetchRow($this->select()->where('id =?', Zend_Auth::getInstance()->getIdentity()->id));

        if (is_object($row))
        {            
            $row->last_enter_date = new Zend_Db_Expr('NOW()');
            $row->save();
        }
    }
    public static function cryptoPassword($password){
        $salt = md5('adidas-running.ru');
        return sha1(md5($password . $salt));
    }
    /**
     *	-	Назначение: Вход
     */
    public function doLogin($values)
    {
        if (isset($values["vk_id"]))
        {
            $login = $values["vk_id"];
            $password = $values["hash"];
        } elseif (isset($values["fb_id"]))
        {
            $login = $values['fb_id'];
            $password = $values['hash'];
        } elseif (isset($values["tw_id"]))
        {
            $login = $values['tw_id'];
            $password = $values['hash'];
        } else
        {
            $login = $values['login'];
            $password = $values['password'];
        }

        try
        {
            if (empty($login))
            {
                throw new Exception('login|empty');
            }

            if (empty($password))
            {
                throw new Exception('password|empty');
            }

            $filter = new Zend_Filter_StringTrim();

            $login = $filter->filter($login);
            $password = $filter->filter($password);

            $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
            if (isset($values["vk_id"]))
            {
                $authAdapter->setTableName($this->_name)->setIdentityColumn('vk_id')->setCredentialColumn('hash');
            } elseif (isset($values["fb_id"]))
            {
                $authAdapter->setTableName($this->_name)->setIdentityColumn('fb_id')->setCredentialColumn('hash');
            } elseif (isset($values["tw_id"]))
            {
                $authAdapter->setTableName($this->_name)->setIdentityColumn('tw_id')->setCredentialColumn('hash');
            } else
            {
                $authAdapter->setTableName($this->_name)->setIdentityColumn('email')->setCredentialColumn('password');
            }
            $authAdapter->setIdentity($login)->setCredential($password);

            $result = $authAdapter->authenticate();
            $user_info = $authAdapter->getResultRowObject();


            if (isset($user_info->last_enter_date)) {
                if ($user_info->last_enter_date == null || $user_info->last_enter_date == '0000-00-00 00:00:00') {                    
                    $_SESSION['showHints'] = true;
                }
            }

            switch ($result->getCode())
            {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    throw new Exception('login|incorect');
                    break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    throw new Exception('password|incorect');
                    break;

                case Zend_Auth_Result::SUCCESS:
                    Zend_Session::rememberMe(365 * 24 * 60 * 60);
                    Zend_Auth::getInstance()->getStorage()->write($authAdapter->getResultRowObject(null, array()));
                    break;
            }
        }

        catch (exception $e)
        {
            $error = explode('|', $e->getMessage());
            return array($error[0] => $error[1]);
        }

        return false;
    }

    /**
     *	-	Назначение: Выход
     *	-	Дата: 28.06.2007 15:05:17
     */
    public function doLogout()
    {
        Zend_Auth::getInstance()->clearIdentity();
    }


    /**
     * Обновить данные Zend_Auth
     */
    public static function refreshAuth()
    {

        $users = new self;
        $user = $users->_new()->where('id', Zend_Auth::getInstance()->getIdentity()->id)->row();


        //getSimpleData(array("primary_key" => Zend_Auth::getInstance()->getIdentity()->id));
        Zend_Auth::getInstance()->getStorage()->write($user);

    }

    public static function getFamousArticleCategory(){
        return array(
            Model_Materials::CATEGORY_COACH_ARTICLES,
            Model_Materials::CATEGORY_INSPIRING_ARTICLES,
            Model_Materials::CATEGORY_ROUTE_ARTICLES,
            Model_Materials::CATEGORY_USER_ARTICLES,
        );
    }

    public function getFamousPeople($users_id = array())
    {
        $article_categories = implode(', ', self::getFamousArticleCategory());

        $this->_new()
            ->where('t.group_id', Model_Groups::FAMOUS)
            //->joinLeft(array('m' => api::META), "t.id = m.resource_id and m.key = 'user_avatar_background' and m.modules_id =" . Model_Meta::USERS, array("bg" => "m.value"))
            ->joinLeft(array('mat' => api::MATERIALS), "t.id = mat.users_id and mat.id = (SELECT MAX(id) from Model_Materials where users_id = t.id and category_id IN($article_categories))", array(
                "article_name" => "mat.name",
                "article_stitle" => "mat.stitle",
                "article_date" => "mat.date",
                "article_count" => " (select count(1) from Model_Materials where users_id = t.id and category_id IN($article_categories))"));
        if (is_array($users_id) && count($users_id)) {
            $this->in('t.id', $users_id);
        } elseif(is_numeric($users_id)) {
            $this->where('t.id', $users_id);
        }

        return $this->rows();
    }

    public function getAuthors()
    {
        $article_categories = implode(', ', self::getFamousArticleCategory());
        $authors = $this->_new()->where('t.group_id', Model_Groups::FAMOUS)
                                ->in('mat.category_id', $article_categories)
                                ->joinLeft(array('mat' => api::MATERIALS), "t.id = mat.users_id",
                                           array("article_count" => "(SELECT COUNT(1) FROM Model_Materials WHERE users_id = t.id)"))
                                ->group('id')
                                ->rows();
        return $authors;
    }

    public function getMaterialsFromFamous($user_id = false)
    {
        $article_categories = implode(', ', self::getFamousArticleCategory());
        $this->_new()
            ->in('mat.category_id', $article_categories)
            ->joinLeft(array('mat' => api::MATERIALS), "t.id = mat.users_id and mat.id = (SELECT MAX(id) from Model_Materials where users_id = t.id and category_id IN($article_categories))", array(
                "article_name" => "mat.name",
                "article_stitle" => "mat.stitle",
                "article_date" => "mat.date",
                "article_count" => " (select count(1) from Model_Materials where users_id = t.id and category_id IN($article_categories))"));

        return $this->rows();
    }

    public static function getCountEvents($users_id) {
        $model = new self;
        return $model->_new(array('count_modern_article'=>'(SELECT COUNT(1) FROM Model_Events WHERE owner_id = t.id AND type = '.Model_Events::TYPE_MODERN_MATERIALS.')'))
            ->where('t.id', $users_id)   
            ->row();
    }

    public function getUserData($user_id, $fields = array()) {
        $currenr_user_id = Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->id : 0;
        $fields[] = 't.*';
        $fields['is_favorite'] = '(SELECT COUNT(1) FROM Model_References WHERE reference_id = t.id AND object_id = '.$currenr_user_id.' AND ref_type = '.Model_References::REF_TYPE_USER_FAVORITEUSERS.')';
        $fields['count_events_profile'] = '(SELECT COUNT(1) FROM Model_Events WHERE owner_id = t.id AND is_read = 0 AND type NOT IN ('.Model_Events::TYPE_NOT_MODERN_MATERIALS . ', ' .Model_Events::TYPE_CLOTHESADD. ') )';
        $fields['count_articles'] = '(SELECT COUNT(1) FROM Model_Materials mt LEFT JOIN Model_Meta m ON m.resource_id = mt.id AND m.modules_id = '.Model_Meta::MATERIALS." AND m.key = 'is_profile_material' WHERE mt.users_id = t.id AND mt.is_modern = ".Model_Materials::TYPE_MODERN." AND mt.category_id !=".Model_Materials::CATEGORY_ROUTERS." AND m.value = '1')";
        $fields['count_run'] = '(SELECT COUNT(1) FROM Model_References ref LEFT JOIN Model_Meta m ON m.resource_id = ref.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."' WHERE ref.object_id = t.id AND ref.ref_type = ".Model_References::REF_TYPE_USER_RUNROUTES.' AND m.int_value = 1)';
        $fields['count_events_favorites'] = '(SELECT COUNT(1) FROM Model_Events WHERE owner_id = t.id AND is_read = 0 AND type = '.Model_Events::TYPE_FAVORITEUSERS.')';

        return $this->_new($fields)
            ->where('t.id', $user_id)
            ->joinLeft(array('c' => api::CARDS), 't.card_id = c.id', array('card_number' => 'c.number'))    
            ->row();
    }

    /**
     * Получить информацию о пользователе 
     */
    public function getUserInfo($param)
    {
        if (!Zend_Auth::getInstance()->getIdentity())
        {
            return;
        } else
        {
            switch ($param)
            {
                case 'name':
                    return Zend_Auth::getInstance()->getIdentity()->name;
                    break;
                case 'sname':
                    return Zend_Auth::getInstance()->getIdentity()->sname;
                    break;
                case 'phone':
                    return Zend_Auth::getInstance()->getIdentity()->phone;
                    break;
                case 'email':
                    return Zend_Auth::getInstance()->getIdentity()->email;
                    break;
                case 'city':
                    return Zend_Auth::getInstance()->getIdentity()->city;
                    break;
                case 'address':
                    return Zend_Auth::getInstance()->getIdentity()->address;
                    break;
                case 'group':
                    $group = Zend_Auth::getInstance()->getIdentity()->group_id;
                    if ($group == Model_Groups::USERS)
                    {
                        $group = Model_Groups::GUEST;
                    } elseif ($group == Model_Groups::ADMIN)
                    {
                        $group = Model_Groups::GUEST;
                    }
                    return $group;
                    break;
                default:
                    return;
                    break;
            }
        }

    }


}
