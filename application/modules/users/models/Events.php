<?php

/**
 */
class Model_Events extends DR_Models_AbstractTable
{

    const TYPE_FAVORITEUSERS = 1;
    const TYPE_CLOTHESADD = 2;
    const TYPE_MODERN_MATERIALS = 3;
    const TYPE_COMMENT_MATERIALS = 4;
    const TYPE_LIKE_MATERIALS = 5;
    const TYPE_RUN_MATERIALS = 6;
    const TYPE_NOT_MODERN_MATERIALS = 7;
    /*
    for events with type TYPE_COMMENT_MATERIALS, TYPE_LIKE_MATERIALS create triggers 
    */
    public static function modernArticleEvent($materials_id, $owner_id, $users_id, $modern_status) {
        $model = new self;
        $event = $model->where('t.resource_id', $materials_id)->where('t.owner_id', $owner_id)->in('t.type', array(self::TYPE_MODERN_MATERIALS, self::TYPE_NOT_MODERN_MATERIALS))->row();
        $event_id = null;
        if(count($event))
            $event_id = $event->id;
        $type = self::TYPE_NOT_MODERN_MATERIALS;
        if(Model_Materials::TYPE_MODERN == $modern_status) {
            $type = self::TYPE_MODERN_MATERIALS;
        }
        $model->doSave(array(
					'owner_id' => $owner_id,
					'resource_id' => $materials_id,
					'type' => $type,
					'users_id' => $users_id,
                    'is_read' => 0
				), $event_id);
    }
    public static function updateEvent($owner_id, $users_id, $resource_id, $event_type) {
        $model = new self;
        $event = $model->where('owner_id', $owner_id)
                      ->where('type', $event_type)
                      ->where('users_id', $users_id)
                      ->where('resource_id', $resource_id)
                      ->row();
        $event_id = null;
        if(count($event))
            $event_id = $event->id;
        $model->doSave(array(
					'owner_id' => $owner_id,
					'resource_id' => $resource_id,
					'type' => $event_type,
					'users_id' => $users_id,
                    'is_read' => 0
				), $event_id);
    }
    public function getData($users_id, $page, $perPage, $owner_id = false)
    {
        $users_id = intval($users_id);
        $favorites = api::getReferences()->_new()
                            ->where('object_id', $users_id)
                            ->where('ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)
                            ->forSelect(array('reference_id'=>'id'));
        $userFavorites = array_keys($favorites);

        if(!count($userFavorites))
            $userFavorites[] = 0;

        if ($owner_id == $users_id) {
            // Просматриваем свой профиль
            // Нужно добавить еще и материалы избранных
            $userFavoritesWithCurrentUser = array_merge($userFavorites, array($users_id));    
        } else {
            // Если чужой пользователь, то показываем только пользователя
            $owner_id = $users_id;
            $userFavoritesWithCurrentUser = array($users_id);
        }
        

        list($data, $paginator) = $this->_new(array('t.id'))
                                        ->in("t.owner_id", $userFavoritesWithCurrentUser)
                                        ->where("t.type", self::TYPE_NOT_MODERN_MATERIALS, "!=")
                                        ->where("t.id NOT IN (SELECT id FROM Model_Events WHERE owner_id = $users_id AND type = ".self::TYPE_CLOTHESADD." UNION SELECT id FROM Model_Events WHERE owner_id IN (".implode(',', $userFavorites).") AND type = ".self::TYPE_FAVORITEUSERS.")")
                                        ->order("t.date DESC")

                                        ->pageRows($page, $perPage);
        $result = array();

        if(count($data)) {
            $ids = array();
            foreach($data as $row) {
                $ids[] = $row['id'];
            }
            
            $data = $this->_new(array("t.type", "event_id"=>"t.id", "t.counter", 'event_is_read'=>'t.is_read'))
                        ->in('t.id', $ids)
                        ->joinLeft(array('us' => api::USERS), 'us.id = t.users_id', array('author_event_avatar' => 'us.avatar', 'author_event_name' => 'us.name', 'author_event_sname'=>'us.sname', 'author_user_id'=>'us.id', 'author_event_sex'=>'us.sex'))
                        ->joinLeft(array('it' => api::ITEMS), 'it.id = t.resource_id', array(
                                                'items_name' => "it.name",
                                                'items_id' => "it.id",
                                                'items_stitle' => "it.stitle",
                                                "it.adidas_link",
                                                "items_image" => "it.image",
                                                "it.is_has_review"))
                        //->joinLeft(array('ref' => api::REFERENCES), 'ref.reference_id = it.id and ref.ref_type = ' . Model_References::REF_TYPE_USER_ITEMS . ' and ref.object_id = '.Zend_Auth::getInstance()->getIdentity()->id, array())
                        ->joinLeft(array('ref' => api::REFERENCES), 'ref.reference_id = it.id and ref.ref_type = ' . Model_References::REF_TYPE_USER_ITEMS . ' and ref.object_id = '. $owner_id, array())
                        ->joinLeft(array('meta'=> api::META), 'meta.resource_id = ref.id and meta.modules_id = '.Model_Meta::ITEMS_REFERENCE_USERS." and meta.key='".Model_Meta::ITEMS_REFERENCE_USERS_KEY."'", array('status_clothes'=>'meta.int_value'))
                        ->joinLeft(array('mrt' => api::MATERIALS), 'mrt.id = t.resource_id', array(
                                                'materials_name' => "mrt.name",
                                                'materials_users_id' => "mrt.users_id",
                                                'materials_id' => "mrt.id",
                                                'materials_is_modern' => "mrt.is_modern",
                                                'materials_stitle' => "mrt.stitle",
                                                "materials_comments" => "mrt.comments",
                                                "materials_rate" => "mrt.rate",
                                                "materials_category"=>"mrt.category_id",
                                                "materials_date" => "mrt.date",))
                        ->joinLeft(array('mt' => api::META), "mt.resource_id = mrt.id AND mt.key IN ('short_text', 'mileage', 'photos', 'routes_type', 'name_point_start', 'name_point_finish', 'banner_preview', 'date_start_event') AND mt.modules_id = ".Model_Meta::MATERIALS, array(
                                    "mt.text_value", "mt.date_value", "mt.value", "mt.double_value", "mt.key"))
                        ->joinLeft(array('fl' => api::FILES), "fl.id = mt.file_id", array('fl.path'))
                        ->joinLeft(array('lv' => api::LIST_VALUES), "lv.id = mt.list_value_id",
                        						array('list_value' => 'lv.value'))
                        ->order('FIELD(t.id, ' . implode(', ', $ids) . ')')
                        ->rows();
            foreach($data as $row) {
                $result[$row->event_id]['event_id'] = $row->event_id;
                $result[$row->event_id]['event_is_read'] = $row->event_is_read;
                $result[$row->event_id]['type'] = $row->type;
                $result[$row->event_id]['counter'] = $row->counter;
                $result[$row->event_id]['author_event_sex'] = $row->author_event_sex;
                $result[$row->event_id]['author_event_avatar'] = $row->author_event_avatar;
                $result[$row->event_id]['author_event_name'] = $row->author_event_name;
                $result[$row->event_id]['author_event_sname'] = $row->author_event_sname;
                $result[$row->event_id]['author_user_id'] = $row->author_user_id;
                $result[$row->event_id]['items_name'] = $row->items_name;
                $result[$row->event_id]['items_image'] = $row->items_image;
                $result[$row->event_id]['items_stitle'] = $row->items_stitle;
                $result[$row->event_id]['items_id'] = $row->items_id;
                $result[$row->event_id]['items_status_clothes'] = $row->status_clothes;
                $result[$row->event_id]['adidas_link'] = $row->adidas_link;
                $result[$row->event_id]['is_has_review'] = $row->is_has_review;
                $result[$row->event_id]['materials_name'] = $row->materials_name;
                $result[$row->event_id]['materials_id'] = $row->materials_id;
                $result[$row->event_id]['materials_is_modern'] = $row->materials_is_modern;
                $result[$row->event_id]['materials_users_id'] = $row->materials_users_id;
                $result[$row->event_id]['materials_stitle'] = $row->materials_stitle;
                $result[$row->event_id]['materials_comments'] = $row->materials_comments;
                $result[$row->event_id]['materials_rate'] = $row->materials_rate;
                $result[$row->event_id]['materials_category'] = $row->materials_category;
                $result[$row->event_id]['materials_date'] = $row->materials_date;
                if(!empty($row->text_value))
                    $result[$row->event_id][$row->key] = $row->text_value;
                if(!empty($row->value))
                    $result[$row->event_id][$row->key] = $row->value;
                if(!empty($row->double_value))
                    $result[$row->event_id][$row->key] = $row->double_value;
                if(!empty($row->list_value))
                    $result[$row->event_id][$row->key] = $row->list_value;
                if(!empty($row->path))
                    $result[$row->event_id][$row->key] = $row->path;
                
                if(!empty($row->date_value))
                    $result[$row->event_id][$row->key] = $row->date_value;
            }
        }
   
        return array($result, $paginator);
    }
}
