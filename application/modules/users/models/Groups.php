<?php

/**
 * Модель для юзеров
 */
class Model_Groups extends DR_Models_AbstractTable {
    
    const GUEST = 4;
    const USERS = 1;
    const ADMIN = 2;
    const FAMOUS = 8;
    const COACH = 9;
    const RUNBASE_UNREGISTER_CARD_MASTER = 10;
}