<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
		<div class="section_inner">
            <ul class="tabs_inner">
                <?if($this->user->count_run):?>
            		<li id="run" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_run')?>'">
                        <?if(Model_Users::isCurrentUserProfile()):?>
                            <?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Я пробежал', 'Я пробежала'))?>
                        <?else:?>
                            <?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Пробежал', 'Пробежала'))?>
                        <?endif;?> 
                        <span><?echo $this->user->count_run?></span>
                    </li>
                <?endif;?>
                <?if(Model_Users::isCurrentUserProfile() && $this->user->count_like_run):?>
        		  <li id="like" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_like')?>'">Хочу пробежать <span><?echo $this->user->count_like_run?></span></li>
                <?endif;?>
                <?if($this->user->count_routes_add):?>
                    <li id="modern" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_modern')?>'"><?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Добавил', 'Добавила'))?> <span><?echo $this->user->count_routes_add?></span></li>
                <?endif;?>
                <?if(Model_Users::isCurrentUserProfile() && $this->user->count_routes_draft):?>
                    <li id="draft" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_draft')?>'">Черновики <span><?echo $this->user->count_routes_draft?></span></li>
                <?endif;?>
           	</ul>
            <div class="box_inner visible <?if(in_array($this->type, array('draft', 'modern'))):?>rough<?endif;?>">
                <div id="container">
                    <?echo $this->action('blockmaterials', 'routes', 'users', array('id'=>$this->user->id, 'type'=>$this->type))?>
                </div>
            </div>
        </div>
    </div>
 </div>
<script type="text/javascript">
    $('li#<?echo $this->type?>').addClass('current');
    MenuNavigator.setCurrentProfileMenu('routes');
    defaultPaginator = new Paginator('/users/routes/blockmaterials');
    defaultPaginator.setParam('id', <?echo $this->user->id?>).setParam('type', '<?echo $this->type?>');
</script> 