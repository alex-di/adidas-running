

<script type="text/javascript" src="/design/modules/plupload/js/plupload.full.js"></script>
<script type="text/javascript" src="/design/modules/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<script type="text/javascript" src="/design/modules/plupload/my/scripts.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/bootstrap-wysiwyg.js"></script>

<link href="/design/css/modals.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/design/js/modals.js"></script>
<script type="text/javascript" src="/design/js/profile/functions.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geometry"></script>
<script src="/design/js/google/config.js"></script>
<?
    $isEdit = isset($this->data);
    $postData = $isEdit ? unserialize($this->data['post_data']) : array();
?>
<script type="text/javascript">
    Materials.gallery_upload_settings.max_file_size = '<?echo $this->settings['usphoto_max_size']?>kb';
    Materials.gallery_upload_settings.filters.extensions = '<?echo implode(',', $this->lists['usphoto_extension'])?>';
    Materials.gallery_upload_settings.url = '/files/index/upload/moduleName/usphoto';
    Materials.gallery_upload_settings.multi_selection = true;
    Materials.gallery_upload_settings.multipart_params.PHPSESSID = '<?echo session_id()?>';  
    var map;
    var polyline = new google.maps.Polyline({strokeColor: '#FFEC2C', strokeWeight: 8});
    var markers = [];
    function initialize() {
        var mapSelector = 'map-canvas';
        var mapDiv = document.getElementById(mapSelector);
        var mapOptions = {
            zoom: 13,
            scrollwheel:false,
            center: new google.maps.LatLng(55.73, 37.63),//Moscow,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl:false,
            panControl:false,
            zoomControl: false,
            styles: STYLES_MAP
        };
        map = new google.maps.Map(mapDiv, mapOptions);
        initControl(map, false);
        polyline.setMap(map);

        google.maps.event.addListener(map, 'click', function(e) {
            var path = polyline.getPath();
            path.push(e.latLng);
            markers.push(initMarker(e.latLng, 1 == path.length));
            polylineCompleteDraw();
        });
        <?if($isEdit):?>
       	    var overviewPath = <?echo $postData['overview_path']?>;
            var path = [];
            for(var i in overviewPath) {
                var latLng = new google.maps.LatLng(overviewPath[i].jb, overviewPath[i].kb);
           	    path.push(latLng);
                markers.push(initMarker(latLng, 1 == path.length));
            }
            polyline.setPath(path);
            map.fitBounds(getBoundsForPoly(polyline));
        <?endif;?>
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setTypes([]);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // Inform the user that the place was not found and return.
                return;
            }
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    function initMarker(latLng, isFirst) {
        var options = {map: map, position: latLng, icon: {size: new google.maps.Size(26, 26),url: '/design/images/map_img/point.png', anchor: new google.maps.Point(12, 12)}};
        if(isFirst) {
            options.icon.url =  '/design/images/map_img/flag_small.png';
            options.icon.anchor = new google.maps.Point(6, 20);
        }
        var marker = new google.maps.Marker(options);
        google.maps.event.addListener(marker, 'dblclick', function(e) {
            var path = [];

            for(var index in markers) markers[index].setMap(null);
            markers = [];
            polyline.getPath().forEach(function(latLng) {
                if(e.latLng.lat() != latLng.lat() || e.latLng.lng() != latLng.lng()) {
                    path.push(latLng);
                    markers.push(initMarker(latLng, 1 == path.length));
                }
            });
            polyline.setPath(path);
            polylineCompleteDraw();
        });


        return marker;
    }
    function polylineCompleteDraw(){
        var path = [];
        polyline.getPath().forEach(function(latLng) {
            path.push({jb: latLng.lat(), kb: latLng.lng()});
        });
        if (path.length) {
            $('input[name="start_lat"]').val(path[0].jb);
            $('input[name="start_lng"]').val(path[0].kb);
            $('input[name="end_lat"]').val(path[path.length - 1].jb);
            $('input[name="end_lng"]').val(path[path.length - 1].kb);
            var bounds = getBoundsForPoly(polyline);
            polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
            var center = bounds.getCenter();
            var northEast = bounds.getNorthEast();
            var radius = getRadius(center, northEast);
            //console.log(radius);	
            $('input[name="center_lat"]').val(center.lat());
            $('input[name="center_lng"]').val(center.lng());
            $('input[name="radius_square"]').val(radius);
            $('input[name="overview_path"]').val(JSON.stringify(path));
            var mileage = Math.round(polyLengthInMeters) / 1000;
                mileage = Math.round(mileage * 100) / 100;


            if (mileage < 99.99) {
                $('input[name="mileage"]').val(mileage);
                $('.route_length').html(mileage + '<span>км</span>');
            } else {
                var coords = polyline.getPath().getArray(),
                    lastLat = coords[coords.length - 1].lb,
                    lastLng = coords[coords.length - 1].mb;
                
                var path = [];
                for(var index in markers) markers[index].setMap(null);
                markers = [];
                polyline.getPath().forEach(function(latLng) {
                    if (lastLat != latLng.lat() || lastLng != latLng.lng()) {
                        path.push(latLng);
                        markers.push(initMarker(latLng, 1 == path.length));
                    }
                });

                polyline.setPath(path);
                polylineCompleteDraw();

                $.showInfo('Длина твоего маршрута превышает 99 км 99 м! Пожалуйста, сократи дистанцию');
            }
        }
    }
    function getRadius(p1, p2){
        var R = 6371; // Radius of the Earth in km
        var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
        var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
                    Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d; 
    }
    function saveRoute(status, isPreview) {
        var data = {};
        if(undefined != isPreview && isPreview)
            data.isNeedPreview = 1;
        data.is_modern = status;
        data.name = $('input[name="name"]').val();
        data.start_lat = $('input[name="start_lat"]').val();
        data.start_lng = $('input[name="start_lng"]').val();
        data.end_lat = $('input[name="end_lat"]').val();
        data.end_lng = $('input[name="end_lng"]').val();
        data.center_lat = $('input[name="center_lat"]').val();
        data.center_lng = $('input[name="center_lng"]').val();
        data.radius_square = $('input[name="radius_square"]').val();
        data.overview_path = $('input[name="overview_path"]').val();
        data.mileage = $('input[name="mileage"]').val();
        data.routes_type = $('input[name="routes_type"]').val();
        data.name_point_start = $('input[name="name_point_start"]').val();
        data.name_point_finish = $('input[name="name_point_finish"]').val();
        data.dragElements = searializeDropElement();
        $.post('/users/routes/save<? echo $isEdit ? '/id/'.$this->data['id'] : ''?>', data, function(data) {
            if(data.is_valid) {
                if(undefined != isPreview && isPreview)
                    window.location = data.article_url;
                else
                    $.showInfo('Успешно сохранено', '<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_draft')?>');
            } else {
                for(var name in data) {
                    var object = $('[name="'+name+'"]');
                    if(object.hasClass('isParentView'))
                        object = object.parent();
                    if('' == $.trim(data[name])) {
                        object.removeClass('error');
                    } else {
                        object.addClass('error');
                    }
                }
                $.showInfo('Неверно заполненые поля');
            }
        }, 'json');
    }
</script>

<div class="sidebar_content">
    <form>
        <div class="grey_box route_name">
            <span class="route_ico"></span>                                
            <input type="text" class="yellow_input article_title" placeholder="название маршрута" name="name" value="<?echo $isEdit ? $postData['name'] : ''?>"/>
        </div>
        <!-- -->
       
        <div class="grey_box with_arrow">
            <div class="grey_box_title">построение маршрута</div>
            <input id="searchTextField" type="text" class="yellow_input" placeholder="найти место" />
            <div id="map-canvas" style="width: 700px; height: 450px"></div>
            <div class="route_info">
                <div class="route_length"><?echo $isEdit ? $postData['mileage'] : '0'?><span>км</span></div>
                <div class="title">добавление машрута</div>
                <div class="sub_text">Щелкните на карту, чтобы поставить точку. Щелкните два раза, чтобы удалить.</div>
                <input type="text" class="yellow_input" placeholder="начальная точка" name="name_point_start"  value="<?echo $isEdit ? $postData['name_point_start'] : ''?>"/>
                <input type="text" class="yellow_input delimiter" placeholder="конечная точка" name="name_point_finish"  value="<?echo $isEdit ? $postData['name_point_finish'] : ''?>"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['start_lat'] : ''?>" name="start_lat"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['start_lng'] : ''?>" name="start_lng"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['end_lat'] : ''?>" name="end_lat"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['end_lng'] : ''?>" name="end_lng"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['center_lat'] : ''?>" name="center_lat"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['center_lng'] : ''?>" name="center_lng"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['radius_square'] : ''?>" name="radius_square"/>
                <input type="hidden" value="<?echo $isEdit ? htmlspecialchars($postData['overview_path']) : ''?>" name="overview_path"/>
                <input type="hidden" value="<?echo $isEdit ? $postData['mileage'] : ''?>" name="mileage"/>
                <div class="combo routes_type">
                    <div class="current">тип  маршрута</div>
                    <div class="arrow"></div>
                    <ul>
                        <?foreach($this->lists['routes_type'] as $key=>$val):
                            if(Model_Materials::ROUTE_TYPE_NOT_SELECTED != $key):
                        ?>
                                <li data-id="<?echo $key?>"><?echo $val?></li>
                            <?endif;?>
                        <?endforeach?>
                    </ul>
                    <input class="isParentView" name="routes_type" type="text" value="<?echo $isEdit ? $postData['routes_type'] : ''?>"/>
                </div>
                <?if($isEdit && Model_Materials::ROUTE_TYPE_NOT_SELECTED != $postData['routes_type']):?>
                    <script type="text/javascript">
                        $("div.routes_type .current").text($('div.routes_type li[data-id="<?echo $postData['routes_type']?>"]').text());
                    </script>
                <?endif;?>
            </div>
        </div>
        <div id="drop_element_box" class="drag_box drag_box_element">
            <?echo $this->partial('partials/profile/edit_materials.tpl', array('data'=>isset($postData['dragElements']) ? $postData['dragElements'] : array()))?>
        </div>
        <div class="save_material" onclick="saveRoute(<?echo Model_Materials::TYPE_DRAFTS?>)"><span></span> в черновики</div>
        <div class="view_material" onclick="saveRoute(<?echo Model_Materials::TYPE_DRAFTS?>, true)"><span></span>предварительный просмотр</div>
        <div class="submit_material" onclick="saveRoute(<?echo Model_Materials::TYPE_NOT_MODERN?>)"><span></span>отправить на модерацию</div>
        <div style="clear: both;"></div>
    </form>
    <aside> 
        <div style="position:fixed"  class="grey_sidebar">
            <div class="title">Добавить</div>
            <div class="add_point text" onclick="addText()">текст<span></span></div>
            <div class="add_point photo" onclick="addPhoto()">фото<span></span></div>
            <div class="add_point video" onclick="addVideo()">видео<span></span></div>
            <div class="add_point audio" onclick="addPlaylist()">аудио<span></span></div>
        </div>                           
    </aside>
</div>
    