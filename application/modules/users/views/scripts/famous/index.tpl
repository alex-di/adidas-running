<?php
    $author_id = (isset($this->author_id) && $this->author_id !== false) ? $this->author_id : false;
?>
<div id="peopleColumn" style="margin: 20px 0 0">
    <ul>
        <li class="peopleColumn__articles <?php echo ($author_id === false) ? 'peopleColumn__articles-active' : '' ?>" onclick="window.location.href='/users/famous/'">
            <a href="javascript:void(0)">все статьи</a>
            <span class="triangleArrow"></span>
            <span class="circleArrow"></span>
        </li>
    <?php
        foreach ($this->authors as $author) {
    ?>
        <li class="peopleColumn__author <?php echo ($author_id == $author['id']) ? 'peopleColumn__author-active' : '' ?>" data-author="<?php echo $author['id'] ?>" onclick="window.location.href='/users/famous/?author_id=<?php echo $author['id'] ?>'">
            <span class="author__avatar">
            <?php
                $avatar = Model_Files::getPreview($author['avatar'], 260);
                $avatar_gray = Model_Files::getGrayscale($avatar);
            ?>
                <img src="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $avatar_gray ?>" alt="<?php echo $author['name'] . ' ' . $author['sname'] ?>" width="80" height="80" class="author__avatar-gray">
                <img src="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $avatar ?>" alt="<?php echo $author['name'] . ' ' . $author['sname'] ?>" width="80" height="80" class="author__avatar-color">
                <div class="author__avatar-over"></div>
            </span>
            <h3 class="author__name"><?php echo $author['name'] . '<br>' . $author['sname'] ?></h3>
            <?php
                if ((int) $author['article_count'] !== 0) {
            ?>
            <span class="author__articleCounter">Статьи: <?php echo $author['article_count'] ?></span>
            <?php
                }
            ?>
            <span class="triangleArrow"></span>
        </li>
        <?php
        }
        ?>
    </ul>
</div>
<div id="container" class="containerPeopleArticles" style="margin: -15px 0 0">
    <?php
        echo $this->action('index', 'people', 'materials', array('author_id' => $author_id));
    ?>
</div>
<script type="text/javascript">
    MenuNavigator.setCurrentMenu('users/famous');
</script>
<div style="clear:both"></div>
