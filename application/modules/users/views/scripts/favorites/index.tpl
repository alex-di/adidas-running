<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
		<div class="section_inner">
        	<ul class="tabs_inner">
                <?
                $typeData = 'my';
                if($this->favoritesData['favorites']):
                    $typeData = 'favorites';
                ?>
        		  <li class="current" onclick="updateStrip('favorites')">Меня добавили <span><?echo $this->favoritesData['favorites']?></span></li>
        		<?endif;?>
                <li <?if(!$this->favoritesData['favorites']):?>class="current"<?endif;?> onclick="updateStrip('my')">
                    <?if(Model_Users::isCurrentUserProfile()):?>
                        <?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Я добавил', 'Я добавила'))?>
                    <?else:?>
                        <?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Добавил', 'Добавила'))?>
                    <?endif;?> 
                    <span><?echo $this->favoritesData['myFavorites']?></span>
                </li>
        	</ul>
            <div class="box_inner visible">
                <div id="container">
                    <?echo $this->action('blockmaterials', 'favorites', 'users', array('id'=>$this->user->id, 'type'=>$typeData))?>
                </div>
            </div>
        </div>
    </div>
 </div>
<script type="text/javascript">
    MenuNavigator.setCurrentProfileMenu('favorites');
    defaultPaginator = new Paginator('/users/favorites/blockmaterials');
    defaultPaginator.setParam('id', <?echo $this->user->id?>)
                    .setParam('type', <?echo $typeData?>);
    function updateStrip(type) {
        defaultPaginator.resetPage();
        defaultPaginator.setParam('type', type)
        defaultPaginator.getMaterials(true);
    }
</script> 