<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
        <?if(Model_Users::isCurrentUserProfile()):?>
    		<div class="section_inner">
            	<ul class="tabs_inner clothes_tab">
                    <?if($this->clothesStatus['have']):?>
            		  <li class="current" onclick="updateStrip('have')">У меня есть <span data-counter="<?echo $this->clothesStatus['have']?>"><?echo $this->clothesStatus['have']?></span></li>
            		<?endif;?>
                    <?if($this->clothesStatus['like']):?>
                        <li onclick="updateStrip('like')">Я хочу <span data-counter="<?echo $this->clothesStatus['like']?>"><?echo $this->clothesStatus['like']?></span></li>
                    <?endif;?>            	</ul>
                <div class="box_inner visible">
                    <div id="container">
                        <?echo $this->action('blockmaterials', 'clothes', 'users', array('id'=>$this->user->id))?>
                    </div>
                </div>
            </div>
      <?else:?>
            <div id="container">
                <?echo $this->action('blockmaterials', 'clothes', 'users', array('id'=>$this->user->id))?>
            </div>
      <?endif;?>
    </div>
 </div>
<script type="text/javascript">
    MenuNavigator.setCurrentProfileMenu('clothes');
    defaultPaginator = new Paginator('/users/clothes/blockmaterials');
    defaultPaginator.setParam('id', <?echo $this->user->id?>);
    function updateStrip(type) {
        defaultPaginator.resetPage();
        defaultPaginator.setParam('type', type)
        defaultPaginator.getMaterials(true);
    }
</script> 