

<link rel="stylesheet" type="text/css" href="/design/modules/imgareaselect/css/imgareaselect-default.css" media="screen" />
<script type="text/javascript" src="/design/modules/imgareaselect/scripts/jquery.imgareaselect.js"></script>

<script type="text/javascript" src="/design/modules/plupload/js/plupload.full.js"></script>
<script type="text/javascript" src="/design/modules/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<script type="text/javascript" src="/design/modules/plupload/my/scripts.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="/design/modules/bootstrap/js/bootstrap-wysiwyg.js"></script>

<link href="/design/css/modals.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/design/js/modals.js"></script>
<script type="text/javascript" src="/design/js/profile/functions.js"></script>

<link rel="stylesheet" type="text/css" href="/design/modules/jqueryui/css/ui-lightness/jquery-ui-1.9.2.custom.min.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/design/admin/plugins/timepicker/timepicker.css" media="screen" />
<script type="text/javascript" src="/design/admin/plugins/timepicker/timepicker.js"></script>

<script type="text/javascript">
    Materials.gallery_upload_settings.max_file_size = '<?echo $this->settings['usphoto_max_size']?>kb';
    Materials.gallery_upload_settings.filters.extensions = '<?echo implode(',', $this->lists['usphoto_extension'])?>';
    Materials.gallery_upload_settings.url = '/files/index/upload/moduleName/usphoto';
    Materials.gallery_upload_settings.multi_selection = true;
    Materials.gallery_upload_settings.multipart_params.PHPSESSID = '<?echo session_id()?>';  
</script>
<div class="modal" id="edit_preview" style="display: none">
    <div class="title">редактирование превью</div>
    <div class="sub_text">Осталось выбрать квадратную область для маленьких фотографий.<br /> Выбранная миниатюра будет использоваться в новостях, личных сообщениях и комментариях.</div>
    <!--<div class="image_preview_block">
        <img src=""/>
    </div>-->
    <img  src="" alt="img" />
    
    <div class="save" onclick="saveImageArea()"><span></span>сохранить изменения</div>
    <div class="close" onclick="$.modal().close();"><span></span>отменить</div>
</div>
<div class="sidebar_content">
    <form>
        <!-- -->
        <?
            $isEdit = isset($this->data);
            $postData = $isEdit ? unserialize($this->data['post_data']) : array();
            $category_list = array(Model_Materials::CATEGORY_COACH_ARTICLES => "статья тренера",
												Model_Materials::CATEGORY_INSPIRING_ARTICLES => "вдохновляющая статья",
												Model_Materials::CATEGORY_ROUTE_ARTICLES => "статья о маршрутах",
                                                Model_Materials::CATEGORY_MICOACH_ARTICLES => "статья для micoach",
                                                Model_Materials::CATEGORY_EVENTS => "событие",);
		?>
        <div class="grey_box">
            <div class="combo category">
                <div class="current">выберите раздел</div>
                <div class="arrow"></div>
                <ul>
            	   <?php foreach($category_list as $key=>$val):?>
                   		<li data-id='<?php echo $key?>'><?php echo $val?></li>
                   <?php endforeach;?>
                </ul>
                <input type="text" value="<?echo $isEdit ? $postData['category_id'] : ''?>" name="category_id" class="isParentView"/>
            </div>
            <div class="date" onclick="showDatatimepicker()" <?if(!$isEdit || Model_Materials::CATEGORY_EVENTS != $postData['category_id']):?>style="display: none"<?endif;?>>
                <div><?echo $isEdit && isset($postData['date_event']) ? $postData['date_event'] : 'дата'?></div>
                <span></span>
                <input type="text" value="<?echo $isEdit && isset($postData['date_event']) ? $postData['date_event'] : ''?>" name="date_event" style="opacity: 0;" class="isParentView"/>
            </div>
            
            <script type="text/javascript">
                $(function(){
                   $("input[name='date_event']").datetimepicker({dateFormat: 'yy-mm-dd', date: new Date(), onSelect: function(dataText, ui){
                        $('.date > div').text(dataText);
                   }});
                   $('input[name="category_id"]').change(function(){
                        if("<?echo Model_Materials::CATEGORY_EVENTS?>" == $(this).val())
                            $('.date').show();
                        else
                            $('.date').hide();
                   }); 
                });
                function showDatatimepicker(){
                    $("input[name='date_event']").datetimepicker('show');
                }
            </script>
            <div style="clear: both;"></div>
            <?if($isEdit && Model_Materials::CATEGORY_NOT_SELECTED != $postData['category_id']):?>
                <script type="text/javascript">
                    $("div.category .current").text($('div.category li[data-id="<?echo $postData['category_id']?>"]').text());
                </script>
            <?endif;?>
            <input name="name" type="text" class="yellow_input article_title" placeholder="заголовок статьи" value="<?echo $isEdit ? $postData['name'] : ''?>"/>
        </div>
        <!-- -->
       <?$isLoadPreview = $isEdit && !empty($postData['preview']);
         $isLoadBanner = $isEdit && !empty($postData['banner']);
       ?>
        <div class="grey_box with_arrow">
            <div class="grey_box_title">превью и обложка  статьи</div>
            <div class="load_area priview">
                <div class="img_area <?if($isLoadPreview):?>upload<?endif;?>">
                    <?if($isLoadPreview):?>
                        <img src="<?echo $postData['preview']?>" style="display: none;"/>
                    <?endif;?>
                </div>
                <div class="title">превью статьи</div>
                <div class="sub_text">Немного про требования к фотографии, максимум 6 мб. Допустимые расширения .jpg .png .gif</div>
                <div id="upload_pickfiles_1" class="black_button load uploader"><span></span><?if($isLoadPreview):?>загрузить другое фото<?else:?>загрузить фото<?endif;?></div>
                <div class="black_button edit edit_preview_link disabled" onclick="editArea(this, 'preview', {minWidth: 260, minHeight: 260, x1: 0, y1: 0, x2: 260, y2: 260, aspectRatio:'1:1'})"><span></span>редактировать</div>
                <input type="hidden" name="preview" value="<?echo $isLoadPreview ? $postData['preview'] : ''?>"/>
                <input type="hidden" name="preview_x_start" value="<?echo $isLoadPreview ? $postData['preview_x_start'] : ''?>"/>
                <input type="hidden" name="preview_y_start" value="<?echo $isLoadPreview ? $postData['preview_y_start'] : ''?>"/>
                <input type="hidden" name="preview_width" value="<?echo $isLoadPreview ? $postData['preview_width'] : ''?>"/>
                <input type="hidden" name="preview_height" value="<?echo $isLoadPreview ? $postData['preview_height'] : ''?>"/>
            </div>
            <div class="load_area main_img">
                <div  class="img_area <?if($isLoadBanner):?>upload<?endif;?>">
                    <?if($isLoadBanner):?>
                        <img src="<?echo $postData['banner']?>"  style="display: none;"/>
                    <?endif;?>
                </div>
                <div class="title">обложка статьи</div>
                <div class="sub_text">Немного про требования к фотографии, максимум 6 мб. Допустимые расширения .jpg .png .gif</div>
                <div id="upload_pickfiles_2" class="black_button load uploader isbanner "><span></span><?if($isLoadBanner):?>загрузить другое фото<?else:?>загрузить фото<?endif;?></div>
                <div class="black_button edit edit_preview_link disabled" onclick="editArea(this, 'banner', {minWidth: 1400, minHeight: 700, x1: 0, y1: 0, x2: 1400, y2: 700, aspectRatio: '2:1'})"><span></span>редактировать</div>
                <input type="hidden" name="banner" value="<?echo $isLoadBanner ? $postData['banner'] : ''?>"/>
                <input type="hidden" name="banner_x_start" value="<?echo $isLoadBanner ? $postData['banner_x_start'] : ''?>"/>
                <input type="hidden" name="banner_y_start" value="<?echo $isLoadBanner ? $postData['banner_y_start'] : ''?>"/>
                <input type="hidden" name="banner_width" value="<?echo $isLoadBanner ? $postData['banner_width'] : ''?>"/>
                <input type="hidden" name="banner_height" value="<?echo $isLoadBanner ? $postData['banner_height'] : ''?>"/>
                <input type="hidden" name="banner_id" value="<?echo $isLoadBanner ? $postData['banner_id'] : ''?>"/>
            </div>
            <?
                DR_View_Helper_Upload::$already = true;
                echo $this->upload('upload_pickfiles_1', 'usbanner', 'flash', array('error'=>'onErrorUpload', 'fileUploaded'=>'uploadHeadPhoto'));
                echo $this->upload('upload_pickfiles_2', 'uswall', 'flash', array('error'=>'onErrorUpload', 'fileUploaded'=>'uploadHeadPhoto'));?>
            <div style="clear: both;"></div>
        </div>
        <script type="text/javascript">

            // максимальная ширина области выбора превью
            var maxPreviewWidth = 600;
            var previewBlockWidth = 200;
            var previewBlockHeight = 200;
            // настройки превью 
            var settings = {
                banner: {preview: {width: 425, height: 260}, absolute: {width: 0, height: 0}, is_ready: false},
                preview: {preview: {width: 260, height: 260}, absolute: {width: 0, height: 0}, is_ready: false}
            };
            <?if($isLoadBanner):?>
                var bannerImg = new Image();
                bannerImg.onload = function(){
                    settings.banner.absolute.width = bannerImg.width;
                    settings.banner.absolute.height = bannerImg.height;
                    settings.banner.is_ready = true;
                    initImagesPreview('banner');
                    showButtonPreviewEdit('banner');
                };
                bannerImg.src = '<?echo $postData['banner']?>';
            <?endif;?>
            <?if($isLoadPreview):?>
                var previewImg = new Image();
                previewImg.onload = function(){
                    settings.preview.absolute.width = previewImg.width;
                    settings.preview.absolute.height = previewImg.height;
                    settings.preview.is_ready = true;
                    initImagesPreview('preview');
                    showButtonPreviewEdit('preview');
                };
                previewImg.src = '<?echo $postData['preview']?>';
            <?endif;?>
            
            var curretnEditPreviewButton = null;
            function uploadHeadPhoto(up, file, response) {
                var jsa = $.parseJSON(response.response);
                if('error' != jsa.status) {
                    var button = $('#'+up.settings.browse_button);
                    var namespace = 'preview';
                    if(button.hasClass('isbanner')) {
                         namespace = 'banner';
                         $('input[name="banner_id"]').val(jsa.id);
                    }
                    settings[namespace].absolute.width = jsa.xsize;
                    settings[namespace].absolute.height = jsa.ysize;
                    settings[namespace].is_ready = true;
                    curretnEditPreviewButton = button.next();
                    var photoBox = button.parent();
                    photoBox.find('.img_area').addClass('upload').empty().append('<img onload="curretnEditPreviewButton.click()" src="'+jsa.file+'" data-width="'+jsa.xsize+'" data-height="'+jsa.ysize+'"/>');
                    button.empty().html('<span></span>загрузить другое фото');
                    button.next().removeClass('disabled')
                                 .next().val(jsa.file)
                                 .next().val('');    
                } else {
                    $.showInfo(jsa.message);
                }
            }
            var currentOptionImageArea = {};
            function editArea(button, namespace, option) {
                if(settings[namespace].is_ready) {
                    var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
                        
                    option.minWidth = option.minWidth * scale;
                    if(undefined != option.maxWidth)
                        option.maxWidth = option.minWidth;
                    option.x2 = option.minWidth;
                    var previewHeight = maxPreviewWidth * settings[namespace].absolute.height / settings[namespace].absolute.width;
                    option.minHeight = option.minHeight * scale;
                    if(undefined != option.maxHeight)
                        option.maxHeight = option.minHeight;
                    option.y2 = option.minHeight;
                    
                    var currentImage = $(button).parent().find('.img_area img');
                    var src = currentImage.attr('src');
                    var currentSrc = $('#edit_preview > img').attr('src');
                    $('#edit_preview > img').attr('src', src).data('namespace', namespace);
                    
                    //$('#edit_preview .image_preview_block img').attr('src', src);
                    currentOptionImageArea = option;
                    if('' != $('input[name="'+namespace+'_x_start"]').val()) {
                        currentOptionImageArea.x1 = scale * parseFloat($('input[name="'+namespace+'_x_start"]').val());
                        currentOptionImageArea.y1 = scale * parseFloat($('input[name="'+namespace+'_y_start"]').val());
                        currentOptionImageArea.x2 = currentOptionImageArea.x1 + scale * parseFloat($('input[name="'+namespace+'_width"]').val());
                        currentOptionImageArea.y2 = currentOptionImageArea.y1 + scale * parseFloat($('input[name="'+namespace+'_height"]').val());
                    }
                    currentOptionImageArea.onSelectChange = changeSelect;
                    currentOptionImageArea.persistent = true;
                    currentOptionImageArea.onInit = initImageArea;
                    currentOptionImageArea.handles = 'corners';
                    currentOptionImageArea.movable = true;
                    //currentOptionImageArea.parent = '#edit_preview';
                    $('#edit_preview').modal().open({
                                                closeOnOverlayClick: false,
                                                closeOnEsc: false,
                                                isReposition: false,
                                                onOpen: function(){
                                                    //$('.uploader').hide();
                                                    //if(currentSrc == src) 
                                                       initImageAreaSelect($('#edit_preview > img'));
                                                    
                                                },
                                                onClose: function(){
                                                    //$('.uploader').show();
                                                    $('#edit_preview > img').imgAreaSelect({remove:true});
                                                    $('body').css('overflow', 'auto');
                                                },
                                                onReposition: function(){
                                                    //$('#edit_preview > img').imgAreaSelect({ instance: true }).update();
                                                }
                                                });
                }
            }
            function saveImageArea(){
                var img = $('#edit_preview > img');
                var ias = img.imgAreaSelect({ instance: true });
                setFields(img, ias.getSelection());
                $.modal().close();
            }
	    
    	    function changeSelect(img, selection) {
	            /*var namespace = $(img).data('namespace');
                var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
                    
                var selectionWidth = selection.width / scale;
                var selectionHeight = selection.height / scale;
                var x_start = selection.x1 / scale;
                var y_start = selection.y1 / scale;

                var scaleX = previewBlockWidth / selectionWidth;
                var scaleY = previewBlockHeight / selectionHeight;
                $('#edit_preview .image_preview_block img').css({
                    width: Math.round(scaleX * settings[namespace].absolute.width),
                    height: Math.round(scaleY * settings[namespace].absolute.height),
                    marginLeft: -Math.round(scaleX * x_start),
                    marginTop: -Math.round(scaleY * y_start)
                });*/
    	    }
	    
            function initImageAreaSelect(img){
                $(img).imgAreaSelect(currentOptionImageArea);
            }
            function initImageArea(img, selection){
                changeSelect(img, selection);
                setFields(img, selection);
                
                var top = ($(window).height() - $('#edit_preview').height()) / 4;
                console.log(top);
                top =  $('#edit_preview').offset().top - (top > 0 ? top : 0);
                window.scrollTo(0, top);
                //$('body').css('overflow', 'hidden');
                $('#edit_preview > img').imgAreaSelect({ instance: true }).update();
            }
            function selectEndImageArea(img, selection){
                setFields(img, selection);
            }
            function showButtonPreviewEdit(namespace) {
                $('input[name="'+namespace+'"]').parent().find('.edit_preview_link').removeClass('disabled');
            }
            function initImagesPreview(namespace) {
                var selectionWidth = parseFloat($('input[name="'+namespace+'_width"]').val());
                var selectionHeight = parseFloat($('input[name="'+namespace+'_height"]').val());
                var x_start = parseFloat($('input[name="'+namespace+'_x_start"]').val());
                var y_start = parseFloat($('input[name="'+namespace+'_y_start"]').val());
                var scaleX = settings[namespace].preview.width / selectionWidth;
                var scaleY = settings[namespace].preview.height / selectionHeight;
                $("input[name="+namespace+"]").parent().find(".img_area > img").css({
                    width: Math.round(scaleX * settings[namespace].absolute.width),
                    height: Math.round(scaleY * settings[namespace].absolute.height),
                    marginLeft: -Math.round(scaleX * x_start),
                    marginTop: -Math.round(scaleY * y_start),
                    display: 'block'
                });
            }
            function setFields(img, selection) {
       	        var namespace = $(img).data('namespace');
                var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
                    
                var selectionWidth = selection.width / scale;
                var selectionHeight = selection.height / scale;
                var x_start = selection.x1 / scale;
                var y_start = selection.y1 / scale;

                var scaleX = settings[namespace].preview.width / selectionWidth;
                var scaleY = settings[namespace].preview.height / selectionHeight;
                $("input[name="+namespace+"]").parent().find(".img_area > img").css({
                    width: Math.round(scaleX * settings[namespace].absolute.width),
                    height: Math.round(scaleY * settings[namespace].absolute.height),
                    marginLeft: -Math.round(scaleX * x_start),
                    marginTop: -Math.round(scaleY * y_start)
                });
                $('input[name="'+namespace+'_x_start"]').val(x_start);
                $('input[name="'+namespace+'_y_start"]').val(y_start);
                $('input[name="'+namespace+'_width"]').val(selectionWidth);
                $('input[name="'+namespace+'_height"]').val(selectionHeight);
            }  
            function saveArticle(status, isPreview) {
                var data = {};
                if(undefined != isPreview && isPreview)
                    data.isNeedPreview = 1;
                data.is_modern = status;
                data.date_event = $('input[name="date_event"]').val();
                data.category_id = $('input[name="category_id"]').val();
                data.name = $('input[name="name"]').val();
                data.preview = $('input[name="preview"]').val();
                data.preview_x_start = $('input[name="preview_x_start"]').val();
                data.preview_y_start = $('input[name="preview_y_start"]').val();
                data.preview_width = $('input[name="preview_width"]').val();
                data.preview_height = $('input[name="preview_height"]').val();
                data.banner = $('input[name="banner"]').val();
                data.banner_id = $('input[name="banner_id"]').val();
                data.banner_x_start = $('input[name="banner_x_start"]').val();
                data.banner_y_start = $('input[name="banner_y_start"]').val();
                data.banner_width = $('input[name="banner_width"]').val();
                data.banner_height = $('input[name="banner_height"]').val();
                data.dragElements = searializeDropElement();
                $.post('/users/articles/save<? echo $isEdit ? '/id/'.$this->data['id'] : ''?>', data, function(data) {
                    if(data.is_valid) {
                        if(undefined != isPreview && isPreview)
                            window.location = data.article_url;
                        else
                            $.showInfo('Успешно сохранено', '<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_draft_view')?>');
                    } else {
                        for(var name in data) {
                            var object = $('[name="'+name+'"]');
                            if(object.hasClass('isParentView'))
                                object = object.parent();
                            if('' == $.trim(data[name])) {
                                object.removeClass('error');
                            } else {
                                object.addClass('error');
                            }
                        }
                        $.showInfo('Неверно заполненые поля');
                    }
                }, 'json');
            }
        </script>
        <!-- -->
        <div id="drop_element_box" class="drag_box drag_box_element">
            <?echo $this->partial('partials/profile/edit_materials.tpl', array('data'=>isset($postData['dragElements']) ? $postData['dragElements'] : array()))?>
        </div>
        <?if(!$isEdit):?>
            <script type="text/javascript">
                addText();
            </script>
        <?endif;?>
        <div class="save_material" onclick="saveArticle(<?echo Model_Materials::TYPE_DRAFTS?>)"><span></span>в черновики</div>
        <div class="view_material" onclick="saveArticle(<?echo Model_Materials::TYPE_DRAFTS?>, true)"><span></span>предварительный просмотр</div>
        <div class="submit_material" onclick="saveArticle(<?echo Model_Materials::TYPE_NOT_MODERN?>)"><span></span>отправить на модерацию</div>
        <!--<div class="delete_article" onclick=""><span></span>удалить статью</div>-->
        <div style="clear: both;"></div>
    </form>
    
    <aside> 
        <div style="position:fixed" class="grey_sidebar">
            <div class="title">Добавить</div>
            <div class="add_point text" onclick="addText()">текст<span></span></div>
            <div class="add_point photo" onclick="addPhoto()">фото<span></span></div>
            <div class="add_point video" onclick="addVideo()">видео<span></span></div>
            <div class="add_point audio" onclick="addPlaylist()">аудио<span></span></div>
        </div>                           
    </aside>
    <div style="clear: both;"></div>                        
</div>


