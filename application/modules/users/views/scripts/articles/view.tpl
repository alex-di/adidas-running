<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
		<div class="section_inner">
            <ul class="tabs_inner">
                <?if($this->user->count_articles):?>
                    <li id="modern" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_articles_modern_view')?>'"><?echo DR_Api_Tools_Tools::getTextForSex($this->user->sex, array('Добавил', 'Добавила'))?> <span><?echo $this->user->count_articles?></span></li>
                <?endif;?>
                <?if(Model_Users::isCurrentUserProfile() && $this->user->count_articles_draft):?>
                    <li id="draft" onclick="window.location = '<?echo $this->url(array('id'=>$this->user->id), 'profile_articles_draft_view')?>'" class="">Черновики <span><?echo $this->user->count_articles_draft?></span></li>
                <?endif;?>
           	</ul>
            <div class="box_inner visible <?if('draft' == $this->type):?>rough<?endif;?>">
                <div id="container">
                    <?echo $this->action('blockmaterials', 'articles', 'users', array('id'=>$this->user->id, 'type'=>$this->type))?>
                </div>
            </div>
        </div>
    </div>
 </div>
<script type="text/javascript">
    MenuNavigator.setCurrentProfileMenu('article');
    $('li#<?echo $this->type?>').addClass('current');
    defaultPaginator = new Paginator('/users/articles/blockmaterials');
    defaultPaginator.setParam('id', <?echo $this->user->id?>).setParam('id', '<?echo $this->type?>');
</script> 