<script type="text/javascript">
    //layoutMode = 'fitRows';
</script>
<? echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>

<div class="section vertical">
    <? echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
    	<div id="container">
            <? if(!empty($this->user->card_number)): ?>                             
                <div style="cursor: default;" class="element card <?echo $this->user->sex == 'woman' ? 'women' : 'man'?>"><?= $this->user->card_number ?></div> <!-- women\man -->
            <?endif;?>
            <div class="element counter article_counter" <?if(!$this->user->count_articles):?>style="cursor: default;"<?else:?>onclick="window.location='<?echo $this->url(array('id'=>$this->user->id), 'profile_articles_modern_view')?>'"<?endif;?>>
                <div class="container">
                    <div class="number"><?echo $this->user->count_articles?></div><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_articles, array("статья добавлена", "статей добавлено", "статей добавлено", "статьи добавлено", "статей добавлено"))?>
                    <?php 
                        if (isset(Zend_Auth::getInstance()->getIdentity()->id) && Zend_Auth::getInstance()->getIdentity()->id == $this->user->id) {
                    ?>
                    <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_add')?>" class="add">добавить статью<span></span></a>
                    <?php
                        }
                    ?>
                    <div class="lines"></div>
                </div>                                    
            </div>            

            <div class="element counter route_counter" <?if(!$this->user->count_run):?>style="cursor: default;"<?else:?>onclick="window.location='<?echo $this->url(array('id'=>$this->user->id), 'profile_routes_run')?>'"<?endif;?>>
                <div class="route_map">
                    <?if($this->user->count_run):?>
                        <?foreach($this->rotesBackground as $image):?>
                            <img src="<?echo $image?>" alt="img" />
                        <?endforeach;?>
                    <?endif;?>
                </div>
                <div class="number"><?echo $this->user->count_run?></div> 
                <div class="lines"></div>
                <div class="text"><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_run, array("маршрут пройден", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено"))?></div>
                <?php 
                    if (isset(Zend_Auth::getInstance()->getIdentity()->id) && Zend_Auth::getInstance()->getIdentity()->id == $this->user->id) {
                ?>
                <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_add')?>" class="add">добавить маршрут<span></span></a>
                <?php 
                    }
                ?>
            </div>
            
            <? echo $this->action('blockmaterials', 'profile', 'users', array('id'=>$this->user->id)) ?>
        </div>
    </div>
 </div>
<script type="text/javascript">
    MenuNavigator.setCurrentProfileMenu('profile');
    defaultPaginator = new Paginator('/users/profile/blockmaterials');
    defaultPaginator.setParam('id', <?echo $this->user->id?>);
    
</script> 