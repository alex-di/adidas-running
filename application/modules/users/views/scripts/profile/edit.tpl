<link rel="stylesheet" type="text/css" href="/design/modules/imgareaselect/css/imgareaselect-default.css" media="screen" />
<script type="text/javascript" src="/design/modules/imgareaselect/scripts/jquery.imgareaselect.js"></script>
<script type="text/javascript">
    var twLoginOnSite = function(twData) {
        if(undefined != twData.is_login && twData.is_login)
            window.location = '/';
        else 
            addSocialData('tw', twData.id, twData.name);
    }
    var vkLoginOnSite = function(vkData) {
        if(undefined != vkData.is_login && vkData.is_login)
            window.location = '/';
        else 
            addSocialData('vk', vkData.id, vkData.name+' '+vkData.sname); 
    }
    var fbLoginOnSite = function(fbData) {
        if(undefined != fbData.is_login && fbData.is_login)
            window.location = '/';
        else 
            addSocialData('fb', fbData.id, fbData.name+' '+fbData.sname);
    }
    var fqLoginOnSite = function(fqData) {
        if(undefined != fqData.is_login && fqData.is_login)
            window.location = '/';
        else 
            addSocialData('fq', fqData.id, fqData.name+' '+fqData.sname);
    }
    function addSocialAccount(obj, namespace) {
        if(!$(obj).hasClass('active')) {
            if('tw' == namespace) {
                twitterLogin();
            }
            if('vk' == namespace) {
                vkLogin();
            }
            if('fb' == namespace) {
                faceboockLogin();
            }
            if('fq' == namespace) {
                foursquareLogin();
            }
        }
    }

    
    function addSocialData(namespace, uid, username) {

        $('#'+namespace+'_id').addClass('active')
                       .find('.text')
                       .empty()
                       .html(username+'<input type="hidden" name="'+namespace+'_id" value="'+uid+'"/>'
                                     +'<input type="hidden" name="'+namespace+'_username" value="'+username+'"/>');
    }
    function resetAccount(span, text) {
        var aTag = $(span).parent();
        setTimeout(function(){
            aTag.removeClass('active').removeClass('soc_active').find('.text').empty().html(text); 
        }, 100);          
    }
    $(function(){
        $('.fix_text_color input[type="text"]').each(function() {
            if($(this).val() == '') {
                $(this).val($(this).data('placeholder')).addClass('green_text');
            }
        });
        $('.fix_text_color input[type="text"]').on('focusin', function(){
            if($(this).data('placeholder') == $(this).val()) {
                $(this).val('');
            }
            $(this).removeClass('green_text');
        }).on('focusout', function(){
            if('' == $(this).val()) {
                $(this).val($(this).data('placeholder')).addClass('green_text');
            }
        });
    });
</script>
<div class="modal" id="edit_preview" style="display: none">
    <div class="title">редактирование превью</div>
    <div class="sub_text">Осталось выбрать квадратную область для маленьких фотографий.<br /> Выбранная миниатюра будет использоваться в новостях, личных сообщениях и комментариях.</div>
    <img  src="" alt="img" />
    
    <div class="save" onclick="saveImageArea()"><span></span>сохранить изменения</div>
    <div class="close" onclick="$.modal().close();"><span></span>отменить</div>
</div>
<form onsubmit="return jphp('/users/profile/edit', this)" class="registration edit_profile"> 
    <h2 class="edit_profile_title">Редактирование профиля</h2>  
    <div class="registration_column photo_edit">
        <? $isHasAvatar = !empty($this->user['avatar']);?>
        <div id="auth_uploader" class="img_place <?if($isHasAvatar):?>has_avatar<?endif;?>">
            <?if($isHasAvatar):?>
                <div class="edit_img"><img src="<?= $this->user['avatar']?>" alt="img" /></div>
            <?else:?>
                <div class="text">загрузить фото</div>
            <?endif;?>
            <div id="auth_change_uploader" class="change_photo" <?if(!$isHasAvatar):?>style="display:none"<?endif;?>>изменить фото</div>
            <input id="avatar" type="hidden" name="avatar" value="<?= $this->user['avatar'] ?>"/>
            <input type="hidden" name="preview_x_start" value="">
            <input type="hidden" name="preview_y_start" value="">
            <input type="hidden" name="preview_width" value="">
            <input type="hidden" name="preview_height" value="">
<!--             <input type="hidden" name="preview_id" value=""> -->
        </div>
        <?if(!$isHasAvatar):?>
            <?= $this->upload('auth_uploader', 'auth' ,'flash', array('fileUploaded'=>'callback', 'error' => 'uploadError', 'filesAdded'=>'startUpload'), 'true') ?>
        <?endif;?>
        <?= $this->upload('auth_change_uploader', 'auth' ,'flash', array('fileUploaded'=>'callback', 'error' => 'uploadError', 'filesAdded'=>'startUpload'), 'true') ?>
    </div>                      
    <div class="registration_column">
        <div class="control">
            <label>имя</label>
            <input name="name" type="text" placeholder="" value="<?= $this->user['name'] ?>" />
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>фамилия</label>
            <input name="sname" type="text" placeholder="" value="<?= $this->user['sname'] ?>" />
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control">
            <label>город</label>
            <input name="city" value="<?= $this->user['city'] ?>" type="text" placeholder="" />
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control sex">
            <label>пол</label><br />
            <div class="error_box sex">
                <label class="sex_label <?= 'man' == $this->user['sex'] ? 'current' : ''?>" for="man"><input type="radio" name="sex" id="man" value="man" />М</label>
                <label class="sex_label second<?= 'woman' == $this->user['sex'] ? 'current' : ''?>" for="woman"><input type="radio" name="sex" id="woman" value="woman" />Ж</label>
            </div>
        </div>

    </div>
    <div class="registration_column fix_edit">
        <div class="control fix_text_color">
            <label>телефон</label>
            <input type="text" data-placeholder="X-XXX-XXXX" value="<?= isset($this->user['phone']) ? $this->user['phone'] : '' ?>" name="phone"/>
            <div class="error">Текст ошибки</div>
        </div>
        <div class="control fix_text_color">
            <label>дата рождения</label>
            <input type="text" data-placeholder="ДД.ММ.ГГГГ" value="<?$time = strtotime($this->user['bday']);echo $time > 0 ? date('d.m.Y', $time) : ''?>" name="bday" />
            <div class="error">Текст ошибки</div>
        </div> 
        <!--<div class="control">
            <label>e-mail</label>
            <input type="text" value="<?= $this->user['email'] ?>" name="email" placeholder="e-mail" />
            <div class="error"></div>
        </div>-->
        <div class="control">
            <label>пароль</label>
            <input type="password" name="password" placeholder="" />
            <div class="error under_pass"></div>
        </div>
        <div class="control">
            <label>повторите пароль</label>
            <input type="password" name="password2" placeholder="" />
            <div class="error"></div>
        </div>
    </div>
    <div class="registration_column">
        <div class="control">
        <label>добавить аккаунты</label>
        <div class="form_social">
            <a id="vk_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'vk')" class="vk <?if(!empty($this->user['vk_id'])):?>active<?endif;?>">
                <span class="ico"></span>
                <p class="text">
                    <?if(!empty($this->user['vk_id'])):?>
                        <input type="hidden" value="<?echo $this->user['vk_id']?>" name="vk_id"/>
                        <input type="hidden" value="<?echo $this->user['vk_username']?>"  name="vk_username"/>
                        <?echo $this->user['vk_username']?>
                    <?else:?>
                        вконтакте
                    <?endif;?>
                </p>
                <span class="del" onclick="resetAccount(this, 'вконтакте')"></span>
            </a>
            <a id="fb_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'fb')" class="fb <?if(!empty($this->user['fb_id'])):?>active<?endif;?>">
                <span class="ico"></span>
                <p class="text">
                    <?if(!empty($this->user['fb_id'])):?>
                        <input type="hidden" value="<?echo $this->user['fb_id']?>" name="fb_id"/>
                        <input type="hidden" value="<?echo $this->user['fb_username']?>"  name="fb_username"/>
                        <?echo $this->user['fb_username']?>
                    <?else:?>
                        facebook
                    <?endif;?>
                </p>
                <span class="del" onclick="resetAccount(this, 'facebook')"></span>
            </a>
            <a id="tw_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'tw')" class="tw <?if(!empty($this->user['tw_id'])):?>active<?endif;?>">
                <span class="ico"></span>
                <p class="text">
                    <?if(!empty($this->user['tw_id'])):?>
                        <input type="hidden" value="<?echo $this->user['tw_id']?>" name="tw_id"/>
                        <input type="hidden" value="<?echo $this->user['tw_username']?>"  name="tw_username"/>
                        <?echo $this->user['tw_username']?>
                    <?else:?>
                        twitter
                    <?endif;?>
                </p>
                <span class="del" onclick="resetAccount(this, 'twitter')"></span>
            </a>
            <a id="fq_id" href="javascript:void(0)" onclick="addSocialAccount(this, 'fq')" class="fs <?if(!empty($this->user['fq_id'])):?>active<?endif;?>">
                <span class="ico"></span>
                <p class="text">
                    <?if(!empty($this->user['fq_id'])):?>
                        <input type="hidden" value="<?echo $this->user['fq_id']?>" name="fq_id"/>
                        <input type="hidden" value="<?echo $this->user['fq_username']?>"  name="fq_username"/>
                        <?echo $this->user['fq_username']?>
                    <?else:?>
                        foursquare
                    <?endif;?>
                </p>
                <span class="del" onclick="resetAccount(this, 'foursquare')"></span>
            </a>
        </div>
        </div>                              
    </div>
    <div style="clear: both;"></div>
    <div class="button_area">
        <input onclick="window.location='<?= $_SERVER['HTTP_REFERER'] ?>'" type="reset" value="Отмена" />
        <input type="submit" value="Сохранить" />
    </div>
    <div style="clear: both;"></div>
</form>

<script type="text/javascript">

    var maxPreviewWidth = 600;
    var previewBlockWidth = 200;
    var previewBlockHeight = 200;
    // настройки превью 
    var settings = {
        preview: {preview: {width: 260, height: 260}, absolute: {width: 0, height: 0}, is_ready: false}
    };
                  

    //функция обработки загрузки картинки
    function callback(up, file, response) {
        jsa = $.parseJSON(response.response);
        if ('error' != jsa.status) {
            $("#avatar").val(jsa.file);
            if ($("#auth_uploader").hasClass('has_avatar')) {
                $("#auth_uploader").find('img').attr('src', jsa.file);
                $("#auth_uploader").find('img').parent().addClass('nomax');
            } else {
                $("#auth_uploader").prepend('<div class="edit_img nomax"><img src="'+jsa.file+'"/></div>');
                $("#auth_uploader").addClass('has_avatar').find('.text').remove();
                $("#auth_change_uploader").show();
            }

            settings['preview'].absolute.width = jsa.xsize;
            settings['preview'].absolute.height = jsa.ysize;
            settings['preview'].is_ready = true;

            $("#auth_change_uploader").text('изменить фото');

            editArea($("#auth_change_uploader"), 'preview', {minWidth: 260, minHeight: 260, x1: 0, y1: 0, x2: 260, y2: 260, aspectRatio:'1:1'})
        } else {
            $.showInfo(jsa.message);
            $("#auth_change_uploader").text('изменить фото');
            $("#auth_uploader").find('.text').text('загрузить фото');
        }
    };

    function startUpload(){
        $("#auth_change_uploader").text('загрузка');
        $("#auth_uploader").find('.text').text('загрузка');
    }

    function uploadError(up, err) {
        if (-600 == err.code) {
            $.showInfo("Недопустимый размер файла. Максимально допустимый размер файла "+formatSize(up.settings.max_file_size));
        } else {
            $.showInfo(err.message);
        }
    }

    var currentOptionImageArea = {};
    function editArea(button, namespace, option) {
        if(settings[namespace].is_ready) {
            var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
                
            option.minWidth = option.minWidth * scale;
            if(undefined != option.maxWidth)
                option.maxWidth = option.minWidth;
            option.x2 = option.minWidth;
            var previewHeight = maxPreviewWidth * settings[namespace].absolute.height / settings[namespace].absolute.width;
            option.minHeight = option.minHeight * scale;
            if(undefined != option.maxHeight)
                option.maxHeight = option.minHeight;
            option.y2 = option.minHeight;
            
            //var currentImage = $(button).parent().find('.img_area img');
            var src = $('#avatar').val();
            var currentSrc = $('#edit_preview > img').attr('src');
            $('#edit_preview > img').attr('src', src).data('namespace', namespace);
            
            //$('#edit_preview .image_preview_block img').attr('src', src);
            currentOptionImageArea = option;
            if('' != $('input[name="'+namespace+'_x_start"]').val()) {
                currentOptionImageArea.x1 = scale * parseFloat($('input[name="'+namespace+'_x_start"]').val());
                currentOptionImageArea.y1 = scale * parseFloat($('input[name="'+namespace+'_y_start"]').val());
                currentOptionImageArea.x2 = currentOptionImageArea.x1 + scale * parseFloat($('input[name="'+namespace+'_width"]').val());
                currentOptionImageArea.y2 = currentOptionImageArea.y1 + scale * parseFloat($('input[name="'+namespace+'_height"]').val());
            }
            currentOptionImageArea.onSelectChange = changeSelect;
            currentOptionImageArea.persistent = true;
            currentOptionImageArea.onInit = initImageArea;
            currentOptionImageArea.handles = 'corners';
            currentOptionImageArea.movable = true;
            //currentOptionImageArea.parent = '#edit_preview';
            $('#edit_preview').modal().open({
                                        closeOnOverlayClick: false,
                                        closeOnEsc: false,
                                        isReposition: false,
                                        onOpen: function(){
                                            //$('.uploader').hide();
                                            //if(currentSrc == src) 
                                               initImageAreaSelect($('#edit_preview > img'));
                                            
                                        },
                                        onClose: function(){
                                            //$('.uploader').show();
                                            $('#edit_preview > img').imgAreaSelect({remove:true});
                                            $('body').css('overflow', 'auto');
                                        },
                                        onReposition: function(){
                                            //$('#edit_preview > img').imgAreaSelect({ instance: true }).update();
                                        }
                                        });
        }
    }

    function saveImageArea(){
        var img = $('#edit_preview > img');
        var ias = img.imgAreaSelect({ instance: true });
        setFields(img, ias.getSelection());
        $.modal().close();
    }

    function changeSelect(){

    }

    function initImageAreaSelect(img){
        $(img).imgAreaSelect(currentOptionImageArea);
    }
    function initImageArea(img, selection){
        changeSelect(img, selection);
        setFields(img, selection, true);
        
        var top = ($(window).height() - $('#edit_preview').height()) / 4;
        top =  $('#edit_preview').offset().top - (top > 0 ? top : 0);
        window.scrollTo(0, top);
        //$('body').css('overflow', 'hidden');
        $('#edit_preview > img').imgAreaSelect({ instance: true }).update();
    }
    function selectEndImageArea(img, selection){
        setFields(img, selection);
    }
    function showButtonPreviewEdit(namespace) {
        $('input[name="'+namespace+'"]').parent().find('.edit_preview_link').removeClass('disabled');
    }
    function initImagesPreview(namespace) {
        var selectionWidth = parseFloat($('input[name="'+namespace+'_width"]').val());
        var selectionHeight = parseFloat($('input[name="'+namespace+'_height"]').val());
        var x_start = parseFloat($('input[name="'+namespace+'_x_start"]').val());
        var y_start = parseFloat($('input[name="'+namespace+'_y_start"]').val());
        var scaleX = settings[namespace].preview.width / selectionWidth;
        var scaleY = settings[namespace].preview.height / selectionHeight;
        $("input[name="+namespace+"]").parent().find(".img_area > img").css({
            width: Math.round(scaleX * settings[namespace].absolute.width),
            height: Math.round(scaleY * settings[namespace].absolute.height),
            marginLeft: -Math.round(scaleX * x_start),
            marginTop: -Math.round(scaleY * y_start),
            display: 'block'
        });
    }
    
    function setFields(img, selection, firstInit) {
        var namespace = $(img).data('namespace');
        var scale = settings[namespace].absolute.width > maxPreviewWidth ? maxPreviewWidth / settings[namespace].absolute.width : 1;
            
        var selectionWidth = selection.width / scale;
        var selectionHeight = selection.height / scale;
        var x_start = selection.x1 / scale;
        var y_start = selection.y1 / scale;

        var scaleX = settings[namespace].preview.width / selectionWidth;
        var scaleY = settings[namespace].preview.height / selectionHeight;
        if (firstInit == true) {
            $("input[name=avatar]").parent().find("img").css({
                maxWidth: '100%'
            })
        } else {
            $("input[name=avatar]").parent().find("img").css({
                marginLeft: -Math.round(scaleX * x_start),
                marginTop: -Math.round(scaleY * y_start),
                maxWidth: 'none',
                width: Math.round(scaleX * settings[namespace].absolute.width),
                height: Math.round(scaleY * settings[namespace].absolute.height)
            });    
        }
        
        $('input[name="'+namespace+'_x_start"]').val(x_start);
        $('input[name="'+namespace+'_y_start"]').val(y_start);
        $('input[name="'+namespace+'_width"]').val(selectionWidth);
        $('input[name="'+namespace+'_height"]').val(selectionHeight);
    }  
   
</script>