<div class="element photo_column bg2"><? if(!empty($this->user->avatar)): ?><img src="<?= $this->user->avatar  ?>" alt="img" /><? endif ?></div>
<div class="info_column">
    <div class="name">
        <?= $this->escape($this->user->name), " ", $this->escape($this->user->sname) ?>
        <div class="personal_soc_ico">
            <?if(!empty($this->user->vk_id)):?>
                <a href="http://vk.com/id<?echo $this->user->vk_id?>" class="vk"></a>
            <?endif;?>
            <?if(!empty($this->user->fb_id)):?>
                <a href="https://www.facebook.com/<?echo $this->user->fb_id?>" class="fb"></a>
            <?endif;?>
            <?if(!empty($this->user->tw_id)):?>
                <a href="https://twitter.com/account/redirect_by_id?id=<?echo $this->user->tw_id?>" class="tw"></a>
            <?endif;?>
        </div>
    </div>
    <div class="info">
        <?if('0000-00-00 00:00:00' != $this->user->bday): $years = DR_Api_Tools_Tools::yearDifference($this->user->bday, time());?>
            Возраст: <?echo $years .' '.DR_Api_Tools_Tools::getPostfixYears($years)?>
        <?else:?>
            Возраст: не указано 
        <?endif?>
        
        <br /> <?= $this->escape($this->user->city) ?>
    </div>
    <? if(!empty($this->user->card_number)): ?>
        <div class="runner"><?= $this->user->card_number ?></div>
    <? endif ?>
    <a href="/users/profile/edit" class="edit">редактировать профиль</a>
</div> 
<div style="clear: both;"></div>   