<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Users_articlesController extends DR_Controllers_Site { 
    
    public function editAction(){
        if($this->_hasParam('article_id')) {
            $data = api::getMaterials()->where('t.id', $this->_getParam('article_id'))
                                ->where('t.users_id', Zend_Auth::getInstance()->getIdentity()->id)
                                ->where('t.is_modern', Model_Materials::TYPE_MODERN, '!=')
                                ->where('m.key', 'post_data')
                                ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND modules_id = '.Model_Meta::MATERIALS." AND m.key = 'post_data'", array('post_data' => 'm.text_value'))
                                ->row();
            if(!count($data)) 
                $this->_redirect('/error404');
            $this->view->data = $data;
        }
    }
    
    public function saveAction() {
        if(Model_Materials::CATEGORY_EVENTS != $_POST['category_id'])
            unset($_POST['date_event']);
        if(empty($_POST['category_id']))
            $_POST['category_id'] = Model_Materials::CATEGORY_NOT_SELECTED;
            
        $post_data = $_POST;
        unset($post_data['dragElements']);
        list($html, $gallery, $data) = DR_Api_Tools_Materials::parseDragElememts($_POST['dragElements']);
   
        $post_data = array_merge($post_data, $data);
        $isNotStrictValidation = Model_Materials::TYPE_DRAFTS == $_POST['is_modern'] && !isset($_POST['isNeedPreview']);
        if($isNotStrictValidation) {

            $this->getValidator()->setNotRequiredFields(array_merge(array_keys($data), array('preview', 
                                                                                            'preview_width', 
                                                                                            'preview_height', 
                                                                                            'preview_x_start', 
                                                                                            'preview_y_start', 
                                                                                            'banner', 
                                                                                            'banner_id', 
                                                                                            'banner_width', 
                                                                                            'banner_height', 
                                                                                            'banner_x_start', 
                                                                                            'banner_y_start')));
        }
        list($valid, $data) = $this->isValid($post_data);
        if(!count($_POST['dragElements']) && !$isNotStrictValidation)
            $valid = false;
        $data['is_valid'] = $valid ? 1 : 0;
   
  		if ($valid) {
  		    $post = $_POST;
            unset($post['dragElements']);
            $post['users_id'] = Zend_Auth::getInstance()->getIdentity()->id;
            $post['banner_preview'] = $post['title_image'] = '';
            if(!empty($post['preview']))
                $post['banner_preview'] = Model_Files::cropFile($post['preview'], $_SERVER['DOCUMENT_ROOT'], $post['preview_width'], $post['preview_height'], $post['preview_x_start'], $post['preview_y_start']);
            if(!empty($post['banner']))
                $post['title_image'] = Model_Files::cropFile($post['banner'], $_SERVER['DOCUMENT_ROOT'], $post['banner_width'], $post['banner_height'], $post['banner_x_start'], $post['banner_y_start']);
            
            $post['name'] = mb_strtolower($post['name'], 'UTF-8');
            $post['stitle'] = DR_Api_Tools_Tools::stitle($post['name'], 40);
			$post['stitle'] = api::getMaterials()->stitleUnique($post['stitle'], $this->_getParam('id'));
            // избегаем хака модерации
            if(Model_Materials::TYPE_MODERN == $post['is_modern'])
                $post['is_modern'] = Model_Materials::TYPE_DRAFTS;
            
			$newId = api::getMaterials()->doSave($post, $this->_getParam('id'));
			if(Model_Materials::TYPE_NOT_MODERN == $post['is_modern'])
                Model_Events::modernArticleEvent($newId, $post['users_id'], $post['users_id'], Model_Materials::TYPE_NOT_MODERN);
            //api::getKeywords()->saveMaterialKeywords($post['keywords'], $newId);
            
            $model_meta = api::getMeta();
            
			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
            $model_meta
					->doSave(
							array('value' => $post['banner_preview'], 'key' => 'banner_preview',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
            $model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));

            $shortDesc = DR_Api_Tools_Tools::getShortString(strip_tags($html), 60);

            $model_meta
					->doSave(
							array('text_value' => $shortDesc, 'key' => 'short_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
  			$model_meta
					->doSave(
							array('text_value' => serialize($_POST), 'key' => 'post_data', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			if(!empty($post['banner_id']))
                $model_meta
    					->doSave(
    							array('key' => 'photos', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
    									'file_id' => $post['banner_id']));
			$model_meta
					->doSave(
							array('key' => 'is_profile_material', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
									'value' => 1));
            if(isset($post['date_event']))
      			$model_meta
    					->doSave(
    							array('date_value' => trim($post['date_event']), 'key' => 'date_start_event',
    									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
            if (count($gallery)) {
				foreach ($gallery as $index => $images) {
					foreach ($images as $img) {
						$model_meta
								->doSave(
										array('value' => $img['src'], 'int_value' => $index, 'key' => 'gallery', 'text_value'=>$img['name'], 
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            $data['article_url'] = $this->view->url(array('stitle' => $post['stitle']), 'article_view');
		}
        die(json_encode($data));
    }
    public function processtomodernAction() {
        $article = api::getMaterials()->where('t.id', $this->_getParam('id'))
                                ->where('t.users_id', Zend_Auth::getInstance()->getIdentity()->id)
                                ->where('t.is_modern', Model_Materials::TYPE_MODERN, '!=')->row();
        if(count($article)) {
            api::getMaterials()->doSave(array('is_modern'=>Model_Materials::TYPE_NOT_MODERN), $article->id);
            Model_Events::modernArticleEvent($article->id, $article->users_id, Zend_Auth::getInstance()->getIdentity()->id, Model_Materials::TYPE_NOT_MODERN);
        }
        die;
    }
    public function viewAction(){
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $id = $this->_getParam('id');
        $fields = array('count_articles_draft'=>'(SELECT COUNT(1) FROM Model_Materials mt LEFT JOIN Model_Meta m ON m.resource_id = mt.id AND m.modules_id = '.Model_Meta::MATERIALS." AND m.key = 'is_profile_material' WHERE mt.users_id = t.id AND mt.is_modern IN(".Model_Materials::TYPE_CANCELED.', '.Model_Materials::TYPE_DRAFTS.") AND mt.category_id !=".Model_Materials::CATEGORY_ROUTERS." AND m.value = '1')",);
        $this->view->user =  api::getUsers()->getUserData($id, $fields);
        $this->view->type = $this->_getParam('type');
        if('modern' != $this->view->type && !Model_Users::isCurrentUserProfile())
            $this->_redirect('/error404');
    }
    public function blockmaterialsAction() {
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $page = $this->_getParam('page', 1);
        $type = $this->_getParam('type', 'modern');
        $user_id = $this->_getParam('id', 0);
        api::getMaterials()->_new(array('t.*', 'is_event'=>'ev.id', 'event_type'=>'ev.type', 'ev.counter', 'event_id'=>'ev.id', 'event_is_read'=>'ev.is_read'))
           ->where('t.category_id', Model_Materials::CATEGORY_ROUTERS, '!=')
           ->where('t.users_id', $user_id)
           ->where('m1.value', '1')
           ->joinLeft(array('m1' => api::META), 'm1.resource_id = t.id AND m1.modules_id = '.Model_Meta::MATERIALS." AND m1.key = 'is_profile_material'", array('is_profile_material'=>'m1.value'))
           ->joinLeft(array('m2' => api::META), 'm2.resource_id = t.id AND m2.modules_id = '.Model_Meta::MATERIALS." AND m2.key = 'banner_preview'", array('banner_preview'=>'m2.value'))
           ->joinLeft(array('m3' => api::META), 'm3.resource_id = t.id AND m3.modules_id = '.Model_Meta::MATERIALS." AND m3.key = 'short_text'", array('short_text'=>'m3.text_value'))
           ->joinLeft(array('ev' => api::EVENTS), 'ev.resource_id = t.id AND ev.owner_id = t.users_id AND ev.type IN ('.Model_Events::TYPE_MODERN_MATERIALS.','.Model_Events::TYPE_NOT_MODERN_MATERIALS.')');
        if('modern' == $type) {
            $status = array(Model_Materials::TYPE_MODERN);
            if(Model_Users::isCurrentUserProfile()) {
                $status[] = Model_Materials::TYPE_NOT_MODERN;
                $this->view->noFindData = $this->getTemplateNotFindAddedArtciles();
            }
            api::getMaterials()->in('t.is_modern', $status);
        } else {
            api::getMaterials()->in('t.is_modern', array(Model_Materials::TYPE_CANCELED, Model_Materials::TYPE_DRAFTS));
            Zend_Registry::set('drafts_article', 1); 
        }

        list($this->view->data, $paginator) = api::getMaterials()->pageRows($page, 10);
        $this->view->pageCount = $paginator->count();
            
        parent::blockmaterialsAction();
    }
    private function getTemplateNotFindAddedArtciles() {
        return '<div class="default_box">
                    <div class="text_part">Вы еще не добавили ни одной статьи.</div>
                    <a class="button add articles" href="/profile/'.Zend_Auth::getInstance()->getIdentity()->id.'/articles/add">добавить статью<span></span></a>
                </div>';
    }
}