<?php

class Users_famousController extends DR_Controllers_Site
{

	/**
	 * Просмотр профиля
	 */
	public function indexAction()
    {

        $author_id = $this->_request->getParam('author_id', false);

        $authors = api::getUsers()->getAuthors();

        //$articles = api::getMaterials()->getMaterialsFromFamous($user_id);
        //$this->view->data = api::getUsers()->getMaterialsFromFamous();

        $this->view->assign('author_id', $author_id);
        $this->view->assign('authors', $authors);
	}
        
}
