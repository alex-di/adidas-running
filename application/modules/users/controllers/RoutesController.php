<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Users_routesController extends DR_Controllers_Site { 
    
    private $templateNotFindData = array('run'=>'<div class="default_box">
                                                    <div class="text_part">Вы не отметились ни на одном маршруте.</div>
                                                    <a class="button goTo routes" href="/routes">перейти в раздел маршруты<span></span></a>
                                                </div>',
                                        'like'=>'<div class="default_box">
                                                    <div class="text_part">Вы не заинтересовались ни одним маршрутом.</div>
                                                    <a class="button goTo routes" href="/routes">перейти в раздел маршруты<span></span></a>
                                                </div>',);
    public function editAction(){
        if($this->_hasParam('route_id')) {
            $data = api::getMaterials()->where('t.id', $this->_getParam('route_id'))
                                ->where('t.users_id', Zend_Auth::getInstance()->getIdentity()->id)
                                ->where('t.is_modern', Model_Materials::TYPE_MODERN, '!=')
                                ->where('m.key', 'post_data')
                                ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND modules_id = '.Model_Meta::MATERIALS." AND m.key = 'post_data'", array('post_data' => 'm.text_value'))
                                ->row();
            if(!count($data)) 
                $this->_redirect('/error404');
            $this->view->data = $data;
        }
    }
    
    public function saveAction() {
        if(empty($_POST['routes_type']))
            $_POST['routes_type'] = Model_Materials::ROUTE_TYPE_NOT_SELECTED;
        if(!isset($_POST['dragElements']))
            $_POST['dragElements'] = array();
        $post_data = $_POST;
        unset($post_data['dragElements']);
        list($html, $gallery, $data) = DR_Api_Tools_Materials::parseDragElememts($_POST['dragElements']);
        
        $isNotStrictValidation = Model_Materials::TYPE_DRAFTS == $_POST['is_modern'] && !isset($_POST['isNeedPreview']);
        if($isNotStrictValidation) {
            $this->getValidator()->setNotRequiredFields(array_merge(array_keys($data), array('name_point_start', 
                                                                                            'name_point_finish')));
        }
        $post_data = array_merge($post_data, $data);
        
        list($valid, $data) = $this->isValid($post_data);
        //if(!count($_POST['dragElements']))
        //    $valid = false;
        $data['is_valid'] = $valid ? 1 : 0;
  		if ($valid) {

  		    $post = $_POST;
            unset($post['dragElements']);
            $post['users_id'] = Zend_Auth::getInstance()->getIdentity()->id;
            $post['category_id'] = Model_Materials::CATEGORY_ROUTERS;
            
            $post['name'] = mb_strtolower($post['name'], 'UTF-8');
            $post['stitle'] = DR_Api_Tools_Tools::stitle($post['name'], 40);
			$post['stitle'] = api::getMaterials()->stitleUnique($post['stitle'], $this->_getParam('id'));
            // избегаем хака модерации
            if(Model_Materials::TYPE_MODERN == $post['is_modern'])
                $post['is_modern'] = Model_Materials::TYPE_DRAFTS;
			$newId = api::getMaterials()->doSave($post, $this->_getParam('id'));
			if(Model_Materials::TYPE_NOT_MODERN == $post['is_modern'])
                Model_Events::modernArticleEvent($newId, $post['users_id'], $post['users_id'], Model_Materials::TYPE_NOT_MODERN);
            $path = Model_Routesdata::getCoordinatesFromLatLng(json_decode($post['overview_path']));
            $post['banner_preview'] = Model_Routesdata::drawRoute($path);
			$routesdata_id = null;
            $routesdata = api::getRoutesdata()->_new(array('t.id'))->where('t.materials_id', $newId)->row();
            if(count($routesdata))
                $routesdata_id = $routesdata->id;
			api::getRoutesdata()
					->doSave(
							array('materials_id' => $newId, 'radius_square' => $post['radius_square'],
									'center_lat' => $post['center_lat'], 'center_lng' => $post['center_lng'],
									'start_lat' => $post['start_lat'], 'start_lng' => $post['start_lng'],
									'end_lat' => $post['end_lat'], 'end_lng' => $post['end_lng']), $routesdata_id);
            
            $model_meta = api::getMeta();
            
			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
            $model_meta
					->doSave(
							array('value' => $post['banner_preview'], 'key' => 'banner_preview',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
            $model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
  			$model_meta
					->doSave(
							array('text_value' => serialize($_POST), 'key' => 'post_data', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('key' => 'is_profile_material', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
									'value' => 1));
						
			$model_meta
					->doSave(
							array('double_value' => $post['mileage'], 'key' => 'mileage', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('list_value_id' => $post['routes_type'], 'key' => 'routes_type',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('key' => 'overview_path', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'text_value' => $post['overview_path']));
			$model_meta
					->doSave(
							array('key' => 'name_point_start', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'value' => $post['name_point_start']));
			$model_meta
					->doSave(
							array('key' => 'name_point_finish', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'value' => $post['name_point_finish']));
            
            if (count($gallery)) { 
				foreach ($gallery as $index => $images) {
					foreach ($images as $img) {
						$model_meta
								->doSave(
										array('value' => $img['src'], 'int_value' => $index, 'key' => 'gallery', 'text_value'=>$img['name'], 
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            $data['article_url'] = $this->view->url(array('stitle' => $post['stitle']), 'route_view');
		}
        die(json_encode($data));
    }
    public function processrunrouteAction(){
        $route_id = $this->_getParam('id');
        $type = $this->_getParam('type', 0);
        $reference = api::getReferences()->_new(array('t.id', 'm.int_value', 'meta_id'=>'m.id'))
                                   ->where('t.object_id', Zend_Auth::getInstance()->getIdentity()->id)
                                   ->where('t.ref_type', Model_References::REF_TYPE_USER_RUNROUTES)
                                   ->where('t.reference_id', $route_id)
                                   ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."'")
                                   ->row();
        $response = 'OK';
        $isRunning = false;
        if(!count($reference)) {
            $reference_id = api::getReferences()->doSave(array('object_id'=>Zend_Auth::getInstance()->getIdentity()->id, 'ref_type'=>Model_References::REF_TYPE_USER_RUNROUTES, 'reference_id'=>$route_id));
            api::getMeta()->doSave(array('resource_id'=>$reference_id, 'modules_id'=>Model_Meta::RUNROUTES_REFERENCE_USERS, 'key'=>Model_Meta::RUNROUTES_REFERENCE_USERS_KEY, 'int_value'=>$type));
            $isRunning = 1 == $type;
        } else {
            if($reference->int_value == $type)
                $response = 'ALLREADY';
            elseif($reference->int_value == 1 && $type == 0)
                $response = 'IMPOSIBLE'; 
            else {
                api::getMeta()->doSave(array('int_value' => 1), $reference->meta_id);
                $isRunning = true;
            }    
        }
        if($isRunning) {
            $data = api::getReferences()->_new(array('count_running'=>'count(1)'))
                                   ->where('t.ref_type', Model_References::REF_TYPE_USER_RUNROUTES)
                                   ->where('t.reference_id', $route_id)
                                   ->where('m.int_value', 1)
                                   ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."'")
                                   ->row();
            api::getMaterials()->doSave(array('count_running' => $data->count_running), $route_id);
            api::getEvents()->doSave(array('owner_id' => Zend_Auth::getInstance()->getIdentity()->id, 'resource_id' => $route_id, 'type' => Model_Events::TYPE_RUN_MATERIALS, 'users_id' => Zend_Auth::getInstance()->getIdentity()->id));
        }
        die($response);
    }
    
    public function viewAction(){
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $id = $this->_getParam('id');


        $fields = array('count_routes_add'=>'(SELECT COUNT(1) FROM Model_Materials mt LEFT JOIN Model_Meta m ON m.resource_id = mt.id AND m.modules_id = '.Model_Meta::MATERIALS." AND m.key = 'is_profile_material' WHERE mt.users_id = t.id AND mt.category_id = ".Model_Materials::CATEGORY_ROUTERS." AND mt.is_modern = ".Model_Materials::TYPE_MODERN." AND m.value = '1')",
                            'count_like_run'=>'(SELECT COUNT(1) FROM Model_References ref LEFT JOIN Model_Meta m ON m.resource_id = ref.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."' WHERE ref.object_id = t.id AND ref.ref_type = ".Model_References::REF_TYPE_USER_RUNROUTES.' AND m.int_value = 0)',
                            'count_routes_draft'=>'(SELECT COUNT(1) FROM Model_Materials mt LEFT JOIN Model_Meta m ON m.resource_id = mt.id AND m.modules_id = '.Model_Meta::MATERIALS." AND m.key = 'is_profile_material' WHERE mt.users_id = t.id AND mt.category_id = ".Model_Materials::CATEGORY_ROUTERS." AND mt.is_modern IN (".Model_Materials::TYPE_CANCELED.', '.Model_Materials::TYPE_DRAFTS.") AND m.value = '1')",);
        $this->view->user =  api::getUsers()->getUserData($id, $fields);
        $this->view->type = $this->_getParam('type');
        if(!in_array($this->view->type, array('run', 'modern')) && !Model_Users::isCurrentUserProfile())
            $this->_redirect('/error404');
    }
    public function blockmaterialsAction() {
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $page = $this->_getParam('page', 1);
        $type = $this->_getParam('type', 'run');
        $user_id = $this->_getParam('id', 0);
        $model = null;
        switch($type) {
            case 'run':
            case 'like':
                api::getReferences()->_new(array('mt.*'))
                                   ->where('t.ref_type', Model_References::REF_TYPE_USER_RUNROUTES)
                                   ->where('t.object_id', $user_id)
                                   ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."'")
                                   ->joinLeft(array('m1' => api::META), 'm1.resource_id = t.reference_id AND m1.modules_id = '.Model_Meta::MATERIALS." AND m1.key = 'is_profile_material'", array('is_profile_material'=>'m1.value'))
                                    ->joinLeft(array('m2' => api::META), 'm2.resource_id = t.reference_id AND m2.modules_id = '.Model_Meta::MATERIALS." AND m2.key = 'banner_preview'", array('banner_preview'=>'m2.value'))
                                   ->joinLeft(array('m3' => api::META), 'm3.resource_id = t.reference_id AND m3.modules_id = '.Model_Meta::MATERIALS." AND m3.key = 'mileage'", array('mileage'=>'m3.double_value'))
                                   ->joinLeft(array('m4' => api::META), 'm4.resource_id = t.reference_id AND m4.modules_id = '.Model_Meta::MATERIALS." AND m4.key = 'name_point_start'", array('name_point_start'=>'m4.value'))
                                   ->joinLeft(array('m5' => api::META), 'm5.resource_id = t.reference_id AND m5.modules_id = '.Model_Meta::MATERIALS." AND m5.key = 'name_point_finish'", array('name_point_finish'=>'m5.value'))
                                   ->joinLeft(array('m6' => api::META), 'm6.resource_id = t.reference_id AND m6.modules_id = '.Model_Meta::MATERIALS." AND m6.key = 'routes_type'")
                                   ->joinLeft(array('lv' => 'Model_Listvalues'), 'lv.id = m6.list_value_id', array('routes_type'=>'lv.value'))
                                   ->joinLeft(array('m7' => api::META), 'm7.resource_id = t.reference_id AND m7.modules_id = '.Model_Meta::MATERIALS." AND m7.key = 'photos'")
                                   ->joinLeft(array('fl' => 'Model_Files'), 'fl.id = m7.file_id', array('photos'=>'fl.path'))
                                    ->joinLeft(array('mt' => 'Model_Materials'), 'mt.id = t.reference_id', array());
                
                if('run' == $type) {
                    if(Model_Users::isCurrentUserProfile())
                        $this->view->noFindData = $this->templateNotFindData['run'];
                    api::getReferences()->where('m.int_value', 1);
                } else {
                    if(Model_Users::isCurrentUserProfile())
                        $this->view->noFindData = $this->templateNotFindData['like'];
                    api::getReferences()->where('m.int_value', 0);
                }
                    
                $model = api::getReferences();
                break;
            case 'modern':
            case 'draft':
                api::getMaterials()->_new(array('t.*','is_event'=>'ev.id', 'event_type'=>'ev.type', 'ev.counter', 'event_id'=>'ev.id', 'event_is_read'=>'ev.is_read'))
                   ->where('t.category_id', Model_Materials::CATEGORY_ROUTERS)
                   ->where('t.users_id', $user_id)
                   ->where('m1.value', '1')
                   ->joinLeft(array('m1' => api::META), 'm1.resource_id = t.id AND m1.modules_id = '.Model_Meta::MATERIALS." AND m1.key = 'is_profile_material'", array('is_profile_material'=>'m1.value'))
                   ->joinLeft(array('m2' => api::META), 'm2.resource_id = t.id AND m2.modules_id = '.Model_Meta::MATERIALS." AND m2.key = 'banner_preview'", array('banner_preview'=>'m2.value'))
                   ->joinLeft(array('m3' => api::META), 'm3.resource_id = t.id AND m3.modules_id = '.Model_Meta::MATERIALS." AND m3.key = 'mileage'", array('mileage'=>'m3.double_value'))
                   ->joinLeft(array('m4' => api::META), 'm4.resource_id = t.id AND m4.modules_id = '.Model_Meta::MATERIALS." AND m4.key = 'name_point_start'", array('name_point_start'=>'m4.value'))
                   ->joinLeft(array('m5' => api::META), 'm5.resource_id = t.id AND m5.modules_id = '.Model_Meta::MATERIALS." AND m5.key = 'name_point_finish'", array('name_point_finish'=>'m5.value'))
                   ->joinLeft(array('m6' => api::META), 'm6.resource_id = t.id AND m6.modules_id = '.Model_Meta::MATERIALS." AND m6.key = 'routes_type'")
                   ->joinLeft(array('lv' => 'Model_Listvalues'), 'lv.id = m6.list_value_id', array('routes_type'=>'lv.value'))
                   ->joinLeft(array('ev' => api::EVENTS), 'ev.resource_id = t.id AND ev.owner_id = t.users_id AND ev.type IN ('.Model_Events::TYPE_MODERN_MATERIALS.','.Model_Events::TYPE_NOT_MODERN_MATERIALS.')');
                if('modern' == $type) {
                    $status = array(Model_Materials::TYPE_MODERN);
                    if(Model_Users::isCurrentUserProfile()) {
                        $status[] = Model_Materials::TYPE_NOT_MODERN;
                        $this->view->noFindData = $this->getTemplateNotFindAddedRoute();
                    }
                        
                    api::getMaterials()->in('t.is_modern', $status);
                } else {
                    api::getMaterials()->in('t.is_modern', array(Model_Materials::TYPE_CANCELED, Model_Materials::TYPE_DRAFTS));
                    Zend_Registry::set('drafts_route', 1); 
                }
                   
                $model = api::getMaterials();
                break; 
        }
        //$model->debug(true);
        list($this->view->data, $paginator) = $model->pageRows($page, 10);
        $this->view->pageCount = $paginator->count();

        parent::blockmaterialsAction();
    }
    private function getTemplateNotFindAddedRoute() {
        return '<div class="default_box">
                    <div class="text_part">Вы еще не добавили ни одного маршрута.</div>
                    <a class="button add routes" href="/profile/'.Zend_Auth::getInstance()->getIdentity()->id.'/routes/add">добавить маршрут<span></span></a>
                </div>';
    }
}