<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Users_profileController extends DR_Controllers_Site
{

    /**
     * Просмотр профиля
     */
    public function indexAction()
    {   

        $this->view->headTitle('Личный кабинет - adidas running');
        if ($id = $this->_getParam('id') and $id == Zend_Auth::getInstance()->getIdentity()->id)
        {
            $model = api::getUsers()->_new();
            $this->view->user = $model->where('t.id', $id)
                ->joinLeft(array('c' => api::CARDS), 't.card_id = c.id', array('card_number' => 'c.number'))    
                ->row();
            //_d($this->view->user);
            
        } else
        {
            $this->_redirect('/error404');
        }
    }

    public function viewAction() {

        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $this->view->headTitle('Личный кабинет - adidas running');
        $id = $this->_getParam('id');
        $this->view->user =  api::getUsers()->getUserData($id);

        if (!count($this->view->user)) {
            $this->_redirect('/error404');
        }
        
        if ($this->view->user->count_run) {
            $data = api::getReferences()->_new(array('t.id'))
                                ->where('t.ref_type', Model_References::REF_TYPE_USER_RUNROUTES)
                                ->where('t.object_id', $id)
                                ->where('m1.int_value', 1)
                                ->joinLeft(array('m1' => api::META), 'm1.resource_id = t.id AND m1.modules_id = '.Model_Meta::RUNROUTES_REFERENCE_USERS." AND m1.key = '".Model_Meta::RUNROUTES_REFERENCE_USERS_KEY."'", array())
                                ->joinLeft(array('m2' => api::META), 'm2.resource_id = t.reference_id AND m2.modules_id = '.Model_Meta::MATERIALS." AND m2.key = 'banner_preview'", array('banner_preview' => 'm2.value'))
                                ->joinLeft(array('m3' => api::META), 'm3.resource_id = t.reference_id AND m3.modules_id = '.Model_Meta::MATERIALS." AND m3.key = 'photos'", array())
                                ->joinLeft(array('fl' => api::FILES), 'm3.file_id = fl.id', array("fl.path"))
                                ->rows();

            $rotesBackground = array();
            if(count($data)) {
                foreach($data as $row) {
                    $rotesBackground[$row->id] = (!empty($row->banner_preview)) ? $row->banner_preview : $row->path;
                }
            }
            $this->view->rotesBackground = $rotesBackground;
        }
    }

    public function blockmaterialsAction(){

        $page = $this->_getParam('page', 1);
        $user_id = $this->_getParam('id', 0);
        $owner_id = isset( Zend_Auth::getInstance()->getIdentity()->id) ?  Zend_Auth::getInstance()->getIdentity()->id : false;
        
        Zend_Registry::set('profile_users_id', $user_id);
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        list($this->view->data, $paginator) = api::getEvents()->getData($user_id, $page, 10, $owner_id);

        $this->view->pageCount = $paginator->count();

		if ($this->_request->isXmlHttpRequest()) {
			Zend_Layout::getMvcInstance()->setLayout('ajax');
			$body = $this->view
					->partial('blockmaterials-profile.tpl',
							array('data' => $this->view->data, 'pageCount' => $this->view->pageCount));
			$body = trim(strtr($body, array("\n" => "", "\r" => "", "\t" => "")));
			die(json_encode(array('body' => $body)));
		} else {
			$this->render("blockmaterials-profile", null, true);
		}
    }
    /**
     * редактирование профиля
     */
    public function editAction()
    {
        if (Zend_Auth::getInstance()->hasIdentity())
        {
            if ($this->_request->isPost())
            {

                if ($_POST['bday'] == 'ДД.ММ.ГГГГ')
                    $_POST['bday'] = '';
                if ($_POST['phone'] == 'X-XXX-XXXX')
                    $_POST['phone'] = '';
                
                if (isset($_POST['email']) && $_POST['email'] == Zend_Auth::getInstance()->getIdentity()->email)
                    unset($_POST['email']);

                if (isset($_POST['avatar']) && !empty($_POST['preview_x_start']) && !empty($_POST['preview_y_start'])) {
                    $_POST['avatar'] = Model_Files::cropFile($_POST['avatar'], $_SERVER['DOCUMENT_ROOT'], $_POST['preview_width'], $_POST['preview_height'], $_POST['preview_x_start'], $_POST['preview_y_start']);
                    $fileinfo = pathinfo($_POST['avatar']);
                    $M = new DR_Api_Tools_Photo;

                    $way = $_SERVER['DOCUMENT_ROOT'] . $fileinfo['dirname'] . '/' . $fileinfo['filename'];

                    if (!file_exists($way))
                        mkdir($way, 0755);

                    $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev100." . $fileinfo['extension'], 0, 100);
                    $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev225." . $fileinfo['extension'], 0, 225);
                    $M->doPreview($_SERVER['DOCUMENT_ROOT'] . $_POST['avatar'], $way . "/prev260." . $fileinfo['extension'], 0, 260);
                }

                unset($_POST['preview_x_start']);
                unset($_POST['preview_y_start']);
                unset($_POST['preview_width']);
                unset($_POST['preview_height']);

                $allow_fields = array(
                    "email",
                    "group_id",
                    "password",
                    "password2",
                    "avatar",
                    "name",
                    "sname",
                    "sex",
                    "bday",
                    "city",
                    "fb_id",
                    "vk_id",
                    "tw_id",
                    "fq_id",
                    "fb_username",
                    "vk_username",
                    "tw_username",
                    "fq_username",
                    "phone",
                    "accept_rules");

                $this->getValidator()
                     ->setFieldsAllowedForSave($allow_fields)
                     ->setNotRequiredFields(array("bday",
                                                "phone",
                                                "avatar",
                                                "accept_rules",
                                                "password",
                                                "password2",
                                                "fb_id",
                                                "vk_id",
                                                "tw_id",
                                                "fq_id",
                                                "fb_username",
                                                "vk_username",
                                                "tw_username",
                                                "fq_username"))
                ->setElement('email')
                    ->email('Неверный email')
                    ->recordNoExists('Email уже зарегистрирован')
                ->setElement('bday')
                    ->date('Неверный формат даты', array('format' => 'd.m.Y'))
                ->setElement('phone')
                    ->phone();

                $_POST = DR_Api_Tools_Tools::trimValues($_POST);
                list($valid, $Data) = $this->isValid($_POST);
                $scripts = array();

                if ($valid) {

                    if (!empty($_POST['bday'])) {
                        $date = DateTime::createFromFormat('d.m.Y', $_POST['bday']);
                        $_POST['bday'] = $date->format('Y-m-d');
                    }
                    
                    if (empty($_POST['password']))
                    {
                        unset($_POST['password']);
                    } else {
                        $_POST['password'] = Model_Users::cryptoPassword($_POST['password']);
                    }

                    $users_id = Zend_Auth::getInstance()->getIdentity()->id;
                    
                    if(!isset($_POST['fb_id']))
                        $_POST['fb_id'] = '';
                    if(!isset($_POST['vk_id']))
                        $_POST['vk_id'] = '';
                    if(!isset($_POST['tw_id']))
                        $_POST['tw_id'] = '';
                    if(!isset($_POST['fq_id']))
                        $_POST['fq_id'] = '';

                    api::getUsers()->doSave($_POST, $users_id);
                    api::getUsers()->refreshAuth();
                    $meta_keys = array("fb_username", "vk_username", "tw_username", "fq_username", "phone");
                    foreach($meta_keys as $key) {
                        if(isset($_POST[$key])) {
                            $row = api::getMeta()->_new('id')->where('t.modules_id', Model_Meta::USERS)->where('t.resource_id', $users_id)->where('t.key', $key)->row();
                            $id = null;
                            if(count($row))
                                $id = $row->id;
                            api::getMeta()->doSave(array('modules_id'=>Model_Meta::USERS, 'resource_id'=>$users_id, 'key'=>$key, 'value'=>isset($_POST[$key]) ? $_POST[$key] : ''), $id);
                        }
                    }

                    DR_Api_Tools_Auth::clear();
                    $scripts[] = '$.showInfo("Успешно сохранено", "/profile/'.Zend_Auth::getInstance()->getIdentity()->id.'");';
                } else {
                    $passwordValidator = new DR_Api_Validate_Validators_Password();
                    $messageTemplates = $passwordValidator->getMessageTemplates();
                    if (isset($Data['password']) && in_array($Data['password'], $messageTemplates)) {
                        $Data['password'] = 'Неверный формат';
                        $scripts[] = "$.showInfo('".implode('. ', $messageTemplates)."')";
                    } else {
                        $scripts[] = "$.showInfo('Неверно заполнены поля')";
                    }
                }

                $this->Response($Data, $scripts);

            } else
            {
                $this->view->headTitle('Редактирование профиля - adidas running');
                $this->view->user = api::getUsers()->getProfileInfo(Zend_Auth::getInstance()->getIdentity()->id);
                //_d($this->view->user);
            }
        } else
        {
            $this->_redirect('/');
        }
    }
    
    public function processcheckeventAction() {
        if(isset($_POST['data']) && is_array($_POST['data']) && count($_POST['data'])) {
            $eventsId = array();
            foreach($_POST['data'] as $event_id)
                $eventsId[] = intval($event_id);
            api::getEvents()->executeQuery('UPDATE Model_Events SET is_read = 1 WHERE owner_id = ' . Zend_Auth::getInstance()->getIdentity()->id . ' AND id IN ('.implode(', ', $eventsId).')');
        }
        die;    
    }


}
