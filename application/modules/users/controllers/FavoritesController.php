<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Users_favoritesController extends DR_Controllers_Site
{    
    private $templateNotFindData = '<div class="default_box">
                                        <div class="text_part">У вас нет избранных авторов. Добавляйте<br> понравившихся вам авторов из раздела ЛЮДИ<br> или из любой статьи на сайте.</div>
                                        <a class="button goTo favorite" href="/users/famous">перейти в раздел люди<span></span></a>
                                    </div>';
    public function preDispatch(){
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
    }
    public function indexAction() {
        if(!Model_Users::isCurrentUserProfile())
            $this->_redirect('/error404');
        $this->view->headTitle('Личный кабинет - adidas running');
        $id = $this->_getParam('id');
        $this->view->user =  api::getUsers()->getUserData($id);
        $this->view->favoritesData = array();
        $this->view->favoritesData['myFavorites'] = api::getReferences()->_new()
                            ->where('t.object_id', $id)->where('t.ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)
                            ->count();
        $this->view->favoritesData['favorites'] = api::getReferences()->_new()
                            ->where('t.reference_id', $id)->where('t.ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)
                            ->count();
    }
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $user_id = $this->_getParam('id', 0);

        if($this->_getParam('type', 'favorites') != 'my')
            api::getReferences()->_new(array('us.id', 'us.name', 'us.sname', 'us.avatar', 'author_event_sex'=>'us.sex'))
                                ->where('t.reference_id', $user_id)
                                ->group('us.id')
                                ->joinLeft(array('us' => api::USERS), 'us.id = t.object_id')
                                ->joinLeft(array('ev' => api::EVENTS), 'ev.resource_id = t.object_id AND ev.type = '.Model_Events::TYPE_FAVORITEUSERS, array('is_event'=>'ev.id', 'event_id'=>'ev.id', 'event_is_read'=>'ev.is_read'));
        else {
            api::getReferences()->_new(array('us.id', 'us.name', 'us.sname', 'us.avatar'))
                                ->where('t.object_id', $user_id)
                                ->joinLeft(array('us' => api::USERS), 'us.id = t.reference_id');
            $this->view->noFindData = $this->templateNotFindData;
        }
            
             
        list($this->view->data, $paginator) = api::getReferences()->where('t.ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)->pageRows($page, 10);

        $this->view->pageCount = $paginator->count();
        $this->view->needTemplate = 'partials/profile/favorites.tpl';
        parent::blockmaterialsAction();
    }
    public function processaddfavoritesAction() {
        $favorites_id = $this->_getParam('user_id', 0);
        $response = array('status'=>'error', 'message'=>'Произошла ошибка');
        $current_user = Zend_Auth::getInstance()->getIdentity()->id;
        if($favorites_id != $current_user) {
            $isAllreadyFavorite = api::getReferences()->_new()->where('t.object_id', $current_user)->where('t.ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)->where('t.reference_id', $favorites_id)->count();
            if(!$isAllreadyFavorite) {
                api::getReferences()->doSave(array('object_id'=>$current_user, 'ref_type'=>Model_References::REF_TYPE_USER_FAVORITEUSERS, 'reference_id'=>$favorites_id));
                Model_Events::updateEvent($favorites_id, $current_user, $current_user, Model_Events::TYPE_FAVORITEUSERS);
                $response['status'] = 'ok';
                $response['message'] = 'Пользователь успешно добавлен в избранные';
            } else {
                $response['message'] = 'Пользователь уже находится в избранном';
            }
        }
        die(json_encode($response));
    }
    public function processdeletefavoritesAction(){
        $current_user = Zend_Auth::getInstance()->getIdentity()->id;
        $favorites_id = $this->_getParam('user_id', 0);
        $data = api::getReferences()->_new()->where('t.object_id', $current_user)
                                    ->where('t.ref_type', Model_References::REF_TYPE_USER_FAVORITEUSERS)
                                    ->where('t.reference_id', $favorites_id)->row();
        if(count($data)) {
            api::getReferences()->doDelete($data->id);
            api::getReferences()->executeQuery('DELETE FROM Model_Events WHERE type = '.Model_Events::TYPE_FAVORITEUSERS.' AND owner_id = '.$data->reference_id.' AND resource_id = '.$data->object_id);
        }
        die;
    }
}