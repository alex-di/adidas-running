<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Users_clothesController extends DR_Controllers_Site
{
    private $templateNotFindData = array('have'=>'<div class="default_box">
                                                    <div class="text_part">У Вас нет беговой экипировки.<br> Добавляйте вещи из раздела ЭКИПИРОВКА или<br> из любой статьи с упоминанием вещей.</div>
                                                    <a class="button goTo clothes" href="/clothes">перейти в раздел экипировки<span></span></a>
                                                </div>',
                                        'like'=>'<div class="default_box">
                                                    <div class="text_part">Вас не заинтересовала ни одна из моделей беговой экипировки.</div>
                                                    <a class="button goTo clothes" href="/clothes">перейти в раздел экипировки<span></span></a>
                                                </div>');
    public function preDispatch(){
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
    }
    public function indexAction() {
        
        $this->view->headTitle('Личный кабинет - adidas running');
        $id = $this->_getParam('id');
        $this->view->user =  api::getUsers()->getUserData($id);
        $data = api::getReferences()->_new(array('total'=>'count(t.id)'))
                            ->where('t.object_id', $id)->where('t.ref_type', Model_References::REF_TYPE_USER_ITEMS)
                            ->joinLeft(array('mt' => api::META), 'mt.resource_id = t.id AND mt.modules_id = '.Model_Meta::ITEMS_REFERENCE_USERS." AND mt.key='".Model_Meta::ITEMS_REFERENCE_USERS_KEY."'", array('status' => 'mt.int_value'))
                            ->group('mt.int_value')
                            ->rows();
        $this->view->clothesStatus = array('have'=>0, 'like'=>0);
        if(count($data)) {
            foreach($data as $row) {
                if(!is_null($row->status))
                if($row->status)
                    $this->view->clothesStatus['have'] = $row->total;
                else
                    $this->view->clothesStatus['like'] = $row->total;
            }
        }
    }
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $user_id = $this->_getParam('id', 0);

        $status = 0;
        if($this->_getParam('type', 'have') != 'like')
              $status = 1;
        Zend_Registry::set('status_clothes_like', !$status);   
        list($this->view->data, $paginator) = api::getReferences()->_new(array('it.*', 'ref_id'=>'t.id'))
                                        ->where('t.object_id', $user_id)->where('t.ref_type', Model_References::REF_TYPE_USER_ITEMS)
                                        ->joinLeft(array('mt' => api::META), 'mt.resource_id = t.id AND mt.modules_id = '.Model_Meta::ITEMS_REFERENCE_USERS." AND mt.key='".Model_Meta::ITEMS_REFERENCE_USERS_KEY."'")
                                        ->joinLeft(array('it' => api::ITEMS), 'it.id = t.reference_id')
                                        ->joinLeft(array('ref' => api::REFERENCES), 'ref.reference_id = it.id and ref.ref_type = ' . Model_References::REF_TYPE_USER_ITEMS . ' and ref.object_id = '.Zend_Auth::getInstance()->getIdentity()->id, array())
                                        ->joinLeft(array('meta'=> api::META), 'meta.resource_id = ref.id and meta.modules_id = '.Model_Meta::ITEMS_REFERENCE_USERS." and meta.key='".Model_Meta::ITEMS_REFERENCE_USERS_KEY."'", array('status_clothes'=>'meta.int_value'))
                                        ->where('mt.int_value', $status)
                                        ->pageRows($page, 10);
        if(Model_Users::isCurrentUserProfile()) {
            if($status)
                $this->view->noFindData = $this->templateNotFindData['have'];
            else
                $this->view->noFindData = $this->templateNotFindData['like'];
        }
        $this->view->pageCount = $paginator->count();
        $this->view->needTemplate = 'partials/profile/clothes.tpl';
        parent::blockmaterialsAction();
    }
    public function processaddclothesAction() {
        $items_id = $this->_getParam('clothes_id', 0);
        $type= $this->_getParam('type', 0);
        $referenceItem = api::getReferences()->where('t.object_id', Zend_Auth::getInstance()->getIdentity()->id)->where('t.ref_type', Model_References::REF_TYPE_USER_ITEMS)->where('t.reference_id', $items_id)->row();
        $reference_id = null;
        if(count($referenceItem))
            $reference_id = $referenceItem->id;
        else
            $reference_id = api::getReferences()->doSave(array('object_id'=>Zend_Auth::getInstance()->getIdentity()->id, 'ref_type'=>Model_References::REF_TYPE_USER_ITEMS, 'reference_id'=>$items_id));
        $metaData = api::getMeta()->where('t.resource_id', $reference_id)->where('t.modules_id', Model_Meta::ITEMS_REFERENCE_USERS)->where('t.key', Model_Meta::ITEMS_REFERENCE_USERS_KEY)->row();
        $meta_id = null;
        if(count($metaData))
            $meta_id = $metaData->id;
        api::getMeta()->doSave(array('resource_id'=>$reference_id, 'modules_id'=>Model_Meta::ITEMS_REFERENCE_USERS, 'key'=>Model_Meta::ITEMS_REFERENCE_USERS_KEY, 'int_value'=>$type), $meta_id);
        $user_id = Zend_Auth::getInstance()->getIdentity()->id;
        Model_Events::updateEvent($user_id, $user_id, $items_id, Model_Events::TYPE_CLOTHESADD);
        die;
    }
    public function processdeleteclothesAction() {
        $items_id = $this->_getParam('clothes_id', 0);
        $referenceItems = api::getReferences()->where('t.reference_id', $items_id)
                                             ->where('t.object_id', Zend_Auth::getInstance()->getIdentity()->id)
                                             ->where('t.ref_type', Model_References::REF_TYPE_USER_ITEMS)->rows();
        if(count($referenceItems)) {
             foreach($referenceItems as $row) {
                 api::getReferences()->doDelete($row->id);
                 $metaData = api::getMeta()->_new()->where('t.resource_id', $row->id)
                                           ->where('t.modules_id', Model_Meta::ITEMS_REFERENCE_USERS)
                                           ->where('t.key', Model_Meta::ITEMS_REFERENCE_USERS_KEY)
                                           ->row();
                 if(count($metaData))
                    api::getMeta()->doDelete($metaData->id);
             }

        }
        die;
    }
}