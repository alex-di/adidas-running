<?php

class Model_Comments extends DR_Models_AbstractTable {
	
	const MATERIALS = 1;
	
	public function getComments($resource_id, $modules_id) {
		return $this->_new()->where('t.resource_id', $resource_id)->where('t.modules_id', $modules_id)->joinLeft(array('us' => api::USERS), "us.id = t.users_id",
						array('username' => "concat(us.name, ' ', us.sname)", 'us.avatar'))->order('t.id DESC')->rows();
	}
}




