<?php

/**

 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Comments_MaterialsController extends DR_Controllers_Comments {
	
	public function preDispatch() {
		$this->modules_id = Model_Comments::MATERIALS;
		parent::preDispatch();
	}
	
	public function indexAction() {
	   
        if($this->_hasParam('layout'))
            Zend_Layout::getMvcInstance()->setLayout($this->_getParam('layout'));
		$this->view->data = api::getComments()->getComments($this->_getParam('resource'), $this->modules_id);

	}

	public function saveAction() {
		$response = parent::saveAction();
		if ('ok' == $response['status']) {
			$response['body'] = $this->view
					->partial('partials/material_comments.tpl',
							array(
									'data' => array(
											'username' => Zend_Auth::getInstance()->getIdentity()->name . ' '
													. Zend_Auth::getInstance()->getIdentity()->sname,
											'avatar' => Zend_Auth::getInstance()->getIdentity()->avatar,
                                            'users_id' => Zend_Auth::getInstance()->getIdentity()->id,
											'text' => $response['text'], 'date' => time())));
		}
		die(json_encode($response));
	}
}
