<?php

/**
 * Модель для 
 */
class Model_References extends DR_Models_AbstractTable
{
    /**
     * условное обозначение
     * REF_TYPE_ object_id _ reference_id
     * */
    const REF_TYPE_MATERIAL_ITEMS = 1;
    const REF_TYPE_MATERIAL_MATERIALS = 2;
    const REF_TYPE_MATERIAL_SHOP = 3;
    const REF_TYPE_USER_EVENTS = 4;
    const REF_TYPE_RUNBASE = 5;
    const REF_TYPE_RUNBASE_EVENT = 6;
    const REF_TYPE_USER_ITEMS = 7;
    const REF_TYPE_USER_FAVORITEUSERS = 8;
    const REF_TYPE_USER_RUNROUTES = 9;
    const REF_TYPE_USER_CONTACTS = 10;
    const REF_TYPE_EVENT_COMMENTS = 11;
    
    public function getUserFavorites($users_id) {
        return $this->_new()
                ->joinLeft(array("us" => "Model_Users"), "us.id = t.reference_id", array('username'=>"concat(us.name,' ', us.sname)"))
                ->order('username ASC')
                ->where('t.object_id', $users_id)
                ->where('t.ref_type', self::REF_TYPE_USER_FAVORITEUSERS)
                ->forSelect(array('reference_id' => 'username'));
    }
    
    
    public function saveReference($object_id, $ref_type, $reference = array(), $date = null)
    {
        if($ref_type == self::REF_TYPE_RUNBASE) {
            $row = $this->_new()->where('t.object_id', $object_id)->where('t.reference_id', $reference)->where('t.date', $date)->where('t.ref_type', self::REF_TYPE_RUNBASE)->row();
            if(count($row)) {
                return false;
            }
        } else {
            $this->executeQuery('DELETE FROM Model_References WHERE object_id = ' . $object_id . ' AND ref_type = ' . $ref_type);
        }
        
        if(count($reference)) {
            foreach($reference as $reference_id) {
                if(0 != $reference_id)
                    $values = array('object_id'=>$object_id, 'ref_type'=>$ref_type, 'reference_id'=>$reference_id);
                    if(!is_null($date))
                        $values['date'] = $date;
                    $this->doSave($values);
                    
            }
        }
        return true;
    }
    
    public function deleteReference($object_id, $ref_type)
    {
        $this->executeQuery('DELETE FROM Model_References WHERE object_id = ' . $object_id . ' AND ref_type = ' . $ref_type);
        return true;
    }

}
