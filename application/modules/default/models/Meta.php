<?php 
/**
 * Модель для метаданных
 */
class Model_Meta extends DR_Models_AbstractTable
{
    const USERS = 1;   
    const MATERIALS = 2;
    const INTERNAL = 3;
    const ITEMS_REFERENCE_USERS = 4;
    const RUNROUTES_REFERENCE_USERS = 5;
    const MESSAGES = 6;
    const CONTACTSDELETE_REFERENCE_USERS = 7;
    
    const INTERNAL_KEY_TWITTER_LAST_HASH_TAG = 'twitter_last_hash_tags';
    const INTERNAL_KEY_INSTAGRAM_LAST_HASH_TAG = 'instagram_last_hash_tags';
    const INTERNAL_KEY_LAST_MATERIAL_LIKE = 'last_parse_material_like';
    
    const ITEMS_REFERENCE_USERS_KEY = 'status_reference_items';
    const RUNROUTES_REFERENCE_USERS_KEY = 'status_reference_runroutes';
    const MESSAGES_ATTACHMENTS_FILES_KEY = 'attachments';
    const MESSAGES_ATTACHMENTS_ROUTES_KEY = 'attachment_routes';
    const CONTACTSDELETE_REFERENCE_USERS_KEY = 'date_remove_dialog';
    
    public function getIntValue($key, $modules_id, $resource_id = 0) {
    	$data = $this->_new(array('int_value', 'id'))->where('t.key', $key)->where('t.modules_id', $modules_id)->where('t.resource_id', $resource_id)->row();
    	if(count($data)) 
    		return array($data->id, $data->int_value);
    	return array(null, null);
    }
    public static function saveData($key, $resource_id, $modules_id, $data = array('value'=>'')) {
        $data_function = each($data);
        $model = new self;
        $model->executeQuery("DELETE FROM Model_Meta WHERE `resource_id` = $resource_id AND `modules_id` = $modules_id AND `key`='$key'"); 
        $model->doSave(array('resource_id'=>$resource_id, 'modules_id'=>$modules_id, 'key'=>$key, $data_function['key']=>$data_function['value']));
    }
    public function getParserMinMaxId($namespace) {
        $data = $this->_new(array('value', 'id'))->where('t.key', $namespace)->where('t.modules_id', self::INTERNAL)->where('t.resource_id', 0)->row();
        if(count($data)) {
            return explode('^', $data->value);
        }
        return array(null, null);
    }
    public function saveParserMinMaxId($namespace, $min_id, $max_id) {
        if(!is_null($max_id) && !is_null($min_id)) {
            $this->executeQuery('DELETE FROM Model_Meta WHERE `modules_id` = '.self::INTERNAL." AND `resource_id` = 0 AND `key` ='$namespace'");
            $this->doSave(array('modules_id'=>self::INTERNAL, 'resource_id'=>0, 'key'=>$namespace, 'value'=>$min_id.'^'.$max_id));
        }
    }
}