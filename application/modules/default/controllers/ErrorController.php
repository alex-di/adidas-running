<?php

/**
 * �������
 * 
 * @author appalach
 * @version 1.3
 * @final
 */
class ErrorController extends DR_Controllers_Site
{
 
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        if(APPLICATION_ENV == 'production')
            $this->_redirect('/error404');
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
 
                // 404 error -- controller or action not found
                //$this->_redirect('/error404');
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }
        $this->view->exception = $errors->exception;
        $this->view->request   = $errors->request;
    }

    public function error404Action(){
        $this->getResponse()->setHttpResponseCode(404);    
    }
}
