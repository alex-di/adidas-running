<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>adidas Running</title>
        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <link href="http://adidas-running.ru/design/css/reset.css" rel="stylesheet" type="text/css">
        <link href="http://adidas-running.ru/design/css/style.css" rel="stylesheet" type="text/css">
        <link href="http://adidas-running.ru/design/css/mediaqueries.css" rel="stylesheet" type="text/css">
        <link href="http://adidas-running.ru/design/css/fixes.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="http://adidas-running.ru/css/adidas-energy.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="/design/js/jquery-1-9-0.js"></script>
        <script type="text/javascript" src="/design/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script type="text/javascript" src="http://adidas-running.ru/design/js/isotope.js"></script>  
        <script type="text/javascript" src="http://adidas-running.ru/design/js/styled.js"></script>
        <script type="text/javascript" src="http://adidas-running.ru/design/js/main.js"></script>
        <script type="text/javascript" src="http://adidas-running.ru/design/js/dialogmessage.js"></script>

        <script type="text/javascript" src="http://adidas-running.ru/js/jquery.preload-1.0.8-min.js"></script>
        <script type="text/javascript" src="http://adidas-running.ru/js/script.js"></script>

        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?49"></script>
        <script type="text/javascript" src="http://adidas-running.ru/design/js/social/auth.js"></script>
        <script type="text/javascript">
            $(function() {
                VK.init({apiId: 3661023});
            });
            
        </script>

    </head>
    <body>  
        <div id="wrapper">
            <header id="header">
                <div class="banner"><a class="limit" href="http://www.adidas.ru/%D0%BE%D0%B1%D1%83%D0%B2%D1%8C-%D0%B4%D0%BB%D1%8F-%D0%B1%D0%B5%D0%B3%D0%B0--energy-boost-w/Q21114_530.html?cgid=Running-Shoes&cm_vc=SEARCH"><img src="http://adidas-running.ru/data/banners/2013/05/d3ad5f7044bc0c11ccf6a5b386723455.png"></a></div>
                <div class="top_part">
                    <div class="limit">
                        <div class="span logo"><a href="/" class="logo"></a> </div>
                        <div class="span menu">
                            <nav>
                                <ul>
                                    <li class="yellow"><a href="/social">общение</a></li>
                                    <li class="green"><a href="/trainings">тренировки</a></li>
                                    <li class="black"><a href="/inspirate">мотивация</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="span search">
                              <form class="search" onsubmit="search(this);
                                return false">
                                <input type="button" value=" " />
                                <input type="text" placeholder="поиск" />
                            </form>
                            <script type="text/javascript">
                                function search(form) {
                                    var value = $(form).find('input[type="text"]').val();
                                    if ('' != value)
                                        window.location = '/search/q/' + value;
                                }
                            </script>
                            <ul class="social_ico">
                                <li class="fb"><a target="_blank" href="http://www.facebook.com/adidasRunning"></a></li>                                   
                                <li class="tw"><a target="_blank" href="https://twitter.com/adidasRU"></a></li>
                                <li class="vk"><a target="_blank" href="http://vk.com/adidasrunning"></a></li>
                            </ul>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </div>
                <div class="bottom_part limit">
                    <nav>
                        <ul>
                            <li><a href="/clothes">Одежда</a></li>
                            <li><a href="/routes">Маршруты</a></li>
                            <li><a href="/runbase">Runbase</a></li>
                            <li><a href="/micoach"><span>mi</span>coach</a></li>
                            <li><a href="/events">События</a></li>
                            <li><a href="/users/famous">Люди</a></li>
                        </ul>
                    </nav>
                    <div class="login_logout drop_menu_button">
                        <div class="login">Войти</div>
                    </div>
                    <form class="login_form drop_menu_content" onsubmit="return jphp('/auth/login', this)">
                        <label>войти с помощью</label>
                        <a href="javascript:void(0)" onclick="vkLogin()" class="vk">вконтакте<span></span></a>
                        <a href="javascript:void(0)" onclick="faceboockLogin()" class="fb">facebook<span></span></a>
                        <a href="javascript:void(0)" onclick="twitterLogin()" class="tw">twitter<span></span></a>
                        <div class="control"><input type="text" placeholder="E-mail" name="login"/></div>
                        <div class="control"><input type="password" placeholder="Пароль" name="password"/></div>
                        <div class="link_area">
                            <a href="/auth/register">Регистрация</a>
                            <a href="/auth/remember">Забыли пароль?</a>
                        </div>
                        <input type="button" value="вход" onclick="$('.login_form').submit()"/>
                        <div style="clear: both;"></div>
                    </form>
                </div>
            </header>
            <section id="contentEnergy">
                <div id="contentEnergyWrapper">
                    <div id="contentEnergy__header">
                        <h1>Заряди город энергией бега</h1>
                        <ul id="contentEnergy__stats">
                            <li>5 забегов</li>
                            <li>20000 бегунов</li>
                            <li>30000 амперчасов</li>
                        </ul>
                        <div id="contentEnergy__likes">
                            <div id="contentEnergy__likes_vk">
                                <!-- Put this script tag to the <head> of your page -->
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?97"></script>

                                <script type="text/javascript">
                                    VK.init({apiId: 3766513, onlyWidgets: true});
                                </script>

                                <!-- Put this div tag to the place, where the Like block will be -->
                                <div id="vk_like"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Like("vk_like", {type: "mini"});
                                </script>
                            </div>
                            <div id="contentEnergy__likes_fb">
                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-like" data-href="http://www.adidas-running.ru/energy" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
                            </div>
                            <div id="contentEnergy__likes_twi">
                                <a href="https://twitter.com/share" class="twitter-share-button" data-via="twitterapi" data-lang="en">Tweet</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                            </div>
                            <div id="contentEnergy_likes_google">
                                <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                                <g:plusone size="medium"></g:plusone>
                            </div>
                        </div>
                    </div>
                    <div id="contentEnergy__summary">
                        <p>Вы когда-нибудь задавались вопросом, что будет, если собрать энергию множества бегунов и направить ее на единую большую цель?</p>
                        <p>Мы решили проверить! На каждом забеге adidas мы выдаём бегунам  мобильные энергогенераторы – устройства, которые преобразуют кинетическую энергию, производимую во время бега. </p>
                    </div>

                    <div class="my_lamp_x" ></div>
                    <div class="my_lamp_1" id="lamp">
                        
                        <div class="amper-big">
                            <span class="fline">уже накоплено</span><br/>
                            <span class="sline">100 000 А</span>
                        </div>
                        
                        <div class="cep">
                            <div class="point_6_1"  ></div>
                            <div class="point_6_2"  ></div>
                            <div class="point_6_3"  ></div>
                            <div class="point_6_4"  ></div>
                            <div class="point_6_5"  ></div>
                            <div class="point_6_6"  ></div>
                            <div class="point_6_7"  ></div>

                            <div class="point_7_1"  ></div>
                            <div class="point_7_2"  ></div>
                            <div class="point_7_3"  ></div>
                            <div class="point_7_4"  ></div>
                            <div class="point_7_5"  ></div>
                            <div class="point_7_6"  ></div>
                            <div class="point_7_7"  ></div>
                            <div class="point_7_8"  ></div>
                            <div class="point_7_9"  ></div>
                            <div class="point_7_10"  ></div>
                            <div class="point_7_11"  ></div>

                            <div class="point_8_1"  ></div>
                            <div class="point_8_2"  ></div>
                            <div class="point_8_3"  ></div>
                            <div class="point_8_4"  ></div>
                            <div class="point_8_5"  ></div>

                            <div class='popup_6'> 
                                <div class="popup_text2" >
                                    <div class="popup_discr2" >Между забегами генерируй <br> энергию бега на беговой базе  adidas runbase></div>                   
                                    <div class="popup_login2"><br>                                   
                                        <a href="http://adidas-running.ru/runbase/" class="popup_6_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_6_login_img" />
                                    </div>
                                </div>
                            </div>

                            <div class='popup_7'> 
                                <div class="popup_text" >
                                    <div class="popup_discr" >Продолжаем собирать   <br> энергию даже во время <br> тренировок</div>                   
                                    <div class="popup_login"><br>                                   
                                        <a href="http://adidas-running.ru/runbase/" class="popup_7_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_7_login_img" />
                                    </div>
                                </div>
                            </div>

                            <div class='popup_8'> 
                                <div class="popup_text" >
                                    <div class="popup_discr" >Приходи на adidas runbase>  <br> и сделай свой вклад в <br> энергию бега</div>                   
                                    <div class="popup_login"><br>                                   
                                        <a href="http://adidas-running.ru/runbase/" class="popup_8_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_8_login_img" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cep1">
                            <div class="point_1"  ></div>
                            <div class="point_2"  ></div>
                            <div class="point_3"  ></div>
                            <div class="point_4"  ></div>
                            <div class="point_5"  ></div>
                        </div>
                        <div class='popup_1' >
                            <div class="popup_text " >
                                <div class="popup_date">13.07 </div>
                                <div class="popup_title">ADIDAS <br>ENERGY RUN</div> 
                                <div class="popup_discr" >Первый энергетический<br> забег, открывающий проект<br> energy run.</div> 
                                <div class="popup_km"><strong>10 и 3 км<br>15000 человек</strong></div>
                                <div class="popup_login"><br>
                                    <a href="#" class="popup_1_about_link"><strong>О забеге</strong></a> <img class="popup_1_about_img" src="http://adidas-running.ru/images/fly.png" /> <br>
                                    <a href="#" class="popup_1_login_link"><!--strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_1_login_img" /-->
                                </div>
                            </div>
                        </div>
                        <div class='popup_2'> 
                            <div class="popup_text" >
                                <div class="popup_date">04.08 </div>
                                <div class="popup_title">ОСЕННИЙ <br>ГРОМ</div> 
                                <div class="popup_discr" >Ежегодный полумарафон по <br>осенней Москве<br></div> 
                                <div class="popup_km"><strong>21 и 10 км<br>3000 человек</strong></div>
                                <div class="popup_login"><br>
                                    <a href="#" class="popup_2_about_link"><strong>О забеге</strong></a> <img class="popup_2_about_img" src="http://adidas-running.ru/images/fly.png" /> <br>
                                    <a href="#" class="popup_2_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_2_login_img" />
                                </div>
                            </div>
                        </div>
                        <div class='popup_3'> 

                            <div class="popup_text" >
                                <div class="popup_date">25.07 </div>
                                <div class="popup_title">КРАСОЧНЫЙ  <br>ЗАБЕГ</div> 
                                <div class="popup_discr" >Самые веселые и яркие<br> 5 километров по Москве!</div> 
                                <div class="popup_km"><strong>5 км<br>2500 человек</strong></div>
                                <div class="popup_login"><br>
                                    <a href="#" class="popup_3_about_link"><strong>О забеге</strong></a> <img class="popup_3_about_img" src="http://adidas-running.ru/images/fly.png" /> <br>
                                    <a href="#" class="popup_3_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_3_login_img" />
                                </div>
                            </div>
                        </div>
                        <div class='popup_4'> 
                            <div class="popup_text" >
                                <div class="popup_date">15.09 </div>
                                <div class="popup_title">МОСКОВСКИЙ <br>МАРАФОН</div> 
                                <div class="popup_discr" >Легендарный марафон, в<br> котором из года в год <br> принимают участие тысячи<br>бегунов.</div> 
                                <div class="popup_km"><strong>5 км<br>2500 человек</strong></div>
                                <div class="popup_login"><br>
                                    <a href="#" class="popup_3_about_link"><strong>О забеге</strong></a> <img class="popup_3_about_img" src="http://adidas-running.ru/images/fly.png" /> <br>
                                    <a href="#" class="popup_3_login_link"><strong>Зарегистрироваться</strong> </a><img src="http://adidas-running.ru/images/fly.png" class="popup_3_login_img" />
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <ol id="contentEnergy__checkpoints">
                        <li id="contentEnergy__checkpoints_item1">
                            <time datetime="2013-07-13">13.07</time>
                            <span class="title">Adidas Energy Run</span>
                            <span class="amper-way">3000 А</a>
                        </li>
                        <li id="contentEnergy__checkpoints_item2">
                            <time datetime="2013-08-04">04.08</time>
                            <span class="title">Осенний гром</span>
                        </li>
                        <li id="contentEnergy__checkpoints_item3">
                            <time datetime="2013-08-25">25.08</time>
                            <span class="title">Красочный забег</span>
                        </li>
                        <li id="contentEnergy__checkpoints_item4">
                            <time datetime="2013-09-15">15.09</time>
                            <span class="title">Московский марафон</span>
                        </li>
                    </ol>

                    <div id="contentEnergy__aside">
                        <h2 class="green">94 бегуна зарядили город энергией бега во время adidas energy run</h2>
                        <h2>как направить свою энергию бега на общую цель</h2>
                        <ol>
                            <li class="first">
                                <span class="icon icon-arrow"></span>
                                <p>Приходи на забеги adidas или на runbase &gt;</p>
                            </li>
                            <li>
                                <span class="icon icon-clock"></span>
                                <p>Бегай с мобильным энергогенератором, который тебе выдадут на старте</p>
                            </li>
                            <li>
                                <span class="icon icon-accumulate"></span>
                                <p>Заряжай своей энергией специальный накопитель вместе с тысячами других бегунов  </p>
                            </li>
                            <li>
                                <span class="icon icon-run"></span>
                                <p>Стань частью общего энергетического порыва и отправляйся на финальный забег </p>
                            </li>
                            <li class="last">
                                <span class="icon icon-smile"></span>
                                <p>Улыбнись, когда твоя энергия превратится в свет, озаривший спортивную площадку, которая никогда раньше не была освещена</p>
                            </li>
                        </ol>
                    </div>
                </div>
            </section>
    </body>
</html> 