<?php

/**
 * Просмотр материала
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Items_reviewController extends DR_Controllers_Site {
    
    
    public function indexAction() {

        $data= api::getItems()->_new()->where('t.stitle', $this->_getParam('stitle'))
            ->joinLeft(array('ref'=>'Model_References'), 'ref.reference_id = t.id and ref.ref_type = '.Model_References::REF_TYPE_MATERIAL_SHOP, array('ref.object_id'))
            ->rows();
        if(!count($data))
            $this->_redirect('/');
        $ids = array();
        foreach($data as $item) {
            $this->view->item = $item;
            if(!empty($item['object_id']))
                $ids[] = $item['object_id'];
        }
        if(count($ids)) {
            $this->view->data = api::getMaterials()->getMaterials($ids);
        }
        
    }
}