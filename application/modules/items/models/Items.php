<?php

class Model_Items extends DR_Models_AbstractTable {
    public function doSave($values, $id = null, $returnData = false) {
        if(!isset($values['stitle']) && isset($values['name'])) {
            $values['stitle'] = $this->stitleUnique(DR_Api_Tools_Tools::stitle($values['name'], 40), $id);
        }
        return parent::doSave($values, $id, $returnData);
    }
}
