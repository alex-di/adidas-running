<?php

class Model_Messages extends DR_Models_AbstractTable {
    
    public function getLastMessages($users_id) {
        $query = api::getContacts()->fields(array("messages_id"=>"(select id from Model_Messages where (users_to = t.users_id and users_from = t.contacts_id) or (users_from = t.users_id and users_to = t.contacts_id) order by id desc limit 0,1)"))
            ->where("t.users_id", $users_id)->debug();
        
        $data = $this->_new()->fields(array("*"))
                     ->joinLeft(array("us" => "Model_Users"), "us.id = t.users_from", array("username_from" => "concat(us.name, ' ', us.sname)", "avatar_from"=>"us.avatar"))
                     ->joinLeft(array("us_to" => "Model_Users"), "us_to.id = t.users_to", array("username_to" => "concat(us_to.name, ' ', us_to.sname)", "avatar_to"=>"us_to.avatar"))
                     ->queryIn("t.id", $query)
                     ->order(array("t.status ASC", "t.id DESC"))
                     //->order("t.id DESC")
                     ->limit(100)
                     ->rows();
        $attachments = array();
        if(count($data)) {
            $metaData = api::getMeta()->_new(array('t.resource_id', 't.file_id','fl.path', 'fl.name', 'fl.ext', 'fl.mime'))
                              ->queryIn('t.resource_id', $query)
                              ->where('t.modules_id', Model_Meta::MESSAGES)
                              ->in('t.key', array(Model_Meta::MESSAGES_ATTACHMENTS_FILES_KEY, Model_Meta::MESSAGES_ATTACHMENTS_ROUTES_KEY))
                              ->joinLeft(array("fl" => "Model_Files"), "fl.id = t.file_id")
                              ->rows();
            foreach($metaData as $row) {
                if(!empty($row->file_id))
                    $attachments[$row->resource_id]['attachments'][$row->file_id] = array('path'=>$row->path, 'mime'=>$row->mime, 'ext'=>$row->ext, 'name'=>$row->name);
            }
        }
        return array($data, $attachments);
    }
    public function getDialog($users_id, $oponent_id) {
        $this->executeQuery("UPDATE Model_Messages SET status = 1 WHERE users_from = $oponent_id AND users_to = $users_id");
        
        $metaData = api::getReferences()->_new(array('m.date_value'))
                           ->where('t.object_id', $users_id)
                           ->where('t.ref_type', Model_References::REF_TYPE_USER_CONTACTS)
                           ->where('t.reference_id', $oponent_id)
                           ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::CONTACTSDELETE_REFERENCE_USERS." AND m.key = '".Model_Meta::CONTACTSDELETE_REFERENCE_USERS_KEY."'")
                           ->row();
        $this->_new()->fields(array("*"))
                     ->joinLeft(array("us_from" => "Model_Users"), "us_from.id = t.users_from", array("user_name" => "concat(us_from.name, ' ', us_from.sname)","avatar"=>"us_from.avatar"))
                     ->where("t.id IN(select id from Model_Messages where (users_from = $users_id and users_to=$oponent_id) or (users_from =$oponent_id and users_to=$users_id))")
                     ->order("t.id DESC")
                     ->limit(100);
        if(count($metaData) && !empty($metaData->date_value))
            $this->where('t.date', $metaData->date_value, '>');
                 
        $data = $this->rows();
        $result = array();
        if(count($data)) {
            foreach($data as $row) {
                $result[$row->id]['id'] = $row->id;
                $result[$row->id]['users_to'] = $row->users_to;
                $result[$row->id]['users_from'] = $row->users_from;
                $result[$row->id]['username'] = $row->user_name;
                $result[$row->id]['avatar'] = $row->avatar;
                $result[$row->id]['date'] = $row->date;
                $result[$row->id]['text'] = $row->text;
            }
            $messages_id = array_keys($result);
            $data = api::getMeta()->_new(array('t.resource_id', 't.file_id','fl.path', 'fl.name', 'fl.ext', 'fl.mime'))
                          ->in('t.resource_id', $messages_id)->where('t.modules_id', Model_Meta::MESSAGES)
                          ->in('t.key', array(Model_Meta::MESSAGES_ATTACHMENTS_FILES_KEY, Model_Meta::MESSAGES_ATTACHMENTS_ROUTES_KEY))
                          ->joinLeft(array("fl" => "Model_Files"), "fl.id = t.file_id")
                          ->rows();
            if(count($data)) {
                foreach($data as $row) {
                    if(!empty($row->file_id))
                        $result[$row->resource_id]['attachments'][$row->file_id] = array('path'=>$row->path, 'mime'=>$row->mime, 'ext'=>$row->ext, 'name'=>$row->name);
                }
            }
            ksort($result);
        }
        return $result;
    }
    
    public static function getCountUnread($users_id) {
        $model = new self;
        return $model->where("users_to", $users_id)->where("status", 0)->count();
    }
}