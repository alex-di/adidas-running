<?php

class Model_Contacts extends DR_Models_AbstractTable {
    
    public function getContactList($users_id) {
        return $this->_new()
                        ->joinLeft(array("us" => "Model_Users"), "us.id = t.contacts_id", array('username'=>"concat(us.name,' ', us.sname)"))
                        ->order('username ASC')
                        ->where('t.users_id', $users_id)
                        ->forSelect(array('contacts_id' => 'username'));
    }
}