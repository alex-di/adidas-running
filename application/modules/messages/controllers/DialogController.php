<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Messages_dialogController extends DR_Controllers_Site { 
    

    public function indexAction() {
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        if($this->_getParam('users_id') == Zend_Auth::getInstance()->getIdentity()->id)
            $this->_redirect('/error404');
        $this->view->users_id = $this->_getParam('users_id');
        $this->view->user = api::getUsers()->getUserData(Zend_Auth::getInstance()->getIdentity()->id);
        $this->view->messages = api::getMessages()->getDialog(Zend_Auth::getInstance()->getIdentity()->id, intval($this->_getParam('users_id')));
    }

}