<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Messages_indexController extends DR_Controllers_Site { 
    

    public function indexAction() {
        Zend_Layout::getMvcInstance()->setLayout('layout_profile');
        $this->view->user = api::getUsers()->getUserData(Zend_Auth::getInstance()->getIdentity()->id);
        list($this->view->messages, $this->view->attachments) = api::getMessages()->getLastMessages(Zend_Auth::getInstance()->getIdentity()->id);
    }
    public function blockmessageAction(){
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $this->view->contactList = array();
        if($this->_hasParam('users_id')) {
            $this->view->contactList = array($this->_getParam('users_id', 0) => $this->_getParam('username'));
        } else {
            $this->view->contactList = api::getReferences()->getUserFavorites(Zend_Auth::getInstance()->getIdentity()->id);
        }
    }
    public function processdeletedialogAction(){
        $users_id = Zend_Auth::getInstance()->getIdentity()->id;
        $contacts_id = intval($this->_getParam('id'));
        api::getMessages()->executeQuery("DELETE FROM Model_Contacts WHERE users_id = $users_id AND contacts_id = $contacts_id");
        $reference = api::getReferences()->_new(array('t.id', 'meta_id'=>'m.id'))
                           ->where('t.object_id', $users_id)
                           ->where('t.ref_type', Model_References::REF_TYPE_USER_CONTACTS)
                           ->where('t.reference_id', $contacts_id)
                           ->joinLeft(array('m' => api::META), 'm.resource_id = t.id AND m.modules_id = '.Model_Meta::CONTACTSDELETE_REFERENCE_USERS." AND m.key = '".Model_Meta::CONTACTSDELETE_REFERENCE_USERS_KEY."'")
                           ->row();
        $metaId = null;
        $resource_id = 0;
        if(count($reference)) {
            $metaId = $reference->meta_id;
            $resource_id = $reference->id;
        } else {
            $resource_id = api::getReferences()->doSave(array('object_id' => $users_id, 'ref_type'=>Model_References::REF_TYPE_USER_CONTACTS, 'reference_id'=>$contacts_id));
        }
        api::getMeta()->doSave(array('resource_id' => $resource_id, 
                                      'modules_id' => Model_Meta::CONTACTSDELETE_REFERENCE_USERS, 
                                      'key' => Model_Meta::CONTACTSDELETE_REFERENCE_USERS_KEY, 
                                      'date_value' => new Zend_Db_Expr('NOW()')), $metaId);
        die;
    }

}