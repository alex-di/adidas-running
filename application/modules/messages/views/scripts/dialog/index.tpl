<script type="text/javascript" language="javascript" src="/design/modules/lytebox/lytebox1.js"></script>
<link rel="stylesheet" href="/design/modules/lytebox/lytebox1.css" type="text/css" media="screen"/>

<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible form_parent">
    	<!--<div class="first_element_container">
            <? if(!empty($this->user->card_number)): ?>                             
                <div class="element card <?echo $this->user->sex == 'woman' ? 'women' : 'man'?>"><?= $this->user->card_number ?></div> 
            <?endif;?>
            <div class="element counter article_counter">
                <div class="container">
                    <div class="number"><?echo $this->user->count_articles?></div><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_articles, array("статья добавлена", "статей добавлено", "статей добавлено", "статьи добавлено", "статей добавлено"))?>
                    <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_add')?>" class="add">добавить статью<span></span></a>
                    <div class="lines"></div>
                </div>                                    
            </div>            
            
            <div class="element counter route_counter">
                <div class="route_map">
                    <img src="/design/images/route/289.png" alt="img" />
                    <img src="/design/images/route/143.png" alt="img" />
                    <img src="/design/images/route/126.png" alt="img" />
                </div>
                <div class="number"><?echo $this->user->count_run?></div> 
                <div class="lines"></div>
                <div class="text"><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_run, array("маршрут пройден", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено"))?></div>
                <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_add')?>" class="add">добавить маршрут<span></span></a>
            </div>
        </div>-->    
                 
        <!--
        <div class="grey_panel">
            <div class="to_dialogs" onclick="window.location='/messages'">К списку диалогов</div>
            <div class="del_dialog del_dialog_link" onclick="deleteDialogs(<?echo $this->users_id?>)">Удалить диалог<span></span></div>
        </div>
        -->
        <div class="write_message" onclick="window.location='/messages'">
          <div class="write_message_arrow"></div>
          к списку диалогов
        </div>
        
        
        <div id="message_content_block" class="message_container in_dialog <?if(count($this->messages)):?>hasmessage<?endif;?>"><!-- .in_dialog -->
            <?if(count($this->messages)):?>
                <?foreach($this->messages as $row):?>
                    <div class="message_point">
                        <a target="_blank" href="<?echo $this->url(array('id'=>$row['users_from']), 'profile_view')?>" class="ava_box">
                            <?if(empty($row['avatar'])):?>
                                <img src="/design/images/avatar_default.png" alt="img" />
                            <?else:?>
                                <img src="<?echo Model_Files::getPreview($row['avatar'], 260);?>" alt="img" />
                            <?endif;?>
                        </a>
                        <div class="title">
                            <a target="_blank" href="<?echo $this->url(array('id'=>$row['users_from']), 'profile_view')?>" class="name"><?echo $row['username']?></a>
                            <div class="date"><?php echo DR_Api_Tools_Tools::getCommentDate($row['date'])?></div>
                        </div>
                        <div class="massage_text">
                            <?echo $row['text']?>
                            <?if(isset($row['attachments']) && count($row['attachments'])):?>
                                <div class="img_inside">
                                    <?foreach($row['attachments'] as $file):?>
                                        <a rel="lytebox[messages<?echo $row['id']?>]" href="<?echo $file['path']?>" target="_blank" style="display: none;">
                                            <img onload="$(this).parent().fadeIn(200)" src="<?echo Model_Files::getPreview($file['path'], 180)?>" alt="img" />
                                        </a>
                                    <?endforeach?>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif?>
        </div>
        <!-- -->
        <form id="contaierNewMessage" class="new_message">
            <div class="form_lines"></div>
            <textarea id="messageFieldText" placeholder="введите ваше сообщение..."></textarea>
            <div id="controlSizeTextarea"></div>
            <div class="load_img_box">
                <div class="preview_point loading" style="display: none;">
                    <div class="progress_bar"><div class="complete" style="width: 70%;"></div></div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="send" onclick="message()"><span></span>Ответить</div>
            <div id="upload_pickfiles_1" class="add_photo"><span></span>добавить фото</div>
            <div style="clear: both;"></div>
        </form>
        <?echo $this->upload('upload_pickfiles_1', 'usmessage', 'flash', array('container'=>'contaierNewMessage','error'=>'onErrorUploadToMessages','filesAdded'=>'startPhotoUpload','uploadProgress'=>'progressBarPhoto', 'fileUploaded'=>'uploadPhoto'));?>       
     </div>
 </div>
 <script type="text/javascript">
    $(function(){
        initLytebox();
        setInterval(function(){
            controleSizeInputFields($('#messageFieldText'), $('#controlSizeTextarea'), 1500)
        }, 100);
    })
    function repositionMessageForm() {
        var BOTTOM = 40;
        var ui = $('#contaierNewMessage');
        var topLimit = $('.message_container').offset().top;
        var bottomLimit = $('.message_container').offset().top + $('.message_container').height();
        
        var percent = $(window).scrollTop() / ($(document).height() - $(window).height());
        var position = percent * ($(document).height() - $(window).height()) + $(window).height() - ui.height() - BOTTOM;
        if(position < topLimit)
            position = topLimit;
        if(position > bottomLimit)
            position = bottomLimit;
        ui.offset({top: position});
    }
    $(window).scroll(function(){
        repositionMessageForm();
    });
    repositionMessageForm();
    
     MenuNavigator.setCurrentProfileMenu('messages');
     scrollMessageBlock();
     packet.to = <?echo $this->users_id?>;
     function message() {
        $('.preview_point.load_image').each(function(){
            var imageData = $(this).data('imagedata');
            packet.attachments.files.push(imageData.id);
            packet.additional.files.push(imageData.path);
        });
        $('.preview_point.load_image').remove();
        var ui = $('.new_message textarea');
        packet.text = ui.val();
        ui.val('');
        sendMessage();
     }
     function uploadPhoto(up, file, response) {
        var jsa = $.parseJSON(response.response);
        if('error' != jsa.status) {
            var ui = $('.load_img_box .preview_point.loading');
            ui.hide();
            var preview = getPreviewImage(jsa.file, 100);
            ui.before('<div id="image'+jsa.id+'" class="preview_point load_image">'+
                            '<div class="del" onclick="$(this).parent().remove()"></div>'+
                            '<img src="'+preview+'" alt="img" />'+
                        '</div>');
            $('#image'+jsa.id).data('imagedata', {path: jsa.file, id: jsa.id});  
        } else {
            $.showInfo(jsa.message);
        }
        repositionMessageForm();        
    }
    function progressBarPhoto(up, file){
        $('.load_img_box .preview_point.loading .progress_bar').css('width', file.percent+'%');
    }
    function startPhotoUpload(up, file){
        var ui = $('.load_img_box .preview_point.loading');
        ui.find('.progress_bar').css('width', '0%');
        ui.show();
        repositionMessageForm();
    }

 </script>
             