<div class="modal write_message_box" id="write_massage" style="display: none;">
    <div class="title">новое сообщение</div>
    <form>
        <? $firstKey = count($this->contactList) == 1 ? array_keys($this->contactList) : 0 ?>
        <div class="combo <?if($firstKey):?>disabled<?endif;?>">
            <div class="current"><?if(count($this->contactList) == 1): echo trim($this->contactList[$firstKey[0]]); else:?>кому<?endif;?></div>
            <div class="arrow"></div>
            <ul>
                <?if(count($this->contactList)):?>
                    <?foreach($this->contactList as $id=>$name):?>
                        <li data-id="<?echo $id?>"><?echo trim($name)?></li>
                    <?endforeach;?>
                <?endif;?>
            </ul>
            <input id="users_to" type="text" value="<?echo count($this->contactList) == 1 ? $firstKey[0] : ''?>"/>
        </div>
        <textarea id="messageFieldText" placeholder="написать ответ..."></textarea>
        <div id="controlSizeTextarea"></div>
        <div class="load_img_box">
            <div class="preview_point loading" style="display: none;">
                <div class="progress_bar"><div class="complete" style="width: 70%;"></div></div>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div class="send" onclick="fastMessage()"><span></span>отправить</div>
        <div class="close" onclick="$.modal().close()"><span></span>отменить</div>
        <div id="upload_pickfiles_1" class="add_photo"><span></span>добавить фото</div>
        <div style="clear: both;"></div>
        <?echo $this->upload('upload_pickfiles_1', 'usmessage', 'flash', array('error'=>'onErrorUploadToMessages','filesAdded'=>'startPhotoUpload','uploadProgress'=>'progressBarPhoto', 'fileUploaded'=>'uploadPhoto'));?> 
        <script type="text/javascript">
            setInterval(function(){
                controleSizeInputFields($('#messageFieldText'), $('#controlSizeTextarea'), 1500)
            }, 100);
             initStyleComboBox();
             function fastMessage() {
                var users_to = $('#users_to');
                packet.to = parseInt(users_to.val());
                if(0 != packet.to && !isNaN(packet.to)) {
                    users_to.parent().removeClass('error');
                    $('.preview_point.load_image').each(function(){
                        var imageData = $(this).data('imagedata');
                        packet.attachments.files.push(imageData.id);
                        packet.additional.files.push(imageData.path);
                    });
                    $('.preview_point.load_image').remove();
                    var ui = $('.modal.write_message_box textarea');
                    if(packet.additional.files.length || $.trim(ui.val()) != '') {
                        ui.removeClass('error');
                        packet.text = ui.val();
                        if(sendMessage()) {
                            $.modal().close();
                            $.showInfo('Сообщение отправлено');
                        }
                    } else {
                        ui.addClass('error');
                    }
                } else {
                    users_to.parent().addClass('error');
                }
             }
             function uploadPhoto(up, file, response) {
                var jsa = $.parseJSON(response.response);
                if('error' != jsa.status) {
                    var ui = $('.load_img_box .preview_point.loading');
                    ui.hide();
                    var preview = getPreviewImage(jsa.file, 100);
                    ui.before('<div id="image'+jsa.id+'" class="preview_point load_image">'+
                                    '<div class="del" onclick="$(this).parent().remove()"></div>'+
                                    '<img src="'+preview+'" alt="img" />'+
                                '</div>');
                    $('#image'+jsa.id).data('imagedata', {path: jsa.file, id: jsa.id});  
                } else {
                    $.showInfo(jsa.message);
                }
            }
            function progressBarPhoto(up, file){
                $('.load_img_box .preview_point.loading .progress_bar').css('width', file.percent+'%');
            }
            function startPhotoUpload(up, file){
                var ui = $('.load_img_box .preview_point.loading');
                ui.find('.progress_bar').css('width', '0%');
                ui.show();
            }
     </script>
    </form>
</div>