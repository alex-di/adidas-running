

<?echo $this->partial('partials/profile/person_box.tpl', array('user'=>$this->user));?>
<div class="section vertical">
    <?echo $this->partial('partials/profile/left_menu.tpl', array('user'=>$this->user));?>
    <div class="box visible">
    	<!--<div class="first_element_container">
            <? if(!empty($this->user->card_number)): ?>                             
                <div class="element card <?echo $this->user->sex == 'woman' ? 'women' : 'man'?>"><?= $this->user->card_number ?></div>
            <?endif;?>
            <div class="element counter article_counter">
                <div class="container">
                    <div class="number"><?echo $this->user->count_articles?></div><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_articles, array("статья добавлена", "статей добавлено", "статей добавлено", "статьи добавлено", "статей добавлено"))?>
                    <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_articles_add')?>" class="add">добавить статью<span></span></a>
                    <div class="lines"></div>
                </div>                                    
            </div>            
            
            <div class="element counter route_counter">
                <div class="route_map">
                    <img src="/design/images/route/289.png" alt="img" />
                    <img src="/design/images/route/143.png" alt="img" />
                    <img src="/design/images/route/126.png" alt="img" />
                </div>
                <div class="number"><?echo $this->user->count_run?></div> 
                <div class="lines"></div>
                <div class="text"><?echo DR_Api_Tools_Tools::getWordsByNumber($this->user->count_run, array("маршрут пройден", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено", "маршрутов пройдено"))?></div>
                <a href="<?echo $this->url(array('id'=>Zend_Auth::getInstance()->getIdentity()->id), 'profile_routes_add')?>" class="add">добавить маршрут<span></span></a>
            </div>
        </div>
        <div class="write_message write_massage_link" onclick="messageDialog()"><div class="write_message_arrow"></div>написать сообщение</div>
        -->
        <div class="message_container">
            <?if(count($this->messages)):?>
                <?foreach($this->messages as $row):
                    $postfix = $row['users_from'] == Zend_Auth::getInstance()->getIdentity()->id ? '_to' : '_from';
                ?>
                    <div id="addresat<?echo $row['users'.$postfix]?>" style="cursor: pointer;"  onclick="clickByDialog(event, '<?echo $this->url(array('users_id'=>$row['users'.$postfix]), 'dialogs')?>', <?echo $row['users'.$postfix]?>, this)" class="message_point <?if(!$row['status'] && $row['users_from'] != Zend_Auth::getInstance()->getIdentity()->id):?>new<?endif?>">
                        <a href="<?echo $this->url(array('users_id'=>$row['users'.$postfix]), 'dialogs')?>" class="ava_box">
                            <?if(empty($row['avatar'.$postfix])):?>
                                <img src="/design/images/avatar_default.png" alt="img" />
                            <?else:?>
                                <img src="<?echo Model_Files::getPreview($row['avatar'.$postfix], 260)?>" alt="img" />
                            <?endif;?>
                        </a>
                        <div class="title">
                            <a href="<?echo $this->url(array('users_id'=>$row['users'.$postfix]), 'dialogs')?>" class="name"><?echo $row['username'.$postfix]?></a>
                            <div class="date"><?php echo DR_Api_Tools_Tools::getCommentDate($row['date'])?></div>
                        </div>
                        <div class="del_dialog" onclick="deleteDialogs(<?echo $row['users'.$postfix]?>)"></div>
                        <div class="massage_text">
                            <?echo $row['text']?>
                            <?if('' == trim($row['text']) && count($this->attachments) && isset($this->attachments[$row['id']]['attachments'])):?>
                                <div class="img_inside">
                                    <?foreach($this->attachments[$row['id']]['attachments'] as $file):?>
                                        <img  src="<?echo Model_Files::getPreview($file['path'], 180)?>" alt="img" />
                                    <?endforeach?>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif?>
        </div>                  
    </div>
 </div>
<script type="text/javascript">
    MenuNavigator.setCurrentProfileMenu('messages');
    function clickByDialog(event, url, user_id, ui) {
        event = event || window.event;
        if(hitTestPoint($(ui).find('.del_dialog'), event.pageX, event.pageY)) {
            deleteDialogs(user_id);
        } else {
            window.location = url;
        }
    }
</script> 