<?php

/**
 */
class Search_IndexController extends DR_Controllers_Site {

	public function indexAction() {
	
		$this->view->query = urldecode($this->_getParam('query', ''));
		$this->view->tag = urldecode($this->_getParam('tag', ''));
	}
    public function blockmaterialsAction()
    {
        $page = $this->_getParam('page', 1);
        $query = $this->_getParam('query', '');
        $tag = $this->_getParam('tag', '');
        //_d($tag);
        $ids = array();
        $materials = api::getMaterials();
        
        if(!empty($query)) {
            $data = $this->search($query);
            if(count($data)) {
                list($data, $paginator) = $materials->_new(array('id'))->in('t.id', $data)->where('t.id is not null')->where('is_modern', 1)->pageRows($page, 10); 
                $ids = Model_Materials::getArrayObjectId($data);
                $this->view->pageCount = $paginator->count();     
            }
        } elseif(!empty($tag)) {

            list($data, $paginator) = api::getKeywords()->_new(array('id'=>'mtk.materials_id'))->where('mtk.materials_id is not null')->where('t.name', $tag)->joinLeft(array('mtk' => 'Model_Materialkeys'),
						"mtk.keywords_id = t.id",
						array())->pageRows($page, 10);
            $this->view->pageCount = $paginator->count();
            $ids = Model_Materials::getArrayObjectId($data); 
        }
        if (count($ids))
        {
            $this->view->data = $materials->getMaterials($ids);
        }
        $this->view->noFindData = '<p style="margin: 0 0 0 20px">Ничего не найдено</p>';
        parent::blockmaterialsAction();
    }
    protected function search($query) {
        $sphinx = new DR_Api_Tools_Sphinx();
        $string = $sphinx->getSphinxKeyword($query);
        $sphinx->setServer('localhost', 9312);
        // Совпадение по любому слову
        $sphinx->setMatchMode(SPH_MATCH_EXTENDED2);

        // Результаты сортировать по релевантности
        $sphinx->SetSortMode( SPH_SORT_ATTR_ASC, "public_date" );
        $sphinx->setLimits(0, 1000, 1000);
        
        $result = $sphinx->query($string, 'adidas');
        //_d($result);
        //_d($result, false);
        //_d($sphinx->getLastError(), false);
        //_d($sphinx->getLastWarning());
        if ($result && isset($result['matches']))
            return array_keys($result['matches']);

        return array();
    }
}
