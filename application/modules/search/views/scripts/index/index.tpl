<div class="sort">
     <div class="title">
        <?if(!empty($this->query) || !empty($this->tag)):?>
            <?if(!empty($this->query)):?>
                Результаты поиска по запросу <span><?echo $this->escape($this->query)?></span>
            <?else:?>
                Результаты поиска по тегу <span><?echo $this->escape($this->tag)?></span>
            <?endif;?>
        <?else:?>
            Введите поисковый запрос
        <?endif;?>
     </div>
</div>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<?if(!empty($this->query) || !empty($this->tag)):?>
    <script type="text/javascript">
    	defaultPaginator = new Paginator('/search/index/blockmaterials');
        defaultPaginator.setParam('query', '<?echo $this->query?>')
                        .setParam('tag', '<?echo $this->tag?>');
    </script>
    <div id="container">
        <?= $this->action('blockmaterials', 'index', 'search', array('query'=>$this->query, 'tag'=>$this->tag))?>
    </div>
<?endif;?>