<?php /**
 * Модель для языков
 */
class Model_Languages extends DR_Models_AbstractTable
{
    const RU = 35;
    const EN = 36;
    const GR = 37;

    const SESSION_LANGUAGES_NAMESPACE = "language";
    const IS_INIT_SERVICE = "_launguages_init";
    const LANG_LIST = "_lang_list";
    
    public static function setLocale($locale_id)
    {
        $model = new self;
        $data = $model->where("id", $locale_id)->row();
        if (count($data)) {
            $ses = new Zend_Session_Namespace(self::SESSION_LANGUAGES_NAMESPACE);
            $ses->language_id = $data['id'];
            $ses->language_geography_suffix = $data['geography_suffix'];
        }
    }
    public static function getLocale()
    {
        $ses = new Zend_Session_Namespace(self::SESSION_LANGUAGES_NAMESPACE);
        if (!isset($ses->language_id)) {
            $model = new self;
            $data = $model->where("is_default", 1)->row();

            if (count($data)) {
                $ses->language_id = $data['id'];
                $ses->language_geography_suffix = $data['geography_suffix'];
            }
        }
        return array($ses->language_id, $ses->language_geography_suffix);
    }
    public static function getPhraseTranslate()
    {
        
        if(!Zend_Registry::isRegistered(self::IS_INIT_SERVICE)) {
            list($lang_id, $geo_id) = self::getLocale();
            
            Zend_Registry::set(self::LANG_LIST, Model_Phrases::getLanguagelist($lang_id));
            Zend_Registry::set(self::IS_INIT_SERVICE, true);
        }
        
        return Zend_Registry::get(self::LANG_LIST);
    }
    public static function addPhraseToTranslate($phrase) {
        
        Model_Phrases::savePhrase($phrase);
        
        $language_list = Zend_Registry::get(self::LANG_LIST);
        $language_list[$phrase] = "";
        Zend_Registry::set(self::LANG_LIST, $language_list);
    }
}
