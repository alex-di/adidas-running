<?php 
/**
 * 
 */
class Model_Phrases extends DR_Models_AbstractTable
{
    public static function getLanguagelist($language_id) {
        $model = new self;
        $data = $model->fields(array("t.phrase", "ls.value"))
                     ->joinLeft(array("ls" => "Model_Languageslists"), "ls.phrase_id = t.id and ls.language_id=$language_id")
                     ->rows();
        
        $result = array();
        if(count($data)) {
            foreach($data as $row) {
                $result[$row['phrase']] = is_null($row['value']) ? "" : $row['value'];
            }
        }

        return $result;
        
    }
    public static function savePhrase($phrase) {
        $model = new self;
        $model->doSave(array('phrase' => $phrase));
    }
}