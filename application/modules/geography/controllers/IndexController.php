<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Geography_indexController extends DR_Api_Controllers_Site {
    
    public function blockregionsAction(){
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $model_regions = new Model_Regions;
        $this->view->data = $model_regions->order("region_name_ru")->where('country_id', $this->_getParam("country_id"))->forselect(array('id' => 'region_name_ru'));
        //$this->view->data = $model_regions->getDataForSelect(array("country_id"=>$this->_getParam("country_id")), 'name');
    }
    public function blockcityAction(){
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $model_city = new Model_City;
        $this->view->data = $model_city->order("city_name_ru")->where('regions_id', $this->_getParam("regions_id"))->forselect(array('id' => 'city_name_ru'));
        //$this->view->data = $model_city->getDataForSelect(array("regions_id"=>$this->_getParam("regions_id")), 'name');
    }
}