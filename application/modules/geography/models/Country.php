<?php

/**
 * Модель для работы из странами
 */
class Model_Country extends DR_Models_AbstractTable
{

    const GEOGRAPHY_SUFFIX_RU = "ru";
    const GEOGRAPHY_SUFFIX_ENG = "en";

    public static function getGeographySuffixDescription()
    {

        return array(self::GEOGRAPHY_SUFFIX_RU => "русский", self::GEOGRAPHY_SUFFIX_ENG => "english");
    }
}
