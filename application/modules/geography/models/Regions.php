<?php

/**
 * Модель для работы c регионами
 */
class Model_Regions extends DR_Models_AbstractTable {
    
    public function getRegionsId($name) {
        $data = $this->_new(array('id'))->where('t.name', $name)->row();
        $id = null;
        if(count($data)) {
            $id = $data->id;
        }
        return $id;
    }
}