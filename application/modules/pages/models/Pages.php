<?php

/**
 * Модель для статичесик страниц
 */
class Model_Pages extends DR_Models_AbstractTable {
    
    const MICOACH_PAGE = 20;
    
    public function getPages($id) {
        if(!DR_Api_View_Helper_Translate::isLanguagesEnabled()) {
            return $this->where("id", $id)->row();
        } else {
            list($lang, $geo) = Model_Languages::getLocale();
            $data = $this->fields(array("t.name", "t.text", "lang_name"=>"lp.name", "lang_text"=>"lp.text"))
                     ->joinLeft(array("lp" => "Model_Languagespages"), "lp.pages_id = t.id and lp.language_id=".intval($lang))
                     ->where("t.id", $id)
                     ->row();
            if(!is_null($data['lang_text'])) {
                return array("name"=>$data['lang_name'], "text"=>$data['lang_text']);
            } else {
                return array("name"=>$data['name'], "text"=>$data['text']); 
            }
        }
    }
}