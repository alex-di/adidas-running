<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Pages_viewController extends DR_Controllers_Site {
    
    public function indexAction(){
        
        if($link=$this->_getParam("link")) 
            {             
                $model_pages = api::getPages();
                $this->view->data = $model_pages($link, 'stitle');
            }            
        else 
            {
                $this->_redirect("/");
            }        
    } 

}