<script type="text/javascript">
    var count_image = <? echo count($this->photo) ?>;
    function callback(uploder,file,response) {
		var jsa = jQuery.parseJSON(response.response);
        post("/files/manager/processaddphoto", {image:jsa.file, id:jsa.id, name:jsa.name, handler:'<?= $this->function ?>'}, function(data) {
            $(".scroll_block").prepend(data);
            $(".file_name").text("<?=$this->translate("файл")?> "+jsa.name+" <?=$this->translate("загружен")?>");
            updateCountImage(1);
            $(".center_container").click();
            init();
        });
    }
    // id - записи в мете
    function removePhoto(id) {
        post("/files/manager/processdeletephoto", {id:id}, function(data) {
            $("#photos_"+id).remove();
            updateCountImage(-1);
        });
    }
    
    function updateCountImage(i){
        count_image+=i;
        $(".img_counter").text("<?=$this->translate("Всего изображений")?>: "+count_image);
    }
    
    function init() {
       	$('.scroll_block img').hover(
        	  function () {
        	  	$(this).next('.remove_img').css('display', 'block');	
        	  }, 
              function(e){
                if(!hitTestPoint($(this).next('.remove_img'), e.pageX, e.pageY)) {
                    $(this).next('.remove_img').css('display', 'none');
                }
              }
    	);
        $('.remove_img').hover(function(){}, function(){
            $(this).css('display', 'none');
        });
    }
    /*function closePhotoWindow(){
        $.closeDialog();
        if(materialDialogForm != undefined) {
            $("#"+materialDialogForm).dialog("open"); 
        }
    }*/
    $(function(){
       /*$(".close_button").click(function(){
           closePhotoWindow(); 
       });*/
       init();
    });
</script>
<? echo $this->upload('pickfiles_uploader1', 'photo', 'flash', array("fileUploaded"=>"callback")) ?>
<div class="blue_field">
	<span class="load_span"><?=$this->translate("Загрузите изображение")?>:</span>
    <div id="pickfiles_uploader1" class="more load_file"><?=$this->translate("Загрузить")?></div>
    <span class="file_name"></span>
</div>
<div class="center_container">
	<span class="img_counter"><?=$this->translate("Всего изображений")?>: <? echo count($this->photo) ?></span>
    <div class="scroll_block">
    <? if(count($this->photo)): ?>
        <? foreach($this->photo as $photo): ?>
            <div id="photos_<? echo $photo->id  ?>" class="block_photo">
            	<img src="<? echo $photo->path ?>" alt="img" onclick="<?= $this->function ?>('<? echo $photo->path ?>', <? echo $photo->file_id  ?>)"/>
                <div class="remove_img" onclick="removePhoto(<? echo $photo->id ?>)"></div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
    </div>
    <div style="clear:both"></div>
    <div class="more exit_button4" onclick="$.closeDialog();"><?=$this->translate("Отмена")?></div>
</div>

