<div class="file_block" id="documents_<? echo $this->data['id'] ?>">
	<span class="name_file"><? echo $this->data['name'].".".$this->data['ext'] ?></span>
    <span class="load_time"><?=$this->translate("Добавлено")?> <? echo date("H:i d-m-H", time())?></span>
    <div class="button_panel">
    	<div class="more" onclick="<?= $this->function ?>('<? echo $this->data['name'].".".$this->data['ext'] ?>', <? echo $this->file_id ?>)"><?=$this->translate("Добавить в запись")?></div>
        <div class="more" onclick="window.location = '/files/index/download/id/<? echo $this->file_id ?>'"><?=$this->translate("Скачать")?></div>
        <div class="more" onclick="removeDocument(<? echo $this->data['id'] ?>)"><?=$this->translate("Удалить")?></div>
    </div>
</div>