<script type="text/javascript">
    var count_file = <? echo count($this->file) ?>;
    function callback(uploder,file,response) {
	var jsa = jQuery.parseJSON(response.response);
        post("/files/manager/processaddocument", {id:jsa.id, name:jsa.name, ext:jsa.ext, handler:'<?= $this->function ?>'}, function(data) {
            $(".scroll_block").prepend(data);
            updateCountImage(1);
            //init();
        });
    }
    function removeDocument(id) {
        post("/files/manager/processdeletedocument", {id:id}, function(data) {
            $("#documents_"+id).remove();
            updateCountImage(-1);
        });
    }
    
    function updateCountImage(i){
        count_file+=i;
        $(".img_counter").text("<?=$this->translate("Всего файлов")?>: "+count_file);
    }
    /*function closeDocumentWindow(){
        $.closeDialog();
        if(materialDialogForm != undefined) {
            $("#"+materialDialogForm).dialog("open"); 
        }
    }*/
    $(function(){
       /*$(".close_button").click(function(){
            if(materialDialogForm != undefined) {
                $("#"+materialDialogForm).dialog("open"); 
            }
       });*/
    });
</script>
<? echo $this->upload('pickfiles_uploader1', 'documents', 'flash' , array("fileUploaded"=>"callback")) ?>
<div class="blue_field">
	<span class="load_span"><?=$this->translate("Загрузить файл")?>:</span>
    <div id="pickfiles_uploader1" class="more load_file"><?=$this->translate("Загрузить")?></div>
    <span class="file_size"><?=$this->translate("Файл до 5Мб")?></span>
</div>
<div class="center_container">
	<span class="img_counter"><?=$this->translate("Всего файлов")?>: <? echo count($this->file) ?></span>
    <div class="scroll_block">
    <? if(count($this->file)): ?>
        <? foreach($this->file as $file): ?>
        	<div class="file_block" id="documents_<? echo $file->id ?>">
            	<span class="name_file"><? echo $file->name ?></span>
                <span class="load_time"><?=$this->translate("Добавлено")?> <? echo date("H:i d-m-H", strtotime($file->date))?></span>
                <div class="button_panel">
                	<div class="more" onclick="<?= $this->function ?>('<? echo $file->name ?>', <? echo $file->file_id ?>)"><?=$this->translate("Добавить в запись")?></div>
                    <div class="more" onclick="window.location = '/files/index/download/id/<? echo $file->file_id ?>'"><?=$this->translate("Скачать")?></div>
                    <div class="more" onclick="removeDocument(<? echo $file->id ?>)"><?=$this->translate("Удалить")?></div>
                </div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
    </div>
    <div style="clear:both"></div>
    <div class="more exit_button4" onclick="$.closeDialog();"><?=$this->translate("Отмена")?></div>
</div>
