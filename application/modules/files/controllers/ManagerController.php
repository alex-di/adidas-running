<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Files_managerController extends DR_Api_Controllers_Site
{
    /**
     * сохранить скриншот передаваемый флеш сервером
     */
    public function saveAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('ajax');

        if ($this->_getParam("id"))
        {
            
            $moduleName = "users";
            $extension = "jpg";

            $M = new DR_Api_Tools_Photo;
            $patch = $M->createSubDirectory("/data/" . $moduleName . "/" . date("Y") . "/" . date("m") . "/");
            $easyfilename = md5(microtime()) . "." . $extension;
            $webFileName = $patch . "/" . $easyfilename;
            $fileName = $_SERVER['DOCUMENT_ROOT'] . $patch . "/" . $easyfilename;

            $fp = fopen($fileName, 'wb');
            fwrite($fp, file_get_contents("php://input"));
            fclose($fp);

            $model_files = new Model_Files;
            $file_id = $model_files->doSave(array(
                "path" => $webFileName,
                "name" => 'screen_' . date("d_m_Y_H_i"),
                "ext" => $extension));
            $model_meta = new Model_Meta;
            $model_meta->doSave(array(
                "key" => "screenshot",
                "file_id" => $file_id,
                "resource_id" => $this->_getParam("id"),
                "modules_id" => Model_Meta::USERS));
        }
        die("OK");
    }


}
