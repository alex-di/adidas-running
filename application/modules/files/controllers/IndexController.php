<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Files_indexController extends DR_Controllers_Site
{


    public function indexAction()
    {
    }
    /**
     * Скачивание файла
     * 
     */
    public function downloadAction()
    {
        /*
        //Если нет доступа
        
        if (userHasNoPermissions) {
        $this->view->msg = 'This file cannot be downloaded!';
        $this->_forward('error', 'download');
        }
        */
        if(empty($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], "download") === false) {
            $this->_redirect("/");
        }
        $file_id = $this->_getParam('id');
        $file = $this->_modulesModel->getSimpleData(array("primary_key" => $file_id));
        $mime_type = "image/jpeg";
        $mime = Model_Files::getMimeTypes($file->ext);
        if(count($mime))
            $mime_type = $mime[0];
        header('Content-Type: '.$mime_type);
        header('Content-Disposition: attachment; filename="' . $file->name . '"');

        readfile($_SERVER['DOCUMENT_ROOT'] . $file->path);

        // отключаем вид
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     *	-	Назначение: Загрузить
     *	-	Дата: 24.12.2010 14:57:06
     */
    public function uploadAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        
        if ($_FILES['file']['tmp_name'])
        {
            $settings = array();
            $module = $this->_getParam('moduleName');
            $savePath = $this->_getParam('path', null);
            $settings['max_size'] = isset($this->view->settings[$module . '_max_size']) ? $this->view->settings[$module . '_max_size'] : 1;
            $settings['enabled_extension'] = explode(",", DR_View_Helper_Upload::__ext($module));

            # - Разрешаем загрузку методом flash
            if(isset($_POST['PHPSESSID']))
            {
                //'bin' => array('application/octet-stream')
                $settings['enabled_extension'][] = 'bin';
            }

            if (!is_array($settings['enabled_extension']))
            {
                die(json_encode(array("status"=>"error", "message"=>$this->view->translate("Настройки для загрузки файлов в данном модуле отсутствуют, код ошибки 765"))));
            }
            
            //_d($module);
            if($module == 'users' || $module == 'articles' /*|| $module == 'auth'*/) {
                if(!Model_Files::chechRatio($_FILES['file']['tmp_name'])) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузке допускаются только квадратные изображения"))));
                }
            }
            $M = new DR_Api_Tools_Photo;
            if($module == 'usbanner') {
                if(!$M->checkSizeGreatThan($_FILES['file']['tmp_name'], 260, 260)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузке допускаются изображения размером больше 260x260 и меньше 2000х2000"))));
                }
                if(!$M->checkSizeLessThan($_FILES['file']['tmp_name'], 2000, 2000)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузке допускаются изображения размером больше 260x260 и меньше 2000х2000"))));
                }
            }
            if($module == 'auth') {
                if(!$M->checkSizeGreatThan($_FILES['file']['tmp_name'], 260, 260)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузке допускаются изображения размером больше 260x260 и меньше 2000х2000"))));
                }
                if(!$M->checkSizeLessThan($_FILES['file']['tmp_name'], 2000, 2000)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузке допускаются изображения размером больше 260x260 и меньше 2000х2000"))));
                }
            }
            if($module == 'uswall') {
                if(!$M->checkSizeGreatThan($_FILES['file']['tmp_name'], 1400, 700)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузку допускаются изображения размером больше 1400x700"))));
                }
                
            }
            if($module == 'materials') {
                if(!$M->checkSizeGreatThan($_FILES['file']['tmp_name'], 840, 500)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузку допускаются изображения размером больше 840x500"))));
                }
                
            }
            /*if($module == 'usphoto') {
                if(!$M->checkSizeGreatThan($_FILES['file']['tmp_name'], 600, 600)) {
                    die(json_encode(array("status"=>"error", "message"=>$this->view->translate("К загрузку допускаются изображения размером больше 600x600"))));
                }
                
            }*/
            list($status, $filename, $fileinfo) = Model_Files::saveFile($settings, $module, $savePath);
            
            if ($status == "ok")
            {

                # - Превью для картинок
                $preview = '';
                $xsize = 0;
                $ysize = 0;
                if ($fileinfo['mime_type'] == "image")
                {
                    
                    if ($M->isValid($M->dir . $filename))
                    {
                        $data = getimagesize($M->dir . $filename);
                        $xsize = $data[0];
                        $ysize = $data[1];
                    	$preview = str_replace("." . $fileinfo['extension'], "", $filename);
                        $way = $M->dir . str_replace("." . $fileinfo['extension'], "", $filename);
                        if(!file_exists($way))
                            mkdir($way, 0755);
                        
                        if('banners' == $module) {
                            
                            $M->doPreview($M->dir . $filename, $way . "/prev560." . $fileinfo['extension'], 0, 560);
                            $M->doPreview($M->dir . $filename, $way . "/prev1500." . $fileinfo['extension'], 0, 1500);
                        } elseif('usmessage' == $module) {
                            $M->doPreview($M->dir . $filename, $way . "/prev180." . $fileinfo['extension'], 0, null, 180);
                        }
                        
                        $M->doPreview($M->dir . $filename, $way . "/prev100." . $fileinfo['extension'], 0, 100);
                        $M->doPreview($M->dir . $filename, $way . "/prev225." . $fileinfo['extension'], 0, 225);
                        $M->doPreview($M->dir . $filename, $way . "/prev260." . $fileinfo['extension'], 0, 260);
                        $M->doPreview($M->dir . $filename, $way . "/prev560." . $fileinfo['extension'], 0, 560);
                        $M->doPreview($M->dir . $filename, $way . "/prev840." . $fileinfo['extension'], 0, 840);
                        $preview .= "/prev560." . $fileinfo['extension'];
                        
                        
                        
                        
                    }
                }
                $model_files = api::getFiles();
                $file_id = $model_files->doSave(array(
                    "path" => $filename,
                    "name" => $fileinfo['filename'],
                    "ext" => $fileinfo['extension'],
                    "mime" => $fileinfo['mime_type'],
                    "users_id" => Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->id : 0));

                $values = array(
                    "status"=>"ok",
                    "file" => $filename,
                	"preview"=>$preview,
                    "id" => $file_id,
                    "name" => $fileinfo['filename'],
                    "ext" => $fileinfo['extension'],
                    "mime_type" => $fileinfo['mime_type'],
                    "xsize"=>$xsize,
                    "ysize"=>$ysize);
                die(json_encode($values));
            } else
            {
                die(json_encode(array("22","status"=>"error", "message"=>$filename)));
            }
        } else
        {
            die(json_encode(array("status"=>"error", "message"=>$this->view->translate("файл не выбран"))));
        }
    }

    /**
     *	-	Назначение: Режем фото
     *	-	Дата: 26.01.2011 10:59:55
     * @author celeron	 
     * @version 1.3
     */
    public function cropAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('ajax');

        if (strpos($_POST['i'], '?') !== false)
        {
            list($_POST['i'], $temp) = explode('?', $_POST['i']);
        }

        $destination = Model_Files::cropFile($_POST['i'], $_SERVER['DOCUMENT_ROOT'], (($_POST['w'] < 1) ? 1 : $_POST['w']), (($_POST['h'] < 1) ? 1 : $_POST['h']), $_POST['x'], $_POST['y']);

        # - Превью для картинок
        $M = new DR_Api_Tools_Photo;

        if ($M->isValid($M->dir . $destination))
        {
            $M->doPreview($M->dir . $destination, $M->dir . Model_Files::_getPreview($destination), 25);
        }

        die($destination . '?' . rand());
    }


    /**
     * Удвлить файл
     */
    public function deleteprevAction()
    {
        $mUsMeta = new Model_Files;
        $file = $mUsMeta->getSimpleData(array("primary_key" => $this->_getParam("id")));
        unlink($_SERVER['DOCUMENT_ROOT'] . $file->path);

        $file->delete();
        die(1);


        //$mUsMeta->doDelete($this->_getParam())

    }
}
