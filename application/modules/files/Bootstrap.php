<?php

class Files_Bootstrap extends Zend_Application_Module_Bootstrap {


   protected function _initAutoload()
   {
        $moduleLoader = new Zend_Application_Module_Autoloader(
                               array( 'namespace' => '',
                                        'basePath' => APPLICATION_PATH. '/modules/files/'));
	return  $moduleLoader;
   }
   
}