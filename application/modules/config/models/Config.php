<?php



/**
 * Модель обработки настроек
 */
class Model_Config extends DR_Models_AbstractTable {
    const TYPE_TEXT = 1;
    const TYPE_IMAGE = 2;
    
    
    public static function getSettings($name) {
        $model = new self;
        $temp = $model->_new()->where('t.name', $name)->row();
        
        if(count($temp)) {
            
            return $temp->value;
        } else {
            return false;
        }
    }
    
}




