<?php



/**
 * 
 */
class Model_Listvalues extends DR_Models_AbstractTable {
    
	
	const HASH_TAGS_LIST = 28;
    public function doSave($values, $id = null, $returnData = false) {
        if(is_null($id)) {
            $values['pos'] = $this->_new()->nextId();
        }
        return parent::doSave($values, $id, $returnData);
    }
  
    public function getNextHashTag($last_tag) {
    	$data = $this->_new(array('id', 'value'))->where('t.list_id', self::HASH_TAGS_LIST)->forSelect(array('id'=>'value'));
    	if(!count($data))    		
    		return array(null, null);
    	$keys = array_keys($data);
    	if(!is_null($last_tag)) {
    		foreach($data as $id=>$name) {
    			if($id > $last_tag)
    				return array($id, $name);
    		}	
    	}
    	return array($keys[0], $data[$keys[0]]);
    	
    	
    }
}




