<?php

/**
 * Модель для обратной связи
 */
class Model_Feedback extends DR_Models_AbstractTable {
    const TYPE_REMOVE_ACCOUNT = 1;
    const TYPE_UNSUBSCRIBE = 2;
}