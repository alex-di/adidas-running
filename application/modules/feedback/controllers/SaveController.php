<?php

/**
 * @author rizo
 * @version 1.0
 * @final
 */
class Feedback_saveController extends DR_Controllers_Site
{

    public function indexAction()
    {
        if ($this->_request->isPost())
        {
            $this->getValidator()->setElement('email')->email('Неверный e-mail')->recordExists('E-mail не найден');

            list($valid, $Data) = $this->isValid($_POST);
            $sripts = array();
            if ($valid)
            { 
                $M = api::getUsers();
                $user = $M($_POST['email'], 'email');
                $key = 'unsubscribe_email';
                if(Model_Feedback::TYPE_REMOVE_ACCOUNT == $_POST['type'])
                    $key = 'need_remove_account';
                $data = api::getMeta()->where('t.resource_id', $user->id)->where('t.modules_id', Model_Meta::USERS)->where('t.key', $key)->row();
                $metaId = null;
                if(count($data))
                    $metaId = $data->id;
                api::getMeta()->doSave(array('resource_id'=>$user->id, 'modules_id'=>Model_Meta::USERS, 'key'=>$key, 'value'=>1), $metaId);

                $sripts[] = "$.showInfo('" . $this->view->translate("Спасибо за Ваше обращение. Заявка будет расмотрена в кратчайшие строки. ") . "', '/');";

            } else {
                $sripts[] = "$.showInfo('Неверно заполнены поля')";
            }
            $this->Response($Data, $sripts);
        }
    }
}
