<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Votes_materialsController extends DR_Controllers_Votes
{

    public function indexAction()
    {
        $this->modules_id = Model_Votes::MATERIALS;
        parent::indexAction();
    }
}
