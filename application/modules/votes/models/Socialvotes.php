<?php

/**
 * 
 */
class Model_Socialvotes extends DR_Models_AbstractTable
{
    const MATERIALS = 1;
    const SOCIAL_TYPE_VK = 1;
    const SOCIAL_TYPE_FB = 2;
    const SOCIAL_TYPE_TW = 3;
    const SOCIAL_TYPE_GP = 4;
    
    public function doVotes($resource_id, $modules_id, $namespace, $counter) {
        $vote = $this->_new(array('id'))->where('modules_id', $modules_id)->where('resource_id', $resource_id)->where('social_type', $namespace)->row();
        $vote_id = count($vote) ? $vote->id : null;
        $this->doSave(array('modules_id' => $modules_id, 'resource_id' => $resource_id, 'social_type' => $namespace, 'counter' => $counter), $vote_id);
    }
    public function updateMaterialsRate($material_id) {
        $data = $this->_new(array('total'=>'sum(counter)'))->where('modules_id', self::MATERIALS)->where('resource_id', $material_id)->row();
        if(count($data)) {
            api::getMaterials()->doSave(array('rate' => $data->total), $material_id);
        }
    }
}