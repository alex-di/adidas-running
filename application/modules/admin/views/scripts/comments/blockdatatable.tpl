
<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr class="<?if(!$row['is_modern']):?>not_modern<?endif?>">
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
            
            <td><?=$row['id'] ?></td>
            <td id="comments_<?=$row['id'] ?>"><?=$row['text'] ?></td>
            <td><?=$row['username'] ?></td>
            <td><a target="_blank" href="<?echo $this->url(array('stitle'=>$row['article_stitle']), 'article_view')?>"><?=$row['article_name'] ?></a></td>
            <td><?=$row['date'] ?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <a title="Отмодерировать коментарий" class="btn btn-small" href="javascript:modernComments([<?=$row['id'] ?>])"><i class="icon-ok"></i></a>
                        <a title="Редактировать коментарий" class="btn btn-small" href="javascript:editComments(<?=$row['id'] ?>)"><i class="icon-pencil"></i></a>
                        <a title="Удалить коментарий" class="btn btn-small" href="javascript:deleteObject('<?= '/admin/' . $this->module_params['controller']. '/delete' ?>/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>