<?echo $this->partial('index.tpl', array('tables' => $this->tables, 'module_params'=>$this->module_params))?>

<div style="display:none;" id="dialog-edit-comments">
	<form id="mws-edit" class="mws-form" onsubmit="return saveComments(this);">
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Коментарий:</label>
				<div class="mws-form-item small">
					<textarea name="text" id="text_value" style="width:90%" cols="100%" rows="100%"></textarea>
					<div id="err_text"> </div>
				</div>
			</div>
			<input type="hidden" name="id" value="" id="lv_id" />
		</div>
	</form>
</div>
<script type="text/javascript">

       	$("#dialog-edit-comments").dialog({
    		autoOpen: false, 
    		title: "Редактирование коментария", 
    		modal: true, 
    		width: "640", 
    		buttons: [{
    				text: "Сохранить", 
    				click: function() {
    					$( this ).find('form').submit();
    				}}]
       	});
        function saveComments(form) {
            var text = $('#text_value').val();
            $('#err_text').text('').removeClass('mws-error');
            if('' == $.trim(text)) {
                $('#err_text').text('Заполните поле').addClass('mws-error');
            } else {
                $("#dialog-edit-comments").dialog('close');
                $.post('/admin/comments/processsave', $(form).serialize(), function(){
                    var comment_id = $('#lv_id').val();
                    $('#comments_'+comment_id).text($('#text_value').val()).parent().removeClass('not_modern');
                });
            }
            return false;
        }
        function editComments(comment_id) {
            $('#lv_id').val(comment_id);
            $('#text_value').val($('#comments_'+comment_id).text());
            $("#dialog-edit-comments").dialog('open');
        }
        function fastModernComments(){
            var elements = $("input.mass_checked:checked");
            var id = [];
            elements.each(function(){
                id.push($(this).val());
            });
            if(!id.length) {
                alert('Выберите коментарии');
            } else if(confirm('Отмодерировать выбраные коментарии?')) {
                modernComments(id);
            }
        }
        function modernComments(comments) {
            $.post('/admin/comments/processmodern', {id: comments}, function() {
                for(var i in comments) {
                    $('#comments_'+comments[i]).parent().removeClass('not_modern');
                }
            });
        }
</script>
<style>
    tr.not_modern td{
        font-weight: bold;
    }
</style>