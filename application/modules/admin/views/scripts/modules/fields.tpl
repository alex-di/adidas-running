<table>
    <tr>
        <th>
            имя поля
        </th>
        <th style="padding-left:20px;" >
            тайтл поля
        </th>
        <th >
            сортировка по полю
        </th>
        <th style="padding-left:20px;">
            фильтр
        </th>
    </tr>
    <? foreach($this->columns as $column): ?>
        <tr>
            <td >
                <? if(isset($this->params['fields'][$column])): ?>
                    <input checked="true" type="checkbox" name="params[fields][<?= $column ?>]" value="<?= $column ?>"/> <?= $column ?>
                <? else: ?>
                    <input type="checkbox" name="params[fields][<?= $column ?>]" value="<?= $column ?>"/> <?= $column ?>
                <? endif ?>
            </td>
            <td style="padding-left:20px;">
                <? if(isset($this->params['field_names'][$column])): ?>
                    <input type="text" name="params[field_names][<?= $column ?>]" value="<?= $this->params['field_names'][$column] ?>" />
                <? else: ?>
                    <input type="text" name="params[field_names][<?= $column ?>]" value="" />
                <? endif ?>
                
            </td>
            <td >
                <? if(isset($this->params['fields_sort'][$column])): ?>
                    <input checked="true" type="checkbox" name="params[fields_sort][<?= $column ?>]" value="<?= $column ?>"/>
                <? else: ?>
                    <input type="checkbox" name="params[fields_sort][<?= $column ?>]" value="<?= $column ?>"/>
                <? endif ?>
            </td>
            <td >
                <select name="params[field_filter][<?= $column ?>]">
                    <option value="0">Не выбрано</option>
                    <? foreach($this->filters as $key => $filter): ?>
                
                            <option value="<?= $key ?>"><?= $filter ?></option>
                        
                    <? endforeach ?>
                </select>
            </td>
        </tr>
    <? endforeach ?>
</table>
<script>
    
    <? foreach($this->params['field_filter'] as $name => $filter): ?>
        $("select[name = 'params[field_filter][<?= $name ?>]']").val('<?= $filter ?>');
    <? endforeach; ?>
</script>