<? 
    function splitData($data) {
        $data = explode(" ", $data);
        $res = array("class"=>"", "resource"=>"");
        $r = array();
        foreach($data as $val) {
            if(strpos($val, "admin_") === false && !empty($val)) {
                $res['class'] = $val;
            } else {
               $r[] = $val;
            }
        }
        $res['resource'] = implode(" ", $r);
        return $res;
    }
 ?>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="mws-i-24 i-file-cabinet">Редактирование админ меню</span>
    </div>
    <div class="mws-panel-body">
    	<form method="post"  class="mws-form" id="mws-validate">
                    <div id="elfinder" class="el-finder">
                        <div class="el-finder-toolbar">
                            <ul>
                                <li style="background-position: 3px -775px" onclick="addItem()" title="Добавить"></li>
                                <li style="background-position: 3px -330px" onclick="deleteItem()" title="Удалить"></li>
                                <li style="background-position: 3px -435px" onclick="editItem()" title="Редактировать"></li>
                                <li style="background-position: 3px -8px" onclick="moveDown()" title="Двигать вниз"></li>
                                <li style="background-position: 3px 8px" onclick="moveUp()" title="Двигать вверх"></li>
                            </ul>
                        </div>
                        <div class="el-finder-workzone">
                            <div class="el-finder-nav ui-resizable ui-resizable-autohide" style="height: 100%;width:100%;min-height: 300px;background: #999999;">
                                <? if(count($this->data)): //echo "<pre>";print_r($this->data);die;?>
                                    <ul class="el-finder-places">
                                        <? foreach($this->data as $data): ?>
                                            <li>
                                                <? $res = splitData($data['parent']->class); ?>
                                                <a class="item mws-i-24 <? echo $res['class'] ?>" style="background-position: 8px center;height: 24px;" href="javascript:void(0)" data-resource="<? echo $res['resource'] ?>"  data-id="<? echo $data['parent']->id ?>" data-parent="<? echo $data['parent']->parent_id ?>" data-title="<? echo $data['parent']->title ?>" data-action="<? echo $data['parent']->action ?>" data-class="<? echo $res['class'] ?>"><div></div><? echo $data['parent']->title ?> [<? echo $data['parent']->action == "" ? "Действие не задано" : $data['parent']->action?>]</a>
                                                <? if(count($data['child'])): ?>
                                                    <ul>
                                                        <? 
                                                            ksort($data['child']);
                                                            foreach($data['child'] as $row): ?>
                                                                <li>
                                                                    <? $res = splitData($row->child_class); ?>
                                                                    <a class="item mws-i-24 <? echo $res['class'] ?>" style="background-position: 8px center;height: 24px;" data-resource="<? echo $res['resource'] ?>"  data-id="<? echo $row->child_id ?>" data-parent="<? echo $data['parent']->id ?>" data-title="<? echo $row->child_title ?>" data-action="<? echo $row->child_action ?>" data-class="<? echo $res['class'] ?>"><div></div><? echo $row->child_title ?> [<? echo $row->child_action == "" ? "Действие не задано" : $row->child_action ?>]</a>
                                                                </li>
                                                        <? endforeach; ?>
                                                    </ul>
                                                <? endif; ?>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif; ?>
                                <div class="ui-resizable-handle ui-resizable-e" style="display: none;"></div>
                                <ul class="el-finder-tree"></ul>
                            </div>
                            <!--<div class="el-finder-cwd" unselectable="on" style="height: 300px;"></div>-->
                        </div>
                    </div>
        </form>
    </div>    	
</div>
<script type="text/javascript">
$(function(){
   	$("#dialog_content").dialog({
		autoOpen: false, 
		title: "Форма добавления", 
		modal: true, 
		width: "640", 
		buttons: [{
				text: "Сохранить", 
				click: function() {
				    $.post("/admin/config/savesidebar", $("#dialog_content form").serialize(), function(){
				       alert("Сохранено");
                       window.location.reload(); 
				    });
                    
				}}]
	});
	$("#icons").dialog({
		autoOpen: false, 
		title: "Иконки", 
		modal: false, 
		width: "1000" 
	});
    $("#resource").dialog({
		autoOpen: false, 
		title: "Ресурсы", 
		modal: false, 
		width: "640",
		buttons: [{
		text: "Выбрать", 
		click: function() {
                $("#resource").dialog("close");
                var resource = $(".resources").filter(".selected");
                var data = [];
                for(var i = 0; i < resource.length; i++) {
                    data.push(resource.eq(i).data("resource"));
                }
                $("#preview_resource").text(data.join(" "));
                $("input[name='resource']").val(data.join(" "));
		    }
		}]
    });
    $("#icons-24 li").click(function(){
        var clazz = $(this).attr('title').split(".")[2];
        $("#curent_icon").removeClass($("input[name='class']").val());
        $("#curent_icon").addClass(clazz);
        $("input[name='class']").val(clazz);
        $("#icons").dialog("close");
    })
    $(".item").click(function(){
        $(".item").removeClass("selected");
        $(this).addClass("selected");
    });
 });
 function addItem() {
    var isAdd = true;
    var parent = 0;
    if($(".item").filter(".selected").length) {
        if($(".item").filter(".selected").data("parent") != "0") {
            alert("Невозможно добавить");
            isAdd = false;
        } else {
            parent = $(".item").filter(".selected").data("id");
        }
    }
    if(isAdd) {
        setDataToForm('i-pacman', '', '', parent, 0, '');
        $("#dialog_content").dialog("open");
    }  
 }
 function editItem(){
    if($(".item").filter(".selected").length) {
        var element = $(".item").filter(".selected");
        setDataToForm(element.data("class"), element.data("title"), element.data("action"), element.data("parent"),  element.data("id"), element.data("resource"));
        $("#dialog_content").dialog("open");
    } else{
        alert("Что бы редактировать - выберите что то");
    }
 }
 function deleteItem() {
    if($(".item").filter(".selected").length) {
        if(confirm("Вы уверены?")) {
    	    $.post("/admin/config/processdeleteitemmenu", {id:$(".item").filter(".selected").data("id")}, function(){
               window.location.reload(); 
    	    });
        }
    } else{
        alert("Что бы удалить - выберите что то");
    }
 }
 function moveDown(){
    if($(".item").filter(".selected").length) {
        if($(".item").filter(".selected").parent().next().length) {
            move($(".item").filter(".selected").data("id"), $(".item").filter(".selected").parent().next().find("> a").data("id"));   
        }
    } else{
        alert("Что бы двигать - выберите что то");
    }
 }
 function moveUp(){
    if($(".item").filter(".selected").length) {
        if($(".item").filter(".selected").parent().prev().length) {
            move($(".item").filter(".selected").data("id"), $(".item").filter(".selected").parent().prev().find("> a").data("id"));   
        }
    } else{
        alert("Что бы двигать - выберите что то");
    }
 }
 function move(first_id, two_id) {
    $.post("/admin/config/processmovemenuitem", {first_id:first_id, two_id:two_id}, function(){
        window.location.reload();
    });
 }
 function setDataToForm(clazz, title, action, parent, id, resource){
        
        $("#curent_icon").removeClass($("input[name='class']").val());
        $("#curent_icon").addClass(clazz);
        $("input[name='class']").val(clazz);
        $("input[name='title']").val(title);
        $("input[name='action']").val(action);
        $("input[name='parent_id']").val(parent);
        $("input[name='id']").val(id);
        $("input[name='resource']").val(resource);
        $(".el-finder-places-root").removeClass("selected");
        $("#preview_resource").text(resource);
        var resource = resource.split(" ");
        for(var i = 0; i < resource.length; i++) {
            $("#resource_"+resource[i]).click();
        }
 }
</script>
<style>
    .icon_preview{
        height: 40px;
        width: 40px;
        background-color: #434343;
        background-position: center center;
        border-radius: 4px 4px 4px 4px;
        display: block;
        float: left;
        margin: 4px;
    }
</style>
<div id="dialog_content" style="display: none;">
    <form id="mws-validate" class="mws-form">
        <div class="mws-form-inline">
            <div class="mws-form-row">
                <label>Название</label>
                <div class="mws-form-item large">
                    <input type="text" name="title" class="mws-textinput required" />
                </div>
            </div>
            <div class="mws-form-row">
                <label>Действие</label>
                <div class="mws-form-item large">
                    <input type="text" name="action" class="mws-textinput required" />
                </div>
            </div>
            <div class="mws-form-row">
                <label>Ассоциации с ресурсами</label>
                <div class="mws-form-item large">
                    <input type="button" onclick='$("#resource").dialog("open")' class="mws-button green" value="Выбрать"/>
                    <b id="preview_resource"></b>
                </div>
            </div>
            <div class="mws-form-row">
                <label><input type="button" onclick='$("#icons").dialog("open")' class="mws-button green" value="Сменить иконку"/></label>
                <div class="mws-form-item large">
              		<ul class="clearfix">
                        <li id="curent_icon" class="mws-i-24 i-pacman icon_preview"></li>
                    </ul>
                </div>
            </div>

            
            <input type="hidden" value="i-pacman" name="class"/>
            <input type="hidden" value="" name="resource"/>
            <input type="hidden" value="0" name="id"/>
            <input type="hidden" value="0" name="parent_id"/>
        </div>
    </form>
</div>
<div id="resource" style="display: none;">
    <form id="mws-validate" class="mws-form">
        <div class="mws-form-inline">
                    <div id="elfinder" class="el-finder">
                        <div class="el-finder-toolbar">
                            <ul></ul>
                        </div>
                        <div class="el-finder-workzone">
                            <div class="el-finder-nav ui-resizable ui-resizable-autohide" style="height: 100%;width:100%;min-height: 300px;height:400px">
                                <? if(count($this->resource)): //echo "<pre>";print_r($this->data);die;?>
                                    <ul class="el-finder-places">
                                        <li>
                                            <a style="background-position: 15px -30px;" class="el-finder-places-root modules" href="javascript:void(0)"><div></div>admin</a>
                                            <? foreach($this->resource as $controller=>$data): ?>
                                                <ul>
                                                    <li>
                                                        <a style="background-position: 15px -260px;" class="el-finder-places-root controllers" href="javascript:void(0)"><div></div><? echo $controller ?></a>
                                                        <ul>
                                                         <?foreach($data as $action): ?>
                                                            <li>
                                                                <? $class = "admin_".$controller."_".$action; ?>
                                                                <a id="resource_<? echo $class ?>" style="background-position: 15px -55px;" href="javascript:void(0)" class="el-finder-places-root resources" data-resource="<? echo $class?>"><div></div><? echo $action ?></a>
                                                            </li>
                                                        <? endforeach; ?>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            <? endforeach; ?>
                                        </li>
                                    </ul>
                                <? endif; ?>
                                <div class="ui-resizable-handle ui-resizable-e" style="display: none;"></div>
                                <ul class="el-finder-tree"></ul>
                            </div>
                            <!--<div class="el-finder-cwd" unselectable="on" style="height: 300px;"></div>-->
                        </div>
                    </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $(".modules").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                $(this).parent().find(".controllers, .resources").removeClass("selected");
           } else {
                $(this).addClass("selected");
                $(this).parent().find(".controllers, .resources").addClass("selected");
           } 
        });
        $(".controllers").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                $(this).parent().find(".resources").removeClass("selected");
           } else {
                $(this).addClass("selected");
                $(this).parent().find(".resources").addClass("selected");
           }
           if($(this).parent().parents("li").find(".controllers").length == $(this).parent().parents("li").find(".controllers").filter(".selected").length) {
                $(this).parent().parents("li").find(".modules").addClass("selected");
           } else {
                $(this).parent().parents("li").find(".modules").removeClass("selected");
           }
        });
        
        $(".resources").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
           } else {
                $(this).addClass("selected");
           }
           
           var parent = $(this).parent().parent().parent();
           if(parent.find(".resources").length == parent.find(".resources").filter(".selected").length) {
                parent.find(".controllers").addClass("selected");
           } else {
                parent.find(".controllers").removeClass("selected");
           }
           parent = parent.parents("li");
           if(parent.find(".controllers").length == parent.find(".controllers").filter(".selected").length) {
                parent.find(".modules").addClass("selected");
           } else {
                parent.find(".modules").removeClass("selected");
           } 
        });
    });
</script>
<div id="icons" style="display: none;">
	<div class="mws-dialog-inner">
		<ul class="clearfix" id="icons-24" style="overflow: auto;height:500px">
            <li title=".mws-i-24 .i-abacus" class="mws-i-24 i-abacus"></li>
            <li title=".mws-i-24 .i-acces-denied-sign" class="mws-i-24 i-acces-denied-sign"></li>
            <li title=".mws-i-24 .i-address-book-1" class="mws-i-24 i-address-book-1"></li>
            <li title=".mws-i-24 .i-address-book-2" class="mws-i-24 i-address-book-2"></li>
            <li title=".mws-i-24 .i-address-book-3" class="mws-i-24 i-address-book-3"></li>
            <li title=".mws-i-24 .i-address-book-4" class="mws-i-24 i-address-book-4"></li>
            <li title=".mws-i-24 .i-address-book-5" class="mws-i-24 i-address-book-5"></li>
            <li title=".mws-i-24 .i-address-book" class="mws-i-24 i-address-book"></li>
            <li title=".mws-i-24 .i-adidas-sport-shirt" class="mws-i-24 i-adidas-sport-shirt"></li>
            <li title=".mws-i-24 .i-admin-user-2" class="mws-i-24 i-admin-user-2"></li>
            <li title=".mws-i-24 .i-admin-user" class="mws-i-24 i-admin-user"></li>
            <li title=".mws-i-24 .i-airplane" class="mws-i-24 i-airplane"></li>
            <li title=".mws-i-24 .i-alarm-bell-2" class="mws-i-24 i-alarm-bell-2"></li>
            <li title=".mws-i-24 .i-alarm-bell" class="mws-i-24 i-alarm-bell"></li>
            <li title=".mws-i-24 .i-alarm-clock" class="mws-i-24 i-alarm-clock"></li>
            <li title=".mws-i-24 .i-alert-2" class="mws-i-24 i-alert-2"></li>
            <li title=".mws-i-24 .i-alert" class="mws-i-24 i-alert"></li>
            <li title=".mws-i-24 .i-american-express" class="mws-i-24 i-american-express"></li>
            <li title=".mws-i-24 .i-android" class="mws-i-24 i-android"></li>
            <li title=".mws-i-24 .i-apartment-building" class="mws-i-24 i-apartment-building"></li>
            <li title=".mws-i-24 .i-applications" class="mws-i-24 i-applications"></li>
            <li title=".mws-i-24 .i-archive" class="mws-i-24 i-archive"></li>
            <li title=".mws-i-24 .i-arrow-down" class="mws-i-24 i-arrow-down"></li>
            <li title=".mws-i-24 .i-arrow-left" class="mws-i-24 i-arrow-left"></li>
            <li title=".mws-i-24 .i-arrow-right" class="mws-i-24 i-arrow-right"></li>
            <li title=".mws-i-24 .i-arrow-up" class="mws-i-24 i-arrow-up"></li>
            <li title=".mws-i-24 .i-bag-2" class="mws-i-24 i-bag-2"></li>
            <li title=".mws-i-24 .i-bag" class="mws-i-24 i-bag"></li>
            <li title=".mws-i-24 .i-balloons" class="mws-i-24 i-balloons"></li>
            <li title=".mws-i-24 .i-bandaid" class="mws-i-24 i-bandaid"></li>
            <li title=".mws-i-24 .i-bar-graph" class="mws-i-24 i-bar-graph"></li>
            <li title=".mws-i-24 .i-battery-almost-empty" class="mws-i-24 i-battery-almost-empty"></li>
            <li title=".mws-i-24 .i-battery-almost-full" class="mws-i-24 i-battery-almost-full"></li>
            <li title=".mws-i-24 .i-battery-empty" class="mws-i-24 i-battery-empty"></li>
            <li title=".mws-i-24 .i-battery-full" class="mws-i-24 i-battery-full"></li>
            <li title=".mws-i-24 .i-battery" class="mws-i-24 i-battery"></li>
            <li title=".mws-i-24 .i-bended-arrow-down" class="mws-i-24 i-bended-arrow-down"></li>
            <li title=".mws-i-24 .i-bended-arrow-left" class="mws-i-24 i-bended-arrow-left"></li>
            <li title=".mws-i-24 .i-bended-arrow-right" class="mws-i-24 i-bended-arrow-right"></li>
            <li title=".mws-i-24 .i-bended-arrow-up" class="mws-i-24 i-bended-arrow-up"></li>
            <li title=".mws-i-24 .i-big-brush" class="mws-i-24 i-big-brush"></li>
            <li title=".mws-i-24 .i-bills-stack" class="mws-i-24 i-bills-stack"></li>
            <li title=".mws-i-24 .i-blackberry-2" class="mws-i-24 i-blackberry-2"></li>
            <li title=".mws-i-24 .i-blackberry-3" class="mws-i-24 i-blackberry-3"></li>
            <li title=".mws-i-24 .i-blackberry" class="mws-i-24 i-blackberry"></li>
            <li title=".mws-i-24 .i-blocks-images" class="mws-i-24 i-blocks-images"></li>
            <li title=".mws-i-24 .i-blu-ray" class="mws-i-24 i-blu-ray"></li>
            <li title=".mws-i-24 .i-bluetooth-2" class="mws-i-24 i-bluetooth-2"></li>
            <li title=".mws-i-24 .i-bluetooth" class="mws-i-24 i-bluetooth"></li>
            <li title=".mws-i-24 .i-book-bookmark" class="mws-i-24 i-book-bookmark"></li>
            <li title=".mws-i-24 .i-book-large" class="mws-i-24 i-book-large"></li>
            <li title=".mws-i-24 .i-book" class="mws-i-24 i-book"></li>
            <li title=".mws-i-24 .i-bookmark" class="mws-i-24 i-bookmark"></li>
            <li title=".mws-i-24 .i-books-2" class="mws-i-24 i-books-2"></li>
            <li title=".mws-i-24 .i-books" class="mws-i-24 i-books"></li>
            <li title=".mws-i-24 .i-box-incoming-2" class="mws-i-24 i-box-incoming-2"></li>
            <li title=".mws-i-24 .i-box-incoming" class="mws-i-24 i-box-incoming"></li>
            <li title=".mws-i-24 .i-box-outgoing-2" class="mws-i-24 i-box-outgoing-2"></li>
            <li title=".mws-i-24 .i-box-outgoing" class="mws-i-24 i-box-outgoing"></li>
            <li title=".mws-i-24 .i-bulls-eye" class="mws-i-24 i-bulls-eye"></li>
            <li title=".mws-i-24 .i-calculator" class="mws-i-24 i-calculator"></li>
            <li title=".mws-i-24 .i-calendar-today" class="mws-i-24 i-calendar-today"></li>
            <li title=".mws-i-24 .i-camera-2" class="mws-i-24 i-camera-2"></li>
            <li title=".mws-i-24 .i-camera" class="mws-i-24 i-camera"></li>
            <li title=".mws-i-24 .i-candy-cane" class="mws-i-24 i-candy-cane"></li>
            <li title=".mws-i-24 .i-car" class="mws-i-24 i-car"></li>
            <li title=".mws-i-24 .i-cash-register" class="mws-i-24 i-cash-register"></li>
            <li title=".mws-i-24 .i-cassette" class="mws-i-24 i-cassette"></li>
            <li title=".mws-i-24 .i-cat" class="mws-i-24 i-cat"></li>
            <li title=".mws-i-24 .i-cd" class="mws-i-24 i-cd"></li>
            <li title=".mws-i-24 .i-cell-phone-reception" class="mws-i-24 i-cell-phone-reception"></li>
            <li title=".mws-i-24 .i-chair" class="mws-i-24 i-chair"></li>
            <li title=".mws-i-24 .i-chart-2" class="mws-i-24 i-chart-2"></li>
            <li title=".mws-i-24 .i-chart-3" class="mws-i-24 i-chart-3"></li>
            <li title=".mws-i-24 .i-chart-4" class="mws-i-24 i-chart-4"></li>
            <li title=".mws-i-24 .i-chart-5" class="mws-i-24 i-chart-5"></li>
            <li title=".mws-i-24 .i-chart-6" class="mws-i-24 i-chart-6"></li>
            <li title=".mws-i-24 .i-chart-7" class="mws-i-24 i-chart-7"></li>
            <li title=".mws-i-24 .i-chart-8" class="mws-i-24 i-chart-8"></li>
            <li title=".mws-i-24 .i-chart" class="mws-i-24 i-chart"></li>
            <li title=".mws-i-24 .i-check" class="mws-i-24 i-check"></li>
            <li title=".mws-i-24 .i-chemical" class="mws-i-24 i-chemical"></li>
            <li title=".mws-i-24 .i-chrome" class="mws-i-24 i-chrome"></li>
            <li title=".mws-i-24 .i-clapboard" class="mws-i-24 i-clapboard"></li>
            <li title=".mws-i-24 .i-clipboard-1" class="mws-i-24 i-clipboard-1"></li>
            <li title=".mws-i-24 .i-clipboard" class="mws-i-24 i-clipboard"></li>
            <li title=".mws-i-24 .i-clock-1" class="mws-i-24 i-clock-1"></li>
            <li title=".mws-i-24 .i-clock-2" class="mws-i-24 i-clock-2"></li>
            <li title=".mws-i-24 .i-clock" class="mws-i-24 i-clock"></li>
            <li title=".mws-i-24 .i-cloud-download" class="mws-i-24 i-cloud-download"></li>
            <li title=".mws-i-24 .i-cloud-lightning-rain" class="mws-i-24 i-cloud-lightning-rain"></li>
            <li title=".mws-i-24 .i-cloud-lightning" class="mws-i-24 i-cloud-lightning"></li>
            <li title=".mws-i-24 .i-cloud-rain-shining-sun" class="mws-i-24 i-cloud-rain-shining-sun"></li>
            <li title=".mws-i-24 .i-cloud-rain-sun" class="mws-i-24 i-cloud-rain-sun"></li>
            <li title=".mws-i-24 .i-cloud-upload" class="mws-i-24 i-cloud-upload"></li>
            <li title=".mws-i-24 .i-cloud" class="mws-i-24 i-cloud"></li>
            <li title=".mws-i-24 .i-cog-2" class="mws-i-24 i-cog-2"></li>
            <li title=".mws-i-24 .i-cog-3" class="mws-i-24 i-cog-3"></li>
            <li title=".mws-i-24 .i-cog-4" class="mws-i-24 i-cog-4"></li>
            <li title=".mws-i-24 .i-cog-5" class="mws-i-24 i-cog-5"></li>
            <li title=".mws-i-24 .i-cog" class="mws-i-24 i-cog"></li>
            <li title=".mws-i-24 .i-companies" class="mws-i-24 i-companies"></li>
            <li title=".mws-i-24 .i-computer-imac" class="mws-i-24 i-computer-imac"></li>
            <li title=".mws-i-24 .i-coverflow" class="mws-i-24 i-coverflow"></li>
            <li title=".mws-i-24 .i-create" class="mws-i-24 i-create"></li>
            <li title=".mws-i-24 .i-creditcard" class="mws-i-24 i-creditcard"></li>
            <li title=".mws-i-24 .i-cross" class="mws-i-24 i-cross"></li>
            <li title=".mws-i-24 .i-crosshair" class="mws-i-24 i-crosshair"></li>
            <li title=".mws-i-24 .i-cup" class="mws-i-24 i-cup"></li>
            <li title=".mws-i-24 .i-cut-scissors" class="mws-i-24 i-cut-scissors"></li>
            <li title=".mws-i-24 .i-cut" class="mws-i-24 i-cut"></li>
            <li title=".mws-i-24 .i-day-calendar" class="mws-i-24 i-day-calendar"></li>
            <li title=".mws-i-24 .i-delicious" class="mws-i-24 i-delicious"></li>
            <li title=".mws-i-24 .i-digg-2" class="mws-i-24 i-digg-2"></li>
            <li title=".mws-i-24 .i-digg-3" class="mws-i-24 i-digg-3"></li>
            <li title=".mws-i-24 .i-digg-4" class="mws-i-24 i-digg-4"></li>
            <li title=".mws-i-24 .i-digg" class="mws-i-24 i-digg"></li>
            <li title=".mws-i-24 .i-document-1" class="mws-i-24 i-document-1"></li>
            <li title=".mws-i-24 .i-document" class="mws-i-24 i-document"></li>
            <li title=".mws-i-24 .i-documents-1" class="mws-i-24 i-documents-1"></li>
            <li title=".mws-i-24 .i-documents" class="mws-i-24 i-documents"></li>
            <li title=".mws-i-24 .i-dog-tag" class="mws-i-24 i-dog-tag"></li>
            <li title=".mws-i-24 .i-dog-tags" class="mws-i-24 i-dog-tags"></li>
            <li title=".mws-i-24 .i-download-to-computer" class="mws-i-24 i-download-to-computer"></li>
            <li title=".mws-i-24 .i-download" class="mws-i-24 i-download"></li>
            <li title=".mws-i-24 .i-dress" class="mws-i-24 i-dress"></li>
            <li title=".mws-i-24 .i-dribbble-2" class="mws-i-24 i-dribbble-2"></li>
            <li title=".mws-i-24 .i-dribbble-3" class="mws-i-24 i-dribbble-3"></li>
            <li title=".mws-i-24 .i-dribbble-4" class="mws-i-24 i-dribbble-4"></li>
            <li title=".mws-i-24 .i-dribbble" class="mws-i-24 i-dribbble"></li>
            <li title=".mws-i-24 .i-dropbox" class="mws-i-24 i-dropbox"></li>
            <li title=".mws-i-24 .i-drupal" class="mws-i-24 i-drupal"></li>
            <li title=".mws-i-24 .i-dvd" class="mws-i-24 i-dvd"></li>
            <li title=".mws-i-24 .i-eject" class="mws-i-24 i-eject"></li>
            <li title=".mws-i-24 .i-electricity-input-2" class="mws-i-24 i-electricity-input-2"></li>
            <li title=".mws-i-24 .i-electricity-input-plug" class="mws-i-24 i-electricity-input-plug"></li>
            <li title=".mws-i-24 .i-electricity-input" class="mws-i-24 i-electricity-input"></li>
            <li title=".mws-i-24 .i-electricity-plug" class="mws-i-24 i-electricity-plug"></li>
            <li title=".mws-i-24 .i-excel-document" class="mws-i-24 i-excel-document"></li>
            <li title=".mws-i-24 .i-excel-documents-1" class="mws-i-24 i-excel-documents-1"></li>
            <li title=".mws-i-24 .i-excel-documents" class="mws-i-24 i-excel-documents"></li>
            <li title=".mws-i-24 .i-exit" class="mws-i-24 i-exit"></li>
            <li title=".mws-i-24 .i-expose" class="mws-i-24 i-expose"></li>
            <li title=".mws-i-24 .i-expression-engine" class="mws-i-24 i-expression-engine"></li>
            <li title=".mws-i-24 .i-eyedropper" class="mws-i-24 i-eyedropper"></li>
            <li title=".mws-i-24 .i-facebook-1" class="mws-i-24 i-facebook-1"></li>
            <li title=".mws-i-24 .i-facebook-like" class="mws-i-24 i-facebook-like"></li>
            <li title=".mws-i-24 .i-facebook" class="mws-i-24 i-facebook"></li>
            <li title=".mws-i-24 .i-favorite" class="mws-i-24 i-favorite"></li>
            <li title=".mws-i-24 .i-female-contour" class="mws-i-24 i-female-contour"></li>
            <li title=".mws-i-24 .i-female-symbol" class="mws-i-24 i-female-symbol"></li>
            <li title=".mws-i-24 .i-file-cabinet" class="mws-i-24 i-file-cabinet"></li>
            <li title=".mws-i-24 .i-film-2" class="mws-i-24 i-film-2"></li>
            <li title=".mws-i-24 .i-film-camera-2" class="mws-i-24 i-film-camera-2"></li>
            <li title=".mws-i-24 .i-film-camera" class="mws-i-24 i-film-camera"></li>
            <li title=".mws-i-24 .i-film-strip" class="mws-i-24 i-film-strip"></li>
            <li title=".mws-i-24 .i-film" class="mws-i-24 i-film"></li>
            <li title=".mws-i-24 .i-finish-banner" class="mws-i-24 i-finish-banner"></li>
            <li title=".mws-i-24 .i-finish-flag" class="mws-i-24 i-finish-flag"></li>
            <li title=".mws-i-24 .i-firefox" class="mws-i-24 i-firefox"></li>
            <li title=".mws-i-24 .i-flag-2" class="mws-i-24 i-flag-2"></li>
            <li title=".mws-i-24 .i-flag" class="mws-i-24 i-flag"></li>
            <li title=".mws-i-24 .i-flatscreen" class="mws-i-24 i-flatscreen"></li>
            <li title=".mws-i-24 .i-flip-clock" class="mws-i-24 i-flip-clock"></li>
            <li title=".mws-i-24 .i-folder-1" class="mws-i-24 i-folder-1"></li>
            <li title=".mws-i-24 .i-folder" class="mws-i-24 i-folder"></li>
            <li title=".mws-i-24 .i-footprint" class="mws-i-24 i-footprint"></li>
            <li title=".mws-i-24 .i-footprints" class="mws-i-24 i-footprints"></li>
            <li title=".mws-i-24 .i-fountain-pen" class="mws-i-24 i-fountain-pen"></li>
            <li title=".mws-i-24 .i-frames" class="mws-i-24 i-frames"></li>
            <li title=".mws-i-24 .i-g-key" class="mws-i-24 i-g-key"></li>
            <li title=".mws-i-24 .i-glass" class="mws-i-24 i-glass"></li>
            <li title=".mws-i-24 .i-globe-2" class="mws-i-24 i-globe-2"></li>
            <li title=".mws-i-24 .i-globe" class="mws-i-24 i-globe"></li>
            <li title=".mws-i-24 .i-go-back-from-screen-1" class="mws-i-24 i-go-back-from-screen-1"></li>
            <li title=".mws-i-24 .i-go-back-from-screen" class="mws-i-24 i-go-back-from-screen"></li>
            <li title=".mws-i-24 .i-go-full-screen" class="mws-i-24 i-go-full-screen"></li>
            <li title=".mws-i-24 .i-google-buzz" class="mws-i-24 i-google-buzz"></li>
            <li title=".mws-i-24 .i-google-maps" class="mws-i-24 i-google-maps"></li>
            <li title=".mws-i-24 .i-graph-2" class="mws-i-24 i-graph-2"></li>
            <li title=".mws-i-24 .i-graph" class="mws-i-24 i-graph"></li>
            <li title=".mws-i-24 .i-group-2" class="mws-i-24 i-group-2"></li>
            <li title=".mws-i-24 .i-group" class="mws-i-24 i-group"></li>
            <li title=".mws-i-24 .i-halloween-pumpkin" class="mws-i-24 i-halloween-pumpkin"></li>
            <li title=".mws-i-24 .i-hard-disk" class="mws-i-24 i-hard-disk"></li>
            <li title=".mws-i-24 .i-hd-2" class="mws-i-24 i-hd-2"></li>
            <li title=".mws-i-24 .i-hd-3" class="mws-i-24 i-hd-3"></li>
            <li title=".mws-i-24 .i-hd" class="mws-i-24 i-hd"></li>
            <li title=".mws-i-24 .i-headphones" class="mws-i-24 i-headphones"></li>
            <li title=".mws-i-24 .i-heart-favorite" class="mws-i-24 i-heart-favorite"></li>
            <li title=".mws-i-24 .i-help" class="mws-i-24 i-help"></li>
            <li title=".mws-i-24 .i-home-1" class="mws-i-24 i-home-1"></li>
            <li title=".mws-i-24 .i-home-2" class="mws-i-24 i-home-2"></li>
            <li title=".mws-i-24 .i-home" class="mws-i-24 i-home"></li>
            <li title=".mws-i-24 .i-hour-glass" class="mws-i-24 i-hour-glass"></li>
            <li title=".mws-i-24 .i-ice-cream-2" class="mws-i-24 i-ice-cream-2"></li>
            <li title=".mws-i-24 .i-ice-cream" class="mws-i-24 i-ice-cream"></li>
            <li title=".mws-i-24 .i-ichat" class="mws-i-24 i-ichat"></li>
            <li title=".mws-i-24 .i-ideal" class="mws-i-24 i-ideal"></li>
            <li title=".mws-i-24 .i-image-2" class="mws-i-24 i-image-2"></li>
            <li title=".mws-i-24 .i-image" class="mws-i-24 i-image"></li>
            <li title=".mws-i-24 .i-images-2" class="mws-i-24 i-images-2"></li>
            <li title=".mws-i-24 .i-images" class="mws-i-24 i-images"></li>
            <li title=".mws-i-24 .i-inbox" class="mws-i-24 i-inbox"></li>
            <li title=".mws-i-24 .i-info-about" class="mws-i-24 i-info-about"></li>
            <li title=".mws-i-24 .i-ipad-2" class="mws-i-24 i-ipad-2"></li>
            <li title=".mws-i-24 .i-ipad" class="mws-i-24 i-ipad"></li>
            <li title=".mws-i-24 .i-iphone-3g" class="mws-i-24 i-iphone-3g"></li>
            <li title=".mws-i-24 .i-iphone-4-2" class="mws-i-24 i-iphone-4-2"></li>
            <li title=".mws-i-24 .i-iphone-4" class="mws-i-24 i-iphone-4"></li>
            <li title=".mws-i-24 .i-ipod-classic" class="mws-i-24 i-ipod-classic"></li>
            <li title=".mws-i-24 .i-ipod-nano-2" class="mws-i-24 i-ipod-nano-2"></li>
            <li title=".mws-i-24 .i-ipod-nano" class="mws-i-24 i-ipod-nano"></li>
            <li title=".mws-i-24 .i-ipod-shuffle" class="mws-i-24 i-ipod-shuffle"></li>
            <li title=".mws-i-24 .i-joomla" class="mws-i-24 i-joomla"></li>
            <li title=".mws-i-24 .i-key-2" class="mws-i-24 i-key-2"></li>
            <li title=".mws-i-24 .i-key" class="mws-i-24 i-key"></li>
            <li title=".mws-i-24 .i-keyboard" class="mws-i-24 i-keyboard"></li>
            <li title=".mws-i-24 .i-ladys-purse" class="mws-i-24 i-ladys-purse"></li>
            <li title=".mws-i-24 .i-lamp" class="mws-i-24 i-lamp"></li>
            <li title=".mws-i-24 .i-laptop" class="mws-i-24 i-laptop"></li>
            <li title=".mws-i-24 .i-lastfm-2" class="mws-i-24 i-lastfm-2"></li>
            <li title=".mws-i-24 .i-lastfm" class="mws-i-24 i-lastfm"></li>
            <li title=".mws-i-24 .i-leaf" class="mws-i-24 i-leaf"></li>
            <li title=".mws-i-24 .i-lemonade-stand-2" class="mws-i-24 i-lemonade-stand-2"></li>
            <li title=".mws-i-24 .i-lemonade-stand" class="mws-i-24 i-lemonade-stand"></li>
            <li title=".mws-i-24 .i-light-bulb" class="mws-i-24 i-light-bulb"></li>
            <li title=".mws-i-24 .i-link-2" class="mws-i-24 i-link-2"></li>
            <li title=".mws-i-24 .i-link" class="mws-i-24 i-link"></li>
            <li title=".mws-i-24 .i-linux" class="mws-i-24 i-linux"></li>
            <li title=".mws-i-24 .i-list-image" class="mws-i-24 i-list-image"></li>
            <li title=".mws-i-24 .i-list-images" class="mws-i-24 i-list-images"></li>
            <li title=".mws-i-24 .i-list" class="mws-i-24 i-list"></li>
            <li title=".mws-i-24 .i-loading-bar" class="mws-i-24 i-loading-bar"></li>
            <li title=".mws-i-24 .i-lock-locked" class="mws-i-24 i-lock-locked"></li>
            <li title=".mws-i-24 .i-locked-2" class="mws-i-24 i-locked-2"></li>
            <li title=".mws-i-24 .i-locked-folder" class="mws-i-24 i-locked-folder"></li>
            <li title=".mws-i-24 .i-locked" class="mws-i-24 i-locked"></li>
            <li title=".mws-i-24 .i-looking-glass" class="mws-i-24 i-looking-glass"></li>
            <li title=".mws-i-24 .i-macos" class="mws-i-24 i-macos"></li>
            <li title=".mws-i-24 .i-magic-mouse" class="mws-i-24 i-magic-mouse"></li>
            <li title=".mws-i-24 .i-magnifying-glass-2" class="mws-i-24 i-magnifying-glass-2"></li>
            <li title=".mws-i-24 .i-magnifying-glass" class="mws-i-24 i-magnifying-glass"></li>
            <li title=".mws-i-24 .i-mail-2" class="mws-i-24 i-mail-2"></li>
            <li title=".mws-i-24 .i-mail" class="mws-i-24 i-mail"></li>
            <li title=".mws-i-24 .i-male-contour" class="mws-i-24 i-male-contour"></li>
            <li title=".mws-i-24 .i-male-symbol" class="mws-i-24 i-male-symbol"></li>
            <li title=".mws-i-24 .i-map" class="mws-i-24 i-map"></li>
            <li title=".mws-i-24 .i-marker" class="mws-i-24 i-marker"></li>
            <li title=".mws-i-24 .i-mastercard" class="mws-i-24 i-mastercard"></li>
            <li title=".mws-i-24 .i-medical-case" class="mws-i-24 i-medical-case"></li>
            <li title=".mws-i-24 .i-megaphone" class="mws-i-24 i-megaphone"></li>
            <li title=".mws-i-24 .i-message" class="mws-i-24 i-message"></li>
            <li title=".mws-i-24 .i-mickey" class="mws-i-24 i-mickey"></li>
            <li title=".mws-i-24 .i-microphone" class="mws-i-24 i-microphone"></li>
            <li title=".mws-i-24 .i-mighty-mouse" class="mws-i-24 i-mighty-mouse"></li>
            <li title=".mws-i-24 .i-minus" class="mws-i-24 i-minus"></li>
            <li title=".mws-i-24 .i-mobile-phone-2" class="mws-i-24 i-mobile-phone-2"></li>
            <li title=".mws-i-24 .i-mobile-phone" class="mws-i-24 i-mobile-phone"></li>
            <li title=".mws-i-24 .i-mobypicture" class="mws-i-24 i-mobypicture"></li>
            <li title=".mws-i-24 .i-money-2" class="mws-i-24 i-money-2"></li>
            <li title=".mws-i-24 .i-money" class="mws-i-24 i-money"></li>
            <li title=".mws-i-24 .i-monitor" class="mws-i-24 i-monitor"></li>
            <li title=".mws-i-24 .i-month-calendar" class="mws-i-24 i-month-calendar"></li>
            <li title=".mws-i-24 .i-mouse-" class="mws-i-24 i-mouse-"></li>
            <li title=".mws-i-24 .i-mouse-2" class="mws-i-24 i-mouse-2"></li>
            <li title=".mws-i-24 .i-mouseires" class="mws-i-24 i-mouseires"></li>
            <li title=".mws-i-24 .i-multiple-users" class="mws-i-24 i-multiple-users"></li>
            <li title=".mws-i-24 .i-music-folder" class="mws-i-24 i-music-folder"></li>
            <li title=".mws-i-24 .i-music" class="mws-i-24 i-music"></li>
            <li title=".mws-i-24 .i-musical-keyboard" class="mws-i-24 i-musical-keyboard"></li>
            <li title=".mws-i-24 .i-myspace-2" class="mws-i-24 i-myspace-2"></li>
            <li title=".mws-i-24 .i-myspace" class="mws-i-24 i-myspace"></li>
            <li title=".mws-i-24 .i-new-message" class="mws-i-24 i-new-message"></li>
            <li title=".mws-i-24 .i-next-track" class="mws-i-24 i-next-track"></li>
            <li title=".mws-i-24 .i-nike-sport-shirt" class="mws-i-24 i-nike-sport-shirt"></li>
            <li title=".mws-i-24 .i-note-book" class="mws-i-24 i-note-book"></li>
            <li title=".mws-i-24 .i-off" class="mws-i-24 i-off"></li>
            <li title=".mws-i-24 .i-old-phone" class="mws-i-24 i-old-phone"></li>
            <li title=".mws-i-24 .i-pacman-ghost" class="mws-i-24 i-pacman-ghost"></li>
            <li title=".mws-i-24 .i-pacman" class="mws-i-24 i-pacman"></li>
            <li title=".mws-i-24 .i-paint-brush" class="mws-i-24 i-paint-brush"></li>
            <li title=".mws-i-24 .i-pants" class="mws-i-24 i-pants"></li>
            <li title=".mws-i-24 .i-paperclip" class="mws-i-24 i-paperclip"></li>
            <li title=".mws-i-24 .i-paypal-2" class="mws-i-24 i-paypal-2"></li>
            <li title=".mws-i-24 .i-paypal-3" class="mws-i-24 i-paypal-3"></li>
            <li title=".mws-i-24 .i-paypal-4" class="mws-i-24 i-paypal-4"></li>
            <li title=".mws-i-24 .i-paypal-5" class="mws-i-24 i-paypal-5"></li>
            <li title=".mws-i-24 .i-paypal" class="mws-i-24 i-paypal"></li>
            <li title=".mws-i-24 .i-pdf-document" class="mws-i-24 i-pdf-document"></li>
            <li title=".mws-i-24 .i-pdf-documents-1" class="mws-i-24 i-pdf-documents-1"></li>
            <li title=".mws-i-24 .i-pdf-documents" class="mws-i-24 i-pdf-documents"></li>
            <li title=".mws-i-24 .i-pencil-edit" class="mws-i-24 i-pencil-edit"></li>
            <li title=".mws-i-24 .i-pencil" class="mws-i-24 i-pencil"></li>
            <li title=".mws-i-24 .i-personal-folder-user-folder" class="mws-i-24 i-personal-folder-user-folder"></li>
            <li title=".mws-i-24 .i-phone-2" class="mws-i-24 i-phone-2"></li>
            <li title=".mws-i-24 .i-phone-3" class="mws-i-24 i-phone-3"></li>
            <li title=".mws-i-24 .i-phone-4" class="mws-i-24 i-phone-4"></li>
            <li title=".mws-i-24 .i-phone-hook" class="mws-i-24 i-phone-hook"></li>
            <li title=".mws-i-24 .i-phone" class="mws-i-24 i-phone"></li>
            <li title=".mws-i-24 .i-piggy-bank" class="mws-i-24 i-piggy-bank"></li>
            <li title=".mws-i-24 .i-plane-suitcase" class="mws-i-24 i-plane-suitcase"></li>
            <li title=".mws-i-24 .i-plate-diner" class="mws-i-24 i-plate-diner"></li>
            <li title=".mws-i-24 .i-play" class="mws-i-24 i-play"></li>
            <li title=".mws-i-24 .i-plixi" class="mws-i-24 i-plixi"></li>
            <li title=".mws-i-24 .i-plus" class="mws-i-24 i-plus"></li>
            <li title=".mws-i-24 .i-polaroids" class="mws-i-24 i-polaroids"></li>
            <li title=".mws-i-24 .i-post-card" class="mws-i-24 i-post-card"></li>
            <li title=".mws-i-24 .i-power" class="mws-i-24 i-power"></li>
            <li title=".mws-i-24 .i-powerpoint-document" class="mws-i-24 i-powerpoint-document"></li>
            <li title=".mws-i-24 .i-powerpoint-documents-1" class="mws-i-24 i-powerpoint-documents-1"></li>
            <li title=".mws-i-24 .i-powerpoint-documents" class="mws-i-24 i-powerpoint-documents"></li>
            <li title=".mws-i-24 .i-presentation-1" class="mws-i-24 i-presentation-1"></li>
            <li title=".mws-i-24 .i-presentation" class="mws-i-24 i-presentation"></li>
            <li title=".mws-i-24 .i-preview" class="mws-i-24 i-preview"></li>
            <li title=".mws-i-24 .i-previous-track" class="mws-i-24 i-previous-track"></li>
            <li title=".mws-i-24 .i-price-tag" class="mws-i-24 i-price-tag"></li>
            <li title=".mws-i-24 .i-price-tags" class="mws-i-24 i-price-tags"></li>
            <li title=".mws-i-24 .i-printer" class="mws-i-24 i-printer"></li>
            <li title=".mws-i-24 .i-quicktime-new" class="mws-i-24 i-quicktime-new"></li>
            <li title=".mws-i-24 .i-quicktime" class="mws-i-24 i-quicktime"></li>
            <li title=".mws-i-24 .i-radio-signal" class="mws-i-24 i-radio-signal"></li>
            <li title=".mws-i-24 .i-radio" class="mws-i-24 i-radio"></li>
            <li title=".mws-i-24 .i-rake-and-scoop" class="mws-i-24 i-rake-and-scoop"></li>
            <li title=".mws-i-24 .i-record" class="mws-i-24 i-record"></li>
            <li title=".mws-i-24 .i-recycle-symbol" class="mws-i-24 i-recycle-symbol"></li>
            <li title=".mws-i-24 .i-recycled-bag" class="mws-i-24 i-recycled-bag"></li>
            <li title=".mws-i-24 .i-refresh-2" class="mws-i-24 i-refresh-2"></li>
            <li title=".mws-i-24 .i-refresh-3" class="mws-i-24 i-refresh-3"></li>
            <li title=".mws-i-24 .i-refresh-4" class="mws-i-24 i-refresh-4"></li>
            <li title=".mws-i-24 .i-refresh" class="mws-i-24 i-refresh"></li>
            <li title=".mws-i-24 .i-repeat" class="mws-i-24 i-repeat"></li>
            <li title=".mws-i-24 .i-robot" class="mws-i-24 i-robot"></li>
            <li title=".mws-i-24 .i-rss" class="mws-i-24 i-rss"></li>
            <li title=".mws-i-24 .i-ruler-2" class="mws-i-24 i-ruler-2"></li>
            <li title=".mws-i-24 .i-ruler" class="mws-i-24 i-ruler"></li>
            <li title=".mws-i-24 .i-running-man" class="mws-i-24 i-running-man"></li>
            <li title=".mws-i-24 .i-safari" class="mws-i-24 i-safari"></li>
            <li title=".mws-i-24 .i-save" class="mws-i-24 i-save"></li>
            <li title=".mws-i-24 .i-scan-label-2" class="mws-i-24 i-scan-label-2"></li>
            <li title=".mws-i-24 .i-scan-label-3" class="mws-i-24 i-scan-label-3"></li>
            <li title=".mws-i-24 .i-scan-label" class="mws-i-24 i-scan-label"></li>
            <li title=".mws-i-24 .i-sd-2" class="mws-i-24 i-sd-2"></li>
            <li title=".mws-i-24 .i-sd-3" class="mws-i-24 i-sd-3"></li>
            <li title=".mws-i-24 .i-sd" class="mws-i-24 i-sd"></li>
            <li title=".mws-i-24 .i-settings-1" class="mws-i-24 i-settings-1"></li>
            <li title=".mws-i-24 .i-settings-2" class="mws-i-24 i-settings-2"></li>
            <li title=".mws-i-24 .i-settings" class="mws-i-24 i-settings"></li>
            <li title=".mws-i-24 .i-shades" class="mws-i-24 i-shades"></li>
            <li title=".mws-i-24 .i-shopping-bag" class="mws-i-24 i-shopping-bag"></li>
            <li title=".mws-i-24 .i-shopping-basket-2" class="mws-i-24 i-shopping-basket-2"></li>
            <li title=".mws-i-24 .i-shopping-basket" class="mws-i-24 i-shopping-basket"></li>
            <li title=".mws-i-24 .i-shopping-cart-2" class="mws-i-24 i-shopping-cart-2"></li>
            <li title=".mws-i-24 .i-shopping-cart-3" class="mws-i-24 i-shopping-cart-3"></li>
            <li title=".mws-i-24 .i-shopping-cart-4" class="mws-i-24 i-shopping-cart-4"></li>
            <li title=".mws-i-24 .i-shopping-cart" class="mws-i-24 i-shopping-cart"></li>
            <li title=".mws-i-24 .i-shuffle" class="mws-i-24 i-shuffle"></li>
            <li title=".mws-i-24 .i-sign-post-2" class="mws-i-24 i-sign-post-2"></li>
            <li title=".mws-i-24 .i-sign-post" class="mws-i-24 i-sign-post"></li>
            <li title=".mws-i-24 .i-single-document" class="mws-i-24 i-single-document"></li>
            <li title=".mws-i-24 .i-single-excel-document" class="mws-i-24 i-single-excel-document"></li>
            <li title=".mws-i-24 .i-single-pdf-document" class="mws-i-24 i-single-pdf-document"></li>
            <li title=".mws-i-24 .i-single-powerpoint-document" class="mws-i-24 i-single-powerpoint-document"></li>
            <li title=".mws-i-24 .i-single-user" class="mws-i-24 i-single-user"></li>
            <li title=".mws-i-24 .i-single-word-document" class="mws-i-24 i-single-word-document"></li>
            <li title=".mws-i-24 .i-single-zip-document" class="mws-i-24 i-single-zip-document"></li>
            <li title=".mws-i-24 .i-skype-2" class="mws-i-24 i-skype-2"></li>
            <li title=".mws-i-24 .i-skype" class="mws-i-24 i-skype"></li>
            <li title=".mws-i-24 .i-sleeveless-shirt" class="mws-i-24 i-sleeveless-shirt"></li>
            <li title=".mws-i-24 .i-small-brush" class="mws-i-24 i-small-brush"></li>
            <li title=".mws-i-24 .i-socks" class="mws-i-24 i-socks"></li>
            <li title=".mws-i-24 .i-sound" class="mws-i-24 i-sound"></li>
            <li title=".mws-i-24 .i-speech-bubble-2" class="mws-i-24 i-speech-bubble-2"></li>
            <li title=".mws-i-24 .i-speech-bubble" class="mws-i-24 i-speech-bubble"></li>
            <li title=".mws-i-24 .i-speech-bubbles-1" class="mws-i-24 i-speech-bubbles-1"></li>
            <li title=".mws-i-24 .i-speech-bubbles-2" class="mws-i-24 i-speech-bubbles-2"></li>
            <li title=".mws-i-24 .i-speech-bubbles" class="mws-i-24 i-speech-bubbles"></li>
            <li title=".mws-i-24 .i-sport-shirt" class="mws-i-24 i-sport-shirt"></li>
            <li title=".mws-i-24 .i-sticky-note" class="mws-i-24 i-sticky-note"></li>
            <li title=".mws-i-24 .i-stop" class="mws-i-24 i-stop"></li>
            <li title=".mws-i-24 .i-stopatch" class="mws-i-24 i-stopatch"></li>
            <li title=".mws-i-24 .i-strategy-2" class="mws-i-24 i-strategy-2"></li>
            <li title=".mws-i-24 .i-strategy" class="mws-i-24 i-strategy"></li>
            <li title=".mws-i-24 .i-strike-through-month-calendar" class="mws-i-24 i-strike-through-month-calendar"></li>
            <li title=".mws-i-24 .i-stubleupon" class="mws-i-24 i-stubleupon"></li>
            <li title=".mws-i-24 .i-suitcase-1" class="mws-i-24 i-suitcase-1"></li>
            <li title=".mws-i-24 .i-suitcase-2" class="mws-i-24 i-suitcase-2"></li>
            <li title=".mws-i-24 .i-sweater" class="mws-i-24 i-sweater"></li>
            <li title=".mws-i-24 .i-t-shirt" class="mws-i-24 i-t-shirt"></li>
            <li title=".mws-i-24 .i-table-1" class="mws-i-24 i-table-1"></li>
            <li title=".mws-i-24 .i-table" class="mws-i-24 i-table"></li>
            <li title=".mws-i-24 .i-tag-1" class="mws-i-24 i-tag-1"></li>
            <li title=".mws-i-24 .i-tag" class="mws-i-24 i-tag"></li>
            <li title=".mws-i-24 .i-tags-2" class="mws-i-24 i-tags-2"></li>
            <li title=".mws-i-24 .i-television" class="mws-i-24 i-television"></li>
            <li title=".mws-i-24 .i-text-document" class="mws-i-24 i-text-document"></li>
            <li title=".mws-i-24 .i-text-documents" class="mws-i-24 i-text-documents"></li>
            <li title=".mws-i-24 .i-text-styling-2" class="mws-i-24 i-text-styling-2"></li>
            <li title=".mws-i-24 .i-text-styling-3" class="mws-i-24 i-text-styling-3"></li>
            <li title=".mws-i-24 .i-text-styling" class="mws-i-24 i-text-styling"></li>
            <li title=".mws-i-24 .i-timer" class="mws-i-24 i-timer"></li>
            <li title=".mws-i-24 .i-tools" class="mws-i-24 i-tools"></li>
            <li title=".mws-i-24 .i-traffic-light" class="mws-i-24 i-traffic-light"></li>
            <li title=".mws-i-24 .i-trashcan-2" class="mws-i-24 i-trashcan-2"></li>
            <li title=".mws-i-24 .i-trashcan" class="mws-i-24 i-trashcan"></li>
            <li title=".mws-i-24 .i-travel-suitcase" class="mws-i-24 i-travel-suitcase"></li>
            <li title=".mws-i-24 .i-tree" class="mws-i-24 i-tree"></li>
            <li title=".mws-i-24 .i-trolly" class="mws-i-24 i-trolly"></li>
            <li title=".mws-i-24 .i-truck-2" class="mws-i-24 i-truck-2"></li>
            <li title=".mws-i-24 .i-truck" class="mws-i-24 i-truck"></li>
            <li title=".mws-i-24 .i-tumbler" class="mws-i-24 i-tumbler"></li>
            <li title=".mws-i-24 .i-twitter-1" class="mws-i-24 i-twitter-1"></li>
            <li title=".mws-i-24 .i-twitter-2" class="mws-i-24 i-twitter-2"></li>
            <li title=".mws-i-24 .i-twitter" class="mws-i-24 i-twitter"></li>
            <li title=".mws-i-24 .i-umbrella" class="mws-i-24 i-umbrella"></li>
            <li title=".mws-i-24 .i-under-construction" class="mws-i-24 i-under-construction"></li>
            <li title=".mws-i-24 .i-unfavorite" class="mws-i-24 i-unfavorite"></li>
            <li title=".mws-i-24 .i-universal-acces" class="mws-i-24 i-universal-acces"></li>
            <li title=".mws-i-24 .i-unlock-unlocked" class="mws-i-24 i-unlock-unlocked"></li>
            <li title=".mws-i-24 .i-unlocked" class="mws-i-24 i-unlocked"></li>
            <li title=".mws-i-24 .i-upload" class="mws-i-24 i-upload"></li>
            <li title=".mws-i-24 .i-usb-stick" class="mws-i-24 i-usb-stick"></li>
            <li title=".mws-i-24 .i-user-2" class="mws-i-24 i-user-2"></li>
            <li title=".mws-i-24 .i-user-comment" class="mws-i-24 i-user-comment"></li>
            <li title=".mws-i-24 .i-user" class="mws-i-24 i-user"></li>
            <li title=".mws-i-24 .i-users-2" class="mws-i-24 i-users-2"></li>
            <li title=".mws-i-24 .i-users" class="mws-i-24 i-users"></li>
            <li title=".mws-i-24 .i-v-card-1" class="mws-i-24 i-v-card-1"></li>
            <li title=".mws-i-24 .i-v-card-2" class="mws-i-24 i-v-card-2"></li>
            <li title=".mws-i-24 .i-v-card-3" class="mws-i-24 i-v-card-3"></li>
            <li title=".mws-i-24 .i-v-card" class="mws-i-24 i-v-card"></li>
            <li title=".mws-i-24 .i-vault" class="mws-i-24 i-vault"></li>
            <li title=".mws-i-24 .i-vimeo-2" class="mws-i-24 i-vimeo-2"></li>
            <li title=".mws-i-24 .i-vimeo-3" class="mws-i-24 i-vimeo-3"></li>
            <li title=".mws-i-24 .i-vimeo-4" class="mws-i-24 i-vimeo-4"></li>
            <li title=".mws-i-24 .i-vimeo-5" class="mws-i-24 i-vimeo-5"></li>
            <li title=".mws-i-24 .i-vimeo" class="mws-i-24 i-vimeo"></li>
            <li title=".mws-i-24 .i-visa-2" class="mws-i-24 i-visa-2"></li>
            <li title=".mws-i-24 .i-visa" class="mws-i-24 i-visa"></li>
            <li title=".mws-i-24 .i-wacom-2" class="mws-i-24 i-wacom-2"></li>
            <li title=".mws-i-24 .i-wacom" class="mws-i-24 i-wacom"></li>
            <li title=".mws-i-24 .i-walking-man" class="mws-i-24 i-walking-man"></li>
            <li title=".mws-i-24 .i-wifi-2" class="mws-i-24 i-wifi-2"></li>
            <li title=".mws-i-24 .i-wifi-signal-2" class="mws-i-24 i-wifi-signal-2"></li>
            <li title=".mws-i-24 .i-wifi-signal" class="mws-i-24 i-wifi-signal"></li>
            <li title=".mws-i-24 .i-wifi" class="mws-i-24 i-wifi"></li>
            <li title=".mws-i-24 .i-windows" class="mws-i-24 i-windows"></li>
            <li title=".mws-i-24 .i-wine-glass" class="mws-i-24 i-wine-glass"></li>
            <li title=".mws-i-24 .i-winner-podium" class="mws-i-24 i-winner-podium"></li>
            <li title=".mws-i-24 .i-word-document" class="mws-i-24 i-word-document"></li>
            <li title=".mws-i-24 .i-word-documents-1" class="mws-i-24 i-word-documents-1"></li>
            <li title=".mws-i-24 .i-word-documents" class="mws-i-24 i-word-documents"></li>
            <li title=".mws-i-24 .i-wordpress-2" class="mws-i-24 i-wordpress-2"></li>
            <li title=".mws-i-24 .i-wordpress" class="mws-i-24 i-wordpress"></li>
            <li title=".mws-i-24 .i-youtube-2" class="mws-i-24 i-youtube-2"></li>
            <li title=".mws-i-24 .i-youtube-3" class="mws-i-24 i-youtube-3"></li>
            <li title=".mws-i-24 .i-youtube-4" class="mws-i-24 i-youtube-4"></li>
            <li title=".mws-i-24 .i-youtube" class="mws-i-24 i-youtube"></li>
            <li title=".mws-i-24 .i-zip-documents" class="mws-i-24 i-zip-documents"></li>
            <li title=".mws-i-24 .i-zip-file" class="mws-i-24 i-zip-file"></li>
            <li title=".mws-i-24 .i-zip-files" class="mws-i-24 i-zip-files"></li>
        </ul>
    </div>
</div>