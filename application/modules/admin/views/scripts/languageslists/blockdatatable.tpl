
<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                <td><?=$row['phrase'] ?></td>
                <td class="phrase_<?=$row['id_phrase']?> <?if(!empty($row['id'])):?>languagelist_<?=$row['id']?><?endif;?>"><?=$row['value'] ?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <a class="btn btn-small" href="javascript:void(0)" onclick="<?if(!empty($row['id'])):?>editObject(<?=$row['id'] ?>, '<?=$this->escape($row['value']) ?>')<?else:?>addObject(<?=$row['id_phrase']?>, this)<?endif;?>"><i class="icon-pencil"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>