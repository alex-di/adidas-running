
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="mws-i-24 i-file-cabinet">Редактирование прав доступа для групы "<? echo $this->group->title ?>"</span>
    </div>
    <div class="mws-panel-body">
    	<form method="post"  class="mws-form" id="mws-validate">
                    <div id="elfinder" class="el-finder">
                        <div class="el-finder-toolbar">
                            <ul></ul>
                        </div>
                        <div class="el-finder-workzone">
                            <div class="el-finder-nav ui-resizable ui-resizable-autohide" style="height: 100%;width:100%;min-height: 300px;">
                                <? if(count($this->data['data'])): //echo "<pre>";print_r($this->data['data']);die;?>
                                    <ul class="el-finder-places">
                                        <? foreach($this->data['data'] as $module=>$controllers): ?>
                                            <li>
                                                <a style="background-position: 15px -30px;" class="el-finder-places-root modules" href="javascript:void(0)"><div></div><? echo $module ?></a>
                                                <? foreach($controllers as $controller=>$actions): ?>
                                                    <ul>
                                                        <li>
                                                            <a style="background-position: 15px -260px;" class="el-finder-places-root controllers" href="javascript:void(0)"><div></div><? echo $controller ?></a>
                                                                <? foreach($actions as $action=>$resource): ?>
                                                                    <ul>
                                                                        <li>
                                                                            <a id="resource_<? echo $resource->id ?>" title="<? echo $resource->description == "" ? "Описание не доступно" : $this->escape($resource->description)  ?>" style="background-position: 15px -55px;" href="javascript:void(0)" class="el-finder-places-root resources" data-resource="<? echo $resource->id ?>"><div></div><? echo $action ?></a>
                                                                            <ul></ul>
                                                                        </li>
                                                                    </ul>
                                                                <? endforeach; ?>
                                                        </li>
                                                    </ul>
                                                <? endforeach; ?>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif; ?>
                                <div class="ui-resizable-handle ui-resizable-e" style="display: none;"></div>
                                <ul class="el-finder-tree"></ul>
                            </div>
                            <!--<div class="el-finder-cwd" unselectable="on" style="height: 300px;"></div>-->
                        </div>
                    </div>
            <div class="mws-button-row">
                <?if(!empty($this->group->parent_id)):?>
                    <input type="button" onclick='setParentAcl()' class="mws-button green" value="Встановить права родителя"/>
                <?endif;?>
                <input type="button" onclick='$(".el-finder-places-root").addClass("selected")' class="mws-button green" value="Выбрать всё"/>
            	<input type="button" onclick='$(".el-finder-places-root").removeClass("selected")' class="mws-button green" value="Снять всё"/>
                <input type="button" onclick="save()" class="mws-button red" value="Сохранить"/>
				<div id="err_index"> </div>
            </div>
        </form>
    </div>    	
</div>

<style>
a.el-finder-places-root{
    cursor:pointer;
}
</style>
<script type="text/javascript">
    $(function(){
        $(".modules").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                $(this).parent().find(".controllers, .resources").removeClass("selected");
           } else {
                $(this).addClass("selected");
                $(this).parent().find(".controllers, .resources").addClass("selected");
           } 
        });
        $(".controllers").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                $(this).parent().find(".resources").removeClass("selected");
           } else {
                $(this).addClass("selected");
                $(this).parent().find(".resources").addClass("selected");
           }
           if($(this).parent().parents("li").find(".controllers").length == $(this).parent().parents("li").find(".controllers").filter(".selected").length) {
                $(this).parent().parents("li").find(".modules").addClass("selected");
           } else {
                $(this).parent().parents("li").find(".modules").removeClass("selected");
           }
        });
        
        $(".resources").click(function(){
           if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
           } else {
                $(this).addClass("selected");
           }
           
           var parent = $(this).parent().parent().parent();
           if(parent.find(".resources").length == parent.find(".resources").filter(".selected").length) {
                parent.find(".controllers").addClass("selected");
           } else {
                parent.find(".controllers").removeClass("selected");
           }
           parent = parent.parents("li");
           if(parent.find(".controllers").length == parent.find(".controllers").filter(".selected").length) {
                parent.find(".modules").addClass("selected");
           } else {
                parent.find(".modules").removeClass("selected");
           } 
        });
        <? if(count($this->data['allowed'])): ?>
            <? foreach($this->data['allowed'] as $resource_id): ?>
                $("#resource_<? echo $resource_id?>").click();
            <? endforeach; ?>
        <? endif; ?>
    });
    function setParentAcl(){
        $.post('/admin/permissions/proccessparentpermissions/id/<? echo $this->group->parent_id ?>', {}, function(data){
             $(".el-finder-places-root").removeClass("selected");
             for(var res in data) {
                $("#resource_"+data[res]).click()
             }
        }, "json");
    }
    function save() {
        var allowed = [];
        for(var i = 0; i < $(".resources").filter(".selected").length; i++) {
            allowed.push($(".resources").filter(".selected").eq(i).data("resource"));
        }
        $.post('/<? echo  $this->module_params['module']."/".$this->module_params['controller']?>/save/id/<? echo $this->group->id ?>', {allowed: allowed}, function(){
            alert("Успешно сохранено"); 
        });

    }
</script>