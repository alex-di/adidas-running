    <div class="mws-form-row">
        <label><a href="<?echo $this->data['post_material_link']?>">ссылка на пост в соц. сети</a></label>
    </div>
    <?if(!empty($this->data['post_photo'])):?>
        <div class="mws-form-row">
            <label>Картинка</label>
            <div class="mws-form-item large">
                <img src="<?echo $this->data['post_photo']?>"/>
            </div>
        </div>
    <?endif;?>
    <?if(!empty($this->data['name'])):?>
        <div class="mws-form-row">
            <label>Текст</label>
            <div class="mws-form-item large">
                <?echo $this->data['name']?>
            </div>
        </div>
    <?endif;?>
    <div class="mws-form-row">
        <label>Аватар пользователя <b><?echo $this->data['post_user_screen_name']?></b></label>
        <div class="mws-form-item large">
            <img src="<?echo $this->data['post_user_avatar']?>"/>
        </div>
    </div>
