<ul>
    <? if(count($this->data)):?>
            <? foreach($this->data as $data): ?>
                <? if($this->acl->isAllowedResource($data['parent']->action)): ?>
                    <li>
                        <a id="item_<? echo $data['parent']->id ?>" class="mws-i-24 <? echo $data['parent']->class ?>" href="<? if($data['parent']->action != ""): ?><? echo $data['parent']->action ?><? else: ?>javascript:void(0)<? endif; ?>"><? echo $data['parent']->title ?></a>
                        <? if(count($data['child'])): ?>
                            <ul class="closed">
                                <? 
                                    ksort($data['child']);
                                    $isVisibleParent = false;
                                    foreach($data['child'] as $row): ?>
                                        <? if($this->acl->isAllowedResource($row->child_action)): $isVisibleParent = true;?>
                                            <li>
                                                <a class="mws-i-24 <? echo $row->child_class ?>" href="<? if($row->child_action != ""): ?><? echo $row->child_action ?><? else: ?>javascript:void(0)<? endif; ?>"><? echo $row->child_title ?></a>
                                            </li>
                                        <? endif; ?>
                                <? endforeach; 
                                if(!$isVisibleParent):?>
                                    <script type="text/javascript">
                                        $("#item_<? echo $data['parent']->id ?>").parent().remove();
                                    </script>
                                <? endif; ?>
                            </ul>
                        <? endif; ?>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
    <? endif; ?>

</ul>
<script type="text/javascript">
    <? $resource = $this->module_params['module']."_".$this->module_params['controller']."_".$this->module_params['action'];?> 
        $(".<?=$resource ?>").parent().addClass("active");
        $(".<?=$resource ?>").parents("ul").removeClass("closed");

</script>