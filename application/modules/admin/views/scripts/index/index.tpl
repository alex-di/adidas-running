	
    <div id="mws-themer" style="right: -292px;">
        <div id="mws-themer-content">
        	<div id="mws-themer-ribbon"></div>
            <div id="mws-themer-toggle"></div>
        	<div id="mws-theme-presets-container" class="mws-themer-section">
	        	<label for="mws-theme-presets">Color Presets</label>
            </div>
            <div class="mws-themer-separator"></div>
        	<div id="mws-theme-pattern-container" class="mws-themer-section">
	        	<label for="mws-theme-patterns">Background</label>
            </div>
            <div class="mws-themer-separator"></div>
            <div class="mws-themer-section">
                <ul>
                    <li class="clearfix"><span>Base Color</span> <div id="mws-base-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Highlight Color</span> <div id="mws-highlight-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Color</span> <div id="mws-text-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Glow Color</span> <div id="mws-textglow-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Glow Opacity</span> <div id="mws-textglow-op"></div></li>
                </ul>
            </div>
            <div class="mws-themer-separator"></div>
            <div class="mws-themer-section">
                <button class="mws-button green small" onclick="saveConfiguration()">Применить</button>
                <button class="mws-button red small" onclick="deleteConfiguration()">Удалить конфигурацию</button></button>
            </div>
        </div>
        <div id="mws-themer-css-dialog">
        	<form class="mws-form">
            	<div class="mws-form-row">
		        	<div class="mws-form-item">
                    	<textarea cols="auto" rows="auto" readonly="readonly"></textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <? if(count($this->data)): ?>
        <script type="text/javascript">
            var views = <? echo json_encode($this->data); ?>;
            var tempViews = <? echo json_encode($this->data['data'][$this->data['selected']]); ?>;
            
            function deleteConfiguration(){
                var selectedView = $("#mws-theme-presets option:selected").val();
                if(views.data[selectedView].is_edit) {
                    if(selectedView != views.selected) {
                        $.post("/admin/index/processdeleteconfiguration", {id: selectedView}, function(data){
                               if(isPermission(data)) {
                                    window.location.reload();
                               } 
                        });
                    } else {
                        alert("Не возможно удалить конфигурацию. Конфигурация используется.");
                    }
                } else {
                    alert("Не возможно удалить стандартную конфигурацию");
                }
            }
            function saveConfiguration() {
                var selectedView = $("#mws-theme-presets option:selected").val();
                var configuration_id = -1;
                tempViews.name = null;
                if(views.data[selectedView].is_edit || equalConfiguration(tempViews, views.data[selectedView])) {
                    configuration_id = selectedView;
                    tempViews.name = views.data[selectedView].name;
                } else {
                    while((tempViews.name = prompt("Введите название для новой конфигурации")) == ""){}
                }
                if(tempViews.name != null) {
                    $.post("/admin/index/processaveconfiguration", {id: configuration_id, 
                                                                    name:tempViews.name,
                                                                    patterns:tempViews.patterns,
                                                                    baseColor:tempViews.baseColor,
                                                                    highlightColor:tempViews.highlightColor,
                                                                    textColor:tempViews.textColor,
                                                                    a:tempViews.textGlowColor.a,
                                                                    r:tempViews.textGlowColor.r,
                                                                    g:tempViews.textGlowColor.g,
                                                                    b:tempViews.textGlowColor.b,
                                                                    css:generateCSS()}, function(data){
                       if(isPermission(data)) {
                            window.location.reload();
                       }
                    });
                }
                
            }
            function equalConfiguration(config1, config2) {
                return config1.patterns == config2.patterns && 
                        config1.baseColor == config2.baseColor && 
                        config1.highlightColor == config2.highlightColor && 
                        config1.textColor == config2.textColor && 
                        config1.textGlowColor.a == config2.textGlowColor.a && 
                        config1.textGlowColor.r == config2.textGlowColor.r && 
                        config1.textGlowColor.g == config2.textGlowColor.g &&
                        config1.textGlowColor.b == config2.textGlowColor.b;
            }
        </script>
        <script type="text/javascript" src="/design/admin/js/themer.js"></script>
    <? endif; ?>