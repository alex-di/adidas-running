<div style="display:none;" id="mws-form-dialog-tiny-upload">
	<div id="mws-validate" class="mws-form">
		<div class="mws-panel grid_8 tableContent">
			<?if(empty($this->image_module)): ?>
                <div style="color: red;font-size:12px">Загрузка файлов не возможна. Не указан модуль загрузчика.</div>
            <?endif;?>
            <div style="height:600px;overflow: auto;" class="mws-panel-body">
				<table class="mws-datatable mws-table" id="block_table_content_tinyupload">
                        <thead>
                        	<tr>
                        		<th>Название файла</th>
                        		<th>Превью</th>
                                <th>Опции</th>
                        	</tr>
                        </thead>
                		<tbody>
                			<?php if(count($this->data)):?>
                				<?php foreach($this->data as $file):?>
	                			<tr>
	                        		<td><?php echo $file['name']?></td>
                                    <td class="image"><img src="<?echo $file['path']?>"/></td>
	                        		<td style="width:15px">
						               	<span class="btn-group">
                                            <a class="btn btn-small" href="javascript:void(0)" onclick="appendImage('<?echo $file['path']?>')"><i class="icon-attachment"></i></a>
					                        <!--<a class="btn btn-small"><i class="icon-trash"></i></a>-->
					                    </span>
	                        		</td>
	                        	</tr>
	                        	<?php endforeach;?>
                        	<?php endif;?>
                		</tbody>
                </table>
			</div>
            
		</div>
        <style>
            td.image img{
                max-width:100px;
                max-height:100px;
            }
        </style>
        
<?
    if(!empty($this->image_module)) 
        echo $this->upload('tiny_pickfiles_download', $this->image_module, 'flash', array("fileUploaded"=>"callbackUploadTiny", "error" => "onErrorUploadTiny", "already" => "true"), false, $this->savePath)
?>
	</div>
</div>

<script type="text/javascript">
	var tinyFileManager = null;
	$("#mws-form-dialog-tiny-upload").dialog({
		autoOpen: true, 
		title: "Файловый менеджер", 
		modal: true, 
		width: "640",
        zIndex: 70000,
        close: function(){
            $("#mws-form-dialog-tiny-upload").dialog('destroy').remove();
        },
		buttons: [{
				text: "Загрузить файл", 
                id:'tiny_pickfiles_download',
                },
				{
					text: "ОК", 
					click: function() {
						$("#mws-form-dialog-tiny-upload").dialog('close');
				}}]
   	});
    function appendImage(src) {
        tinyFileManager.value(src);
        $("#mws-form-dialog-tiny-upload").dialog('close');
    }
    function setTinyFileManager(tinyManager) {
        tinyFileManager = tinyManager;
    }
    function callbackUploadTiny(up, file, response) {
        jsa = $.parseJSON(response.response)
        console.log(jsa);
        $('#block_table_content_tinyupload').append('<tr>'+
	                        		'<td>'+jsa.name+'</td>'+
                                    '<td class="image"><img src="'+jsa.file+'"/></td>'+
	                        		'<td style="width:15px">'+
						               	'<span class="btn-group">'+
                                            '<a class="btn btn-small" href="javascript:void(0)" onclick="appendImage(\''+jsa.file+'\')"><i class="icon-attachment"></i></a>'+
					                    '</span>'+
	                        		'</td>'+
	                        	'</tr>');
    };
    

    function onErrorUploadTiny(up, error) {
        if(-600 == error.code) {
            alert("Недопустимый размер файла. Максимально допустимый размер файла "+formatSize(up.settings.max_file_size));
        } else {
            alert(error.message);
        }
        
    }
</script>