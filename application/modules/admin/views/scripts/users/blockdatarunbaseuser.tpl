
<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                    <td><?=$this->escape($row['id']) ?></td>
                    <td><?=$this->escape($row['email']) ?></td>
                    <td><?=$this->escape($row['name']) ?></td>
                    <td><?=$this->escape($row['sname']) ?></td>
                    <td><?=date('d.m.Y', strtotime($row['bday'])) ?></td>
                    <td><?=$this->escape($row['city']) ?></td>
                    <td><?= ($row['sex'] == 'man' ? 'М' : 'Ж') ?></td>
                    <td>
                        <?if(!empty($row['fb_id'])):?>
                            <a href="https://www.facebook.com/<?echo $row['fb_id']?>"><?echo $row['fb_username']?></a>
                        <?else:?>
                            -
                        <?endif?>
                    </td>
                    <td>
                        <?if(!empty($row['vk_id'])):?>
                            <a href="http://vk.com/id<?echo $row['vk_id']?>"><?echo $row['vk_username']?></a>
                        <?else:?>
                            -
                        <?endif?>
                    </td>
                    <td>
                        <?if(!empty($row['tw_id'])):?>
                            <a href="https://twitter.com/account/redirect_by_id?id=<?echo $row['tw_id']?>"><?echo $row['tw_username']?></a>
                        <?else:?>
                            -
                        <?endif?>
                    </td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <a class="btn btn-small" href="<?= isset($this->editlink) ? $this->editlink : '/admin/'.$this->module_params['controller'].'/edit'?>/id/<?=$row['id'] ?>"><i class="icon-pencil"></i></a>
                        <a class="btn btn-small" href="javascript:deleteObject('<?= isset($this->deletelink) ? $this->deletelink : '/admin/' . $this->module_params['controller']. '/delete' ?>/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>