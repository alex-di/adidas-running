<div class="mws-form-row">
    <label>выключить WYSIWYG</label>
    <div class="mws-form-item small">
        <input name="wysiwyng_status" type="checkbox" <?if(isset($this->data['wysiwyng_status'])):?>checked="checked"<?endif;?> onchange="changeTextMode(this)" value="1"/>
        <br />
        <textarea id="boxCode" style="display: none;width: 100%;resize: vertical;height: 500px;"><?if(isset($this->data['material_text'])) {echo $this->data['material_text'];}?></textarea>
    </div>
</div>
<script type="text/javascript">
    function changeTextMode(checkbox) {
        checkbox = $(checkbox);
        if(checkbox.is(':checked')) {
            $('#boxCode').val(tinyMCE.get('material_text').getContent()).show();
            $('#material_text').parents('.mws-form-row').hide();
        } else {
            if(confirm('статья отмечена как сверстанная, это действие приведет к изменению статьи. продолжить?')) {
                $('#material_text').parents('.mws-form-row').show();
                tinyMCE.get('material_text').setContent($('#boxCode').val());
                $('#boxCode').hide();
            } else {
                checkbox.attr('checked', 'checked');
            }
        }
    }
    
    var saveForm = function saveForm(url, form) {
        
        if($('#boxCode').is(":visible")) {
            tinyMCE.get('material_text').setContent($('#boxCode').val());
        }
        if (typeof SpawEngine != 'undefined') {
            SpawEngine.updateFields();
        }
        
        if (typeof tinyMCE != 'undefined') {
            $.each($('.tiny_block_editor'), function(index, value) {
                text = tinyMCE.get($(value).attr('id')).getContent()
                $("#" + $(value).attr('id')).val(text);
            });     
        }
        $.php(url, $(form).serialize());
        return false;
    }
    <?if(isset($this->data['wysiwyng_status'])):?>
        $(function(){
            $('#material_text').parents('.mws-form-row').hide();
            $('#boxCode').show();
        })
    <?endif;?>
</script>