<div class="mws-form-row">
    <label>Категория</label>
    <div class="mws-form-item small">
        <? echo $this->formSelect('category_id', isset($this->data['category_id']) ?  $this->data['category_id'] : null, array('onchange'=>'changeCategory(this)'), $this->category)	?>
		<div id="err_category_id"></div>
    </div>
</div>

<div id="items_review_block" class="mws-form-row" <?if(!isset($this->data['category_id']) || Model_Materials::CATEGORY_SHOP_REVIEW != $this->data['category_id']):?>style="display: none;"<?endif;?>>
    <label><input type="button" onclick="addItemsReview(0)" class="mws-button green" value="Добавить товары"/></label>
    <div class="mws-form-item small">
       <br/>
       <div class="mws-panel-body">
			
			<table class="mws-datatable mws-table" id="items_block_content">
                    <thead>
                    	<tr>
                            <th>ID</th>
                    		<th>Название</th>
                    		<th>Опции</th>
                    	</tr>
                    </thead>
            		<tbody>
            			<?php if(count($this->items)):?>
            				<?php foreach($this->items as $row):?>
                			<tr id="item_<?echo $row['id']?>" data-id="<?echo $row['id']?>" class="items">
                        		<td><?php echo $row['id']?></td>
                                <td><?php echo $row['name']?></td>
                        		<td style="width:20px">
					               	<span class="btn-group">
				                        <a class="btn btn-small" href="javascript:deleteReviewItems(<?php echo $row['id']?>)"><i class="icon-trash"></i></a>
				                    </span>
                                     <input type="hidden" name="items_review[]" value="<?echo $row['id']?>"/>
                        		</td>
                        	</tr>
                        	<?php endforeach;?>
                    	<?php endif;?>
            		</tbody>
            </table>
		</div>
    </div>
</div>
			
<div style="display:none;" class="mws-form-dialog-add-items-review">
	<div id="mws-validate" class="mws-form">
		<div class="mws-panel grid_8 tableContent">
            <div class="mws-panel-body">
                <div id="tableWrapper-items-review" class="dataTables_wrapper"></div>
			</div>
		</div>
	</div>
</div>			
<script type="text/javascript">
    function changeCategory(select) {
        var optionVal = $(select).find('option:selected').val();
        if(<?echo Model_Materials::CATEGORY_SHOP_REVIEW?> == optionVal) {
            $("#items_review_block").show();
        } else {
            $("#items_review_block").hide();
        }
    }
    function deleteReviewItems(item_id) {
        $('#item_'+item_id).remove(); 
    }
    function addItemsReview(page) {
        
        var addedItems = [];
        $('.items').each(function(){
            addedItems.push($(this).data('id'));
        });
        $.post('/admin/articles/blockgetitems/page/'+page, {added_items: addedItems}, function(data){
           $('#tableWrapper-items-review').empty().append(data);
           $(".mws-form-dialog-add-items-review").dialog('open'); 
        });
    }


	$(".mws-form-dialog-add-items-review").dialog({
		autoOpen: false, 
		title:"Выбор товаров для обзора", 
		modal: true, 
		width: "1000", 
		buttons: [{
				text: "Ok", 
				click: function() {
					$('#block_review_items .mass_checked:checked').each(function() {
					   var id = $(this).val();
					   if(!$('#item_'+id).length) {
					       $("#items_block_content").append(getItemsReviewBlock(id, $(this).parent().next().text()));
					   }
					});
                    $(".mws-form-dialog-add-items-review").dialog('close');
				}}]
   	});
    function getItemsReviewBlock(id, name, category_name) {
		  return '<tr id="item_'+id+'" data-id="'+id+'" class="items">'+
                        		'<td>'+id+'</td>'+
                                '<td>'+name+'</td>'+
                        		'<td style="width:20px">'+
					               	'<span class="btn-group">'+
				                        '<a class="btn btn-small" href="javascript:deleteReviewItems('+id+')"><i class="icon-trash"></i></a>'+
				                    '</span>'+
                                    '<input type="hidden" name="items_review[]" value="'+id+'"/>'+
                        		'</td>'+
                        	'</tr>';
    }

</script>			
