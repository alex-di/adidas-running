
    <div class="mws-form-row">
        <label>Картинка (баннер):<input id='uploader' type='button' class='mws-button green' value='Загузить'/></label>
        <div class="mws-form-item large">
			<div id="attachments_block" class="image">
				<?php 
					$photo_id = '';
					if(isset($this->data['photos']) && count($this->data['photos'])):
						$keys = array_keys($this->data['photos']);
						$photo_id = $keys[0];
				?>
					<img src="<?php echo Model_Files::getPreview($this->data['photos'][$keys[0]]['path'], 260)?>">
				<?php endif;?>
			</div>
			<div id="err_image"></div>
			<input type="hidden" value="<?php echo $photo_id?>" name="image">
        </div>
        <?= $this->upload('uploader', 'articles' ,'flash', array('fileUploaded'=>'callbackAttachments', 'error' => 'error'), 'true') ?>
    </div>
<script type="text/javascript">

    //функция обработки загрузки картинки
    function callbackAttachments(up, file, response) {
        jsa = $.parseJSON(response.response)
        console.log(jsa);
	if (jsa.status == 'error') {
	       alert(jsa.message);
    	} else {
    	    $('#attachments_block').empty().append('<img src="'+jsa.preview+'">');
    	    $('input[name="image"]').val(jsa.id);
    	}
    }
    
    function error(up, error) {
        if(-600 == error.code) {
            alert("Недопустимый размер файла. Максимально допустимый размер файла "+formatSize(up.settings.max_file_size));
        } else {
            alert(error.message);
        }
    }
            
   
</script>