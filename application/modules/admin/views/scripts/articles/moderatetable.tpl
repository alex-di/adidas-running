
<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <?//_d($row);?>
        <tr  class="<?if($row['is_modern'] != Model_Materials::TYPE_MODERN):?>article_notmodern<? else: ?>article_modern<?endif?>">
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
            
            <td><?=$row['id'] ?></td>
            <td><?=$row['name'] ?></td>
            <td><?=$row['username'] ?></td>
            <td><?=$row['category_name'] ?></td>
            <td id="status<?=$row['id'] ?>"><? 
                switch($row['is_modern']) {
                    case Model_Materials::TYPE_CANCELED:
                        echo "отклонено";
                        break;
                    case Model_Materials::TYPE_NOT_MODERN:
                        echo "не отмодерировано";
                        break;
                    case Model_Materials::TYPE_MODERN:
                        echo "отмодерировано";
                        break;
                    case Model_Materials::TYPE_DRAFTS:
                        echo "черновик";
                        break;
                }
            ?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <?if(in_array($row['is_modern'], array(Model_Materials::TYPE_NOT_MODERN, Model_Materials::TYPE_MODERN))):?>
                            <a title="Отмодерировать" class="btn btn-small" href="javascript:moderateArticle([<?=$row['id'] ?>])"><i class="icon-ok"></i></a>
                            <a title="Отказать в модерации" class="btn btn-small" href="javascript:unmoderateArticle([<?=$row['id'] ?>])"><i class="icon-stop"></i></a>
                        <?endif;?>
                        <a title="Перейти на просмотр" class="btn btn-small" target="_blank" href="/article/<?= $row['stitle'] ?>"><i class="icon-search"></i></a>
                        <a class="btn btn-small" href="/admin/articles/usedit/id/<?=$row['id'] ?>"><i class="icon-pencil"></i></a>
                        <a title="Удалить" class="btn btn-small" href="javascript:deleteArticle(<?=$row['id'] ?>)"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>

<script>
    
    function moderateArticle(id) {
        if(confirm('Отмодерировать?')) {
            $.post('/admin/articles/processmodern', {id: id}, function() {
                for(var i in id) {
                    $('#status'+id[i]).text('отмодерировано');
                }
            });
        }

    }
    
    function unmoderateArticle(id) {
        if(confirm('Отклонить модерацию?')) {
            $.post('/admin/articles/processunmodern', {id: id}, function() {
                for(var i in id) {
                    $('#status'+id[i]).text('отклонено');
                }
            });
        }
    }
    function deleteArticle(id) {
        if(confirm('Вы уверены?')) {
            window.location = '/admin/articles/deleteuserarticle/id/'+id;
        }
    }
</script>