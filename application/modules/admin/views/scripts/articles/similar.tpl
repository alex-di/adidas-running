<div class="mws-form-row">
    <label><input type="button" onclick="addArticles(0)" class="mws-button green" value="Добавить похожую статью"/></label>
    <div class="mws-form-item small">
       <br/>
       <div class="mws-panel-body">
			
			<table class="mws-datatable mws-table" id="similar_block_content">
                    <thead>
                    	<tr>
                            <th>ID</th>
                    		<th>Название</th>
                    		<th>Категория</th>
                    		<th>Опции</th>
                    	</tr>
                    </thead>
            		<tbody>
            			<?php if(count($this->data)):?>
            				<?php foreach($this->data as $row):?>
                			<tr id="similar_<?echo $row['id']?>" data-id="<?echo $row['id']?>" class="similar">
                        		<td><?php echo $row['id']?></td>
                                <td><?php echo $row['name']?></td>
                        		<td><?php echo $row['category_name']?></td>
                        		<td style="width:20px">
					               	<span class="btn-group">
				                        <a class="btn btn-small" href="javascript:deleteSimilarArticle(<?php echo $row['id']?>)"><i class="icon-trash"></i></a>
				                    </span>
                                     <input type="hidden" name="similar[]" value="<?echo $row['id']?>"/>
                        		</td>
                        	</tr>
                        	<?php endforeach;?>
                    	<?php endif;?>
            		</tbody>
            </table>
		</div>
    </div>
</div>
			
<div style="display:none;" class="mws-form-dialog-add-similar">
	<div id="mws-validate" class="mws-form">
		<div class="mws-panel grid_8 tableContent">
            <div class="mws-panel-body">
                <div id="tableWrapper" class="dataTables_wrapper"></div>
			</div>
		</div>
	</div>
</div>			
<script type="text/javascript">
    
    function deleteSimilarArticle(article_id) {
        $('#similar_'+article_id).remove(); 
    }
    function addArticles(page) {
        
        var addedArticles = [<?echo $this->resource?>];
        $('.similar').each(function(){
            addedArticles.push($(this).data('id'));
        });
        $.post('/admin/articles/blockgetsimilar/page/'+page, {need_category: <?echo json_encode($this->category)?>, added_articles: addedArticles}, function(data){
           $('#tableWrapper').empty().append(data);
           $(".mws-form-dialog-add-similar").dialog('open'); 
        });
    }


	$(".mws-form-dialog-add-similar").dialog({
		autoOpen: false, 
		title:"Выбор похожих статей", 
		modal: true, 
		width: "1000", 
		buttons: [{
				text: "Ok", 
				click: function() {
					$('#block_similar_articles .mass_checked:checked').each(function() {
					   var id = $(this).val();
					   if(!$('#similar_'+id).length) {
					       $("#similar_block_content").append(getSimilarBlock(id, $(this).parent().next().text(), $(this).parent().next().next().text()));
					   }
					});
                    $(".mws-form-dialog-add-similar").dialog('close');
				}}]
   	});
    function getSimilarBlock(id, name, category_name) {
		  return '<tr id="similar_'+id+'" data-id="'+id+'" class="similar">'+
                        		'<td>'+id+'</td>'+
                                '<td>'+name+'</td>'+
                        		'<td>'+category_name+'</td>'+
                        		'<td style="width:20px">'+
					               	'<span class="btn-group">'+
				                        '<a class="btn btn-small" href="javascript:deleteSimilarArticle('+id+')"><i class="icon-trash"></i></a>'+
				                    '</span>'+
                                    '<input type="hidden" name="similar[]" value="'+id+'"/>'+
                        		'</td>'+
                        	'</tr>';
    }

</script>			
