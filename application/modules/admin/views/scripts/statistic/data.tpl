<div class="mws-form-row">
    <div class="mws-form-item small">
       <br/>
       <div class="mws-panel-body">
			<?php if(count($this->data)):?>
                <?php foreach($this->data as $name => $rows):?>
        			<table class="mws-datatable mws-table">
                        <thead>
                        	<tr>
                                <th colspan="2" style="text-align: center;"><b><?echo $name?></b></th>
                        	</tr>
                        </thead>
                		<tbody>
            				<?php foreach($rows as $row):?>
                    			<tr>
                                    <td>
                                        <?if(!isset($row['stitle'])):?>
                                            <?php echo $row['name']?>
                                        <?else:?>
                                            <a href="<?php echo '/article/'.$row['stitle']?>" target="_blank" style="color: blue;"><?php echo $row['name']?></a>
                                        <?endif;?>
                                    </td>
                            		<td style="width: 30px;"><?php echo $row['total']?></td>
                            	</tr>
                        	<?php endforeach;?>
                		</tbody>
                    </table>
                <?php endforeach;?>
            <?php endif;?>
		</div>
    </div>
</div>
<?if(isset($this->isLoadBox)):?>
    <div id="loadBox"></div>
    
    <script type="text/javascript">
        function getStatistic() {
            var date_start = $('#date_start').datepicker('getDate');
            var date_end = $('#date_end').datepicker('getDate');
            if(null != date_start && null != date_end && date_end > date_start) {
                $.post('/admin/statistic/data', {date_start: $('#date_start').val(), date_end: $('#date_end').val()}, function(data){
                    $('#loadBox').empty().append(data);
                })
            } else {
                alert('Неверно указаная дата');
            }
            return false;
        }
    </script>
<?endif;?>