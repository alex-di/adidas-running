<div class="mws-form-row">
    <div class="mws-form-item small">
       <div class="mws-panel-body">
            <div class="dataTables_wrapper">
                <table class="mws-datatable mws-table">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><b>Название статьи</b></th>
                            <th><b>Количество шейров в VK</b></th>
                            <th><b>Количество шейров в FB</b></th>
                            <th><b>Количество шейров в TW</b></th>
                            <th><b>Количество шейров в G+</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!empty($this->shares)) {
                                foreach ($this->shares as $key => $share) {
                        ?>
                        <tr>
                            <td><a href="http://adidas-running.ru/article/<?php echo $share['alias_material'] ?>" target="_blank" style="color: blue;"><?php echo $share['name_material'] ?></a></td>
                            <td><?php echo (isset($share['vk_share_count'])) ? $share['vk_share_count'] : 'Не удалось получить'; ?></td>
                            <td><?php echo (isset($share['fb_share_count'])) ? $share['fb_share_count'] : 'Не удалось получить'; ?></td>
                            <td><?php echo (isset($share['tw_share_count'])) ? $share['tw_share_count'] : 'Не удалось получить'; ?></td>
                            <td><?php echo (isset($share['gplus_share_count'])) ? $share['gplus_share_count'] : 'Не удалось получить'; ?></td>
                        </tr>
                        <?php
                                }
                            }
                        ?>
                    </tbody>
                </table>
                <div class="paginator_view">
                   <div class="dataTables_info">Показано с <?php echo $this->pagesStart ?> по <?php echo $this->pagesEnds ?> из <?php echo $this->materialsCount ?> записей</div>
                   <div class="dataTables_paginate paging_full_numbers">
                       <span>
                            <?php
                                for ($i = 0; $i < $this->pagesCount; $i++) {
                            ?>
                            <span class="<?php echo ($i == $this->pageCurrent) ? 'paginate_button' : 'paginate_active' ?>" onclick="window.location.href='/admin/statistic/share/?page=<?php echo $i; ?>';"><?php echo ($i + 1); ?></span>
                            <?php
                                }
                            ?>
                        </span>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>