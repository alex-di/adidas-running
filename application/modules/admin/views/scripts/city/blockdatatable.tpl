<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                <? foreach($this->request_post['fields'] as $field): ?>
                    <td><?=$row[$field] ?></td>
                <? endforeach; ?>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <a class="btn btn-small" href="javascript:void(0)" onclick="editObject(<?=$row['id'] ?>, '<?=$row['city_name_ru'] ?>', '<?=$row['city_name_en'] ?>')"><i class="icon-pencil"></i></a>
                        <a class="btn btn-small" href="javascript:deleteObject('/admin/city/delete/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>

