<?
 $language = $this->module_params['id']; 
 echo $this->partial('edit.tpl', array('elements' => $this->elements, 'path'=>'/admin/languages/save/id/'.$language));?>

<? echo $this->action('index', 'languageslists', 'admin', array('languages' => $language))?>
<? echo $this->action('index', 'languagespages', 'admin', array('languages' => $language))?>
<div style="display:none;" id="mws-form-dialog-edit" class="mws-form-dialog">
	<form id="mws-edit" class="mws-form" onsubmit="return saveObject(this)">
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Значение:</label>
				<div class="mws-form-item small">
					<input type="text" name="value" value="" id="editvalue" class="" style="width:90%" />
					<div id="err_value"></div>
				</div>
			</div>
			<input type="hidden" name="id" value="" id="lv_id" />
            <input type="hidden" name="phrase_id" value="" id="ph_id" />
            <input type="hidden" name="language" value="<?=$language?>"/>
		</div>
	</form>
</div>
<script type="text/javascript">
    var isAddObject = false;
    var currentElement;
    function editObject(id, value) {
        isAddObject = false;
        $("#lv_id").val(id);
        $("#editvalue").val(value);
        $("#mws-form-dialog-edit").dialog("open");
    }
    function addObject(phrase_id, element) {
        currentElement = element;
        isAddObject = true;
        $("#lv_id").val("");
        $("#ph_id").val(phrase_id);
        $("#editvalue").val("");
        $("#mws-form-dialog-edit").dialog("open");
    }
    function saveObject(form) {
        if($("#editvalue").val() != "") {
            $.post("/admin/languageslists/save", $(form).serialize(), function(data) {
                if(isAddObject) {
                    $(currentElement).attr("onclick", "editObject("+data.id+", '"+data.value+"');");
                    $(".phrase_"+data.phrase_id).text(data.value).addClass("languagelist_"+data.id);
                } else {
                    $(".languagelist_"+data.id).text(data.value);
                }
                $("#mws-form-dialog-edit").dialog("close");
            }, "json"); 
        }
        return false;
    }
</script>