<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                <td><?=$row['id'] ?></td>
                <td><?=$row['language'] ?></td>
                <td><input type="radio" name="set" value="<?=$row['id'] ?>" <?if($row['is_default'] == 1):?>checked="checked"<? endif;?> onclick="save(this)"/></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <a class="btn btn-small" href="/admin/<?=$this->module_params['controller']?>/edit/id/<?=$row['id'] ?>"><i class="icon-pencil"></i></a>
                        <a class="btn btn-small" href="javascript:deleteObject('/admin/<?=$this->module_params['controller']?>/delete/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>
<script>
    function save(obj){
        $.post("/admin/languages/processsave", {id: $(obj).val()});
    }
</script>
