<?
 $region = $this->module_params['id']; 
 echo $this->partial('edit.tpl', array('elements' => $this->elements, 'path'=>'/admin/regions/save/'.$region));?>

<? echo $this->action('index', 'city', 'admin', array('regions_id' => $region))?>
<div style="display:none;" id="mws-form-dialog">
	<form id="mws-validate" class="mws-form" onsubmit="return saveForm('/admin/city/save', this);">
		<div id="mws-validate-error" class="mws-form-message error" style="display:none;"></div>
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Значение:</label>
				<div class="mws-form-item small">
					<textarea name="value" id="" class="" style="width:90%" cols="100%" rows="100%"></textarea>
					<div id="err_value"> </div>
				</div>
				
			</div>
			<input type="hidden" name="regions_id" value="<? echo $region ?>" />
		</div>
	</form>
</div>


<div style="display:none;" id="mws-form-dialog-edit" class="mws-form-dialog">
	<form id="mws-edit" class="mws-form" onsubmit="return saveForm('/admin/city/save', this);">
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Название по умолчанию:</label>
				<div class="mws-form-item small">
					<input type="text" name="city_name_ru" value="" id="city_name_ru_" class="mws-text" style="width:90%" />
					<div id="err_city_name_ru"> </div>
				</div>
			</div>
			<div class="mws-form-row">
				<label>Интернациональное название:</label>
				<div class="mws-form-item small">
					<input type="text" name="city_name_en" value="" id="city_name_en_" class="mws-text" style="width:90%" />
					<div id="err_city_name_en"> </div>
				</div>
			</div>
			<input type="hidden" name="id" value="" id="lv_id" />
		</div>
	</form>
</div>
<script type="text/javascript">
    function addListsObject() {
        $("#mws-form-dialog").dialog("open");
    }
    function editObject(id, value_ru, value_en) {
        $("#lv_id").val(id);
        $("#city_name_ru_").val(value_ru);
        $("#city_name_en_").val(value_en);
        $("#mws-form-dialog-edit").dialog("open");
    }
</script>