
<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                <td><a target="_blank" href="/admin/pages/edit/id/<?=$row["id_def"] ?>"><?=$row["name_def"] ?></a></td>
                <td><?=$row["name"] ?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <?if(!empty($row['id'])):?>
                            <a class="btn btn-small" href="/admin/languagespages/edit/id/<?=$row["id"] ?>"><i class="icon-pencil"></i></a>
                        <?else:?>
                            <a class="btn btn-small" href="/admin/languagespages/edit/page/<?=$row["id_def"] ?>/language/<?=$this->language?>"><i class="icon-pencil"></i></a>
                        <?endif;?>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>