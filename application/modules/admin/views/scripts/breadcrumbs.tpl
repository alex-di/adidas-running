<?if(count($this->data)):?>
    <div class="breadcrumbs">
        <?foreach($this->data as $index=>$row):?>
            <span>
                <?if(0 == $index):?>
                    <i class="icon-home"></i>
                <?endif;?>
                <a class="breadcrumb_name" href="<?echo $row['url']?>"><?echo $row['name']?></a>
                <?if($index != count($this->data) - 1):?>
                    <span class="divider">»</span>
                <?endif;?>
            </span>
        <?endforeach;?>
    </div>
<?endif;?>