<div id="mws-login-wrapper">
    <div id="mws-login">
        <h1>Sign in</h1>
        <div class="mws-login-lock"><img src="/design/admin/css/icons/24/locked-2.png" alt="" /></div>
        <div id="mws-login-form">
            <form id="login_form" class="mws-form" method="post" onsubmit="return saveForm('/admin/auth/login', this)">
                <div class="mws-form-row">
                    <div class="mws-form-item large">
                        <input type="text" name="login" class="mws-login-username mws-textinput required" placeholder="email" />
                        <div id="err_login"></div>
                    </div>
                </div>
                <div class="mws-form-row">
                    <div class="mws-form-item large">
                        <input type="password" name="password" class="mws-login-password mws-textinput required" placeholder="password" />
                        <div id="err_password"></div>
                    </div>
                </div>
                <div class="mws-form-row">
                    <input type="submit" value="Sign in" class="mws-button green mws-login-button" />
                </div>
            </form>
        </div>
    </div>
</div>
