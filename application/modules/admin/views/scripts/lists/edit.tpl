<?
 $list = $this->module_params['id']; 
 echo $this->partial('edit.tpl', array('elements' => $this->elements, 'path'=>'/admin/lists/save/id/'.$list));?>

<? echo $this->action('index', 'listvalues', 'admin', array('list_id' => $list))?>

<div style="display:none;" id="mws-form-dialog" class="mws-form-dialog">
	<form id="mws-validate" class="mws-form" onsubmit="return saveForm('/admin/listvalues/save', this);">
		<div id="mws-validate-error" class="mws-form-message error" style="display:none;"></div>
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Значение:</label>
				<div class="mws-form-item small">
					<textarea name="value" id="" class="" style="width:90%" cols="100%" rows="100%"></textarea>
					<div id="err_value"> </div>
				</div>
				
			</div>
			<input type="hidden" name="list_id" value="<? echo $list ?>" />
		</div>
	</form>
</div>


<div style="display:none;" id="mws-form-dialog-edit" class="mws-form-dialog">
	<form id="mws-edit" class="mws-form" onsubmit="return saveForm('/admin/listvalues/save', this);">
		<div class="mws-form-inline">
			<div class="mws-form-row">
				<label>Значение:</label>
				<div class="mws-form-item small">
					<input type="text" name="value" value="" id="editvalue" class="" style="width:90%" />
					<div id="err_value"> </div>
					
				</div>
				<label>Русский текст:</label>
				<div class="mws-form-item small">
					
					<input type="checkbox" name="ru" value="1" id="langvalue" class="" checked="0" />
					<div id="err_lang"> </div>
				</div>
				
			</div>
			<input type="hidden" name="id" value="" id="lv_id" />
		</div>
	</form>
</div>
<script type="text/javascript">
    function addListsObject() {
        $("#mws-form-dialog").dialog("open");
    }
    function editObject(id, value, lang, list_id) {
        $("#lv_id").val(id);
	
        $("#editvalue").val(value);
	if (list_id == 28) {
	  if (1 == lang ) {
	    $("#langvalue").attr('checked', true)
	  } else {
	    $("#langvalue").attr('checked', false)
	  }
	}
	
        $("#mws-form-dialog-edit").dialog("open");
    }
    function move(first, two) {
        window.location = "/admin/listvalues/move/list/<? echo $list ?>/first/"+first+"/two/"+two;
    }
</script>