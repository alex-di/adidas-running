<?
    $jsPaginateFunction = 'paginator';
    if(isset($this->jsPaginateFunction))
        $jsPaginateFunction = $this->jsPaginateFunction;
?>
<? if(count($this->pagesInRange) > 1): ?>
        <div class="dataTables_info">Показано с <? echo ($this->current-1)*$this->itemCountPerPage+1; ?> по <?echo ($this->current-1)*$this->itemCountPerPage + $this->currentItemCount;?> из <? echo $this->totalItemCount; ?> записей</div>
        <div class="dataTables_paginate paging_full_numbers">
            <span class="first paginate_button<? if($this->current <= 1):?> paginate_button_disabled<? endif; ?>"
                    <?if($this->current > 1):?> onclick = '<? echo $jsPaginateFunction ?>(1, this)'<? endif; ?>>Первая</span>
            <span class="previous paginate_button<? if($this->current < 2):?> paginate_button_disabled<? endif; ?>"
                    <?if($this->current > 1):?> onclick = '<? echo $jsPaginateFunction ?>(<? echo $this->current-1 ?>, this)'<? endif; ?>>Предыдущая</span>
            <span>
            
            <? foreach ($this->pagesInRange as $page): ?>
                <span class="<? if($page == $this->current):?>paginate_active<? else: ?>paginate_button<? endif; ?>" onclick='<? echo $jsPaginateFunction ?>(<? echo $page?>, this)'><?echo $page?></span>
            <? endforeach; ?>
            </span>
            <span class="next paginate_button <? if($this->current == $this->pageCount):?> paginate_button_disabled<? endif; ?>"
                    <?if($this->current < $this->pageCount):?> onclick = '<? echo $jsPaginateFunction ?>(<? echo $this->next ?>, this)'<? endif; ?>>Следующая</span>
            <span class="last paginate_button paginate_button<? if($this->current == $this->last):?>  paginate_button_disabled<? endif; ?>"
                    <?if($this->current < $this->last):?> onclick = '<? echo $jsPaginateFunction ?>(<? echo $this->last ?>, this)'<? endif; ?>>Последняя</span>
        </div>
<? endif;?>
