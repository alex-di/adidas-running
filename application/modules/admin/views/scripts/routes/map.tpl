    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,drawing,geometry"></script>

    <script>

var map;
var markers = [];

//var directionsDisplay;
//var directionsService = new google.maps.DirectionsService();
var currentPolyline = new google.maps.Polyline();
function initialize() {
    
$('form').before('<div class="panel" >'+
              '<input id="searchTextField" type="text" size="50"/>'+
            '</div>');
  var mapSelector = 'map-canvas';
  $('#'+mapSelector).width($('.mws-form-row').width());
  var mapDiv = document.getElementById(mapSelector);
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(55.73, 37.63),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    streetViewControl:false,
    panControl:false,
    //zoomControl: false,
  };
  map = new google.maps.Map(mapDiv, mapOptions);
    var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_LEFT,
      drawingModes: [
        google.maps.drawing.OverlayType.POLYLINE
      ]
      }
    });
    drawingManager.setDrawingMode(null);
    drawingManager.setMap(map);
    google.maps.event.addListener(drawingManager, 'polylinecomplete', function(polyline) {
        currentPolyline.setMap(null);
        currentPolyline = polyline;
        var path = [];
        polyline.getPath().forEach(function(latLng) {
            path.push({jb: latLng.lat(), kb: latLng.lng()});
        });

        $('input[name="start_lat"]').val(path[0].jb);
        $('input[name="start_lng"]').val(path[0].kb);
        $('input[name="end_lat"]').val(path[path.length - 1].jb);
        $('input[name="end_lng"]').val(path[path.length - 1].kb);
        var bounds = getBoundsForPoly(polyline);
        polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
        console.log(polyLengthInMeters);
        var center = bounds.getCenter();
        var northEast = bounds.getNorthEast();
        var radius = getRadius(center, northEast);
        //console.log(radius);	
        $('input[name="center_lat"]').val(center.lat());
        $('input[name="center_lng"]').val(center.lng());
        $('input[name="radius_square"]').val(radius);
        $('input[name="overview_path"]').val(JSON.stringify(path));
        var mileage = Math.round(polyLengthInMeters) / 1000;
        $('input[name="mileage"]').val(Math.round(mileage * 100) / 100);
        
        /*new google.maps.Marker({
			    position: bounds.getCenter(),
			    map: map
			})
            new google.maps.Marker({
			    position: bounds.getNorthEast(),
			    map: map
			})
            new google.maps.Marker({
			    position: bounds.getSouthWest(),
			    map: map
			})*/
    });
  /*google.maps.event.addListener(map, 'click', function(e) {
	  console.log(e.latLng);
		console.log(e.latLng.jb);
		if(markers.length > 9) {
			alert("Максимальное разрешенное количество путевых точек составляет 8 помимо исходной точки и пункта назначения. Клиентам API Google Карт для организаций разрешено использовать 23 путевые точки помимо исходной точки и пункта назначения. Для маршрутов общественного транспорта путевые точки не поддерживаются.");
		} else {
			markers.push(new google.maps.Marker({
			    position: new google.maps.LatLng(e.latLng.jb, e.latLng.kb),
			    map: map
			}));
		}				
  });*/
  
  //directionsDisplay = new google.maps.DirectionsRenderer({draggable: true});
  //directionsDisplay.setMap(map);
  //google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
  //	 recalculate(directionsDisplay.directions);
  //});
  <?php if(isset($this->data['overview_path'])):?>
  	var overview_path = <?echo $this->data['overview_path']?>;
  	
  	var latLngArray = [];
  	for(var i in overview_path) {
  		latLngArray.push(new google.maps.LatLng(overview_path[i].jb, overview_path[i].kb));
  	}
  	
  	currentPolyline.setPath(latLngArray);
  	currentPolyline.setMap(map);
    map.fitBounds(getBoundsForPoly(currentPolyline));
    
  <?php endif;?>

    //map search
    
    var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    autocomplete.setTypes([]);
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map
    });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    input.className = '';
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      // Inform the user that the place was not found and return.
      input.className = 'notfound';
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
function getBoundsForPoly(polyline) {
  var bounds = new google.maps.LatLngBounds;
  polyline.getPath().forEach(function(latLng) {
    bounds.extend(latLng);
  });
  return bounds;
}

function getRadius(p1, p2){
    var R = 6371; // Radius of the Earth in km
    var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
    var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; 
}

function resetRoute(){
	console.log('resetRoutes');
	currentPolyline.setMap(null);

    return false;
}


</script>
    <style>
      div.panel input {
        border: 1px solid  rgba(0, 0, 0, 0.5);
      }
      div.panel input.notfound {
        border: 2px solid  rgba(255, 0, 0, 0.4);
      }
      div.panel {
            background-color: #FFFFFF;
            border: 1px solid #999999;
            left: 50%;
            margin-left: -95px;
            padding: 5px;
            position: absolute;
            top: 121px;
            z-index: 5;
        }
    </style>
    <div class="mws-form-row">
       	<!--<input id="calculate_button" type='button' class='mws-button green' value='Построить маршрут' onclick="calcucalateRoute()"/>-->
       	<input id="reset_button" type='button' class='mws-button green' value='Сбросить' onclick="resetRoute()"/>
        <br/><br/>
        <div class="mws-form-item large">


			<div id="map-canvas" style="width: 1000px; height: 800px"></div>
			<div id="err_route"></div>

        </div>
    </div>