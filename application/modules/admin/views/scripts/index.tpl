
<? if(count($this->tables)): ?>
    <? foreach($this->tables as $name=>$table): 
        $table_id = md5($name);
        $module = "admin";
        $action = "blockdatatable";
        $data_action = "/$module/".$this->module_params['controller']."/$action";
        $controller = $this->module_params['controller'];
        if(isset($table['data_action'])) {
            $data_action = "/$module/".$table['data_action'];
            list($controller, $action) = explode("/", $table['data_action']);
        }
         
        $namespace = strtr($data_action, array("/"=>""));
    ?>
    <div id="tableContent_<?=$table_id ?>" class="mws-panel grid_8 tableContent" data-action="<?=$data_action ?>" style="width: 100%;">
        <?if(isset($table['message'])):?>
            <?foreach($table['message'] as $type=>$message):?>
                <?
                    $message_class = "";
                    switch($type) {
                        case DR_Api_Admin_Table::MESSAGE_SUCCESS:
                            $message_class = "success";
                            break;
                        default:
                            $message_class = "error";
                            break;  
                    }
                ?>
                <div class="mws-form-message <?echo $message_class?>" ><?echo $message?></div>
            <?endforeach?>
        <?endif;?>
        <div class="mws-panel-header">
        	<span class="mws-i-24 i-table-1"><?=$name ?></span>
            <div class="mws-panel-toolbar top">
            	<ul>
                    <? if(isset($table['toolbar'])): ?>
                        <? foreach($table['toolbar'] as $type=>$button): ?>
                            <? 
                                $res = array();
                                switch($type) {
                                    case DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD:
                                        $res = DR_Api_Admin_Table::getSettingsForToolBar("Добавить", $button, "addObject(this, '$controller')", "ic-add");
                                        break;
                                    case DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE:
                                        $res = DR_Api_Admin_Table::getSettingsForToolBar("Удалить", $button, "massDeleteObject(this, '$controller')", "ic-cross");
                                        break;
                                    //case DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER:
                                    default:
                                        $res = DR_Api_Admin_Table::getSettingsForToolBar($button['title'], $button);
                                        break;
                                }
                             ?>
                	       <li><a class="mws-ic-16 <?=$res['class'] ?>" href="<?=$res['href'] ?>" title="<?=$res['title'] ?>" <? if(!empty($res['handler'])): ?>onclick="<?=$res['handler'] ?>"<?endif;?>><?=$res['title'] ?></a></li>
                        <? endforeach; ?>
                    <? endif; ?>
                </ul>
            </div>
        </div>
        <form onsubmit="paginator(<?=DR_Api_Admin_Table::getFromStorePage($namespace) ?>, this);return false;">
            <div class="mws-panel-body">
                <div class="dataTables_wrapper">
                <? if(isset($table['is_page']) && $table['is_page']): ?>
                    <div class="dataTables_filter" style="text-align: left;">
                        <div>
                            <label>Показать <?=$this->formSelect("per_page", DR_Api_Admin_Table::getFromStorePerPage($namespace), array("onchange"=>"paginator(0, this)"), DR_Api_Admin_Table::getListPerPage()) ?> записей</label>
                        </div>
                    </div>
                <? endif; ?>
        
                    <table class="mws-datatable mws-table">
                        <thead>
                            <? $isSearch = false;
                               $search = "";
                               if(isset($table['is_mass_check']) && $table['is_mass_check'])
                                    $search = '<th><span class="btn-group"><a href="javascript:void(0)" onclick="paginator(0, this)" class="btn btn-small"><i class="icon-search"></i></a></span></th>';
                               if(isset($table['fields'])) {
                                    foreach($table['fields'] as $colum=>$field) {
                                        if(isset($field['filter'])) {
                                            $isSearch = true;
                                            $filter = "";
                                            if($field['filter'] instanceof DR_Api_Admin_FilterNumber) {
                                                $filter = $field['filter']->getHtml($colum, DR_Api_Admin_Table::getFromStoreSearchNumberValue($namespace, $field['filter']->getFieldName($colum)));
                                            } elseif ($field['filter'] instanceof DR_Api_Admin_FilterString){
                                                $filter = $field['filter']->getHtml($colum, DR_Api_Admin_Table::getFromStoreSearchTextValue($namespace, $field['filter']->getFieldName($colum)));
                                            } elseif ($field['filter'] instanceof DR_Api_Admin_FilterList){
                                                $filter = $field['filter']->getHtml($colum, DR_Api_Admin_Table::getFromStoreSearchListValue($namespace, $field['filter']->getFieldName($colum)));
                                            }
                                            $search .= "<th>$filter</th>";
                                        } else {
                                            $search .= "<th></th>";
                                        }
                                    }
                               }
                               if(isset($table['is_option_coll']) && $table['is_option_coll'])
                                    $search .= '<th><span class="btn-group"><a href="javascript:void(0)" onclick="paginator(0, this)" class="btn btn-small"><i class="icon-search"></i></a></span></th>';
                             ?>
                            <tr>
                                <?
                                $collspan = 0;
                                $useFields = array();
                                if(isset($table['is_mass_check']) && $table['is_mass_check']): $collspan++; ?>
                                    <th style="width: 30px;"><input type="checkbox" onclick="massCheck(this)"/></th>
                                <? endif; ?>
                                <? if(isset($table['fields'])): ?>
                                    <? foreach($table['fields'] as $colum=>$field): 
                                        $collspan++;
                                        $useFields[] = $colum;?>
                                        <th id="<?=$colum ?>" class="<? if(!isset($field['is_sort']) || $field['is_sort']): ?>sort_field sorting<? endif; ?>"><?=$field['name'] ?></th>
                                    <? endforeach; ?>
                                <? endif; ?>
                                <?if(isset($table['is_option_coll']) && $table['is_option_coll']): $collspan++; ?>
                                    <th style="width: 60px;">Опции</th>
                                <? endif; ?>
                            </tr>
                            <? if($isSearch): ?>
                                <tr><?=$search ?></tr>
                            <? endif; ?>
                        </thead>
                        
                        <tbody>
                        <?=$this->action($action, $controller, $module, DR_Api_Admin_Table::getTableParams($table, $namespace));?>
                        </tbody>
                    </table>
                    <div class="paginator_view"></div>
                </div>
                <input type="hidden" value="<?=$collspan?>" name="collspan"/>
                <input type="hidden" value="<?=DR_Api_Admin_Table::getFromStorePage($namespace) ?>" name="page"/>
                <input type="hidden" value="" name="order"/>
                <? if(isset($table['is_page'])): ?>
                    <input type="hidden" value="<?echo $table['is_page']?>" name="is_page"/>
                <?endif;?>
                <? foreach($useFields as $field): ?>
                    <input type="hidden" value="<?=$field?>" name="fields[]"/>
                <? endforeach; ?>
                <?if(isset($table['is_mass_check']) && $table['is_mass_check']):?>
                    <input type="hidden" value="1" name="is_mass_check"/>
                <? endif; ?>
                <?if(isset($table['is_option_coll']) && $table['is_option_coll']):?>
                    <input type="hidden" value="1" name="is_option_coll"/>
                <? endif; ?>
                <?if(isset($table['params'])):?>
                    <? foreach($table['params'] as $key=>$val): ?>
                        <?if(!is_array($val)):?>
                            <input type="hidden" value="<?=$val ?>" name="<?=$key ?>"/>
                        <?else:
                            foreach($val as $v):?>
                                <input type="hidden" value="<?=$v ?>" name="<?=$key ?>[]"/>
                            <?endforeach;
                        endif;?>
                    <? endforeach; ?>
                <? endif; ?>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        setOrderColl("tableContent_<?=$table_id ?>", "<?=DR_Api_Admin_Table::getFromStoreOrder($namespace) ?>");
        //$("#tableContent_<?=$table_id ?>").find("form").submit();
        $(function(){
            var paginatorView = $("#tableContent_<?=$table_id ?>").find("tr.paginator_data > td").html();
            $("#tableContent_<?=$table_id ?>").find("tr.paginator_data").remove();
            $("#tableContent_<?=$table_id ?>").find("div.paginator_view").append(paginatorView); 
        });
    </script>
    <? endforeach; ?>
<? endif; 
        /**
         * data_action - от куда таблица берёт данные. не обьязательный параметр (дефолтное значение текущий контроллер/blockdatatableAction()). 
         * название модуля не нужно указывать.
         * is_page - режим пагинации. не обьязательный параметр. по умолчанию выключена
         * is_mass_check - чекбокс массового выбора обьектов. не обьязательный параметр. по умолчанию отключен
         * is_option_coll - столбец для операций с обьектом. не обьязательный параметр. по умолчанию отключен
         * toolbar - меню кнопок таблицы. не обьязательный параметр.
         *     title - название кнопки. не обьязательный параметр.
         *     action - урл ссылка. не обьязательный параметр.
         *     handler - ф-ция, которая срабатывает, при нажатии на кнопку. не обьязательный параметр.
         *     class - css - отображение. не обьязательный параметр.
         * fields - столбци таблицы
         *      name - название столбца.
         *      is_sort - сортировка по столбцу. не обьязательный параметр. по умолчанию включено
         *      filter - поиск по одному из столбцов. не обьязательный параметр.
         * params - дополнительные параметры
            $this->view->tables = array("Test" => array(
                    "data_action" => "default/blockdatatable",
                    "is_page" => true,
                    "is_mass_check" => true,
                    "is_option_coll" => true,
                    "toolbar" => array(
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array(),
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER => array(
                            "title" => "Other",
                            "action" => "",
                            "handler" => "",
                            "class" => "")),
                    "fields" => array(
                        "id" => array(
                            "name" => "ID",
                            "is_sort" => false,
                            "filter" => new DR_Api_View_Admin_FilterNumber()),
                        "email" => array("name" => "E-mail", "filter" => new DR_Api_View_Admin_FilterString()),
                        "group_id" => array("name" => "Група", "filter" => new DR_Api_View_Admin_FilterString()),
                        "name" => array("name" => "Имя"),
                        "sname" => array("name" => "Фамилия"),
                        "date_reg" => array("name" => "Дата регистрации")),
                    "params" => array("parent_id"=>1, "group_id"=>array(1,2,3)),
                    "message" => array("success"=>"Ok!", "error" => "Error!")
                    ));
         */

?>

