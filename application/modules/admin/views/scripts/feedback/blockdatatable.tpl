<? if(count($this->data)): ?>
    <? foreach($this->data as $row): ?>
        
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
                <!--<td>
                    <<?
                     $max_lenght = 20;
                     foreach($this->items[$row['id']] as $item):?>
                     <div style="width: 220px;">
                            <a target="_blank" href="/items/<?=$item['stitle']?>"><?= mb_substr($item['name'], 0, $max_lenght, "UTF-8").(mb_strlen($item['name'], "UTF-8") > $max_lenght ? "..." : "")?></a> - <b><?=$item['count']?> шт.</b>
                        </div>
                    <? endforeach ?>
                </td>-->
                <td><?=$row["id"] ?></td>
                <td><?=$row["name"]?></td>
                
                <td><?=$row["theme"] ?></td>
                <td><div style="width: 100px;"><?=date("d.m.Y H:i", strtotime($row["date"])) ?></div></td>
                <td><?= $row["complited"]==0 ? '<div class="mws-button-row mws-button red">Не рассмотрено</div>' :
                                                '<div class="mws-button-row mws-button green">Рассмотрено</div>'?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        <? if($row['id']): ?>                     
                            <a title="Просмотр" class="btn btn-small" href="/admin/<?=$this->module_params['controller']?>/view/id/<?=$row['id'] ?>"><i class="icon-search"></i></a>
                            <a title="Удалить обьект" class="btn btn-small" href="javascript:deleteObject('/admin/<?=$this->module_params['controller']?>/delete/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                        <? endif ?>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>

 