<? if(count($this->data)):?>
    <?$i=0; foreach($this->data as $row): ?>
        <tr>
            <? if(isset($this->request_post['is_mass_check'])): ?>
                <td><input type="checkbox" class="mass_checked" value="<?=$row['id'] ?>"/></td>
            <? endif; ?>
            <td><?=$i+1 ?></td>
            <td><?=$row['value'] ?></td>
            <? if(isset($this->request_post['is_option_coll'])): ?>
                <td>
                    <span class="btn-group">
                        
                        <a class="btn btn-small" href="javascript:void(0)" <?if($i == 0):?>disabled="disabled"<?else:?>onclick="move(<?=$row['id']?>, <?=$this->data[$i-1]['id']?>)"<?endif;?>><i class="icon-caret-up"></i></a>
                        <a class="btn btn-small" href="javascript:void(0)" <?if($i == count($this->data) - 1):?>disabled="disabled"<?else:?>onclick="move(<?=$row['id']?>, <?=$this->data[$i+1]['id']?>)"<?endif;?>><i class="icon-caret-down"></i></a>
                        
                        <a class="btn btn-small" href="javascript:void(0)" onclick="editObject(<?=$row['id'] ?>, '<?=$row['value'] ?>', <?= $row['ru']?>, <?= $row['list_id']?>)"><i class="icon-pencil"></i></a>
                        <a class="btn btn-small" href="javascript:deleteObject('/admin/listvalues/delete/id/<?=$row['id'] ?>')"><i class="icon-trash"></i></a>
                    </span>
                </td>
            <? endif; ?>
        </tr>
    <?$i++; 
    endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="<?=$this->request_post['collspan'] ?>">
            <center>Нет данных</center>
        </td>
    </tr>
<? endif; ?>
<tr class="paginator_data" style="display: none;">
    <td>
        <? if(!is_null($this->pages)) { echo $this->paginationControl($this->pages, 'Sliding', 'ajaxpaginator.tpl');} ?>
    </td>
</tr>

