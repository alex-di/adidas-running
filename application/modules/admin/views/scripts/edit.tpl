<div class="mws-panel grid_8">
    <div class="mws-panel-header">
    	<span class="mws-i-24 i-pencil"><?=$this->elements['name'] ?></span>
            <div class="mws-panel-toolbar top">
                <ul>
                    <li><a class="mws-ic-16 ic-arrow-left" href="<? echo $_SERVER['HTTP_REFERER'] ?>" title="Назад">Назад</a></li>
                </ul>
            </div>
    </div>
   
    <div class="mws-panel-body">
        <?
            $path = isset($this->path) ? $this->path : '/'.$this->module_params['module'].'/'.$this->module_params['controller'].'/save';
            $path .=  (isset($this->module_params['id']) ? '/id/' . $this->module_params['id'] : '');
	    //(isset($this->module_params['id']) && !isset($this->path)  ? '/id/' . $this->module_params['id'] : "");
        ?>
    	<form onsubmit="return saveForm('<?=$path ?>', this);" method="post" class="mws-form" id="mws-validate">
             <? if(isset($this->elements['fields'])): ?>
                <? foreach($this->elements['fields'] as $name=>$html): ?>
                    <? if($name !== intval($name) && strpos($name, "partial") === false): ?>
                        <div class="mws-form-row">
                            <label><?=$name ?>:</label>
                            <div class="mws-form-item small">
                                <?=$html ?>
                            </div>
                        </div>
                    <? else: ?>
                        <?=$html ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? endif; ?>
            <div class="mws-button-row">
                <? if(isset($this->elements['toolbars'])): ?>
                    <? foreach($this->elements['toolbars'] as $name=>$handler): ?>
                        <input type="button" onclick="<?=$handler ?>" class="mws-button blue" value="<?=$name ?>"/>
                    <? endforeach; ?>
                <? endif; ?>
                <?if(!isset($this->elements['is_hide_save_button']) || !$this->elements['is_hide_save_button']):?>
            	   <input type="submit" class="mws-button green" value="Сохранить"/>
                <?endif;?>
            </div>
        </form>
    </div>    	
</div>