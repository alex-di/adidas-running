<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_statisticController extends DR_Controllers_Admin {
    
    public function commonAction() {
        $this->getBreadcrumbs()->appendView('Общая статистика');
        $this->view->data = $this->getCommonStatistic();
        
		$form = new DR_Api_Admin_EditForm(array());
		$this->view->elements = array("name" => "Общая статистика",
                "is_hide_save_button" => true,
				"fields" => array(
                        $form->partial('statistic/data.tpl', array('data' => $this->view->data, 'isLoadBox'=>true)),
						"Дата начала" => $form->date('date_start'),
                        "Дата окончания" => $form->date('date_end'),),
                "toolbars"=>array("OK"=>"getStatistic()"));
        $this->render("edit", null, true);
    }

    public function shareAction() {
        $shares = array();
        $data = array();

        $limit = $this->getRequest()->getParam('limit', 20);
        $currentPage = $this->getRequest()->getParam('page', 0);


        $this->getBreadcrumbs()->appendView('Статистика по share');
        $count = api::getShares()->_new(array('total'=>'count(1)'))
                                    ->row();
        $materialsCount = (int) $count['total'];
        $shares = array();

        $this->view->assign('materialsCount', $materialsCount);
        $this->view->assign('pagesCount', ceil($materialsCount / $limit));
        $this->view->assign('pageCurrent', $currentPage);
        $this->view->assign('pagesStart', ($currentPage * $limit) + 1);
        $this->view->assign('pagesEnds',  ($currentPage * $limit) + $limit);

        if ($materialsCount !== 0) {
            $shares = api::getShares()->_new()
                                      ->limitPage($currentPage, $limit)
                                      ->rows();
        }

        $this->view->assign('shares', $shares);
    }

    public function dataAction() {
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $this->view->data = $this->getCommonStatistic($this->_getParam('date_start'), $this->_getParam('date_end'));
    }

    protected function getCommonStatistic($date_start = null, $date_end = null) {
        api::getMaterials()->_new(array('t.id','t.name', 't.stitle', 't.rate'))
                        ->notIn('t.category_id', array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST, Model_Materials::CATEGORY_RUNBASE, Model_Materials::CATEGORY_UNIQUE))
                        ->where('t.is_modern', Model_Materials::TYPE_MODERN)
                        ->order('t.rate DESC')
                        ->limit(3);
        $postfix = 'за все время';
        if(!is_null($date_start) && !is_null($date_end)) {
            api::getMaterials()->between('t.date', $date_start, $date_end, '>=', '<=');
            $postfix = "с $date_start по $date_end";
        }
            
        $data = api::getMaterials()->rows();
        $result = array();
        $head = 'Топ3 -популярных статей '.$postfix;
        if(count($data)) {
            foreach($data as $row) {
                $result[$head][$row->id] = array('name'=>$row->name, 'total'=>$row->rate, 'stitle'=>$row->stitle);
            }
        }
        $head = 'Кол-во добавленных статей, комментариев, лайков '.$postfix;
        $result[$head] = array();
        api::getMaterials()->_new(array('total'=>'count(1)'))
                        ->notIn('t.category_id', array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST, Model_Materials::CATEGORY_RUNBASE, Model_Materials::CATEGORY_UNIQUE))
                        ->where('t.is_modern', Model_Materials::TYPE_MODERN);
        if(!is_null($date_start) && !is_null($date_end))
            api::getMaterials()->between('t.date', $date_start, $date_end, '>=', '<=');
        $data = api::getMaterials()->row();
        $result[$head][] = array('name'=>'Статей', 'total'=>$data->total);
        api::getComments()->_new(array('total'=>'count(1)'))
                        ->where('t.modules_id', Model_Comments::MATERIALS);
        if(!is_null($date_start) && !is_null($date_end))
            api::getComments()->between('t.date', $date_start, $date_end, '>=', '<=');
        $data = api::getComments()->row();
        $result[$head][] = array('name'=>'Комментарии', 'total'=>$data->total);
        
        api::getVotes()->_new(array('total'=>'count(1)'))
                        ->where('t.modules_id', Model_Votes::MATERIALS);
        if(!is_null($date_start) && !is_null($date_end))
            api::getVotes()->between('t.date', $date_start, $date_end, '>=', '<=');
        $data = api::getVotes()->row();
        $result[$head][] = array('name'=>'Лайки', 'total'=>$data->total);
        
        $modelListvalues = new Model_Listvalues;
        $modelListvalues->_new(array('total'=>'count(mt.id)', 't.value', 't.id'))
                        ->joinLeft(array('meta' => api::META), "meta.list_value_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS. " and meta.key = 'post_tag_id'")
                        ->joinLeft(array('mt' => api::MATERIALS), "mt.id = meta.resource_id")
                        ->where('mt.is_modern', Model_Materials::TYPE_MODERN)
                        ->where('t.list_id', Model_Listvalues::HASH_TAGS_LIST)
                        ->group('t.id')
                        ->order('total DESC');
        if(!is_null($date_start) && !is_null($date_end))
            $modelListvalues->between('mt.date', $date_start, $date_end, '>=', '<=');
        $head = 'Кол-во опубликованых статей по хештегам '.$postfix;
        $data = $modelListvalues->rows();
        if(count($data)) {
            foreach($data as $row) {
                $result[$head][$row->id] = array('name'=>$row->value, 'total'=>$row->total);
            }
        }
        return $result;
    }
}