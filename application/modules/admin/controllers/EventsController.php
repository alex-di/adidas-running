<?php

/**
 * Админ контроллер для добавления событий
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_eventsController extends DR_Controllers_Admin {
	public function init() {
		$this->_model = api::getMaterials();
	}
	public function indexAction() {
        $this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"События" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),));

		parent::indexAction();
	}

	public function blockdatatableAction() {
		
		$this->_model
		->joinLeft(array('mt' => api::META), 'mt.key = "is_profile_material" and t.id = mt.resource_id', array('is_profile' => "(select 1 from dual)"))
                ->where('mt.id IS NULL')
		->where('category_id', Model_Materials::CATEGORY_EVENTS);
		parent::blockdatatableAction();
	}

	public function editAction() {
        $this->getBreadcrumbs()->appendView('События')->appendEdit();
		$keywords = '';
		$this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
		//_d($this->view->data);
        $similar = array();
		if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
            $similar = $this->_model->getSimilarMaterials($this->view->data['id']);
		}
        
		$user_list = api::getUsers()->_new()->fields(array('username' => "concat(t.name, ' ', t.sname)"))
				->in('t.group_id', array(Model_Groups::FAMOUS, Model_Groups::ADMIN))->forSelect(array('id' => 'username'));
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование события",
				"fields" => array(
                        $form->partial('articles/link.tpl', array('data' => $this->view->data, 'routerName'=>'event_view')),
                        "Название события" => $form->stringInput('name'),
						"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
						"Пользователь" => $form->select('users_id', $user_list),
						"Наличие регистрации на событие" => $form->checkbox('is_need_register', 1),
						"Скрыть на сайте" => $form->checkbox('is_modern', 0, '', 1),
                        "Дата события" => $form->datetime('date_start_event'),
                        "Картинка" => $form->upload('title_image', 'materials'),
						$form->partial('articles/attachments.tpl', array('data' => $this->view->data)),
                        $form->partial('articles/wysiwyg.tpl', array('data' => $this->view->data)),
						"Текст" => $form->tiny('material_text', '', '', array('image_module'=>'materials'))
								. '<b>для того, чтобы использовать галерею - перейдите в редакторе в Инструменты -> Исходный код и обрамите необходимые изображения в '.htmlspecialchars('<div class="gallery"></div>')
								. ' для указания названия изображения в галерее используйте атрибут alt '.htmlspecialchars('(например <img src="" alt="test">)')
                                . '<br></br><b>для того, чтобы прикрепить товар к тексту - перейдите в Инструменты -> Исходный код и обрамите необходимый текст в '.htmlspecialchars('<div id="item_ID"></div> где ID - ид товара').'</b>',
                        $form->partial('articles/similar.tpl', array('data' => $similar, 'resource'=>$this->_getParam('id', 0), 'category'=>Model_Materials::getCategoryForSimilarArticles()))));


	}

	public function saveAction() {

	   
		$this->getValidator()->setNotRequiredFields(array("image"))->setElement('date_start_event')
				->date(null, array('format' => 'Y-m-d H:i:s'))
                ->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			list($html, $gallery, $items) = DR_Api_Tools_Materials::processHtml($_POST['material_text']);
			$_POST['category_id'] = Model_Materials::CATEGORY_EVENTS;
			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
			//$_POST['is_modern'] = 1;
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
			$newId = $this->_model->doSave($_POST, $this->_getParam('id'));
			api::getKeywords()->saveMaterialKeywords($_POST['keywords'], $newId);
			api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_ITEMS, $items);
			api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_MATERIALS, isset($_POST['similar']) ? $_POST['similar'] : array());
			$model_meta = api::getMeta();

			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
			$model_meta
					->doSave(
							array('value' => $_POST['is_need_register'], 'key' => 'is_need_register',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('date_value' => trim($_POST['date_start_event']), 'key' => 'date_start_event',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $_POST['material_text'], 'key' => 'material_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
  			if(isset($_POST['wysiwyng_status']))
                $model_meta
    					->doSave(
    							array('key' => 'wysiwyng_status', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
    									'value' => 1));
			if(isset($_POST['image']) && !empty($_POST['image']))
				$model_meta
						->doSave(
								array('key' => 'photos', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
										'file_id' => $_POST['image']));
			//_d($gallery);
            if (count($gallery)) {
				foreach ($gallery as $index => $images) {
					foreach ($images as $img) {
						$model_meta
								->doSave(
										array('value' => $img['src'], 'int_value' => $index, 'key' => 'gallery', 'text_value'=>$img['name'], 
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            $this->_redirectAjaxAction('/admin/events', $this->message);
		}
		$this->Response($data);

	}
}
