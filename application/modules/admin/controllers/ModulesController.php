<?php

/**
 * @author appalach
 * @version 1.0
 * @date 06.06.2013
 */
class Admin_modulesController extends DR_Controllers_Admin {
    
    public function init() {
        $this->_model = api::getModules();
    }
    
    #Список доступных модулей
    public function indexAction() {
        $this->view->tables = array(
            "Модули" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action' => '/admin/modules/add')),
                "fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название"))));
        
    }
    
    #Добавить новый модуль
    public function addAction() {
        $options = array(
            DR_Api_Admin_Modules::MODULE_EXIST => "Создать модель",
            DR_Api_Admin_Modules::CONTROLLER_ADMIN_EXIST => "Создать Контроллер для админки",
            DR_Api_Admin_Modules::CONTROLLER_SITE_EXIST => "Создать Контроллер для сайта",
            DR_Api_Admin_Modules::VIEWS_EXIST => "Создавать папку Вида для контроллеров сайта",
        );
        
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array(
            "Название(анг) - url модуля" => $form->stringInput('name'),
            "Опции" => $form->select("options", $options, "options[]", array_keys($options)),
        );
    
            
        $this->view->elements = array("name" => "Добавление модуля",
            "fields" => $fields);
        $this->render("edit", null, true);
    }
    
    #редактирование модуля
    public function editAction() {
        parent::editAction();
        
        
        #путь сохранения 
        $this->view->path = '/admin/modules/saveparams';
        $params = unserialize($this->view->data->params);
        
        //_d($params);
        $template = new DR_Api_Admin_Template;
        
	    $form = new DR_Api_Admin_EditForm($this->view->data);
	    $fields = array(
                "Название" => $form->stringInput('title'),
                "Настройки отображения таблици" => $form->multicheckbox(
                    "checkboxes",
                    $template::getCheckboxes(),
                    'params[checkboxes]',
                    isset($params['checkboxes']) ? $params['checkboxes'] : array()),
                "Поля" => $form->partial(
                    'modules/fields.tpl',
                    array(
                        'params' => $params,
                        'columns' => $this->_model->getColumns(),
                        'filters' => $template::getFiltersType()
                    )
                ),
                //"Отображаемые поля" => $form->multicheckbox("fields", $this->_model->getColumns(), 'params[fields]', isset($params['fields']) ? $params['fields'] : array())
            
        );

		$this->view->elements = array("name" => "Редактирование Модуля", "fields" => $fields);
		$this->render("edit", null, true);
	}
    
    
    #Сохранить модуль
    public function saveAction() {
        
        list($valid, $data) = $this->isValid($_POST);
        
        if ($valid) {
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));
            
            $modules_tools = new DR_Api_Admin_Modules($_POST['name'], $options);
            $modules_tools->createModuleStructure();
            
        }
    
        $this->Response($data);
    }
    
    #Сохранить параметры модуля
    public function saveparamsAction() {
        list($valid, $data) = $this->isValid($_POST);
        //_d($_POST);
        if ($valid) {
            
            if(count($_POST['params']['field_filter'])) {
                foreach($_POST['params']['field_filter'] as $field=> $value) {
                    if(empty($value)) {
                        unset($_POST['params']['field_filter'][$field]);
                    }
                }
            }
            //_d($_POST);
            if(isset($_POST['params'])) {
                $_POST['params'] = serialize($_POST['params']);
            }
            //_d($this->_getParam('id'));
            $this->_model->doSave($_POST, $this->_getParam('id'));
        }
    
        $this->Response($data);
    }
    
    #удалить модуль
    public function deleteAction() {
        $id = $this->_getParam('id');
        $model = api::getModules();
        $record = $model($id);
        $modules_tools = new DR_Api_Admin_Modules($record->name);
        $modules_tools->deleteAll();
        
        $record->delete();
        $this->_redirect("/admin/" . $this->view->module_params['controller']);
    }   
     
    
}