<?php

/**
 * Админ контроллер для добавления заметок и цитат
 * @author appaalch
 * @version 1.0
 * @final 
 */
class Admin_itemsController extends DR_Controllers_Admin
{


    public function init()
    {
        $this->_model = api::getItems();
    }


    public function indexAction()
    {
        $this->getBreadcrumbs()->appendView();
        $this->view->tables = array("Товары" => array(
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),
                ));

        parent::indexAction();
    }


    public function editAction()
    {
        parent::editAction();

        $this->getBreadcrumbs()->appendView('Товары')->appendEdit();
        $id = $this->_getParam('id');

        $form = new DR_Api_Admin_EditForm($this->view->data);
        $this->view->elements = array("name" => "Редактирование товара", "fields" => array(
                "Название" => $form->stringInput('name'),
                "Картинка" => $form->upload('image', 'items'),
                'Ссылка в магазине adidas' => $form->stringInput('adidas_link')));

        $this->render("edit", null, true);
    }


    /*public function saveAction()
    {
        $this->getValidator()->setNotRequiredFields(array('adidas_link'));
        parent::saveAction();
    }*/
}
