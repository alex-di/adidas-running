<?php

/**
 * Админ контроллер для настроек сайта
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_configController extends DR_Controllers_Admin
{
    public function indexAction()
    {
        $this->view->tables = array("Текстовые настройки" => array(
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "name" => array("name" => "Internal value", "filter" => new DR_Api_Admin_FilterString()),
                    "comment" => array("name" => "Описание"))));
        parent::indexAction();
    }
    public function blockdatatableAction()
    {
        $this->_model->where("type", Model_Config::TYPE_TEXT);
        parent::blockdatatableAction();
    }
    public function imagesAction()
    {
        $this->view->tables = array("Дефолтные картинки" => array(
                "data_action" => "config/blockimages",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array("action"=>"/admin/config/editimage"), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "name" => array("name" => "Internal value", "filter" => new DR_Api_Admin_FilterString()),
                    "comment" => array("name" => "Описание"))));
        parent::indexAction();
    }
    public function blockimagesAction()
    {
        $this->isSelfViewTable = true;
        $this->_model->where("type", Model_Config::TYPE_IMAGE);
        parent::blockdatatableAction();
    }
    // насройка админ меню
    public function sidebarAction()
    {
        $model_sidebar = api::getControlSidebar();
        $this->view->resource = DR_Api_Tools_Controlmenu::getAdminActions();
        $this->view->data = $model_sidebar->getMenu();
    }
    // сохранение элемента админ панели
    public function savesidebarAction()
    {
        api::getControlSidebar()->doSaveItem($_POST);
        die("ok");
    }
    // удаление элемента админ панели
    public function processdeleteitemmenuAction()
    {
        api::getControlSidebar()->doDeleteItem($_POST['id']);
        die("ok");
    }
    // изменение позиции элементов админ панели
    public function processmovemenuitemAction()
    {
        api::getControlSidebar()->move($_POST['first_id'], $_POST['two_id']);
        die("ok");
    }
    
    public function editimageAction(){
        parent::editAction();
        if (!$this->view->data['id'])
        {
            $id = $this->_model->doSave(array("name" => "New Settings Image", "type"=>Model_Config::TYPE_IMAGE));
            $this->_redirect("/admin/config/editimage/id/" . $id);
        } else
        {
            $form = new DR_Api_Admin_EditForm($this->view->data);
            $this->view->elements = array("name" => "Редактирование настройки картинки", "fields" => array(
                    "Название" => $form->stringInput('name'),
                    "Картинка" => $form->upload('value', 'config'),
                    "Коментарий" => $form->stringInput('comment')));
            $this->render("edit", null, true);
        }
    }
    /**
     * редактирование настройки
     */
    public function editAction()
    {

        parent::editAction();
        if (!$this->view->data['id'])
        {
            $id = $this->_model->doSave(array("name" => "New Settings", "type"=>Model_Config::TYPE_TEXT));
            $this->_redirect("/admin/config/edit/id/" . $id);
        } else
        {
            $form = new DR_Api_Admin_EditForm($this->view->data);
            $this->view->elements = array("name" => "Редактирование настройки", "fields" => array(
                    "Название" => $form->stringInput('name'),
                    "Значение" => $form->textarea('value'),
                    "Настройка участвует в переводе" => $form->checkbox('is_translate', 1),
                    "Коментарий" => $form->stringInput('comment')));
            $this->render("edit", null, true);
        }
    }
    public function deleteAction(){
        
        if(strpos($_SERVER['HTTP_REFERER'], "images") !== false)
            $this->location = "/admin/config/images";
        parent::deleteAction();
    }
}
