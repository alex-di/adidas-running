<?php

/**
 * Админ контроллер для добавления обзоров товара
 * @author appalach
 * @version 1.0
 * @final 
 */
class Admin_shopController extends DR_Controllers_Admin {
	public function init() {
		$this->_model = api::getMaterials();
	}
        
	public function indexAction() {
        $this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Обзоры товаров" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),));

		parent::indexAction();
	}

	public function blockdatatableAction() {
		$this->_model->where('category_id', Model_Materials::CATEGORY_SHOP_REVIEW);
		parent::blockdatatableAction();
	}

	public function editAction() {

		$keywords = '';
        $this->getBreadcrumbs()->appendView('Обзоры товаров')->appendEdit();
		$this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
		
		$user_list = api::getUsers()->_new()->fields(array('username' => "concat(t.name, ' ', t.sname)"))
				->in('t.group_id', array(Model_Groups::FAMOUS, Model_Groups::ADMIN))->forSelect(array('id' => 'username'));
		$references = array();
		if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
            
            $dat_ref = api::getReferences()
                    ->_new()
                    ->where('t.object_id', $this->view->data['id'])
                    ->where('t.ref_type', Model_References::REF_TYPE_MATERIAL_SHOP)->rows();
            
            //_d($dat_ref);
            foreach($dat_ref as $ref) {
                $references[] = $ref->reference_id;
            }
                
		}
		  $shop_list = api::getItems()->_new()->fields(array('t.name'))
				->forSelect(array('id' => 'name'));
                $shop_list[0] = "";
                
                
                
                ksort($shop_list);
                    
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование обзора",
				"fields" => array("Название события" => $form->stringInput('name'),
						"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
						"Пользователь" => $form->select('users_id', $user_list),
						"Товар" => $form->select('item_id', $shop_list, 'item_id[]', $references),
                        "Картинка" => $form->upload('title_image', 'materials'),
						$form->partial('articles/attachments.tpl', array('data' => $this->view->data)),
						"Текст" => $form->tiny('material_text', '', '', array('image_module'=>'materials'))
								. '<b>для того, чтобы использовать галерею - перейдите в редакторе в Инструменты -> Исходный код и обрамите необходимые изображения в '.htmlspecialchars('<div class="gallery"></div>')
								. ' для указания названия изображения в галерее используйте атрибут alt '.htmlspecialchars('(например <img src="" alt="test">)')
                                . '<br></br><b>для того, чтобы прикрепить товар к тексту - перейдите в Инструменты -> Исходный код и обрамите необходимый текст в '.htmlspecialchars('<div id="item_ID"></div> где ID - ид товара').'</b>'));

		$this->render("edit", null, true);
	}

	public function saveAction() {
		$this->getValidator()->setNotRequiredFields(array("image"))->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			list($html, $gallery, $items) = DR_Api_Tools_Materials::processHtml($_POST['material_text']);
			$_POST['category_id'] = Model_Materials::CATEGORY_SHOP_REVIEW;
			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
                        $_POST['is_modern'] = 1;
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
                        
                        
                        $newId = $this->_model->doSave($_POST, $this->_getParam('id'));
			api::getKeywords()->saveMaterialKeywords($_POST['keywords'], $newId);
                        api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_ITEMS, $items);
                        api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_SHOP, $_POST['item_id']);
			$model_meta = api::getMeta();

			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
			
			$model_meta
					->doSave(
							array('text_value' => $_POST['material_text'], 'key' => 'material_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			if(isset($_POST['image']) && !empty($_POST['image']))
				$model_meta
						->doSave(
								array('key' => 'photos', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
										'file_id' => $_POST['image']));
			if (count($gallery)) {
				foreach ($gallery as $index => $images) {
					foreach ($images as $src) {
						$model_meta
								->doSave(
										array('value' => $src, 'int_value' => $index, 'key' => 'gallery',
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            $this->_redirectAjaxAction('/admin/shop', $this->message);
		}
		$this->Response($data);

	}
}
