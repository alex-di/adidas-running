<?php

/**

 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_routesController extends DR_Controllers_Admin {

	public function init() {
		$this->_model = api::getMaterials();
	}
	public function indexAction() {
        $this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Маршруты" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название"))));

		parent::indexAction();
	}

	public function blockdatatableAction() {
		$this->_model
		->joinLeft(array('mt' => api::META), 'mt.key = "is_profile_material" and t.id = mt.resource_id', array('is_profile' => "(select 1 from dual)"))
                ->where('mt.id IS NULL')
		->where('category_id', Model_Materials::CATEGORY_ROUTERS);
		parent::blockdatatableAction();
	}

	public function editAction() {
        $this->getBreadcrumbs()->appendView('Маршруты')->appendEdit();
		$this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
         $similar = array();
		if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
			$routes_type_id = array_keys($this->view->data['routes_type']);
			$this->view->data['routes_type'] = $routes_type_id[0];
            $similar = $this->_model->getSimilarMaterials($this->view->data['id']);
		}
		//_d($this->view->data);
		$user_list = api::getUsers()->_new()->fields(array('username' => "concat(t.name, ' ', t.sname)"))
				->in('t.group_id', array(Model_Groups::FAMOUS, Model_Groups::ADMIN))->forSelect(array('id' => 'username'));
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование маршрута",
				"fields" => array(
                        $form->partial('articles/link.tpl', array('data' => $this->view->data, 'routerName'=>'route_view')),
                        $form->partial('routes/map.tpl', array('data' => $this->view->data)),
						"Название маршрута" => $form->stringInput('name'),
						"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
                        "Скрыть на сайте" => $form->checkbox('is_modern', 0, '', 1),
						"Пользователь" => $form->select('users_id', $user_list),
                        "Картинка" => $form->upload('title_image', 'materials'),
						$form->partial('articles/attachments.tpl', array('data' => $this->view->data)),
						"Километраж" => $form->stringInput('mileage'),
						"Тип маршрута" => $form->select('routes_type', $this->view->lists['routes_type']),
						"Точка старта" => $form->stringInput('name_point_start'),
						"Точка финиша" => $form->stringInput('name_point_finish'),
                        $form->partial('articles/wysiwyg.tpl', array('data' => $this->view->data)),
						"Текст" => $form->tiny('material_text', '', '', array('image_module'=>'materials'))
								. '<b>для того, чтобы использовать галерею - перейдите в редакторе в Инструменты -> Исходный код и обрамите необходимые изображения в '.htmlspecialchars('<div class="gallery"></div>')
								. ' для указания названия изображения в галерее используйте атрибут alt '.htmlspecialchars('(например <img src="" alt="test">)')
                                . '<br></br><b>для того, чтобы прикрепить товар к тексту - перейдите в Инструменты -> Исходный код и обрамите необходимый текст в '.htmlspecialchars('<div id="item_ID"></div> где ID - ид товара').'</b>',
                        $form->partial('articles/similar.tpl', array('data' => $similar, 'resource'=>$this->_getParam('id', 0), 'category'=>Model_Materials::getCategoryForSimilarArticles())),
						$form->hidden('overview_path'), $form->hidden('radius_square'), $form->hidden('center_lat'),
						$form->hidden('center_lng'), $form->hidden('start_lat'), $form->hidden('start_lng'),
						$form->hidden('end_lat'), $form->hidden('end_lng'), $form->hidden('routesdata_id')));

		$this->render("edit", null, true);
	}

	public function saveAction() {
		$this->getValidator()->setNotRequiredFields(array("routesdata_id"))->setElement('mileage')->float()
                            ->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			list($html, $gallery, $items) = DR_Api_Tools_Materials::processHtml($_POST['material_text']);
			
			$_POST['category_id'] = Model_Materials::CATEGORY_ROUTERS;
			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
			//$_POST['is_modern'] = 1;
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
			$newId = $this->_model->doSave($_POST, $this->_getParam('id'));

			api::getKeywords()->saveMaterialKeywords($_POST['keywords'], $newId);
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_ITEMS, $items);
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_MATERIALS, isset($_POST['similar']) ? $_POST['similar'] : array());
			$routesdata_id = null;
			if (!empty($_POST['routesdata_id']))
				$routesdata_id = $_POST['routesdata_id'];
			api::getRoutesdata()
					->doSave(
							array('materials_id' => $newId, 'radius_square' => $_POST['radius_square'],
									'center_lat' => $_POST['center_lat'], 'center_lng' => $_POST['center_lng'],
									'start_lat' => $_POST['start_lat'], 'start_lng' => $_POST['start_lng'],
									'end_lat' => $_POST['end_lat'], 'end_lng' => $_POST['end_lng']), $routesdata_id);
			$model_meta = api::getMeta();

			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
			
			$model_meta
					->doSave(
							array('text_value' => $_POST['material_text'], 'key' => 'material_text', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));						
			$model_meta
					->doSave(
							array('double_value' => $_POST['mileage'], 'key' => 'mileage', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('list_value_id' => $_POST['routes_type'], 'key' => 'routes_type',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('key' => 'photos', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
									'file_id' => $_POST['image']));
			$model_meta
					->doSave(
							array('key' => 'overview_path', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'text_value' => $_POST['overview_path']));
			$model_meta
					->doSave(
							array('key' => 'name_point_start', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'value' => $_POST['name_point_start']));
			$model_meta
					->doSave(
							array('key' => 'name_point_finish', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS, 'value' => $_POST['name_point_finish']));
  			if(isset($_POST['wysiwyng_status']))
                $model_meta
    					->doSave(
    							array('key' => 'wysiwyng_status', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
    									'value' => 1));
            if (count($gallery)) {
				foreach ($gallery as $index => $images) {
					foreach ($images as $img) {
						$model_meta
								->doSave(
										array('value' => $img['src'], 'int_value' => $index, 'key' => 'gallery', 'text_value'=>$img['name'], 
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            $this->_redirectAjaxAction('/admin/routes', $this->message);
		}
		$this->Response($data);

	}
}
