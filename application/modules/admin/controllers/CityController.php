<?php
/**
 * Админ контроллер для городов
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_cityController extends DR_Controllers_Admin
{
    public function init()
    {
        $this->_model = api::getCity();
    }
    public function indexAction()
    {
        $this->view->tables = array("Города" => array(
                "data_action" => "city/blockdatatable",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array("handler" => "addListsObject()"), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "city_name_ru" => array("name" => "Название по умолчанию", "filter" => new DR_Api_Admin_FilterString()),
                    "city_name_en" => array("name" => "Интернациональное название", "filter" => new DR_Api_Admin_FilterString())),
                "params" => array("regions_id" => $this->_getParam('regions_id', 0))));
        parent::indexAction();
    }
    public function blockdatatableAction()
    {
        $this->isSelfViewTable = true;
        $this->_model->where('regions_id', $this->_getParam('regions_id', 0));
        parent::blockdatatableAction();
    }
    public function saveAction()
    {
        list($valid, $data) = $this->isValid($_POST);
        if ($valid)
        {
            $values = array();
            if ($_POST['id'])
            {
                $values['city_name_ru'] = $_POST['city_name_ru'];
                $values['city_name_en'] = $_POST['city_name_en'];
                $id = $this->_model->doSave($values, $_POST['id']);
            } else
            {
                $list_values = str_replace("\r", '', explode("\n", $_POST['name']));
                $values = array("regions_id" => $_POST['regions_id']);
                foreach ($list_values as $value)
                {
                    $values['city_name_ru'] = $value;
                    $this->_model->doSave($values);
                }
            }
        }
        $this->Response($data);
    }
    //удалить значения региона
    public function deleteAction()
    {
        if ($id = $this->_getParam('id'))
        {
            $this->_model->doDelete($id);
            $this->_redirect("/admin/regions/edit/id/" . $this->_getParam('regions_id'));
        } else
        {
            $this->showError("обьект для удаления не передан");
        }
    }
}