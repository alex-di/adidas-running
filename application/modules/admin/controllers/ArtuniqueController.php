<?php

/**
 * Админ контроллер для добавления статей
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_artuniqueController extends DR_Controllers_Admin {
	
    public function init() {
		$this->_model = api::getMaterials();
	}
    
	public function indexAction() {
		$this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Уникальные статьи" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),));

		parent::indexAction();
	}
    
	public function blockdatatableAction() {
	    $this->isSelfViewTable = true;
		$this->_model->where('t.is_unique', 1);
		parent::blockdatatableAction();
	}
    
    public function editAction(){
         $this->getBreadcrumbs()->appendView('Уникальные статьи')->appendEdit();
  		 $keywords = '';
         $similar = array();
		 $this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
         $reviewItems = array();
 		 if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
            $similar = $this->_model->getSimilarMaterials($this->view->data['id']);
            $reviewItems = $this->_model->getReviewItems($this->view->data['id']);
		}
        $category_list = array(Model_Materials::CATEGORY_COACH_ARTICLES => "статья тренера",
										Model_Materials::CATEGORY_INSPIRING_ARTICLES => "вдохновляющая статья",
										Model_Materials::CATEGORY_ROUTE_ARTICLES => "статья о маршрутах",
                                        Model_Materials::CATEGORY_MICOACH_ARTICLES => "статья для micoach",
                                        Model_Materials::CATEGORY_SHOP_REVIEW => "обзор товаров");
        
        $form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование статьи",
            "fields" => array(
                "Название статьи" => $form->stringInput('name'),
                $form->partial('articles/category.tpl', array('data' => $this->view->data, 'category'=>$category_list, 'items'=>$reviewItems)),
				"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
                $form->partial('articles/similar.tpl', array('data' => $similar, 'resource'=>$this->_getParam('id', 0), 'category'=>Model_Materials::getCategoryForSimilarArticles())),
                "Скрыть на сайте" => $form->checkbox('is_modern', 0, '', 1),
                "Картинка" => $form->upload('title_image', 'materials'),
                "Краткое описание" => $form->textarea('short_text'),
                "Сыылка на статью" => $form->stringInput('article_url'),
            ));
		$this->render("edit", null, true);
    }
    public function saveAction(){
        $this->getValidator()->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
        if(Model_Materials::CATEGORY_SHOP_REVIEW != $_POST['category_id'] || !isset($_POST['items_review']))
            $_POST['items_review'] = array();
		
        list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
            $_POST['is_unique'] = 1;
            $_POST['users_id'] = Zend_Auth::getInstance()->getIdentity()->id;
			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
            $post = $_POST;
			$newId = $this->_model->doSave($post, $this->_getParam('id'));
			api::getKeywords()->saveMaterialKeywords($post['keywords'], $newId);
            
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_MATERIALS, isset($post['similar']) ? $post['similar'] : array());
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_SHOP, $_POST['items_review']);
            
            $model_meta = api::getMeta();
            
			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
			$model_meta
					->doSave(
							array('text_value' => $post['short_text'], 'key' => 'short_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('value' => $post['article_url'], 'key' => 'article_url', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
            
            $this->_redirectAjaxAction('/admin/artunique');
		}
		$this->Response($data);
    }
 }