<?php

/**
 * Админ контроллер для баннеров
 * @author appalach
 * @version 1.0
 */
class Admin_PromomaterialsController extends DR_Controllers_Admin
{

    public function indexAction()
    {
        $promo_id = $this->_getParam('promo_id', 0);
        $action = '/admin/promomaterials/edit/promo_id/' . $promo_id;

        $count_material = $this->_getParam('count_banners');
        $current_count_material = $this->_model->_new()->where('promo_id', $promo_id)->count();

        $this->view->tables = array("Промо материалы" => array(
                "is_page" => true,
                "data_action" => "promomaterials/blockdatatable",
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array( //DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action' => $action),
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("id" => array("name" => "ID"), "type_block" => array("name" => "Тип")),
                "params" => array("promo_id" => $promo_id)));

        if ($count_material > $current_count_material)
        {
            $this->view->tables['Промо материалы']['toolbar'][DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD] = array('action' => $action);
        }

        parent::indexAction();
    }

    public function blockdatatableAction()
    {
        $promo_id = $this->_getParam('promo_id');
        $this->_model->where('promo_id', $this->_getParam('promo_id'));
        $this->_model->joinLeft(array('promo' => api::LIST_VALUES), 't.type = promo.id', array('type_block' => 'promo.value'));
        $this->view->editlink = '/admin/promomaterials/edit/promo_id/' . $promo_id;
        $this->view->deletelink = '/admin/promomaterials/delete';


        parent::blockdatatableAction();
    }

    public function editAction()
    {
        parent::editAction();
        $promo_id = $this->_getParam('promo_id');
        $this->getBreadcrumbs()->append('Промо Блоки', 'promoblock')->append('Редактирование','promoblock/edit/id/'.$promo_id)->appendEdit('Редактирование промо материала');
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array(
            "Тип блока" => $form->select('type', $this->view->lists['promo_type_blocks'], "type", array(), array('onchange' => 'changeType(this)')),
            "Банер" => $form->upload('image', 'banners'),
            "Код" => $form->textarea('code'),
            "Id материала" => $form->stringInput('material_id'),

            );
        
        $fields[] = $form->hidden('promo_id', 'promo_id', $promo_id);

        $this->view->elements = array("name" => "Редактирование банера", "fields" => $fields);
        //$this->render("edit", null, true);
    }
    public function saveAction()
    {

        switch ($_POST['type'])
        {
            case Model_Promomaterials::BANNER:
                unset($_POST['image']);
                unset($_POST['code']);
                break;
            case Model_Promomaterials::CODE:
                unset($_POST['material_id']);
                unset($_POST['image']);
                break;
            case Model_Promomaterials::IMAGE:
                unset($_POST['material_id']);
                unset($_POST['code']);
                break;
        }

        //_d($_POST);

        $this->getValidator()->setElement('material_id')->int("Необходимо целочисленное значение")->recordExists("Материал не найден", array('table' => api::MATERIALS, 'field' => 'id'));
        $this->location = '/admin/promoblock/edit/id/' . $_POST['promo_id'];
        parent::saveAction();
    }

    public function deleteAction()
    {
        $this->location = $_SERVER['HTTP_REFERER'];
        parent::deleteAction();
    }
}
