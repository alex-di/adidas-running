<?php

/**
 * Ђдмин контроллер длЯ значений списков
 * @author appalach
 * @version 1.0
 * @final 
 */
class Admin_listvaluesController extends DR_Controllers_Admin
{
    public function indexAction()
    {
        $list_id = $this->_getParam('list_id', 0);
        $this->view->tables = array("Элементы списка" => array(
                "data_action" => "listvalues/blockdatatable",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array("handler" => "addListsObject()") ,DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("pos" => array("name" => "ID"), "value" => array("name" => "Значение", "filter" => new DR_Api_Admin_FilterString())),
                "params" => array("list_id" => $list_id)));
        parent::indexAction();
    }
    public function blockdatatableAction()
    {
        $this->_model->nextId();
        $this->isSelfViewTable = true;
        $this->_model->where('list_id', $this->_getParam('list_id', 0));
        parent::blockdatatableAction();
    }
    public function moveAction(){
        $first = $this->_model->where('id', $this->_getParam('first'))->row();
        $two = $this->_model->_new()->where('id', $this->_getParam('two'))->row();
        
        $temp = $first['pos'];
        $first['pos'] = $two['pos'];
        $two['pos'] = $temp;
        $first->save();
        $two->save();
        $this->_redirect("/admin/lists/edit/id/" . $this->_getParam('list'));
    }
    //сохранить значениЯ списка
    public function saveAction()
    {
        list($valid, $Data) = $this->isValid($_POST);

        if ($valid)
        {

            $values = array();
            if (isset($_POST['id']))
            {
                $values['value'] = $_POST['value'];
		if(isset($_POST['ru'])) {
		    $values['ru'] = 1;
		} else {
		    $values['ru'] = 0;
		}
                $id = $this->_model->doSave($values, $_POST['id']);
            } else
            {
                $list_values = str_replace("\r", '', explode("\n", $_POST['value']));
                $values = array("list_id" => $_POST['list_id']);

                foreach ($list_values as $value)
                {
                    $values['value'] = $value;
                    $this->_model->doSave($values);
                }

            }
        }
        $this->Response($Data, array('location.reload();'));
    }

    //удалить значениЯ списка
    public function deleteAction()
    {
        if ($id = $this->_getParam('id'))
        {
			$this->_model->cacheOff();
            $data = $this->_model->in("id", $id)->rows();

            foreach ($data as $row)
            {
                $list_id = $row['list_id'];
                $row->delete();
            }
            $this->_model->cacheOn();
            $location = "/admin/lists/edit/id/" . $list_id;
            if($this->_request->isPost()) {
                die($location);
            } else {
                $this->_redirect($location);
            }
            
        }

    }

}
