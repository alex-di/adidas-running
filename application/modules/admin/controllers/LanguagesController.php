<?php

/**
 * Админ контроллер для языков сайта
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_languagesController extends DR_Controllers_Admin {

	public function indexAction() {
		$this->view->tables = array(
				"Языки" => array("data_action" => "languages/blockdatatable", "is_page" => true,
						"is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "language" => array("name" => "Название"),
								"is_default" => array("name" => "По умолчанию"))));
		parent::indexAction();
	}
	public function blockdatatableAction() {
		$this->isSelfViewTable = true;
		parent::blockdatatableAction();
	}
	public function editAction() {
		if (!$this->_getParam('id')) {
			$id = $this->_model
					->doSave(
							array("language" => "Введите название нового язык",
									"geography_suffix" => Model_Country::GEOGRAPHY_SUFFIX_RU));
			$this->_redirect("/admin/languages/edit/id/" . $id);
		} else {
			parent::editAction();
			$form = new DR_Api_Admin_EditForm($this->view->data);
			$this->view->elements = array("name" => "Редактирование языка",
					"fields" => array("Название" => $form->stringInput('language'),
							"География" => $form
									->select('geography_suffix', Model_Country::getGeographySuffixDescription())));
		}

	}
	public function processsaveAction() {
		$this->_model->executeQuery("UPDATE Model_Languages SET is_default = 0");
		$this->_model->doSave(array("is_default" => 1), $_POST['id']);
		die;
	}

	public function savelistAction() {
		$model = api::_getModel(DR_Api_Models::LANGUAGE_LISTS);
		$model->doSave(array("value" => $_POST['value']), $_POST['id']);
		die("OK");
	}
}
