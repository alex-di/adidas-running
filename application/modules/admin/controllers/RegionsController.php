<?php

/**
 * Админ контроллер для регионов
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_regionsController extends DR_Controllers_Admin
{

    public function init()
    {
        $this->_model = api::getRegion();
    }
    public function indexAction()
    {
        $country = $this->_getParam('country_id', 0);
        $this->view->tables = array("Регионы" => array(
                "data_action" => "regions/blockdatatable",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action'=>'/admin/regions/edit/country/'.$country), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "region_name_ru" => array("name" => "Название по умолчанию", "filter" => new DR_Api_Admin_FilterString()),
                    "region_name_en" => array("name" => "Интернациональное название", "filter" => new DR_Api_Admin_FilterString())),
                "params" => array("country_id" => $country)));
        parent::indexAction();
    }
    public function blockdatatableAction()
    {
        $this->isSelfViewTable = true;
        $this->_model->where('country_id', $this->_getParam('country_id', 0));
        parent::blockdatatableAction();
    }
    public function editAction()
    {
        parent::editAction();
        $country = $this->_getParam('country', 0);
        if (!$this->view->data['id'])
        {
            $id = $this->_model->doSave(array("region_name_ru" => "Новый регион", "region_name_en" => "New regions", "country_id"=>$country));
            $this->_redirect("/admin/regions/edit/country/".$country."/id/" . $id);
        } else
        {
            $this->getBreadcrumbs()->appendView('cтраны')->append('Редактирование страны', '/admin/geography/edit/id/'.$country)->appendEdit('Редактирование региона');
            $form = new DR_Api_Admin_EditForm($this->view->data);
            $this->view->elements = array("name" => "Редактирование региона", "fields" => array("Название по умолчанию" => $form->stringInput('region_name_ru'), "Интернациональное название" => $form->stringInput('region_name_en')));
        }
    }

    //удалить значения региона
    public function deleteAction()
    {
        if ($id = $this->_getParam('id'))
        {
            $this->_model->doDelete($id);
            $this->_redirect("/admin/geography/edit/id/" . $this->_getParam('country_id'));
        } else
        {
            $this->showError("обьект для удаления не передан");
        }

    }
}
