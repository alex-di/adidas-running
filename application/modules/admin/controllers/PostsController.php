<?php

/**

 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_postsController extends DR_Controllers_Admin {
	const NAMESPACE_PARSE_DATA = 'namespace-parse-data';
    public function init() {
		$this->_model = api::getMaterials();
	}
	public function indexAction() {

        $this->getBreadcrumbs()->appendView();
        $settings = array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(array('title'=>'Twitter: получить новые посты', 'handler'=>'searchTwitterPosts()', 'class'=>'ic-database'),array('title'=>'Instagram: получить новые посты', 'handler'=>'searchInstagramPosts()', 'class'=>'ic-database'),DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"),
								"image" => array("name" => "Картинка", 'is_sort' => false),
								"name" => array("name" => "Текст", 'is_sort' => false),
								"category_id" => array("name" => "Категория",
										"filter" => new DR_Api_Admin_FilterList(
												array(Model_Materials::CATEGORY_TWITTER_POST => 'twitter',
														Model_Materials::CATEGORY_INSTAGRAM_POST => 'instagram'),
												't.category_id')),
								"is_modern" => array("name" => "Модерация",
										"filter" => new DR_Api_Admin_FilterList(
												array(Model_Materials::TYPE_NOT_MODERN => 'Не отмодерирован',
														Model_Materials::TYPE_MODERN => 'Отмодерирован',
                                                        Model_Materials::TYPE_CANCELED => 'Возвращено'),
												't.is_modern')),));
        $parserData = $this->getFromStore(self::NAMESPACE_PARSE_DATA);
        if(!is_null($parserData) && count($parserData)) {
            $this->setToStore(array(), self::NAMESPACE_PARSE_DATA);
            if($parserData['is_find']) {
                $message = 'Twitter: ';
                if(isset($parserData['inst']))
                    $message = 'Instagram: ';
                $message .= 'по хештегу "'.$parserData['name'].'" найдено '.$parserData['find'].' постов. Сохранено '.$parserData['save'].' постов';
                $settings['message'] = array(DR_Api_Admin_Table::MESSAGE_SUCCESS => $message);
            }
        }
        $this->view->tables = array("Посты соц. сетей" => $settings);
		parent::indexAction();
	}
	public function blockdatatableAction() {
		$this->isSelfViewTable = true;
		$this->_model
				->in('t.category_id',
						array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST))
				->joinLeft(array('meta' => api::META),
						"meta.resource_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS.' and meta.key="post_photo"',
						array('image'=>'meta.value'));
		parent::blockdatatableAction();
	}
	public function editAction() {
        $this->getBreadcrumbs()->appendView('Посты соц. сетей')->appendEdit();
		$this->view->data = Model_Materials::getFirst($this->_model->getMaterials(intval($this->_getParam('id', 0))));

		$form = new DR_Api_Admin_EditForm($this->view->data);
		$fields = array(
						$form->partial('posts/view.tpl', array('data' => $this->view->data)),
						"Отмодерирован" => $form->checkbox('is_modern', 1));
		if(isset($this->view->data['post_photo']))
			$fields['Встановить 4-х кратный размер блока'] = $form->checkbox('is_large_size', 1);
		else 
			$fields[] = $form->hidden('is_large_size', "", 0);
			
		$this->view->elements = array("name" => "Модерация поста",
				"fields" => $fields);

		$this->render("edit", null, true);
	}
	public function saveAction() {
		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			$newId = $this->_model->doSave($_POST, $this->_getParam('id'));
            $model_meta = api::getMeta();

			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE Model_Meta.resource_id = ' . $newId . ' AND Model_Meta.modules_id = '
									. Model_Meta::MATERIALS ." AND Model_Meta.key = 'is_large_size'");
			$model_meta
					->doSave(
							array('value' => $_POST['is_large_size'], 'key' => 'is_large_size',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
		}
		$this->Response($data);

	}
    
	public function twittersearchAction() {

		$data = Parser_Manager::execute(new Parser_Social_Twitter());
		$data['tw'] = true;
        $this->setToStore($data, self::NAMESPACE_PARSE_DATA);
        $this->_redirect('/admin/posts');
	}
	public function instagramsearchAction() {
		$data = Parser_Manager::execute(new Parser_Social_Instagram());
		$data['inst'] = true;
        $this->setToStore($data, self::NAMESPACE_PARSE_DATA);
        $this->_redirect('/admin/posts');
	}
    protected function setToStore($data, $namespace) {
        $session = new Zend_Session_Namespace($namespace);
        $session->data = $data;
    }
    protected function getFromStore($namespace) {
        $session = new Zend_Session_Namespace($namespace);
        if(isset($session->data))
            return $session->data;
        return null;
    }
}
