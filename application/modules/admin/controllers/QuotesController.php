<?php

/**
 * Админ контроллер для добавления заметок и цитат
 * @author appaalch
 * @version 1.0
 * @final 
 */
class Admin_quotesController extends DR_Controllers_Admin
{
    public function init()
    {
        $this->_model = api::getMaterials();
    }

    public function indexAction()
    {
        $this->getBreadcrumbs()->appendView();
        $this->view->tables = array("Заметки и цитаты" => array(
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),
                ));

        parent::indexAction();
    }

    public function blockdatatableAction()
    {
        $this->_model->where('category_id', Model_Materials::CATEGORY_QUETES);
        parent::blockdatatableAction();
    }

    public function editAction()
    {
        $this->getBreadcrumbs()->appendView('Заметки и цитаты')->appendEdit();
        //$this->view->data = api::getUsers()->where('id', $this->_getParam('id'))->joinLeft()->row();
        if ($id = $this->_getParam('id') or $id === 0)
        {
            $this->view->data = $this->_model->_new()->where('t.id', $id)->joinLeft(array('meta' => api::META), "meta.key = '" . Model_Materials::QUOTE_TEXT . "' and meta.resource_id = t.id and meta.modules_id = " . Model_Meta::MATERIALS, array(Model_Materials::QUOTE_TEXT => 'meta.value'))->joinLeft(array('meta2' => api::META), "meta2.key = '" . Model_Materials::QUOTE_AUTOR . "' and meta2.resource_id = t.id and meta2.modules_id = " . Model_Meta::MATERIALS, array(Model_Materials::QUOTE_AUTOR => 'meta2.value')) //->debug(true)
                ->row();


        } else
        {
            $this->view->data = $this->_model->createRow()->toArray();

            $this->view->data[Model_Materials::QUOTE_AUTOR] = '';
            $this->view->data[Model_Materials::QUOTE_TEXT] = '';
            //_d($this->view->data);
        }

        $form = new DR_Api_Admin_EditForm($this->view->data);
        $this->view->elements = array("name" => "Редактирование известной личности", "fields" => array(
                "Название" => $form->stringInput('name'),
                "Текст" => $form->textarea(Model_Materials::QUOTE_TEXT),
                "Автор" => $form->stringInput(Model_Materials::QUOTE_AUTOR),
                ));

        $this->render("edit", null, true);
    }


    # сохранить известного
    public function saveAction()
    {

        $_POST['category_id'] = Model_Materials::CATEGORY_QUETES;

        $meta = array();
        if (isset($_POST[Model_Materials::QUOTE_AUTOR]))
        {
            $meta[Model_Materials::QUOTE_AUTOR] = $_POST[Model_Materials::QUOTE_AUTOR];
            unset($_POST[Model_Materials::QUOTE_AUTOR]);
        }
        if (isset($_POST[Model_Materials::QUOTE_TEXT]))
        {
            $meta[Model_Materials::QUOTE_TEXT] = $_POST[Model_Materials::QUOTE_TEXT];
            unset($_POST[Model_Materials::QUOTE_TEXT]);
        }

        list($valid, $data) = $this->isValid($_POST);

        /**
         * Если все проверки пройдены, сохраняем
         */
        if ($valid)
        {
            $_POST['users_id'] = Zend_Auth::getInstance()->getIdentity()->id;
            $_POST['is_modern'] = 1;
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));

            # Сохраняем данные
            if (count($meta))
            {
                $meta_model = api::getMeta();

                if (isset($meta[Model_Materials::QUOTE_AUTOR]))
                {

                    $m_data = $meta_model->where('t.resource_id', $newId)->where('t.key', Model_Materials::QUOTE_AUTOR)->row();
                    if (count($m_data))
                    {
                        $m_data->value = $meta[Model_Materials::QUOTE_AUTOR];
                        $m_data->save();
                    } else
                    {
                        $meta_model->doSave(array(
                            'resource_id' => $newId,
                            'key' => Model_Materials::QUOTE_AUTOR,
                            'value' => $meta[Model_Materials::QUOTE_AUTOR],
                            'modules_id' => Model_Meta::MATERIALS));
                    }
                }
                if (isset($meta[Model_Materials::QUOTE_TEXT]))
                {

                    $m_data = $meta_model->where('t.resource_id', $newId)->where('t.key', Model_Materials::QUOTE_TEXT)->row();
                    if (count($m_data))
                    {
                        $m_data->text_value = $meta[Model_Materials::QUOTE_TEXT];
                        $m_data->save();
                    } else
                    {
                        $meta_model->doSave(array(
                            'resource_id' => $newId,
                            'key' => Model_Materials::QUOTE_TEXT,
                            'text_value' => $meta[Model_Materials::QUOTE_TEXT],
                            'modules_id' => Model_Meta::MATERIALS));
                    }
                }
            }

            if (!is_null($this->location))
            {
                $this->_redirectAjaxAction($this->location, $this->message);
            }

            if (!is_null($this->locationAtNew))
            {
                $this->_redirectAjaxAction($this->locationAtNew . $newId, $this->message);
            }
        }

        $this->Response($data);
    }


}
