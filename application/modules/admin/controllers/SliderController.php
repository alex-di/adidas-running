<?php

/**
 * Админ контроллер для слайдеров
 * @author appalach
 * @version 1.0
 */
class Admin_sliderController extends DR_Controllers_Admin
{

    public function indexAction()
    {
        $this->getBreadcrumbs()->appendView();
        $this->view->tables = array("Слайдеры" => array(
                "is_page" => true,

                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название", "key" => array("name" => "Ключ")))));
        parent::indexAction();
    }

    public function editAction()
    {
        parent::editAction();
        if (!$this->view->data['id'])
        {
            $id = $this->_model->doSave(array("name" => "New Slider", "key" => "new"));
            $this->_redirect("/admin/slider/edit/id/" . $id);
        }
        $this->getBreadcrumbs()->appendView('Слайдеры')->appendEdit();
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array("Название" => $form->stringInput('name'), "Ключ" => $form->stringInput('key'));

        $this->view->elements = array("name" => "Редактирование банера", "fields" => $fields);

    }

}
