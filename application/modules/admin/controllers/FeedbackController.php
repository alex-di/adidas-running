<?php

/**
 * Админ контроллер для обратной связи
 * @author rizo
 * @version 1.0
 * @final 
 */
class Admin_feedbackController extends DR_Controllers_Admin {

	public function indexAction() {
		$this->view->tables = array(
				"Обратная связь" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Имя пользователя"),
								"theme" => array("name" => "Тема"), "date" => array("name" => "Дата отправления"),
								"complited" => array("name" => "Статус"))));
		parent::indexAction();

	}
	public function blockdatatableAction() {
		$this->isSelfViewTable = true;
		parent::blockdatatableAction();
	}

	public function viewAction() {
		$this->view->data = api::getFeedback()->_new()->where("t.id", $this->_getParam("id"))->row();
	}

	public function saveAction() {

		$id = $this->_getParam("id");

		if (isset($_POST['answer']) && (strip_tags($_POST['answer']) != "")) {
			$values = array('title' => $this->view->settings['title_email_of_new_message'],
					'email' => $_POST['answeremail'], 'email_callback' => $this->view->settings['email_callback'],
					'message' => str_replace(array('{data}',), array($_POST['answer']),
							$this->view->settings['email_of_new_message']));
			DR_Api_Tools_Tools::send($values);

			api::_getModel(DR_Api_Models::FEEDBACK)
					->doSave(array("complited" => $_POST['complited'], "answer" => $_POST['answer']),
							$this->_getParam('id'));

			parent::saveAction();

		} else {
			$_POST = array("complited" => $_POST['complited']);
			$this->getValidator()->setNotRequiredFields(array("answer"));
			parent::saveAction();
		}

	}

}
