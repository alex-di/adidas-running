<?php

/**
 * Админ контроллер для статически страниц
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_pagesController extends DR_Controllers_Admin {

	public function indexAction() {
        
        $this->getBreadcrumbs()->appendView();
        
		$this->view->tables = array(
				"Статические страницы" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название"))));
		parent::indexAction();
	}

	public function editAction() {
		parent::editAction();
        $this->getBreadcrumbs()->appendView('Статические страницы')->appendEdit();
        
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$fields = array("Название" => $form->stringInput('name'), "Текст" => $form->tiny('text', '', '', array('image_module'=>'banners')),
				"Ссылка на страницу" => $form->stringInput('stitle'));

		$this->view->elements = array("name" => "Редактирование страницы", "fields" => $fields);
		$this->render("edit", null, true);
	}
	public function saveAction() {
		if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
			$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
		}

		$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
		parent::saveAction();
	}
}
