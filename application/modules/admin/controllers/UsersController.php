<?php

/**
 * Админ контроллер для юзеров
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_usersController extends DR_Controllers_Admin
{

    # все пользователи
    public function indexAction()
    {
        $this->getBreadcrumbs()->appendView();
        $groups = api::getGroups()->forSelect(array("id" => "title"));
        $this->view->tables = array("Пользователи" => array(
                "data_action" => "users/blockdatatable",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "email" => array("name" => "Email"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    "group_name" => array("name" => "Группа", "filter" => new DR_Api_Admin_FilterList($groups, 't.group_id')))));
        parent::indexAction();
    }


    public function blockdatatableAction()
    {
        $this->_model->fields(array("*"))
            //->where('t.group_id', Model_Groups::RUNBASE_UNREGISTER_CARD_MASTER, '!=')
            ->joinLeft(array("gr" => api::GROUPS), "gr.id = t.group_id ", array("group_name" => "gr.title"));
        parent::blockdatatableAction();
    }
    
    
    /**
     * Редактировать пользователя
     */
    public function editAction()
    {
        $this->getBreadcrumbs()->appendView('Пользователи')->appendEdit();
        parent::editAction();
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $this->view->data['password'] = '';
        
        $this->view->elements = array("name" => "Редактирование пользователя", "fields" => array(
                "Пароль" => $form->password('password'),
                "Email" => $form->stringInput('email'),
                "Имя" => $form->stringInput('name'),
                "Фамилия" => $form->stringInput('sname'),
                "Група" => $form->select("group_id", api::getGroups()->forSelect(array("id" => "title")))));
        $this->render("edit", null, true);
    }

    public function saveAction()
    {
        if($this->_getParam('id') && empty($_POST['password'])) {
            unset($_POST['password']);
        }
        $this->getValidator()->setElement('email')->email();
        list($valid, $data) = $this->isValid($_POST);
        
        if ($valid) {
            if(isset($_POST['password']))
                $_POST['password'] = Model_Users::cryptoPassword($_POST['password']);
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));
            $this->_redirectAjaxAction('/admin/users', $this->message);
        }
        $this->Response($data);
    }
    public function eventusersAction()
    {
        $events_id = $this->_getParam('events_id', 0);
        $this->view->tables = array("Зарегистрированные пользователи" => array(
                "data_action" => "users/blockdataeventuser",
                "is_page" => true,
                "is_mass_check" => false,
                "is_option_coll" => false,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER => array('title' => 'Скачать excel', 'action' => '/admin/users/download/events_id/' . $events_id)),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "email" => array("name" => "Email"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    "bday" => array("name" => "Дата рождения"),
                    "city" => array("name" => "Город"),
                    "sex" => array("name" => "Пол"),
                    "fb_username" => array("name" => "Facebook"),
                    "vk_username" => array("name" => "Vk"),
                    "tw_username" => array("name" => "Twitter")),
                "params"=>array('events_id'=>$events_id)));
        parent::indexAction();
    }


    public function blockdataeventuserAction()
    {
        $this->isSelfViewTable = true;
        $events_id = intval($this->_getParam('events_id', 0));
        $this->_model->where("t.id IN (SELECT object_id FROM Model_References WHERE reference_id = $events_id AND ref_type = ".Model_References::REF_TYPE_USER_EVENTS.')')
                    ->joinLeft(array('meta1' => api::META), 'meta1.resource_id = t.id and meta1.modules_id = '.Model_Meta::USERS." and meta1.key = 'fb_username'", array('fb_username'=>'meta1.value'))
                    ->joinLeft(array('meta2' => api::META), 'meta2.resource_id = t.id and meta2.modules_id = '.Model_Meta::USERS." and meta2.key = 'vk_username'", array('vk_username'=>'meta2.value'))
                    ->joinLeft(array('meta3' => api::META), 'meta3.resource_id = t.id and meta3.modules_id = '.Model_Meta::USERS." and meta3.key = 'tw_username'", array('tw_username'=>'meta3.value'));
        parent::blockdatatableAction();
    }
    
    public function downloadAction() {
        $events_id = intval($this->_getParam('events_id', 0));
        $data = $this->_model
                ->where("t.id IN (SELECT object_id FROM Model_References WHERE reference_id = $events_id AND ref_type = ".Model_References::REF_TYPE_USER_EVENTS.')')
                    ->joinLeft(array('meta1' => api::META), 'meta1.resource_id = t.id and meta1.modules_id = '.Model_Meta::USERS." and meta1.key = 'fb_username'", array('fb_username'=>'meta1.value'))
                    ->joinLeft(array('meta2' => api::META), 'meta2.resource_id = t.id and meta2.modules_id = '.Model_Meta::USERS." and meta2.key = 'vk_username'", array('vk_username'=>'meta2.value'))
                    ->joinLeft(array('meta3' => api::META), 'meta3.resource_id = t.id and meta3.modules_id = '.Model_Meta::USERS." and meta3.key = 'tw_username'", array('tw_username'=>'meta3.value'))
                    ->rows();
        
                    //->debug(true)
        $excel = new DR_Api_Tools_Excel;
        
        $excel->setHeaders(array("email", "Имя", "Фамилия", "Дата рождения", "Город", "Пол", "Fb id", "Vk id", "Tw id"));
        //_d($data);
        $result = array();
        
        foreach($data as $row) {
            $result[] = array(
                $row->email,
                $row->name,
                $row->sname,
                date("Y-m-d", strtotime($row->bday)),
                $row->city,
                $row->sex == 'men' ? 'Мужчина' : 'Женщина',
                $row->fb_id != 0 ? $row->fb_id : "",
                $row->vk_id != 0 ? $row->vk_id : "",
                $row->tw_id != 0 ? $row->tw_id : ""
            );
        }
        
        
        $excel->write($result);
        die;
        
    }
    
    ################ Известные личности Старт ####################
    
    
    #Знаменитые личности
    public function famousAction()
    {
        $this->getBreadcrumbs()->appendView();
        $this->view->tables = array("Известные личности" => array(
                "data_action" => "users/blockdatatablefamous",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action' => '/admin/users/editfamous'), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    )));
        parent::indexAction();
    }


    public function blockdatatablefamousAction()
    {
        $this->view->editlink = '/admin/users/editfamous';
        $this->_model->fields(array("*"))->where('t.group_id', Model_Groups::FAMOUS);
        parent::blockdatatableAction();
    }
    
    
    /**
     * Редактировать известного пользователя
     */
    public function editfamousAction()
    {
        parent::editAction();
        $this->getBreadcrumbs()->appendView('Известные личности')->appendEdit();

        $this->view->path = '/admin/users/savefamous';
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $this->view->elements = array("name" => "Редактирование известной личности", "fields" => array(
                "Имя" => $form->stringInput('name'),
                "Фамилия" => $form->stringInput('sname'),
                "Фотография" => $form->upload('avatar', 'users'),

                ));

        $this->render("edit", null, true);
    }

    # сохранить известного

    public function savefamousAction()
    {


        $_POST['group_id'] = Model_Groups::FAMOUS;


        list($valid, $data) = $this->isValid($_POST);


        if ($valid)
        {
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));


            if (!is_null($this->location))
            {
                $this->_redirectAjaxAction($this->location, $this->message);
            }

            if (!is_null($this->locationAtNew))
            {
                $this->_redirectAjaxAction($this->locationAtNew . $newId, $this->message);
            }
        }

        $this->Response($data);
    }

    ################ Известные личности СТОП 
    
    
    
    ################ Тренера Старт ####################
    
    
    #Знаменитые личности
    public function coachAction()
    {
        $this->getBreadcrumbs()->appendView();
        $this->view->tables = array("Известные личности" => array(
                "data_action" => "users/blockdatatablecoach",
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action' => '/admin/users/editcoach'), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    )));
        parent::indexAction();
    }


    public function blockdatatablecoachAction()
    {
        $this->view->editlink = '/admin/users/editcoach';
        $this->_model->fields(array("*"))->where('t.group_id', Model_Groups::COACH);
        parent::blockdatatableAction();
    }
    
    
    /**
     * Редактировать известного пользователя
     */
    public function editcoachAction()
    {
        parent::editAction();
        $this->getBreadcrumbs()->appendView('Известные личности')->appendEdit();

        $this->view->path = '/admin/users/savecoach';
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $this->view->elements = array("name" => "Редактирование известной личности", "fields" => array(
                "Имя" => $form->stringInput('name'),
                "Фамилия" => $form->stringInput('sname'),
                //"Фотография" => $form->upload('avatar', 'users'),

                ));

        $this->render("edit", null, true);
    }

    # сохранить известного

    public function savecoachAction()
    {


        $_POST['group_id'] = Model_Groups::COACH;


        list($valid, $data) = $this->isValid($_POST);


        if ($valid)
        {
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));


            if (!is_null($this->location))
            {
                $this->_redirectAjaxAction($this->location, $this->message);
            }

            if (!is_null($this->locationAtNew))
            {
                $this->_redirectAjaxAction($this->locationAtNew . $newId, $this->message);
            }
        }

        $this->Response($data);
    }

    ################ Известные личности СТОП ####################
    
    ################ Записи на Runbase ##########################
    public function runbaseeventusersAction()
    {
        $events_id = $this->_getParam('events_id', 0);
        $this->view->tables = array("Зарегистрированные пользователи" => array(
                "data_action" => "users/blockdatarunbaseuser",
                "is_page" => true,
                "is_mass_check" => false,
                "is_option_coll" => false,
                //"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER => array('title' => 'Скачать excel', 'action' => '/admin/users/download/events_id/' . $events_id)),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "email" => array("name" => "Email"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    "bday" => array("name" => "Дата рождения"),
                    "city" => array("name" => "Город"),
                    "sex" => array("name" => "Пол"),
                    "fb_username" => array("name" => "Facebook"),
                    "vk_username" => array("name" => "Vk"),
                    "tw_username" => array("name" => "Twitter")),
                "params"=>array('events_id'=>$events_id)));
        parent::indexAction();
    }


    public function blockdatarunbaseuserAction()
    {
        $this->isSelfViewTable = true;
        $events_id = intval($this->_getParam('events_id', 0));
        $this->_model->where("t.id IN (SELECT reference_id FROM Model_References WHERE object_id = $events_id AND ref_type = ".Model_References::REF_TYPE_RUNBASE.')')
                    ->joinLeft(array('meta1' => api::META), 'meta1.resource_id = t.id and meta1.modules_id = '.Model_Meta::USERS." and meta1.key = 'fb_username'", array('fb_username'=>'meta1.value'))
                    ->joinLeft(array('meta2' => api::META), 'meta2.resource_id = t.id and meta2.modules_id = '.Model_Meta::USERS." and meta2.key = 'vk_username'", array('vk_username'=>'meta2.value'))
                    ->joinLeft(array('meta3' => api::META), 'meta3.resource_id = t.id and meta3.modules_id = '.Model_Meta::USERS." and meta3.key = 'tw_username'", array('tw_username'=>'meta3.value'));
        parent::blockdatatableAction();
    }
    ########################################################################
    
}
