<?php

/**
 * Контроллер индекса для главной модуля стран
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_geographyController extends DR_Controllers_Admin {
	public function init() {
		$this->_model = api::getCountry();
	}
	public function indexAction() {
	    $this->getBreadcrumbs()->appendView('cтраны');
		$this->view->tables = array(
				"Страны" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"),
								"country_name_ru" => array("name" => "Название по умолчанию",
										"filter" => new DR_Api_Admin_FilterString()),
								"country_name_en" => array("name" => "Интернациональное название",
										"filter" => new DR_Api_Admin_FilterString()))));
		parent::indexAction();
	}
	public function editAction() {
	   $this->getBreadcrumbs()->appendView('cтраны')->appendEdit('Редактирование страны');
		parent::editAction();
		if (!$this->view->data['id']) {
			$id = $this->_model->doSave(array("country_name_ru" => "Новая страна", "country_name_en" => "New country"));
			$this->_redirect("/admin/geography/edit/id/" . $id);
		} else {
			$form = new DR_Api_Admin_EditForm($this->view->data);
			$this->view->elements = array("name" => "Редактирование страны",
					"fields" => array("Название по умолчанию" => $form->stringInput('country_name_ru'),
							"Интернациональное название" => $form->stringInput('country_name_en')));
		}
	}

}
