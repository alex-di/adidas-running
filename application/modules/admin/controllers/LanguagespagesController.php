<?php

/**
 * Админ контроллер для редактирования языков сайта
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_languagespagesController extends DR_Controllers_Admin
{

    public function indexAction()
    {
        $this->view->tables = array("Статические страницы" => array(
                "data_action" => "languagespages/blockdatatable",
                "is_page" => true,
                "is_mass_check" => false,
                "is_option_coll" => true,
                "fields" => array(
                    "name_def" => array("name" => "По умолчанию"),
                    "name" => array("name" => "Перевод")),
                "params"=>array("languages"=>$this->_getParam("languages"))));
        parent::indexAction();
    }
    public function blockdatatableAction()
    {
        $this->isSelfViewTable = true;
        $input_data = $this->_request->getParams();
        //_d($input_data);
        $this->_model = api::getPages();
        
        $this->_model->fields(array("name_def"=>"t.name", "id_def"=>"t.id", "lp.*"))
                     ->joinLeft(array("lp" => api::LANGUAGE_PAGES), "lp.pages_id = t.id and lp.language_id=".$input_data['languages']);
        $this->view->language = $input_data['languages'];
        parent::blockdatatableAction();
    }
    
    public function editAction()
    {
        parent::editAction();
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array(
            "Название" => $form->stringInput('name'),
            "Текст" => $form->tiny('text'));
        if(!$this->_getParam("id")) {
            $fields[] = $form->hidden("pages_id", "" , $this->_getParam("page"));
            $fields[] = $form->hidden("language_id", "" , $this->_getParam("language"));
        } else {
            $fields[] = $form->hidden("language_id");
        }
        $this->view->elements = array("name" => "Перевод статической страницы", "fields" => $fields);
        $this->render("edit", null, true);
    }
    
    public function saveAction(){
        $this->location = "/admin/languages/edit/id/".$_POST['language_id'];
        $this->message = "Успешно сохранено";
        parent::saveAction();
    }

}
