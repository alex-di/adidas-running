<?php

/**
 * Админ контроллер для баннеров
 * @author appalach
 * @version 1.0
 */
class Admin_bannersController extends DR_Controllers_Admin
{

    public function indexAction()
    {
        //$this->_model = api::getBanners();
        $this->getBreadcrumbs()->appendView();
        $slider_id = $this->_getParam('slider_id', 0);
        if ($slider_id != 0)
        {
            $action = '/admin/banners/edit/slider_id/' . $slider_id;
        } else
        {
            $action = '/admin/banners/edit';
        }

        $this->view->tables = array("Банеры" => array(
                "is_page" => true,
                "data_action" => "banners/blockdatatable",
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array('action' => $action), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),
                "params" => array("slider_id" => $slider_id)));

        parent::indexAction();
    }

    public function blockdatatableAction()
    {
        if ($slider_id = $this->_getParam('slider_id'))
        {
            $this->_model->where('slider_id', $this->_getParam('slider_id'));
            $this->_model->fields(array("*"))->where('t.type', Model_Banners::TYPE_SLIDER);
            $this->view->editlink = '/admin/banners/edit/slider_id/' . $slider_id;
            $this->view->deletelink = '/admin/banners/delete';

        } else
        {
            $this->_model->fields(array("*"))->where('t.type', Model_Banners::TYPE_BANNER);
        }
        parent::blockdatatableAction();
    }

    public function editAction()
    {
        parent::editAction();
        
        $colors = array(
            'yellow' => 'Желтый',
            'black' => 'Черный',
            'green' => 'Зеленый',
            );

        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array(
            "Название, возможные BB-code для задания цвета текста - [yellow] текст [/yellow], [black] текст [/black] " => $form->textarea('name'),
            "Банер" => $form->upload('file', 'banners'),
            "Идентификатор банера(необязательно)" => $form->stringInput('stitle'),
            'Цвет баннера' => $form->select('color', $colors),
            'Ссылка' => $form->stringInput('link'));
        if ($slider_id = $this->_getParam('slider_id'))
        {
            $this->getBreadcrumbs()->append('Слайдеры', 'slider')->append('Редактирование','slider/edit/id/'.$slider_id)->appendEdit('Редактирование баннера');
            $fields[] = $form->hidden('slider_id', 'slider_id', $slider_id);
        } else {
            $this->getBreadcrumbs()->appendView('Банеры')->appendEdit();
        }
        $this->view->elements = array("name" => "Редактирование банера", "fields" => $fields);
        $this->render("edit", null, true);
    }
    public function saveAction()
    {
        if (isset($_POST['slider_id']))
        {
            $this->location = '/admin/slider/edit/id/' . $_POST['slider_id'];
        }
        if (!isset($_POST['stitle']) || empty($_POST['stitle']))
        {
            $_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
        }

        if (isset($_POST['slider_id']))
        {
            $_POST['type'] = Model_Banners::TYPE_SLIDER;
        }

        $_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
        parent::saveAction();
    }

    public function deleteAction()
    {
        $this->location = $_SERVER['HTTP_REFERER'];
        parent::deleteAction();
    }
}
