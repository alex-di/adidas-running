<?php

/**
 * Админ контроллер для груп
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_groupsController extends DR_Controllers_Admin {
	public function indexAction() {
	    $this->getBreadcrumbs()->appendView();
		$parent_id = $this->_getParam('parent', 0);
		$this->view->tables = array(
				"Группы пользователей" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(
								DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(
										'action' => '/admin/groups/edit/parent/' . $parent_id)),
						"fields" => array("id" => array("name" => "ID"), "title" => array("name" => "Название")),
						"params" => array("parent" => $parent_id)));
		parent::indexAction();
	}

	public function blockdatatableAction() {
		$this->isSelfViewTable = true;
		if ($this->_getParam('parent') == 0)
			$this->_model->where('parent_id is null');
		else
			$this->_model->where('parent_id', $this->_getParam('parent'));
		parent::blockdatatableAction();
	}

	public function editAction() {
		parent::editAction();
        $this->getBreadcrumbs()->appendView('Группы пользователей')->appendEdit();
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование группы",
				"fields" => array("Название" => $form->stringInput('title')));
	}
	public function saveAction() {
		if (!$this->_getParam('id')) {
			if ($this->_getParam('parent') != 0)
				$_POST['parent_id'] = $this->_getParam('parent');
		}
		parent::saveAction();
	}
}
