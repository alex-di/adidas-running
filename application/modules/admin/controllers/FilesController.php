<?php

/**
 * 
 */
class Admin_filesController extends DR_Controllers_Admin
{

    public function tinymanagerAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $this->view->image_module = $this->_getParam('image_module');
        $this->view->savePath = 'tiny';
        $users_id = Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->id : 0;
        $root = $_SERVER['DOCUMENT_ROOT'];
        $path = '/data/' . $this->view->savePath . '/' . $this->view->image_module . '/' . $users_id;
        $this->view->data = array();

        $scandir = $root . $path;
        //_d($scandir);
        if (file_exists($scandir))
        {
            $data = scandir($scandir);
            if (count($data))
            {
                foreach ($data as $file_name)
                {
                    if ('.' != $file_name && '..' != $file_name && is_file($root . $path . '/' . $file_name))
                    {
                        $this->view->data[] = array('name' => $file_name, 'path' => $path . '/' . $file_name);
                    }
                }
            }
        }

    }
}
