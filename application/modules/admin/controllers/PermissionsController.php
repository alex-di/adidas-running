<?php

/**
 * @author appalach
 * @version 1.0
 * @final 
 */
class Admin_permissionsController extends DR_Controllers_Admin {
    
    // обновить ресурсы
    public function generateAction(){
        $data = DR_Api_Tools_Aclresource::getResource();
      
        api::getAclResources()->addResource($data);
        $this->view->data = $data;
    }
    // настройка прав доступа для групы
    public function groupAction(){
        $id = $this->_getParam("id");
        
        if(!(!empty($id) || $id==0))
            $this->_redirect("/admin");
        $this->view->group = api::getGroups()->_new()->where("id" ,$this->_getParam("id"))->row();
        if(!count($this->view->group))
            $this->_redirect("/admin");
        $this->getBreadcrumbs()->append('Группы пользователей', 'froups')->appendEdit('Настройка ACL');
        $this->view->data = api::getAclResources()->getResource($this->view->group->id);
    }
    
    public function proccessparentpermissionsAction(){
        $data = api::getAclResources()->getResource(intval($this->_getParam("id")));
        die(json_encode($data['allowed']));
    }
    
    public function saveAction(){
        api::getPermissions()->savePermissions(isset($_POST['allowed']) ? $_POST['allowed'] : array(), $this->_getParam("id"));
        die("OK");
    }
    
}