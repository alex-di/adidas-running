<?php

/**
 * Админ контроллер для настроек сайта
 * @author appalach
 * @version 1.0
 * @final 
 */
class Admin_listsController extends DR_Controllers_Admin {

	public function indexAction() {
		$this->view->tables = array(
				"Списки" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"),
								"name" => array("name" => "Internal value", "filter" => new DR_Api_Admin_FilterString()),
								"comment" => array("name" => "Описание"))));
		parent::indexAction();
	}

	public function editAction() {
		parent::editAction();
		if (!$this->view->data['id']) {
			$id = $this->_model->doSave(array("name" => "New List", "comment" => "Описание"));
			$this->_redirect("/admin/lists/edit/id/" . $id);
		} else {
			$form = new DR_Api_Admin_EditForm($this->view->data);
			$this->view->elements = array("name" => "Редактирование списка",
					"fields" => array("Название" => $form->stringInput('name'),
							"Коментарий" => $form->stringInput('comment'),
							"Список участвует в переводе" => $form->checkbox('is_translate', 1)));
		}
	}
}
