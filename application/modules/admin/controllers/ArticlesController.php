<?php

/**
 * Админ контроллер для добавления статей
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_articlesController extends DR_Controllers_Admin {
	public function init() {
		$this->_model = api::getMaterials();
	}
	public function indexAction() {
		$this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Статьи" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), 
                                          "name" => array("name" => "Название", "filter" => new DR_Api_Admin_FilterString()), 
                                          "category_name" => array("name" => "Категория")),));

		parent::indexAction();
	}

	public function blockdatatableAction() {
		$this->_model
        ->where('t.is_unique', 0)
		->joinLeft(array('mt' => api::META), 'mt.key = "is_profile_material" and t.id = mt.resource_id', array())
                ->where('mt.id IS NULL')
		->joinLeft(array('lv'=>'Model_Listvalues'), 'lv.id = t.category_id', array('category_name'=>'lv.value'))
				->in('category_id',
						array(Model_Materials::CATEGORY_COACH_ARTICLES, Model_Materials::CATEGORY_INSPIRING_ARTICLES,
								Model_Materials::CATEGORY_ROUTE_ARTICLES, Model_Materials::CATEGORY_MICOACH_ARTICLES, Model_Materials::CATEGORY_SHOP_REVIEW));

		parent::blockdatatableAction();
		//_d($this->view->data);
	}
	
	
	public function moderateAction() {
		$this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Модерация статей и событий" => array("is_page" => true, 
                                                        "data_action" => "articles/moderatetable", 
                                                        "is_mass_check" => false, 
                                                        "is_option_coll" => true,
                                                        "fields" => array("id" => array("name" => "ID"), 
                                                                        "name" => array("name" => "Название"),
                                                                        "username" => array("name" => "Пользователь"), 
                                                                        "category_name" => array("name" => "Категория"),
                                                                        "is_modern" => array('name'=>'Статус')),));

		parent::indexAction();
	}
	
	public function moderatetableAction() {
		$this->isSelfViewTable = true;
		$this->_model
        ->joinLeft(array('us'=>'Model_Users'), 'us.id = t.users_id', array('username'=>"concat(us.name,' ', us.sname)"))
		->joinLeft(array('mt' => api::META), 'mt.key = "is_profile_material" and t.id = mt.resource_id', array())
                ->where('mt.id IS NOT NULL')
		->joinLeft(array('lv'=>'Model_Listvalues'), 'lv.id = t.category_id', array('category_name'=>'lv.value'))
				->in('category_id',
						array(Model_Materials::CATEGORY_COACH_ARTICLES, Model_Materials::CATEGORY_INSPIRING_ARTICLES, Model_Materials::CATEGORY_EVENTS,
								Model_Materials::CATEGORY_ROUTE_ARTICLES, Model_Materials::CATEGORY_MICOACH_ARTICLES, Model_Materials::CATEGORY_SHOP_REVIEW, Model_Materials::CATEGORY_ROUTERS));
		parent::blockdatatableAction();
		//_d($this->view->data);
	}
	
	public function processmodernAction(){
		
        if(isset($_POST['id']) && is_array($_POST['id']) && count($_POST['id'])) {
            foreach($_POST['id'] as $id) {
                $material = $this->_model->_new()->where('t.id', $id)->row();
                $this->_model->doSave(array('is_modern' => Model_Materials::TYPE_MODERN), $id);
                Model_Events::modernArticleEvent($material->id, $material->users_id, Zend_Auth::getInstance()->getIdentity()->id, Model_Materials::TYPE_MODERN);
            }
        }
		die('OK');
	}
	
	public function processunmodernAction(){
		
        if(isset($_POST['id']) && is_array($_POST['id']) && count($_POST['id'])) {
        foreach($_POST['id'] as $id) {
            $material = $this->_model->_new()->where('t.id', $id)->row();
            $this->_model->doSave(array('is_modern' => Model_Materials::TYPE_CANCELED), $id);
            Model_Events::modernArticleEvent($material->id, $material->users_id, Zend_Auth::getInstance()->getIdentity()->id, Model_Materials::TYPE_CANCELED);
        }
}
		die('OK');
	}
    public function deleteuserarticleAction(){
        $id = intval($this->_getParam('id'));
        $materialsEventType = array(Model_Events::TYPE_COMMENT_MATERIALS, Model_Events::TYPE_LIKE_MATERIALS, Model_Events::TYPE_MODERN_MATERIALS, Model_Events::TYPE_NOT_MODERN_MATERIALS, Model_Events::TYPE_RUN_MATERIALS);
        $this->_model->executeQuery('DELETE FROM Model_Events WHERE resource_id = ' . $id . ' AND type IN ('.implode(', ', $materialsEventType).')');
        $this->_model->doDelete($id);
        $this->_redirect('/admin/articles/moderate');
    }
    public function useditAction(){
         $this->getBreadcrumbs()->appendView('Модерация статьи')->appendEdit();
  		 $keywords = '';
		 $this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
 		 if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
		}
        $form = new DR_Api_Admin_EditForm($this->view->data);
        $fields = array(
                    "Название статьи" => $form->stringInput('name'),
    				"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
    				"Краткое описание" => $form->textarea('short_text'),
                );
        if(Model_Materials::CATEGORY_ROUTERS != $this->view->data['category_id'])
            $fields['Картинка'] = $form->upload('title_image', 'materials');
        $fields['Текст'] = $form->tiny('material_body', '', '', array('image_module'=>'materials'));
        
		$this->view->elements = array("name" => "Редактирование статьи",
		      "fields" => $fields,
              "toolbars"=>array("Предпросмотр"=>"previewArticle()"));
        $this->view->path = '/admin/articles/ussave';
    }
    public function ussaveAction(){

		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
            $post = $_POST;
			$newId = $this->_model->doSave($post, $this->_getParam('id'));
			api::getKeywords()->saveMaterialKeywords($post['keywords'], $newId);
            $model_meta = api::getMeta();
            
			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE `resource_id` = ' . $newId . ' AND `modules_id` = '
									. Model_Meta::MATERIALS. " AND `key` IN ('short_text', 'material_body')");
			$model_meta
					->doSave(
							array('text_value' => $post['short_text'], 'key' => 'short_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $post['material_body'], 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));
            if($this->_hasParam('needPreview'))
                $this->_redirectAjaxAction('/article/'.$_POST['stitle']);
            else
                $this->_redirectAjaxAction('/admin/articles/moderate');
		}
		$this->Response($data);
    }
	public function editAction() {
        $this->getBreadcrumbs()->appendView('Статьи')->appendEdit();
		$keywords = '';
		$this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
		//_d($this->view->data);
        $similar = array();
        $reviewItems = array();
		if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
			$this->view->data['keywords'] = implode(', ', array_values($this->view->data['tags']));
            $similar = $this->_model->getSimilarMaterials($this->view->data['id']);
            $reviewItems = $this->_model->getReviewItems($this->view->data['id']);
		}
        $category_list = array(Model_Materials::CATEGORY_COACH_ARTICLES => "статья тренера",
												Model_Materials::CATEGORY_INSPIRING_ARTICLES => "вдохновляющая статья",
												Model_Materials::CATEGORY_ROUTE_ARTICLES => "статья о маршрутах",
                                                Model_Materials::CATEGORY_MICOACH_ARTICLES => "статья для micoach",
                                                Model_Materials::CATEGORY_SHOP_REVIEW => "обзор товаров");
		$user_list = api::getUsers()->_new()->fields(array('username' => "concat(t.name, ' ', t.sname)"))
				->in('t.group_id', array(Model_Groups::FAMOUS, Model_Groups::ADMIN))->forSelect(array('id' => 'username'));
		$form = new DR_Api_Admin_EditForm($this->view->data);
		$this->view->elements = array("name" => "Редактирование статьи",
				"fields" => array(
                        $form->partial('articles/link.tpl', array('data' => $this->view->data, 'routerName'=>'article_view')),
                        "Название статьи" => $form->stringInput('name'),
						$form->partial('articles/category.tpl', array('data' => $this->view->data, 'category'=>$category_list, 'items'=>$reviewItems)),
                        /*"Категория" => $form
								->select('category_id', $category_list),*/
                        "Скрыть на сайте" => $form->checkbox('is_modern', 0, '', 1),
						"Ключевые слова(разделитель - запятая)" => $form->stringInput('keywords'),
						"Пользователь" => $form->select('users_id', $user_list),
                        "Картинка" => $form->upload('title_image', 'materials'),
						 $form->partial('articles/attachments.tpl', array('data' => $this->view->data)),
						"Краткое описание" => $form->textarea('short_text'),
                        $form->partial('articles/wysiwyg.tpl', array('data' => $this->view->data)),
						"Текст" => $form->tiny('material_text', '', '', array('image_module'=>'materials'))
								. '<b>для того, чтобы использовать галерею - перейдите в редакторе в Инструменты -> Исходный код и обрамите необходимые изображения в '.htmlspecialchars('<div class="gallery"></div>')
								. ' для указания названия изображения в галерее используйте атрибут alt '.htmlspecialchars('(например <img src="" alt="test">)')
                                . '<br></br><b>для того, чтобы прикрепить товар к тексту - перейдите в Инструменты -> Исходный код и обрамите необходимый текст в '.htmlspecialchars('<div id="item_ID"></div> где ID - ид товара').'</b>',
                        $form->partial('articles/similar.tpl', array('data' => $similar, 'resource'=>$this->_getParam('id', 0), 'category'=>Model_Materials::getCategoryForSimilarArticles()))));

		$this->render("edit", null, true);
	}

	public function saveAction() {
        $this->getValidator()->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
        if(Model_Materials::CATEGORY_SHOP_REVIEW != $_POST['category_id'] || !isset($_POST['items_review']))
            $_POST['items_review'] = array();

		list($valid, $data) = $this->isValid($_POST);
		if ($valid) {
			list($html, $gallery, $items) = DR_Api_Tools_Materials::processHtml($_POST['material_text']);

			if (!isset($_POST['stitle']) || empty($_POST['stitle'])) {
				$_POST['stitle'] = DR_Api_Tools_Tools::stitle($_POST['name'], 40);
			}
			//$_POST['is_modern'] = 1; 
			$_POST['stitle'] = $this->_model->stitleUnique($_POST['stitle'], $this->_getParam('id'));
            $post = $_POST;
			$newId = $this->_model->doSave($post, $this->_getParam('id'));
            
			api::getKeywords()->saveMaterialKeywords($post['keywords'], $newId);
			api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_ITEMS, $items);
            
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_MATERIALS, isset($post['similar']) ? $post['similar'] : array());
            
            api::getReferences()->saveReference($newId, Model_References::REF_TYPE_MATERIAL_SHOP, $_POST['items_review']);
            $model_meta = api::getMeta();
            
			$model_meta
					->executeQuery(
							'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
									. Model_Meta::MATERIALS);
			
            $model_meta
					->doSave(
							array('text_value' => $post['material_text'], 'key' => 'material_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $post['short_text'], 'key' => 'short_text',
									'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
			$model_meta
					->doSave(
							array('text_value' => $html, 'key' => 'material_body', 'resource_id' => $newId,
									'modules_id' => Model_Meta::MATERIALS));

			$model_meta
					->doSave(
							array('key' => 'photos', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
									'file_id' => $post['image']));
			
			if(isset($_POST['wysiwyng_status']))
                $model_meta
    					->doSave(
    							array('key' => 'wysiwyng_status', 'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS,
    									'value' => 1));
            if (count($gallery)) {
				foreach ($gallery as $index => $images) {
					foreach ($images as $img) {
						$model_meta
								->doSave(
										array('value' => $img['src'], 'int_value' => $index, 'key' => 'gallery', 'text_value'=>$img['name'], 
												'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
					}
				}
			}
            
            $this->_redirectAjaxAction('/admin/articles');
		}
		$this->Response($data);

	}
    
    /*public function processdeletesimilarAction(){
        
        $data = api::getReferences()->where('t.object_id', $this->_getParam('materials'))->where('t.reference_id', $this->_getParam('id'))->where('t.ref_type', Model_References::REF_TYPE_MATERIAL_MATERIALS)->row();
        if(count($data)) {
            api::getReferences()->doDelete($data->id);
        }
        die;
    }*/
    public function blockgetsimilarAction(){
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $perPage = 25;
        $page = $this->_getParam('page', 1);
        
  		list($this->view->data, $this->view->pages) = $this->_model->notIn('t.id', $this->_getParam('added_articles'))
                    ->in('t.category_id', $this->_getParam('need_category'))
                    ->where('t.is_modern', Model_Materials::TYPE_MODERN)
                    ->joinLeft(array('lv'=>'Model_Listvalues'), 'lv.id = t.category_id', array('category_name'=>'lv.value'))
                    ->pageRows($page, $perPage);
        
        
    }
    public function blockgetitemsAction(){
        Zend_Layout::getMvcInstance()->setLayout('ajax');
        $perPage = 25;
        $page = $this->_getParam('page', 1);
  		api::getItems()->_new();
        if(isset($_POST['added_items']) && count($_POST['added_items']))
            api::getItems()->notIn('t.id', $_POST['added_items']);
        
        list($this->view->data, $this->view->pages) = api::getItems()->pageRows($page, $perPage);
    }
}
