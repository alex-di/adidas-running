<?php

/**
 * Контроллер входа в админку
 * @author appalach
 * @version 1.0
 * @final 
 */
class admin_authController extends DR_Controllers_Admin {

	/**
	 *   - Назначение: Конфигурация
	 */ 
	function preDispatch() {

		parent::preDispatch();

		# - Шаблон модуля         
		Zend_Layout::getMvcInstance()->setLayout('admin_layout_auth');
	}

	/**
	 * блок автроризации
	 */ 
	public function indexAction() {
	}

	/**
	 * вход
	 */
	public function loginAction() {
		if ($this->_request->isPost()) {
			$M = api::getUsers();
			$response = array();
            $_POST['password'] = Model_Users::cryptoPassword($_POST['password']);
			$Data = $M->doLogin($_POST);
			$evalScript = array();
			if (!$Data) {
				$this->_redirectAjaxAction('/admin');
			} else {
				isset($Data['login']) ? $response['#err_login'] = "Логин некоректный" : $response['#err_login'] = "";
				isset($Data['password']) ? $response['#err_password'] = "Пароль некоректный"
						: $response['#err_password'] = "";
				$evalScript[] = '$("#mws-login").effect("shake", {distance: 6, times: 2}, 35);';
			}
			$this->Response($response, $evalScript);
		}
	}

	/**
	 * выход
	 */
	public function logoutAction() {
		$M = api::getUsers();
		$M->doLogout();
		$this->_redirect("/admin/auth");
	}

}
