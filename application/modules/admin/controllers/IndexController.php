<?php

/**
 * @author appalach
 * @version 1.0
 * @final
 */
class Admin_indexController extends DR_Controllers_Admin
{

    /**
     *   - Назначение: Конфигурация админ контроллера
     */
    function preDispatch()
    {
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->setLayout('admin_layout');
    }
    //главная админ панели
    public function indexAction()
    {
        $this->view->data = DR_Api_Tools_Adminthemer::getThemes(); 
    }
    // удалить тему админ панели
    public function processdeleteconfigurationAction(){
        DR_Api_Tools_Adminthemer::deleteTheme($_POST['id']);
        die();
    }
    
    // сохранить тему админ панели
    public function processaveconfigurationAction(){
        DR_Api_Tools_Adminthemer::saveTheme($_POST);
        die;
    }

    //боковое меню админ панели
    public function sidebarAction()
    {
        $this->view->data = api::getControlSidebar()->getMenu();
    }


    public function disabledAction()
    {

    }


}
/**
array("data" => array(
                0 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "Default",
                    "baseColor" => "35353a",
                    "highlightColor" => "c5d52b",
                    "textColor" => "c5d52b",
                    "textGlowColor" => array(
                        "r" => 197,
                        "g" => 213,
                        "b" => 42,
                        "a" => 0.5)),
                1 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "Army",
                    "baseColor" => "363d1b",
                    "highlightColor" => "947131",
                    "textColor" => "ffb575",
                    "textGlowColor" => array(
                        "r" => 237,
                        "g" => 255,
                        "b" => 41,
                        "a" => 0.4)),
                2 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "RockyMountains",
                    "baseColor" => "2f2f33",
                    "highlightColor" => "808080",
                    "textColor" => "b0e6ff",
                    "textGlowColor" => array(
                        "r" => 230,
                        "g" => 232,
                        "b" => 208,
                        "a" => 0.4)),
                3 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "ChineseTemple",
                    "baseColor" => "4f1b1b",
                    "highlightColor" => "e8cb10",
                    "textColor" => "f7ff00",
                    "textGlowColor" => array(
                        "r" => 255,
                        "g" => 255,
                        "b" => 0,
                        "a" => 0.6)),
                4 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "Boutique",
                    "baseColor" => "292828",
                    "highlightColor" => "f08dcc",
                    "textColor" => "fcaee3",
                    "textGlowColor" => array(
                        "r" => 186,
                        "g" => 9,
                        "b" => 230,
                        "a" => 0.5)),
                5 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "Toxic",
                    "baseColor" => "42184a",
                    "highlightColor" => "97c730",
                    "textColor" => "b1ff4c",
                    "textGlowColor" => array(
                        "r" => 230,
                        "g" => 232,
                        "b" => 208,
                        "a" => 0.45)),
                6 => array(
                    "is_edit" => 0,
                    "patterns" => "Paper",
                    "name" => "Aquamarine",
                    "baseColor" => "192a54",
                    "highlightColor" => "88a9eb",
                    "textColor" => "8affe2",
                    "textGlowColor" => array(
                        "r" => 157,
                        "g" => 224,
                        "b" => 245,
                                               
                        "a" => 0.5))), "selected" => 5);
 */
