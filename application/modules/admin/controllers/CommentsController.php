<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_commentsController extends DR_Controllers_Admin
{

    # все пользователи
    public function indexAction()
    {
        $this->getBreadcrumbs()->appendView('Модерация коментариев');
        $groups = api::getGroups()->forSelect(array("id" => "title"));
        $this->view->tables = array("Модерация коментариев к статьям" => array(
                "is_page" => true,
                "is_mass_check" => true,
                "is_option_coll" => true,
                "toolbar" => array(array('title'=>'Быстрая модерация', 'handler'=>'fastModernComments()', 'class'=>'ic-lightning'), DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "text" => array("name" => "Текст"),
                    "username" => array("name" => "Пользователь"),
                    "article_name" => array("name" => "Статья", "filter" => new DR_Api_Admin_FilterString('mt.name')),
                    "date" => array("name" => "Дата"),
        )));
    }


    public function blockdatatableAction()
    {
        $this->isSelfViewTable = true;
        $this->_model->fields(array("*"))
            ->joinLeft(array("us" => api::USERS), "us.id = t.users_id", array("username" => "concat(us.name, ' ', us.sname)"))
            ->joinLeft(array("mt" => api::MATERIALS), "mt.id = t.resource_id", array("article_name" => "mt.name", 'article_stitle'=>'mt.stitle'))
            ->where('t.modules_id', Model_Comments::MATERIALS);
        parent::blockdatatableAction();
    }
    public function processsaveAction() {
        $this->_model->doSave(array('is_modern' => 1, 'text'=>$_POST['text']), $_POST['id']);
        die('OK');
    }
    public function processmodernAction(){
        if(isset($_POST['id']) && is_array($_POST['id']) && count($_POST['id'])) {
            foreach($_POST['id'] as $comment_id) {
                $this->_model->doSave(array('is_modern' => 1), $comment_id);
            }
        }
        die('OK');
    }
    public function deleteAction()
    {
        if ($id = $this->_getParam('id')) {
            if (!is_array($id)) 
                $id = array($id);
            $data = api::getReferences()->_new(array('ev.counter','count_delete_comment' => 'count(t.id)','event_id' =>'ev.id'))->in('t.reference_id', $id)->where('t.ref_type', Model_References::REF_TYPE_EVENT_COMMENTS)
                                    ->joinLeft(array('ev' => api::EVENTS), 'ev.id = t.object_id', array())
                                    ->group('ev.id')
                                    ->rows();
            if(count($data)) {
                foreach($data as $row) {
                    if($row->count_delete_comment == $row->counter)
                        api::getEvents()->doDelete($row->event_id);
                    else
                        api::getEvents()->doSave(array('counter' => $row->counter - $row->count_delete_comment), $row->event_id);
                }
            }    
            foreach ($id as $i)
                $this->_model->doDelete($i);

            if (is_null($this->location))
                $this->location = "/admin/" . $this->view->module_params['controller'];
            if ($this->_request->isPost()) {
                die($this->location);
            } else {
                $this->_redirect($this->location);
            }

        } else {
            $this->showError("обьект для удаления не передан");
        }

    }
}