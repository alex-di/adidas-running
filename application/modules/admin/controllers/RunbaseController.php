<?php

/**
 * Админ контроллер для добавления runbase
 * @author appalach
 * @version 1.0
 * @final 
 */
class Admin_runbaseController extends DR_Controllers_Admin {
	
        #Инициализация модели
        public function init() {
		$this->_model = api::getMaterials();
	}
        
        
	public function indexAction() {
        $this->getBreadcrumbs()->appendView();
		$this->view->tables = array(
				"Runbase события" => array("is_page" => true, "is_mass_check" => true, "is_option_coll" => true,
						"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(''),
								DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array()),
						"fields" => array("id" => array("name" => "ID"), "name" => array("name" => "Название")),));

		parent::indexAction();
	}

	public function blockdatatableAction() {
		$this->_model->where('category_id', Model_Materials::CATEGORY_RUNBASE);
		parent::blockdatatableAction();
	}

        
        
        
	public function editAction() {
                $this->getBreadcrumbs()->appendView('Runbase события')->appendEdit();
		$keywords = '';
		
                
        $this->view->data = $this->_model->getMaterials(intval($this->_getParam('id', 0)));
		//_d($this->view->data);
        $event = "";
		if (count($this->view->data)) {
			$this->view->data = Model_Materials::getFirst($this->view->data);
            $event = $this->_model->getRunbaseEvents($this->view->data['id']);
            if(count($event)) {
                $event = $event['id'];
            }
		}
       
        
        //_d($event);
		$form = new DR_Api_Admin_EditForm($this->view->data);
                #выбираем Тренеров и администраторов
                $user_list = api::getUsers()->_new()->fields(array('username' => "concat(t.name, ' ', t.sname)"))
				    ->in('t.group_id', array(Model_Groups::COACH, Model_Groups::ADMIN))
                    ->order("t.group_id DESC")
                    ->forSelect(array('id' => 'username'), true);
                $event_list = api::getMaterials()->_new()->where('t.category_id', Model_Materials::CATEGORY_EVENTS)
                    ->order('t.id DESC')->forSelect(array('id' => 'name'), true);
            $artileLink = '/article/programmyi-trenirovok';
		    $linkList = array($artileLink . '#intervaltranings' => 'интервальная тренировка',
                                $artileLink . '#runstretching' => 'бег + stretching (растягивание)',
                                $artileLink . '#runofp' => 'бег + ОФП',
                                $artileLink . '#runwalk' => 'бег + спортивная ходьба',
                                $artileLink . '#runcore' => 'бег + core',
                                $artileLink . '#cross' => 'кросс',
                                $artileLink . '#runtranings' => 'бег + функциональный тренинг',);
		      $this->view->elements = array("name" => "Редактирование события",
			"fields" => array(
                        //$form->partial('articles/link.tpl', array('data' => $this->view->data, 'routerName'=>'event_view')),
                        "Название события" => $form->stringInput('name'),
                            "Тренер" => $form->select('users_id', $user_list),
                            "Скрыть на сайте" => $form->checkbox('is_modern', 1, '', 0),
                            "Дата события" => $form->datetime('date_start_event'),
                            "Привязка к событию" =>  $form->select('event_id', $event_list, 'event_id', $event),
                            'Привязка к видам тренировок статьи "программы тренировок"' =>  $form->select('anchor_article_link', $linkList),
                        ));
                //$this->render("edit", null, true);

	}

	public function saveAction() {

	   
            $this->getValidator()->setNotRequiredFields(array("event_id"))->setElement('date_start_event')
                ->date(null, array('format' => 'Y-m-d H:i:s'))
                ->setElement('title_image')->resetValidatorAndSetNotEmpty('Загрузите изображение');
            list($valid, $data) = $this->isValid($_POST);
            if ($valid) {
                $_POST['category_id'] = Model_Materials::CATEGORY_RUNBASE;
                
                $newId = $this->_model->doSave($_POST, $this->_getParam('id'));
                
                $model_meta = api::getMeta();

                $model_meta
                                ->executeQuery(
                                                'DELETE FROM Model_Meta WHERE resource_id = ' . $newId . ' AND modules_id = '
                                                                . Model_Meta::MATERIALS);
                $model_meta
                                ->doSave(
                                                array('date_value' => trim($_POST['date_start_event']), 'key' => 'date_start_event',
                                                                'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));
                                                                
                $model_meta
                                ->doSave(
                                                array('value' => $_POST['anchor_article_link'], 'key' => 'anchor_article_link',
                                                                'resource_id' => $newId, 'modules_id' => Model_Meta::MATERIALS));                                         
                if(isset($_POST['event_id']) and !empty($_POST['event_id'])) {      
                    api::getReferences()->saveReference($newId, Model_References::REF_TYPE_RUNBASE_EVENT, array($_POST['event_id']));
                } else {
                    api::getReferences()->deleteReference($newId, Model_References::REF_TYPE_RUNBASE_EVENT);
                }
                    
                $this->_redirectAjaxAction('/admin/runbase', $this->message);
            }
            $this->Response($data);

	}
	
	
	
	#Показать забронированныех
	public function reserveAction(){
		$this->view->tables = array("Зарегистрированные пользователи" => array(
                "data_action" => "runbase/blockdatarunbaseuser",
                "is_page" => true,
                "is_mass_check" => false,
                "is_option_coll" => false,
                //"toolbar" => array(DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER => array('title' => 'Скачать excel', 'action' => '/admin/users/download/events_id/' . $events_id)),
                "fields" => array(
                    "id" => array("name" => "ID"),
                    "email" => array("name" => "Email"),
                    "name" => array("name" => "Имя"),
                    "sname" => array("name" => "Фамилия"),
                    "bday" => array("name" => "Дата рождения"),
                    "city" => array("name" => "Город"),
                    "sex" => array("name" => "Пол"),
		    "reserve_date" => array("name" => "Дата бронирования"),
                    //"fb_username" => array("name" => "Facebook"),
                    //"vk_username" => array("name" => "Vk"),
                    //"tw_username" => array("name" => "Twitter")
		)));
		parent::indexAction();
	}
	
	public function blockdatarunbaseuserAction()
	{
	    $this->isSelfViewTable = true;
	    $this->_model = api::getUsers();
	    $this->_model->_new()->where("t.id IN (SELECT reference_id FROM Model_References WHERE object_id = 1 AND date > NOW() AND ref_type = ".Model_References::REF_TYPE_RUNBASE.')')
			->joinLeft(
				array('ref' => api::REFERENCES),
				'ref.reference_id = t.id and ref.object_id = 1 and ref.ref_type ='.Model_References::REF_TYPE_RUNBASE,
				array('reserve_date'=>'ref.date'));
			//->joinLeft(array('meta2' => api::META), 'meta2.resource_id = t.id and meta2.modules_id = '.Model_Meta::USERS." and meta2.key = 'vk_username'", array('vk_username'=>'meta2.value'))
			//->joinLeft(array('meta3' => api::META), 'meta3.resource_id = t.id and meta3.modules_id = '.Model_Meta::USERS." and meta3.key = 'tw_username'", array('tw_username'=>'meta3.value'));
		
	    parent::blockdatatableAction();
	    //_d($this->view->data);
	}
}
