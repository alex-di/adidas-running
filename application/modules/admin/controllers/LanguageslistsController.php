<?php

/**
 * Админ контроллер для редактирования языков сайта
 * @author yurecboss
 * @version 1.0
 * @final 
 */
class Admin_languageslistsController extends DR_Controllers_Admin {

	public function indexAction() {
		$this->view->tables = array(
				"Фразы" => array("data_action" => "languageslists/blockdatatable", "is_page" => true,
						"is_mass_check" => false, "is_option_coll" => true,
						"fields" => array("phrase" => array("name" => "По умолчанию"),
								"value" => array("name" => "Перевод")),
						"params" => array("languages" => $this->_getParam("languages"))));
		parent::indexAction();
	}
	public function blockdatatableAction() {
		$this->isSelfViewTable = true;
		$input_data = $this->_request->getParams();
		$this->_model = api::getPhrases();
		$this->_model->fields(array("t.phrase", "ls.*", "id_phrase" => "t.id"))
				->joinLeft(array("ls" => 'Model_Languageslists'),
						"ls.phrase_id = t.id and ls.language_id=" . $input_data['languages']);
		parent::blockdatatableAction();
	}
	public function saveAction() {
		$model = api::getLanguage_lists();
		$id = null;
		if (!empty($_POST['id'])) {
			$id = $_POST['id'];
			$model->doSave(array("value" => $_POST['value']), $id);
		} else {
			$id = $model
					->doSave(
							array("value" => $_POST['value'], "phrase_id" => $_POST['phrase_id'],
									"language_id" => $_POST['language']));
		}

		die(
				json_encode(
						array("id" => $id, "value" => $this->view->escape($_POST['value']),
								"phrase_id" => $_POST['phrase_id'])));
	}
}
