<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
<script src="/design/js/google/cluster.js"></script>
<script src="/design/js/google/mileage_overlay.js"></script>
<script src="/design/js/google/config.js"></script>
<script>
MenuNavigator.setCurrentMenu('/routes');
var map;
var polyline = new google.maps.Polyline();
var markers = [];
var markerCluster = null;
var isClickMarker = false;
var overlay = {};
function getSettings(town) {
    switch(town) {
        case 'anapa':
            return {zoom:  12, center: new google.maps.LatLng(44.76256779296794, 37.4106167602539)};
        default:
            //Moscow
            return {zoom:  11, center: new google.maps.LatLng(55.7802315750141, 37.62038696289062)};
    }
}
function initialize() {
    var mapSelector = 'map-canvas';
    $('#'+mapSelector).css('height', $(window).height() - $('.layout header').height());
    var mapDiv = document.getElementById(mapSelector);
    settings = getSettings();
    var mapOptions = {
        zoom: settings.zoom,
        minZoom: 4,
        scrollwheel:false,
        center: settings.center,//new google.maps.LatLng(55.7802315750141,37.62038696289062),//Moscow,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl:false,
        panControl:false,
        zoomControl: false,
        styles: STYLES_MAP
    };
    map = new google.maps.Map(mapDiv, mapOptions);
    initControl(map);
    <?echo $this->action('blockdata', 'map', 'routes')?>
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>



                <div class="limit">
                    <div class="route_sort">                         
                         <ul class="route_left_menu">
                              <li><span class="grid"></span><a href="/routes">сетка</a></li>
                              <li class="current"><span class="map"></span><a href="/routes/map">карта</a></li>
                         </ul>
                        <div class="combo length" style="width: 163px;">
                           <div class="current">Все</div>
                           <div class="arrow"></div>
                           <ul>
                           	   <li data-id=''>Все</li>
                           	   <?php foreach($this->lists['routes_between'] as $key=>$val):?>
                               		<li data-id='<?php echo $key?>'><?php echo $val?></li>
                               <?php endforeach;?>
                           </ul>
                           <input type="text" value="" />
                       </div>
                        <div class="combo town">
                            <div class="current">Москва</div>
                            <div class="arrow"></div>
                            <ul>
                                <li data-id="moscow">Москва</li>
                                <li data-id="anapa">Анапа</li>
                            </ul>
                            <input type="text" value=" " />
                        </div>
                        <ul class="route_right_menu">
                             <!--<li>Популярные</li>
                             <li class="current">Новые</li>http://support-drcore.com.ua/adidas/issue/140-->
                             <!--<li>Ближайшие</li>-->
                         </ul>
                         <div style="clear: both;"></div>
                    </div>
                </div>
                
                </div><!--close div.limit-->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <div class="map_container" style="width: 100%; height: 100%">  
                          <div id="map-canvas" style="width: 100%; height: 700px"></div>
                    </div> 
                <div><!--open div>-->
                <script type="text/javascript">
                    var paginator = new Paginator('/routes/map/blockdata');
                    paginator.setContainerSelector('body');
                  	$('.combo.length input').change(function(){
                		paginator.resetPage().setParam('between', $(this).val()).getData();
                	});
                	$('.combo.town input').change(function(){
                	    settings = getSettings($(this).val());
                		map.setCenter(settings.center);
                        map.setZoom(settings.zoom);
                	});
                </script>