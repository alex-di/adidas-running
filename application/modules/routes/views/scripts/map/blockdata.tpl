<?if($this->isAjax):?>
    <script type="text/javascript">
<?endif;?>
    for(var i in markers) {
        markers[i].setMap(null);
    }
    markers = [];
    overlay = {};
    var bounds = new google.maps.LatLngBounds;
    bounds.extend(getRunbaseLatLng());
    <?if(count($this->data)):?>

        <?foreach($this->data as $row):?>
            var startPositionLatLng = new google.maps.LatLng(<?echo $row->start_lat?>, <?echo $row->start_lng?>);
            var marker = getMarker(startPositionLatLng);
            bounds.extend(startPositionLatLng);
            overlay["<?echo $row->id?>"] = new MileageOverlay(<?echo number_format($row->mileage, 2, '.', '')?>, marker, map);
            
            google.maps.event.addDomListener(marker, 'click', function() {
           	    window.location = "<?echo $this->url(array('stitle'=>$row->stitle),'route_view')?>";
            });
            google.maps.event.addDomListener(marker, 'mouseover', function() {
            	overlay["<?echo $row->id?>"].show();
           	    var overview_path = <?echo $row->overview_path?>;
                var latLngArray = [];
                polyline.setMap(null);
                polyline = new google.maps.Polyline({strokeColor: '#FFEC2C', strokeWeight: 4});
                for(var i in overview_path) {
                	latLngArray.push(new google.maps.LatLng(overview_path[i].jb, overview_path[i].kb));
                }
                polyline.setPath(latLngArray);
                polyline.setMap(map);
            });
            google.maps.event.addDomListener(marker, 'mouseout', function() {
                if(!isClickMarker) {
               	    overlay["<?echo $row->id?>"].hide();
                    polyline.setMap(null);
                }
                isClickMarker = false;

            });
            markers.push(marker);
        <?endforeach;?>
    <?endif;?>

    markerCluster = new MarkerClusterer(map, markers, {styles: CLUSTER_STYLES});
    //map.fitBounds(bounds);
<?if($this->isAjax):?>
    </script>
<?endif;?>