<div class="route_sort">                         
    <ul class="route_left_menu">
         <li class="current"><span class="grid"></span><a href="/routes">сетка</a></li>
         <li ><span class="map"></span><a href="/routes/map">карта</a></li>
    </ul>
    <div class="combo length" style="width: 163px;">
       <div class="current">Все</div>
       <div class="arrow"></div>
       <ul>
       	   <li data-id=''>Все</li>
       	   <?php foreach($this->lists['routes_between'] as $key=>$val):?>
           		<li data-id='<?php echo $key?>'><?php echo $val?></li>
           <?php endforeach;?>
       </ul>
       <input type="text" value="" />
   </div>
   <!--<div class="combo town disabled">
       <div class="current">Москва</div>
       <div class="arrow"></div>
       <ul>
           <li>Санкт-Петербург</li>
           <li>Москва</li>
           <li>Март</li>
           <li>Брянск</li>
           <li>Уфа</li>
           <li>Казань</li>
       </ul>
       <input type="text" value=" " />
   </div>-->
   <ul class="route_right_menu">
         <li onclick="updateStripOrder('rate')">Популярные</li>
         <li onclick="updateStripOrder('id')" class="current">Новые</li>
         <!--<li>Ближайшие</li>-->
    </ul>
    <div style="clear: both;"></div>
</div>
<script type="text/javascript">
    MenuNavigator.setCurrentMenu('/routes');
	defaultPaginator = new Paginator('/routes/index/blockmaterials');
	function updateStripOrder(order) {
		defaultPaginator.resetPage();
		defaultPaginator.setParam('sort', order)
		defaultPaginator.getMaterials(true);
	}
	$('.combo input').change(function(){
		console.log($(this).val());
		defaultPaginator.resetPage();
		defaultPaginator.setParam('between', $(this).val())
		defaultPaginator.getMaterials(true);
	});
    $(function(){
       $('.index_slider').mySlider(); 
    });
</script>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<div id="container">                          
    <?= $this->action('view', 'slider', 'banners', array('key' => Model_Slider::SLIDER_IN_ROUTES, 'type' => 'route_type'))?>            
    
	<?= $this->action('blockmaterials', 'index', 'routes')?>
   
   
   <div style="clear: both;"></div>                              	
</div> 