<?php

/**
 * 
 * @author yurecboss
 * @version 1.3
 * @final
 */
class Routes_indexController extends DR_Controllers_Site {

	public function indexAction() {
	    $this->view->headTitle('Маршруты - adidas running');
		$atricles = api::getMaterials();
		$atricles->_new(array('id'))->where('t.category_id', Model_Materials::CATEGORY_ROUTERS);
		$ids = $atricles->forSelect(array('id' => 'id'));
		$this->view->data = $atricles->getMaterials($ids);
	}
	public function blockmaterialsAction() {
		$page = $this->_getParam('page', 1);
		$sort = $this->_getParam('sort', 'id');
		$between = $this->_getParam('between', '');
		$materials = api::getMaterials();
		list($data, $paginator) = $materials->_new(array('id'))
				->where('t.category_id', Model_Materials::CATEGORY_ROUTERS)->order($sort . ' DESC')
				->where('is_modern', 1)->routeBetween($between)->pageRows($page, 10);
		$this->view->pageCount = $paginator->count();
		if (count($data)) {
			$ids = Model_Materials::getArrayObjectId($data);
			$this->view->data = $materials->getMaterials($ids);
		}

		parent::blockmaterialsAction();
	}
}
