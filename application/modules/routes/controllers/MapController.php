<?php

/**
 * @author yurecboss
 * @version 1.0
 * @final
 */
class Routes_mapController extends DR_Controllers_Site {
    
    public function indexAction(){
        $this->view->headTitle('Маршруты - adidas running');
    }
    public function blockdataAction(){
  		if ($this->view->isAjax = $this->_request->isXmlHttpRequest())
			Zend_Layout::getMvcInstance()->setLayout('ajax');
        $between = $this->_getParam('between', '');
        
        $this->view->data = api::getMaterials()->_new(array('t.id', 't.stitle'))->where('t.category_id', Model_Materials::CATEGORY_ROUTERS)
                                                ->where('is_modern', 1)
                                                ->routeBetween($between)
    				                            ->joinLeft(array('meta_overview_path' => api::META), "meta_overview_path.resource_id = t.id and meta_overview_path.key='overview_path' and meta_overview_path.modules_id = ". Model_Meta::MATERIALS, array('overview_path' => 'meta_overview_path.text_value'))
                                                ->joinLeft(array('rt' => api::ROUTESDATA), "rt.materials_id = t.id", array('rt.start_lat', 'rt.start_lng'))
                                                ->limit(100)
                                                ->rows();
    }
}