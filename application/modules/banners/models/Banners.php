<?php

/**
 * Модель для статичесик страниц
 */
class Model_Banners extends DR_Models_AbstractTable {
    
    #баннеры
    const TYPE_BANNER = 'banner';
    #баннеры из слайдеров
    const TYPE_SLIDER = 'slider';
    #баннеры из промо блоков
    const TYPE_PROMO =  'promo';
    
}