
<div class="index_slider element <?echo $this->type?>">
    <?if(count($this->slider_banners) > 1):?>
        <div class="slider_numbers">
            <? foreach($this->slider_banners as $index => $banner): ?>
                <a href="" class="t_<?= !empty($banner['banner_color']) ? $banner['banner_color'] : 'black'; ?>"><?echo $index + 1?><span></span></a><!-- t_black\t_green\t_yellow - цвет текста при наведении-->
            <?endforeach;?>
        </div>
    <?endif;?>
    <? 
    $matches = array('/\[yellow\](.*)\[\/yellow\]/', '/\[black\](.*)\[\/black\]/');
    $replaces = array('<span class="text_box yellow">$1</span>', '<span class="text_box black">$1</span>');
    foreach($this->slider_banners as $index => $banner): 
        $color = !empty($banner['banner_color']) ? $banner['banner_color'] : 'black';
    ?>
        <div class="index_slide" <?if(!empty($banner['banner_link'])):?>onclick="window.location='<?echo $banner['banner_link']?>'"<?endif;?>>
            <img src="<?= Model_Files::getPreview($banner['banner_image'], 560) ?>" alt="img" />
        </div>
    <?endforeach;?>                            
</div>