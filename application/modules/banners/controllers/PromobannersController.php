<?php

/**

 * 
 * @author appalach
 * @version 1.0
 * @final
 */
class Banners_PromobannersController extends DR_Controllers_Comments {
    
    public function viewAction() {
        if($key = $this->_getParam('key')) {
            $sliders = api::getPromoblock();
            $this->view->banners = $sliders->_new(array('t.id'))->where('t.key', $key)
                ->joinLeft(array('pm' => api::PROMOMAT), 'pm.promo_id = t.id', array('promo_type' => 'pm.type', 'pm.code', 'pm.image', 'pm.material_id'))
                ->rows();
            
            $banners = array();
            foreach($this->view->banners as $banner) {
                if($banner->promo_type == Model_Promomaterials::BANNER) {
                    $banners[] = $banner->material_id;
                }
            }
            
            $this->view->materials = api::getMaterials()->getMaterials($banners);
            
            //_d($this->view->materials);
        }
        else die('promo block not found');
    }
    
}
