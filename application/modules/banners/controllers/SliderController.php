<?php

/**

 * 
 * @author appalach
 * @version 1.0
 * @final
 */
class Banners_SliderController extends DR_Controllers_Comments
{

    public function viewAction()
    {
        if ($key = $this->_getParam('key'))
        {
            $sliders = api::getSlider();
            $this->view->slider_banners = $sliders->_new()->where('t.key', $key)
                        ->joinLeft(array('b' => api::BANNERS), 'b.slider_id = t.id', array(
                                    'banner_image' => 'b.file',
                                    'banner_name' => 'b.name',
                                    'banner_color' => 'b.color',
                                    'banner_link' => 'b.link',
                                    ))->order('b.id ASC')->rows();
            $this->view->type = $this->_getParam('type', 'default');

            //_d($this->view->slider_banners);
        } else
            die('slider not found');
    }

}
