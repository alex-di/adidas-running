<div class="calends calends-black">
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">16-30</div>
            <div class="calend-month">сентября</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">вручение ПОДАРКОВ УЧАСТНИКАМ <br/>Московского <br/> марафона*</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">16-22</div>
            <div class="calend-month">сентября</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">тест-драйв boost*</div>
            <p>При предъявлении специального флайера из "Набора новичка" каждый участник акции может купить
                революционные кроссовки boost и тестировать на протяжении 30 дней. По истичении этого срока кроссовки
                можно будет вернуть в один из акционных магазинов при предъявлении флайера и чека.</p>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>
</div>

<div class="calend-shops">
    <p class="calend-shops-title">*МАГАЗИНЫ, ПРИНИМАЮЩИЕ УЧАСТИЕ В АКЦИИ</p>

    <p><span>ТЦ Европейский</span> пл. Киевского вокзала, дом 2</p>

    <p><span>Адидас Олимп</span> ул. Красная пресня, дом 23</p>

    <p><span>Мега Белая Дача</span> Люберецкий район, Московская область</p>

    <p><span>ТЦ Вегас</span> Московская Кольцевая Автомобильная дор., 24-й км</p>

    <p><span>ТЦ Золотой Вавилон</span> Проспект Мира, д.211, к.2</p>

    <p><span>ТЦ Манеж</span> Манежная площадь, 1, стр. 2</p>

    <p><span>ТЦ Фестиваль</span> ул. Мичуринский проспект. Олимпийская деревня, д.3, корп. 1</p>

    <p><span>Мега Теплый стан</span> Московская область, Ленинский район, Калужское шоссе, 21-й км (или 41-й км МКАД)
    </p>

    <p><span>Мега Химки</span> ш. Ленинградское Московская область</p>
</div>

<div class="calend-title calend-title-yellow">выдача флаеров на получение карты runner></div>

<div class="calends calends-yellow">
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">17</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">вторник</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">ТЦ “охотный ряд”</div>
            <p>Манежная площадь, дом 1, строение 2</p>
        </div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">19</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">четверг</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">ТЦ “Европейский”</div>
            <p> пл. Киевского вокзала, дом 2</p>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">24</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">вторник</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">ТЦ “Европейский”</div>
            <p> пл. Киевского вокзала, дом 2</p>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">26</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">четверг</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">ТРЦ “Золотой Вавилон”</div>
            <p>Проспект Мира, дом 211/p>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>
</div>

<div class="calend-title calend-title-green">консультации тренера runbase></div>


<div class="calend-subtitle">
   <span>ТЦ “охотный ряд”</span>  Манежная площадь, дом 1, строение 2
</div>

<div class="calends calends-green">
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">18</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">среда</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">17:30 - 21:00</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">20</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">пятница</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">17:30 - 21:00</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>
</div>

<div class="calend-subtitle">
    <span>ТЦ “ЕВРОПЕЙСКИЙ”</span> Площадь Киевского вокзала, дом 2
</div>

<div class="calends calends-green">
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">22</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">воскресенье</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">13:00 - 21:00</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">25</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">среда</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">17:30 - 21:00</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>
</div>

<div class="calend-subtitle">
    <span>ТЦ “Золотой Вавилон”</span>Проспект Мира, дом 211
</div>

<div class="calends calends-green">
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">27</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">пятница</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">17:30 - 21:00</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="calend">
        <div class="calend-date-month">
            <div class="calend-date">28</div>
            <div class="calend-month">сентября</div>
            <div class="calend-day">суббота</div>
        </div>
        <div class="calend-desc">
            <div class="calend-desc-title">13:00 - 17:30</div>
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>
</div>