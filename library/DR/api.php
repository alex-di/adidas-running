<?php

/**
 * Обертка упрощенного доступа к классам
 * @author yurecboss
 * @version 2.0
 */
class api
{

    #Стандартные модели
    const ACLRESOURCES = "Model_Aclresources";
    const BANNERS = "Model_Banners";
    const GROUPS = "Model_Groups";
    const CARDS = "Model_Cards";
    const EVENTS = "Model_Events";
    const CATEGORY = "Model_Category";
    const CITY = "Model_City";
    const CONTACTS = "Model_Contacts";
    const COMMENTS = "Model_Comments";
    const COUNTRY = "Model_Country";
    const CONFIG = "Model_Config";
    const FEEDBACK = "Model_Feedback";
    const FILES = "Model_Files";
    const LISTS = "Model_Lists";
    const LIST_VALUES = "Model_Listvalues";
    const LANGUAGE_LISTS = "Model_Languageslists";
    const LANGUAGE_PAGES = "Model_Languagespages";
    const MATERIALS = "Model_Materials";
    const KEYWORDS = "Model_Keywords";
    const MATERIALKEYS = "Model_Materialkeys";
    const META = "Model_Meta";
    const MESSAGES = "Model_Messages";
    const NEWS = "Model_News";
    const PAGES = "Model_Pages";
    const PERMISSIONS = "Model_Permissions";
    const PHRASES = "Model_Phrases";
    const ROUTESDATA = "Model_Routesdata";
    const REGION = "Model_Regions";
    const REFERENCES = "Model_References";
    const USERS = "Model_Users";
    const ITEMS = "Model_Items";
    const VOTES = "Model_Votes";
    const SLIDER = "Model_Slider";
    const SOCIALPOST = "Model_Socialpost";
    const PROMOBLOCK = "Model_Promoblock";
    const PROMOMAT = "Model_Promomaterials";
    const MODULES = "Model_modules";
    const CONTROLSIDEBAR = "Model_Controlsidebar";
    const SHARES = "Model_Shares";
   
   
    private static $store = array();

    public static function __callStatic($method, $args)
    {
     	$model = strtoupper(strtr($method, array('get'=>'')));
     
     	return self::_get(constant('api::'. $model), $model);
    }
    
    public static function _get($model, $name)
    {
        if (!isset(self::$store[$model])) {
            if (class_exists($model)) {
               self::$store[$model] = new $model;
            } else {
               $model_temp = "DR_Models_Default_" . ucfirst(strtolower($name));
               //_d($model_temp);
               self::$store[$model] = new $model_temp;
            }
        }
        return self::$store[$model];
    }
}
