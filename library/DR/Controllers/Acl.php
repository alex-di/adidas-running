<?

/**
 * проверка прав доступа 
 */
class DR_Controllers_Acl extends Zend_Acl
{

    protected $group = "";
    protected $current_role = "";
    const ADMIN_GROUP = 2;
    const QUEST_GROUP = 4;
    
    public function __construct()
    {

        #Добавляем роль текущего пользователя
        $this->current_role = api::getUsers()->getRoleId(self::QUEST_GROUP);
        $this->addRole($this->current_role);
        
        list(, $this->group) = explode("_", $this->current_role);

        #извлекаем ресурсы
        $model_resources = api::getAclResources();
        $data = $model_resources->_new()->rows();

        if(count($data)) {
            foreach ($data as $resource)
            {
                $this->add(new Zend_Acl_Resource($resource->value));
            }
        }




        #Извлекаем разрешение если оно есть
        $model_permissions = api::getPermissions();

        $allows = $model_permissions
            ->where('t.group_id', $this->group)
            ->joinLeft(
                array("ar" => api::ACLRESOURCES), 
                "ar.id = t.resource_id",
                array("resource" => "ar.value")
        )->rows();

        if ($this->group == self::ADMIN_GROUP)
        {
            $this->allow($this->current_role, null);
        } elseif (count($allows))
        {
            foreach ($allows as $allow)
            {
                $this->allow($this->current_role, $allow->resource);
            }
        }

    }

    public function isAllow($resource)
    {
        if (!$this->has($resource))
        {
            $this->add(new Zend_Acl_Resource($resource));
        }
        #Разрешаем всем просматривать ошибку
        if ('admin:index:disabled' == $resource)
        {
            $this->allow($this->current_role, $resource);
        }
        return $this->isAllowed($this->current_role, $resource);
    }

    public function isAllowedResource($resource)
    {
        if ("" == $resource)
            return true;
        $resource = explode("/", $resource);
        $temp = array();
        foreach ($resource as $res)
        {
            if ($res != "")
                $temp[] = $res;
        }
        if (!count($temp))
            return true;
        $size = count($temp);
        for ($i = 0; $i < 3 - $size; $i++)
            $temp[] = "index";
        return $this->isAllow(implode(":", $temp));
        //return true;
    }
    

}