<?php
class DR_Controllers_Template_Admin extends DR_Controllers_Admin
{
    public function indexAction() {
        $module_name = $this->getRequest()->getControllerName();
        
        $modules_model = api::getModules();
        $module = $modules_model($module_name, 'name');
        
        if(count($module)) {
            $template = new DR_Api_Admin_Template($module->title);
            $this->view->tables = $template->getTables($module->params);
            //_d($this->view->tables);
        }
        parent::indexAction();
    }
    

}