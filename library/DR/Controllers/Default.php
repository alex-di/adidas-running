<?php

// Root schema

class DR_Controllers_Default extends Zend_Controller_Action {



	public $host = "";

	/**
	 * По умолчанию после сохранения никуда не переходим 
	 */
	var $location = null;
	/**
	 * После сохранения переходим по указанной в етой переменной ссылке + добавляем id от успешно сохраненной записи
	 */
	var $locationAtNew = null;
	/**
	 * По умолчанию после сохранения не показываем сообщение
	 */
	var $message = null;
	/**
	 * Режим работы пагинатора 
	 */
	var $paginatorMode = true;
	/**
	 * Параметры запроса
	 */
	var $requestParams = null;

    protected $validator = null;
	/**
	 *   Назначение: Конфигурация контроллера
	 */
	function preDispatch() {
		$this->host = $_SERVER['HTTP_HOST'];
		$this->view->http_host = $_SERVER['HTTP_HOST'];

		# - Получаем параметры запроса
		$this->requestParams = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		$this->view->module_params = $this->requestParams;

		$resource = $this->requestParams['module'] . ':' . $this->requestParams['controller'] . ':'
				. $this->requestParams['action'];
		$this->view->acl = $this->getAcl();
		
		if (!$this->view->acl->isAllow($resource))
		{

		    if ($this->_request->isXmlHttpRequest())
		    {
		        die("PERMISSION_DENIED");
		    } else
		    {
		        if ($this->requestParams['module'] == "admin")
		        {
		            if (!Zend_Auth::getInstance()->hasIdentity())
		            {
		                $this->_redirect('/admin/auth');
		            } else
		            {
		                $this->_redirect('/admin/index/disabled');
		            }
		        } else
		        {
		            $this->_redirect('/');
		        }
		    }
		}

		# - Опции сайта
		//_d($this->getOptions());
		list($this->view->settings, $this->view->lists, $this->view->images) = $this->getOptions();

		Zend_Registry::set('lists', $this->view->lists);
		Zend_Registry::set('settings', $this->view->settings);
		Zend_Registry::set('images', $this->view->images);

        Zend_Registry::set('request', $this->requestParams);
		parent::preDispatch();
	}

	protected function getAcl() {
		$namespace = 'acl_namespace';
		if (Zend_Registry::isRegistered($namespace))
			return Zend_Registry::get($namespace);
		else {
			$acl = new DR_Controllers_Acl();
			Zend_Registry::set($namespace, $acl);
			return $acl;
		}
	}
    
    public function getValidator() {
        if(is_null($this->validator))
            $this->validator = new DR_Api_Validate_Manager();
        return $this->validator;
    }
   
    public function isValid($data) {
       return $this->getValidator()->validate($data);
	}
    
	/**
	 *   - Назначение: Возвращение данных браузеру
	 *   - Дата: 29.03.2010 11:21:31 
	 * @param struct Массив данных
	 * @return JSON
	 */
	public function Response($ArrayOfValidateData, $scripts = array()) {
		$this->_helper->getHelper('Jquery');

		if (is_array($ArrayOfValidateData) and count($ArrayOfValidateData)) {
			foreach ($ArrayOfValidateData as $name => $value) {
				if ($value and "" != $value) {
					if (strpos((string) $name, 'img') !== false) {
						jQuery($name)->attr('src', $value);
					} else {
						jQuery($name)->addClass("mws-error");
						jQuery($name)->html($value);
					}
				} else {
					jQuery($name)->removeClass("mws-error");
					jQuery($name)->html('');
				}
			}
		}
		foreach ($scripts as $script) {
			jQuery::evalScript($script);
		}
		$this->_helper->getHelper('Jquery')->sendResponse();
	}

	/**
	 *	-	Назначение: Редирект и сообщение (AJAX)
	 *	-	Дата: 30.06.2007 10:50:09
	 * @param string URL куда переадресовывать
	 * @param string Сообщение
	 */
	public function _redirectAjaxAction($redirectUrl, $message = null) {
		$this->_helper->getHelper('Jquery');

		if (!is_null($message)) {
			jQuery::evalScript("alert('$message');");
		}
		//print_r($message); die;
		if ($redirectUrl) {
			jQuery::evalScript("window.location='$redirectUrl'");
		}

		$this->_helper->getHelper('Jquery')->sendResponse();
	}

	/**
	 *  Получить конфигурации сайта
	 */
	public function getOptions() {
		$namespace = 'config_site_namespace';
		if (Zend_Registry::isRegistered($namespace))
			return Zend_Registry::get($namespace);
		
		$model_config = api::getConfig();
		$model_lists = api::getLists();
		$setting = $model_config->_new()->fields(array('name', 'value', 'type', 'is_translate'))->rows();

		$list = $model_lists->_new()
				->joinLeft(array("um" => api::LIST_VALUES), "t.id = um.list_id", array("um.id", "um.value"))->rows();
		$settings = array();
		$lists = array();
		$images = array();
		if (count($setting)) {
			foreach ($setting as $row) {
				if ($row->type == Model_Config::TYPE_TEXT) {
					if ($row->is_translate)
						$settings[$row->name] = $this->view->translate($row->value);
					else
						$settings[$row->name] = $row->value;
				} elseif ($row->type == Model_Config::TYPE_IMAGE) {
					$images[$row->name] = $row->value;
				}
			}
		}
		if (count($list)) {
			foreach ($list as $row) {
				if(!is_null($row->value)) {
					if ($row->is_translate)
						$lists[$row->name][$row->id] = $this->view->translate($row->value);
					else
						$lists[$row->name][$row->id] = $row->value;
				}


			}
		}

		$data = array($settings, $lists, $images);
		Zend_Registry::set($namespace, $data);
		return $data;
	}

}