<?php

// Site schema

class DR_Controllers_Site extends DR_Controllers_Default {

	function preDispatch() {
		parent::preDispatch();
		# - Шаблон по умолчанию
		Zend_Layout::getMvcInstance()->setLayout('layout');
	}
	public function indexAction() {

	}

	public function blockmaterialsAction() {
	    $this->view->jsPaginator = $this->_getParam('jsPaginator', '');
		if ($this->_request->isXmlHttpRequest()) {
			Zend_Layout::getMvcInstance()->setLayout('ajax');
            $notFindData = '';
            if(isset($this->view->noFindData) && (!isset($this->view->pageCount) || $this->view->pageCount == 0))
                $notFindData = $this->view->noFindData;
			$body = $this->view
					->partial('blockmaterials.tpl',
							array('data' => $this->view->data, 
                                  'pageCount' => $this->view->pageCount, 
                                  'jsPaginator'=>$this->view->jsPaginator, 
                                  'needTemplate'=>isset($this->view->needTemplate) ? $this->view->needTemplate : null));
			$body = trim(strtr($body, array("\n" => "", "\r" => "", "\t" => "")));
			die(json_encode(array('body' => $body, 
                                    'isNotFind' => '' != $notFindData, 
                                    'notFindData' => $notFindData)));
		} else {
			$this->render("blockmaterials", null, true);
		}
	}

	// выбор типа метаданных
	protected function getType($type) {
		switch ($type) {
			case "list":
				return "list_value_id";
				break;
			case "value":
				return "value";
				break;
			case "text":
				return "text_value";
				break;
			case "file":
				return "file_id";
				break;
			case "date":
				return "date_value";
				break;
			default:
				return "value";
				break;
		}
		return $array;
	}

	#Ответ браузеру jquery php
	public function Response($ArrayOfValidateData, $scripts = array(), $validate_form_selector = '') {
		$this->_helper->getHelper('Jquery');

		if (is_array($ArrayOfValidateData) and count($ArrayOfValidateData)) {
			foreach ($ArrayOfValidateData as $name => $value) {
			     $selector = $validate_form_selector . '[name="' . $name . '"]';
				jQuery($selector)->next()->html('')->parent()
						->removeClass('error')
                        ->removeClass('success');
				if ($value and "" != $value) {
					$value = $this->view->escape($value);
					jQuery($selector)->next()->html($value)->parent()
							->addClass('error');
				} else {
				    //jQuery($selector)->parent()
					//		->addClass('success');
				}
			}
		}
		if (count($scripts)) {
			foreach ($scripts as $script) {
				jQuery::evalScript($script);
			}
		}
		$this->_helper->getHelper('Jquery')->sendResponse();
	}

	protected function setLastUri() {
		$session = new Zend_Session_Namespace;
		if (!isset($session->last_url))
			$session->last_url = $_SERVER['HTTP_REFERER'];
	}
	protected function getLastUri() {
		$session = new Zend_Session_Namespace;
		$uri = "/";
		if (isset($session->last_url)) {
			$uri = $session->last_url;
			unset($session->last_url);
			return $uri;
		}
		return $uri;
	}
}

?>