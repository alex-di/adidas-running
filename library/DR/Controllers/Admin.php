<?php // Admin schema
class DR_Controllers_Admin extends DR_Controllers_Default
{
    // присутсвие в модуля сообственно рендера таблицы
    var $isSelfViewTable = false;

    // Тип пагинатора по умолчанию
    var $pagination_type = "Sliding";

    // ?аблон пагинатора по умолчанию
    var $pagination_template = "paginator.tpl";

    var $location = null;
    var $message = null;
	var $_model = null;
    protected $breadcrumbs = null;
    
    public function init()
    {
        if (is_null($this->_model))
            $this->_model = $this->getModel($this->getRequest()->getControllerName());
    }
    /**
     *   - Назначение: Концигурация админ контроллера
     */
    function preDispatch()
    {
        parent::preDispatch();
        # - Шаблон модуля
        Zend_Layout::getMvcInstance()->setLayout('admin_layout');
    }

    function getBreadcrumbs(){
        if(is_null($this->breadcrumbs)) {
            $this->breadcrumbs = new DR_Api_Admin_Breadcrumbs($this->requestParams);
            $this->view->breadCrumbs = $this->breadcrumbs;
        }
        return $this->breadcrumbs;
    }
    private function getModel($controller)
    {
        $cont = ucfirst($controller);
        $module = "Model_" . $cont;
        if (class_exists($module))
            return new $module;
        return null;
    }
    /**
     * вывод данных
     */
    public function blockdatatableAction()
    {

        if ($this->_request->isXmlHttpRequest())
            Zend_Layout::getMvcInstance()->setLayout('ajax');
        $input_data = $this->_request->getParams();

        $this->view->request_post = $input_data;
        $this->setToMemory($input_data);
        if (isset($input_data['search'])) {
            foreach ($input_data['search'] as $type => $fields) {
                foreach ($fields as $field => $val) {
                    if (!empty($val) || '0' == $val) {
                        $perffix = '';
                        if(strpos($field, '.') === false)
                            $perffix = 't.';
                        if ("number" == $type || "list" == $type) {
                            if (is_array($val)) {
                                $this->_model->in($perffix.$field, $val);
                            } else {
                                $this->_model->where($perffix.$field, $val);
                            }
                        } elseif ("text" == $type) {
                            $this->_model->regexp($perffix.$field, $val);
                        }
                    }
                }
            }
        }
        if (!empty($input_data['order']))
            $this->_model->order($input_data['order']);
        //$this->view->data = $this->_model->rows();
        if (isset($input_data['is_page']) && $input_data['is_page']) {
            list($this->view->data, $this->view->pages) = $this->_model->pageRows($input_data['page'], $input_data['per_page']);
        } else {
            $this->view->data = $this->_model->rows();
            $this->view->pages = null;
        }
        if (!$this->isSelfViewTable) {
            $this->render("blockdatatable", null, true);
        }
    }
    // шаблон сообщения об ошибке в админке
    function showError($message)
    {
        $this->_forward("index", "error", "admin", array("message" => $message));
    }
    /**
     * запомнить необходимые значения экшена
     */
    public function setToMemory($data)
    {
        $namespace = "admin" . $this->requestParams['controller'] . $this->requestParams['action'];
        DR_Api_Admin_Table::putToStoreValue($namespace, $data);
    }
    /**
     * главная
     */
    public function indexAction()
    {
        $this->render("index", null, true);
    }


    /**
     * Удаление
     */
    public function deleteAction()
    {
        if ($id = $this->_getParam('id')) {
            if (is_array($id)) {
                foreach ($id as $i) {
                    $this->_model->doDelete($i);
                }
            } else {
                $this->_model->doDelete($id);
            }
            if (is_null($this->location))
                $this->location = "/admin/" . $this->view->module_params['controller'];
            if ($this->_request->isPost()) {
                die($this->location);
            } else {
                $this->_redirect($this->location);
            }

        } else {
            $this->showError("обьект для удаления не передан");
        }

    }


    /**
     * Редактирование
     */
    public function editAction()
    {
        if ($id = $this->_getParam('id') or $id === 0)
        {
            $this->view->data = $this->_model->where('id', $id)->row();
        } else {
            $this->view->data = $this->_model->createRow();
        }
    }

    /**
     * Сохранение
     */
    public function saveAction()
    {

        list($valid, $data) = $this->isValid($_POST);
 
        /**
         * Если все проверки пройдены, сохраняем
         */
        if ($valid) {
            $newId = $this->_model->doSave($_POST, $this->_getParam('id'));

            if (!is_null($this->location)) {
               
                $this->_redirectAjaxAction($this->location, $this->message);
            }

            if (!is_null($this->locationAtNew)) {
                $this->_redirectAjaxAction($this->locationAtNew . $newId, $this->message);
            }
        }

        $this->Response($data);
    }

    public function Response($ArrayOfValidateData, $scripts = array())
    {
        $this->_helper->getHelper('Jquery');
        $isError = false;
        if (is_array($ArrayOfValidateData) and count($ArrayOfValidateData)) {

            
            foreach ($ArrayOfValidateData as $name => $value) {
            	$selector = "#err_".$name;
                if (!empty($value)) {
                    jQuery($selector)->addClass("mws-error");
                    jQuery($selector)->html($value);
                    $isError = true;
                } else {
                    jQuery($selector)->removeClass("mws-error");
                    jQuery($selector)->html('');
                }
            }
        }
        $message = '<div class="mws-form-message success">Успешно сохранено!</div>';
        if ($isError) {
            $message = '<div class="mws-form-message error">Ошибка!</div>';
        }
        jQuery::evalScript("if(typeof showFormMessages == 'function') showFormMessages('$message');");

        foreach ($scripts as $script) {
            jQuery::evalScript($script);
        }
        $this->_helper->getHelper('Jquery')->sendResponse();
    }

}
