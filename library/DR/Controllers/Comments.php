<?php



class DR_Controllers_Comments extends DR_Controllers_Default
{

    protected $modules_id = null;

    public function saveAction()
    {

        $text = trim($this->_getParam('text'));
        list($valid, $data) = $this->isValid(array('text'=>strtr($text, array("\n"=>"", "\t"=>"", "\r"=>""))));
        $response = array('status'=>'error');
        if(!$valid) {
        	$response['error'] = "Коментарий не может быть пустым";
        	return $response;
        }
        $text = $this->view->escape($text);
        $text = strtr($text, array("\n"=>"<br/>", "\t"=>"", "\r"=>""));
        $resource_id = $this->_getParam('resource');
        $model_votes = api::getComments()->_new(array());
        $values = array(
            'modules_id' => $this->modules_id,
        	'users_id'=>Zend_Auth::getInstance()->getIdentity()->id,
            'resource_id' => $this->_getParam('resource'),
            'text' => $text);
        api::getComments()->doSave($values);

        $response['status'] = 'ok';
        $response['text'] = $text;
		return $response;
    }
}
