<?php



class DR_Controllers_Votes extends Zend_Controller_Action
{

    protected $modules_id = null;

    public function indexAction()
    {

        $namespace = $this->_getParam('namespace');
        $type = $this->_getParam('type', 1);
        $vote_namespace = array(
            Model_Votes::NAMESPACE_VK,
            Model_Votes::NAMESPACE_FB,
            Model_Votes::NAMESPACE_TW,
            Model_Votes::NAMESPACE_GP,
            Model_Votes::NAMESPACE_SITE);
        if (!in_array($namespace, $vote_namespace))
            die("Invalid namespace");
        $resource_id = $this->_getParam('resource');
        $model_votes = api::getVotes()->_new(array('t.id'));
        $values = array(
            'modules_id' => $this->modules_id,
            'resource_id' => $resource_id,
            'social_namespace' => $namespace);
        if (Zend_Auth::getInstance()->hasIdentity())
        {
            $values['users_id'] = Zend_Auth::getInstance()->getIdentity()->id;
            $model_votes->where('t.users_id', $values['users_id']);

        } else
        {
            $values['ip'] = Zend_Controller_Front::getInstance()->getRequest()->getClientIp();
            $model_votes->where('t.users_id = 0');
        }
        $vote = $model_votes->where('t.modules_id', $this->modules_id)->where('t.resource_id', $resource_id)->where('t.social_namespace', $namespace)->row();
        if($type) {
            if(!count($vote) || !Zend_Auth::getInstance()->hasIdentity())
                $model_votes->doSave($values);
        } else {
            if(count($vote))
                $model_votes->doDelete($vote->id);
        }
        die('ok');
    }
}
