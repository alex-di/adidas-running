<?

/**
 * проверка прав доступа 
 */
class DR_Models_Cache
{

    /**
     * @var string directory
     */
    var $directory;

    /**
     * @var string Zend_Cache
     */
    var $object;

    /**
     * @var string name
     */
    var $name;


    /**
     * @var integer Время жизни кеша
     */
    var $life = 1800;

    /**
     * @var instance
     */
    protected static $_instance = null;

    /**
     *	-	Назначение: Конструктор
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     */
    public function __construct($directory, $name = null)
    {
        $this->setDirectory($directory);
        $this->setName($name);

        $frontendOptions = array('lifetime' => $this->life, 'automatic_serialization' => true, 'automatic_cleaning_factor' => 1, 'ignore_user_abort' => true);

        $backendOptions = array('cache_dir' => $this->directory);

        $this->object = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        
        #Удаляем устаревшие
        $this->object->clean(Zend_Cache::CLEANING_MODE_OLD);
        
    }
    


    /**
     *	-	Назначение: Установить директорию
     *	-	Дата: 01.02.2013 10:19:26
     * @param string directory
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    /**
     *	-	Назначение: Установить имя
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     * @param string name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * Singleton pattern implementation
     *
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     *	-	Назначение: Проверить актуален ли кеш 
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     * @return boolean
     */
    public function check()
    {
        if (!$this->isAllow())
        {
            return false;
        }

        if ($this->object->load($this->name, true, true))
        {
            return true;
        }

        return false;
    }


    /**
     *	-	Назначение: Удалить запись
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     * @param string tag
     */
    public function clean($tag = null)
    {
        if(!is_array($tag)) {
            $tag = array($tag);
        }
        $this->object->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, $tag);
    }

    /**
     *	-	Назначение: Очистить кеш
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     */
    public function clear()
    {
        $this->object->clean(Zend_Cache::CLEANING_MODE_ALL);
    }

    /**
     *	-	Назначение: Загрузить данные
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0     
     * @return string 
     */
    public function load()
    {
        if (!$this->isAllow())
        {
            return false;
        }

        return $this->object->load($this->name);
    }

    /**
     *	-	Назначение: Кеш включен?
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     */
    public function isAllow()
    {

    	if(!Zend_Registry::isRegistered('db_cache') || !Zend_Registry::get('db_cache'))
    		return false;

        return true;
    }

    /**
     *	-	Назначение: Сохранить
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     * @param struct data
     * @return void
     */
    public function save($data, $tags = array())
    {
        if (!$this->isAllow())
        {
            return false;
        }

        $this->object->save($data, $this->name, $tags);
    }

    /**
     *	-	Назначение: Выключить КЕШ
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     */
    public static function off()
    {
        Zend_Registry::set('db_cache', false);
    }

    /**
     *	-	Назначение: Включить КЕШ
     *	-	Дата: 01.02.2013 10:19:26
     * @version 1.0
     */
    public static function on()
    {
    	Zend_Registry::set('db_cache', true);
    }


}
