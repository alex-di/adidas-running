<?php

/**
 * Модель - Стандартные функции для всех таблиц
 * @author Appalach
 * @version 1.0
 */
class DR_Models_AbstractTable extends Zend_Db_Table_Abstract
{




    // primary Ключ
    public $primary_key = "";

    // Тип пагинатора по умолчанию
    public $pagination_type = "Sliding";

    // ?аблон пагинатора по умолчанию
    public $pagination_template = "paginator.tpl";


    private $object = null;

    #Обьект кеша
    private $cache = null;

    #Включить кеш
    private $is_cache_enabled = false;

    /**
     * Конструктор
     */
    public function __construct()
    {

        parent::__construct();

        $info = $this->info();
        $this->primary_key = array_shift($info['primary']);

        #Создаем обьект запроса
        $this->object = $this->select();
        
        $this->cache = Zend_Registry::get('cache');
        
        $this->is_cache_enabled = Zend_Registry::get('db_cache');


        if (Zend_Registry::get('db_meta_cache'))
        {
            Zend_Db_Table_Abstract::setDefaultMetadataCache($this->cache->getObject());
        }
    }
    
    public function __invoke($id, $field = null) {
        $field = !is_null($field) ? $field : 'id';
        return $this->_new()->where($field, $id)->row();
       
    }
    
    public function cacheOn(){
        $this->cache->on();
    }
    public function cacheOff(){
        $this->cache->off();
    }
    #Получаем теги для запроса
    public function getTags()
    {
        $string = $this->object->__toString();
        preg_match_all('/(Model_\w*)/i', $string, $m, PREG_PATTERN_ORDER);


        return array_unique($m[1]);
    }

    #печатаем запрос
    public function debug($die = false)
    {
        if ($die)
        {
            print_r($this->object->__toString());
            die;
        }
        return $this->object->__toString();
    }

    #Данные с пагинацией
    public function pageRows($page, $per_page = 10, $row_count = null, $pageRange = 5)
    {
        $data = $this->_cache($page . "_" . $per_page . "_" . $row_count);

        if (is_null($data))
        {
            $adapter = new Zend_Paginator_Adapter_DbSelect($this->object);
            if (!is_null($row_count))
            {
                $adapter->setRowCount(intval($row_count));
            }
            $paginator = new Zend_Paginator($adapter);
            $paginator->setCurrentPageNumber($page)
            ->setPageRange($pageRange)
            ->setItemCountPerPage($per_page)
            ->setDefaultScrollingStyle($this->pagination_type);

            // подключаем шаблон
            $data = array($paginator->getCurrentItems(), $paginator);
            $this->cache->save($data, $this->getTags());
        }

        return $data;
    }
    
    function pageArray($data, $page, $per_page = 10, $pageRange = 5) {
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($data));
        $paginator->setCurrentPageNumber($page)
                  ->setPageRange($pageRange)
                  ->setItemCountPerPage($per_page)
                  ->setDefaultScrollingStyle($this->pagination_type);
        return array($paginator->getCurrentItems(), $paginator);
    }
    #получить одну строку
    public function row()
    {
        $data = $this->_cache();
        if (is_null($data))
        {
            $data = $this->fetchRow($this->object);
            $this->cache->save($data, $this->getTags());
        }
        return $data;
    }

    #Получить все строки
    public function rows()
    {

        $data = $this->_cache();
        if (is_null($data))
        {
            $data = $this->fetchAll($this->object);
            $this->cache->save($data, $this->getTags());
        }
        return $data;
    }


    # функция кеширования
    private function _cache($added_string = '')
    {

        $key = md5($this->object->__toString() . $added_string);

        $this->cache->setName($key);

        if ($this->cache->check() and $this->is_cache_enabled)
        {
            return $this->cache->load();
        } else
        {
            return null;
        }

    }
	public function getStatement(){
		return $this->object;
	}
	public function union($unions_stmt) {
    	$this->object = $this->select()->union($unions_stmt);
    	return $this;
	}
    #Получить слудующее айди в таблице
    public function nextId()
    {
        $db = Zend_Registry::get("mysql");
        $stmt = $db->query("SHOW TABLE STATUS LIKE '$this->_name'");
        $stmt->setFetchMode(Zend_Db::FETCH_ASSOC);
        $data = $stmt->fetchAll();
        return $data[0]['Auto_increment'];
    }
    #Получить все строки в виде масива
    public function forSelect($field = array('id' => 'name'), $empty_field = false)
    {
        $data = $this->rows()->toArray();

        $result = array();
        if($empty_field) {
            $result[""] = "";
        }
        $data_function = each($field);
        if(count($data)) {
            foreach ($data as $one)
            {
                $result[$one[$data_function['key']]] = $one[$data_function['value']];
            }
        }

        return $result;
    }

    #Cоздаем новый обьект запроса
    public function _new($fields = array("t.*"))
    {
        $this->object = $this->select()->from(array("t" => $this->_name), $fields);
        return $this;
    }

    #Выбрать таблицу из которой брать данные
    public function from($from)
    {
    	if(!is_array($from))
    		$from = array('t'=>$from);
       	$this->object = $this->select()->from($from, array());
        $this->object->setIntegrityCheck(false);
        return $this;
    }

    # Произвести выборку из текущей таблицы, инициализация колонок для результата
    private function fromSelf($zeroFields = false)
    {
        $from = $this->object->getPart('from');

        if (!$from)
        {
            if (!$zeroFields)
                $this->object->from(array('t' => $this->_name));
            else
                $this->object->from(array('t' => $this->_name), array());
        }
        return $this;
    }

    # Выбрать колонки для результатов
    public function fields($fields = array())
    {
        $this->fromSelf(true);
        $this->object->columns($fields);
        return $this;
    }

    # Пример всех джойнов
    public function joinLeft($table, $condition, $columns = array())
    {
        $this->fromSelf();
        $this->object->setIntegrityCheck(false);
        $this->object->joinLeft($table, $condition, $columns);
        //echo $this->debug($this->object); die;

        return $this;
    }

    # Пример всех джойнов
    public function joinRight($table, $condition, $columns = array())
    {
        $this->fromSelf();
        $this->object->setIntegrityCheck(false);
        $this->object->joinRight($table, $condition, $columns);
        return $this;
    }

    #получить количество записай
    public function count()
    {
        $this->fromSelf();
        $this->fields(array('count(1) num'));
        return $this->fetchRow($this->object)->num;
    }

    #получить максимальное значение
    public function max($field)
    {
        $this->fromSelf();
        $this->fields(array(' max(' . $field . ') max'));
        return $this->fetchRow($this->object)->max;
    }

    #имплементация where
    public function where($field, $value = null, $sign = "=")
    {
        $this->fromSelf();
        if (is_null($value))
            $this->object->where("$field");
        else
            $this->object->where("$field $sign ?", $value);
        return $this;
    }

    #имплементация between
    public function between($field, $from, $to, $signleft = ">", $signRight = "<")
    {
        $this->fromSelf();
        $this->object->where("$field $signleft ?", $from)->where("$field $signRight ?", $to);
        return $this;
    }

    #имплементация  IN
    public function in($field, $conditions)
    {
        $this->fromSelf();
        $this->object->where("$field IN(?)", $conditions);
        return $this;
    }

    #имплементация  NOT IN
    public function notIn($field, $conditions)
    {
        $this->fromSelf();
        $this->object->where("$field NOT IN(?)", $conditions);
        return $this;
    }

    #имплементация  IN c запросом в качестве аргумента
    public function queryIn($field, $conditions)
    {
        $this->fromSelf();
        $this->object->where("$field IN(?)", new Zend_Db_Expr($conditions));
        return $this;
    }

    #имплементация  NOT IN c запросом в качестве аргумента
    public function queryNotIn($field, $conditions)
    {
        $this->fromSelf();
        $this->object->where("$field NOT IN(?)", new Zend_Db_Expr($conditions));
        return $this;
    }
    
    #имплементация  exists c запросом в качестве аргумента
    public function exists($conditions)
    {
        $this->fromSelf();
        $this->object->where("EXISTS(?)", new Zend_Db_Expr($conditions));
        return $this;
    }
    
    #имплементация  not exists c запросом в качестве аргумента
    public function notExists($conditions)
    {
        $this->fromSelf();
        $this->object->where("NOT EXISTS(?)", new Zend_Db_Expr($conditions));
        return $this;
    }

    #имплементация  like
    public function like($field, $value)
    {
        $this->fromSelf();
        $this->object->where("$field LIKE ?", $value);
        return $this;
    }

    #имплементация  regexp
    public function regexp($field, $value)
    {
        $this->fromSelf();
        $this->object->where("LOWER($field) REGEXP LOWER(?)", $value);
        return $this;
    }

    # Группировка
    public function group($field)
    {
        $this->fromSelf();
        $query = $this->object->group($field);
        return $this;
    }

    #Ограничить количество
    public function limit($count)
    {
        $this->fromSelf();
        $this->object->limit($count);
        return $this;
    }

    public function limitPage($offset, $limit)
    {
        $page = $offset * $limit;

        $this->fromSelf();
        $this->object->limit($limit, $page);
        return $this;
    }

    #Сортировка
    public function order($conditions)
    {
        $this->fromSelf();
        $this->object->order($conditions);
        return $this;
    }
    /**
     * получить уникальную запись для поля 
     */
    public function stitleUnique($stitle, $id = null, $field = 'stitle')
    {
        $stitle = $stitle ? $stitle : '_';
        $stitle_n = $stitle;
        $stitle_p = 0;
        do
        {
            $stitle_n = $stitle . ($stitle_p == 0 ? '' : $stitle_p);
            $this->_new()->where($field, $stitle_n);
            if (!is_null($id))
                $this->where("id", $id, '!=');
            $stitle_c = $this->count();
            $stitle_p++;
        } while ($stitle_c > 0);
        return $stitle_n;
    }

    /**
     * Выполнить запрос
     */
    public function executeQuery($query)
    {
        $db = Zend_Registry::get("mysql");
        $db->query($query);
    }

    /**
     * Удалить обьект по ид
     */
    public function doDelete($id)
    {
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        $this->cache->clean($this->_name);
        $this->delete($where);

    }

    /**
     *	-	Назначение: Сохранить
     *	-	Дата: 28.06.2007 15:23:20
     * @param array Данные
     * @param integer Ид
     * @return id
     * @version 1.9
     * @final
     */
    public function doSave($values, $id = null, $returnData = false)
    {

        
        if (!is_null($id) and $id != 'undefined')
        {
            $row = $this->fetchRow($this->select()->where($this->primary_key . ' = ?', $id));
        } else
        {
            $row = $this->createRow();
        }
        $row->setFromArray($values);

        foreach ($values as $name => $value)
        {
            if (strpos($name, '+') !== false)
            {
                $row->{str_replace("+", "", $name)} += $value;
            }
        }

        $row->save();
        $this->cache->clean($this->_name);

        if ($returnData)
        {
            return $row;
        }

        return $row->{$this->primary_key};
    }
    /**
     *	-	Назначение: Существует ли значение
     *	-	Дата: 30.06.2007 10:54:29
     * @param struct Фильтер
     * @return boolean Существует ли поле
     * @final
     */
    public function checkRow($field, $value)
    {
        return $this->where($field, $value)->count();
    }


    public function clearCache() {
        $this->cache->clean($this->_name);
    }
    
    
    public function getColumns()
    {
        $info =  $this->info(Zend_Db_Table_Abstract::COLS);
        $result = array();
        foreach($info as $field) {
            $result[$field] = $field;
        }
        return $result;
    }

}

?>