<?php

# Клас для работы с ACL доступами
class DR_Models_Default_Permissions extends DR_Models_AbstractTable {
    
   protected $_name = 'Model_Permissions';
   
   public function savePermissions($allowed, $group) {
        $data = $this->where("group_id", $group)->rows();
        foreach($data as $row) {
            $row->delete();
        }
        if(count($allowed)) {
            foreach($allowed as $res) {
                $this->doSave(array("group_id"=>$group, "resource_id"=>$res));
            }
        }
        
   }
    
}