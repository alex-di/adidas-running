<?php

class DR_Models_Default_Controlsidebar extends DR_Models_AbstractTable
{
    protected $_name = 'Model_Controlsidebar';

    public function getMenu()
    {
        $data = $this
            ->_new()
            ->where('t.parent_id', 0)
            ->joinLeft(
                array("cs" => $this->_name), 
                "cs.parent_id = t.id",
                array(
                    "child_title" => "cs.title",
                    "child_pos" => "cs.pos",
                    "child_id" => "cs.id",
                    "child_action" => "cs.action",
                    "child_class" => "cs.class")
        )->rows();
        
        
        $result = array();
        if (count($data))
        {
            foreach ($data as $row)
            {
                $result[$row->pos]['parent'] = $row;
                if (!isset($result[$row->pos]['child']))
                    $result[$row->pos]['child'] = array();
                if (!is_null($row->child_id))
                {
                    $result[$row->pos]['child'][$row->child_pos] = $row;
                }
            }
        }
        ksort($result);
        return $result;
    }
    public function doSaveItem($data)
    {
        $id = null;
        if (!empty($data['id']))
        {
            $id = $data['id'];
        } else
        {
            $row = $this->_new()
                ->order('pos DESC')->fields(array('pos'))->rows();
            //$row = $this->getSimpleData(array("order" => "pos DESC"), array("pos"));
            $data['pos'] = 1;
            if (count($row))
            {
                $data['pos'] = $row->offsetGet(0)->pos + 1;
            }
        }
        $data['class'] .= " ".$data['resource'];
        unset($data['id'], $data['resource']);
        if (empty($data['title']))
            $data['title'] = "No title";

        $this->doSave($data, $id);
    }
    public function doDeleteItem($id){
        $this->doDelete($id);
        $data = $this->_new()->where('parent_id', $id)->rows();

        if(count($data)) {
            foreach($data as $row) {
                $row->delete();
            }
        }
    }
    public function move($first, $two)
    {
        $data = $this->_new()
            ->in('id', array($first, $two))->rows();
        
        
        //$data = $this->getSimpleData(array("where" => array("id" => array("type" => "in", "value" => array($first, $two)))));
        if (count($data))
        {
            $first = $data->offsetGet(0);
            $two = $data->offsetGet(1);
            $pos = $first->pos;
            $first->pos = $two->pos;
            $two->pos = $pos;
            $first->save();
            $two->save();
        }
    }
}
