<?php

# Клас для работы с ACL ресурсами
class DR_Models_Default_Aclresources extends DR_Models_AbstractTable
{

    protected $_name = 'Model_Aclresources';

    public function addResource($resources)
    {
        $data = $this->_new()->rows();
        $result = array();
        $delete_resource = array();
        foreach ($data as $row)
        {
            $result[$row['value']] = $row['id'];
            $delete_resource[$row['id']] = 1;
        }
        foreach ($resources as $resource => $description)
        {
            // если такого ресурса нет
            if (!isset($result[$resource]))
            {
                $this->doSave(array("value" => $resource, "description" => $description));
            } else
            {
                // ресурс присутствует в БД. обновляем описание
                $this->doSave(array("description" => $description), $result[$resource]);
                unset($delete_resource[$result[$resource]]);
            }
        }
        if (count($delete_resource))
        {
            foreach ($delete_resource as $id => $v)
            {
                $this->doDelete($id);
            }
        }
    }
    public function getResource($group_id)
    {
        $data = $this->_new()->fields(array(
            "*",
            "(select count(1) from Model_Permissions where group_id = $group_id and resource_id = t.id) as is_allowed"))
            ->order("value ASC")->rows();
        $result = array();
        $is_allowed = array();
        foreach ($data as $row)
        {
            $resource = explode(":", $row->value);
            $result[$resource[0]][$resource[1]][$resource[2]] = $row;
            if ($row->is_allowed)
            {
                $is_allowed[] = $row->id;
            }
        }
        return array("data" => $result, "allowed" => $is_allowed);
    }

}
