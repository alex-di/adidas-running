<?php

/**
 * помощник для построения спискового фильтра
 */
class DR_Api_Admin_FilterList
{
    protected $isSelectView;
    protected $isMultiply;
    protected $field_name;
    protected $list = array();

    public function __construct($list, $field_name = null, $isMultiply = false, $isSelectView = true)
    {
        $this->list = $list;
        $this->isSelectView = $isSelectView;
        $this->field_name = $field_name;
        $this->isMultiply = $isMultiply;
    }

    public function getHtml($name, $value)
    {
        if (!is_null($this->field_name))
            $name = $this->field_name;

        if ($this->isSelectView)
        {
            $name = "search[list][$name]";
            if ($this->isMultiply)
                $name .= '[]';
            return Zend_Layout::getMvcInstance()->getView()->formSelect($name, $value, array("class" => "search_field"), $this->list, "<br />\n", "Не выбрано");
        } else
        {
            $checkbox = array();
            foreach ($this->list as $key => $val)
            {
                $checkbox[] = "<input name='search[list][$name][]' type='checkbox' value='$key' " . (in_array($key, $value) ? "checked='checked'" : "") . " class='search_field'/>$val";
            }
            return implode(" ", $checkbox);
        }
    }

    public function getFieldName($field_name)
    {
        if (!is_null($this->field_name))
            $field_name = $this->field_name;
        return $field_name;
    }
}
