<?php

class DR_Api_Admin_Breadcrumbs
{
    const PREFIX_VIEW = "Просомтр ";
    const PREFIX_EDIT = "Редактирование ";
    const PREFIX_ADD = "Добавления ";
    protected $data = array();
    protected $default = array(array('url' => '/admin', 'name' => 'Главная'));
    protected $requestParams = array();

    public function __construct($request_params)
    {
        $this->data = $this->default;
        $this->requestParams = $request_params;
    }
    public function reset()
    {
        $this->data = $this->default;
        return $this;
    }
    public function append($name, $url)
    {
        $this->data[] = array('url' => '/admin/'.$url, 'name' => $name);
        return $this;
    }

    public function appendView($name = null, $url = null)
    {
        
        if(!is_null($name)) {
            $name = $name;
        } else {
            $name = self::PREFIX_VIEW;
        }

        if (is_null($url))
        {
            $url = '/'.$this->requestParams['module'] . '/' . $this->requestParams['controller'];
        }
        $this->data[] = array('url' => $url, 'name' => $name);
        return $this;
    }
    public function appendEdit($name = null, $url = null, $checkField = 'id')
    {
        if(is_null($name)) {
            if(isset($this->requestParams[$checkField]) && !empty($this->requestParams[$checkField])) {
                $name = self::PREFIX_EDIT;
            } else {
                $name = self::PREFIX_ADD;
            }
        }
        if(is_null($url)) {
            $url = '';
        }
        $this->data[] = array('url' => $url, 'name' => $name);
        return $this;
    }
    public function getBreadcrumbs(){
        return $this->data;
    }
}
