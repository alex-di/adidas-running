<?php

/**
 * помощник для построения целочисельного фильтра
 */
class DR_Api_Admin_FilterNumber
{
    protected $field_name = null;

    public function __construct($field_name = null)
    {
        $this->field_name = $field_name;
    }

    public function getHtml($name, $value)
    {
        if (!is_null($this->field_name))
            $name = $this->field_name;
        return "<input type='text' name='search[number][$name]' value='$value' class='filter_number search_field' />";
    }
    
    public function getFieldName($field_name)
    {
        if (!is_null($this->field_name))
            $field_name = $this->field_name;
        return $field_name;
    }
}
