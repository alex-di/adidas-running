<?php

class DR_Api_Admin_Template
{
    #is_page - режим пагинации. не обьязательный параметр. по умолчанию выключена
    const PAGINATOR = 'is_page';
    #is_mass_check - чекбокс массового выбора обьектов. не обьязательный параметр. по умолчанию отключен
    const MASS_CHECK = 'is_mass_check';
    #is_option_coll - столбец для операций с обьектом. не обьязательный параметр. по умолчанию отключен
    const OPTIONS_COLUMN = 'is_option_coll';
    #toolbar - меню кнопок таблицы. не обьязательный параметр.
    const TOOLBAR = 'toolbar';
        const TOOLBAR_TITLE = 'title'; #title - название кнопки. не обьязательный параметр.
        const TOOLBAR_ACTION = 'action'; #action - урл ссылка. не обьязательный параметр.
        const TOOLBAR_HANDLER = 'handler'; #handler - ф-ция, которая срабатывает, при нажатии на кнопку. не обьязательный параметр.
        const TOOLBAR_CLASS = 'class'; #class - css - отображение. не обьязательный параметр.
    #fields - столбци таблицы
    const FIELDS = 'fields';
        const FIELDS_NAME = 'name';  #name - название столбца.
        const SORTING = 'is_sort';   #is_sort - сортировка по столбцу. не обьязательный параметр. по умолчанию включено
        const FILTRATION = 'filter'; #filter - поиск по одному из столбцов. не обьязательный параметр.
            const FILTER_STRING = "DR_Api_Admin_FilterString";
            const FILTER_LIST = "DR_Api_Admin_FilterList";
            const FILTER_NUMBER = "DR_Api_Admin_FilterNumber";
    
    #имя таблици
    public $name = '';
    # масив - обьект параметров
    public $object = array();
    
    
    
    
    
    #--------------------------------
    public static function getCheckboxes() {
        return array(
            self::PAGINATOR => "режим пагинации",
            self::MASS_CHECK => "чекбокс массового выбора обьектов",
            self::OPTIONS_COLUMN => "столбец для операций с обьектом",
        );
    }
    
    public static function getFiltersType() {
        return array(
            self::FILTER_STRING => "Фильтр строковому значению",
            self::FILTER_LIST => "Фильтр по списковому значению",
            self::FILTER_NUMBER => "Фильтр по числовому значению",
        );
    }
    #-------------------------------
    public function __construct($name = null) {
        if(!is_null($name)) {
            $this->setName($name);
        }
    }
    

    
    #установить имя таблици
    public function setName($name) {
        $this->name = $name;
    }
    
    public function getTables($params) {
        $params = unserialize($params);
        
        if(isset($params['checkboxes'])) {
            $this->getSettings($params['checkboxes']);
        }
       
        if(isset($params['fields']) ) {
            $this->getFields($params);
        }
        
        $result = array(
            $this->name => $this->object
        );
        
        return $result;
        //
    }
    
    
    public function getSettings($checkboxes = array()) {
        if(count($checkboxes)) {
            foreach($checkboxes as $checkbox) {
                $this->object[$checkbox] = true;
            }
        }
    }
    
    
    public function getFields($fields = array()) {
        $columns = $fields['fields'];
        if(count($columns)) {
            foreach($columns as $field) {
                //_d($fields['field_filter'][$field]);
                #тайтл поля
                $this->object['fields'][$field] = array('name' => $fields['field_names'][$field]);
                #сортировка по полю
                if(isset($fields['fields_sort'][$field])) {
                    $this->object['fields'][$field][self::SORTING] = true;
                }
                else {
                    ///_d($field);
                    $this->object['fields'][$field][self::SORTING] = false;
                }
                
                if(isset($fields['field_filter'][$field])) {
                    $class_name = $fields['field_filter'][$field];
                    
                    $this->object['fields'][$field][self::FILTRATION] = new $class_name;
                }
            }
        }
        //_d($this->object);
    }
    
    
    
    /* toolbar - меню кнопок таблицы. не обьязательный параметр.
         *     title - название кнопки. не обьязательный параметр.
         *     action - урл ссылка. не обьязательный параметр.
         *     handler - ф-ция, которая срабатывает, при нажатии на кнопку. не обьязательный параметр.
         *     class - css - отображение. не обьязательный параметр.
         * fields - столбци таблицы
         *      name - название столбца.
         *      is_sort - сортировка по столбцу. не обьязательный параметр. по умолчанию включено
         *      filter - поиск по одному из столбцов. не обьязательный параметр.
         * params - дополнительные параметры
            $this->view->tables = array("Test" => array(
                    "data_action" => "default/blockdatatable",
                    "is_page" => true,
                    "is_mass_check" => true,
                    "is_option_coll" => true,
                    "toolbar" => array(
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_ADD => array(),
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_MASSDELETE => array(),
                        DR_Api_Admin_Table::TOOLBAR_BUTTON_OTHER => array(
                            "title" => "Other",
                            "action" => "",
                            "handler" => "",
                            "class" => "")),
                    "fields" => array(
                        "id" => array(
                            "name" => "ID",
                            "is_sort" => false,
                            "filter" => new DR_Api_View_Admin_FilterNumber()),
                        "email" => array("name" => "E-mail", "filter" => new DR_Api_View_Admin_FilterString()),
                        "group_id" => array("name" => "Група", "filter" => new DR_Api_View_Admin_FilterString()),
                        "name" => array("name" => "Имя"),
                        "sname" => array("name" => "Фамилия"),
                        "date_reg" => array("name" => "Дата регистрации")),
                    "params" => array("parent_id"=>1, "group_id"=>array(1,2,3))
                    ));
         */
    
    
}