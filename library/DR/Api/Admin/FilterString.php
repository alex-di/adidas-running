<?php

/**
 * помощник для построения строкового фильтра
 */
class DR_Api_Admin_FilterString
{
    protected $field_name = null;

    public function __construct($field_name = null)
    {
        $this->field_name = $field_name;
    }

    public function getHtml($name, $value)
    {
        if (!is_null($this->field_name))
            $name = $this->field_name;
        return "<input type='text' name='search[text][$name]' value='$value' class='search_field' />";
    }

    public function getFieldName($field_name)
    {
        if (!is_null($this->field_name))
            $field_name = $this->field_name;
        return $field_name;
    }
}
