<?php class DR_Api_Admin_Table
{
    const TOOLBAR_BUTTON_ADD = -1;
    const TOOLBAR_BUTTON_MASSDELETE = -2;
    const TOOLBAR_BUTTON_OTHER = 0;
    
    const MESSAGE_SUCCESS = 1;
    const MESSAGE_ERROR = 2;
    
    const SESSION_NAMESPACE = "admin_table";

    public static function getSettingsForToolBar($title, $data = array(), $default_handler = "", $default_class = "ic-cross")
    {
        $result = array(
            "title" => $title,
            "href" => "javascript:void(0)",
            "handler" => $default_handler,
            "class" => $default_class);
        if (isset($data['action'])) {
            $result["href"] = $data['action'];
            $result["handler"] = "";
        }
        if (isset($data['handler'])) {
            $result["handler"] = $data['handler'];
            $result["href"] = "javascript:void(0)";
        }
        if (isset($data['class'])) {
            $result["class"] = $data['class'];
        }
        //_d($result);
        return $result;
    }

    public static function getListPerPage()
    {
        return array(
            10 => 10,
            25 => 25,
            50 => 50,
            100 => 100,
            200 => 200);
    }

    public static function putToStoreValue($namespace, $value)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        $session->{$namespace} = $value;
    }
    public static function getFromStorePerPage($namespace)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['per_page']))
            return $session->{$namespace}['per_page'];
        return null;
    }
    public static function getFromStorePage($namespace)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['page']))
            return $session->{$namespace}['page'];
        return 0;
    }
    public static function getFromStoreOrder($namespace)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['order']))
            return $session->{$namespace}['order'];
        return "";
    }
    public static function getFromStoreSearchNumberValue($namespace, $name)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['search']['number'][$name]))
            return $session->{$namespace}['search']['number'][$name];
        return "";
    }
    public static function getFromStoreSearchTextValue($namespace, $name)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['search']['text'][$name]))
            return $session->{$namespace}['search']['text'][$name];
        return "";
    }
    public static function getFromStoreSearchListValue($namespace, $name)
    {
        $session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        if (isset($session->{$namespace}['search']['list'][$name]))
            return $session->{$namespace}['search']['list'][$name];
        return array();
    }

    public static function getTableParams($table, $namespace)
    {
        $collspan = 0;
        $params = array();
        if (isset($table['params']))
            $params = $table['params'];
        if (isset($table['is_mass_check']) && $table['is_mass_check']) {
            $params['is_mass_check'] = 1;
            $collspan++;
        }
        if (isset($table['is_option_coll']) && $table['is_option_coll']) {
            $params['is_option_coll'] = 1;
            $collspan++;
        }
        if (isset($table['fields'])) {
            $params['fields'] = array();
            $params['collspan'] = count($table['fields']) + $collspan;
            foreach ($table['fields'] as $colum => $field) {
                $params['fields'][] = $colum;
                if (isset($field['filter'])) {
                    $search_field = $field['filter']->getFieldName($colum);
                    if ($field['filter'] instanceof DR_Api_View_Admin_FilterNumber) {
                        $params['search']['number'][$colum] = DR_Api_View_Admin_Table::getFromStoreSearchNumberValue($namespace, $field['filter']->getFieldName($colum));
                    } elseif ($field['filter'] instanceof DR_Api_View_Admin_FilterString) {
                        $params['search']['text'][$colum] = DR_Api_View_Admin_Table::getFromStoreSearchTextValue($namespace, $field['filter']->getFieldName($colum));
                    } elseif ($field['filter'] instanceof DR_Api_View_Admin_FilterList) {
                        $value = DR_Api_View_Admin_Table::getFromStoreSearchListValue($namespace, $field['filter']->getFieldName($colum));
                        if (!empty($value))
                            $params['search']['list'][$search_field] = $value;
                    }
                }
            }
        }
        $per_page = self::getFromStorePerPage($namespace);
        $params['is_page'] = true;
        if (isset($table['is_page']) && $table['is_page'])
            if (is_null($per_page))
                $params['per_page'] = 10;
            else
                $params['per_page'] = $per_page;
        else 
            $params['is_page'] = false;
        $params['order'] = self::getFromStoreOrder($namespace);
        $params['page'] = self::getFromStorePage($namespace);
        //_d($params);
        return $params;

    }
}
