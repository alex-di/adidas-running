<?php

class DR_Api_Admin_EditForm
{

    private $data = array();
    private $view = null;
    public function __construct($edit_data = array())
    {
        //_d($edit_data);
        $this->data = $edit_data;
        $this->view = Zend_Layout::getMvcInstance()->getView();
    }
    public function partial($tpl, $data) {
        return $this->view->partial($tpl, $data);
    }
    /**
     * формируем инпут для ввода целочисельных типов
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function numberInput($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formText($name, $default_value, array("class" => "filter_number mws-textinput")) . $this->formErrorField($field);
    }
    /**
     * формируем инпут
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function stringInput($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]) and $this->data[$field] != '')
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formText($name, $default_value, array("class" => "mws-textinput")) . $this->formErrorField($field);
    }
    
        
    /**
     * формируем нередактируемую строку
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $default_value - значение по умолчанию
     */
    public function label($field, $default_value = "")
    {
        if (isset($this->data[$field]))
            $default_value = $this->data[$field];
        return $default_value;
    }
    
    /**
     * формируем чекбокс
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $checked_value - активное значение 
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function checkbox($field, $checked_value, $name = "", $default_value = 0)
    {
        $name = empty($name) ? $field : $name;
        return $this->view->formCheckbox($name, null, array('checked' => $this->data[$field] == $checked_value), array($checked_value, $default_value)) . $this->formErrorField($field);
    }
    
    /**
     * формируем мультичекбокс
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $list - список значений key => value
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function multicheckbox($field, $list = array(), $name = "", $default_value = array(), $scripts = array())
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formMultiCheckbox($name, $default_value, $scripts, $list) . $this->formErrorField($field);
    }
    
    /**
     * формируем скрытое поле
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function hidden($field, $name = "", $default_value = "")
    {
        
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
       
        return $this->view->formHidden($name, $default_value) . $this->formErrorField($field);
    }
    /**
     * формируем поле для ввода даты
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function date($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formText($name, $default_value, array("class" => "date_field mws-textinput")) . $this->formErrorField($field);
    }
    /**
     * формируем поле для ввода даты
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function datetime($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formText($name, $default_value, array("class" => "datetime_field mws-textinput")) . $this->formErrorField($field);
    }
    /**
     * формируем поле для пароля
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function password($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return '<input type="password" class="mws-textinput" name="'.$name.'" value="'.$default_value.'"/>' . $this->formErrorField($field);
        //return $this->view->formPassword($name, $default_value, array("class" => "mws-textinput")) . $this->formErrorField($field);
    }
    /**
     * формируем список (комбобокс)
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $list - список
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function select($field, $list = array(), $name = "", $default_value = array(), $scripts = array())
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formSelect($name, $default_value, $scripts, $list) . $this->formErrorField($field);
    }
    /**
     * формируем текстовое поле (текстареа)
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function textarea($field, $name = "", $default_value = "")
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->formTextarea($name, $default_value) . $this->formErrorField($field);
    }
    /**
     * формируем стилизирований редактор
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function spaw($field, $name = "", $default_value = "", $mode = 'standart', $width = 485, $height = 200)
    {
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->spaw($name, $default_value, $mode, $width, $height) . $this->formErrorField($field);
    }
    /**
     * формируем стилизирований редактор
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $name - название поля для формы. например meta[data][value][name]. по умолчанию берётся $field
     * @param $default_value - значение по умолчанию
     */
    public function tiny($field, $name = "", $default_value = "", $options = array())
    {
        if(!isset($options['plugins']))
            $options['plugins'] = array("advlist autolink lists link admin_filemanager charmap print preview hr anchor pagebreak",
                                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                                "paste textcolor"
                                        );
        if (isset($this->data[$field]) and !empty($this->data[$field]))
            $default_value = $this->data[$field];
        $name = empty($name) ? $field : $name;
        return $this->view->tiny($name, $default_value, $options) . $this->formErrorField($field);
    }
    /**
     * формируем загрузчик файлов
     * @param $field - название поля. так же по названию этого поля берутся данные из выборки
     * @param $module - название модуля, откудова загрузчик будет брать настройки
     * @param $options - опции загрузчика
     */
    public function upload($field, $module, $options = array()){
        
        $isMultiply = strpos($field, "[]") !== false;
        $field_name = str_replace("[]", "", $field);
        $button_id = "upload_button_$field_name";
        $html = "<input id='$button_id' type='button' class='mws-button green' value='Загузить'/>";
        $html .= '<div class="image_block">';
        if(isset($this->data[$field]) and !empty($this->data[$field])) {
            if($isMultiply) {
                
            } else {
                $html .= '<div class="image">'.
                                '<img src="'.$this->data[$field].'">'.
                                '<input type="hidden" name="'.$field_name.'" value="'.$this->data[$field].'">'.
                            '</div>';
            }
        } else {
            if($isMultiply) {
                
            } else {
                $data_field = isset($this->data[$field]) ? $this->data[$field] : '';
                $html .= '<div class="image">'.
                                '<input type="hidden" name="'.$field_name.'" value="'. $data_field . '">'.
                            '</div>';
            }
        }
        $html .= '</div>';
        if(!isset($options['fileUploaded'])) {
            $html .= '<script type="text/javascript">';
            $callback_name = $field_name.'Callback';
            if($isMultiply) {
                
            } else {
                $html .= " function $callback_name(up, file, response) {callbackUpload('$field_name', false, response, '$button_id');}";
            }
            $html .= '</script>';
            $options['fileUploaded'] = $callback_name;
        }
        if(!isset($options['error']))
            $options['error'] = 'callbackUploadError';
        return $html . $this->view->upload($button_id, $module, 'flash', $options).$this->formErrorField($field_name);
         
    }
    private function formErrorField($field)
    {
        return '<div id="err_' . $field . '"></div>';
    }
}
