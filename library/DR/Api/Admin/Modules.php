<?php

class DR_Api_Admin_Modules
{
    const MODULE_EXIST = 1;
    const VIEWS_EXIST = 2;
    const CONTROLLER_ADMIN_EXIST = 3;
    const CONTROLLER_SITE_EXIST = 4;
    
    private $template_site_controller = '';
    private $template_admin_controller = '';
    private $template_model = '';
    private $template_bootstrap = '';
    private $module = '';
    
    private $options;
    
    function __construct($module, $options = array()) {
        $this->module = $module;
        $this->options = $options;
        $this->template_site_controller = ROOT_PATH . '/library/DR/Api/Templates/SiteController.php';
        $this->template_model = ROOT_PATH . '/library/DR/Api/Templates/Model.php';
        $this->template_admin_controller = ROOT_PATH . '/library/DR/Api/Templates/AdminController.php';
        $this->template_bootstrap = ROOT_PATH . '/library/DR/Api/Templates/Bootstrap.php';
    }
    
    private function checkOption($key) {
        if(in_array($key,$this->options)) {
            return true;
        } else {
            return false;
        }
    }
    
    # Создаем новый модуль или обновляем старый
    public function createModuleStructure()
    {
        $new_module = APPLICATION_PATH . "/modules/" . $this->module;
        if(!is_dir($new_module)) {
            mkdir($new_module, 0755);
        }
        if($this->checkOption(self::CONTROLLER_SITE_EXIST) || $this->checkOption(self::CONTROLLER_ADMIN_EXIST)) {
            $this->createController($new_module);
        }
        if($this->checkOption(self::MODULE_EXIST)) {
            $this->createModels($new_module);
        }
        //$this->createView($new_module);
    }
    
    #Создаем контроллеры
    public function createController($path) {
        $controllers_path = $path . "/controllers";
        if(!is_dir($controllers_path)) {
            mkdir($controllers_path, 0755);
        }
        if($this->checkOption(self::CONTROLLER_SITE_EXIST)) {
            $this->makeSiteController($controllers_path);
        }
        if($this->checkOption(self::CONTROLLER_ADMIN_EXIST)) {
            $this->makeAdminController($this->module);
        }
    }
    
    #Создаем модели
    public function createModels($path) {
        $model_path = $path . "/models";
        if(!is_dir($model_path)) {
            mkdir($model_path, 0755);
        }
        $this->makeModel($model_path);
        $this->makeBootstrap($path);
        
    }
    
    #создаем вид
    public function createView($path) {
        $view_path = $path . '/views';
        if(!is_dir($view_path)) {
            mkdir($view_path, 0755);
            mkdir($view_path . '/scripts', 0755);
        }
    }
    
    #создаем контроллер для сайта
    public function makeSiteController($path, $name = 'index') {
        $controller_path = $path . "/" . ucfirst($name) . 'Controller.php';
        if(!file_exists($controller_path)) {
            $search_patterns = array("{module}", "{controller}");
            $replace_patterns = array(ucfirst($this->module), ucfirst($name));
            $file_template = file_get_contents($this->template_site_controller);
            $file = str_replace($search_patterns, $replace_patterns, $file_template);
            file_put_contents($controller_path, $file);
        }
        if($this->checkOption(self::VIEWS_EXIST)) {
            $this->makeControllerView($path . "/../views/scripts/" . $name);
        }
    }  
    
    #создаем контроллер для админки
    public function makeAdminController($name) {
        $controller_path = APPLICATION_PATH . '/modules/admin/controllers/'. ucfirst($name) . 'Controller.php';
        if(!file_exists($controller_path)) {
            $search_patterns = array("{module}");
            $replace_patterns = array($name);
            $file_template = file_get_contents($this->template_admin_controller);
            $file = str_replace($search_patterns, $replace_patterns, $file_template);
            file_put_contents($controller_path, $file);
        }
        
    }
    
    
    #создаем контроллер для бутстрапа
    public function makeBootstrap($path) {
        $bootstrap_path = $path. '/Bootstrap.php';
        if(!file_exists($bootstrap_path)) {
            $search_patterns = array("{module}");
            $replace_patterns = array(ucfirst($this->module));
            $file_template = file_get_contents($this->template_bootstrap);
            $file = str_replace($search_patterns, $replace_patterns, $file_template);
            file_put_contents($bootstrap_path, $file);
        }
        
    }
    
    #создаем модель
    public function makeModel($path) {
        $model_path = $path . "/" . ucfirst($this->module) . '.php';
        if(!file_exists($model_path)) {
            $search_patterns = array("{model}");
            $replace_patterns = array(ucfirst($this->module));
            $file_template = file_get_contents($this->template_model);
            $file = str_replace($search_patterns, $replace_patterns, $file_template);
            file_put_contents($model_path, $file);
        }
        
    }
    
    #создаем папку вида для контроллера
    public function makeControllerView($path) {
        if(!is_dir($path)) {
            mkdir($path, 0755, true);
        }
    }
    
    
    #Удаляем модуль
    public function deleteAll() {
        
        $admin_controller = APPLICATION_PATH . "/modules/admin/controllers/" . ucfirst($this->module) . "Controller.php";
        
        if(file_exists($admin_controller)) {
            unlink($admin_controller);
        }
        
        $module_folder = APPLICATION_PATH . "/modules/" . $this->module;
        if(is_dir($module_folder)) {
            $this->rmdir_recursive($module_folder);
        }
        
    }
    
    
    private function rmdir_recursive($dir) {
        $files = scandir($dir);;
        array_shift($files); // remove ‘.’ from array
        array_shift($files); // remove ‘..’ from array
        
        foreach ($files as $file) {
            $file = $dir . '/' . $file;
            if (is_dir($file)) {
                $this->rmdir_recursive($file);
                if (is_dir($file))
                    rmdir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }
}
