<?php

class DR_Api_Tools_Auth{
	const NAMESPACE_SOCIAL_DATA = "namespace_social_data";
    
    public static function putToStoreData($input) {
  		$session = new Zend_Session_Namespace(self::NAMESPACE_SOCIAL_DATA);
        $data = array();
        if(isset($session->data))
            $data = $session->data;
        $data['name'] = $input['name'];
        $data['sname'] = $input['sname'];
        $data['email'] = $input['email']; 
        $data['password'] = $input['password'];
        $data['password2'] = $input['password2']; 
        $data['sex'] = $input['sex']; 
        $data['bday'] = $input['bday']; 
        $data['bmonth'] = $input['bmonth'];
        $data['byear'] = $input['byear']; 
        $data['city'] = $input['city']; 
        $data['avatar'] = $input['avatar'];
        $data['accept_rules'] = $input['accept_rules']; 
        if(!isset($input['fb_id']) && isset($data['fb_id']))
            unset($data['fb_id']);
        if(!isset($input['vk_id']) && isset($data['vk_id']))
            unset($data['vk_id']);
        if(!isset($input['tw_id']) && isset($data['tw_id']))
            unset($data['tw_id']);
        $session->data = $data;
    }
	public static function putToStoreSocialData($input = array()) {
		$session = new Zend_Session_Namespace(self::NAMESPACE_SOCIAL_DATA);
        $data = array();
        if(isset($session->data))
            $data = $session->data;
        $data[$input['namespace']] = $input;
        if(!isset($data['name']))
            $data['name'] = $input['name'];
        if(!isset($data['sname']))
            $data['sname'] = $input['sname'];
        $session->data = $data;
	}
	public static function getSocialData() {
		$data = array();
		$session = new Zend_Session_Namespace(self::NAMESPACE_SOCIAL_DATA);
		if(isset($session->data))
			$data = $session->data;
		return $data;
	}
    public static function clearNamespace($namespace) {
        $session = new Zend_Session_Namespace(self::NAMESPACE_SOCIAL_DATA);
        if(isset($session->data[$namespace]))
            unset($session->data[$namespace]);
    }
    public static function clear(){
        $session = new Zend_Session_Namespace(self::NAMESPACE_SOCIAL_DATA);
        $session->data = array();
    }
}