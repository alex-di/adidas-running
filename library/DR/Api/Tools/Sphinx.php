<?php

require_once ("./library/Other/sphinx/sphinxapi.php");
/**
 * работа с Sphinx
 */
class DR_Api_Tools_Sphinx extends SphinxClient
{
    /**
     * получить из строки строку ключовых слов с булевыми операторами
     */
    public function getSphinxKeyword($sQuery)
    {
        $aRequestString = preg_split('/[\s,-]+/', $sQuery, 5);
        if ($aRequestString)
        {
            $aKeyword = array();
            foreach ($aRequestString as $sValue)
            {
                
                
                $sValue = str_replace("/", "\/", $sValue);
                //_d($sValue);
                //if (strlen($sValue) > 3)
                //{
                    $aKeyword[] .= "(" . $sValue . " | *" . $sValue . "*)";
                //}
            }
            $sSphinxKeyword = implode(" & ", $aKeyword);
        }
        return $sSphinxKeyword;
    }
}
