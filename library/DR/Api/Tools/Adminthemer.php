<?php

class DR_Api_Tools_Adminthemer
{

    public static $css_path = "/design/admin/css/themer.css";
    public static $propeties_path = "/design/admin/css/themer.properties";

    public static function getThemes()
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        
        $data = file_get_contents($root . self::$propeties_path);
        if ($data === false)
            return array();
        return unserialize($data);
    }
    public static function saveTheme($post)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        $data = file_get_contents($root . self::$propeties_path);
        if ($data === false)
            return false;
        $data = unserialize($data);
        $value = array(
            "is_edit" => 1,
            "patterns" => $post['patterns'],
            "name" => $post['name'],
            "baseColor" => $post['baseColor'],
            "highlightColor" => $post['highlightColor'],
            "textColor" => $post['textColor'],
            "textGlowColor" => array(
                "r" => $post['r'],
                "g" => $post['g'],
                "b" => $post['b'],
                "a" => $post['a']));

        $selected = $post['id'];
        if ($post['id'] >= 0)
        {
            $value['is_edit'] = $data['data'][$post['id']]['is_edit'];
            $data['data'][$post['id']] = $value;
        } else
        {
            $data['data'][] = $value;
            $selected = count($data['data']) - 1;
        }
        $data['selected'] = $selected;
        file_put_contents($root . self::$propeties_path, serialize($data));
        file_put_contents($root . self::$css_path, $post['css']);
    }

    public static function deleteTheme($id)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        $data = file_get_contents($root . self::$propeties_path);
        if ($data === false)
            return false;
        $data = unserialize($data);
        unset($data['data'][$id]);
        file_put_contents($root . self::$propeties_path, serialize($data));
    }
}
