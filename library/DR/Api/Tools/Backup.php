<?

/**
 * Класс backup
 * 
 * @author celeron
 * @copyright 2011
 * @version 1.0
 * @access public
 */
 
class DR_Api_Tools_Backup
{
    var $directory;
    var $name;
    var $dbFile;
    var $mode;
    var $destanation;
    
    /**
     * backup::__construct()
     * 
     * @return void
     */
    function __construct($mode = 'library')
    {
        $this->directory = $_SERVER['DOCUMENT_ROOT'];        
        
        if(!is_dir($this->directory))
        {
            mkdir($this->directory);    
        }
        
        $this->mode = $mode;        
        $this->destanation = $this->directory . '/data/backup/' . 'backup_'. md5(microtime()) . '_' . date('d-m-Y_H-i')  . '.zip';                        
    }
    
    /**
     * backup::database()
     * 
     * @return mixed $data
     */
    function database($mode = 'dumper')
    {
        $this->exec();
        return $this->$mode();
    }    
    
    /**
     * backup::exec()
     * 
     * @return void
     */
    function exec()
    {
        require $_SERVER['DOCUMENT_ROOT'] . '/application/settings/config.php';
        
        $dbUsername = $config['mysql']['params']['username'];
        $dbPassword = $config['mysql']['params']['password'];
        $dbName = $config['mysql']['params']['dbname'];
        $file = $this->directory . '/' . time() . '.sql';
        
        $command = sprintf
        (
            "mysqldump -u %s --password=%s -d %s --skip-no-data > %s", escapeshellcmd($dbUsername), escapeshellcmd($dbPassword), escapeshellcmd($dbName),escapeshellcmd($file)
        );
        
        exec($command);
                
        $this->dbFile = $file;
                
        return $this->dbFile;        
    }
    
    /**
     * dumper::directory()
     * 
     * @return void
     */    
    function dumper()
    {        
        $_REQUEST['name'] = $_SERVER['DOCUMENT_ROOT'] . '/data/backup/' . md5(microtime());
        $this->dbFile = $_REQUEST['name'] . '.sql';
        
        ob_start(); include $_SERVER['DOCUMENT_ROOT']  . '/design/modules/dumper/php/dumper.php'; ob_clean();
        
        return $this->dbFile;
    }
    
    /**
     * backup::directory()
     * 
     * @return void
     */
    function directory()
    {
        $backupList = array
        (
            $this->directory . '/application/',            
            $this->directory . '/design/',
            $this->directory . '/library/',            
            $this->directory . '/index.php',            
        );
        
            
        
        if(file_exists($this->dbFile))
        {
            $backupList[] = $this->dbFile;
        }
                                            
        if(file_exists($this->directory . '/bash/'))
        {
            $backupList[] = $this->directory . '/bash/';
        }
        
        if(file_exists($this->directory . '/api.php'))
        {
            $backupList[] = $this->directory . '/api.php';
        }        
        
        if($this->mode !== 'library')
        {
            $backupList[] = $this->directory . '/data/';
        }
        $rar = new DR_Api_Tools_Rar( $this->destanation );
                                   
        $result = $rar->create( $backupList );
          
    }
    
    /**
     * backup::process()
     * 
     * @return void
     */
    function process()
    {        
        $this->database();                        
        $this->directory();        
        $this->remove();
        return $this->response( $this->destanation );        
    }
    
    /**
     * backup::remove()
     * 
     * @return void
     */    
    function remove()
    {
        unlink($this->dbFile);
    }    
    
    /**
     * backup::response()
     * 
     * @param mixed $message
     * @return
     */
    function response($message)
    {
        return $message;
    }
}

?>