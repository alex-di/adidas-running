<?php

class DR_Api_Tools_Controlmenu
{
    public static function getAdminActions()
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/application/modules/admin/controllers";
        $resources = array();
        $controllers = scandir($dir);
        foreach ($controllers as $controller)
        {
            $file_name = $dir . "/" . $controller;
            if (strpos($controller, ".php") !== false)
            {
                $resource = file_get_contents($file_name);
                $default_action = array(
                    "index"=>"index",
                    "save"=>"save",
                    "edit"=>"edit",
                    "delete"=>"delete");
                $resources[self::getControllerName($controller)] = self::getAction($resource, $default_action);
            }
        }
        return $resources;
    }
    public static function getAction($resource, $default = array())
    {
        $resource_without_space = strtr($resource, array(" " => ""));
        $_pattern = "|function(.*)Action\(\)|";
        preg_match_all($_pattern, $resource_without_space, $mathes);
        if (isset($mathes[1]))
        {
            foreach ($mathes[1] as $action)
            {
                $default[trim($action)] = trim($action);
            }
        }
        return $default;
    }
    public static function getControllerName($controller)
    {
        $controller = strtr($controller, array("Controller.php" => ""));
        return strtolower($controller);
    }
}
