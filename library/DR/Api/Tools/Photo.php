<?

/**
 * Ќазначени: Љласс длЯ работы с картинками
 * @version 2.0
 */

class DR_Api_Tools_Photo
{
    /**
     * @var integer Љачество при сохранении
     */
    var $latency = 100;
    /**
     * @var struct Ќастройки
     */
    var $settings = array();
    /**
     * @var array Ћшибки
     */
    var $errors = array();
    /**
     * @var string Џапка длЯ сохранениЯ
     */
    var $dir = '';
    /**
     * @var object Zend_View
     */
    var $view = false;
    /**
     * @var ЊинимальнаЯ длинна длЯ превью и водЯного знака
     */
    static $minWidth = 160;
    /**
     * @var Њинимальное соотношение сторон
     */
    static $aspectMin = 0.6;
    /**
     * @var Њаксимальное соотношение сторон
     */
    static $aspectMax = 1.4;

    /**
     * Љонструктор
     * @param string DIR
     * @return void
     */
    public function __construct($dir = false)
    {
        if (!$dir)
        {
            $this->dir = $_SERVER['DOCUMENT_ROOT'];
        }

        if (class_exists('Zend_layout'))
        {
            $this->view = Zend_Layout::getMvcInstance()->getView();
        }
    }

    /**
     * ‘генерировать рандомное имЯ картинке
     * @param string Photo Location on Hard Drive
     */
    public function generateRandomImageName($photo)
    {
        $data = $this->getImageData($photo);
        return md5(date("Y:M:d|H:i:s") . rand(100, 32000)) . '.' . str_replace("image/", "", $data['mime']);
    }

    /**
     * ‘оздание папки с проверкой
     * @param string patch
     * @param integer [chmod]
     */
    public function createSubDirectory($path, $chmod = 0777)
    {
        $rootVar = $_SERVER['DOCUMENT_ROOT'];

        $PatchArray = explode('/', $path);

        $myPatch = '';
        foreach ($PatchArray as $number => $path)
        {
            if ($path)
            {
                $myPatch .= "/" . $path;

                if (!is_dir($rootVar . $myPatch))
                {
                    mkdir($rootVar . $myPatch, $chmod);
                }
            }
        }

        $this->dir = $rootVar . $myPatch;
        return $myPatch;
    }

    /**
     *	‘оздаем папку
     * @param string [login]
     * @param string [album]     
     */
    public function createDirectory($login = null, $album = null)
    {
        if (is_null($login) and is_null($album))
        {
            $this->createSubDirectory(date('Y/M/d'));
        }

        if (!is_null($login) and is_null($album))
        {
            $this->createSubDirectory($login . date('/Y/M/d'));
        }

        if (!is_null($login) and !is_null($album))
        {
            $this->createSubDirectory($login . '/' . MyClass::str2url($album));
        }
    }

    /**
     * Ћбработка данных
     * @param struct „анные фото
     * @param string Patch to FileName
     * @return boolean
     */
    function processData($Data, $fileName)
    {
        if ((int)$Data['isOptimize'] and (int)$Data['optimize'])
        {
            $this->latency = intval($Data['optimize']);
        }

        if ((int)$Data['isTurnImage'])
        {
            $this->doTurnImage($fileName, $Data['turn']);
        }

        if ((int)$Data['sizeLabel'])
        {
            $this->doTextWatemark($fileName, intval(filesize($fileName) / 1024) . " кб");
        }

        if ((int)$Data['textLabel'] and $Data['imageText'] !== "")
        {
            $this->doTextWatemark($fileName, $Data['imageText']);
        }

        if ($Data['isResize'] and $Data['resizeProp'] !== "")
        {
            $this->doChangeSize($fileName, intval($Data['resizeProp']));
        }

        if ($Data['previewSize'])
        {
            $previewSize = $Data['previewSize'];
        } else
        {
            $previewSize = 50;
        }

        $this->doPreview($fileName, str_replace(basename($fileName), "preview_" . basename($fileName), $fileName), $previewSize);
    }

    /**
     * ЏроверЯем на ошибки
     * @param struct „анные файла
     * @param struct errors
     * @return struct errors
     */
    function isValid($fileName)
    {
        $this->errors = array();

        $allowedExt = array(
            'image/jpg',
            'image/jpeg',
            'image/png',
            'image/gif');

        $data = getimagesize($fileName);

        if (!in_array($data['mime'], $allowedExt))
        {
            $this->errors[$fileName][] = $this->view->translate("’ип файла не поддерживаетсЯ");
        }

        if (!$this->errors)
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * Џовернуть изображение
     * @param string Photo Location on Hard Drive
     * @param integer degree [0-360]
     * @return boolean
     */
    public function doTurnImage($photo, $degree)
    {
        $image = $this->doLoadImage($photo);
        $rotate = imagerotate($image, $degree, 0);
        $this->doSaveImage($photo, $rotate);
    }

    /**
     * Џоставить текстовую надпись на файле
     * @param string Photo Location on Hard Drive
     * @param string TEXT
     * @return boolean
     */
    public function doTextWatemark($photo, $text)
    {
        $image = $this->doLoadImage($photo);

        if ($image)
        {
            imagestring($image, 205, 5, 5, $text, imagecolorallocate($image, 255, 255, 255));

            /*
            imagettftext
            (
            $image,                                                  # -  как всегда, идентификатор ресурса
            11,                                                      # -  размер шрифта
            0,                                                       # -  угол наклона шрифта
            15, 15,                                                  # -  координаты (x,y), соответствующие левому нижнему                
            0xFFFFFF,                                                # -  цвет шрифта
            $_SERVER['DOCUMENT_ROOT'] . "/design/fonts/arial.ttf",   # -  имЯ ttf-файла
            $text
            );
            */

            $this->doSaveImage($photo, $image);
        }
    }


    /**
     * Џоставить вотермарку на картинку
     * @param string Photo Location on Hard Drive
     * @param string TEXT
     * @return boolean
     */
    public function doWatemark($photo, $mark)
    {
        $photo = $this->dir . $photo;
        $mark = $this->dir . $mark;

        $znak_hw = getimagesize($mark);
        $foto_hw = getimagesize($photo);

        $image = $this->doLoadImage($photo);
        $watermark = $this->doLoadImage($mark);

        $data = $this->getImageData($photo);

        if ($image and $watermark and $data[0] > self::$minWidth)
        {
            imagecopy($image, $watermark, $foto_hw[0] - $znak_hw[0], $foto_hw[1] - $znak_hw[1], 0, 0, $znak_hw[0], $znak_hw[1]);

            imageAlphaBlending($image, false);
            imageSaveAlpha($image, true);

            $this->doSaveImage($photo, $image);
        }

        imagedestroy($image);
        imagedestroy($watermark);
    }

    /**
     * Џолучить данные о картинке
     * @param string Photo Location on Hard Drive
     * @return struct Image Data
     */
    public function getImageData($photo)
    {
        $photo = str_replace("//", "/", $photo);
        return getimagesize($photo);
    }

    /**
     * Џроверка соотношениЯ сторон
     * @param string Photo Location on Hard Drive
     * @return struct Image Data
     */
    public function checkAspect($photo)
    {
        $data = $this->getImageData($photo);

        $x = array_shift($data);
        $y = array_shift($data);
        
        if ($x === 0 or $y === 0)
        {
            return false;
        }

        $ratio = $x / $y;
        
        if ($ratio > self::$aspectMin and $ratio < self::$aspectMax)
        {
            return true;
        }

        return false;
    }
    public function checkSizeGreatThan($photo, $xsize, $ysize) {
        $data = $this->getImageData($photo);

        $x = array_shift($data);
        $y = array_shift($data);
        return $x >= $xsize && $y >= $ysize;
    }
    public function checkSizeLessThan($photo, $xsize, $ysize) {
        $data = $this->getImageData($photo);

        $x = array_shift($data);
        $y = array_shift($data);
        return $x <= $xsize && $y <= $ysize;
    }
    public function checkSize($photo, $xsize, $ysize) {
        $data = $this->getImageData($photo);

        $x = array_shift($data);
        $y = array_shift($data);
        return $x >= $xsize && $y >= $ysize;
    }
    /**
     * ‘делать превью картинки
     * @param string Photo Location on Hard Drive
     * @param string Preview Location on Hard Drive
     * @param integer ЏропорциЯ картинки [1-100]
     * @param [width]
     * @param [height]
     * @return boolean
     */
    public function doPreview($photo, $preview, $prop, $myWidth = null , $myHeight = null)
    {        
        $image = $this->doLoadImage($photo);
        
        $width = imagesx($image);
        $height = imagesy($image);
        $prop_real = $width / $height;
        if( is_null($myWidth) and is_null($myHeight) )
        {
            $myWidth  = imagesx($image) / 100 * $prop;
            $myHeight = imagesy($image) / 100 * $prop;
            
            if( $myWidth < $width)
            {
                $myWidth = imagesx($image);
                $myHeight = imagesy($image);
            }
        } elseif(!is_null($myWidth) and is_null($myHeight)) {
            
            
            $myHeight = round($myWidth / $prop_real);
        	
            if( $myWidth > $width)
            {
                $myWidth = imagesx($image);
                $myHeight = imagesy($image);
            }
        }
        elseif(is_null($myWidth) and !is_null($myHeight)) {
            $myWidth = round($myHeight * $prop_real);
        	
            if( $myHeight > $height)
            {
                $myWidth = imagesx($image);
                $myHeight = imagesy($image);
            }
        }
        
        $empty = imageCreateTrueColor($myWidth, $myHeight);
        imageAlphaBlending($empty, false);
	    imageCopyResampled($empty, $image, 0, 0, 0, 0, $myWidth, $myHeight, imagesx($image), imagesy($image));
		imageSaveAlpha($empty, true);
        
        $this->doSaveImage($photo, $empty, $preview);

	    imageDestroy($empty);
   		imageDestroy($image);  
    }

    /**
     * €зменить размеры картинки
     * @param string Photo Location on Hard Drive
     * @param integer ЏропорциЯ картинки [1-100]
     * @return boolean
     */

    public function doChangeSize($photo, $prop)
    {
        $image = $this->doLoadImage($photo);

        $myWidth = imagesx($image) / 100 * $prop;
        $myHeight = imagesy($image) / 100 * $prop;

        $empty = imageCreateTrueColor($myWidth, $myHeight);
        imageAlphaBlending($empty, false);
        imageCopyResampled($empty, $image, 0, 0, 0, 0, $myWidth, $myHeight, imagesx($image), imagesy($image));
        imageSaveAlpha($empty, true);

        $this->doSaveImage($photo, $empty);

        imageDestroy($empty);
        imageDestroy($image);
    }

    /**
     * ‡агрузить фотку
     * @param string Photo Location on Hard Drive
     * @return resource integer
     */
    public function doLoadImage($photo)
    {
        $data = $this->getImageData($photo);

        switch (str_replace("image/", "", $data['mime']))
        {
            case "gif":
                return imagecreatefromgif($photo);
            case "jpg":
            case "jpeg":
                return imagecreatefromjpeg($photo);
            case "png":
                return imagecreatefrompng($photo);
            default:
                return false;
        }
    }

    /**
     * ‘охранить фотку
     * @param string Photo Location on Hard Drive
     * @param resource Image Data
     * @param string Photo Location To
     * @return resource integer
     */
    public function doSaveImage($photo, $image, $url = null)
    {
        $data = $this->getImageData($photo);

        if (!is_null($url))
        {
            $photo = $url;
        }

        switch (str_replace("image/", "", $data['mime']))
        {
            case "gif":
                return imagegif($image, $photo, $this->latency);
            case "jpg":
            case "jpeg":
                return imagejpeg($image, $photo, $this->latency);
            case "png":
                return imagepng($image, $photo);

            default:
                return false;
        }
    }


    /**
     *	-	Ќазначение: проверить пропорции в зависимости от настроек
     *	-	дата: 23.04.2011 11:39:21
     * @author Appalach
     * @version 1.0
     */
    public function doCheckProp($settings, $file)
    {
        $file = $this->doLoadImage($file);

        if ($settings['min_height'] >= imagesy($file))
        {
            return false;
        }

        if ($settings['min_width'] >= imagesx($file))
        {
            return false;
        }

        return true;
    }


}

?>