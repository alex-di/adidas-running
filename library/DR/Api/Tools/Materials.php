<?

class DR_Api_Tools_Materials {
    public static function parseDragElememts($dragElements) {

        $html = '';
        $gallery = array();
        $result = array();
        $object = new self;
        $jevix = $object->getJevix();
        $error = null;
        //$html = $purifier->purify($html);
        foreach($dragElements as $dragElement) {
            foreach($dragElement as $type=>$data) {
                foreach($data as $namePostfix=>$values) {
                    if('text' == $type) {
                        $html .= $jevix->parse(trim($values['editor']), $error).'<br/>';
                        //$html .= trim($values['editor']).'<br/>';
                        $result['editor'.$namePostfix] = $values['editor'];
                    } elseif('video' == $type) {
                        $html .= '<div class="video_title">'.$values['name'].'</div>' . 
                                    self::getIframeVideo($values['link']).'<br/>' . 
                                    '<div class="video_desc">' . $values['description'] . '</div>';
                        $result['link'.$namePostfix] = $values['link'];
                        $result['name'.$namePostfix] = $values['name'];
                        $result['description'.$namePostfix] = $values['description'];
                    } elseif('audio' == $type) {
                        $playlistName = $values['playlist'];
                        $result['playlist'.$namePostfix] = $playlistName;
                        $html .= '<div class="playlist_title">' . $playlistName . '</div>';
                        foreach($values['list'] as $listData) {
                            foreach($listData as $postfix=>$listvalue){
                                $html .= self::getIframeAudio($listvalue['audio']).'<br/>';
                                $result['audio'.$postfix] = $listvalue['audio'];
                            }
                        }
                    } elseif('photo' == $type) {
                        $galleryData = array();
                        foreach($values['list'] as $listData) {                            
                            foreach($listData as $postfix=>$listvalue){
                                if(1 == count($values['list'])) {
                                    $html .= '<img src="'.self::getAbsoluteImagePath($listvalue['photo']).'"/><br/>';
                                } else {
                                    if(!count($galleryData))    
                                        $html .= '<div id="simplegallery'.(count($gallery)+1).'" class="article_block_gallery"> </div><br/>';
                                    $galleryData[] = array('src'=>$listvalue['photo'], 'name'=>$listvalue['description']);
                                }
                                $result['photo'.$postfix] = $listvalue['photo'];
                                $result['description'.$postfix] = $listvalue['description'];
                            }

                        }
                        if(count($galleryData))
                            $gallery[] = $galleryData;
                    } 
                }
            }
        }
        //_d($html);
        //$html = strtr($html, array("\n"=>"", "\r"=>"", "\t"=>""));
        //$object = new self;
        //$purifier = $object->getPurifier();
        //$error = null;
        //$html = $purifier->purify($html);
        //_d($html);
        return array($html, $gallery, $result);
    }
    public static function getAbsoluteImagePath($previewPath) {
        $temp = pathinfo($previewPath);
        if(strpos($temp['filename'], 'prev') !== false) {
            return $temp['dirname'].'.'.$temp['extension'];
        }
        return $previewPath;
    }
    public static function getIframeAudio($link) {
        $result = '';
        if((strpos($link, "<iframe") !== false && strpos($link, "soundcloud.com/player/?url=") !== false) || (strpos($link, "<object") !== false && strpos($link, "www.youtube.com/v/") !== false)) {
            $result = $link;
        }
        return $result;
    }
    public static function getIframeVideo($link) {
        $result = '';
        if((strpos($link, "<iframe") !== false && strpos($link, "vk.com/video_ext.php") !== false) || strpos($link, "youtube.com/watch?v=") !== false) {
            if(strpos($link, "<iframe") !== false)
                $result = $link;
            elseif(strpos($link, "youtube.com/watch") !== false) {
                $temp = explode("v=", $link);
                if(isset($temp[1])) 
                    list($result, ) = explode("&", $temp[1]);
                $result = self::getYoutubeIframeTemplate($result);
            }
        }
        /*if(strpos($link, "www.youtube.com/watch") !== false) {
            $temp = explode("v=", $link);
            if(isset($temp[1])) 
                list($result, ) = explode("&", $temp[1]);
            $result = self::getYoutubeIframeTemplate($result);
        } elseif(strpos($link, "youtu.be/") !== false) {
            $temp = explode("/", $link);
            $result = self::getYoutubeIframeTemplate($temp[count($temp) - 1]);
        } elseif(strpos($link, "http://vk.com/video_ext.php") !== false) {
            $result = $link;
        }*/
        return $result;
    }
    public static function getYoutubeIframeTemplate($link) {
        if(empty($link))
            return '';
        return '<iframe width="640" height="390" src="//www.youtube.com/embed/'.$link.'" frameborder="0" allowfullscreen></iframe>';
    }
	public static function processHtml($html) {
		$object = new self;

		list($html, $gallery) = $object->gallery($html);
		
		//$jevix = $object->getJevix();
		//$error = null;
		//$html = $jevix->parse($html, $error);
        //_d($html);
        $reference_items = $object->getReferenceItems($html);
		return array($html, $gallery, $reference_items);

	}

	public function gallery($html) {
	   
        require_once ('./library/Other/phpQuery/phpQuery/phpQuery.php');
        $doc = phpQuery::newDocument('<div id="content">'.$html.'</div>');
        $gallerys = $doc['.gallery'];
        $galleryData = array();
        $M = new DR_Api_Tools_Photo;
        foreach($gallerys as $key=>$gallery) {
            $imagesData = array();
            $images = pq($gallery)->find('img');
            foreach($images as $img) {
                $src = pq($img)->attr('src');
				$info = pathinfo($src);
				$preview = str_replace("." . $info['extension'], "", $src);
				//$way = $M->dir . $preview;
				//if (!file_exists($way))
				//	mkdir($way, 0755);
				//$M->doPreview($M->dir . $src, $way . "/prev560." . $info['extension'], 0, 560);
				//$M->doPreview($M->dir . $src, $way . "/prev840." . $info['extension'], 0, 840);
                $imagesData[] = array('src'=>$preview . "/prev560." . $info['extension'], 'name'=>pq($img)->attr('alt'));
                //$imagesData[] = array('src'=>$preview . "/prev840." . $info['extension'], 'name'=>pq($img)->attr('alt'));
            }
            if(count($imagesData)) {
                $galleryData[$key] = $imagesData;
                pq($gallery)->attr('class', 'article_block_gallery')->attr('id', 'simplegallery'.($key+1))->children()->remove();
            }
            
        }
        //_d(htmlspecialchars($doc['#content']->html()), false);
        //_d($galleryData);

		return array($doc['#content']->html(), $galleryData);
	}
	public function getReferenceItems($html){
        $result = array();
        preg_match_all('/id="item_([^"]*)"/', $html, $math);
        if(isset($math[1])) {
            foreach($math[1] as $id) {
                $id = intval($id);
                if(0 != $id)
                    $result[] = $id;
            }
        }
        return $result;
	}
    public function getPurifier(){
        require_once('./library/Other/htmlpurifier/library/HTMLPurifier.auto.php');
        $config = HTMLPurifier_Config::createDefault();
        //$config->set('HTML.Allowed', 'a[href,title],img,i,b,u,strong,li,ol,ul,h1,h2,h3,h4,h5,h6,br,img[src,title,height,width,alt],p,b,i,strong,em,div[id,class],iframe[width,height,src,frameborder,scrolling,marginheight,marginwidth]'); 
        $config->set('HTML.Allowed', 'div,img');
        $config->set('URI.Base', 'http://adidas-running.ru');
        $config->set('URI.MakeAbsolute', true);
        //$config->set('AutoFormat.AutoParagraph', true);
        //$config->set('AutoFormat.RemoveEmpty', false);

        $purifier = new HTMLPurifier($config);
        return $purifier;
    }
	public function getJevix() {
		require('./library/Other/jevix-php-1.0/jevix.class.php');
		$jevix = new Jevix();

		//Конфигурация

		// 1. Устанавливаем разрешённые теги. (Все не разрешенные теги считаются запрещенными.)
		$jevix
				->cfgAllowTags(
						array('a', 'img', 'i', 'b', 'u', 'em', 'strong', 'nobr', 'li', 'ol', 'ul', 'sup', 'abbr',
								'pre', 'acronym', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'adabracut', 'br', 'code',
								'embed', 'iframe', 'hr', 'div', 'strike', 'span'));

		// 2. Устанавливаем коротие теги. (не имеющие закрывающего тега)
		$jevix->cfgSetTagShort(array('br', 'img', 'hr'));

		// 3. Устанавливаем преформатированные теги. (в них все будет заменятся на HTML сущности)
		$jevix->cfgSetTagPreformatted(array('pre'));

		// 4. Устанавливаем теги, которые необходимо вырезать из текста вместе с контентом.
		$jevix->cfgSetTagCutWithContent(array('script', 'style'));

		// 5. Устанавливаем разрешённые параметры тегов. Также можно устанавливать допустимые значения этих параметров.
		$jevix->cfgAllowTagParams('a', array('title', 'href'));
		$jevix->cfgAllowTagParams('div', array('align', 'class', 'id'));
		$jevix->cfgAllowTagParams('embed', array('width', 'height', 'src', 'type', 'wmode'));
		$jevix->cfgAllowTagParams('iframe', array('width', 'height', 'src', 'type', 'wmode'));
		$jevix
				->cfgAllowTagParams('img',
						array('src', 'alt' => '#text', 'title', 'align' => array('right', 'left', 'center'),
								'width' => '#int', 'height' => '#int', 'hspace' => '#int', 'vspace' => '#int'));

		// 6. Устанавливаем параметры тегов являющиеся обязяательными. Без них вырезает тег оставляя содержимое.
		$jevix->cfgSetTagParamsRequired('img', 'src');
		$jevix->cfgSetTagParamsRequired('a', 'href');

		// 7. Устанавливаем теги которые может содержать тег контейнер
		//    cfgSetTagChilds($tag, $childs, $isContainerOnly, $isChildOnly)
		//       $isContainerOnly : тег является только контейнером для других тегов и не может содержать текст (по умолчанию false)
		//       $isChildOnly : вложенные теги не могут присутствовать нигде кроме указанного тега (по умолчанию false)
		//$jevix->cfgSetTagChilds('ul', 'li', true, true);
		//$jevix->cfgSetTagChilds('ol', 'li', true, true);

		// 11. Включаем или выключаем режим замены переноса строк на тег <br/>. (по умолчанию включен)
		$jevix->cfgSetAutoBrMode(false);

		// 12. Включаем или выключаем режим автоматического определения ссылок. (по умолчанию включен)
		$jevix->cfgSetAutoLinkMode(true);
		return $jevix;
	}
}
