<?
require_once ('./library/Other/htmlpurifier/library/HTMLPurifier.auto.php');
require_once ('./library/Other/phpQuery/phpQuery/phpQuery.php');
class DR_Api_Tools_News
{
    
    public static function purifier($text){
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%');
        $config->set('Attr.EnableID', true);
        $config->set('CSS.AllowedProperties', array());
        $purifier = new HTMLPurifier($config);
        
        return $purifier->purify($text);
    }
    
    public static function processNews($html, $lenght_short_news, $host, $count_needle_elements = 1, $materials_id = 0)
    {


        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%');
        $config->set('Attr.EnableID', true);
        $config->set('CSS.AllowedProperties', array());

        $purifier = new HTMLPurifier($config);
        $html = "<div id='content'>$html</div>";
        $clean_html = $purifier->purify($html);
        //print_r($clean_html);die;
        $short_news = self::getShortNews($clean_html, $lenght_short_news, $count_needle_elements);

        $result = array();
        $doc = phpQuery::newDocument($clean_html);
        $result['text'] = $doc['#content']->text();
        $result['attachments'] = self::getAttachmentPhoto($doc);
        $doc = self::wrapLink($doc, $host);
        $doc = self::wrapPhotos($doc);
        $doc = self::addActionPhotoPreview($doc, $materials_id);
        $result['full_news'] = $doc['#content']->html();

        $short_news = $purifier->purify($short_news);
        $doc = phpQuery::newDocument($short_news);
        $doc = self::wrapLink($doc, $host);
        $doc = self::wrapPhotos($doc);
        $doc = self::addActionPhotoPreview($doc, $materials_id);
        $result['short_news'] = $doc['#content']->html();

        return $result;
    }
    /**
     * получить краткую нововсть
     */
    public static function getShortNews($html, $max_lenght, $count_needle_elements)
    {
        $lenght = 0;
        $count_elements = 0;
        $short_news = "";
        for ($i = 0; $i < strlen($html); $i++)
        {
            if (self::isTag($html, $i))
            {
                if (self::isNeedleElement($html, $i))
                {
                    $count_elements++;
                }
                if ($count_elements < $count_needle_elements + 1)
                {
                    $i += self::getTagSize($html, $i);
                }

            } else
            {
                $lenght++;
            }
            if ($lenght > $max_lenght || $count_elements > $count_needle_elements)
            {
                $short_news = substr($html, 0, $i);
                break;
            }
        }

        if ($short_news == "")
        {
            $short_news = $html;
        }
        return $short_news;
    }
    /**
     * является ли тег искомым      
     */
    public static function isNeedleElement($html, $position)
    {
        $needle_tag = array("img", "iframe");
        $maxLength = 10;
        $haystack = substr($html, $position, $maxLength);
        foreach ($needle_tag as $tag)
            if (strpos($haystack, "<" . $tag) === 0)
                return true;
        return false;
    }
    /**
     * получить размер тега
     */
    public static function getTagSize($html, $position)
    {
        $size = 0;
        for ($i = $position; $i < strlen($html); $i++)
        {
            $size++;
            if (substr($html, $i, 1) == ">")
            {
                break;
            }
        }
        return $size - 1;
    }
    /**
     * проверяем строку на соответсвия тегам
     */
    public static function isTag($html, $position)
    {
        $tags = array(
            "div",
            "p",
            "img",
            "a",
            "b",
            "i",
            "iframe",
            "br");
        $maxLength = 10;
        $haystack = substr($html, $position, $maxLength);
        foreach ($tags as $tag)
            if (strpos($haystack, "<" . $tag) === 0 || strpos($haystack, "</" . $tag) === 0)
                return true;
        return false;
    }
    /**
     * получаем ид фотографий, приатаченых к новости
     */
    public static function getAttachmentPhoto($dom)
    {
        $photos = array();
        foreach ($dom['img'] as $img)
        {
            $photo_id = pq($img)->attr("id");
            $photos[str_replace("preview_photos_", "", $photo_id)] = 1;
        }
        return array_keys($photos);
    }
    /**
     * оборочиваем приатаченные фотографии в заданные теги 
     */
    public static function wrapLink($dom, $host)
    {
        foreach ($dom['a'] as $link)
        {
            $href = pq($link)->attr("href");
            $h = self::checkHost($href);
            if (trim($h) != "" && strpos($h, $host) === false)
                pq($link)->attr("href", "/away.php?to=" . $h);

        }
        return $dom;
    }
    /**
     * оборочиваем приатаченные фотографии в заданные теги 
     */
    public static function wrapPhotos($dom)
    {
        foreach ($dom['img'] as $img)
        {
            pq($img)->wrap("<div class='image_box'></div>")->wrap("<a href='javascript:void(0)' class='view_photos'></a>");
        }
        $dom['a.view_photos']->before("<img src='/design/images/img_shadow_material.png' class='image_material_shadow'>");
        $dom['div.image_box']->after("<br />");
        return $dom;
    }
    /**
     * добавляем действия для просмотра картинки
     */
    public static function addActionPhotoPreview($dom, $material_id)
    {
        foreach ($dom['a.view_photos'] as $link)
        {
            $src = pq($link)->find("img")->attr("src");
            $photo_id = pq($link)->find("img")->attr("id");
            $photo_id = str_replace("preview_photos_", "", $photo_id);
            pq($link)->attr("onclick", "viewPhoto($material_id, '$src', $photo_id)");
        }
        return $dom;
    }
    /**
     * проверка правильности урл. в случаее удачи вернёт хост
     */
    public static function checkHost($resource)
    {
        if (preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $resource, $math))
        {
            $result = "";
            for ($i = 0; $i < count($math); $i++)
            {
                if ($i > 0)
                {
                    if ("" != trim($math[$i]))
                    {
                        $result .= (strpos($math[$i], "/") !== false || strpos($math[$i - 1], "/") !== false) ? $math[$i] : "." . $math[$i];
                    }
                }
            }
            if(strpos($result, ".") === 0)
                $result = substr($result, 1);
            $link = strtr($resource, array($result=>""));
            $link = explode("/", $link);
            foreach($link as $el) {
                if(trim($el) != "")
                    $result .= "/".$el;
            }
            if(strpos($result, "http://") === false && strpos($result, "https://") === false)
                return "http://".$result;
            return $result;
        }
        return "";
    }
}
