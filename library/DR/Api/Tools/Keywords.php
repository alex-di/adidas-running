<?php
class DR_Api_Tools_Keywords
{
  var $word_length_min=3; // минимальное слово
  var $words_count=3; // количество ключевых слов
  var $meta=false; // true - возвращает готовый мета тег, иначе будет массив
  
  
  function keywords($_text)
  { $search=array ("'ё'",
    "'<script[^>]*?>.*?</script>'si", // Вырезается javascript
    "'<[\/\!]*?[^<>]*?>'si", // Вырезаются html-теги
    "'([\r\n])[\s]+'", // Вырезается пустое пространство
    // Замещаются html-элементы
    "'&quot;'i", "'&amp;'i","'&lt;'i","'&gt;'i","'&nbsp;'i",
    "'&iexcl;'i","'&cent;'i","'&pound;'i","'&copy;'i","'&#(\d+);'e");
    $replace=array('е',' ',' ',"\\1 ",'" ',' ',' ',' ',' ',
    chr(161),chr(162),chr(163),chr(169),"chr(\\1)");
  
    $text=preg_replace($search,$replace,$_text);// избавляемся в тексте от всякой html фигни
    
    
    
    // по каким то причинам жопы в preg_match_all приходится извращаться с кодировкой
    // отбираем из текст слова (из кодирвоки cp1251)
    preg_match_all("/([a-zA-Z.А-Яа-я\x80-\xFF]+)/i",$text,$words_all);
    //[a-zA-Z.А-Яа-я]
    //print_r($words_all); die;
    // массив стоп слов, которые включать по любасу не нужно
    $words_stop=array('','как','для','что','или','это','этих','всех','вас',
    'они','оно','еще','когда','где','эта','лишь','уже','вам','нет','если','надо','все',
    'так','его','чем','при','даже','мне','есть','раз','два','аля','нас','тем','через','многие','многое',
    'по', 'на', 'за', 'свой', 'своя', 'свои'
    );
    
    
    $result = array();
    $similar_words = array();
    foreach($words_all[1] as $word) {
      $word = mb_strtolower($word, 'utf-8');
      if(!in_array($word, $words_stop) and mb_strlen($word, "utf-8") > $this->word_length_min) {
        if(isset($result[$word])) {
          $result[$word] = $result[$word] + 1;
        } else {
          if(count($result)) {
            $flag = false;
            
            foreach($result as $key => $num) {
              if((mb_strpos($word, $key, 0, 'utf-8') !== false) or (mb_strpos($key, $word, 0, 'utf-8') !== false)) {
                
                if(levenshtein($word, $key) < 2) {
                  $result[$key] = $result[$key] + 1;
                  $flag = true;
                }
              } 
            }
            
            if(!$flag) {
              $result[$word] = 1;
            }
          } else {
            $result[$word] = 1;
          }
        }
        
        
      }
    }
    
    #Сортировка по частте
    arsort($result);
    $return = array();
    for($i = 0; $i < $this->words_count; $i++) {
      $temp = each($result);
      if(!empty($temp)) {
        $return[] = $temp['key'];
      }
    }
    
    return $return;
  }
}
?> 