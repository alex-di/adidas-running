<?

class DR_Api_Tools_Comments
{
    
    public static function processComments($html, $host)
    {
        require_once ('./data/htmlpurifier-4.4.0/library/HTMLPurifier.auto.php');
        require_once ('./data/phpQuery/phpQuery/phpQuery.php');

        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%');
        $config->set('Attr.EnableID', true);
        $config->set('CSS.AllowedProperties', array());

        $purifier = new HTMLPurifier($config);
        $html = "<div id='content'>$html</div>";
        $clean_html = $purifier->purify($html);
        //print_r($clean_html);die;
        $doc = phpQuery::newDocument($clean_html);
        $doc = self::wrapLink($doc, $host);
        return $doc['#content']->html();
    }
    /**
     * оборочиваем приатаченные фотографии в заданные теги 
     */
    public static function wrapLink($dom, $host)
    {
        foreach ($dom['a'] as $link)
        {
            $href = pq($link)->attr("href");
            $h = self::checkHost($href);
            if (trim($h) != "" && strpos($h, $host) === false)
                pq($link)->attr("href", "/away.php?to=" . $h);

        }
        return $dom;
    }
    /**
     * проверка правильности урл. в случаее удачи вернёт хост
     */
    public static function checkHost($resource)
    {
        if (preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $resource, $math))
        {
            $result = "";
            for ($i = 0; $i < count($math); $i++)
            {
                if ($i > 0)
                {
                    if ("" != trim($math[$i]))
                    {
                        $result .= (strpos($math[$i], "/") !== false || strpos($math[$i - 1], "/") !== false) ? $math[$i] : "." . $math[$i];
                    }
                }
            }
            if(strpos($result, ".") === 0)
                $result = substr($result, 1);
            $link = strtr($resource, array($result=>""));
            $link = explode("/", $link);
            foreach($link as $el) {
                if(trim($el) != "")
                    $result .= "/".$el;
            }
            if(strpos($result, "http://") === false && strpos($result, "https://") === false)
                return "http://".$result;
            return $result;
        }
        return "";
    }
}