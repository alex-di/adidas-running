<?php

class DR_Api_Tools_Aclresource
{

    public static function getResource()
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/application/modules/";
        $modules = scandir($dir);
        $resources = array();
        foreach ($modules as $module)
        {
            if (is_dir($dir . $module))
            {
                $curent_controller_path = $dir . $module . "/controllers";
                if (file_exists($curent_controller_path))
                {
                    $controllers = scandir($curent_controller_path);
                    foreach ($controllers as $controller)
                    {
                        $file_name = $curent_controller_path . "/" . $controller;
                        if (is_file($file_name) && strpos($controller, ".php") !== false)
                        {
                            $resource = file_get_contents($file_name);
                            $default_action = array();
                            if ("admin" == $module)
                            {
                                $default_action = array(
                                    "index" => "",
                                    "save" => "",
                                    "edit" => "",
                                    "delete" => "");
                            } else {
                                $default_action = array(
                                    "index" => "");
                            }
                            $controller_action = self::getAction($resource, $default_action);
                            foreach ($controller_action as $action => $val)
                            {
                                $res = $module . ":" . self::getControllerName($controller) . ":" . $action;
                                $resources[$res] = $val;
                            }
                        }
                    }
                }
            }
        }
        return $resources;
        //echo "<pre>";
        //print_r($resources);
        //die;
    }
    public static function getAction($resource, $default = array())
    {
        $resource_without_space = strtr($resource, array(" " => ""));
        $_pattern = "|function(.*)Action\(\)|";
        preg_match_all($_pattern, $resource_without_space, $mathes);
        if (isset($mathes[1]))
        {
            foreach ($mathes[1] as $action)
            {
                $default[trim($action)] = "";
            }
        }
        $copy = $default;
        foreach ($copy as $action => $val)
        {
            $pos = strpos($resource, " ".$action . "Action()");
            if ($pos !== false)
            {
                $text = substr($resource, 0, $pos);
                $start = strrpos($text, "}");
                if ($start === false)
                {
                    $start = strrpos($text, "{");
                }
                if ($start !== false)
                {
                    $text = substr($text, $start);
                    $text = strtr($text, array(
                        "public" => "",
                        "function" => "",
                        "{" => "",
                        "}" => "",
                        "*" => "",
                        "/" => "",
                        "#" => ""));
                    $default[$action] = trim($text);
                }
            }
        }
        return $default;
    }
    public static function getControllerName($controller)
    {
        $controller = strtr($controller, array("Controller.php" => ""));
        return strtolower($controller);
    }
}
