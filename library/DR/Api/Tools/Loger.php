<?

class DR_Api_Tools_Loger
{
    public static function getLogger($filename)
    {
        return Zend_Log::factory(array('timestampFormat' => 'Y-m-d H:i', array('writerName' => 'Stream', 'writerParams' => array('stream' => './tmp/logs/' . $filename))));
    }
}
