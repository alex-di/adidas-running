<?php

/**
 * Класы - вспомогательные функции
 * @author Appalach
 * @version 1.0
 */
class DR_Api_Tools_Tools {
	
    public static function trimValues($data = array()) {
        $temp = $data;
        foreach($temp as $key=>$val) {
            if(!is_array($val)) {
                $data[$key] = trim($val);
            }
        }
        return $data;
    }
    public static function getTextForSex($sex, $text = array()) {
        if('woman' == $sex)
            return $text[1];
        return $text[0];
    }
    
	public static function getShortString($string, $size, $isDot = true) {
		$return = mb_substr($string, 0, $size, 'UTF-8');
		if($isDot && mb_strlen($string, 'UTF-8') > $size)
			$return .= '...';
		return $return;
	}
	
	public static function getCommentDate($date) {
		$now = time();
		$current = is_integer($date) ? $date : strtotime($date);
		$different = $now - $current;
		$days = 24 * 60 * 60;
		$month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
				'ноября', 'декабря');
		$current_mounth = intval(date('m', $current)) - 1;

        if(date('Y-m-d') == date('Y-m-d', $current)) {
			return 'сегодня в '.date('H:i', $current);
		} elseif(date('Y-m-d', $now - $days) == date('Y-m-d', $current)){
			return 'вчера в '.date('H:i', $current);
		} elseif(date('Y', $now) == date('Y', $current)) {
			return date('d', $current).' '.$month[$current_mounth];
		} else {
			return date('d', $current).' '.$month[$current_mounth].' '.date('Y', $current).'г.';
		}
	}
	public static function getDate($seconds, $deascription_minute = 'мин', $deascription_second = 'сек',
			$deascription_hours = 'ч', $deascription_days = 'дн') {
		$seconds = round($seconds);
		$sec = $seconds % 60;
		$sec = $sec == 0 ? "" : $sec . ' ' . $deascription_second . '.';
		$minutes = ($seconds - $sec) / 60;
		$min = $minutes % 60;
		$hours = ($minutes - $min) / 60;
		$min = $min == 0 ? "" : $min . ' ' . $deascription_minute . '. ';

		$hr = $hours % 60;
		$days = ($hours - $hr) / 24;
		$days = $days == 0 ? "" : $days . ' ' . $deascription_days . '. ';
		$hr = $hr == 0 ? "" : $hr . ' ' . $deascription_hours . '. ';
		return $days . $hr . $min . $sec;
	}

	/**
	 * Получить разницу между датами в годах 
	 */
	public static function yearDifference($startDate) {
	    list($year, $mounts) = explode('-', $startDate);
        $difference = abs(intval($year) - intval(date('Y')));
        if($mounts > date('m'))
            $difference--;
        return $difference;
	}
	public static function getPostfixYears($years) {
	   	$str = '';
		if (($years >= 5) && ($years <= 14))
			$str = "лет";
		else {
			$num = $years - (floor($years / 10) * 10);

			if ($num == 1) {
				$str = "год";
			} elseif ($num == 0) {
				$str = "лет";
			} elseif (($num >= 2) && ($num <= 4)) {
				$str = "года";
			} elseif (($num >= 5) && ($num <= 9)) {
				$str = "лет";
			}
		}
        return $str;
	}
	
	/**
	 * Получить месяц на русском
	 */
	public static function rusMonth($date) {
		$months = array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября",
				"Ноября", "Декабря");
		
		$time = strtotime($date);
        
		return $months[intval(date('m', $time)) - 1];
	}

	/**
	 * Получить денб недели на русском
	 */
	public static function rusDayOfWeek($date) {
		$days = array("воскресенье", "понедельник", "вторник", "среда", "четверг",
			      "пятница", "суббота");
		
		$time = strtotime($date);
		return $days[intval(date('w', $time))];
	}
	
	/**
	 * Получить разницу между датами в годах днях и месяцах
	 */
	public static function dateDifference($startDate, $endDate) {
		$startDate = strtotime($startDate);
		$endDate = strtotime($endDate);

		if ($startDate === false || $startDate < 0 || $endDate === false || $endDate < 0 || $startDate > $endDate)
			return false;
        
		$years = date('Y', $endDate) - date('Y', $startDate);

		$endMonth = date('m', $endDate);
		$startMonth = date('m', $startDate);

		// Calculate months
		$months = $endMonth - $startMonth;
		if ($months <= 0) {
			$months += 12;
			$years--;
		}
		if ($years < 0)
			return false;

		// Calculate the days
		$offsets = array();
		if ($years > 0)
			$offsets[] = $years . (($years == 1) ? ' year' : ' years');
		if ($months > 0)
			$offsets[] = $months . (($months == 1) ? ' month' : ' months');
		$offsets = count($offsets) > 0 ? '+' . implode(' ', $offsets) : 'now';

		$days = $endDate - strtotime($offsets, $startDate);
		$days = date('z', $days);

		return array($years, $months, $days);
	}

	/**
	 *	-	Назначение: Отправить сообщение на емейл
	 *	-	Дата: 30.06.2010 11:49:01
	 * @param struct Массив данных(title, message, email)
	 */
	public static function send($data) {
		$headers = 'From:' . $data['email_callback'] . "\r\n" . 'content-type: text/html; charset=KOI8-R' . "\r\n";
		return mail($data['email'], iconv("UTF-8", "KOI8-R//IGNORE", $data['title']),
				iconv("UTF-8", "KOI8-R//IGNORE", nl2br($data['message'])), $headers);
		//$headers = 'From:'. $data['email_callback'] . "\r\n". 'content-type: text/html; charset=cp1251' . "\r\n";

		//return mail($data['email'], iconv("UTF-8", "Windows-1251//IGNORE", $data['title']),  iconv("UTF-8", "Windows-1251//IGNORE", $data['message']), $headers);

		//$headers = 'From:'. $data['email_callback'] . "\r\n". 'content-type: text/html; charset=utf-8' . "\r\n";

		//return mail($data['email'], "=?utf-8?B?" . base64_encode($data['title']) . "?=", $data['message'], $headers);

	}
	public static function getDomain($string) {
		return strtr($string, array("http://" => "", "https://" => "", "www." => "", "/" => ""));
	}
	public static function br2nl($str) {
		$str = preg_replace("/(rn|n|r)/", "", $str);
		return preg_replace("=<br */?>=i", "n", $str);
	}
	/* public static function send($data) {
	    Zend_Service_Amazon_Ses::setKeys('AKIAIR4SGH3XBPMAHJ7Q', '0++4E+M0duAcb28BUdmf2Oz79CvqKPEPlCtwYJB3');
	    $email = new Zend_Service_Amazon_Ses_Email();
	    $email->addTo($data['email'])
	        ->setFrom($data['email_callback'], 'uchtorg')
	        ->setSubject($data['title'])
	        //->setBodyText($data['email'])
	        ->setBodyHtml($data['message']);
	    
	    $ses = new Zend_Service_Amazon_Ses();
	    $ses->sendEmail($email);
	    return true;
	}
	 */

	/**
	 * Получить список файлов директории
	 * @param string Название папки
	 * @param boolean Только папки
	 * @param boolean recursive
	 * @return array
	 */
	static function getListDir($directory, $onlydir = false, $recursive = false) {
		if ($recursive) {
			return self::directoryToArray($directory, true);
		}

		$files = false;
		$dir = opendir($directory);

		while (false !== ($filename = readdir($dir))) {
			if ($filename !== '.' && $filename !== '..') {
				if ($onlydir) {
					if (is_dir($directory . '/' . $filename)) {
						$files[] = $filename;
					}
				} else {
					$files[] = $filename;
				}
			}
		}

		return $files;
	}

	/**
	 *	-	Назначение: Рекурсивно прочитать каталог
	 *	-	Дата: 02.02.2011 9:47:40
	 * @param string directory
	 * @param boolean recursive false
	 * @return struct
	 */ 
	static function directoryToArray($directory, $recursive = true) {
		$array_items = array();

		if ($handle = opendir($directory)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if (is_dir($directory . "/" . $file)) {
						if ($recursive) {
							$array_items = array_merge($array_items,
									self::directoryToArray($directory . "/" . $file, $recursive));
						}

						$file = $directory . "/" . $file;
						if (is_file($file)) {
							$array_items[] = preg_replace("/\/\//si", "/", $file);
						}
					} else {
						$file = $directory . "/" . $file;
						if (is_file($file)) {
							$array_items[] = preg_replace("/\/\//si", "/", $file);
						}
					}
				}
			}

			closedir($handle);
		}

		return $array_items;
	}

	/**
	 *	-	Назначение: Обработка масива: удаление пустых елементов самого масива и его подмасивов
	 *	-	Дата: 17.02.2011 10:03:18
	 * @param {array, object, struct}
	 * @return array
	 * @version 1.0
	 */
	static function clearEntity($array) {
		if (is_object($array)) {
			$array = $array->toArray();
		}

		$result = array();
		foreach ($array as $id => $value) {
			if (is_array($value)) {
				$value = self::clearEntity($value);
			}
			if ($value != '' and $value != "0") {
				$result[$id] = $value;
			}
		}
		if (count($result) == 0) {
			return;
		}
		return $result;
	}

	static function translit($str) {
		$tr = array("Ґ" => "G", "Ё" => "YO", "Є" => "E", "Ї" => "YI", "І" => "I", "і" => "i", "ґ" => "g", "ё" => "yo",
				"№" => "#", "є" => "e", "ї" => "yi", "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
				"Е" => "E", "Ж" => "ZH", "З" => "Z", "И" => "I", "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M",
				"Н" => "N", "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F",
				"Х" => "H", "Ц" => "TS", "Ч" => "CH", "Ш" => "SH", "Щ" => "SCH", "Ъ" => "'", "Ы" => "YI", "Ь" => "",
				"Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
				"е" => "e", "ж" => "zh", "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m",
				"н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f",
				"х" => "h", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "'", "ы" => "yi", "ь" => "",
				"э" => "e", "ю" => "yu", "я" => "ya");
		return strtr($str, $tr);
	}

	/**
	 * генерация сео - тайтла для строки
	 */
	static function stitle($str, $length = 56000) {
		$str = self::translit($str);
		$str = strtolower($str);
		$str = @preg_replace('/[^\w]/', '-', $str);
		while (strpos($str, '--') !== false)
			$str = str_replace('--', '-', $str);
		$str = trim($str, '-');
		if (strlen($str) > $length) {
			$p = explode('-', $str);
			$c = array();
			foreach ($p as $el)
				$c[] = strlen($el);
			while (strlen($str) > $length) {
				$i = array_search(max($c), $c);
				if ($i === false)
					break;
				unset($c[$i]);
				unset($p[$i]);
				$str = implode('-', $p);
			}
		}
		return $str;
	}

	//сформировать URL
	static function seo_friendly_url($url) {

		$cyrillic = array("а", "б", "в", "г", "д", "ѓ", "е", "ж", "з", "ѕ", "и", "ј", "к", "л", "љ", "м", "н", "њ",
				"о", "п", "р", "с", "т", "ќ", "у", "ф", "х", "ц", "ч", "џ", "ш", "А", "Б", "В", "Г", "Д", "Ѓ", "Е",
				"Ж", "З", "Ѕ", "И", "Ј", "К", "Л", "Љ", "М", "Н", "Њ", "О", "П", "Р", "С", "Т", "Ќ", "У", "Ф", "Х",
				"Ц", "Ч", "Џ", "Ш");

		$latin = array("a", "b", "v", "g", "d", "gj", "e", "z", "z", "dz", "i", "j", "k", "l", "lj", "m", "n", "nj",
				"o", "p", "r", "s", "t", "kj", "u", "f", "h", "c", "ch", "dj", "sh", "A", "B", "V", "G", "D", "Gj",
				"E", "Z", "Z", "Dz", "I", "J", "K", "L", "Lj", "M", "N", "Nj", "O", "P", "R", "S", "T", "Kj", "U", "F",
				"H", "C", "Ch", "Dj", "Sh");

		$url = str_replace($cyrillic, $latin, $url);
		$url = strtolower($url);

		$url = preg_replace('/[^a-zA-Z0-9\-_\s]/', '', $url);

		$url = str_replace(array(' ', '_'), '-', $url);

		if (strpos($url, '--') != false) {
			$url = preg_replace('/\-{2,}/', '-', $url);
		}

		if ($url[strlen($url) - 1] == '-') {
			$url[strlen($url) - 1] = '';
		}

		return $url;
	}
	/**
	 * num - значение
	 * text[0] - значения для чисел, результат которых по модулю 10 равный 1 и число != 11
	 * text[1] - значения для чисела 11
	 * text[2] - значения для чисел, результат которых по модулю 10 попадает в интервал [2, 4] и число < 20 && > 10
	 * text[3] - значения для чисел, результат которых по модулю 10 попадает в интервал [2, 4] и число > 20
	 * text[4] - остальные числа
	 */
	static function getWordsByNumber($num, $text = array()) {
		$string = $text[4];
		if ($num != 0) {
			$rest = $num % 10 == 0 ? 9 : $num % 10;
			if ($rest == 1) {
				if ($num != 11) {
					$string = $text[0];
				} else {
					$string = $text[1];
				}
			}
			if ($rest > 1 && $rest < 5) {
				if ($num < 20 && $num > 10) {
					$string = $text[2];
				} else {
					$string = $text[3];
				}

			}
			if ($rest > 4) {
				$string = $text[4];
			}
		}
		return str_replace("#num#", $num, $string);
	}
	/// Склоняемость возраста
	static function AgeToStr($days, $month, $year) {
		$now_year = date("Y", time());
		$now_days = date("d", time());
		$now_month = date("m", time());
		$months = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь",
				"Ноябрь", "Декабрь");
		$m = 1;
		$year = $now_year - $year;
		for ($i = 1; $i <= count($months); $i++) {
			if ($months[$i - 1] == $month) {
				$m = $i;
				break;
			}
		}
		if ($now_month == $m) {
			if ($now_days > $days) {
				$year--;
			}
		} else {
			if ($now_month < $m) {
				$year--;
			}
		}
		$Age = $year;
		if (($Age >= 5) && ($Age <= 14))
			$str = "лет";
		else {
			$num = $Age - (floor($Age / 10) * 10);

			if ($num == 1) {
				$str = "год";
			} elseif ($num == 0) {
				$str = "лет";
			} elseif (($num >= 2) && ($num <= 4)) {
				$str = "года";
			} elseif (($num >= 5) && ($num <= 9)) {
				$str = "лет";
			}
		}
		return $Age . " " . $str;
	}
	/**
	 * Функция для форматирования даты
	 * @param string DATE
	 * @param string YEAR
	 * @param boolean textformat если true отдаст месяц текстом
	 * @return string Formatted date
	 */
	static function formatDate($date) {
		$month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
				'ноября', 'декабря');
		$date_time_string = date("Y-m-d", strtotime($date));
		$date_elements = explode('-', $date_time_string);
		return $date_elements[2] . ' ' . $month[(int) $date_elements[1] - 1] . ' ' . $date_elements[0];
	}

	static function format_fsize($size) {
		$metrics[0] = 'bytes';
		$metrics[1] = 'KB';
		$metrics[2] = 'MB';
		$metrics[3] = 'GB';
		$metrics[4] = 'TB';
		$metric = 0;
		while (floor($size / 1024) > 0) {
			++$metric;
			$size /= 1024;
		}
		$ret = round($size, 1) . " " . (isset($metrics[$metric]) ? $metrics[$metric] : '??');
		return $ret;
	}

}

?>