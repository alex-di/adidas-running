<?php

set_include_path(get_include_path() . PATH_SEPARATOR . '/library/Phpexcel/PHPExcel/');
include_once ROOT_PATH . '/library/Phpexcel/PHPExcel.php';
include(ROOT_PATH. "/library/Phpexcel/PHPExcel/Writer/Excel5.php");

class DR_Api_Tools_Excel
{
    
    var $headers = array();
    var $data = array();
    var $current_row = 1;
    var $writer = null;
    var $sheet = null;
    
    public function setHeaders($array) {
        $this->headers = $array;
    }
    
    public function write($array) {
        $this->writer = new PHPExcel();
        $this->writer->setActiveSheetIndex(0);
        $this->sheet = $this->writer->getActiveSheet();
        $this->sheet->setTitle('Данные');
        
        if(count($this->headers)) {
           
            $this->writeHeaders();
        }
        if(count($array)) {
            
            $this->writeData($array);
        }
        
        $objWriter = new PHPExcel_Writer_Excel5($this->writer);
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="rate.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter->save('php://output');
        
    }
    
    public function writeHeaders() {
        $i = 65;  //A
        foreach($this->headers as $title) {
            
            $column = chr($i); 
            
            #Устанавливаем автоширину для колонок
            $this->writer->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            
            $this->sheet->setCellValue($column. $this->current_row, $title);
            $i++;
        }
        
        $this->current_row++;
        
    }
    
    public function writeData($array) {
        foreach($array as $row) {
            $i = 65;  //A
            foreach($row as $col) {
                $column = chr($i); 
                $this->sheet->setCellValue($column. $this->current_row, $col);
                $i++;
            }
            $this->current_row++;
        }
    }
    
   
}