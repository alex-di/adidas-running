<?php

class DR_Api_Validate_Manager {
	protected $validators = array();
	protected $current_elements = null;
	protected $view = null;
	protected $notRequiredFields = array();
	protected $fieldsAllowedForSave = array();

	public function __construct() {
		$this->view = Zend_Layout::getMvcInstance()->getView();
	}
	/**
	 *  установить не обьязательные поля
	 */
	public function setNotRequiredFields($fields = array()) {
		$this->notRequiredFields = $fields;
		return $this;
	}
	/**
	 *  установить поля допустимые для сохранения
	 */
	public function setFieldsAllowedForSave($fields = array()) {
		$this->fieldsAllowedForSave = $fields;
		return $this;
	}
	/**
	 *  встановить поле
	 */
	public function setElement($elements_name) {
		$this->current_elements = $elements_name;
		return $this;
	}
	/**
	 *  сбросить валидаторы
	 */
	public function resetValidator() {
		$this->validators[$this->current_elements]['validators'] = new Zend_Validate();
		return $this;
	}
	/**
	 *  сбросить валидаторы и установить валидатор проверки на пустоту
	 */
	public function resetValidatorAndSetNotEmpty($message = 'Обязательное поле') {
		$this->validators[$this->current_elements]['validators'] = new Zend_Validate();
		$tempValidator = new Zend_Validate_NotEmpty();
		$tempValidator->setMessage($this->view->translate($message));
		$this->validators[$this->current_elements]['validators']->addValidator($tempValidator);
		return $this;
	}
	/**
	 *  добавить валидатор
	 */
	public function addValidator($validator, $message = null) {
		if (!is_null($message)) {
			if (is_string($message)) {
                $messageTemplates = $validator->getMessageTemplates();
                $keys = array_keys($messageTemplates);
                foreach($keys as $key)
				    $validator->setMessage($message, $key);
			} else {
				foreach ($message as $key => $val) {
					$validator->setMessage($val, $key);
				}
			}
		}

		if (isset($this->validators[$this->current_elements]['validators']))
			$this->validators[$this->current_elements]['validators']->addValidator($validator);
		else {
			if (!isset($this->validators[$this->current_elements]['validatorChain']))
				$this->validators[$this->current_elements]['validatorChain'] = array();
			$this->validators[$this->current_elements]['validatorChain'][] = $validator;
		}
		return $this;
	}
	/**
	 *  валидация на вхождения числа в заданный интервал
	 */
	public function between($message = null, $params = array('min' => 10, 'max' => 20)) {
	    $this->setDefaultMessage($message, "'%value%' не находится между '%min%' и '%max%'");
		return $this->addValidator(new Zend_Validate_Between($params), $message);
	}
	/**
	 * валидация даты
	 */
	public function date($message = null, $params = array('format' => 'Y-m-d H:i:s')) {
	    $this->setDefaultMessage($message, "не верный формат даты");
		return $this->addValidator(new Zend_Validate_Date($params), $message);
	}
	/**
	 * валидация наличия записи в таблице БД
	 */
	public function recordExists($message = null, $params = array('table' => api::USERS, 'field' => 'email')) {
        return $this->addValidator(new Zend_Validate_Db_RecordExists($params), $message);
	}
	/**
	 * валидация отсутсвия записи в таблице БД
	 */
	public function recordNoExists($message = null, $params = array('table' => api::USERS, 'field' => 'email')) {
		return $this->addValidator(new Zend_Validate_Db_NoRecordExists($params), $message);
	}
	/**
	 * валидация на число
	 */ 
	public function digits($message = null, $params = array()) {
		return $this->addValidator(new Zend_Validate_Digits(), $message);
	}
	/**
	 * валидация на email
	 */
	public function email($message = null, $params = array()) {
		return $this->addValidator(new Zend_Validate_EmailAddress(), $message);
	}
	/**
	 * валидация на число с плавающей точкой
	 */
	public function float($message = null, $params = array()) {
		return $this->addValidator(new Zend_Validate_Float(array('locale' => 'en')), $message);
	}
	/**
	 * валидация на целое число
	 */
	public function int($message = null, $params = array()) {
		return $this->addValidator(new Zend_Validate_Int(), $message);
	}
	/**
	 * валидация IP
	 */
	public function ip($message = null, $params = array()) {
		return $this->addValidator(new Zend_Validate_Ip($params), $message);
	}
	/**
	 * валидация длины строки
	 */
	public function stringLength($message = null, $params = array('min' => 1, 'max' => 6, 'encoding' => 'UTF-8')) {
		return $this->addValidator(new Zend_Validate_StringLength($params), $message);
	}
	/**
	 * валидация номера телефона
	 */
	public function phone($message = null, $params = array()) {
		return $this->addValidator(new DR_Api_Validate_Validators_Telephone($params), $message);
	}
	/**
	 * валидация
	 */
	public function validate($data) {
		$result = array();
		$isValid = true;
		foreach ($data as $name => $value) {
			if (count($this->fieldsAllowedForSave) && !in_array($name, $this->fieldsAllowedForSave)) {
				$result[$name] = "Недопустимое поле";
				$isValid = false;
				break;
			}
			if (is_array($value))
				continue;
				
			$result[$name] = '';
			$validatorChain = new Zend_Validate();
			if (isset($this->validators[$name]) && isset($this->validators[$name]['validators'])) {
				if(in_array($name, $this->notRequiredFields) && empty($value))
					continue;
				$validatorChain = $this->validators[$name]['validators'];
			} else {
				if (!in_array($name, $this->notRequiredFields)) {
					$tempValidator = new Zend_Validate_NotEmpty();
					$tempValidator->setMessage($this->view->translate("Заполните поле"));
					$validatorChain->addValidator($tempValidator);
				}
				if (!empty($value)) {
					switch ($name) {
						case "captcha":
							$tempValidator = new Zend_Validate_Captcha();
							$validatorChain->addValidator($tempValidator);
							break;
						case "password2":
							$tempValidator = new Zend_Validate_Identical(array("token" => $data['password']));
							$tempValidator->setMessage($this->view->translate("Введенные пароли не совпадают"));
							$validatorChain->addValidator($tempValidator);
							break;
						case "mainpassword":
							$model_users = api::getUsers();
							$user = $model_users->fields(array('password'))->_new()
									->where('t.id', Zend_Auth::getInstance()->getIdentity()->id)->row();
							$tempValidator = new Zend_Validate_Identical(array("token" => $user->password));
							$tempValidator->setMessage($this->view->translate("Текущий пароль указан неверно"));
							$validatorChain->addValidator($tempValidator);
							break;
						case "password":
							$validatorChain->addValidator(new DR_Api_Validate_Validators_Password());
							break;
					}
					if (isset($this->validators[$name]) && isset($this->validators[$name]['validatorChain'])) {
						foreach ($this->validators[$name]['validatorChain'] as $validator)
							$validatorChain->addValidator($validator);
					}
				}

			}
			if (!$validatorChain->isValid($value)) {
				$messages = array_values($validatorChain->getMessages());
				$result[$name] = $messages[0];
				$isValid = false;
			}
		}
		return array($isValid, $result);
	}
    protected function setDefaultMessage(&$message, $default_value) {
        if(is_null($message))
            $message = $default_value;
    }
}
