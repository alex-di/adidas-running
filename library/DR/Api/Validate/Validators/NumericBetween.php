<?php




class DR_Api_Validate_Validators_NumericBetween extends Zend_Validate_Abstract
{
    const MSG_NUMERIC = 'msgNumeric';
    const MSG_MINIMUM = 'msgMinimum';
    const MSG_MAXIMUM = 'msgMaximum';
 
    public $minimum = 0;
    public $maximum = 100;
 	public function __construct($min, $max) {
 		$this->minimum = $min;
		$this->maximum = $max;
 	}
    protected $_messageVariables = array(
        'min' => 'minimum',
        'max' => 'maximum'
    );
 
    protected $_messageTemplates = array(
        self::MSG_NUMERIC => "'%value%' не является числом",
        self::MSG_MINIMUM => "'%value%' должен быть не меньше '%min%'",
        self::MSG_MAXIMUM => "'%value%' должен быть не больше '%max%'"
    );
 
    public function isValid($value)
    {
        $this->_setValue($value);
 
        if (!is_numeric($value)) {
            $this->_error(self::MSG_NUMERIC);
            return false;
        }
 
        if ($value < $this->minimum) {
            $this->_error(self::MSG_MINIMUM);
            return false;
        }
 
        if ($value > $this->maximum) {
            $this->_error(self::MSG_MAXIMUM);
            return false;
        }
 
        return true;
    }
}

