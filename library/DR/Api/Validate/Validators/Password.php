<?php




class DR_Api_Validate_Validators_Password extends Zend_Validate_Abstract 
{
    /**
     * Метка ошибки
     * @var const 
     */    
    const INVALID = 'passwordInvalid';

    /**
     * Метка ошибки
     * @var const 
     */    
    const INVALID_LENGTH = 'passwordBadLength';    
    
    /**
     * Текст ошибки
     * @var array 
     */
    protected $_messageTemplates = array(
        self::INVALID => 'Можно использовать только латинские буквы и цифры',
        self::INVALID_LENGTH => 'Пароль не может быть меньше 6 и больше 20 символов'
    );

    /**
     * Проверка пароля
     * 
     * @param string $value значение которое поддается валидации
     */
    public function isValid($value) 
    {
        $this->_setValue($value);
        
        $validatorStringLength = new Zend_Validate_StringLength(6, 30);
        
        if (!$validatorStringLength->isValid($value)) {
            $this->_error(self::INVALID_LENGTH);
            return false;            
        }
        elseif (!preg_match("/^[~!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\\\\/\\{\\}\\[\\].,\\?<>:;a-z0-9]*$/i", $value)) {
            $this->_error(self::INVALID);
            return false;            
        }

        return true;
    }
}