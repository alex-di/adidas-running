<?php

class DR_Api_Validate_Validators_IsExists extends Zend_Validate_Abstract
{
    const NOT_EXISTS = false;
    const EXISTS = true;

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::EXISTS => "Не существует",
        self::NOT_EXISTS => "Уже существует",
        );

    /**
     * @var array
     */
    protected $_messageVariables = array('row' => '_row');

    /**
     * RowName
     *
     * @var mixed
     */
    protected $_row;

    /**
     * db
     *
     * @var mixed
     */
    protected $_db;

    /**
     * @var mode 
     */
    protected $_mode;


    /**
     * Sets validator options
     *
     * @param string RowName
     * @param mixed|Zend_Table_Abstract
     * @return void
     */
    public function __construct($row, $db, $mode = true)
    {
        if (is_object($db))
        {
            $this->_db = $db;
        }

        $this->_mode = $mode;

        if (is_array($row))
        {
            if (array_key_exists('row', $row))
            {
                $row = $row['row'];
            } else
            {
                require_once 'Zend/Validate/Exception.php';
                throw new Zend_Validate_Exception("Missing option 'row'");
            }
        }

        $this->setRow($row);
    }


    /**
     * Sets the max option
     *
     * @param  mixed $max
     * @return Zend_Validate_LessThan Provides a fluent interface
     */
    public function setRow($rowName)
    {
        $this->_row = $rowName;
        return $this;
    }

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is less than max option
     *
     * @param  mixed $value
     * @return boolean
     */
    public function isValid($value)
    {

        $M = $this->_db;
        if ("all" === $this->_mode)
        {
            return true;
        }

        if ($this->_mode)
        {
            if (!$M->checkRow($this->_row, $value))
            {
                $this->_error(self::EXISTS);
                return false;
            }
        } else
        {
            //print_r($M->checkRow( array("where" => array($this->_row => $value)))); die;
            if ($M->checkRow($this->_row, $value))
            {

                $this->_error(self::NOT_EXISTS);
                return false;
            }
        }


        return true;
    }


}
