<?php
/**
  * Хелпер для интеграции с spaw 2 
  * @author apalach
  */ 
  
/**
 * Подключаем библиотеку редактора
 */
 require_once($_SERVER['DOCUMENT_ROOT']. "/design/modules/spaw2/spaw.inc.php");
 
/**
 * Хедпер для ZF
 */
class DR_View_Helper_Spaw
{
    /**
     * Получить редактор
     */
     
    function spaw($name, $value = null, $mode = 'standart', $width = 485, $height = 200)
    {
        /**
         * Настройки
         */         
         switch($mode)
         {
            case "mini":
                SpawConfig::setStaticConfigItem('default_toolbarset','mini');
                SpawConfig::setStaticConfigItem('default_width','250px');
                SpawConfig::setStaticConfigItem('default_height','100px');
            break;
            
            case "miniMiddle":
                SpawConfig::setStaticConfigItem('default_toolbarset','mini');
                SpawConfig::setStaticConfigItem('default_width','485px');
                SpawConfig::setStaticConfigItem('default_height','200px');
            break;
            
            case "control":
                SpawConfig::setStaticConfigItem('default_toolbarset','custom');
                SpawConfig::setStaticConfigItem('default_width',$width);
                SpawConfig::setStaticConfigItem('default_height',$height);
            break;
            
            case "standart":
                SpawConfig::setStaticConfigItem('default_toolbarset','custom');
            break;
         }

        $userLogin = '';
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $userLogin = str_replace("@", "_", Zend_Auth::getInstance()->getIdentity()->email) . '/';
        }

        $spaw = new SpawEditor($name, $value);
                
        $this->createSubDirectory('/data/spaw/images/' . $userLogin , 0777);
        $spaw->setConfigItem(
        'PG_SPAWFM_DIRECTORIES',
          array(
            array(
              'dir'     => '/data/spaw/images/' . $userLogin,
              'caption' => 'Images',
              'params'  => array(
                'allowed_filetypes' => array('images')
              )
            ),
          ),
          SPAW_CFG_TRANSFER_SECURE
        );
                  
        /**
         * Возвращаем данные
         */
        return $spaw->getHtml();
    }
    
    
        /**
     * Создание папки с проверкой
     * @param string patch
     * @param integer [chmod]
     */
    function createSubDirectory($path, $chmod = 0777, $root_patch = null)
    {
        if(!is_null($root_patch))
        {
            $rootVar = $_SERVER['DOCUMENT_ROOT'] . $root_patch;
        }
        else
        {
            $rootVar = $_SERVER['DOCUMENT_ROOT'];
        }
        $PatchArray = explode('/',$path);
            
        $myPatch = '';
        foreach($PatchArray as $number => $path)
        {
            if($path)
            {
                $myPatch .= "/" . $path;
                    
                if(!is_dir($rootVar . $myPatch))
                {
                    mkdir($rootVar . $myPatch, $chmod);                           
                }                                
            }
        }          
    }
}

?>

