<?php

/**
 * @version 1.0
 * @author yurecboss
 */
class DR_View_Helper_Captcha extends Zend_View_Helper_Abstract
{
    public function captcha($width = 240, $height = 50)
    {
        Zend_Captcha_Word::$VN = Zend_Captcha_Word::$CN = range(0, 9);

        $captcha = new Zend_Captcha_Image(array(
            'captcha' => 'Image',
            'wordLen' => 5,
            'width' => $width,
            'heigh' => $height,
            'timeout' => 300,
            'imgDir' => $_SERVER['DOCUMENT_ROOT'] . '/data/captcha/',
            'font' => $_SERVER['DOCUMENT_ROOT'] . '/fonts/arial.ttf',
            'imgUrl' => '/data/captcha/',

            /*Линии и точки*/
            'dotNoiseLevel' => 0,
            'lineNoiseLevel' => 0));
        $id = $captcha->generate();
        $html = $captcha->render($this->view);
        return array($id, $html);
    }
}
