<?php
/**
 * @version 2.0
 */
class DR_View_Helper_FlashVideo
{
    /**
     * Если библиотеки уже загружены(флаг)
     */
    static $already = false;
    
      /**
       * Конструктор
       * @param string elementId
       * @return HTML
       */
      function FlashVideo( $file, $width = 420, $height = 330, $autoStart = true, $type = 'uppod')
      {            
            $data = (string)'';
            
            if(!self::$already)
            {
                switch($type)
                {
                    case "flowplayer":
                        $data .= '<script type="text/javascript" src="/design/modules/flowplayer/js/flowplayer.js"></script>';
                    break;
                    
                    case "uppod":
                        $data .= '<script type="text/javascript" src="/design/modules/uppod/js/swfobject.js"></script>';
                    break;
                }
                                
                self::$already = true;                 
            }
                        
            $element = md5(microtime());
    		
            switch($type)
            {
                case "flowplayer":
                    $data .= "<a href='$file' style='display:block;width:{$width}px;height:{$height}px' id='{$element}'></a>";            
                    $data .= '<script type="text/javascript">';
                    
                    $data .='flowplayer("'. $element .'", {src: "/design/modules/flowplayer/swf/flowplayer-3.2.5.swf", wmode: \'opaque\'},
        				     {
        						clip: 
                                {
        						     url: \''. $file .'\',
        						     scaling: \'fit\',
        						     autoBuffering: true,
        						     provider: \'pseudo\',
                                     autoPlay: true
        						},
        						plugins: 
                                {
        						     pseudo: 
                                     {
        						          url: \'/design/modules/flowplayer/swf/flowplayer.pseudostreaming-3.2.5.swf\'
        						      }
        						},
        				    });';
                    $data .= '</script>';
                break;
                
                case "uppod":
		    $data .= '<div id="'. $element .'"></div>';
                    $data .= '<script type="text/javascript">
                        var flashvars = {"comment":"","st":"/design/modules/uppod/css/video135-69.txt","m":"video","file":"'. $file .'"};
                        var params = {bgcolor:"#ffffff", wmode:"window", allowScriptAccess:"always", allowFullScreen:"true",id:"'. $element .'"}; 
                        swfobject.embedSWF("/design/modules/uppod/swf/uppod.swf", "'. $element .'", "'. $width .'", "'. $height .'", "9.0.115.0", false, flashvars, params);';
                    $data .= '</script>';                    
                break;
            }
                                
            $data .= '</script>';            
            
                        
            return $data;
      }        
            
}