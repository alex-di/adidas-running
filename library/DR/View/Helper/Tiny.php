<?php

class DR_View_Helper_Tiny
{
    static $already = false;
    const WIDTH = 800;
    const HEIGHT = 400;
    /**
     * Получить редактор
     */
     
    function tiny($name, $value = '', $options = array())
    {
        $data = '';
        if(!self::$already)
        {
            $data .= '<script type="text/javascript" src="/design/modules/tinymce/js/tinymce/tinymce.min.js"></script>';
            self::$already = true;                 
        }
        $options['selector'] = 'textarea[name="'.$name.'"]';
        if(!isset($options['theme']))
            $options['theme'] = 'modern';
        if(!isset($options['width']))
            $style = 'style="width:100%"';
            //$options['width'] = self::WIDTH;
        if(!isset($options['height']))
            $options['height'] = self::HEIGHT;
        if(!isset($options['language_namespace']))
            $options['language_namespace'] = 'ru';
        if(!isset($options['content_css']))
            $options['content_css'] = '/design/css/reset.css,/design/css/style.css,/design/css/fixes.css';
        $options['language_url'] = '/design/modules/tinymce/langs/'.$options['language_namespace'].'.js';
        if(!isset($options['plugins']))
            $options['plugins'] = array("advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                                "paste textcolor"
                                        );

    

        if(!isset($options['toolbar']))
            $options['toolbar'] = "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image";

        $options['body_class'] = 'tiny_editor';
        $options['convert_urls'] = false;
        $options['relative_urls'] = false;
        $data .= '<textarea id="'.strtr($name, array('['=>'_', ']'=>'_')).'" name="'.$name.'" '.$style.' class="tiny_block_editor">'.$value.'</textarea>';
        $data .= '<script type="text/javascript">tinymce.init('.json_encode($options).');</script>';

        return $data;
    }
    
    
        /**
     * Создание папки с проверкой
     * @param string patch
     * @param integer [chmod]
     */
    function createSubDirectory($path, $chmod = 0777, $root_patch = null)
    {
        if(!is_null($root_patch))
        {
            $rootVar = $_SERVER['DOCUMENT_ROOT'] . $root_patch;
        }
        else
        {
            $rootVar = $_SERVER['DOCUMENT_ROOT'];
        }
        $PatchArray = explode('/',$path);
            
        $myPatch = '';
        foreach($PatchArray as $number => $path)
        {
            if($path)
            {
                $myPatch .= "/" . $path;
                    
                if(!is_dir($rootVar . $myPatch))
                {
                    mkdir($rootVar . $myPatch, $chmod);                           
                }                                
            }
        }          
    }
}

?>

