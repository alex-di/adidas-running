<?php
/**
 * метаданные 
 */
class DR_View_Helper_Metadata extends Zend_View_Helper_Abstract{

	public function metadata($default_title) {
		$title = $this->view->headTitle();
		$title = strtr($title, array('<title>'=>'', '</title>'=>''));
		if(empty($title))
			$this->view->headTitle($default_title);
		return $this->view->headTitle().$this->view->headMeta();
	}
}