<?php
/**
 * @version 3.0
 * @author yurecboss
 * http://www.plupload.com/documentation.php
 */
class DR_View_Helper_Upload
{
    /**
     * Если библиотеки уже загружены(флаг)
     */
    static $already = false;
    
      /**
       * Конструктор
       * @param string elementId
       * @param string module
       * @param string runtimes - flash или html5(drag files)
       * @param array определение callback функций (init, queueChanged, filesAdded, uploadProgress, error, fileUploaded).
       * init - будет вызвана при инициализации загрузчика. аргументами функции являютя (up, params).
       * queueChanged - будет вызвана при изменении очереди файлов в загрузчике. аргументами функции являютя (up).
       * filesAdded - будет вызвана при добавлении файла в очередь загрузки. аргументами функции являютя (up, files).
       * uploadProgress - будет вызвана при процессе загрузки. аргументами функции являютя (up, file).
       * fileUploaded - будет вызвана после загрузки файла. аргументами функции являютя (up, file, info).
       * error - будет вызвана в случае возникновения ошибки. аргументами функции являютя (up, err).
       */
      function upload($element, $module, $runtimes = 'flash', $args = array(), $isMultiselect = false, $savePath = null)
      {
            $data = '';
    	    if(isset($args['already'])) {
    		  self::$already = true;
    	    }
            if(!self::$already)
            {
                $data .= '<script type="text/javascript" src="/design/modules/plupload/js/plupload.full.js"></script>';
                $data .= '<script type="text/javascript" src="/design/modules/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>';
                $data .= '<script type="text/javascript" src="/design/modules/plupload/my/scripts.js"></script>';
                              
            }
	    
	    
            $data .= '<script type="text/javascript">';
            $args['url'] = "/files/index/upload/moduleName/".$module;
            if(!is_null($savePath)) {
                $args['url'] .= '/path/'.$savePath;
            }
            $args['flash_swf_url'] = "/design/modules/plupload/js/plupload.flash.swf";
            $args['runtimes'] = $runtimes;
            if("flash" == $runtimes) {
                $args['browse_button'] = $element;
                $args['multipart_params'] = array("PHPSESSID"=>session_id());
            } else {
                $args['drop_element'] = $element;
            }

	    
	    
            $args['multi_selection'] = $isMultiselect;
            $args['filters'] = array("title"=>"Files", "extensions"=>self::__ext($module));
            $settings = Zend_Registry::get('settings');
            $args['max_file_size'] = (isset($settings[$module . '_max_size']) ? $settings[$module . '_max_size'] : 5). 'kb';
            
            $data .= 'var uploader_'.$element.' = {}; $(function(){Uploader.init(uploader_'.$element.', '.json_encode($args).');});';
            $data .= '</script>';
            return $data;
      }
      
	/**
     * @param string moduleName
     * @return implode String
   	 */
    public static function __ext($module)
    {
        $lists = Zend_Registry::get('lists');                
                
        if(is_array($lists) && is_array($lists[$module . '_extension']))
        {
            return implode(",", $lists[$module . '_extension']);
        }
        
        return "";                
    }
            
}