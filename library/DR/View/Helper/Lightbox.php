<?php
/**
 * @version 1.0
 */
class DR_View_Helper_Lightbox
{    
      /**
       * Конструктор
       * @param string elementId
       * @return HTML
       */
      function Lightbox( $element )
      {                        
            $data = (string)'';
            
            $data .= '<style type="text/css">@import url(/design/modules/lightbox/css/jquery.lightbox-0.5.css);</style>';
            $data .= '<script type="text/javascript" src="/design/modules/lightbox/js/jquery.lightbox-0.5.js"></script>';
            $data .= '<script type="text/javascript">$(\'#'. $element .' a\').lightBox()</script>';
                        
            return $data;
      } 
}