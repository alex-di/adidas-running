<?php

/**
 * https://github.com/devbridge/jQuery-Autocomplete 
 */
class DR_View_Helper_Autocompleate
{
    /**
     * @var Flag Если библиотеки уже загружены
     */
    static $already = false;

    /**
     * Конструктор
     * @param string elementId
     * @return HTML
     */
    function Autocompleate($element, $action, $callback = null, $options = array())
    {
        $data = (string )'';

        if (!self::$already)
        {
            $data = '<style type="text/css">@import url(/design/modules/autocomplete/css/styles.css);</style>';
            $data .= '<script type="text/javascript" src="/design/modules/autocomplete/dist/jquery.autocomplete.js"></script>';
            self::$already = true;
        }
        $options['serviceUrl'] = $action;
        $data .= '<script type="text/javascript">';
        $options = json_encode($options);
        if(!is_null($callback)) {
            $options = substr($options, 0, strlen($options)-1);
            $options .= ', "onSelect":'.$callback.'}'; 
        }
        $data .= <<< HTML
                $(function()
                {
                    $("#$element").autocomplete($options);        
                });            
HTML;
        $data .= '</script>';

        return $data;
    }
}
