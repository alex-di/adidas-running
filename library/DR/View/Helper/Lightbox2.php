<?php
/**
 * @version 1.0
 */
class DR_View_Helper_Lightbox2
{    
      /**
       * Конструктор
       * @param string elementId
       * @return HTML
       */
      function Lightbox2( )
      {                        
            $data = (string)'';
            //$data .= '<script type="text/javascript" src="/design/modules/lightbox2/js/jquery-1.7.2.min.js"></script>';
            $data .= '<script type="text/javascript" src="/design/modules/lightbox2/js/lightbox.js"></script>';
            $data .= '<link href="/design/modules/lightbox2/css/lightbox.css" rel="stylesheet" />';
            
            return $data;
      } 
}