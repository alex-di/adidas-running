<?php
/**
 * @version 1.0
 * @author yurecboss
 * http://jhtmlarea.codeplex.com/
 * стилизирований редактор
 */
class DR_View_Helper_Htmlarea
{
    /**
     * Если библиотеки уже загружены(флаг)
     */
    static $already = false;
    protected $view;
    
    /**
     * @param $name - название поля ввода
     * @param $value - значение
     * @param $width - ширина 
     * @param $height - высота
     * @param $imageButton - массив значений конфигурации кнопки загрузки изображений в редактор
     *      - ключ class - класс который будет присвоен кнопке
     *      - ключ upload - изпользовать загрузчик (также необходимо указать ключ module)
     *      - ключ module - модуль, из которого загрузчик возмет настройки
     *      - ключ action - вызываемое события при нажатии на кнопку или же используваемое загрузчиком.
     */
    public function htmlarea($name, $value, $width = "200px", $height = "200px", $imageButton = array())
    {
        $data = '';
        $id = strtr($name, array("["=>"", "]"=>""));
        if(!self::$already)
        {
            $data .= '<script type="text/javascript" src="/design/modules/jHtmlArea/scripts/jHtmlArea-0.7.5.js"></script>';
            $data .= '<link href="/design/modules/jHtmlArea/style/jHtmlArea.css" rel="stylesheet" type="text/css" />';
            self::$already = true;                 
        }
        $toolbar = '[["bold", "italic", "underline"],
                     ["h1", "h2", "h3", "h4", "h5", "h6"]';
        
        $class = "";
        $additional_script = "";
        $callbackUpload = "";
        if(count($imageButton)) {
            $class = "image".(isset($imageButton['class']) ? " ".$imageButton['class'] : "");
            $action = "";
            if(isset($imageButton['action'])) {
                $action = $imageButton['action'];
            }
            if(isset($imageButton['upload'])) {
                $callbackUpload = $action;
                if("" == $callbackUpload) {
                    $callbackUpload = "callbackHtmlAreaUpload";
                    $additional_script = 
                    "function callbackHtmlAreaUpload(up, fl, res){
                         var jsa = jQuery.parseJSON(res.response);
                         if(jsa.status == 'ok') {
                            console.log(jsa);
                            var currentHtml = $('#$id').htmlarea('toHtmlString');
                            $('#$id').htmlarea('html', currentHtml+'<img src=\"'+jsa.file+'\"/>');
                         }
                    }";
                }
                $action = "";
            }
            $action = $action == "" ? 0 : $action;
            $toolbar.= ", [{css: '$class', text: 'Add image', action: $action}]";
        }
        $toolbar .= ']';
        $data .= "<textarea id='$id' style='width:$width;height:$height;' name='$name'>$value</textarea>";
        $data .= '<script type="text/javascript">$(function(){'.
                    '$("#'.$id.'").htmlarea({toolbar: '.$toolbar.'});}); '.
                    $additional_script.
                 '</script>';
        if(isset($imageButton['upload'])) {
            $id = str_replace(" ", "_", $class);
            $data .= $this->view->upload($id, $imageButton['module'], 'flash', array("fileUploaded"=>$callbackUpload));
        }
        return $data;
        
    }
    public function setView($view)
    {
        $this->view = $view;
    }
}



