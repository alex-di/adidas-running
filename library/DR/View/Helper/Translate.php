<?php 
/**
 * 
 * @author yurecboss
 * @version 1.0
 */
class DR_View_Helper_Translate extends Zend_View_Helper_Translate
{
    const IS_ENABLE_SERVICE = '_launguages_enabled_';
    /**
     * перевод строки или массива
     */
    public function translate($phrase = null)
    {
        
       
        if (self::isLanguagesEnabled()) {
            
            if (is_array($phrase)) {
                $result = array();
                foreach ($phrase as $key => $value) {
                    $result[$key] = $this->processTranslate($value);
                }
                
                return $result;
            } else {
                return $this->processTranslate($phrase);
            }
        } 
        return $phrase;
    }
    
    private function processTranslate($phrase)
    {       
            // получаем перевод фразы в зависимости от языка
            $laungauge = Model_Languages::getPhraseTranslate();
            
            //_d($laungauge);
            if (isset($laungauge[$phrase]))
            {
                if (!empty($laungauge[$phrase]))
                    return $laungauge[$phrase];
                return $phrase;
            }
            // если такой фразы ещё нет, то сохраняем её
            Model_Languages::addPhraseToTranslate($phrase);
            
            return $phrase;
    }
    /**
     * разрешить режим мультиязичности
     */
    public static function enabledLanguages()
    {
        Zend_Registry::set(self::IS_ENABLE_SERVICE, true);
    }
    
    public static function isLanguagesEnabled()
    {
        return Zend_Registry::isRegistered(self::IS_ENABLE_SERVICE) ? Zend_Registry::get(self::IS_ENABLE_SERVICE) : false;
    }
}
