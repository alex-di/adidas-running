<?php

/**
 * @version 1.0
 * @author appalach
 */
class DR_View_Helper_Banner extends Zend_View_Helper_Abstract
{
    public function banner($stitle, $default)
    {
        $model_pages = api::getBanners();
        $banner = $model_pages($stitle, 'stitle');
        
        if(count($banner)) {
            $link = "<a target='_blank' class='limit' href='".$banner->link."' > <img src='".$banner->file."' /> </a>";
            return $link;//$banner->file;
        } else {
            return $default;
        }
          
    }
}
