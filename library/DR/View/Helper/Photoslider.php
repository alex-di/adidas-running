<?php

class DR_View_Helper_Photoslider
{
    /**
     * @version 1.0
     * @param string elementName 
     * @param string fileName
     * @param integer width          
    */
    public function photoslider($element, $files , $width = 640, $height = 400 )
    {       // print_r($files); die;
        $this->element = $element;
        $html = '';
        
        $html .= '<link href="/design/modules/rcarousel/css/rcarousel.css" rel="stylesheet" type="text/css" />';  
             
        $html .= '<style type="text/css">
			#container {
				width: 100%;
				position: relative;
			}
			
			#carousel {
				margin: 0 auto;
			}

			#carousel img {
				border: 0;
			}

			#ui-carousel-next, #ui-carousel-prev {
				width: 31px;
				height: 103px;
				background: url(url(/design/images/image_arrow_right.png))) #fff center center no-repeat;
				display: block;
				position: absolute;
				top: 0;
				z-index: 100;
			}

			#ui-carousel-next {
				right: 0;
                margin: 26px 10px 0 0;
				background-image: url(/design/images/image_arrow_right.png);
			}
            #ui-carousel-prev {
				left: 0;
                margin: 26px -45px 0 10px;
				background-image: url(/design/images/image_arrow_left.png);
			}

			#ui-carousel-next > span, #ui-carousel-prev > span {
				display: none;
			}
		</style>';       
        $html .= '<div id="container"><div id="carousel">';

        //$files = explode('|', $files);
        foreach($files  as $file )
        {
            $html.="<img src=".$file." />";
        }
        
        $html .= '</div><a href="#" id="ui-carousel-next"><span>next</span></a>
				<a href="#" id="ui-carousel-prev"><span>prev</span></a></div>';
$html .= '<script type="text/javascript" src="/design/modules/rcarousel/js/jquery.ui.rcarousel.min.js"></script>';   
        $html .= '<script type="text/javascript">
            jQuery(function($) {
				$( "#container").rcarousel({
					margin: 10, visible:5, width:155, height:155
				});
				
				$( ".gallery_big_img_right" )
					.add( ".gallery_big_img_left" )
					.hover(
						function() {
							$( this ).css( "opacity", 0.7 );
						},
						function() {
							$( this ).css( "opacity", 1.0 );
						}
					);					
			});
		</script';
        
        return $html;
    }
}