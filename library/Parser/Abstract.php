<?

/**
 *  
 */
class Parser_Abstract
{
    protected $consumer_key = '';
    protected $consumer_secret = '';
    protected $access_token = '';
    protected $access_token_secret = '';
    protected $store = null;
    protected $namespace = '0';
    protected $category_id = 0;
    protected $meta_key = 0;
    protected $data = array();

    public function getData()
    {
        return $this->data;
    }
    public function getMetaKey()
    {
        return $this->meta_key;
    }
}
