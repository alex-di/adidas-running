<?

/**
 *  
 */
class Parser_Social_Google extends Parser_Abstract
{

    public static function getCountLikes($resource)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $resource . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec ($curl);
        curl_close ($curl);
     
        $json = json_decode($curl_results, true);
        if(isset($json[0]['result']['metadata']['globalCounts']['count']))
            return intval( $json[0]['result']['metadata']['globalCounts']['count'] );
        return null;
    }
}
