<?

/**
 *  
 */
class Parser_Social_Instagram extends Parser_Abstract
{
    public function __construct()
    {

        $this->client_id = 'b44013ad20034855b17e78f10d461ebc';
        $this->client_key = "85e67c7c03b5445895a54f23c53f47fe";
		$this->namespace = 'http://instagram.com/';
		$this->category_id = Model_Materials::CATEGORY_INSTAGRAM_POST;
		$this->meta_key = Model_Meta::INTERNAL_KEY_INSTAGRAM_LAST_HASH_TAG;
    } 

    
    public function search($query, $params = array()) {
    	$query = urlencode($query);
    	$client = new Zend_Http_Client('https://api.instagram.com/v1/tags/'.$query.'/media/recent');
        $client->setParameterGet('client_id', $this->client_id);
        if(isset($params['min_id']) && isset($params['max_id']) && !is_null($params['min_id']) && !is_null($params['max_id'])) {
            //$client->setParameterGet('min_id', $params['min_id']);
            //$client->setParameterGet('max_id', $params['max_id']);
        }
        $response = $client->request();
        $body = json_decode($response->getBody());
        $result = array();
        $minId = null;
        $maxId = null;
        
		if(isset($body->data)) {
            foreach($body->data as $row) {
                //if('image' == $row->type) {
    				$uid = md5($this->namespace . $row->id);
        			$result[$uid]['id'] = $row->id;
        			$result[$uid]['category_id'] = $this->category_id;
        			$result[$uid]['text'] = '';
        			$user = array();
        			$user['screen_name'] = $row->user->username;
        			$user['avatar'] = $row->user->profile_picture;
        			$user['profile_link'] = $this->namespace . $row->user->username;
        			$result[$uid]['material_link'] = $row->link;
        			$result[$uid]['photo'] = $row->images->standard_resolution->url;
    				$result[$uid]['user'] = $user;
                //}

                if(is_null($minId)) {
                    $minId = $row->id;
                    $maxId = $row->id;
                } else {
                    if($minId > $row->id)
                        $minId = $row->id;
                    if($maxId < $row->id)
                        $maxId = $row->id;
                }
			}
		}
		$this->data = $result;
        return array($minId, $maxId);
    }
}