<?

/**
 *  
 */
class Parser_Manager{

	public static function execute($object) {
		$result = array('name'=>'', 'save'=>0, 'find'=>0, 'is_find'=>false);
		list($meta_id, $last_tag_id) = api::getMeta()->getIntValue($object->getMetaKey(), Model_Meta::INTERNAL);
		list($id, $name) = api::getList_values()->getNextHashTag($last_tag_id);
		if(!is_null($id)) {
            $namespace = strtolower(get_class($object)).'_'.strtolower($name);
            list($min_id, $max_id) = api::getMeta()->getParserMinMaxId($namespace);
            $result['name'] = $name;
			list($min_id, $max_id) = $object->search($name, array('min_id'=>$min_id, 'max_id'=>$max_id));
            api::getMeta()->saveParserMinMaxId($namespace, $min_id, $max_id);
            $data = $object->getData();
            $result['find'] = count($data);
            $result['save'] = api::getMaterials()->saveParserData($object->getData(), $id);          
			api::getMeta()->doSave(array('key'=>$object->getMetaKey(), 'modules_id'=>Model_Meta::INTERNAL, 'resource_id'=>0, 'int_value'=>$id), $meta_id);
            $result['is_find'] = true;
		}
		return $result;
	}
}