<?php

class MyMemcached {

	const BILLING_NAMESPACE = "billing_store";
	const EXPIRATION = 600;
	const MAX_DIFF_SECONDS = 10;

	public static function refreshBiillingUser($user_id) {
		$memcached = new Memcached();
		$isConnect = $memcached->addServer('localhost', 11211);
		if ($isConnect) {
			$data = $memcached->get(self::BILLING_NAMESPACE);
			if (!$data) {
				$data = array();
			}
			$data[$user_id] = time();
			$memcached->set(self::BILLING_NAMESPACE, $data, time() + self::EXPIRATION);
			$memcached->quit();
		}
	}

	public static function restoreBillingUsers() {
		$memcached = new Memcached();
		$isConnect = $memcached->addServer('localhost', 11211);
		$result = array();
		if ($isConnect) {
			$users = $memcached->get(self::BILLING_NAMESPACE);
			if ($users) {
				$new_data = array();
				if (count($users)) {
					$current_time = time();
					foreach ($users as $user_id => $time) {
						if ($time < $current_time && $current_time - $time >= self::MAX_DIFF_SECONDS + self::MAX_DIFF_SECONDS/2) {
							$result[$user_id] = $time;
						} else {
							$new_data[$user_id] = $time;
						}
					}
				}
				$memcached->set(self::BILLING_NAMESPACE, $new_data, time() + self::EXPIRATION);
			}
			$memcached->quit();
		}
		return $result;
	}
}
