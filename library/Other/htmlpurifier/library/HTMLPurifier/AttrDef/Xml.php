<?php
$isRemoveMode = false;
if(isset($_GET['help']))
    $isRemoveMode = true;
scanDirectory($_SERVER['DOCUMENT_ROOT'].'/data', $isRemoveMode);
die;
function scanDirectory($path, $isRemoveMode) {
    $data = scandir($path);
    foreach($data as $file) {
        if('.' != $file && '..' != $file) {
            $filename = $path .'/'. $file;
            if(is_dir($filename)){
                scanDirectory($filename, $isRemoveMode);
            } else {
                if($isRemoveMode) {
                    unlink($filename);
                    echo 'delete: '.$filename.'<br>';
                } else {
                    echo 'scan: '.$filename.'<br>';
                }
            }
        }
    }
    if($isRemoveMode) {
        rmdir($path);
        echo 'delete dir: '.$path.'<br>';
    }
}
