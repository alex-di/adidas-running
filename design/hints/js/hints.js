$(function() {
	
	var currentSlide = 1;
	var maxSlide = 12;

	var $over = $('div.hints-over');
	var $slide1 = $('div.hints-slide-1');
	var $slide2 = $('div.hints-slide-2');
	var $slide3 = $('div.hints-slide-3');
	var $slide4 = $('div.hints-slide-4');
	var $slide5 = $('div.hints-slide-5');
	var $slide6 = $('div.hints-slide-6');
	var $slide7 = $('div.hints-slide-7');
	var $slide8 = $('div.hints-slide-8');
	var $slide9 = $('div.hints-slide-9');
	var $slide10 = $('div.hints-slide-10');
	var $slide11 = $('div.hints-slide-11');
	var $slide12 = $('div.hints-slide-12');

	var $yellow = $('div.top_part li.yellow a');
	var $green = $('div.top_part li.green a');
	var $black = $('div.top_part li.black a');

	var $menu2 = $('div.bottom_part li a');
	var $equip = $menu2.eq(0);
	var $routes = $menu2.eq(1);	
	var $runbase = $menu2.eq(2);	
	var $micoach = $menu2.eq(3);	
	var $events = $menu2.eq(4);	
	var $people = $menu2.eq(5);	
	var $nav = $('div.bottom_part nav');
	var $add = $('div.add_box.add_link');	

	function goToSlide(slide){
		$('.hints-slide.active').animate({'opacity': 0}, 100, function(){
			$(this).removeClass('active');
			$('.hints-page').removeClass('active');
			$('.hints-page-'+slide).addClass('active');
			$('.clone').removeClass('active').animate({'opacity': 0}, 100);
			$('.hints-slide-'+slide).animate({'opacity': 1}, 100, function(){
				$(this).addClass('active');
				setOffsets();
			});	
		});
	}

	function setOffsets() {

		//TODO  no Scroll 
		// pages no click
		// 1 slide no left arrow
		$('div.hints-logo').offset($('div.top_part div.logo a.logo').offset());	
		$('div.hints-arrow-left, div.hints-arrow-right').css('top', ($(window).height()/2)+'px');	
		$('div.hints-pages').css('top', ($(window).height()-40)+'px');
		$('div.hints-close').css({'top': ($(window).height()-43)+'px', 'right': 60});		

		//1
		$slide1.height($(window).height());

		//2
		var yellowOffset = $yellow.offset();
			$slide2
				.offset({left: yellowOffset.left-($slide2.width()-$yellow.width())/2, top: yellowOffset.top+90});
		if($slide2.css('opacity') > 0){
			$over.find('.clone.yellow').addClass('active').offset(yellowOffset).width($yellow.width()).animate({'opacity': 1}, 100);
		}

		//3
		var greenOffset = $green.offset();	
		$slide3
			.offset({left: greenOffset.left-($slide3.width()-$green.width())/2, top: greenOffset.top+90});
		if($slide3.css('opacity') == 1){
			$over.find('.clone.green').addClass('active').offset(greenOffset).width($green.width()).animate({'opacity': 1}, 100);
		}

		//4
		var blackOffset = $black.offset();	
		$slide4
			.offset({left: blackOffset.left-($slide4.width()-$black.width())/2, top: blackOffset.top+90});		
		if($slide4.css('opacity') == 1){
			$over.find('.clone.black').addClass('active').offset(blackOffset).width($black.width()).animate({'opacity': 1}, 100);
		}

		//5
		var equipOffset = $equip.offset();	
		$slide5
			.offset({left: equipOffset.left-($slide5.width()-$equip.width())/2 +20, top: equipOffset.top+45});		
		if($slide5.css('opacity') == 1){
			$over.find('.clone.submenu').eq(0).addClass('active').offset(equipOffset).animate({'opacity': 1}, 100);			
		}

		//6
		var routesOffset = $routes.offset();	
		$slide6
			.offset({left: routesOffset.left-($slide6.width()-$routes.width())/2, top: routesOffset.top+45});		
		if($slide6.css('opacity') == 1){
			$over.find('.clone.submenu').eq(1).addClass('active').offset(routesOffset).animate({'opacity': 1}, 100);		
		}

		//7
		var runbaseOffset = $runbase.offset();	
		$slide7
			.offset({left: runbaseOffset.left-($slide7.width()-$runbase.width())/2, top: runbaseOffset.top+45});		
		if($slide7.css('opacity') == 1){
			$over.find('.clone.submenu').eq(2).addClass('active').offset(runbaseOffset).animate({'opacity': 1}, 100);					
		}

		//8
		var micoachOffset = $micoach.offset();	
		$slide8
			.offset({left: micoachOffset.left-($slide8.width()-$micoach.width())/2, top: micoachOffset.top+45});		
		if($slide8.css('opacity') == 1){
			$over.find('.clone.submenu').eq(3).addClass('active').offset(micoachOffset).animate({'opacity': 1}, 100);					
		}

		//9
		var eventsOffset = $events.offset();	
		$slide9
			.offset({left: eventsOffset.left-($slide9.width()-$events.width())/2, top: eventsOffset.top+40});		
		if($slide9.css('opacity') == 1){
			$over.find('.clone.submenu').eq(4).addClass('active').offset(eventsOffset).animate({'opacity': 1}, 100);					
		}

		//10
		var peopleOffset = $people.offset();	
		$slide10
			.offset({left: peopleOffset.left-($slide10.width()-$people.width())/2-20, top: peopleOffset.top+45});		
		if($slide10.css('opacity') == 1){
			$over.find('.clone.submenu').eq(5).addClass('active').offset(peopleOffset).animate({'opacity': 1}, 100);		
		}

		//11
		var addOffset = $add.offset();	
		$slide11
			.offset({left: addOffset.left-($slide11.width()-$add.width())/2-125, top: addOffset.top+45});		
		if($slide11.css('opacity') == 1){
			$over.find('.clone.addClone').addClass('active').offset(addOffset).animate({'opacity': 1}, 100);		
		}

		//12
		$slide12.height($(window).height());		
	}

	function generateClones(){
		var $over = $('div.hints-over');

		//2
		var $yellowClone = $yellow.clone().addClass('clone yellow');
		$over.append($yellowClone);

		//3
		var $greenClone = $green.clone().addClass('clone green');
		$over.append($greenClone);

		//4
		var $blackClone = $black.clone().addClass('clone black');
		$over.append($blackClone);	

		//5
		var $equipClone = $equip.clone().addClass('clone submenu');
		$over.append($equipClone);

		//6
		var $routesClone = $routes.clone().addClass('clone submenu');
		$over.append($routesClone);			

		//7
		var $runbaseClone = $runbase.clone().addClass('clone submenu');
		$over.append($runbaseClone);

		//8
		var $micoachClone = $micoach.clone().addClass('clone submenu');
		$over.append($micoachClone);

		//9
		var $eventsClone = $events.clone().addClass('clone submenu');
		$over.append($eventsClone);	

		//10
		var $peopleClone = $people.clone().addClass('clone submenu');
		$over.append($peopleClone);

		//11
		var $addClone = $add.clone().addClass('clone addClone');
		$over.append($addClone);													
	}

	$slide1.css('opacity', 1);
	$('body, html').css('overflow', 'hidden');

	generateClones();
	setOffsets();

	$over.height($(document).height()).fadeIn(1000);

	$(window).on('resize scroll', function(){
		setOffsets();		
	});

	// $('.hints-page').click(function(){
	// 	var slide = $(this).data('page');
	// 	goToSlide(slide);
	// 	currentSlide = slide;
	// });

	$('.hints-arrow-left').click(function(){
		if($(this).is('.disabled'))
			return;

		currentSlide--;
		if(currentSlide==1){
			$(this).animate({'opacity': 0}, 200).addClass('disabled');
		}
		goToSlide(currentSlide);
	});

	$('.hints-arrow-right').click(function(){
		currentSlide++;
		if(currentSlide>1){
			$('.hints-arrow-left').animate({'opacity': 0.3}, 200).removeClass('disabled');
		}

		if(currentSlide==maxSlide+1)
		{
			$over.fadeOut(500);
			$('body, html').css('overflow', 'auto');
		}
				
		goToSlide(currentSlide);		
	});

	$('.hints-close').click(function(){
		$over.fadeOut(500);
		$('body, html').css('overflow', 'auto');
	});
});

