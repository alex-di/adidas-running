(function($) {

    var dialogsArrayUI = [];
    var curent_id;
    $.showDialog = function(content, clazz, title, height, width) {
        var time = new Date();
        curent_id = time.getTime();
        dialogsArrayUI.push(curent_id);
        $("body").append('<div id="mask_'+curent_id+'" class="mask" style="z-index:'+curent_id+'"></div>');
        $("body").append('<div id="dialog_container_'+curent_id+'" class="'+clazz+'">'+
	                           '<span class="window_title">'+title+
                                    '<div class="close_button"></div>'+
                                '</span>'+
                                content+
                            '</div>');
        $("#dialog_container_"+curent_id).dialog({
 			height:height,
			width:width,
			title: "",
            zIndex:curent_id+1,
            resizable: false,
			dialogClass: "OptionDialog",
            create:function(event, ui) {
                //$(ui).wrap("<div class='sheme'/>");
                $(".sheme").append(ui);
            },
            open:function(){
                        //$("body").addClass("lock");
                		$('#mask_'+curent_id).css({'width':$(window).width(), 'height':$(document).height()}).fadeTo("slow",0.6);
                        $("#dialog_container_"+curent_id+" div.close_button").click(function(){
                            $.closeDialog();
                        });
            
            }
        });
    }
    $.closeDialog = function() {
        var curent_id = dialogsArrayUI.pop();
        if(curent_id != undefined) {
            console.log(curent_id);
            $("#dialog_container_"+curent_id).dialog("destroy");
            $("#dialog_container_"+curent_id+", #mask_"+curent_id).remove();
            
            if(dialogsArrayUI.length == 0) {
                $("body").removeClass("lock");
            }
            /*if(dialogsArrayUI.length != 0) {
                var prev_dialog = dialogsArrayUI[dialogsArrayUI.length-1];
                $("#dialog_container_"+prev_dialog).dialog("open");
            }*/
        }
    }
    $.getCurentDialogId = function() {
        return "dialog_container_"+curent_id;
    }
    
    $.showError = function(message) {
        alert(message);
    }
    $.showInfo = function(message, location) {
        var redirect = '';
        if(location) {
            redirect = 'window.location=\''+location+'\';';
        }
        $('body > .shadow').remove();
        $('body').append('<div class="shadow" onclick="$(this).remove();'+redirect+'"><div>'+message+'</div></div>');
    }
    
    $.confirm = function(options, callback) {
        var option = {message: '', okButton: 'удалить', canceledButton: 'отменить'};
        if('string' == typeof options) {
            option.message = options;
        } else {
            for(var i in option) 
                if(undefined != options[i])
                    option[i] = options[i];
            
        }
        var html = '<div class="modal" id="del_dialog">'+
            '<div class="title">'+option.message+'</div>'+
            '<div class="del">'+option.okButton+'</div>'+
            '<div class="close" onclick="$.modal().close()"><span></span>'+option.canceledButton+'</div>'+
        '</div>';
        $('body').append(html);
        $('#del_dialog > .del').click(function(){
            callback();
            $.modal().close();
        });
        $('#del_dialog').modal().open({isReposition: true, onClose: function(){
                                            $('#del_dialog').remove();
                                        }});
    }
    
})(jQuery);

