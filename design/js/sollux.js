var isDebug = false;
function jphp(destanation, element)
{
    if (typeof SpawEngine == 'undefined')
    {
        
    } 
    else
    {
        SpawEngine.updateFields();    
    }
    $.php(destanation, $(element).serialize());
    
    return false;
}
if(!isDebug) {
    php.error = function(xmlEr, typeEr, except) {
        console.log(xmlEr.responseText);
    }
}

function saveForm(url, form) {
    if (typeof SpawEngine != 'undefined') {
        SpawEngine.updateFields();
    }
    
    if (typeof tinyMCE != 'undefined') {
        $.each($('.tiny_block_editor'), function(index, value) {
            text = tinyMCE.get($(value).attr('id')).getContent()
            $("#" + $(value).attr('id')).val(text);
        });     
    }
    $.php(url, $(form).serialize());
    return false;
}


function post(action, object, callback, dataType) {
    var dataType = dataType || 'html';
    var callback = callback || function(data){};
    $.post(action, object, function(data){
        if("PERMISSION_DENIED" == $.trim(data)) {
            //$.showError(NOT_AUTH_FUNCTION);
        } else {
            callback(data);
        }
    }, dataType).complete(function(data) {
        if("PERMISSION_DENIED" == data.responseText) {
            //$.showError(NOT_AUTH_FUNCTION);
        }

    });
}
function setLocale(locale) {
    post("/languages/index/processetlanguages", {id: locale}, function(){
        window.location.reload();
    })
}
if (typeof console === "undefined" || typeof console.log === "undefined") {
     console = {log: function() {}};
}