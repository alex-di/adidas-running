var Materials = {
                    gallery_upload_settings: 
                                    {
                                        max_file_size: 0, 
                                        filters: {'title': 'Files', 'extensions': ''}, 
                                        url:'',
                                        flash_swf_url: '/design/modules/plupload/js/plupload.flash.swf',
                                        runtimes: 'gears,html5,flash,silverlight,browserplus',
                                        multipart_params: {PHPSESSID: ''},
                                        multi_selection: false,
                                        error: 'onErrorUpload',
                                        uploadProgress: 'progressBarPhotoElement',
                                        fileUploaded: 'addPhotoElement',
                                        filesAdded: 'startPhotoElementUpload',
                                    },
                };

$.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['ru']);
$(function(){
   
   initMove('outer_button', 'drag_box_element'); 
   initMove('button_box', 'drag_box_photo');
   repositionMenuForm();
    $(window).scroll(function(){
        repositionMenuForm();
    });

});
function changeTextareaContent(textarea) {
   /*var currentHeight = $(textarea).height();
   var MIN_RESIZE_HEIGHT = 84;
   var MAX_RESIZE_HEIGHT = 320;
   var scrollHeight = $(textarea)[0].scrollHeight;
   if($.trim($(textarea).val()) == '') {
        $(textarea).height(MIN_RESIZE_HEIGHT+'px');     
   } else if(scrollHeight > MAX_RESIZE_HEIGHT) {
        $(textarea).height(MAX_RESIZE_HEIGHT+'px'); 
   } else if(scrollHeight > currentHeight) {
        $(textarea).height(scrollHeight + 20); 
   }*/
}
function changeEditorContent(editor) {

   var currentHeight = $(editor).innerHeight();
   var MIN_RESIZE_HEIGHT = 120;
   var MAX_RESIZE_HEIGHT = 320;
   var scrollHeight = $(editor)[0].scrollHeight;
   if($.trim($(editor).text()) == '') {
        $(editor).height(MIN_RESIZE_HEIGHT+'px').css('overflow-y', 'hidden');     
   } else if(scrollHeight > MAX_RESIZE_HEIGHT) {
        $(editor).height(MAX_RESIZE_HEIGHT+'px').css('overflow-y', 'auto'); 
   } else if(scrollHeight > currentHeight) {
        $(editor).height(scrollHeight); 
   }
}
function repositionMenuForm() {
    var ui = $('.grey_sidebar');
    var topLimit = $('.grey_box').offset().top;
    var bottomLimit = $('.grey_box.with_arrow').last().offset().top + $('.grey_box.with_arrow').last().height() - ui.height();
    
    var percent = $(window).scrollTop() / ($(document).height() - $(window).height());
    var TOP = $(window).height() - ui.height();
    TOP = TOP < 0 ? 0 : TOP / 2;
    var position = percent * ($(document).height() - $(window).height()) + TOP;
    if(position < topLimit)
        position = topLimit;
    if(position > bottomLimit)
        position = bottomLimit;
    ui.offset({top: position});
}
function initMove(classNameButton, parentClassName) {
    classNameButton = '.'+classNameButton;
    for(var i = 0; i < $('.'+parentClassName).length; i++) {
        $('.'+parentClassName).eq(i).find(classNameButton+' .round_button.first,'+classNameButton+' .round_button.prev,'+classNameButton+' .round_button.next,'+classNameButton+' .round_button.last').removeClass('disabled');
        $('.'+parentClassName).eq(i).find(classNameButton+' .round_button.first').first().addClass('disabled');
        $('.'+parentClassName).eq(i).find(classNameButton+' .round_button.prev').first().addClass('disabled');
        $('.'+parentClassName).eq(i).find(classNameButton+' .round_button.next').last().addClass('disabled');
        $('.'+parentClassName).eq(i).find(classNameButton+' .round_button.last').last().addClass('disabled');
    }

}
function move(button, direction, parentClassName) {
    if(!$(button).hasClass('disabled')) {
        var firstBox = $(button).parents('.drop_element').eq(0);
    
        if('first' == direction || 'last' == direction) {
            var clone = firstBox.clone();
            var parent = firstBox.parent();
            firstBox.remove();
            if('first' == direction) parent.prepend(clone);
            else parent.append(clone);
        } else {
            var twoBox = firstBox.prev();
            if('down' == direction)
                twoBox = firstBox.next();
            firstBox.after(twoBox.clone());
            twoBox.after(firstBox.clone());
            firstBox.remove();
            twoBox.remove();
        }
        initMove($(button).parent().attr('class'), parentClassName); 
    }
}
function delteDragElement(button) {
    $(button).parents('.drop_element').eq(0).remove();
    initMove('outer_button', 'drag_box_element'); 
    initMove('button_box', 'drag_box_photo');
}

function addText(){
    var template = getTextTemplate();
    $('#drop_element_box').append(template.html);
    $('#'+template.editor).wysiwyg({toolbarSelector: '#toolbar_for_'+template.editor, onChangeContent: changeEditorContent});
    initMove('outer_button', 'drag_box_element');
    $(template.focusSelector).focus();
}
///////////////////////////////////////////////////////
function addPhoto(){
    var template = getPhotoTemplate();
    $('#drop_element_box').append(template.html);
    var settings = Materials.gallery_upload_settings;
    settings.browse_button = template.gallery;
    initUploader(settings);
    initMove('outer_button', 'drag_box_element');
    $('html, body').animate({scrollTop: $(template.focusSelector).offset().top + 'px'}, 'fast');
}

function initUploader(settings) {
    var uploader = {};
    Uploader.init(uploader, settings)
}
function deletePhotoElement(button) {
    var l = $(button).parents('.drag_box').eq(0).find('.drop_element').length,
        btn = $(button).parents('.drag_box').eq(0).parent().find('.add_button');
    if (l < 2) {
        $(btn).text('добавить фото в галерею');
    } else if (l <= 10) {
        $(btn).show();
    }
    delteDragElement(button);
}
var countFilesInQueue = 0;

function addPhotoElement(up, file, response) {
    var jsa = $.parseJSON(response.response),
        photoBox = $('#'+up.settings.browse_button).parents('.drop_element');
        currentUploadedLength = photoBox.find('.drop_element').length;

    console.log('Count: ' + countFilesInQueue + '; Uploaded length: ' + currentUploadedLength);

    if ((countFilesInQueue + currentUploadedLength) > 10) {
        var diff = (countFilesInQueue + currentUploadedLength) - 10;
        countFilesInQueue = countFilesInQueue - diff;
    }

    if (countFilesInQueue == 0) {
        photoBox.find('.add_button').hide();
        $.showInfo('Нельзя загружать больше 10 файлов в галерею');
        return false;
    } else if (countFilesInQueue < 0) {
        return false;
    }

    countFilesInQueue--;
    
    if ('error' != jsa.status && photoBox.find('.drop_element').length < 10) {
        var namePostfix = new Date().getTime();
        photoBox.find('.drag_box').append('<div class="drop_element" data-name-postfix="'+namePostfix+'">'+
                                            '<div class="gallery_img_box">'+
                                                '<img src="'+jsa.preview+'" alt="img" />'+
                                                '<div class="button_box">'+
                                                    '<div class="round_button delete" onclick="deletePhotoElement(this)"></div>'+
                                                    '<div class="round_button prev" onclick="move(this, \'up\', \'drag_box_photo\')"></div>'+
                                                    '<div class="round_button next" onclick="move(this, \'down\', \'drag_box_photo\')"></div>'+
                                                '</div>'+
                                            '</div>'+
                                            '<input type="hidden" name="photo'+namePostfix+'" value="'+jsa.preview+'"/>'+
                                            '<input type="text" name="description'+namePostfix+'" class="yellow_input delimiter" placeholder="описание фотографии" value="'+jsa.name+'"/>'+
                                        '</div>');
        photoBox.find('.add_button').text('добавить еще фото в галерею');
        initMove('button_box', 'drag_box_photo');
    } else {
        if (photoBox.find('.drop_element').length >= 10) {
            $.showInfo('Нельзя загружать больше 10 файлов в галерею');
        } else {
            $.showInfo(jsa.message);
        }
    }
    if (countFilesInQueue < 1)
        photoBox.find('.load_process').addClass('disabled');

    if (photoBox.find('.drop_element').length >= 10) {
        photoBox.find('.add_button').hide();
    } else {
        photoBox.find('.add_button').show();
    }
}
function progressBarPhotoElement(up, file) {
    var ui = $('#'+up.settings.browse_button).parents('.drop_element').find('.load_process');
    ui.find('.label').text('Загрузка фото '+file.percent+'%');
    ui.find('.progress').css('width', file.percent+'%');
}
function startPhotoElementUpload(up, file) {
    var ui = $('#'+up.settings.browse_button).parents('.drop_element').find('.load_process');
    ui.find('.label').text('Загрузка фото 0%');
    ui.find('.progress').css('width', '0%');
    ui.removeClass('disabled');
    
    countFilesInQueue = file.length;
}
function onErrorUpload(up, error) {
    if(-600 == error.code) {
        $.showInfo("Недопустимый размер файла. Максимально допустимый размер файла " + formatSize(up.settings.max_file_size));
    } else {
        $.showInfo(error.message);
    }
    countFilesInQueue--;
    if(countFilesInQueue < 1)
        $('#'+up.settings.browse_button).parents('.drop_element').find('.load_process').addClass('disabled');
}
///////////////////////////////////////////////////////
function addVideo(){
    var template = getVideoTemplate();
    $('#drop_element_box').append(template.html);
    initMove('outer_button', 'drag_box_element');
    $(template.focusSelector).focus();
}
function validateVideo(input){
    input = $(input);
    var value = input.val();
    if(!((value.indexOf("<iframe") > -1 && value.indexOf("vk.com/video_ext.php") > -1) || value.indexOf("youtube.com/watch?v=") > -1)) {
        input.val('');
        $.showInfo("Неверный код для вставки. Допустимы ссылки только на VK и Youtube.");
    }
}
///////////////////////////////////////////////////////
function addPlaylist(){
    var template = getAudioTemplate();
    $('#drop_element_box').append(template.html);
    initMove('outer_button', 'drag_box_element');
    $(template.focusSelector).focus();
}
function addAudioElement(button) {
    var namePostfix = new Date().getTime();
    var audio = $(button).prev().val();
    if('' != audio) {
        if((audio.indexOf("<iframe") > -1 && audio.indexOf("soundcloud.com/player/?url=") > -1) || audio.indexOf("youtube.com/watch?v=") > -1) {
            var audioNameBlock = '';
            if(audio.indexOf("youtube.com/watch?v=") > -1) {
                var link = audio.split("watch?v=");
                link = link[1].split('&');
                audio = getYoutubeAudioTemplate(link[0]);
                audioNameBlock = '<input name="audioname'+namePostfix+'" type="text" class="yellow_input" placeholder="название аудио"/><br/><br/>';
            }
            $(button).prev().val('');
            $(button).parent().prev().append('<div class="audio_point drop_element" data-name-postfix="'+namePostfix+'">'+
                        audioNameBlock+
                        audio+
                        '<br/><br/><br/>'+
                        '<div class="delete_button" onclick="deleteAudioElement(this)"><span></span>удалить</div>'+
                        '<input type="hidden" name="audio'+namePostfix+'" value=""/>'+
                    '</div>');
            $('input[name="audio'+namePostfix+'"]').val(audio);
            $(button).parents('.drop_element').find('.playlist_name').removeClass('disabled');
        } else {
            $.showInfo("Неверный код для вставки. Допустимы ссылки только на Youtube и Soundcloud. Чтобы получить ссылку на аудио Soundcloud нажмите Share и скопируйте код из поля Widget Code");
        }
        //<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F21199445&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true"></iframe>
        //<iframe src="http://vk.com/video_ext.php?oid=814672&id=165775676&hash=97d333f0ca9b2d11&hd=1" width="607" height="360" frameborder="0"></iframe>
        //<iframe width="560" height="315" src="//www.youtube.com/embed/6TTx65khfmI" frameborder="0" allowfullscreen></iframe>

    }
}
function deleteAudioElement(button) {
    if($(button).parent().parent().find('.drop_element').length < 2)
        $(button).parent().parents('.drop_element').find('.playlist_name').addClass('disabled');
    delteDragElement(button);
}
///////////////////////////////////////////////////////
function searializeDropElement() {
    var result = [];
    for(var i = 0; i < $('#drop_element_box > .drop_element').length; i++) {
        var dropElement = $('#drop_element_box > .drop_element').eq(i);
        var type = dropElement.data('type');
        var data = {};
        data[type] = {};
        var namePostfix = dropElement.data('name-postfix');
        if('text' == type) {
            var editorValue = dropElement.find('.editor_area').html();
            if('' != $.trim(editorValue))
                data[type][namePostfix] = {editor: dropElement.find('.editor_area').html()};
        } else if('video' == type) {
            var link = $('input[name="link'+namePostfix+'"]').val();
            if('' != $.trim(link)) {
               data[type][namePostfix] = {name: $('input[name="name'+namePostfix+'"]').val(), 
                                        link: link, 
                                        description: $('textarea[name="description'+namePostfix+'"]').val()}; 
            }
            
        } else if('audio' == type) {
            var audioElement = dropElement.find('.drop_element');
            var list = [];
            for(var j = 0; j < audioElement.length; j++) {
                var postfix = audioElement.eq(j).data('name-postfix');
                var temp = {};
                temp[postfix] = {'audio': $('input[name="audio'+postfix+'"]').val()};
                if($('input[name="audioname'+postfix+'"]').val()) 
                    temp[postfix]['audioname'] = $('input[name="audioname'+postfix+'"]').val();
                list.push(temp);
            }
            data[type][namePostfix] = {playlist: $('input[name="playlist'+namePostfix+'"]').val(), list: list};
        } else if('photo' == type) {
            var photoElement = dropElement.find('.drop_element');
            var list = [];
            for(var j = 0; j < photoElement.length; j++) {
                var postfix = photoElement.eq(j).data('name-postfix');
                var temp = {};
                temp[postfix] = {'photo': $('input[name="photo'+postfix+'"]').val(), 'description': $('input[name="description'+postfix+'"]').val()};
                list.push(temp);
            }
            data[type][namePostfix] = {list: list};
        }
        result.push(data);
    }
    return result;
}
///////////////////////////////////////////////////////
function getYoutubeAudioTemplate(link){
    return  '<object width="100%" height="25"> <param name="movie" value="https://www.youtube.com/v/'+link+'?version=2&hl=ru_RU&rel=0">'+
                '<param name="allowFullScreen" value="true">'+
                '<param name="allowscriptaccess" value="always">'+
                '<embed width="100%" height="25" src="https://www.youtube.com/v/'+link+'?version=2&hl=ru_RU&rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true">'+
            '</object>';
}
function getTextTemplate(){
    var namePostfix = new Date().getTime();
    var editorId = 'editor'+namePostfix;
    var data = {editor: editorId};
    data.html = 
    '<div class="grey_box with_arrow drop_element" data-type="text" data-name-postfix="'+namePostfix+'">'+
            '<div class="grey_box_title">текст</div>'+
            '<div class="outer_button">'+
                '<div class="round_button first" onclick="move(this, \'first\', \'drag_box_element\')"></div>'+
                '<div class="round_button prev" onclick="move(this, \'up\', \'drag_box_element\')"></div>'+
                '<div class="label">переместить</div>'+
                '<div class="round_button next" onclick="move(this, \'down\', \'drag_box_element\')"></div>'+
                '<div class="round_button last" onclick="move(this, \'last\', \'drag_box_element\')"></div>'+
                '<div class="round_button delete" onclick="delteDragElement(this)"></div>'+
                '<div class="label red" onclick="delteDragElement(this)">удалить текст</div>'+
            '</div>'+
            '<div class="text_area">'+
                '<div id="toolbar_for_'+editorId+'" class="top_panel">'+
                    '<div class="ico bold editor_button" data-edit="bold">Полужирный</div>'+
                    '<div class="ico italic editor_button" data-edit="italic">Курсив</div>'+
                    '<div class="ico num_list editor_button" data-edit="insertorderedlist"><span></span></div>'+
                    '<div class="ico mark_list editor_button" data-edit="insertunorderedlist"><span></span></div>'+
                    '<div class="ico letter editor_button" data-edit="formatblock h2"><span></span></div>'+
                '</div>'+
                '<div id="'+editorId+'" class="editor_area"></div>'+
            '</div>'+
            '<div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить текст</div>'+
        '</div>';
     data.focusSelector = '#'+editorId;
     return data;
}
function getPhotoTemplate(){
    var namePostfix = new Date().getTime();
    var gallery = 'gallery_'+new Date().getTime();
    var data = {gallery: gallery};
    
    data.html =     
        '<div class="grey_box with_arrow drop_element" data-type="photo" data-name-postfix="'+namePostfix+'">'+
            '<div class="grey_box_title">фото</div>'+
            '<div class="outer_button">'+
                '<div class="round_button first" onclick="move(this, \'first\', \'drag_box_element\')"></div>'+
                '<div class="round_button prev" onclick="move(this, \'up\', \'drag_box_element\')"></div>'+
                '<div class="label">переместить</div>'+
                '<div class="round_button next" onclick="move(this, \'down\', \'drag_box_element\')"></div>'+
                '<div class="round_button last" onclick="move(this, \'last\', \'drag_box_element\')"></div>'+
                '<div class="round_button delete" onclick="delteDragElement(this)"></div>'+
                '<div class="label red" onclick="delteDragElement(this)">удалить галерею</div>'+
            '</div>'+
            '<div class="drag_box drag_box_photo"></div>'+
            '<div class="load_process disabled">'+
                '<div class="label">Загрузка фото 67%</div>'+
                '<div class="container">'+
                    '<div class="progress" style="width: 67%;"></div>'+
                '</div>'+
            '</div>'+
            '<div id="'+gallery+'" class="add_button img uploader"><span></span>добавить фото в галерею</div>'+
            '<div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить галерею</div>'+
        '</div>';
    data.focusSelector = '#'+gallery;
    return data;
}
function getVideoTemplate(){
    var namePostfix = new Date().getTime();
    var data = {};
    data.html =     
        '<div class="grey_box with_arrow drop_element" data-type="video" data-name-postfix="'+namePostfix+'">'+
            '<div class="grey_box_title">видео</div>'+
            '<div class="outer_button">'+
                '<div class="round_button first" onclick="move(this, \'first\', \'drag_box_element\')"></div>'+
                '<div class="round_button prev" onclick="move(this, \'up\', \'drag_box_element\')"></div>'+
                '<div class="label">переместить</div>'+
                '<div class="round_button next" onclick="move(this, \'down\', \'drag_box_element\')"></div>'+
                '<div class="round_button last" onclick="move(this, \'last\', \'drag_box_element\')"></div>'+
                '<div class="round_button delete" onclick="delteDragElement(this)"></div>'+
                '<div class="label red" onclick="delteDragElement(this)">удалить видео</div>'+
            '</div>'+
            '<input name="name'+namePostfix+'" type="text" class="yellow_input" placeholder="название видео" />'+
            '<input onchange="validateVideo(this)" name="link'+namePostfix+'" type="text" class="yellow_input delimiter" placeholder="ссылка на видео" />'+
            '<textarea onkeypress="changeTextareaContent(this)" name="description'+namePostfix+'" placeholder="описание" class="yellow_textarea"></textarea>'+
            '<div class="delete_button" onclick="delteDragElement(this)"><span></span>удалить видео</div>'+
        '</div>';
    data.focusSelector = '[name="name'+namePostfix+'"]';
    return data;
}
function getAudioTemplate(){
    var namePostfix = new Date().getTime();
    var data = {};
    data.html =     
        '<div class="grey_box with_arrow drop_element" data-type="audio" data-name-postfix="'+namePostfix+'">'+
            '<div class="grey_box_title">аудио</div>'+
            '<div class="outer_button">'+
                '<div class="round_button first" onclick="move(this, \'first\', \'drag_box_element\')"></div>'+
                '<div class="round_button prev" onclick="move(this, \'up\', \'drag_box_element\')"></div>'+
                '<div class="label">переместить</div>'+
                '<div class="round_button next" onclick="move(this, \'down\', \'drag_box_element\')"></div>'+
                '<div class="round_button last" onclick="move(this, \'last\', \'drag_box_element\')"></div>'+
                '<div class="round_button delete" onclick="delteDragElement(this)"></div>'+
                '<div class="label red" onclick="delteDragElement(this)">удалить аудио</div>'+
            '</div>'+
            '<input type="text" class="yellow_input disabled playlist_name" placeholder="название плэйлиста" name="playlist'+namePostfix+'"/>'+
            '<div class="drag_box"></div>'+
            '<div class="add_link">'+
                '<input id="audio'+namePostfix+'" type="text" class="yellow_input" placeholder="ссылка на аудио" />'+
                '<div class="add_button" onclick="addAudioElement(this)"><span></span>добавить</div>'+
                '<div style="clear: both;"></div>'+
            '</div>'+
        '</div>';
    data.focusSelector = '#audio'+namePostfix;
    return data;
}
