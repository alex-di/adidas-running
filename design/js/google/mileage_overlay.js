function MileageOverlay(mileage, marker, map, isVisible) {
  this.marker_ = marker;
  this.map_ = map;
  this.mileage_ = mileage;
  this.div_ = null;
  this.isVisible = isVisible || false;
  this.setMap(map);
}
MileageOverlay.prototype = new google.maps.OverlayView();
MileageOverlay.prototype.onAdd = function() {

	  var div = document.createElement('div');
	  div.style.position = "absolute";
	  div.style.width = "20px";
	  div.style.height = "20px";
      div.style.zIndex = 50;
	  if(!this.isVisible)
	  div.style.visibility = "hidden";
	  
	  div.style.textAlign = "center";

	  var controlText = document.createElement('div');
	  controlText.style.fontSize = "47px";
	  controlText.style.fontFamily = 'adineuebold, Arial, Helvetica, sans-serif';
	  
	  //controlText.style.fontWeight = "bold";
	  controlText.style.color = "#000000";
	  controlText.innerHTML = this.mileage_;
	  div.appendChild(controlText);	  
	  //div.innerHtml = '<div>'+this.mileage_+'</div>';
	  //console.log('<div>'+this.mileage_+'</div>');
	  // Set the overlay's div_ property to this DIV
	  this.div_ = div;
	  // We add an overlay to a map via one of the map's panes.
	  // We'll add this overlay to the overlayImage pane.
	  var panes = this.getPanes();
	  panes.overlayLayer.appendChild(div);
}
MileageOverlay.prototype.draw = function() {

	  // Size and position the overlay. We use a southwest and northeast
	  // position of the overlay to peg it to the correct position and size.
	  // We need to retrieve the projection from this overlay to do this.
	  var overlayProjection = this.getProjection();

	  // Retrieve the southwest and northeast coordinates of this overlay
	  // in latlngs and convert them to pixels coordinates.
	  // We'll use these coordinates to resize the DIV.
	  var position = overlayProjection.fromLatLngToDivPixel(this.marker_.getPosition());

	  // Resize the image's DIV to fit the indicated dimensions.
	  //console.log(position);
	  var div = this.div_;
	  div.style.left = (position.x+24) + 'px';
	  div.style.top = (position.y-49) + 'px';
	  //console.log(div);
}
MileageOverlay.prototype.onRemove = function() {
	  this.div_.parentNode.removeChild(this.div_);
	  this.div_ = null;
}
MileageOverlay.prototype.hide = function() {
	if (this.div_) {
		this.div_.style.visibility = "hidden";
	}
}
MileageOverlay.prototype.show = function() {
	if (this.div_) {
		this.div_.style.visibility = "visible";
	}
}