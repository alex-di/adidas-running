var STYLES_MAP = [ { "featureType": "water", "stylers": [ { "color": /*"#dbeefc"*/"#cfcec8" } ] },
                   { "featureType": "road", "stylers": [ { "color": /*"#cecdc8"*/"#f1f1f0" } ] },
                   { "featureType": "transit.station.rail", "elementType": "labels.icon", "stylers": [ { "color": "#6c695b" }, { "invert_lightness": true } ] },
                   { "featureType": "landscape.natural", "stylers": [ { "color": /*"#f2fce4"*/"#ffffff" } ] },
                   { "featureType": "landscape.man_made", "stylers": [ { "color": "#fafaf9" } ] },
                   { "featureType": "poi", "stylers": [ { "color": "#f1f1f0" } ] },
                   { "featureType": "administrative", "elementType": "labels", "stylers": [ { "color": "#2e2a16" }, { "visibility": "on" }, { "saturation": 2 }, { "weight": 0.1 } ] },
                   { "featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [ { "color": "#2e2a16" } ] } ];
var CLUSTER_STYLES = [{url: '/design/images/map_img/mark.png', height: 45, width: 45, textSize: 16, textColor: '#000000'},{url: '/design/images/map_img/mark.png', height: 45, width: 45, textSize: 16, textColor: '#000000'},{url: '/design/images/map_img/mark.png', height: 45, width: 45, textSize: 16, textColor: '#000000'}];
var MAX_ZOOM = 25;
//runbase {"jb":55.72140018145943,"kb":37.548933159559965}
function mapZoomIn(map){
    var zoom = map.getZoom();
    zoom++;
    if(zoom < MAX_ZOOM) map.setZoom(zoom);
}
function mapZoomOut(map){
    var zoom = map.getZoom();
    zoom--;
    if(zoom > 0) map.setZoom(zoom); 
}
function getMarker(position, map) {
	var options = {position: position, icon: {anchor:new google.maps.Point(5, 35),size: new google.maps.Size(46, 36),url: '/design/images/map_img/flag.png'}};
	if(undefined != map) 
		options['map'] = map;
	return new google.maps.Marker(options);
}

function getBoundsForPoly(polyline) {
  var bounds = new google.maps.LatLngBounds;
  polyline.getPath().forEach(function(latLng) {
    bounds.extend(latLng);
  });
  return bounds;
}
function getBoundsZoomLevel(bounds, mapDim) {
    var WORLD_DIM = { height: 256, width: 256 };
    var ZOOM_MAX = 25;

    function latRad(lat) {
        var sin = Math.sin(lat * Math.PI / 180);
        var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    function zoom(mapPx, worldPx, fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }

    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

    var lngDiff = ne.lng() - sw.lng();
    var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

    var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
    var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
}
function getRunbaseLatLng(){
    return new google.maps.LatLng(55.72140018145943, 37.548933159559965);
}
function initControl(map, isShowRunbase) {

	  // Set CSS for the control border.
	  var controlUI = document.createElement('div');
	  controlUI.style.cursor = 'pointer';
	  controlUI.style.marginTop = '30px';
	  controlUI.style.marginLeft = '30px';
	  // Set CSS for the control interior.
	  var img = document.createElement('img');
      //div.style.backgroundImage = '/design/images/map_img/plus.png';
      //div.style.backgroundPosition = '0px 0px';
      //div.style.backgroundRepeat = 'no-repeat';
	  //div.style.width = '48px';
      //div.style.height = '48px';
	  //img.style.height = '48px';
	  img.src = "/design/images/map_img/plus.png";
	  img.style.width = '48px';
	  img.style.height = '48px';
	  controlUI.appendChild(img);
	  
	  google.maps.event.addDomListener(controlUI, 'click', function() {
	    mapZoomIn(map);
	  });
	  
      google.maps.event.addDomListener(controlUI, 'mouseover', function() {
            $(this).find('img').attr('src', "/design/images/map_img/plus_hover.png")
	  });
	  google.maps.event.addDomListener(controlUI, 'mouseout', function() {
	        $(this).find('img').attr('src', "/design/images/map_img/plus.png")
	  });
	  controlUI.index = 1;
	  map.controls[google.maps.ControlPosition.LEFT_TOP].push(controlUI);
	  // Set CSS for the control border.
	  controlUI = document.createElement('div');
	  controlUI.style.cursor = 'pointer';
	  controlUI.style.marginTop = '2px';
	  controlUI.style.marginLeft = '30px';
	  // Set CSS for the control interior.
	  img = document.createElement('img');
	  img.src = "/design/images/map_img/minus.png";
	  img.style.width = '48px';
	  img.style.height = '48px';
	  controlUI.appendChild(img);
	  
	  google.maps.event.addDomListener(controlUI, 'click', function() {
	    mapZoomOut(map);
	  });
      google.maps.event.addDomListener(controlUI, 'mouseover', function() {
            $(this).find('img').attr('src', "/design/images/map_img/minus_hover.png")
	  });
	  google.maps.event.addDomListener(controlUI, 'mouseout', function() {
	        $(this).find('img').attr('src', "/design/images/map_img/minus.png")
	  });
	  controlUI.index = 2;
	  map.controls[google.maps.ControlPosition.LEFT_TOP].push(controlUI);
      if(undefined == isShowRunbase || isShowRunbase) {
       	  var options = {map: map, position: getRunbaseLatLng(), icon: {size: new google.maps.Size(150, 45),url: '/design/images/map_img/runbase.png'}};
          var runbaseMarker = new google.maps.Marker(options);
          
    	  google.maps.event.addDomListener(runbaseMarker, 'click', function() {
    	    window.open('/runbase');
    	  });
      }

}