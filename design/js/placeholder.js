$(function() {
    if($.browser.msie) {
        $('input[type=text], input[type=password]').each(function() {
            $(this).val($(this).attr('placeholder'))
                   .addClass('placeholder');
        });
        
        $('input[type=text], input[type=password]').focus(function() {
            if($(this).attr('placeholder') == $(this).val()) {
                $(this).val('').removeClass('placeholder');
            }
                
        });
        $('input[type=text], input[type=password]').focusout(function() {
            if($.trim($(this).val())=='') {
                var holder = $(this).attr('placeholder');
                $(this).val(holder)
                       .addClass('placeholder');
            } else {
                $(this).removeClass('placeholder');
            }
        });

        $('form input').keydown(function(e){
            e = e || window.event;
            var form = $(this).parents('form');
            if(form.length && e.keyCode == 13) {
                form.submit();
                return false;
            }
        });

    }
});