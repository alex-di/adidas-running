/**
 * The Slider jQuery plugin
 *
 * @version 1.0
 */
;(function($, window, document, undefined) {
	"use strict";
	/*jshint smarttabs:true*/
    var slideInterval = null;
    var defaultOptions = {
        slideTimeLife: 6000,
        slideOpen: 800,
        linesOpen: 300,
    }
    function updateSliderElements(box) {
        var elements = $(box).find('.slider_numbers');
        if(elements.length) {
            var currentSlider = getCurrentSlide(box);
            $(box).find('.index_slide').each(function(index, object) {
                if(index != currentSlider) {
                    $(object).css('opacity', 0).css('display', 'none').find('.slider_lines, .slider_shadow').hide();
                }
            });
        }
    };
    function getCurrentSlide(box) {
        return $(box).find('.slider_numbers').find('a.active').index();
    };
    function nextSlide(box) {
        clearInterval(slideInterval);
        slideInterval = setInterval(function(){
            var currentSlider = getCurrentSlide(box);
            var nextSlider = currentSlider + 1;
            if(currentSlider >= $(box).find('.slider_numbers > a').length - 1)
                nextSlider = 0;
            if(currentSlider != nextSlider && currentSlider >= 0) {
                updateSliderElements(box);
                slide(box, currentSlider, nextSlider);
            }    
            
        }, defaultOptions.slideTimeLife);
    };
    function slide(box, currentSlider, nextSlider) {
        updateSliderElements(box);
        $(box).find('.index_slide').eq(currentSlider).animate({opacity: 0}, {
            duration: defaultOptions.slideOpen,
            progress:function(animation, progress, NremainingMs){},
            complete:function(){
                $(this).hide();
            }
        });
        $(box).find('.index_slide').eq(nextSlider).show().animate({opacity: 1}, {
            duration: defaultOptions.slideOpen,
            progress:function(animation, progress, NremainingMs){
                $(box).find('.slider_numbers > a').removeClass('active').eq(nextSlider).addClass('active');
            },
            complete:function(){
                $(box).find('.index_slide').eq(nextSlider).find('.slider_lines, .slider_shadow').fadeIn(defaultOptions.linesOpen);
            },
        });
    };
    function goToSlide(box, numberSlide) {
        slide(box, getCurrentSlide(box), numberSlide);
        nextSlide(box);
    };
	function init(container) {
	   
        $(container).find('.slider_numbers > a').removeClass('active').eq(0).addClass('active');
        $(container).find('.slider_numbers > a').click(function(e){
            goToSlide(container, $(this).index());
            e.preventDefault();
        })
        updateSliderElements(container);
        nextSlide(container);
	}

	$.mySlider = function(){
		return init($());
	};

	$.fn.mySlider = function() {
		return init(this);
	};

})(jQuery, window, document);
