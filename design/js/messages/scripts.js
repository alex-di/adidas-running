    var DEFAULT_AVATAR = "/design/images/avatar_default.png";
    var NODEJS_PORT = 8099;
    var isReadyService = false;
    var allUnread = 0;
    var timeoutService = 10000;
    var siteTitle = "";
    var packet = {
        avatar: '',
        name: '',
        sname: '',
        to:0,
        text: '',
        attachments: {files: [], routes: []},
        additional: {files:[], routes: []}
      }
    var uid = 0;
    var socket = null;
    function initMessagesObject(id, unread, object) {
        if(object.avatar != undefined)
            packet.avatar = object.avatar;
        if(object.name != undefined)
            packet.name = object.name;
        if(object.sname != undefined)
            packet.sname = object.sname;
        if(object.to != undefined)
            packet.to = object.to;
        uid = id;
        allUnread = unread;
    }
    function setOponent(uid) {
        packet.to = uid;
    }
    function setRead(users_from) {
        socket.emit("read_msg", {users_from: user_from});
    }
    function updateScreen() {
        if(allUnread < 0)
            allUnread = 0;
        if(allUnread > 0)
            $(".n_box.black").show();    
        $(".n_box.black").html('<span></span>'+allUnread);
        $('#messages').text('сообщения '+(allUnread > 0 ? '('+allUnread+')' : ''));
    }
    function getAvatar(data) {
        return data.avatar != undefined && "" != data.avatar ? getPreviewImage(data.avatar, 260) : DEFAULT_AVATAR;
    }
    function getDate(data) {
    	if(undefined != data.date && "" != data.date) {    
    		var temp = data.date.split(' ');
    		var t = temp[0].split('-');
    		var h = temp[1].split(':');
    		return 'сегодня в ' + h[0]+':'+h[1];
    	}
    	return "";
    }
    $(function() {
        socket = io.connect('http://'+window.location.hostname+':'+NODEJS_PORT);
        socket.on('connect', function () {
            socket.emit('auth', {id:uid});
        });
        socket.on('reconnect', function () {
            socket.emit('auth', {id:uid});
        });
        socket.on('disconnect', function () {
            isReadyService = false;
        });
        socket.on('connect_failed', function () {
            isReadyService = false;
        });
        socket.on('ready', function () {
            isReadyService = true;
        });
        socket.on('new_msg', function(data) {
            if(uid == data.users_from || packet.to == data.users_from) {
                insertMessages(data, true);
                repositionMessageForm();
                if(uid != data.users_from) {
                    setRead(data.users_from);
                }
            } else {
                if(uid != data.users_from && window.location.pathname.indexOf('messages') > -1){
                    var ui = $('#addresat' + data.users_from);
                    var message_id = new Date().getTime();
                    if(ui) {
                        var files = getFileBox(data, message_id);
                        ui.addClass('new').find('.massage_text').empty().append(data.text+files);
                        ui.find('.date').empty().append(getDate(data));
                        
                    }
                }
                allUnread++;
                updateScreen();
            }
        });
        updateScreen();
    });
          
    function insertMessages(data, isScroll) {
        var temp = getTemplateMessage(data);
        $("#message_content_block").append(temp.html);
        setTimeout(function(){
            $('#message'+temp.id).removeClass('new');
        }, 1200);
        if(temp.isImages)
            initLytebox();
        if(isScroll) {
           scrollMessageBlock();
        }
    }
    function scrollMessageBlock(){
        //if($("#message_content_block").length)
        //    $(window).animate({scrollTop:$(window)[0].scrollHeight}, "slow"); 
    }
    function sendMessage() {            
        if(packet.to != 0 && !isNaN(packet.to) && (packet.text != '' || packet.attachments.files.length || packet.attachments.routes.length)) {
            if(isReadyService) {
                socket.emit("msg", packet);
                packet.attachments.files = [];
                packet.attachments.routes = [];
                packet.additional.files = [];
                packet.additional.routes = [];
                return true;
            } else {
                $.showInfo('Сервис временно недоступен');
            }
        }
        return false;
    }
    function getTemplateMessage(data) {
        var template = {id: new Date().getTime()};
        var files = getFileBox(data, template.id);
    
        template.html =  '<div id="message'+template.id+'" class="message_point new">'+
                    '<a target="_blank" href="/profile/'+data.users_from+'" class="ava_box">'+
                        '<img src="'+getAvatar(data)+'" alt="img" />'+
                    '</a>'+
                    '<div class="title">'+
                        '<a target="_blank" href="/profile/'+data.users_from+'" class="name">'+data.name+' '+data.sname+'</a>'+
                        '<div class="date">'+getDate(data)+'</div>'+
                    '</div>'+
                    '<div class="massage_text">' + data.text + files + '</div>'+
                '</div>';
        template.isImages = $.trim(files) != '';
        return template;
    }
    function getFileBox(data, message_id) {
        var images = '';
        if(undefined != data.additional && undefined != data.additional.files && data.additional.files.length) {
            for(var p = 0; p < data.additional.files.length; p++) {
                var pathToImage = data.additional.files[p];
                images += '<a rel="lytebox[messages'+message_id+']" href="'+pathToImage+'" target="_blank" style="display:none"><img onload="$(this).parent().fadeIn(200)" src="'+getPreviewImage(pathToImage, 180)+'" alt="img" /></a>';
            }
            images = '<div class="img_inside">'+images+'</div>';
        }
        return images;
    }
    function messageDialog(user) {
        var param = user || {};
        var intervalControlUI = null;
        $.post('/messages/index/blockmessage', param, function(data) {
            $('.modal.write_message_box').remove();
            $('body').append(data);
            $('.modal.write_message_box').modal().open({onClose: function(){
                                    $('.modal.write_message_box').remove();
                                }, onOpen: function() {
                                    $('.themodal-overlay').css('z-index', 90);
                                    intervalControlUI = setInterval(function(){
                                        controleSizeInputFields($('#messageFieldText'), $('#controlSizeTextarea'), 1500)
                                    }, 100);
                                }, onClose: function(){
                                    clearInterval(intervalControlUI);
                                }});
        });
    }
    function onErrorUploadToMessages(up, error) {
        if(-600 == error.code) {
            $.showInfo("Недопустимый размер файла");
        } else {
            $.showInfo(error.message);
        }
    }
    function deleteDialogs(users_id) {
        $.confirm('Удалить диалог?', function(){
           $.post('/messages/index/processdeletedialog/id/'+users_id, {}, function(){
                $.showInfo('Успешно удалено', '/messages');
           }); 
        });
    }
    function getPreviewImage(pathToImage, needSize) {
        console.log('getPreviewImage: '+pathToImage);
        var temp = pathToImage.split('.');
        return temp[0] + '/prev' + needSize + '.' + temp[1]; 
    }