  $.Isotope.prototype._masonryResizeChanged = function() {
    return true;
  };

  $.Isotope.prototype._masonryReset = function() {
    // layout-specific props
    this.masonry = {};
    this._getSegments();
    var i = this.masonry.cols;
    this.masonry.colYs = [];
    while (i--) {
      this.masonry.colYs.push( 0 );
    }  

    if ( this.options.masonry.cornerStampSelector ) {
      var $cornerStamp = this.element.find( this.options.masonry.cornerStampSelector ),
          stampWidth = $cornerStamp.outerWidth(true) - ( this.element.width() % this.masonry.columnWidth ),
          cornerCols = Math.ceil( stampWidth / this.masonry.columnWidth ),
          cornerStampHeight = $cornerStamp.outerHeight(true);
      for ( i = Math.max( this.masonry.cols - cornerCols, cornerCols ); i < this.masonry.cols; i++ ) {
        this.masonry.colYs[i] = cornerStampHeight;
      }
    }
  };
var isResizeIsotope = false;

function onLoadImage(img) {
	isResizeIsotope = true;
}
function isIsotope(selector) {
	return  selector.hasClass('isotope')
}
function initHeightShortText() {
    $('.element .animation_text').each(function(){
        $(this).find('.short_text').height($(this).height()); 
    });
}
var layoutMode = 'masonry';
function initIsotope(selector) {
    if(selector) {
        var $container = $(selector); 
    } else {
        var $container = $('#container');
    }
          
    
    $container.isotope({
      layoutMode: layoutMode,
      itemSelector : '.element',
      masonry : {
        columnWidth : 300,
        cornerStampSelector: '.corner-stamp'
      },
      animationEngine : 'jquery',
      animationOptions: {
    	     duration: 0,
    	     easing: 'linear',
    	     queue: false
      },
      getSortData: {
        number: function($elem) {
                if($elem.hasClass('promo')) {
                    return 0;
                }
                return  Math.round(Math.random() * 1000);
            }
      }
    });
}
function initShortDescription(){
    $(".element.article").hover(          
      function () {
        if ($(this).children(".animation_text").children(".short_text").length > 0){ 
          //$(this).children(".animation_text").css("height", $(this).children(".animation_text").children('a.title').height());
          $(this).children(".animation_text").children('a.title').css("display", "none");
          $(this).children(".animation_text").children('.short_text').css("display", "block");
        }
      }, 
      function () {
        if ($(this).children(".animation_text").children(".short_text").length > 0){
          $(this).children(".animation_text").children('a.title').css("display", "block");
          $(this).children(".animation_text").children('.short_text').css("display", "none");
        }
      }
    );
}
var defaultPaginator = null;
var isOpenDialogAddArticle = false;
$(function() {
    Custom.init();
	initIsotope();
    initShortDescription();
	setInterval(function(){
		var container = $("#container");
		if(container && isResizeIsotope && isIsotope(container)) {
			isResizeIsotope = false;
			container.isotope('reLayout');
		}
	}, 100);

    initHeightShortText();
    
    $("div.menu ul li").click(function () {
		$("div.menu ul li").removeClass('current');
		$(this).addClass('current');
	});
    
    $("label.sex_label").click(function () {
		$("label.sex_label").removeClass('current');
		$(this).addClass('current');
	});
     
     $(".sort ul li, .route_sort ul li").click(function () {
        if(!$(this).hasClass('no_active')) {
            $(this).parent().find('li').removeClass('current');
            $(this).addClass('current'); 
        }

	});
     
     $(".bottom_part ul li").click(function () {
		$(".bottom_part ul li").removeClass('current');
		$(this).addClass('current');
	});
     
     $(".left_menu ul li").click(function () {
		$(".left_menu ul li").removeClass('current');
		$(this).addClass('current');
	});
    $(".section_inner ul.tabs_inner li").click(function () {
		$(".section_inner ul.tabs_inner li").removeClass('current');
		$(this).addClass('current');
	});
     
    $(".right_menu ul li").click(function () {
		$(".right_menu ul li").removeClass('current');
		$(this).addClass('current');
	});
    
    $("form.search input[type='text']").focus(function () {		
		$(this).parents('form.search').children("input[type='button']").addClass("focus");
	});
    
    $("form.search input[type='text']").blur(function () {		
		$(this).parents('form.search').children("input[type='button']").removeClass("focus");
	});
     
     
     ///////......................................................................
     /*$(".route_map").each(function () {        
		$(this).css("top", $(this).prev(".profile_info").height()+22);
	});*/
            

    
    $('.control > input').focus(function() {
      $(this).parent().removeClass('error');
    });
    /////////////////////////////////////////////////////////////////////////////////////
    initStyleComboBox();

    /////////////////////////////////////////////////////////////////////////////////////
    
        
	$('html').click(function(e){
	    if(!hitObjectsArea("drop_menu_content", e.pageX, e.pageY) && e.pageX != undefined) {
	        var obj = hitObjectsArea("drop_menu_button", e.pageX, e.pageY);
	        
	        if(obj) {
	            var menuContent = obj.next();
	            if(!menuContent.is(":visible")) {
	                $(".drop_menu_content").slideUp();
	                menuContent.slideDown();
	            } else {
	               $(".drop_menu_content").slideUp();
	            }                   
	        } else {
	            $(".drop_menu_content").slideUp();
	        }
	    }
	});
	
	$(window).scroll(function(){
	    if( $(window).scrollTop()/($(document).height() - $(window).height()) > 0.4) {
	    	if(null != defaultPaginator && !isOpenDialogAddArticle)
                defaultPaginator.getMaterials();
	    }
	});
    setInterval(function(){
        BadPost.checkBadPost();
        EventsList.checkViewEvents();
    }, 1000);
});
function initStyleComboBox(){
    $(".combo ul li").click(function () {
        $(this).parents(".combo").find(".current").html($(this).html());
        $(this).parents(".combo").find("input").val($(this).data('id')).trigger('change');
        $(this).parents(".combo").find('ul').css('display', 'none');
 
    }); 
    $(".combo > .current, .combo > .arrow").click(function () {
        var ul = $(this).parent().find('ul');
        if(ul.is(':visible')) {
            ul.hide().parent().removeClass('active');
        } else {
            ul.show().parent().addClass('active');
        }
    });
    $('html').click(function(e){
       var currentCombo = $('.combo.active');
       if(currentCombo.length && undefined != e.pageX) {
            var offset= currentCombo.offset();
            if(!(e.pageX >= offset.left && e.pageX <= offset.left+currentCombo.width() && e.pageY>=offset.top && e.pageY<=offset.top+currentCombo.height())) {
                currentCombo.find('ul').hide().parent().removeClass('active');
            }
       } 
    });
}
function showDialogFastAddArticles(){
    isOpenDialogAddArticle = true;
    $('#add_modal').modal().open({isReposition: true, onClose:function(){
        isOpenDialogAddArticle = false;
    }});
}
var BadPost = {
    post:[],
    add:function(post_id) {
        this.post.push(post_id);
    },
    checkBadPost:function(){
        if(this.post.length) {
            var data = this.post;
            this.post = [];
            $.post('/materials/social/processcheckpost', {data: data});
        }
    }   
}
var EventsList = {
    list:[],
    add:function(event_id) {
        this.list.push(event_id);
    },
    checkViewEvents:function(){
        if(this.list.length) {
            var data = this.list;
            this.list = [];
            $.post('/users/profile/processcheckevent', {data: data});
        }
    } 
}
function Paginator(dataPath) {
    
    var dataPath = dataPath;
    var currentParams = {'jsPaginator': 'defaultPaginator'};
    var currentPage = 1;
    var countPage = 0;
    var isBusy = false;
    var callbackEndPaginator = null;
    var containerSelector = '#container';
    
    this.setCallbackEndPaginator = function(callback){
        callbackEndPaginator = callback;
    };
    this.setDataPath =  function(value) {
	   dataPath = value;
       return this;
    };
    this.setCountPage =  function(value) {
	   countPage = value;
        if(null != callbackEndPaginator) {
            callbackEndPaginator(currentPage, countPage);
        }
       return this;
    };
    this.setParam =  function(name, value) {
	   currentParams[name] = value;
       return this;
    };
    this.setContainerSelector =  function(value) {
	   containerSelector = value;
       return this;
    };
    this.resetParam =  function(name, value) {
	   currentParams = {'jsPaginator': 'defaultPaginator'};
       return this;
    };
    this.resetPage =  function() {
    	currentPage = 0;
        countPage = 1;
        return this;
    };
    this.getCountPage = function(){
        return countPage;
    };
    this.getCurrentPage = function(){
        return currentPage;
    };
    this.getMaterials = function(isClearStrips) {
    	if(!isBusy && currentPage < countPage && undefined != dataPath) {
    		isBusy = true;
    	    ++currentPage;
    	    post(dataPath + "/page/" + currentPage, currentParams, function(data) {
    	        var newElements = $(data.body);
    	        if(isClearStrips) {
    				$('footer').hide();
  
    	        	$(containerSelector).animate({opacity: 0}, 300, function(){
                            if(isIsotope($(containerSelector)))
                                $(containerSelector).isotope('destroy'); 
    	        			$(containerSelector).children().not('.promo_rorator, .corner-stamp, .promo, .people, .index_slider').remove();
    	        			$(containerSelector).animate({opacity: 1}, 300, function(){
    	        				if(!data.isNotFind) {
                                    initIsotope(containerSelector);
                                    $(containerSelector).append( newElements ).isotope( 'appended', newElements);
                                    initHeightShortText();
                                    initShortDescription();
    	        				} else {
    	        				   $(containerSelector).append( data.notFindData )
    	        				}
    	        				$('footer').show();
                                isBusy = false;
    	                	});
    	            	});
    	        } else {
    	        	$(containerSelector).append( newElements ).isotope( 'appended', newElements);
                    initHeightShortText();
                    initShortDescription();
                    isBusy = false;
    	        }
    	        
    	    }, "json"); 
    	}  
    };
    this.getData = function(isClearStrips) {
    	if(!isBusy && undefined != dataPath) {
    	    post(dataPath, currentParams, function(data) {
            	$(containerSelector).append(data);
                isBusy = false;      
    	    }); 
    	}  
    };       
}

var MenuNavigator = {};
MenuNavigator.setCurrentMenu = function(path) {
    $('nav li a[href*="'+path+'"]').parent().addClass('current');
}
MenuNavigator.setCurrentProfileMenu = function(id){
    $('div.section.vertical ul.tabs li#'+id).removeClass('active').addClass('current').removeAttr('onclick');
} 
function hitObjectsArea(clazz, mousex, mousey) {
     for(var i = 0; i < $("."+clazz).length; i++) {
        if(hitTestPoint($("."+clazz).eq(i), mousex, mousey)) {
            return $("."+clazz).eq(i);
        }
     }
     return false;
}
function hitTestPoint(obj, x, y) {
    var offset= obj.offset();
    if(x>=offset.left && x<=offset.left+obj.width() && y>=offset.top && y<=offset.top+obj.height()) {
        return true;
    }
    return false;
}


function materialVote(resource, namespace, type) {
    $.post('/votes/materials', {resource: resource, namespace: namespace, type: type}, function(){
        $.post('/likes.php', {resource_id: resource});
    });
}
function addMaterialsComment(form, resource) {
	post('/comments/materials/save/resource/'+resource, $(form).serialize(), function(data){
		if('ok' == data.status) {
			$(form).find('textarea').val('');
			$(form).after(data.body);
		} else if('error' == data.status) {
			alert(data.error);
		}
	}, 'json');
	return false;
}
function registerOnEvent(event_id, obj) {
    post('/materials/events/processregister', {id: event_id}, function(){   
   	    $.showInfo('Вы успешно зарегистрировались на событие');
        $(obj).hide();
	});
}
function addClothes(clothes_id, type, button) {
    if(!$(button).hasClass('disabled')) {
        $.post('/users/clothes/processaddclothes', {clothes_id: clothes_id, type: type}, function(data) {
            $.showInfo('Успешно сохранено');
            $(button).addClass('disabled');
            if($(button).hasClass('ui_have_clothes'))
                $(button).prev('.i_have').hide();
        });
    }

}
function deleteClothes(clothes_id) {
   $.confirm('Вы уверены?', function() {
        post('/users/clothes/processdeleteclothes', {clothes_id: clothes_id}, function(data) {
            $('#clothes_'+clothes_id).remove();
            $.showInfo('Успешно удалено');
            ui = $('.tabs_inner.clothes_tab li.current span');
            if(ui) {
                var countClothes = parseInt(ui.data('counter'));
                countClothes--;
                ui.data('counter', countClothes).text(countClothes);
                if(0 == countClothes)
                    ui.parent('li').click();
            }
            isResizeIsotope = true;
        });
    });
}
function addFavorites(user_id) {
    $.post('/users/favorites/processaddfavorites', {user_id: user_id}, function(data) {
        $.showInfo(data.message);
        $('.favorite, .del_favorite').hide();
        $('.del_favorite').show();
    }, 'json');
}
function deleteFavorites(user_id) {
    $.post('/users/favorites/processdeletefavorites', {user_id: user_id}, function(data) {
        $.showInfo('Успешно удалено');
        $('.favorite, .del_favorite').hide();
        $('.favorite').show();
    });
}
function runningRoute(route_id, type, ui) {
    $.post('/users/routes/processrunroute', {id: route_id, type: type}, function(data) {
        if('OK' == data) {
            $.showInfo('Успешно сохранено');
        } else if('ALLREADY' == data){
            $.showInfo('Успешно сохранено');
        } else if('IMPOSIBLE' == data){
            $.showInfo('Вы уже пробежали данный маршрут');
        } else {
            $.showInfo('Данная функция доступна авторизованным пользователям');
        }
        if(ui && ('OK' == data || 'ALLREADY' == data)) {
            if(1 == type)
                $(ui).next().hide();
            $(ui).addClass('click');
        }
        
    });
}
function redirectOnMain() {
  window.location = '/';
}
function controleSizeInputFields(textFields, controlUI, maxCharsSize) {
    var text = textFields.val();
    textFields.val(text.substr(0, maxCharsSize));
    var size = maxCharsSize - text.length;
    size = size < 0 ? 0 : size;
    controlUI.text('осталось '+size+' '+getWordsByNumber(size, ['символ', 'символов', 'символов', 'символа', 'символов']));
}
/**
 * num - значение
 * text[0] - значения для чисел, результат которых по модулю 10 равный 1 и число != 11
 * text[1] - значения для чисела 11
 * text[2] - значения для чисел, результат которых по модулю 10 попадает в интервал [2, 4] и число < 20 && > 10
 * text[3] - значения для чисел, результат которых по модулю 10 попадает в интервал [2, 4] и число > 20
 * text[4] - остальные числа
 */
function getWordsByNumber(num, text) {
    var string = text[4];
	if (num != 0) {
		rest = num % 10 == 0 ? 9 : num % 10;
		if (rest == 1) {
			if (num != 11) {
				string = text[0];
			} else {
				string = text[1];
			}
		}
		if (rest > 1 && rest < 5) {
			if (num < 20 && num > 10) {
				string = text[2];
			} else {
				string = text[3];
			}

		}
		if (rest > 4) {
			string = text[4];
		}
	}
	return string;
}
function formatSize(length){
    var i = 0, type = ['б','Кб','Мб','Гб','Тб','Пб'];
    while((length / 1000 | 0) && i < type.length - 1) {
        length /= 1024;
        i++;
    }
    return length.toFixed(2) + ' ' + type[i];
}