function saveForm(url, form) {
    if (typeof SpawEngine != 'undefined') {
        SpawEngine.updateFields();
    }
    
    if (typeof tinyMCE != 'undefined') {
        $.each($('.tiny_block_editor'), function(index, value) {
            text = tinyMCE.get($(value).attr('id')).getContent()
            $("#" + $(value).attr('id')).val(text);
        });     
    }
    $.php(url, $(form).serialize());
    return false;
}