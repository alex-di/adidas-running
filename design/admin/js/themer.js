var backgroundPattern;
var baseColor;
var highlightColor;
var textColor;
var textGlowColor = {};

var patterns = {
	Paper: {
		name: "Paper", 
		img: "/design/admin/images/core/bg/paper.png"
	}, 
	Blueprint: {
		name: "Blueprint", 
		img: "/design/admin/images/core/bg/blueprint.png"
	}, 
	Bricks: {
		name: "Bricks", 
		img: "/design/admin/images/core/bg/bricks.png"
	}, 
	Carbon: {
		name: "Carbon", 
		img: "/design/admin/images/core/bg/carbon.png"
	}, 
	Circuit: {
		name: "Circuit", 
		img: "/design/admin/images/core/bg/circuit.png"
	}, 
	Holes: {
		name: "Holes", 
		img: "/design/admin/images/core/bg/holes.png"
	}, 
	Mozaic: {
		name: "Mozaic", 
		img: "/design/admin/images/core/bg/mozaic.png"
	}, 
	Roof: {
		name: "Roof", 
		img: "/design/admin/images/core/bg/roof.png"
	}, 
	Stripes: {
		name: "Stripes", 
		img: "/design/admin/images/core/bg/stripes.png"
	}
};

	var backgroundTargets = 
	[
		"body", 
		"div#mws-container"
	];
	
	var baseColorTargets = 
	[
		"div#mws-sidebar-bg", 
	 	"div#mws-header", 
		".mws-panel .mws-panel-header", 
		"div#mws-error-container", 
		"div#mws-login", 
		"div#mws-login .mws-login-lock", 
		".ui-accordion .ui-accordion-header", 
		".ui-tabs .ui-tabs-nav", 
		".ui-datepicker", 
		".fc-event-skin", 
		".ui-dialog .ui-dialog-titlebar", 
		"div.jGrowl div.jGrowl-notification, div.jGrowl div.jGrowl-closer", 
		"div#mws-user-tools .mws-dropdown-menu .mws-dropdown-box", 
		"div#mws-user-tools .mws-dropdown-menu.toggled a.mws-dropdown-trigger"
	];
	
	var borderColorTargets = 
	[
	 	"div#mws-header"
	];
	
	var highlightColorTargets = 
	[
		"div#mws-searchbox input.mws-search-submit", 
		".mws-panel .mws-panel-header .mws-collapse-button span", 
		"div.dataTables_wrapper .dataTables_paginate div", 
		"div.dataTables_wrapper .dataTables_paginate span.paginate_active", 
		".mws-table tbody tr.odd:hover td", 
		".mws-table tbody tr.even:hover td", 
		".fc-state-highlight", 
		".ui-slider-horizontal .ui-slider-range", 
		".ui-slider-vertical .ui-slider-range", 
		".ui-progressbar .ui-progressbar-value", 
		".ui-datepicker td.ui-datepicker-current-day", 
		".ui-datepicker .ui-datepicker-prev .ui-icon", 
		".ui-datepicker .ui-datepicker-next .ui-icon", 
		".ui-accordion-header .ui-icon", 
		".ui-dialog-titlebar-close .ui-icon"
	];
	
	var textTargets = 
	[
		".mws-panel .mws-panel-header span", 
		"div#mws-navigation ul li.active a", 
		"div#mws-navigation ul li.active span", 
		"div#mws-user-tools #mws-username", 
		"div#mws-navigation ul li span.mws-nav-tooltip", 
		"div#mws-user-tools #mws-user-info #mws-user-functions #mws-username", 
		".ui-dialog .ui-dialog-title", 
		".ui-state-default", 
		".ui-state-active", 
		".ui-state-hover", 
		".ui-state-focus", 
		".ui-state-default a", 
		".ui-state-active a", 
		".ui-state-hover a", 
		".ui-state-focus a"
	];
	
$(function(){
	
	var presetDd = $('<select id="mws-theme-presets"></select>');
	for(var i = 0; i < views.data.length; i++) {
		var option = $("<option></option>").text(views.data[i].name).val(i);
		presetDd.append(option);
	}
	$("#mws-theme-presets-container").append(presetDd);
	
	presetDd.bind('change', function(event) {
		updateBaseColor(views.data[presetDd.val()].baseColor);
		updateHighlightColor(views.data[presetDd.val()].highlightColor);
		updateTextColor(views.data[presetDd.val()].textColor);
		updateTextGlowColor(views.data[presetDd.val()].textGlowColor, views.data[presetDd.val()].textGlowColor.a);
        updateBackground(patterns[views.data[presetDd.val()].patterns].img);
		$("#mws-theme-patterns option[value='"+views.data[presetDd.val()].patterns+"']").attr("selected", "selected");
		tempViews.patterns = views.data[presetDd.val()].patterns;
        attachStylesheet();
		
		event.preventDefault();
	});
	
	
	var patternDd = $('<select id="mws-theme-patterns"></select>');
	for(var i in patterns) {
		var option = $("<option></option>").text(patterns[i].name).val(i);
		patternDd.append(option);
	}
	$("#mws-theme-pattern-container").append(patternDd);
	
	patternDd.bind('change', function(event) {
		updateBackground(patterns[patternDd.val()].img, true);
		
        tempViews.patterns = patternDd.val();
		event.preventDefault();
	});
    
    $("#mws-theme-presets option[value='"+views.selected+"']").attr("selected", "selected");
	$("#mws-theme-patterns option[value='"+views.data[views.selected].patterns+"']").attr("selected", "selected");
    updateBackground(patterns[views.data[views.selected].patterns].img);
    updateBaseColor(views.data[views.selected].baseColor);
	updateHighlightColor(views.data[views.selected].highlightColor);
	updateTextColor(views.data[views.selected].textColor);
	updateTextGlowColor(views.data[views.selected].textGlowColor, views.data[views.selected].textGlowColor.a);
    
    
	$("div#mws-themer #mws-themer-toggle").bind("click", function(event) {
		if($(this).hasClass("opened")) {
			$(this).toggleClass("opened").parent().animate({right: "0"}, "slow");
		} else {
			$(this).toggleClass("opened").parent().animate({right: "288"}, "slow");
		}
	});
	
	$("div#mws-themer #mws-textglow-op").slider({
		range: "min", 
		min:0, 
		max: 100, 
		value: 50, 
		slide: function(event, ui) {
			alpha = ui.value * 1.0 / 100.0;
			updateTextGlowColor(null, alpha, true);
		}
	});
	
	$("div#mws-themer #mws-themer-css-dialog").dialog({
		autoOpen: false, 
		title: "Theme CSS", 
		width: 500, 
		modal: true, 
		resize: false, 
		buttons: {
			"Close": function() { $(this).dialog("close"); }
		}
	});
	
	$("#mws-base-cp").ColorPicker({
		color: baseColor, 
		onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
		},
		onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
		},
		onChange: function (hsb, hex, rgb) {			
			updateBaseColor(hex, true);
		}
	});
	
	$("#mws-highlight-cp").ColorPicker({
		color: highlightColor, 
		onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
		},
		onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
		},
		onChange: function (hsb, hex, rgb) {			
			updateHighlightColor(hex, true);
		}
	});
	
	$("#mws-text-cp").ColorPicker({
		color: textColor, 
		onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
		},
		onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
		},
		onChange: function (hsb, hex, rgb) {			
			updateTextColor(hex, true);
		}
	});
	
	$("#mws-textglow-cp").ColorPicker({
		color: textGlowColor, 
		onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
		},
		onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
		},
		onChange: function (hsb, hex, rgb) {
			updateTextGlowColor(rgb, textGlowColor["a"], true);
		}
	});
});	
	function updateBackground(bg, attach)
	{
		backgroundPattern = bg;
		
		if(attach == true)
			attachStylesheet();
	}
	
	function updateBaseColor(hex, attach)
	{
		baseColor = "#" + hex;
		tempViews.baseColor = hex;
        $("#mws-base-cp").css('backgroundColor', baseColor);
		
		if(attach === true)
			attachStylesheet();
	}
	
	function updateHighlightColor(hex, attach)
	{
		highlightColor = "#" + hex;
        tempViews.highlightColor = hex;
		$("#mws-highlight-cp").css('backgroundColor', highlightColor);
		
		if(attach === true)
			attachStylesheet();
	}
	
	function updateTextColor(hex, attach)
	{
		textColor = "#" + hex;
        tempViews.textColor = hex;
		$("#mws-text-cp").css('backgroundColor', textColor);
		
		if(attach === true)
			attachStylesheet();
	}
	
	function updateTextGlowColor(rgb, alpha, attach)
	{
		if(rgb != null) {
			textGlowColor.r = rgb["r"];
			textGlowColor.g = rgb["g"];
			textGlowColor.b = rgb["b"];
			textGlowColor.a = alpha;
            tempViews.textGlowColor = textGlowColor;
		} else {
			textGlowColor.a = alpha;
            tempViews.textGlowColor.a = alpha;
		}
		
		$("div#mws-themer #mws-textglow-op").slider("value", textGlowColor.a * 100);
		$("#mws-textglow-cp").css('backgroundColor', '#' + rgbToHex(textGlowColor.r, textGlowColor.g, textGlowColor.b));
		
		if(attach === true)
			attachStylesheet();
	}
	
	function attachStylesheet(basePath)
	{
		if($("#mws-stylesheet-holder").size() == 0) {
			$('body').append('<div id="mws-stylesheet-holder"></div>');
		}
		
		$("#mws-stylesheet-holder").html($('<style type="text/css">' + generateCSS(basePath) + '</style>'));
	}
	
	function generateCSS(basePath)
	{
		if(!basePath)
			basePath = "";
			
		var css = 
			backgroundTargets.join(", \n") + "\n" + 
			"{\n"+
			"	background-image:url('" + basePath + backgroundPattern + "');\n"+
			"}\n\n"+			
			baseColorTargets.join(", \n") + "\n" + 
			"{\n"+
			"	background-color:" + baseColor + ";\n"+
			"}\n\n"+
			borderColorTargets.join(", \n") + "\n" + 
			"{\n"+
			"	border-color:" + highlightColor + ";\n"+
			"}\n\n"+
			textTargets.join(", \n") + "\n" + 
			"{\n"+
			"	color:" + textColor + ";\n"+
			"	text-shadow:0 0 6px rgba(" + getTextGlowArray().join(", ") + ");\n"+
			"}\n\n"+
			highlightColorTargets.join(", \n") + "\n" + 
			"{\n"+
			"	background-color:" + highlightColor + ";\n"+
			"}\n";
			
		return css;
	}
	
	function getTextGlowArray()
	{
		var array = new Array();
		for(var i in textGlowColor)
			array.push(textGlowColor[i]);
			
		return array;
	}
	
	function rgbToHex(r, g, b)
	{
		var rgb = b | (g << 8) | (r << 16);
		return rgb.toString(16);
	}
