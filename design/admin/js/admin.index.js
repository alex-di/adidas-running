    var isProcessSearchPosts = false;
    function searchTwitterPosts(){
        if(!isProcessSearchPosts) {
            window.location = '/admin/posts/twittersearch';
            isProcessSearchPosts =true;
        } else {
            alert('Подождите, идет поиск');
        }
    }
    function searchInstagramPosts(){
        if(!isProcessSearchPosts) {
            window.location = '/admin/posts/instagramsearch';
            isProcessSearchPosts =true;
        } else {
            alert('Подождите, идет поиск');
        }
    }
	// пост запрос с проверкой на права доступа
    function post(action, value, callback, dataType) {
        var dataType = dataType || "html";
        var callback = callback || function(data) {};
        var value = value || {};
        
        $.post(action, value, function(data) {
            if("PERMISSION_DENIDED" == data) {
                alert("Не достаточно прав");
            } else {
                callback(data);
            }
        },dataType);
    }
    // пагинация данных таблиц
    function paginator(page, obj) {
        var head = $(obj).parents(".tableContent");
        head.find("input[name='page']").val(page);
        lockedForm(head);
        post($(obj).parents(".tableContent").data("action"), head.find("form").serialize(), function(data){
            head.find("table > tbody").empty().append(data);
            head.find(".paginator_view").empty().append(head.find("table > tbody tr.paginator_data > td").html());
            head.find("table > tbody tr.paginator_data").remove();
            head.find("table tr:odd").addClass("odd");
            head.find("table tr:even").addClass("even");
            unlockedForm(head);
        }, "text");
    }
    function lockedForm(element) {
        var top = element.find('form').offset().top - 100;
        var left = element.find('form').offset().left-246;
        var width = element.find('form').width();
        var height = element.find('form').height();
        element.after("<div class='load_mask' style='left:"+left+"px;top:"+top+"px;width:"+width+"px;height:"+height+"px;'><img style='top:40%;left:50%;position:absolute' src='/design/admin/images/loader.gif'/></div>");
    }
    function unlockedForm(element) {
        element.next(".load_mask").remove();
    }
    // проверка на число
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
    // установить сортируемый столбец
    function setOrderColl(table, order) {
        var row = "";
        var how = "";
        if("" != order) {
            var order = order.split(" ");
            row = order[0];
            how = order[1].toLowerCase();
        } else {
            row = $("#"+table).find(".sort_field").eq(0).attr("id");
            how = "asc";
        }
        $("#"+table+" th#"+row).removeClass("sorting").addClass("sorting_"+how);
        $("#"+table).find("input[name='order']").val(row+" "+how.toUpperCase());
    }
    // массовый выбор управляющих чекбоксов
    function massCheck(obj) {
        var head = $(obj).parents(".tableContent");
        if($(obj).is(":checked")) {
            head.find("input.mass_checked").attr("checked", "checked");
        } else {
            head.find("input.mass_checked").removeAttr("checked");
        }
        
    }
    function addObject(obj, controller) {
        window.location = '/admin/'+controller+'/edit';
    }
    // массовое удаление обьектов
    function massDeleteObject(obj, controller) {
        if(confirm("Вы уверены?")) {
            var head = $(obj).parents(".tableContent");
            if(head.find("input.mass_checked:checked").length) {
                var elements = head.find("input.mass_checked:checked");
                var id = [];
                for(var i = 0; i < elements.length; i++) {
                    id.push(elements.eq(i).val());
                }
                post("/admin/"+controller+"/delete", {id: id}, function(data){
                    window.location = data;
                });
            } else {
                alert("Ничего не выбрано");
            }
        }
    }
    // пре-удаление обьект
    function deleteObject(link) {
        if(confirm("Вы уверены?")){
            window.location = link;
        }
    }
    function showFormMessages(message) {
        //если открыта форма в диалоговом окне
        if($(".ui-dialog-content:visible").length) {
            $(".ui-dialog-content:visible form").append(message);
        } else {
            $(".mws-button-row").before(message);
        }
    }
    
    function formatSize(length){
	    var i = 0, type = ['б','Кб','Мб','Гб','Тб','Пб'];
	    while((length / 1000 | 0) && i < type.length - 1) {
	        length /= 1024;
	        i++;
	    }
	    return length.toFixed(2) + ' ' + type[i];
	}
    function callbackUpload(fild_name, isMultiply, response, button_id) {
        var jsa = jQuery.parseJSON(response.response);
        console.log(jsa);
	if(jsa.status == "ok") {
            if(!isMultiply) {
                $("#"+button_id).next().empty().append(getFileView(fild_name, jsa, isMultiply));
            }
        } else {
            alert(jsa.message);
        }

    }
    function callbackUploadError(up, error) {

        if(-600 == error.code) {
            alert("Недопустимый размер файла. Максимально допустимый размер файла "+formatSize(up.settings.max_file_size));
        } else {
            alert(error.message);
        }
        
    }
    function getFileView(inputname, file, isMultiply) {
        if(isMultiply) {
                /*
    <div class="image">
        <img src="">
                            <input type="hidden" name="attachments[]" value="151">
                            <div>
                                <a style="" onclick="$(this).parent().parent().remove()" href="javascript:void(0)" class="mws-ic-16 ic-cross"></a>
                            </div>
                        </div>
    */
        } else {
            return '<div class="image">'+
                        '<img src="'+file.file+'">'+
                        '<input type="hidden" name="'+inputname+'" value="'+file.file+'">'+
                    '</div>';
        }
    }


    $(function() {
        $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    	$(".datetime_field").datetimepicker({dateFormat: 'yy-mm-dd', date: new Date()});
    	$(".date_field").datepicker({dateFormat: 'yy-mm-dd', date: new Date()});
        
        
        $("input.search_field").keyup(function(e) {
            var e = e || window.event;
            if (e.keyCode === 13)
                paginator(0, this);
        });
        // при выборе позиции в списке выполняем поиск
        $("select.search_field").change(function() {
            paginator(0, this);
        });
        $(".sort_field").click(function(){
            var field = $(this).attr("id");
            var order = "ASC";
            var new_class = "sorting_asc";
            if($(this).hasClass("sorting_asc")) {
                new_class = "sorting_desc";
                order = "DESC";
            }
            $(this).parent().find(".sort_field").removeClass("sorting_asc").removeClass("sorting_desc");
            $(this).parent().find(".sort_field").addClass("sorting");
            $(this).removeClass("sorting").addClass(new_class);
            $(this).parents(".tableContent").find("input[name='order']").val(field+" "+order);
            paginator(0, this);
        });
        // разрешаем ввод только числа
        $("input.filter_number").bind("input",function(){
            var value = $(this).val();
            var re = /[^0-9]/gi;
            if (re.test(value)) {
                value = value.replace(re, '');
            }
            if(isNumber(value)) {
                $(this).val(parseFloat(value));
            } else {
                $(this).val(value);
            }
        });
       	$(".mws-form-dialog").dialog({
    		autoOpen: false, 
    		title: "Редактирование обьекта", 
    		modal: true, 
    		width: "640", 
    		buttons: [{
    				text: "Сохранить", 
    				click: function() {
    					$( this ).find('form').submit();
    				}}]
       	});
    });