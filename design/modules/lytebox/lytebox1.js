/*
				LyteBox v3.22, Markus F. Hay, www.dolem.com/lytebox
				Creative Commons Attribution 3.0 License (http://creativecommons.org/licenses/by/3.0/)
				
				Modified for Pagemap ImageWall
			*/
Array.prototype.removeDuplicates = function () {
    for (var a = 1; a < this.length; a++) this[a][0] == this[a - 1][0] && this.splice(a, 1)
};
Array.prototype.empty = function () {
    for (var a = 0; a <= this.length; a++) this.shift()
};
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "")
};

function LyteBox() {
    this.theme = "grey";
    this.outerBorder = this.hideFlash = true;
    this.resizeSpeed = 8;
    this.maxOpacity = 80;
    this.navType = 1;
    this.doAnimations = this.autoResize = true;
    this.borderSize = 0;
    this.slideInterval = 4E3;
    this.autoEnd = this.showPlayPause = this.showDetails = this.showClose = this.showNavigation = true;
    this.pauseOnNextClick = false;
    this.pauseOnPrevClick = true;
    if (this.resizeSpeed > 10) this.resizeSpeed = 10;
    if (this.resizeSpeed < 1) resizeSpeed = 1;
    this.resizeDuration = (11 - this.resizeSpeed) * 0.15;
    this.resizeWTimerArray = [];
    this.resizeWTimerCount = 0;
    this.resizeHTimerArray = [];
    this.resizeHTimerCount = 0;
    this.showContentTimerArray = [];
    this.showContentTimerCount = 0;
    this.overlayTimerArray = [];
    this.overlayTimerCount = 0;
    this.imageTimerArray = [];
    this.imageTimerCount = 0;
    this.timerIDArray = [];
    this.timerIDCount = 0;
    this.slideshowIDArray = [];
    this.slideshowIDCount = 0;
    this.imageArray = [];
    this.activeImage = null;
    this.slideArray = [];
    this.activeSlide = null;
    this.frameArray = [];
    this.activeFrame = null;
    this.checkFrame();
    this.ie7 = (this.ie = this.isLyteframe =
        this.isSlideshow = false) && window.XMLHttpRequest;
    this.initialize()
}
LyteBox.prototype.initialize = function () {
    this.updateLyteboxItems();
    var a = this.doc.getElementsByTagName("body").item(0);
    if (this.doc.getElementById("lbOverlay")) {
        a.removeChild(this.doc.getElementById("lbOverlay"));
        a.removeChild(this.doc.getElementById("lbMain"))
    }
    var b = this.doc.createElement("div");
    b.setAttribute("id", "lbOverlay");
    b.setAttribute(this.ie ? "className" : "class", this.theme);
    if (this.ie && !this.ie7 || this.ie7 && this.doc.compatMode == "BackCompat") b.style.position = "absolute";
    b.style.display = "none";
    a.appendChild(b);
    b = this.doc.createElement("div");
    b.setAttribute("id", "lbMain");
    b.style.display = "none";
    a.appendChild(b);
    var c = this.doc.createElement("div");
    c.setAttribute("id", "lbOuterContainer");
    c.setAttribute(this.ie ? "className" : "class", this.theme);
    b.appendChild(c);
    a = this.doc.createElement("div");
    a.setAttribute("id", "lbIframeContainer");
    a.style.display = "none";
    c.appendChild(a);
    var d = this.doc.createElement("iframe");
    d.setAttribute("id", "lbIframe");
    d.setAttribute("name", "lbIframe");
    d.style.display =
        "none";
    a.appendChild(d);
    a = this.doc.createElement("div");
    a.setAttribute("id", "lbImageContainer");
    c.appendChild(a);
    d = this.doc.createElement("img");
    d.setAttribute("id", "lbImage");
    a.appendChild(d);
    d = this.doc.createElement("div");
    d.setAttribute("id", "lbLoading");
    c.appendChild(d);
    d = this.doc.createElement("div");
    d.setAttribute("id", "lbDetailsContainer");
    d.setAttribute(this.ie ? "className" : "class", this.theme);
    b.appendChild(d);
    c = this.doc.createElement("div");
    c.setAttribute("id", "lbDetailsData");
    c.setAttribute(this.ie ?
        "className" : "class", this.theme);
    d.appendChild(c);
    b = this.doc.createElement("div");
    b.setAttribute("id", "lbDetails");
    c.appendChild(b);
    d = this.doc.createElement("span");
    d.setAttribute("id", "lbCaption");
    b.appendChild(d);
    d = this.doc.createElement("div");
    d.setAttribute("id", "lbHoverNav");
    a.appendChild(d);
    a = this.doc.createElement("div");
    a.setAttribute("id", "lbBottomNav");
    c.appendChild(a);
    c = this.doc.createElement("a");
    c.setAttribute("id", "lbPrev");
    c.setAttribute(this.ie ? "className" : "class", this.theme);
    c.setAttribute("href",
        "#");
    d.appendChild(c);
    c = this.doc.createElement("a");
    c.setAttribute("id", "lbNext");
    c.setAttribute(this.ie ? "className" : "class", this.theme);
    c.setAttribute("href", "#");
    d.appendChild(c);
    c = this.doc.createElement("span");
    c.setAttribute("id", "lbNumberDisplay");
    b.appendChild(c);
    c = this.doc.createElement("span");
    c.setAttribute("id", "lbNavDisplay");
    c.style.display = "none";
    b.appendChild(c);
    b = this.doc.createElement("a");
    b.setAttribute("id", "lbClose");
    b.setAttribute(this.ie ? "className" : "class", this.theme);
    b.setAttribute("href",
        "#");
    a.appendChild(b);
    b = this.doc.createElement("a");
    b.setAttribute("id", "lbPause");
    b.setAttribute(this.ie ? "className" : "class", this.theme);
    b.setAttribute("href", "#");
    b.style.display = "none";
    a.appendChild(b);
    b = this.doc.createElement("a");
    b.setAttribute("id", "lbPlay");
    b.setAttribute(this.ie ? "className" : "class", this.theme);
    b.setAttribute("href", "#");
    b.style.display = "none";
    a.appendChild(b)
};
LyteBox.prototype.updateLyteboxItems = function () {
    for (var a = this.isFrame ? window.parent.frames[window.name].document.getElementsByTagName("a") : document.getElementsByTagName("a"), b = 0; b < a.length; b++) {
        var c = a[b],
            d = String(c.getAttribute("rel"));
        if (c.getAttribute("href"))
            if (d.toLowerCase().match("lytebox")) c.onclick = function () {
                myLytebox.start(this, false, false);
                return false
            };
            else if (d.toLowerCase().match("lyteshow")) c.onclick = function () {
            myLytebox.start(this, true, false);
            return false
        };
        else if (d.toLowerCase().match("lyteframe")) c.onclick =
            function () {
                myLytebox.start(this, false, true);
                return false
        }
    }
};
LyteBox.prototype.start = function (a, b, c) {
    this.ie && !this.ie7 && this.toggleSelects("hide");
    this.hideFlash && this.toggleFlash("hide");
    this.isLyteframe = c ? true : false;
    c = this.getPageSize();
    var d = this.doc.getElementById("lbOverlay");
    this.doc.getElementsByTagName("body").item(0);
    d.style.height = c[1] + "px";
    d.style.display = "";
    this.appear("lbOverlay", this.doAnimations ? 0 : this.maxOpacity);
    d = this.isFrame ? window.parent.frames[window.name].document.getElementsByTagName("a") : document.getElementsByTagName("a");
    if (this.isLyteframe) {
        this.frameArray = [];
        this.frameNum = 0;
        if (a.getAttribute("rel") == "lyteframe") {
            var e = a.getAttribute("rev");
            this.frameArray.push(Array(a.getAttribute("href"), a.getAttribute("title"), e == null || e == "" ? "width: 400px; height: 400px; scrolling: auto;" : e))
        } else if (a.getAttribute("rel").indexOf("lyteframe") != -1) {
            for (var f = 0; f < d.length; f++) {
                var g = d[f];
                if (g.getAttribute("href") && g.getAttribute("rel") == a.getAttribute("rel")) {
                    e = g.getAttribute("rev");
                    this.frameArray.push(Array(g.getAttribute("href"), g.getAttribute("title"), e == null ||
                        e == "" ? "width: 400px; height: 400px; scrolling: auto;" : e))
                }
            }
            for (this.frameArray.removeDuplicates(); this.frameArray[this.frameNum][0] != a.getAttribute("href");) this.frameNum++
        }
    } else {
        this.imageArray = [];
        this.imageNum = 0;
        this.slideArray = [];
        this.slideNum = 0;
        if (a.getAttribute("rel") == "lytebox") this.imageArray.push(Array(a.getAttribute("href"), a.getAttribute("title")));
        else {
            if (a.getAttribute("rel").indexOf("lytebox") != -1) {
                for (f = 0; f < d.length; f++) {
                    g = d[f];
                    g.getAttribute("href") && g.getAttribute("rel") == a.getAttribute("rel") &&
                        this.imageArray.push(Array(g.getAttribute("href"), g.getAttribute("title")))
                }
                for (this.imageArray.removeDuplicates(); this.imageArray[this.imageNum][0] != a.getAttribute("href");) this.imageNum++
            }
            if (a.getAttribute("rel").indexOf("lyteshow") != -1) {
                for (f = 0; f < d.length; f++) {
                    g = d[f];
                    g.getAttribute("href") && g.getAttribute("rel") == a.getAttribute("rel") && this.slideArray.push(Array(g.getAttribute("href"), g.getAttribute("title")))
                }
                for (this.slideArray.removeDuplicates(); this.slideArray[this.slideNum][0] != a.getAttribute("href");) this.slideNum++
            }
        }
    }
    a =
        this.doc.getElementById("lbMain");
    a.style.top = this.getPageScroll() + c[3] / 15 + "px";
    a.style.display = "";
    if (this.outerBorder) {
        this.doc.getElementById("lbOuterContainer").style.borderBottom = "";
        this.doc.getElementById("lbOuterContainer").setAttribute(this.ie ? "className" : "class", this.theme)
    } else {
        this.doc.getElementById("lbOuterContainer").style.border = "none";
        this.doc.getElementById("lbDetailsContainer").style.border = "none"
    }
    this.doc.getElementById("lbOverlay").onclick = function () {
        myLytebox.end();
        return false
    };
    this.doc.getElementById("lbMain").onclick = function (h) {
        h || (h = window.parent.frames[window.name] && parent.document.getElementsByTagName("frameset").length <= 0 ? window.parent.window.event : window.event);
        if ((h.target ? h.target.id : h.srcElement.id) == "lbMain") {
            myLytebox.end();
            return false
        }
    };
    this.doc.getElementById("lbClose").onclick = function () {
        myLytebox.end();
        return false
    };
    this.doc.getElementById("lbPause").onclick = function () {
        myLytebox.togglePlayPause("lbPause", "lbPlay");
        return false
    };
    this.doc.getElementById("lbPlay").onclick =
        function () {
            myLytebox.togglePlayPause("lbPlay", "lbPause");
            return false
    };
    this.isSlideshow = b;
    this.isPaused = this.slideNum != 0 ? true : false;
    if (this.isSlideshow && this.showPlayPause && this.isPaused) {
        this.doc.getElementById("lbPlay").style.display = "";
        this.doc.getElementById("lbPause").style.display = "none"
    }
    if (this.isLyteframe) this.changeContent(this.frameNum);
    else this.isSlideshow ? this.changeContent(this.slideNum) : this.changeContent(this.imageNum)
};
LyteBox.prototype.changeContent = function (a) {
    if (this.isSlideshow)
        for (var b = 0; b < this.slideshowIDCount; b++) window.clearTimeout(this.slideshowIDArray[b]);
    this.activeImage = this.activeSlide = this.activeFrame = a;
    if (this.outerBorder) {
        this.doc.getElementById("lbOuterContainer").style.borderBottom = "";
        this.doc.getElementById("lbOuterContainer").setAttribute(this.ie ? "className" : "class", this.theme)
    } else {
        this.doc.getElementById("lbOuterContainer").style.border = "none";
        this.doc.getElementById("lbDetailsContainer").style.border =
            "none"
    }
    this.doc.getElementById("lbLoading").style.display = "";
    this.doc.getElementById("lbImage").style.display = "none";
    this.doc.getElementById("lbIframe").style.display = "none";
    this.doc.getElementById("lbPrev").style.display = "none";
    this.doc.getElementById("lbNext").style.display = "none";
    this.doc.getElementById("lbIframeContainer").style.display = "none";
    this.doc.getElementById("lbDetailsContainer").style.display = "none";
    this.doc.getElementById("lbNumberDisplay").style.display = "none";
    if (this.navType == 2 ||
        this.isLyteframe) {
        object = this.doc.getElementById("lbNavDisplay");
        object.innerHTML = '&nbsp;&nbsp;&nbsp;<span id="lbPrev2_Off" style="display: none;" class="' + this.theme + '">&laquo; prev</span><a href="#" id="lbPrev2" class="' + this.theme + '" style="display: none;">&laquo; prev</a> <b id="lbSpacer" class="' + this.theme + '">||</b> <span id="lbNext2_Off" style="display: none;" class="' + this.theme + '">next &raquo;</span><a href="#" id="lbNext2" class="' + this.theme + '" style="display: none;">next &raquo;</a>';
        object.style.display = "none"
    }
    if (this.isLyteframe) {
        a = myLytebox.doc.getElementById("lbIframe");
        var c = this.frameArray[this.activeFrame][2].split(";");
        for (b = 0; b < c.length; b++)
            if (c[b].indexOf("width:") >= 0) {
                var d = c[b].replace("width:", "");
                a.width = d.trim()
            } else if (c[b].indexOf("height:") >= 0) {
            d = c[b].replace("height:", "");
            a.height = d.trim()
        } else if (c[b].indexOf("scrolling:") >= 0) {
            d = c[b].replace("scrolling:", "");
            a.scrolling = d.trim()
        } else c[b].indexOf("border:");
        this.resizeContainer(parseInt(a.width), parseInt(a.height))
    } else {
        imgPreloader =
            new Image;
        imgPreloader.onload = function () {
            var e = imgPreloader.width,
                f = imgPreloader.height;
            if (myLytebox.autoResize) {
                var g = myLytebox.getPageSize(),
                    h = g[2] - 150;
                g = g[3] - 150;
                if (e > h) {
                    f = Math.round(f * (h / e));
                    e = h;
                    if (f > g) {
                        e = Math.round(e * (g / f));
                        f = g
                    }
                } else if (f > g) {
                    e = Math.round(e * (g / f));
                    f = g;
                    if (e > h) {
                        f = Math.round(f * (h / e));
                        e = h
                    }
                }
            }
            h = myLytebox.doc.getElementById("lbImage");
            h.src = myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][0] : myLytebox.imageArray[myLytebox.activeImage][0];
            h.width = e;
            h.height = f;
            myLytebox.resizeContainer(e,
                f);
            imgPreloader.onload = function () {}
        };
        imgPreloader.src = this.isSlideshow ? this.slideArray[this.activeSlide][0] : this.imageArray[this.activeImage][0]
    }
};
LyteBox.prototype.resizeContainer = function (a, b) {
    this.wCur = this.doc.getElementById("lbOuterContainer").offsetWidth;
    this.hCur = this.doc.getElementById("lbOuterContainer").offsetHeight;
    this.xScale = (a + this.borderSize * 2) / this.wCur * 100;
    this.yScale = (b + this.borderSize * 2) / this.hCur * 100;
    var c = this.wCur - this.borderSize * 2 - a,
        d = this.hCur - this.borderSize * 2 - b;
    if (d != 0) {
        this.hDone = false;
        this.resizeH("lbOuterContainer", this.hCur, b + this.borderSize * 2, this.getPixelRate(this.hCur, b))
    } else this.hDone = true; if (c != 0) {
        this.wDone =
            false;
        this.resizeW("lbOuterContainer", this.wCur, a + this.borderSize * 2, this.getPixelRate(this.wCur, a))
    } else this.wDone = true; if (d == 0 && c == 0) this.ie ? this.pause(250) : this.pause(100);
    this.doc.getElementById("lbPrev").style.height = b + "px";
    this.doc.getElementById("lbNext").style.height = b + "px";
    this.doc.getElementById("lbDetailsContainer").style.width = a + this.borderSize * 2 + (this.ie && this.doc.compatMode == "BackCompat" && this.outerBorder ? 2 : 0) + "px";
    this.showContent()
};
LyteBox.prototype.showContent = function () {
    if (this.wDone && this.hDone) {
        for (var a = 0; a < this.showContentTimerCount; a++) window.clearTimeout(this.showContentTimerArray[a]);
        if (this.outerBorder) this.doc.getElementById("lbOuterContainer").style.borderBottom = "none";
        this.doc.getElementById("lbLoading").style.display = "none";
        if (this.isLyteframe) {
            this.doc.getElementById("lbIframe").style.display = "";
            this.appear("lbIframe", this.doAnimations ? 0 : 100)
        } else {
            this.doc.getElementById("lbImage").style.display = "";
            this.appear("lbImage",
                this.doAnimations ? 0 : 100);
            this.preloadNeighborImages()
        } if (this.isSlideshow) {
            if (this.activeSlide == this.slideArray.length - 1) {
                if (this.autoEnd) this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.end('slideshow')", this.slideInterval)
            } else this.isPaused || (this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.changeContent(" + (this.activeSlide + 1) + ")", this.slideInterval));
            this.doc.getElementById("lbHoverNav").style.display = this.showNavigation && this.navType == 1 ? "" : "none";
            this.doc.getElementById("lbClose").style.display =
                this.showClose ? "" : "none";
            this.doc.getElementById("lbDetails").style.display = this.showDetails ? "" : "none";
            this.doc.getElementById("lbPause").style.display = this.showPlayPause && !this.isPaused ? "" : "none";
            this.doc.getElementById("lbPlay").style.display = this.showPlayPause && !this.isPaused ? "none" : "";
            this.doc.getElementById("lbNavDisplay").style.display = this.showNavigation && this.navType == 2 ? "" : "none"
        } else {
            this.doc.getElementById("lbHoverNav").style.display = this.navType == 1 && !this.isLyteframe ? "" : "none";
            if (this.navType ==
                2 && !this.isLyteframe && this.imageArray.length > 1 || this.frameArray.length > 1 && this.isLyteframe) this.doc.getElementById("lbNavDisplay").style.display = "";
            else this.doc.getElementById("lbNavDisplay").style.display = "none";
            this.doc.getElementById("lbClose").style.display = "";
            this.doc.getElementById("lbDetails").style.display = "";
            this.doc.getElementById("lbPause").style.display = "none";
            this.doc.getElementById("lbPlay").style.display = "none"
        }
        this.doc.getElementById("lbImageContainer").style.display = this.isLyteframe ?
            "none" : "";
        this.doc.getElementById("lbIframeContainer").style.display = this.isLyteframe ? "" : "none";
        try {
            this.doc.getElementById("lbIframe").src = this.frameArray[this.activeFrame][0]
        } catch (b) {}
    } else this.showContentTimerArray[this.showContentTimerCount++] = setTimeout("myLytebox.showContent()", 200)
};
LyteBox.prototype.updateDetails = function () {
    var a = this.doc.getElementById("lbCaption"),
        b = this.isSlideshow ? this.slideArray[this.activeSlide][1] : this.isLyteframe ? this.frameArray[this.activeFrame][1] : this.imageArray[this.activeImage][1];
    a.style.display = "";
    a.innerHTML = b == null ? "" : b;
    this.updateNav();
    this.doc.getElementById("lbDetailsContainer").style.display = "";
    a = this.doc.getElementById("lbNumberDisplay");
    if (this.isSlideshow && this.slideArray.length > 1) {
        a.style.display = "";
        a.innerHTML = eval(this.activeSlide +
            1) + " of " + this.slideArray.length;
        this.doc.getElementById("lbNavDisplay").style.display = this.navType == 2 && this.showNavigation ? "" : "none"
    } else if (this.imageArray.length > 1 && !this.isLyteframe) {
        a.style.display = "";
        a.innerHTML = +eval(this.activeImage + 1) + " of " + this.imageArray.length;
        this.doc.getElementById("lbNavDisplay").style.display = this.navType == 2 ? "" : "none"
    } else if (this.frameArray.length > 1 && this.isLyteframe) {
        a.style.display = "";
        a.innerHTML = "Page " + eval(this.activeFrame + 1) + " of " + this.frameArray.length;
        this.doc.getElementById("lbNavDisplay").style.display = ""
    } else this.doc.getElementById("lbNavDisplay").style.display = "none";
    this.appear("lbDetailsContainer", this.doAnimations ? 0 : 100)
};
LyteBox.prototype.updateNav = function () {
    if (this.isSlideshow) {
        if (this.activeSlide != 0) {
            var a = this.navType == 2 ? this.doc.getElementById("lbPrev2") : this.doc.getElementById("lbPrev");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.pauseOnPrevClick && myLytebox.togglePlayPause("lbPause", "lbPlay");
                myLytebox.changeContent(myLytebox.activeSlide - 1);
                return false
            }
        } else if (this.navType == 2) this.doc.getElementById("lbPrev2_Off").style.display = "";
        if (this.activeSlide != this.slideArray.length - 1) {
            a = this.navType == 2 ? this.doc.getElementById("lbNext2") :
                this.doc.getElementById("lbNext");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.pauseOnNextClick && myLytebox.togglePlayPause("lbPause", "lbPlay");
                myLytebox.changeContent(myLytebox.activeSlide + 1);
                return false
            }
        } else if (this.navType == 2) this.doc.getElementById("lbNext2_Off").style.display = ""
    } else if (this.isLyteframe) {
        if (this.activeFrame != 0) {
            a = this.doc.getElementById("lbPrev2");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.changeContent(myLytebox.activeFrame - 1);
                return false
            }
        } else this.doc.getElementById("lbPrev2_Off").style.display =
            ""; if (this.activeFrame != this.frameArray.length - 1) {
            a = this.doc.getElementById("lbNext2");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.changeContent(myLytebox.activeFrame + 1);
                return false
            }
        } else this.doc.getElementById("lbNext2_Off").style.display = ""
    } else {
        if (this.activeImage != 0) {
            a = this.navType == 2 ? this.doc.getElementById("lbPrev2") : this.doc.getElementById("lbPrev");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.changeContent(myLytebox.activeImage - 1);
                return false
            }
        } else if (this.navType == 2) this.doc.getElementById("lbPrev2_Off").style.display =
            "";
        if (this.activeImage != this.imageArray.length - 1) {
            a = this.navType == 2 ? this.doc.getElementById("lbNext2") : this.doc.getElementById("lbNext");
            a.style.display = "";
            a.onclick = function () {
                myLytebox.changeContent(myLytebox.activeImage + 1);
                return false
            }
        } else if (this.navType == 2) this.doc.getElementById("lbNext2_Off").style.display = ""
    }
    this.enableKeyboardNav()
};
LyteBox.prototype.enableKeyboardNav = function () {
    document.onkeydown = this.keyboardAction
};
LyteBox.prototype.disableKeyboardNav = function () {
    document.onkeydown = ""
};
LyteBox.prototype.keyboardAction = function (a) {
    var b = key = escape = null;
    b = a == null ? event.keyCode : a.which;
    key = String.fromCharCode(b).toLowerCase();
    escape = a == null ? 27 : a.DOM_VK_ESCAPE;
    if (key == "x" || key == "c" || b == escape) myLytebox.end();
    else if (key == "p" || b == 37)
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide - 1)
            }
        } else if (myLytebox.isLyteframe) {
        if (myLytebox.activeFrame != 0) {
            myLytebox.disableKeyboardNav();
            myLytebox.changeContent(myLytebox.activeFrame -
                1)
        }
    } else {
        if (myLytebox.activeImage != 0) {
            myLytebox.disableKeyboardNav();
            myLytebox.changeContent(myLytebox.activeImage - 1)
        }
    } else if (key == "n" || b == 39)
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != myLytebox.slideArray.length - 1) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide + 1)
            }
        } else if (myLytebox.isLyteframe) {
        if (myLytebox.activeFrame != myLytebox.frameArray.length - 1) {
            myLytebox.disableKeyboardNav();
            myLytebox.changeContent(myLytebox.activeFrame + 1)
        }
    } else if (myLytebox.activeImage !=
        myLytebox.imageArray.length - 1) {
        myLytebox.disableKeyboardNav();
        myLytebox.changeContent(myLytebox.activeImage + 1)
    }
};
LyteBox.prototype.preloadNeighborImages = function () {
    if (this.isSlideshow) {
        if (this.slideArray.length - 1 > this.activeSlide) {
            preloadNextImage = new Image;
            preloadNextImage.src = this.slideArray[this.activeSlide + 1][0]
        }
        if (this.activeSlide > 0) {
            preloadPrevImage = new Image;
            preloadPrevImage.src = this.slideArray[this.activeSlide - 1][0]
        }
    } else {
        if (this.imageArray.length - 1 > this.activeImage) {
            preloadNextImage = new Image;
            preloadNextImage.src = this.imageArray[this.activeImage + 1][0]
        }
        if (this.activeImage > 0) {
            preloadPrevImage = new Image;
            preloadPrevImage.src = this.imageArray[this.activeImage - 1][0]
        }
    }
};
LyteBox.prototype.togglePlayPause = function (a, b) {
    if (this.isSlideshow && a == "lbPause")
        for (var c = 0; c < this.slideshowIDCount; c++) window.clearTimeout(this.slideshowIDArray[c]);
    this.doc.getElementById(a).style.display = "none";
    this.doc.getElementById(b).style.display = "";
    if (a == "lbPlay") {
        this.isPaused = false;
        this.activeSlide == this.slideArray.length - 1 ? this.end() : this.changeContent(this.activeSlide + 1)
    } else this.isPaused = true
};
LyteBox.prototype.end = function (a) {
    a = a == "slideshow" ? false : true;
    if (!(this.isSlideshow && this.isPaused && !a)) {
        this.disableKeyboardNav();
        this.doc.getElementById("lbMain").style.display = "none";
        this.fade("lbOverlay", this.doAnimations ? this.maxOpacity : 0);
        this.toggleSelects("visible");
        this.hideFlash && this.toggleFlash("visible");
        if (this.isSlideshow)
            for (a = 0; a < this.slideshowIDCount; a++) window.clearTimeout(this.slideshowIDArray[a]);
        this.isLyteframe && this.initialize()
    }
};
LyteBox.prototype.checkFrame = function () {
    if (window.parent.frames[window.name] && parent.document.getElementsByTagName("frameset").length <= 0) {
        this.isFrame = true;
        this.lytebox = "window.parent." + window.name + ".myLytebox";
        this.doc = parent.document
    } else {
        this.isFrame = false;
        this.lytebox = "myLytebox";
        this.doc = document
    }
};
LyteBox.prototype.getPixelRate = function (a, b) {
    var c = b > a ? b - a : a - b;
    if (c >= 0 && c <= 100) return 10;
    if (c > 100 && c <= 200) return 15;
    if (c > 200 && c <= 300) return 20;
    if (c > 300 && c <= 400) return 25;
    if (c > 400 && c <= 500) return 30;
    if (c > 500 && c <= 600) return 35;
    if (c > 600 && c <= 700) return 40;
    if (c > 700) return 45
};
LyteBox.prototype.appear = function (a, b) {
    var c = this.doc.getElementById(a).style;
    c.opacity = b / 100;
    c.MozOpacity = b / 100;
    c.KhtmlOpacity = b / 100;
    c.filter = "alpha(opacity=" + (b + 10) + ")";
    if (b == 100 && (a == "lbImage" || a == "lbIframe")) {
        try {
            c.removeAttribute("filter")
        } catch (d) {}
        this.updateDetails()
    } else if (b >= this.maxOpacity && a == "lbOverlay")
        for (c = 0; c < this.overlayTimerCount; c++) window.clearTimeout(this.overlayTimerArray[c]);
    else if (b >= 100 && a == "lbDetailsContainer") {
        try {
            c.removeAttribute("filter")
        } catch (e) {}
        for (c = 0; c < this.imageTimerCount; c++) window.clearTimeout(this.imageTimerArray[c]);
        this.doc.getElementById("lbOverlay").style.height = this.getPageSize()[1] + "px"
    } else if (a == "lbOverlay") this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.appear('" + a + "', " + (b + 20) + ")", 1);
    else this.imageTimerArray[this.imageTimerCount++] = setTimeout("myLytebox.appear('" + a + "', " + (b + 10) + ")", 1)
};
LyteBox.prototype.fade = function (a, b) {
    var c = this.doc.getElementById(a).style;
    c.opacity = b / 100;
    c.MozOpacity = b / 100;
    c.KhtmlOpacity = b / 100;
    c.filter = "alpha(opacity=" + b + ")";
    if (b <= 0) try {
        c.display = "none"
    } catch (d) {} else if (a == "lbOverlay") this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.fade('" + a + "', " + (b - 20) + ")", 1);
    else this.timerIDArray[this.timerIDCount++] = setTimeout("myLytebox.fade('" + a + "', " + (b - 10) + ")", 1)
};
LyteBox.prototype.resizeW = function (a, b, c, d, e) {
    if (this.hDone) {
        var f = this.doc.getElementById(a);
        e = e ? e : this.resizeDuration / 2;
        b = this.doAnimations ? b : c;
        f.style.width = b + "px";
        if (b < c) b += b + d >= c ? c - b : d;
        else if (b > c) b -= b - d <= c ? b - c : d;
        this.resizeWTimerArray[this.resizeWTimerCount++] = setTimeout("myLytebox.resizeW('" + a + "', " + b + ", " + c + ", " + d + ", " + (e + 0.02) + ")", e + 0.02);
        if (parseInt(f.style.width) == c) {
            this.wDone = true;
            for (a = 0; a < this.resizeWTimerCount; a++) window.clearTimeout(this.resizeWTimerArray[a])
        }
    } else this.resizeWTimerArray[this.resizeWTimerCount++] =
        setTimeout("myLytebox.resizeW('" + a + "', " + b + ", " + c + ", " + d + ")", 100)
};
LyteBox.prototype.resizeH = function (a, b, c, d, e) {
    e = e ? e : this.resizeDuration / 2;
    var f = this.doc.getElementById(a);
    b = this.doAnimations ? b : c;
    f.style.height = b + "px";
    if (b < c) b += b + d >= c ? c - b : d;
    else if (b > c) b -= b - d <= c ? b - c : d;
    this.resizeHTimerArray[this.resizeHTimerCount++] = setTimeout("myLytebox.resizeH('" + a + "', " + b + ", " + c + ", " + d + ", " + (e + 0.02) + ")", e + 0.02);
    if (parseInt(f.style.height) == c) {
        this.hDone = true;
        for (a = 0; a < this.resizeHTimerCount; a++) window.clearTimeout(this.resizeHTimerArray[a])
    }
};
LyteBox.prototype.getPageScroll = function () {
    if (self.pageYOffset) return this.isFrame ? parent.pageYOffset : self.pageYOffset;
    else if (this.doc.documentElement && this.doc.documentElement.scrollTop) return this.doc.documentElement.scrollTop;
    else if (document.body) return this.doc.body.scrollTop
};
LyteBox.prototype.getPageSize = function () {
    var a, b, c, d;
    if (window.innerHeight && window.scrollMaxY) {
        a = this.doc.scrollWidth;
        b = (this.isFrame ? parent.innerHeight : self.innerHeight) + (this.isFrame ? parent.scrollMaxY : self.scrollMaxY)
    } else if (this.doc.body.scrollHeight > this.doc.body.offsetHeight) {
        a = this.doc.body.scrollWidth;
        b = this.doc.body.scrollHeight
    } else {
        a = this.doc.getElementsByTagName("html").item(0).offsetWidth;
        b = this.doc.getElementsByTagName("html").item(0).offsetHeight;
        a = a < this.doc.body.offsetWidth ? this.doc.body.offsetWidth :
            a;
        b = b < this.doc.body.offsetHeight ? this.doc.body.offsetHeight : b
    } if (self.innerHeight) {
        c = this.isFrame ? parent.innerWidth : self.innerWidth;
        d = this.isFrame ? parent.innerHeight : self.innerHeight
    } else if (document.documentElement && document.documentElement.clientHeight) {
        c = this.doc.documentElement.clientWidth;
        d = this.doc.documentElement.clientHeight
    } else if (document.body) {
        c = this.doc.getElementsByTagName("html").item(0).clientWidth;
        d = this.doc.getElementsByTagName("html").item(0).clientHeight;
        c = c == 0 ? this.doc.body.clientWidth :
            c;
        d = d == 0 ? this.doc.body.clientHeight : d
    }
    return Array(a < c ? c : a, b < d ? d : b, c, d)
};
LyteBox.prototype.toggleFlash = function (a) {
    for (var b = this.doc.getElementsByTagName("object"), c = 0; c < b.length; c++) b[c].style.visibility = a == "hide" ? "hidden" : "visible";
    var d = this.doc.getElementsByTagName("embed");
    for (c = 0; c < d.length; c++) d[c].style.visibility = a == "hide" ? "hidden" : "visible";
    if (this.isFrame)
        for (c = 0; c < parent.frames.length; c++) {
            try {
                b = parent.frames[c].window.document.getElementsByTagName("object");
                for (var e = 0; e < b.length; e++) b[e].style.visibility = a == "hide" ? "hidden" : "visible"
            } catch (f) {}
            try {
                d = parent.frames[c].window.document.getElementsByTagName("embed");
                for (e = 0; e < d.length; e++) d[e].style.visibility = a == "hide" ? "hidden" : "visible"
            } catch (g) {}
        }
};
LyteBox.prototype.toggleSelects = function (a) {
    for (var b = this.doc.getElementsByTagName("select"), c = 0; c < b.length; c++) b[c].style.visibility = a == "hide" ? "hidden" : "visible";
    if (this.isFrame)
        for (c = 0; c < parent.frames.length; c++) try {
            b = parent.frames[c].window.document.getElementsByTagName("select");
            for (var d = 0; d < b.length; d++) b[d].style.visibility = a == "hide" ? "hidden" : "visible"
        } catch (e) {}
};
LyteBox.prototype.pause = function (a) {
    var b = new Date;
    for (a = b.getTime() + a;;) {
        b = new Date;
        if (b.getTime() > a) return
    }
};
/*if (window.addEventListener) window.addEventListener("load", initLytebox, false);
else if (window.attachEvent) window.attachEvent("onload", initLytebox);
else window.onload = function () {
    initLytebox()
};
*/
function initLytebox() {
    myLytebox = new LyteBox
};