var Uploader = {
    isDebug: true
};
Uploader.log = function(data) {
    if(Uploader.isDebug) {
        console.log(data);
    }
}
Uploader.init = function(uploader, args){
    // ID елемента
    var element = args.browse_button == undefined ? args.drop_element : args.browse_button;
    var options = {
		runtimes : args.runtimes,
        url: args.url,
		max_file_size : args.max_file_size,
		flash_swf_url : args.flash_swf_url,
        multi_selection: args.multi_selection,
        urlstream_upload: true,
		filters : [args.filters]
	};
    if(args.drop_element != undefined) {
        options.drop_element = args.drop_element;
    } else {
        options.browse_button = args.browse_button;
        options.multipart_params = args.multipart_params;
    }
    if(undefined != args.container)
        options.container = args.container;
    uploader = new plupload.Uploader(options);
    // инициализация загрузчика
	uploader.bind('Init', function(up, params) {
	   if(args.init != undefined)
            eval(args.init).call(null, up, params);
	});
    // изменнеия очереди файлов
    uploader.bind("QueueChanged", function(up) {
	   if(args.queueChanged != undefined)
            eval(args.queueChanged).call(null, up);
        if (up.files.length > 0 && up.state != 2) 
        {
           up.start();;
        }
    }); 
	// перед добавлением файла в очередь
	uploader.bind('FilesAdded', function(up, files) {
	   if(args.filesAdded != undefined)
            eval(args.filesAdded).call(null, up, files);
	});
    // статус загрузки файла
	uploader.bind('UploadProgress', function(up, file) {
        if(args.uploadProgress != undefined)
            eval(args.uploadProgress).call(null, up, file);
	});
    // ошибки
	uploader.bind('Error', function(up, err) {
        if(args.error != undefined)
            eval(args.error).call(null, up, err);
        Uploader.log(err);
	});
    // файл загружен
	uploader.bind('FileUploaded', function(up, file, info) {
        Uploader.log(info);
        if(args.fileUploaded != undefined)
            eval(args.fileUploaded).call(null, up, file, info);
       
	});
    // инициализация
    uploader.init();
    
    var interval = setInterval(function() {
        // при изменении позиции элемента обновляем позицию загрузчика
        uploader.refresh();
        // если елемент был удалён, то удаляем загрузчик
        if(!$("#"+element).length) {
            clearInterval(interval);
            $("#"+uploader.id+"_flash_container").remove();
        }
    }, 100);
}