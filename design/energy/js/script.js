jQuery(document).ready(function() {

    jQuery(".various").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : false,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(58, 42, 45, 0.95)'
                }
            }
        }
    });

    //    jQuery('.point_1').mouseenter(function() {
    //        jQuery('#lamp').css('background-image', 'url(./images/lamp_1.png)').preload();
    //    });
    //    jQuery('.point_1').mouseleave(function() {
    //        jQuery('.my_lamp_x').css('background-image', 'url(./images/lamp_1.png)').preload();
    //    });
    //    jQuery('.point_2').mouseenter(function() {
    //jQuery('#lamp').css('background-image', 'url(./images/lamp_2.png)').preload();
    //    });
    //    jQuery('.point_2').mouseleave(function() {
    //        jQuery('.my_lamp_x').css('background-image', 'url(./images/lamp_2.png)').preload();
    //    });
    //    jQuery('.point_3').mouseenter(function() {
    //        jQuery('#lamp').css('background-image', 'url(./images/lamp_3.png)').preload();
    //    });
    //    jQuery('.point_3').mouseleave(function() {
    //        jQuery('.my_lamp_x').css('background-image', 'url(./images/lamp_3.png)').preload();
    //    });
    //    jQuery('.point_4').mouseenter(function() {
    //        jQuery('#lamp').css('background-image', 'url(./images/lamp_4.png)').preload();
    //    });
    //    jQuery('.point_4').mouseleave(function() {
    //        jQuery('.my_lamp_x').css('background-image', 'url(./images/lamp_4.png)').preload();
    //    });
    //    jQuery('.point_5').mouseenter(function() {
    //        jQuery('#lamp').css('background-image', 'url(./images/lamp_5.png)').preload();
    //    });
    //    jQuery('.point_5').mouseleave(function() {
    //        jQuery('.my_lamp_x').css('background-image', 'url(./images/lamp_5.png)').preload();
    //    });
    // popup_point
    jQuery('.point_1').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_1').fadeIn();
        jQuery('#contentEnergy__checkpoints_item1').css('visibility', 'hidden');
    });
    jQuery('.popup_1').mouseleave(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('#contentEnergy__checkpoints_item1').css('visibility', 'visible');
    });
    // popup_point
    jQuery('.point_2').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        jQuery('.popup_2').fadeIn();
        jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('#contentEnergy__checkpoints_item2').css('visibility', 'hidden');
    });
    jQuery('.popup_2').mouseleave(function() {
        jQuery('.popup_2').fadeOut();
        jQuery('#contentEnergy__checkpoints_item2').css('visibility', 'visible');
    });
    // popup_point
    jQuery('.point_3').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        jQuery('.popup_3').fadeIn();
        jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('#contentEnergy__checkpoints_item3').css('visibility', 'hidden');
    });
    jQuery('.popup_3').mouseleave(function() {
        jQuery('.popup_3').fadeOut();
        jQuery('#contentEnergy__checkpoints_item3').css('visibility', 'visible');
    });
    // popup_point
    jQuery('.point_4').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        jQuery('.popup_4').fadeIn();
        jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('#contentEnergy__checkpoints_item4').css('visibility', 'hidden');
    });
    jQuery('.popup_4').mouseleave(function() {
        jQuery('.popup_4').fadeOut();
        jQuery('#contentEnergy__checkpoints_item4').css('visibility', 'visible');
    });
    //                ===================================================
    //                popup_line 1
    //                  *
    jQuery('.point_6_1').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });

    // **
    jQuery('.point_6_2').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });

    //***
    jQuery('.point_6_3').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });

    //****
    jQuery('.point_6_4').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });

    //*****
    jQuery('.point_6_5').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });


    //*** *** 
    jQuery('.point_6_6').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });



    //*** *** *
    jQuery('.point_6_7').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeIn();
    });





    //popup_line 2
    //                  *
    jQuery('.point_7_1').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    // **
    jQuery('.point_7_2').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    //***
    jQuery('.point_7_3').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    ;
    });

    //****
    jQuery('.point_7_4').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    //*****
    jQuery('.point_7_5').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });


    //*** *** 
    jQuery('.point_7_6').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });



    //*** *** *
    jQuery('.point_7_7').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    //*** *** **
    jQuery('.point_7_8').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    //*** *** ***
    jQuery('.point_7_9').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });


    //*** *** **
    jQuery('.point_7_10').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });

    //*** *** **
    jQuery('.point_7_11').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeIn();
        jQuery('.popup_8').fadeOut();
        jQuery('.popup_6').fadeOut();
    });


    //popup_line 3
    //                  *
    jQuery('.point_8_1').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeIn();
        jQuery('.popup_6').fadeOut();
    });

    // **
    jQuery('.point_8_2').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeIn();
        jQuery('.popup_6').fadeOut();
    });

    //***
    jQuery('.point_8_3').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeIn();
        jQuery('.popup_6').fadeOut();
    ;
    });

    //****
    jQuery('.point_8_4').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeIn();
        jQuery('.popup_6').fadeOut();
    });

    //*****
    jQuery('.point_8_5').mouseenter(function() {
        jQuery('.popup_1').fadeOut();
        jQuery('.popup_2').fadeOut();
        jQuery('.popup_3').fadeOut();
        jQuery('.popup_4').fadeOut();
        //                    jQuery('.popup_6').fadeOut();
        jQuery('.popup_7').fadeOut();
        jQuery('.popup_8').fadeIn();
        jQuery('.popup_6').fadeOut();
    });





    //                =================================================

    // flybtn 1 1
//    jQuery('.popup_1_about_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_1_about_link').attr('href');
//    });
    jQuery('.popup_1_about_link').mouseenter(function() {
        jQuery('.popup_1_about_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
        jQuery('.popup_1_about_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_1_about_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });

    });
    jQuery('.popup_1_login_link').mouseenter(function() {
        jQuery('.popup_1_login_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
        jQuery('.popup_1_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_1_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });

    });
    jQuery('.popup_1').mouseleave(function() {
        jQuery('.popup_1_about_img, .popup_1_login_img').css("display", "inline-block").css("margin-left", "0px").css("opacity", "1");
    });
    
    
    // flybtn 2 1
//    jQuery('.popup_2_about_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_2_about_link').attr('href');
//    });
    jQuery('.popup_2_about_link').mouseenter(function() {
        jQuery('.popup_2_about_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_2_about_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });

    jQuery('.popup_2_login_link').mouseenter(function() {
        jQuery('.popup_2_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_2_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });

    jQuery('.popup_2').mouseleave(function() {
        jQuery('.popup_2_about_img, .popup_2_login_img').css("display", "inline-block").css("margin-left", "0px").css("opacity", "1");
    });
    
    // flybtn 3 1
//    jQuery('.popup_3_about_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_3_about_link').attr('href');
//    });
    jQuery('.popup_3_about_link').mouseenter(function() {
        jQuery('.popup_3_about_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_3_about_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });
    jQuery('.popup_3_login_link').mouseenter(function() {
        jQuery('.popup_3_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_3_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });

    jQuery('.popup_3').mouseleave(function() {
        jQuery('.popup_3_about_img, .popup_3_login_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
    });
    
    // flybtn 4 1
//    jQuery('.popup_4_about_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_4_about_link').attr('href');
//    });
    jQuery('.popup_4_about_link_wrapper').mouseenter(function() {
        jQuery('.popup_4_about_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_4_about_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });
    jQuery('.popup_4').mouseleave(function() {
        jQuery('.popup_4_about_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
    });
    


    //////////////////////////////////////
    // flybtn 6 1
//    jQuery('.popup_6_login_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_6_login_link').attr('href');
//    });
    jQuery('.popup_6_login_link_wrapper').mouseenter(function() {
        jQuery('.popup_6_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_6_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });
    jQuery('.popup_6').mouseleave(function(){
        jQuery('.popup_6_login_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
        jQuery(this).fadeOut(); 
    });

    // flybtn 7 1
//    jQuery('.popup_7_login_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_7_login_link').attr('href');
//    });
    jQuery('.popup_7_login_link_wrapper').mouseenter(function() {
        jQuery('.popup_7_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_7_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });
    jQuery('.popup_7').mouseleave(function(){ 
        jQuery('.popup_7_login_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
        jQuery(this).fadeOut(); 
    });

    // flybtn 8 1
//    jQuery('.popup_8_login_link_wrapper').click(function() {
//        window.location.href = jQuery('.popup_8_login_link').attr('href');
//    });
    jQuery('.popup_8_login_link_wrapper').mouseenter(function() {
        jQuery('.popup_8_login_img').animate({
            opacity: 1,
            marginLeft: "10px",
            display: "none"
        }, 400, function() {
            //alert();
            jQuery('.popup_8_login_img').css("display", "inline").css("margin-left", "10px").css("opacity", "1");
        });
    });
    jQuery('.popup_8').mouseleave(function(){ 
        jQuery('.popup_8_login_img').css("display", "inline").css("margin-left", "0px").css("opacity", "1");
        jQuery(this).fadeOut(); 
    });


//                =====================================================


});
