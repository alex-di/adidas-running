<?php

//error_reporting(E_ALL  & ~E_STRICT);
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');
ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] .'/');

date_default_timezone_set('Europe/Moscow');

// Указание пути к директории приложениЯ
defined('ROOT_PATH')
    || define('ROOT_PATH',
              realpath(dirname(__FILE__)));

// Указание пути к директории приложениЯ
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH',
              realpath(dirname(__FILE__) . '/application'));
 
// Определение текущего режима работы приложениЯ
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'development'));

// Обычно требуетсЯ также добавить директорию library/
// в include_path, особенно если она содержит инсталляцию ZF
set_include_path(implode(PATH_SEPARATOR, array(
    dirname(__FILE__) . '/library', "/"
    
)));

//print_r(get_include_path());die;
/** Zend_Application */
require_once 'library/Zend/Application.php';
 
#DR_Core Api 
require_once 'library/DR/api.php';

// Создание объекта приложениЯ, начальнаЯ загрузка, запуск
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap()
            ->run();

function _d($data, $isDie = true) {
    echo "<pre>";
    print_r($data);
    if($isDie) die;
}