<?php

ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');

date_default_timezone_set('Europe/Moscow');

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../../'));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Определение текущего режима работы приложениЯ
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

set_include_path(realpath(dirname(__FILE__) . '/../../library'));

require_once 'library/Zend/Application.php';
require_once 'DR/api.php';


class cronShares
{

    public $_config;
    public function __construct()
    {
        $this->_initApp();
        $this->_initConfig();
        $this->_initDB();
    }

    public function doJob()
    {
        $data = api::getMaterials()->_new(array('t.id','t.name', 't.stitle', 't.rate'))
                           ->notIn('t.category_id', array(Model_Materials::CATEGORY_TWITTER_POST, Model_Materials::CATEGORY_INSTAGRAM_POST, Model_Materials::CATEGORY_RUNBASE, Model_Materials::CATEGORY_UNIQUE))
                           ->where('t.is_modern', Model_Materials::TYPE_MODERN)
                           ->order('t.rate DESC')
                           ->rows();

        $shares = api::getShares()->rows();
        if (!empty($shares)) {
            foreach ($shares as $row) {
                $row->delete();
            }    
        }
        
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $share = array('id_material' => $data[$key]['id'],
                               'name_material' => $data[$key]['name'],
                               'alias_material' => $data[$key]['stitle']);

                if (isset($data[$key]['stitle']) && !empty($data[$key]['stitle'])) {
                    $materialURL = 'http://adidas-running.ru/article/' . $data[$key]['stitle'];
                    $shareCount = json_decode(file_get_contents('http://cdn.api.twitter.com/1/urls/count.json?url=' . $materialURL));
                    if (isset($shareCount->count)) {
                        $share['tw_share_count'] = $shareCount->count;
                    }

                    $shareCount = json_decode(file_get_contents('http://graph.facebook.com/?id=' . $materialURL));
                    if (isset($shareCount->shares)) {
                        $share['fb_share_count'] = $shareCount->shares;
                    } else {
                        $share['fb_share_count'] = 0;
                    }

                    $shareCount = file_get_contents('http://vk.com/share.php?act=count&url=' . $materialURL);
                    if (!empty($shareCount)) {
                        $share['vk_share_count'] = (int) substr($shareCount, 18, -2);
                    }

                    $share['gplus_share_count'] = 0;
                }

                api::getShares()->doSave($share);
            }
        }
    }

    private function _initApp()
    {
        $application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );

        $application->bootstrap();
    }

    private function _initConfig()
    {
        require APPLICATION_PATH . '/settings/config.php';
        $this->_config = new Zend_Config($config);
    }

    private function _initDB()
    {
        $mysql = Zend_Db::factory($this->_config->mysql);
        Zend_Db_Table_Abstract::setDefaultAdapter($mysql);
        Zend_Registry::set('mysql', $mysql);
        Zend_Registry::set('db_meta_cache', $this->_config->debug->metacache);
        Zend_Registry::set('db_cache', $this->_config->debug->cache);
        Zend_Registry::set('debug', $this->_config->debug->on);
    }

}